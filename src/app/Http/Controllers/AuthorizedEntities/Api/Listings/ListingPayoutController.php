<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use App\Actions\Listings\Payouts\CreatePayout;
use App\Actions\Listings\Payouts\GetPayouts;
use App\Actions\Listings\Payouts\PayForInvestors;
use App\Common\ValidationRules\Listings\ListingPayoutRules;
use App\Enums\Role;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\Payouts\Payout;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorizedEntities\Listings\ListingPayoutResource;
use Illuminate\Support\Facades\Validator;

class ListingPayoutController extends Controller
{
    use CanAccessListing;

    /**
     * List Payouts
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Listing $listing)
    {
        $this->canUserAccessListing($listing);

        return ListingPayoutResource::collection(
            GetPayouts::run($listing)
        );
    }

    /**
     * List Payout details
     *
     * @param \App\Models\Payouts\Payout $payout
     * @return \App\Http\Resources\AuthorizedEntities\Listings\ListingPayoutResource
     */
    public function show($listing, Payout $payout)
    {
        $this->canUserAccessListing($payout->listing);

        $payout->load(['investors.investments', 'listing']);

        return new ListingPayoutResource($payout);
    }

    /**
     * Store payout
     *
     * @param \Illuminate\Http\Request $request
     * @param App\Models\Listings\Listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        if (!request()->user('sanctum')->can('access-admin-portal')) {
            throw new AuthorizationException;
        }

        $this->canUserAccessListing($listing);

        return DB::transaction(function () use ($request, $listing) {
            $data = $this->validator($request->all())->validate();

            if ($listing->investors->count() === 0) {
                return response()->json([
                    'message' => trans('messages.no_investors_in_listing')
                ], 400);
            }

            $payout = CreatePayout::run($listing, $data['total']);

            return response()->json([
                'id' => $payout->id
            ], 201);
        });
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, ListingPayoutRules::get());
    }

    /**
     * pay for investors
     *
     * @param \Illuminate\Http\Request $request
     * @param App\Models\Payouts\Payout
     * @param \App\Support\VirtualBanking\BankService $wallet
     * @return \Illuminate\Http\JsonResponse
     */
    public function pay(Request $request, $listing, Payout $payout)
    {
        if (!request()->user('sanctum')->can('access-admin-portal')) {
            throw new AuthorizationException;
        }

        $this->canUserAccessListing($payout->listing);

        $amounts = $this->validate($request, ListingPayoutRules::getForPay())['investors'];

        return DB::transaction(function () use ($payout, $amounts) {
            PayForInvestors::run($amounts, $payout);

            return response()->json();
        });
    }
}
