<?php

namespace App\Http\Resources\Aml;

use Illuminate\Http\Resources\Json\JsonResource;

class AmlHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
