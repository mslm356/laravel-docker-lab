<?php

namespace App\Console\Commands\Urway;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Listings\Listing;
use App\Models\Urway\UrwayTransaction;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use Carbon\Carbon;
use Cknow\Money\Money;
use Dflydev\DotAccessData\Data;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UpdateRrnTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'urway:update-rrn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'urway update rrn transactions';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            Transaction::query()
                ->where('reason', 8)
                ->whereNull('rrn')
                ->lazyById()
                ->each(function ($transaction) {

                    if (isset($transaction->meta['details']) && isset($transaction->meta['details']['rrn'])) {
                        $transaction->rrn = $transaction->meta['details']['rrn'];
                        $transaction->save();

                        dump($transaction->id . ' -> done');
                    }
                });


        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
