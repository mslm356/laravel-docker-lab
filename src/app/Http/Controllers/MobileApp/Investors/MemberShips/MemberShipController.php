<?php

namespace App\Http\Controllers\MobileApp\Investors\MemberShips;

use App\Services\LogService;
use App\Http\Controllers\Controller;
use App\Services\MemberShip\CreateRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MemberShipController extends Controller
{
    public $logService;
    public $createRequestService;

    public function __construct()
    {
        $this->createRequestService = new CreateRequest();
        $this->logService = new LogService();
    }

    public function approveMemberShip(Request $request)
    {

        try {
            $user = getAuthUser('api');

            $this->createRequestService->newInvestorDecision($user);

            return apiResponse(200, __('Done successfully'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-APPROVE-MEMBER-SHIP', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('APPROVE-MEMBER-SHIP-EXCEPTION', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }


    public function updateMembershipStatus(Request $request)
    {
        try {

            $this->validate($request, [
                'status' => ['required', 'boolean'],
            ]);

            $user = getAuthUser('api');
            $this->createRequestService->newInvestorDecision($user, $request->status);

            $currentMemberShip = $user->member_ship;

            return apiResponse(200, __('Done successfully'), [ 'membership' =>  $currentMemberShip ]);
        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-APPROVE-MEMBER-SHIP', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('APPROVE-MEMBER-SHIP-EXCEPTION', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

}
