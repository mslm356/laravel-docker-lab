<?php

namespace App\Providers;

use Laravel\Telescope\TelescopeApplicationServiceProvider;
use App\Support\Users\Invitations\InvitationManager;
use App\Support\Users\Invitations\InvitationTokenRepository;
use Illuminate\Support\Str;

class UserInvitationServiceProvider extends TelescopeApplicationServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('invitation', function ($app) {
            $key = $app['config']['app.key'];

            if (Str::startsWith($key, 'base64:')) {
                $key = base64_decode(substr($key, 7));
            }

            $invitationTokenRepositor = new InvitationTokenRepository($key, $app['hash']);

            return new InvitationManager($invitationTokenRepositor);
        });
    }
}
