<?php


namespace App\Support\Elm\AbsherOtp;

use App\Enums\Elm\AbsherOtpReason;
use App\Enums\Role;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\NationalIdentity;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;

class forgetEmailService
{

    public function sendOtp($data)
    {
        $nationalIdentity = NationalIdentity::where('nin', $data['nin'])->first();

        if (!$nationalIdentity) {
            return false;
        }

        $user = $nationalIdentity->user;

        if (!$user || !$user->hasRole(Role::Investor)) {
            return false;
        }

        $msg = VerificationSms::instance()->send(
            $user->nationalIdentity->nin,
            $user,
            [
                'reason' => __('absher_otp_purpose.' . AbsherOtpReason::VerifyLogin, [], $user->locale ?? App::getLocale())
            ]
        );

        return $msg;
    }

    public function verify($data, $type = null)
    {
        $absherOtp = AbsherOtp::where('id', $data['vid'])->first();

        if (!$absherOtp) {
            throw ValidationException::withMessages([
                'vid' => __('messages.invalid_sms_verification_vid'),
            ]);
        }

        $user = $absherOtp->user;

        if ($user === null || !$user->nationalIdentity) {
            throw ValidationException::withMessages([
                'vid' => __('messages.invalid_user'),
            ]);
        }

        try {

            $isVerified = VerificationSms::instance()->verify($user->nationalIdentity->nin, $absherOtp, $data['code']);

            if (!$isVerified) {
                throw ValidationException::withMessages([
                    'code' => __('messages.invalid_sms_verification_code'),
                ]);
            }

            if (isset($data['phone_number'])) {
                $user->phone_number = $data['phone_number'];
            } else {
                $user->email = $data['email'];
            }

            $user->save();

            //            DB::table('absher_otps')->where('id', $data['vid'])->delete();

            return true;

        } catch (VerificationSmsException $th) {
            throw ValidationException::withMessages([
                'vid' => __('messages.invalid_sms_verification_vid'),
            ]);
        }
    }

    public function getNinRules()
    {
        return [
            'nin' => ['required', 'string', 'regex:/(1|2)\d{9}/'],
        ];
    }

    public function getRules()
    {
        return [
            'vid' => ['required', 'exists:absher_otps,id', 'string', 'uuid'],
            'code' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users,email', 'max:255'],
        ];
    }
}
