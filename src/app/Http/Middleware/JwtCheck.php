<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class JwtCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $guard=null)
    {

        $guard = $guard ?? 'api';

        $authHeader = $request->header('authorization');

        $token = explode(' ', $authHeader);

        $token = $authHeader && isset($token[1]) ? $token[1] : null;

        if (auth($guard)->check() && auth($guard)->user()->jwt_token === $token) {
            return $next($request);
        }

        return response()->json(['status'=>401,'message'=>'Unauthenticated.'], 401);

    }
}
