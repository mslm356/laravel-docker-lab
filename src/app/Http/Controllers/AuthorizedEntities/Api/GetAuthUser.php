<?php

namespace App\Http\Controllers\AuthorizedEntities\Api;

use App\Enums\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorizedEntities\AuthResource;
use Illuminate\Auth\Access\AuthorizationException;

class GetAuthUser extends Controller
{
    /**
     * Show Admin Profile.
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');

        if ($user->hasRole(Role::AuthorizedEntityAdmin)) {
            return new AuthResource($user);
        }

        throw new AuthorizationException();
    }
}
