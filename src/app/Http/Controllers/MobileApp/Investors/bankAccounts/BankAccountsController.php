<?php

namespace App\Http\Controllers\MobileApp\Investors\bankAccounts;

use App\Enums\BankAccountStatus;
use App\Enums\Banking\BankCode;
use App\Enums\FileType;
use App\Http\Controllers\Controller;
use App\Http\Resources\Common\BankAccounts\BankAccountListItemResource;
use App\Mail\Investors\newBankAccount;
use App\Models\BankAccount;
use App\Models\File;
use App\Models\Users\User;
use App\Rules\VerifyIban;
use App\Services\Files\CheckFile;
use App\Services\LogService;
use App\Traits\SendMessage;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class BankAccountsController extends Controller
{
    use SendMessage ;

    public $logService;

    public $checkFile;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->checkFile = new CheckFile();
    }

    public function index(Request $request)
    {
        try {
            $user = getAuthUser('api');
            $bankAccounts = $user->bankAccounts()->simplePaginate();

            $bankAccount = BankAccountListItemResource::collection($bankAccounts);
            return apiResponse(200, __('mobile_app/message.created_successfully'), $bankAccount);

        } catch (\Exception $e) {
            $code = $this->logService->log('GET-BANKS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }

    }

    public function store(Request $request)
    {
        try {

            $user = getAuthUser('api');

            if (is_array($request['attachments']) && count($request['attachments']) != 1) {
                return apiResponse(400, __('files count not valid'));
            }

            $inputs = $this->validate($request, [
                'name' => ['required', 'string', 'max:100',textValidation()],
                'iban' => ['required', 'string'/*,'unique:bank_accounts,iban'*/, new VerifyIban],
                'bank' => ['required', 'string', new EnumValue(BankCode::class)],
                'attachments' => ['required'/*,'clamav'*/, 'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/heif,image/heic', 'max:4096']
            ]);

            $this->checkFile->handle($inputs['attachments'], null);

            $pendingBankAccount = BankAccount::query()
                ->where('accountable_id', $user->id)
                ->where('accountable_type', User::class)
                ->whereIn('status', [ BankAccountStatus::InReview, BankAccountStatus::PendingReview])
                ->first();

            if ($pendingBankAccount) {
                throw ValidationException::withMessages(['iban' => __('messages.you_have_pending_bank_account'),]);
            }

            $repeatedIban = BankAccount::whereIn('status', [BankAccountStatus::Approved, BankAccountStatus::InReview, BankAccountStatus::PendingReview])
                ->where('iban', $inputs['iban'])->first();

            if ($repeatedIban) {
                throw ValidationException::withMessages(['iban' => __('messages.repeated_iban'),]);
            }

            if ($user->investor && !$user->investor->is_kyc_completed) {
                return apiResponse(400, __('messages.complete_your_profile'));
            }

            $data = [
                'name' => $inputs['name'],
                'iban' => $inputs['iban'],
                'bank' => $inputs['bank'],
                'status' => BankAccountStatus::PendingReview,
            ];

            $bankAccount = $user->bankAccounts()->create($data);

            if (isset($request->attachments)) {
                File::upload($request->attachments, 'bank-accounts/' . $bankAccount->id, [
                    'fileable_id' => $bankAccount->id,
                    'fileable_type' => get_class($bankAccount),
                    'type' => FileType::BankAccountAttachment
                ]);
            }

            // send mail
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($user)->queue(new newBankAccount($user));
            }

            $this->sendSms($user);


            return apiResponse(200, __('mobile_app/message.created_successfully'), $bankAccount);

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-STORE-NEW-BANK', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);

        } catch (\Exception $e) {
            $code = $this->logService->log('STORE-NEW-BANK', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }

    }

    public function sendSms($user)
    {
        $name = $user ? $user->full_name : '---';
        $content_ar_1 = "  تم تلقي طلبكم لإضافة حسابكم البنكي في منصة أصيل إلى المحفظة الشخصية سوف يتم تبليغكم في حال الرد من قبل الفريق المختص شكراً لتعاملك مع منصة أصيل المالية ";
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar_1;
        $content_en =  "Our dear investor ". $name ." Your request to add your bank account has been received, and the team will notify you once it is reviewed Thank you for dealing with Aseel platform" ;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }

    public function getApprovedBankAccounts(Request $request)
    {
        try {
            $user = getAuthUser('api');

            $accounts = $user->bankAccounts()->where('status', BankAccountStatus::Approved)->get()
                ->map(fn ($item) => [
                    'id' => $item->id,
                    'name' => BankCode::getDescription($item->bank) . ' - ' . $item->iban,
                    'iban' => $item->iban,
                ]);

            return apiResponse(200, __('mobile_app/message.success'), $accounts);
        } catch (\Exception $e) {
            $code = $this->logService->log('GET-APPROVED-BANKS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function getBankNames()
    {

        try {
            $banks = collect(BankCode::asSelectArray())->map(function ($name, $code) {
                return [
                    'code' => $code,
                    'name' => $name
                ];
            })->values()->all();

            return apiResponse(200, __('mobile_app/message.success'), $banks);

        } catch (\Exception $e) {
            $code = $this->logService->log('GET-BANK-NAMES', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }

    }

    public function getBankNamesTest()
    {
        return apiResponse(200, __('mobile_app/message.success'));
    }
}
