<?php

namespace App\Http\Resources\AuthorizedEntities;

use App\Enums\Banking\BankCode;
use App\Enums\BankAccountStatus;
use App\Http\Resources\Admins\FileResource;
use App\Models\AuthorizedEntity;
use Illuminate\Http\Resources\Json\JsonResource;

class InvestorInvestmentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_name' => optional($this->investor)->full_name,
            'nin' => optional($this->investor->nationalIdentity)->nin,
            'invested_shares' => $this->invested_shares,
            'invested_amount' => $this->invested_amount->formatByDeciaml()->getAmount(),
            'invested_amount_fmt' => $this->invested_amount->format(),
            'fund_id' => optional($this->listing)->id,
            'listing_title' => optional($this->listing)->title,
        ];
    }
}
