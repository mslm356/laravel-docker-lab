<?php

namespace App\Enums\AML;

use BenSampo\Enum\Enum;

final class AmlStatus extends Enum
{
    const Passed =   1;
    const Found =   2;
    const NotChecked = 3;
}
