<?php

namespace App\Http\Controllers\AuthorizedEntities\Api;

use App\Enums\Listings\ListingReviewStatus;
use App\Enums\Role;
use App\Http\Resources\Admins\BankAccounts\CampaignsResource;
use App\Http\Resources\AuthorizedEntities\InvestorInvestmentsResource;
use App\Models\Listings\ListingInvestor;
use App\Models\Listings\ListingInvestorRecord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorizedEntities\AuthResource;
use Illuminate\Auth\Access\AuthorizationException;

class InvestorInvestments extends Controller
{

    public function Search(Request $request)
    {
        $user = $request->user('sanctum');

        if ($user->hasRole(Role::AuthorizedEntityAdmin)) {
            $data = ListingInvestor::whereHas("listing", function ($query) use ($request) {
                $query->where('authorized_entity_id', authorizedEntityId())
                    ->where('review_status', ListingReviewStatus::Approved);
            })->whereHas("investor", function ($q) use ($request) {
                $q->whereHas('nationalIdentity',function ($q1) use ($request){
                    $q1->where('nin',$request->nin);
                });

            })->simplePaginate();

            return InvestorInvestmentsResource::collection($data);
        }

        throw new AuthorizationException();
    }
}
