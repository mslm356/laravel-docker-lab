<?php

namespace App\Mail\Investors;

use App\Models\BankAccount;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class rejectBankAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $bankAccount;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, BankAccount $bankAccount)
    {
        $this->user = $user;
        $this->bankAccount = $bankAccount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';
        $profileUrl = config('app.investor_url') . '/app/overview';
        $reason = $this->bankAccount ? $this->bankAccount->reviewer_comment : '---';

        return $this->view('emails.investors.reject_bank_account', compact('profileUrl', 'name', 'reason'))
            ->subject(__('emails/investors/reject_bank_account.subject'));
    }
}
