<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentityAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identity_addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('identity_id');
            $table->json('post_code')->nullable();
            $table->json('additional_number')->nullable();
            $table->json('building_number');
            $table->json('city')->nullable();
            $table->json('district')->nullable();
            $table->json('location')->nullable();
            $table->json('street_name')->nullable();
            $table->json('unit_number')->nullable();
            $table->timestamps();

            $table->foreign('identity_id')->references('id')->on('national_identities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identity_addresses');
    }
}
