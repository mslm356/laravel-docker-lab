<?php

namespace App\Console\Commands\Anb;

use App\Jobs\Anb\AnbEodStatement;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Http\HttpException;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class CancelPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:cancel-payment {--external_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cancel PAYMENT ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $response = AnbApiUtil::cancel($this->option('external_id'));
            dd($response);

        } catch (HttpException $e) {
            dd($e->getResponse()->json());
        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }

    }
}
