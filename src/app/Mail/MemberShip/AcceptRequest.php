<?php

namespace App\Mail\MemberShip;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AcceptRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $membership;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $membership)
    {
        $this->user = $user;
        $this->membership = $membership;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';
        $membership = $this->membership;

        $profileUrl = config('app.investor_url') . '/app/overview';

        return $this->view('emails.member_ship.accept', compact('name', 'profileUrl', 'membership'))
            ->subject(__('emails/membership/accept.title', ['membership'=> $membership]));
    }
}
