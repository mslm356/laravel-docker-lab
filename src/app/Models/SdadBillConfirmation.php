<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SdadBillConfirmation extends Model
{
    use HasFactory;

    protected $fillable = [
        'edaat_invoice_id',
        'body',
    ];
    protected $casts = [
        'body' => 'object',
        'created_at' => 'datetime',
    ];
    public function invoice()
    {
        return $this->belongsTo(EdaatInvoice::class,'edaat_invoice_id','id');
    }
}
