<?php

namespace App\Http\Controllers\Admins\Api\Investors;

use App\Actions\Aml\InvestorUpdateStatus;
use App\Actions\ExportSubscriptionForm;
use App\Actions\Investors\RenderSubscriptionForm;
use App\Enums\Banking\WalletType;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Investors\ProfileReviewType;
use App\Enums\Investors\Registration\Education;
use App\Enums\Investors\Registration\MoneyRange;
use App\Enums\Role;
use App\Jobs\Investor\DeleteKycPdfJob;
use App\Mail\Investors\ApproveProfile;
use App\Models\Investors\InvestorProfile;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Rules\NumericString;
use App\Services\Investors\UpgradeProfileServices;
use App\Services\LogService;
use App\Support\Elm\Rabet\RabetService;
use App\Support\PdfGenerator\PdfGenerator;
use App\Support\Users\FilterServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Investors\InvestorListItemResource;
use App\Http\Resources\Admins\Investors\InvestorResource;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use function Doctrine\Common\Cache\Psr6\get;

class InvestorController extends Controller
{
    public $filterService;
    public $logService;
    public $upgradeProfileServices;


    public function __construct()
    {
        $this->filterService = new FilterServices();
        $this->logService = new LogService();
        $this->upgradeProfileServices = new UpgradeProfileServices();
    }

    /**
     * Get Investors list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {

            $user = Auth::guard('sanctum')->user();

            if ($user && $user->authorized_entity_id == 5) {
                return response()->json(['message' => __('service not available now')], 422);
            }

            $investors = $this->filterService->filter($request);

            $investors = $investors->paginate();

            return InvestorListItemResource::collection($investors);

        } catch (\Exception $e) {
            $this->logService->log('ADMIN-INVESTORS', $e);
            return response()->json(['message' => 'sorry please try later'], 400);
        }
    }

    /**
     * Get Investors profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($user)
    {

        $auth = Auth::guard('sanctum')->user();

        if ($auth && $auth->authorized_entity_id == 5) {
            return response()->json(['message' => __('service not available now')], 422);
        }

        $user = User::find($user);
        if (!$user)
            return response()->json(['message' => __('messages.resource_cannot_be_found'),], 400);

        if (!$user->hasRole(Role::Investor)) {
            return response()->json([
                'message' => __('messages.resource_cannot_be_found'),
            ], 400);
        }
        return new InvestorResource($user);
    }

    /**
     * Update investor status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, User $user)
    {

        $data = $this->validate($request, [
            'status' => ['required', 'integer', new EnumValue(InvestorStatus::class, false)],
            'comment' => ['nullable', 'string', 'max:1000',textValidation()]
        ]);


        return DB::transaction(function () use ($data, $user, $request) {
            $profile = $user->investor()->lockForUpdate()->firstOrFail();

            if ($profile->is_kyc_completed === false) {
                return response()->json([
                    'message' => __('messages.investor_status_cannot_be_updated_if_kyc_not_completed')
                ], 400);
            }

            $prepared_data = [
                'status' => $data['status'],
                'reviewer_comment' => $data['comment'],
                'reviewer_id' => $request->user('sanctum')->id,
                'review_type' => ProfileReviewType::Manual,
            ];
            InvestorUpdateStatus::run($user, $prepared_data, $request->user('sanctum')->id);
            return response()->json();

        });
    }

    /**
     * Update investor profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $data = $this->validate($request, [
            'first_name' => ['required', 'string', 'max:100',textValidation()],
            'last_name' => ['required', 'string', 'max:100',textValidation()],
            'phone_number' => 'nullable',
            'id_expiry_date' => ['required', 'string', 'date_format:Y-m-d'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user->id],
        ]);

        if ($request->has("phone_number") && $request->phone_number != null) {
            $data['phone_number'] = phone($data['phone_number'], "SA");

            $repeatedPhone = User::where('phone_number', $data['phone_number'])->where('id', '!=', $user->id)->first();
            if ($repeatedPhone)
                return apiResponse(400, __('messages.repeated_phone'));

        }


        $user->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'phone_number' => $request->has("phone_number") && $request->phone_number != null ? $data['phone_number'] : $user->phone_number,
        ]);

        $user->nationalIdentity->update(['id_expiry_date' => $data['id_expiry_date']]);

        return response()->json();
    }

    function checkPhoneWithRabet(Request $request, User $user)
    {

//        substr($str, 1);

        $this->validate($request, [
            'user' => ['required', 'integer', 'exists:users,id'],
            'phone_number' => ['required'],
        ]);

        $rabet = new RabetService();

        if (!$user->nationalIdentity)
            return apiResponse(400, __('messages.nationalIdentity_valid'));

        $phoneNumber = $request->phone_number;

        $phoneNumber = Str::startsWith($phoneNumber, '+') ? substr($phoneNumber, 1) : $phoneNumber;

        $responce = $rabet->checkRabetPhone($user->nationalIdentity->nin, $phoneNumber);

        if (is_bool($responce))
            return apiResponse(200, 'done', ['status' => $responce]);

        return apiResponse(400, $responce);


    }

    public function deleteAccount(Request $request)
    {
        try {
            $request->validate(["investor_id" => "required|exists:users,id"]);

            $user = User::find($request->investor_id);

            if (!$user)
                return response()->json(['message' => __('messages.user_not_fount')], 400);

            if (!$user->hasRole(Role::Investor))
                return response()->json(['message' => __('mobile_app/message.not_permission')], 400);

            /*if (!$user->hasRole(Role::Investor) || !$user->nationalIdentity) {
                return response()->json(['message' => __('mobile_app/message.not_permission')], 400);
            }*/

            if ($user->investor && $user->investor->status == 8) {
                return response()->json(['message' => __('messages.deleted_account')], 400);
            }


            $userWallet = $user->getWallet(WalletType::Investor);

            if ( $userWallet && $userWallet->balance != 0 )
                return response()->json(['message' => __('messages.wallet_has_balance')], 400);

            $hasInvestments = $user->investingRecords;
            if ($hasInvestments->count() > 0)
                return response()->json(['message' => __('messages.has_investments')], 400);

            $oldPhoneNumber = optional($user->phone_number)->getRawNumber();

            $this->upgradeProfileServices->updatePhone($oldPhoneNumber, $user);

            if ($user->nationalIdentity)
                $this->upgradeProfileServices->updateNationality(optional($user->nationalIdentity)->nin, $user);

            $this->upgradeProfileServices->updateEmail($user->email, $user);

            if ($user->investor) {
                $this->upgradeProfileServices->updateStatus($user);
                $this->upgradeProfileServices->updateReviewerId($user, auth()->user());
            }


            $user->tokens()->delete();

            return apiResponse(200, __('mobile_app/message.success'));
        } catch (ValidationException $e) {
            return response()->json(['message' => $e->getMessage()], 400);

        } catch (\Exception $e) {
            $code = $this->logService->log('DELETE-USER-ACCOUNT', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);

        }
    }

    public function downloadKycPdf($investor_id)
    {

        try {

            $profile = InvestorProfile::query()
                ->where('user_id', $investor_id)
                ->where('is_kyc_completed', 1)
                ->first();

            if (!$profile) {
                throw ValidationException::withMessages(['message' => 'profile not valid']);
            }

            $todayDate = now('Asia/Riyadh')->format('Y-m-d');

            $html = $this->html($profile)->render();

            $originalFileName = "{$todayDate}-{$profile->user_id}-kyc.pdf";

            $targetPath = "kyc-pdf/$originalFileName";

            PdfGenerator::outputFromHtml($html, $targetPath, [
                'gotoOptions' => [
                    'waitUntil' => 'networkidle0'
                ]
            ]);

            $deleteKycFileJob = (new DeleteKycPdfJob($targetPath))->delay(\Carbon\Carbon::now()->addMinutes(5));
            dispatch($deleteKycFileJob);

            $driver = config('filesystems.private');

            return Storage::disk($driver)->download($targetPath,
                $originalFileName,
                ['Content-Disposition' => "attachment;filename={$originalFileName}"]
            );
        } catch (ValidationException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            $this->logService->log('download-pdf', $e);
            return response()->json(['message' => __('messages.user_not_fount')], 400);
        }
    }

    public function mapEnumValues($class)
    {
        try {
            $alert_messages = $class::getAlertMessages();
        } catch (\Exception $exception) {
            $alert_messages = [];
        }


        return array_values(
            array_map(
                function ($value) use ($class, $alert_messages) {
                    return [
                        'name' => $class::getDescription($value),
                        'key' => $value,
                        'alert' => isset($alert_messages[$value]) ? $alert_messages[$value] : null
                    ];
                },
                $class::asArray()
            )
        );
    }

    public function html($profile)
    {
        return view('investors.kyc-pdf', compact('profile'));
    }

}
