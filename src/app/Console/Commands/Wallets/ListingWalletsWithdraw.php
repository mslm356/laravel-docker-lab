<?php

namespace App\Console\Commands\Wallets;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Models\VirtualBanking\Wallet;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ListingWalletsWithdraw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listing:wallet-withdraw {--wallet_id=} {--amount=} {--do_action=} {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listing Wallets Withdraw';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $walletId = $this->option('wallet_id') ?? dd('wallet id required');
            $amount = $this->option('amount') && is_numeric($this->option('amount')) ? ($this->option('amount') * 100) : null;

            $wallet = $this->getWallet($walletId);

            $amount = $amount ?? $wallet->balance;

            $this->checkWalletAmount($wallet, $amount);

            if ($this->option('do_action') && $this->option('do_action') == 'y') {
                $this->makeTransaction($wallet, $amount);
                $this->updateWalletBalance($wallet);
                dump('action done success');
            }

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }

    public function getWallet($walletId)
    {

        $listingTypes = [
//            WalletType::getValue('ListingSpv'),
            WalletType::getValue('ListingEscrow'),
            WalletType::getValue('ListingEscrowVat'),
            WalletType::getValue('ListingEscrowAdminFee')
        ];

        $wallet = DB::table('wallets')
            ->where('id', $walletId)
            ->where('holder_type', Listing::class)
            ->whereIn('slug', $listingTypes)
            ->select('id', 'holder_id', 'balance', 'slug')
            ->first();

        if (!$wallet) {
            dd('wallet not valid');
        }

        return $wallet;
    }

    public function checkWalletAmount($wallet, $amount)
    {

        $amountColumns = [
            WalletType::getValue('ListingEscrow') => 'amount_without_fees',
            WalletType::getValue('ListingEscrowVat') => 'vat_amount',
            WalletType::getValue('ListingEscrowAdminFee') => 'admin_fees'
        ];

        $sumColumn = $amountColumns[$wallet->slug];

        $listingInvestorRecordAmount = DB::table('listing_investor_records')
            ->where('listing_id', $wallet->holder_id)
            ->sum($sumColumn);

        $sumWalletTransactions = DB::table('transactions')
            ->where('wallet_id', $wallet->id)
            ->where('reason', '!=', TransactionReason::AnbExternalTransfer)
            ->where('payable_type', Listing::class)
            ->sum('amount');

        dump('LISTING INVESTOR RECORD => ' . $listingInvestorRecordAmount . ' SUM OF ' . $sumColumn);
        dump('WALLET TRANSACTIONS WITHOUT WITHDRAW  => ' . $sumWalletTransactions);
        dump('WALLET BALANCE => ' . $wallet->balance);

        if ($listingInvestorRecordAmount != $sumWalletTransactions) {
            dd('please check balance and investor record amount not equal');
        }

        if ($amount <= 0) {
            dd('sorry, amount not valid => ' . $amount);
        }

        if ($amount > $wallet->balance) {
            dd('sorry, amount greater than wallet balance');
        }
    }

    public function makeTransaction($wallet, $amount)
    {

        $amount = ($amount * -1);

        $transaction = DB::table('transactions')
            ->where('wallet_id', $wallet->id)
            ->where('reason', TransactionReason::AnbExternalTransfer)
            ->select('id', 'amount')
            ->first();

        if ($transaction) {

            DB::table('transactions')
                ->where('id', $transaction->id)
                ->where('wallet_id', $wallet->id)
                ->where('reason', TransactionReason::AnbExternalTransfer)
                ->update(['amount'=> ($transaction->amount + $amount), 'updated_at' => now()]);

            return true;
        }

        $meta = [
            'reason' => TransactionReason::AnbExternalTransfer,
            'reference' => transaction_ref(),
            'value_date' => now('UTC')->toDateTimeString(),
            'source' => 'cmd-listing-wallet-',
        ];

        DB::table('transactions')->insert([
            'payable_type' => Listing::class,
            'payable_id' => $wallet->holder_id,
            'wallet_id' => $wallet->id,
            'type' => 'withdraw',
            'amount' => $amount,
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => $this->option('date') ?? now(),
            'updated_at' => $this->option('date') ?? now(),
        ]);

        return true;
    }

    public function updateWalletBalance($wallet)
    {
        $walletBalance = DB::table('transactions')
            ->where('wallet_id', $wallet->id)
            ->where('payable_type', Listing::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('id', $wallet->id)
            ->update(['balance' => $walletBalance, 'is_overview_statistics_consistent' => false]);
    }
}
