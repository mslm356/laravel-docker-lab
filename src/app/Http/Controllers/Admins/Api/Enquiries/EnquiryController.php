<?php

namespace App\Http\Controllers\Admins\Api\Enquiries;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\EnquiryResource;
use App\Models\Enquiries\Enquiry;

class EnquiryController extends Controller
{
    /**
     * Get the paginated enquiries
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return EnquiryResource::collection(Enquiry::with(['user', 'reason'])
            ->latest()
            ->paginate());
    }

    /**
     * Get the enquiry with the replies
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Enquiry $enquiry)
    {
        $enquiry->loadMissing('replies');

        $enquiry->setRelation('replies', $enquiry->replies->map(function ($e) use ($enquiry) {
            return $e->setRelation('enquiry', $enquiry);
        }));

        return new EnquiryResource($enquiry);
    }
}
