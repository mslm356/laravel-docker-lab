<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Actions\Listings\Valuations\UpdateOrCreateValuation;
use App\Common\ValidationRules\Listings\ListingValuationRules;
use App\Http\Resources\Admins\Listings\ListingValuationResource;
use App\Http\Resources\Admins\Listings\ValuationListItemResource;
use App\Models\Listings\ListingValuation;
use Illuminate\Validation\ValidationException;

class ListingValuationController extends Controller
{
    /**
     * Get all the valuations of listing
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listing $listing)
    {
        $valuations = $listing->valuations()
            ->latest()
            ->paginate();

        return ValuationListItemResource::collection($valuations);
    }

    /**
     * Store the valuation in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($listing, $id)
    {
        $valuation = ListingValuation::find($id);

        if (!$valuation)
            throw ValidationException::withMessages(['listing' => __('messages.listing_valuation_not_found'),]);

        return new ListingValuationResource($valuation);
    }

    /**
     * Store the valuation in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id)
    {
        $data = $this->validate($request, ListingValuationRules::get());

        $listing = Listing::find($id);

        if (!$listing)
            throw ValidationException::withMessages(['listing' => __('messages.listing_not_found'),]);

        DB::transaction(function () use ($data, $listing) {
            UpdateOrCreateValuation::run($data, $listing);
        });

        return response()->json([], 201);
    }

    /**
     * Update the valuation in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Listings\ListingReport $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $listing, $id)
    {

        $data = $this->validate($request, ListingValuationRules::get());

        $listing = Listing::find($listing);

        if(!$listing)
            throw ValidationException::withMessages(['listing' => __('messages.listing_not_found'),]);

        $valuation = ListingValuation::find($id);

        if (!$valuation)
            throw ValidationException::withMessages(['listing' => __('messages.listing_valuation_not_found'),]);

        DB::transaction(function () use ($data, $valuation, $listing) {
            UpdateOrCreateValuation::run($data, $listing, $valuation);
        });

        return response()->json();
    }

    /**
     * Delete valuation from the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Listings\ListingReport $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, ListingValuation $valuation)
    {
        $valuation->delete();

        return response()->json();
    }
}

