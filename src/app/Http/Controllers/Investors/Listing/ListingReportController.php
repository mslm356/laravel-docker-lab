<?php

namespace App\Http\Controllers\Investors\Listing;

use App\Enums\Listings\AttachmentType;
use App\Enums\Listings\ReportEnum;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Listings\ListingReportResource;
use App\Models\File;
use App\Models\Listings\Listing;
use App\Services\Investors\ListingReportServices;

class ListingReportController extends Controller
{

    public $listingReportServices;

    public function __construct()
    {
        $this->listingReportServices = new ListingReportServices();
    }

    /**
     * Listings list
     *
     * @param
     * @return array
     */
    public function index(Listing $listing)
    {
        $this->authorize('accessListingWhichInvestedIn', $listing);

        return ListingReportResource::collection(
            $listing->reports()->where('status', ReportEnum::Approved)->with('files')->latest()->simplePaginate()
        );
    }

    /**
     * Download file
     *
     * @param \App\Models\Listings\Listing $listing
     * @param \App\Models\File $file
     * @return \Illuminate\Http\File
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function downloadDueDiligence(Listing $listing, File $file)
    {
        if (
            $listing->is_visible &&
            $file->fileable_type === Listing::class &&
            $file->fileable_id === $listing->id &&
            $file->extra_info->get('type') === AttachmentType::DueDiligence
        ) {
            return $file->download();
        }

        abort(404);
    }
}
