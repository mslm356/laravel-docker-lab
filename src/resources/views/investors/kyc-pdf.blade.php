<!DOCTYPE html>
<html dir="rtl" lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>KYC</title>
    <style>
        @include (
          "components.print-components.watermark",
          [ "website" => "https://investaseel.sa",
          "user_name" => $profile->user->full_name,
          "date" =>\Carbon\Carbon::now()->format("Y-m-d H:i:s") ]
        )
        body {
            font-family: "HelveticaNeueLT" !important;
        }

        html {
            height: 100%;
        }
    </style>

    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
<div class="flex flex-col gap-12 p-3 max-w-4xl m-auto">
    <div class="text-center mb-5 pt-0">
        <img
            class="w-[150px] m-auto mb-5"
            src="{{ asset('images/aseel-blue.png') }}"
            width="120"
        />
        <span class="block" dir="rtl" style="font-weight: bold"
        >نموذج اعرف عمليك</span
        >
        <span class="block" dir="ltr" style="font-weight: bold"
        >Know Your Client</span
        >
    </div>

    <div class="flex flex-col gap-5">
        <div class="flex flex-1 justify-between items-center">
            <h6 style="font-weight: bold" dir="rtl">معلومات عامة</h6>
            <h6 style="font-weight: bold" dir="ltr">General information</h6>
        </div>

        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">: الاسم </h6>
                <h6 class="text-sm" dir="ltr"> Name : </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{ optional($profile->nationalIdentityForKYC)->YaqeenFullName}}</span>
                    <h6 dir="ltr">{{ optional($profile->nationalIdentityForKYC)->YaqeenFullName}}</h6>
                </div>
            </div>
        </div>

        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">: الهويه </h6>
                <h6 class="text-sm" dir="ltr"> ID : </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{ optional($profile->nationalIdentityForKYC)->nin}}</span>
                    <h6 dir="ltr">{{ optional($profile->nationalIdentityForKYC)->nin}}</h6>
                </div>
            </div>
        </div>

        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">1: هل أنت فرد أم شركة؟</h6>
                <h6 class="text-sm" dir="ltr">1: Are you individual or company?</h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{ $profile->data['is_individual'] == 2 ? __('company', [], 'ar') : __('individual', [], 'ar')}}</span>
                    <h6 dir="ltr">{{ $profile->data['is_individual']  == 2 ? __('company', [], 'en') : __('individual', [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    2: هل سبق لك العمل في القطاع المالي خلال السنوات الخمس السابقة؟
                </h6>
                <h6 class="text-sm" dir="ltr">
                    2: Have you worked in the financial sector during the past five
                    years?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['any_xp_in_fin_sector'] ? __('messages.true', [], 'ar') : __('messages.false', [], 'ar')}}</span>
                    <h6 dir="ltr">{{$profile->data['any_xp_in_fin_sector'] ? __('messages.true', [], 'en') : __('messages.false', [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    3: هل لك أي خبرات عملية أخرى ذات صلة بالقطاع المالي؟
                </h6>
                <h6 class="text-sm" dir="ltr">
                    3: Do you have any other practical experience related to the
                    financial sector?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['any_xp_related_to_fin_sector'] ? __('messages.true', [], 'ar') : __('messages.false', [], 'ar')}}</span>
                    <h6 dir="ltr">{{$profile->data['any_xp_related_to_fin_sector'] ? __('messages.true', [], 'en') : __('messages.false', [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">4: هل سبق لك الاستثمار في العقار؟</h6>
                <h6 class="text-sm" dir="ltr">
                    4: Have you ever invested in real estate?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['have_you_invested_in_real_estates'] ? __('messages.true', [], 'ar') : __('messages.false', [], 'ar')}}</span>
                    <h6 dir="ltr">{{$profile->data['have_you_invested_in_real_estates'] ? __('messages.true', [], 'en') : __('messages.false', [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    5: هل أنت عضو مجلس إدارة أو لجنة مراجعة أو أحد كبار التنفيذيين في
                    شركة مدرجة؟
                </h6>
                <h6 class="text-sm" dir="ltr">
                    5: Are you a board of directors member, an audit committee member
                    or a senior executive in a listed company?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">

                    <span dir="rtl">{{$profile->is_on_board ? __('messages.true', [], 'ar') : __('messages.false', [], 'ar')}}</span>
                    <h6 dir="ltr">{{$profile->is_on_board ? __('messages.true', [], 'en') : __('messages.false', [], 'en')}}</h6>

                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">6: المعرفة والخبرات الاستثمارية</h6>
                <h6 class="text-sm" dir="ltr">
                    6: Knowledge and investment experience
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{__('enums.' . \App\Enums\Investors\Registration\Level::class . '.' .  $profile->data['investment_xp'] , [], 'ar')}}</span>
                    <h6 dir="ltr">{{__('enums.' . \App\Enums\Investors\Registration\Level::class . '.' .  $profile->data['investment_xp']  , [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    7: هل أنت مكلّف بمهمات عليا في المملكة أو في دولة أجنبية أو مناصب
                    إدارة عليا أو وظيفة في إحدى المنظمات الدولية؟
                </h6>
                <h6 class="text-sm" dir="ltr">
                    7: Is the client entrusted with prominent public functions in the
                    Kingdom or a foreign country, senior management positions, or a
                    position in an international organization?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['is_entrusted_in_public_functions'] ? __('messages.true', [], 'ar') : __('messages.false', [], 'ar')}}</span>
                    <h6 dir="ltr">{{$profile->data['is_entrusted_in_public_functions'] ? __('messages.true', [], 'en') : __('messages.false', [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3 mt-16">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    8: هل لديك صلة قرابة برابطة الدم أو الزواج وصولاً إلى الدرجة
                    الثانية،أو مقرّباً من شخص مكلّف بمهمات عليا في المملكة أو في دولة
                    أجنبية أو مناصب إدارة عليا أو وظيفة في إحدى المنظمات الدولية؟
                </h6>
                <h6 class="text-sm" dir="ltr">
                    8: Does the client have a relationship (by blood or marriage up to
                    the second degree), or have an associations with a person
                    entrusted with a prominent pblic function in the Kingdom or a
                    foreign country, senior management positions, or a position in an
                    international organization?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['have_relationship_with_person_in_public_functions'] ? __('messages.true', [], 'ar') : __('messages.false', [], 'ar')}}</span>
                    <h6 dir="ltr">{{$profile->data['have_relationship_with_person_in_public_functions'] ? __('messages.true', [], 'en') : __('messages.false', [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    9: هل أنت المستفيد الحقيقي من الحساب أو نائبا عن مستفيد آخر؟
                </h6>
                <h6 class="text-sm" dir="ltr">
                    9: Is the client the beneficial owner of the account or business
                    relationship?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['is_beneficial_owner'] ? __('messages.true', [], 'ar') : __('messages.false', [], 'ar')}}</span>
                    <h6 dir="ltr">{{$profile->data['is_beneficial_owner'] ? __('messages.true', [], 'en') : __('messages.false', [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">10: مستوى التعليم</h6>
                <h6 class="text-sm" dir="ltr">10: Education Level</h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{__('enums.' . \App\Enums\Investors\Registration\Education::class . '.' .  $profile->education_level , [], 'ar')}}</span>
                    <h6 dir="ltr">{{__('enums.' . \App\Enums\Investors\Registration\Education::class . '.' .  $profile->education_level , [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">11: المهنة الحالية</h6>
                <h6 class="text-sm" dir="ltr">11: Current Occupation</h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">

                    <span dir="rtl">{{__('enums.' . \App\Enums\Investors\Registration\Occupation::class . '.' .  $profile->occupation , [], 'ar')}}</span>
                    <h6 dir="ltr">{{__('enums.' . \App\Enums\Investors\Registration\Occupation::class . '.' .  $profile->occupation , [], 'en')}}</h6>
                </div>
            </div>
        </div>
    </div>

    <div class="flex flex-col gap-5">
        <div class="flex flex-1 justify-between items-center">
            <h6 style="font-weight: bold" dir="rtl">الحالة المادية</h6>
            <h6 style="font-weight: bold" dir="ltr">Financial status</h6>
        </div>

        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">1: صافي الثروة التقريبي بالريال</h6>
                <h6 class="text-sm" dir="ltr">
                    1: Approximate net worth (in SAR)?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">

                    <span dir="rtl">{{__('enums.' . \App\Enums\Investors\Registration\NetWorth::class . '.' .  $profile->net_worth , [], 'ar')}}</span>
                    <h6 dir="ltr">{{__('enums.' . \App\Enums\Investors\Registration\NetWorth::class . '.' .  $profile->net_worth , [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">2: ما الذي تستثمر فيه حاليًا؟</h6>
                <h6 class="text-sm" dir="ltr">
                    2: What are you currently invested in?
                </h6>
            </div>

            <div class=" flex flex-col rounded relative gap-2">

                <div class="h-[50px] flex flex-1 gap-10   items-center p-2 bg-[#f6f6f6] rounded">

                    @foreach($profile->current_invest  as $current_invest_ar)
                         <span dir="rtl"> - {{__('enums.' . \App\Enums\Investors\Registration\CurrentInvestment::class . '.' .  $current_invest_ar , [], 'ar')}}</span>
                    @endforeach
                </div>

                <div dir="ltr" class="h-[50px] flex flex-1 gap-10   dir-ltr  items-center p-2 bg-[#f6f6f6] rounded">
                    @foreach($profile->current_invest  as $current_invest_en)
                         <h6 dir="ltr"> - {{__('enums.' . \App\Enums\Investors\Registration\CurrentInvestment::class . '.' . $current_invest_en , [], 'en')}}</h6>
                    @endforeach
                </div>
            </div>

        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">3: الأهداف الاستثمارية العامة</h6>
                <h6 class="text-sm" dir="ltr">
                    3: What is your investment objective?
                </h6>
            </div>

            <div class=" flex flex-col rounded relative gap-2">

                <div class="h-[50px] flex flex-1 gap-10 items-center p-2 bg-[#f6f6f6] rounded">

                    @foreach($profile->invest_objective  as $invest_objective_ar)
                        <span dir="rtl"> - {{__('enums.' . \App\Enums\Investors\Registration\InvestmentObjective::class . '.' .  $invest_objective_ar , [], 'ar')}}</span>
                    @endforeach

                </div>

                <div dir="ltr" class=" dir-ltr h-[50px] flex flex-1 gap-10  items-center p-2 bg-[#f6f6f6] rounded">

                    @foreach($profile->invest_objective  as $invest_objective_en)
                        <span dir="ltr"> - {{__('enums.' . \App\Enums\Investors\Registration\InvestmentObjective::class . '.' .  $invest_objective_en , [], 'en')}}</span>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    4: عدد سنوات الاستثمار في أسواق الأوراق المالية
                </h6>
                <h6 class="text-sm" dir="ltr">
                    4: Number of years of investment in securities
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['years_of_inv_in_securities']}}</span>
                    <h6 dir="ltr">{{$profile->data['years_of_inv_in_securities']}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3 mt-10">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">5: الدخل السنوي التقريبي بالريال</h6>
                <h6 class="text-sm" dir="ltr">
                    5: Approximate annual income (in SAR)?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{__('enums.' . \App\Enums\Investors\Registration\MoneyRange::class . '.' .  $profile->annual_income , [], 'ar')}}</span>
                    <h6 dir="ltr">{{__('enums.' . \App\Enums\Investors\Registration\MoneyRange::class . '.' .  $profile->annual_income , [], 'en')}}</h6>
                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3 ">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    6: المبلغ المتوقع استثماره لكل فرصة
                </h6>
                <h6 class="text-sm" dir="ltr">
                    6: The amount expected to be invested for each opportuniy
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">

                    <span dir="rtl">{{__('enums.' . \App\Enums\Investors\Registration\MoneyRange::class . '.' .  $profile->expected_invest_amount_per_opportunity , [], 'ar')}}</span>
                    <h6 dir="ltr">{{__('enums.' . \App\Enums\Investors\Registration\MoneyRange::class . '.' .  $profile->expected_invest_amount_per_opportunity , [], 'en')}}</h6>

                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">7: المبلغ المتوقع استثماره سنويًا</h6>
                <h6 class="text-sm" dir="ltr">
                    7: The amount expected to be invested annualy
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">

                    <span dir="rtl">{{__('enums.' . \App\Enums\Investors\Registration\MoneyRange::class . '.' .  $profile->expected_annual_invest , [], 'ar')}}</span>
                    <h6 dir="ltr">{{__('enums.' . \App\Enums\Investors\Registration\MoneyRange::class . '.' .  $profile->expected_annual_invest , [], 'en')}}</h6>

                </div>
            </div>
        </div>
        <div class="flex flex-col grow gap-3">
            <div class="flex flex-1 justify-between items-center">
                <h6 class="text-sm" dir="rtl">
                    8: أي معلومات مالية أخرى عن الوضع المالي للعميل؟
                </h6>
                <h6 class="text-sm" dir="ltr">
                    8: Any other financial information on the client's financial
                    situation?
                </h6>
            </div>
            <div class="h-[50px] flex rounded relative p-2 bg-[#f6f6f6]">
                <div class="flex flex-1 justify-between items-center">
                    <span dir="rtl">{{$profile->data['extra_financial_information']}}</span>
                    <h6 dir="ltr">{{$profile->data['extra_financial_information']}}</h6>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
