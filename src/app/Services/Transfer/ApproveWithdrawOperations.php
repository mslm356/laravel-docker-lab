<?php


namespace App\Services\Transfer;

use App\Enums\Role;
use App\Enums\Transfer\TransferStatus;
use App\Models\Transfer\TransferRequest;
use App\Models\Users\User;
use App\Services\Investors\TransferServices;
use App\Services\LogService;

class ApproveWithdrawOperations
{

    public $transferServices;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->transferServices = new TransferServices();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function accept(TransferRequest $transferRequest)
    {

        $auth = User::role(Role::Admin)
            ->where('email', "abdelaty@investaseel.sa")
            ->first();

        if (!$auth) {
            $this->logService->log('TRANSFER-ACCEPT-This User Not Found', null, ['auth not found']);
            return false;
        }

        if (!$auth->can('transfer_accept')) {
            $this->logService->log('TRANSFER-ACCEPT-not of your permissions', null, ['not have access']);
            return false;
        }

        $client = $transferRequest->user;

        $transferData = [
            'bank_account_id' => $transferRequest->bank_account_id,
            'fee' => $transferRequest->fee,
            'amount' => \money($transferRequest->amount)->formatByDecimal(),
        ];

        $transferServiceResponse = $this->transferServices->transferMoney($transferData, $client, 'admin', $transferRequest->id);

        if (isset($transferServiceResponse['status']) && $transferServiceResponse['status'] && isset($transferServiceResponse['transfer_id'])) {

            $transferRequest->update([
                'status' => TransferStatus::Approved,
                'reviewer_comment' => "accept all withdraw operations",
                'reviewer_id' => $auth->id,
                'anb_transfer_id' => $transferServiceResponse['transfer_id'] ?? null,
            ]);

        } else {
            $this->logService->log('Request update failed :', null, ['failed', $transferRequest->id]);
        }

        return true;
    }
}
