<?php

namespace App\Actions;

use App\Enums\EnquiryStatus;
use Illuminate\Http\Request;
use App\Models\Enquiries\Enquiry;
use Illuminate\Support\Facades\Mail;
use App\Mail\Enquiries\EnquiryReceived;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateNewEnquiry
{
    use AsAction;

    /**
     * Create new enquiry
     *
     * @param \App\Models\Users\User $sender
     * @param array $data
     *
     * @return \App\Models\Enquiries\Enquiry
     */
    public function handle($sender, $data)
    {
        $data = $this->validator($data, (bool)($sender))->validate();

        $enquiry = Enquiry::create([
            'user_id' => $sender->id ?? null,
            'email' => $sender ? null : $data['email'],
            'name' => $sender ? null : $data['name'],
            'phone_country_iso2' => $sender ? null : $data['phone_country_iso2'],
            'phone_number' => $sender ? null : $data['phone_number'],
            'subject' => $data['subject'],
            'message' => $data['message'],
            'reason_id' => $data['reason_id'],
            'status' => EnquiryStatus::Waiting
        ]);

        if (config('mail.MAIL_STATUS') == 'ACTIVE') {
            Mail::to($enquiry->email)->queue(new EnquiryReceived($enquiry));
//            Mail::to($enquiry->email)->send(new EnquiryReceived($enquiry));
        }

        return $enquiry;
    }

    public function asController(Request $request)
    {
        $this->handle($request->user('sanctum'), $request->input());

        return response()->json([], 201);
    }

    /**
     * Get the validator
     *
     * @param array $data
     * @param bool $isAuth
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data, $isAuth)
    {
        $conditionalRules = $this->resloveConditionalRules($isAuth);

        return Validator::make($data, array_merge([
            'subject' => ['required', 'string', 'max:150',textValidation()],
            'message' => ['required', 'string', 'max:10000', textValidation() ],
            'reason_id' => ['required', 'integer', 'exists:contact_reasons,id'],
        ], $conditionalRules));
    }

    /**
     * Resolve the conditional rules
     *
     * @param bool $isAuth
     * @return array
     */
    public function resloveConditionalRules($isAuth)
    {
        if ($isAuth) {
            return [];
        } else {
            return [
                'name' => ['required', 'string', 'max:150'],
                'email' => ['required', 'email', 'max:250'],
                'phone_country_iso2' => ['required_with:phone_number', 'string', 'size:2'],
                'phone_number' => ['required', 'string', 'phone:phone_country_iso2'],
            ];
        }
    }
}
