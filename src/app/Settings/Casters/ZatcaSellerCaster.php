<?php

namespace App\Settings\Casters;

use App\Support\Zatca\EInvoice\Seller;
use App\Support\Zatca\EInvoice\SellerAddress;
use Spatie\LaravelSettings\SettingsCasts\SettingsCast;

class ZatcaSellerCaster implements SettingsCast
{
    public function get($payload)
    {
        return new Seller(
            $payload['name'],
            $payload['cr_number'],
            $payload['vat_id'],
            new SellerAddress($payload['address']['line1'], $payload['address']['line2'])
        );
    }

    public function set($payload)
    {
        return [
            'name' => [
                'en' => $payload->getName('en'),
                'ar' => $payload->getName('ar'),
            ],
            'cr_number' => $payload->getCrNumber(),
            'vat_id' => $payload->getVatId(),
            'address' => [
                'line1' => [
                    'en' => $payload->getAddress()->getLine1('en'),
                    'ar' => $payload->getAddress()->getLine1('ar'),
                ],
                'line2' => [
                    'en' => $payload->getAddress()->getLine2('en'),
                    'ar' => $payload->getAddress()->getLine2('ar'),
                ],
            ],
        ];
    }
}
