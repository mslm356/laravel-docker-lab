<?php

namespace App\Http\Controllers;

use App\Jobs\Anb\PullStmtForTestStage;
use App\Support\Anb\AnbApiUtil;
use App\Models\VirtualBanking\VirtualAccount;
use App\Rules\NumericString;
use Illuminate\Http\Request;

class AnbTransfer extends Controller
{
    /**
     * List AMl checks for investor
     *
     * @param  $investorId
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $data = $this->validate($request, [
            'amount' => ['required', new NumericString],
            'iban' => [
                'required',
                'exists:virtual_accounts,identifier'
            ]
        ]);

        $virtualAccount = VirtualAccount::whereIdentifier($data['iban'])->first();

        $response = AnbApiUtil::transfer(
            config('services.anb.account_number'),
            $data['amount'],
            (string)rand(1, 100000000),
            now('Asia/Riyadh'),
            [
                'name' => 'Ahmed Medhat',
                'city' => 'Riyadh',
                'id' => $virtualAccount->wallet_id
            ],
            '0108050053560052',
            null,
            null,
            $virtualAccount->identifier
        );

        if (config('app.env') == 'local') {
            $anbPullStatementJob = (new PullStmtForTestStage())->delay(\Carbon\Carbon::now()->addSeconds(10));
            dispatch($anbPullStatementJob);
        }

        logger('anb-deposit-for-testing', [$response]);

        return response()->json();
    }
}
