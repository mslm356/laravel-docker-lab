<?php


namespace App\Traits;

use App\Services\LogService;
use App\Services\LogService as logs;

trait SendMessage
{

    //  TaqnyatSms func
    public function sendOtpMessage($number, $message)
    {

        $log = new LogService();

        $bearer = 'b149f9228c25959e0d757dba76bad9ec';

        $taqnyt = new TaqnyatSmsServices($bearer);

        $body = $message;

        $sender = 'InvestAseel';

        $response = $taqnyt->sendMsg($body, $number, $sender);

        $responseData = json_decode($response, true);

        if (isset($responseData['statusCode']) && $responseData['statusCode'] != 201) {
            $log->log('TAQNYT-RESPONSE', null, ['FAILD TO SEND MSG', $responseData, $number]);
        }

        return $response;
    }

    public function sendMessages($numbers, $message)
    {

        $log = new LogService();

        $bearer = 'b149f9228c25959e0d757dba76bad9ec';

        $taqnyt = new TaqnyatSmsServices($bearer);

        $body = $message;

        $sender = 'InvestAseel';

        $response = $taqnyt->sendMsg($body, $numbers, $sender);

        $responseData = json_decode($response, true);

        if (isset($responseData['statusCode']) && $responseData['statusCode'] != 201) {
            $log->log('TAQNYT-RESPONSE', null, ['FAILD TO SEND MSG', $responseData, $numbers]);
        }

        return $response;
    }
}
