<?php

use App\Http\Controllers\Admins\Api\Enquiries\EnquiryReplyController;
use App\Http\Controllers\AmlStatusController;
use App\Http\Controllers\AnbTransfer;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Common\GetBanksList;
use App\Http\Controllers\Common\GetCities;
use App\Http\Controllers\Common\GetInvestmentTypes;
use App\Http\Controllers\Common\GetReasons;
use App\Http\Controllers\Common\PasswordReset\ResetPassword;
use App\Http\Controllers\Common\PasswordReset\SendPasswordResetLink;
use App\Http\Controllers\Common\ViewFiles;
use App\Http\Controllers\Investors\Auth\AbsherResetPassword;
use App\Http\Controllers\Investors\Auth\forgetEmail;
use App\Http\Controllers\Investors\Auth\VerifyTwoFactorAuth;
use App\Http\Controllers\Investors\General\GetFaqs;
use App\Http\Controllers\Investors\General\GetWebsiteContent;
use App\Http\Controllers\Investors\Listing\ListingController;
use App\Http\Controllers\Investors\StoreEnquiry;
use App\Http\Controllers\MobileApp\Investors\Listings\TransferController;
use App\Http\Controllers\Webhooks\AnbWebhookController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/special-eod-mt940', [\App\Http\Controllers\Admins\Api\SpecialEodStmtMt940::class , 'getSpecialEodStmt']);//->middleware('permission:download_kyc_pdf');
Route::get('/transactions-to-stmt', [\App\Http\Controllers\Admins\Api\SpecialEodStmtMt940::class, 'transactionsToStmt']);//->middleware('permission:download_kyc_pdf');


Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/files/{file}/view', ViewFiles::class)->name('files.view');

});

Route::post('login', [LoginController::class, 'login'])->middleware('recaptcha.v3');
Route::post('logout', [LoginController::class, 'logout'])->name('api.auth.logout');
Route::post('login/verify-two-factor-auth', [VerifyTwoFactorAuth::class, 'verifyCode'])->middleware('recaptcha.v3');
Route::post('forget-email', [forgetEmail::class, 'forgetEmail'])->middleware('recaptcha.v3');
Route::post('reset-email', [forgetEmail::class, 'resetEmail'])->middleware('recaptcha.v3');

Route::post('/ap/forget-password', [AbsherResetPassword::class,'sendResetPasswordCode'])->middleware('recaptcha.v3');
Route::post('/ap/verify-reset-password', [AbsherResetPassword::class,'verfiyCode'])->middleware('recaptcha.v3');
Route::post('/ap/reset-password', [AbsherResetPassword::class,'resetPassword'])->middleware('recaptcha.v3');



Route::post('forgot-password', SendPasswordResetLink::class)->middleware('guest');
Route::post('reset-password', ResetPassword::class)->middleware('guest');

Route::post('webhooks/enquiries/replies', [EnquiryReplyController::class, 'storeEmailReply'])
    ->name('enquiries.replies.store_email_reply');

Route::get('cities', GetCities::class);
Route::get('contact-reasons', GetReasons::class);
Route::post('enquiries', StoreEnquiry::class);
Route::get('investment-types', GetInvestmentTypes::class);

Route::get('banks', GetBanksList::class)->middleware('auth:sanctum');

Route::post('anb-transfer', AnbTransfer::class);
Route::post('anb-webhooks', AnbWebhookController::class);
// Route::get('/anb/pull', function () {
//     AnbPullStatement::dispatch();
// });

Route::get('listings', [ListingController::class, 'index']);
Route::get('faqs', GetFaqs::class);
Route::get('pages/{page:name}', GetWebsiteContent::class);

Route::post('/aml-status', [AmlStatusController::class, 'amlStatus']);
Route::post('/test-aml-status', [AmlStatusController::class, 'testAmlStatus']);

Route::post('/natheer-notify/primaryIdNotification', [\App\Http\Controllers\NatheerController::class, 'updateStatus'])->middleware('natheer-auth');
Route::post('/primaryIdNotification', [\App\Http\Controllers\NatheerController::class, 'updateStatus'])->middleware('natheer-auth');

Route::get('update-can-access/{user_id}', function ($user_id){

    if (config('app.env') =='production')
    dd('not run on production');
    \Illuminate\Support\Facades\Artisan::call('update:user-access --userId='.$user_id);
    dd('done');
});


Route::get('get-can-access/{user_id}', function ($user_id){

    if (config('app.env') =='production')
        dd('not run on production');

    $data=\App\Models\Users\UserCanAccess::where('user_id',$user_id)->first();
    dd($data);

});

Route::prefix('app')->middleware('AddGuardApiToRequest')->group(function () {

    include('app/auth.php');
    include('app/complaints.php');
    include('app/investment.php');
    include('app/bank_accounts.php');
    include('app/listings.php');
    include('app/faqs.php');
    include('app/dashboard.php');
    include('app/profile-upgrade.php');
    include('app/contact_us.php');
    include('app/discussions.php');
    include('app/transfer.php');
    include('app/static_urls.php');
    include('app/mobile_version.php');
    include('app/notifications.php');
    include('app/member_ship.php');
    include('app/rate.php');
    include('app/subscription_form.php');
    include('app/hyperPay.php');
});
