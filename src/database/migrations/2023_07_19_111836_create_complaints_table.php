<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->longText('description');
            $table->longText('reviewer_comment')->nullable();

            $table->bigInteger('complaint_type_id')->unsigned();
            $table->foreign('complaint_type_id')->references('id')->on('complaint_types');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->bigInteger('reviewer_id')->unsigned()->nullable();
            $table->foreign('reviewer_id')->references('id')->on('users');

            $table->bigInteger('status_id')->unsigned();
            $table->foreign('status_id')->references('id')->on('complaint_statuses');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
