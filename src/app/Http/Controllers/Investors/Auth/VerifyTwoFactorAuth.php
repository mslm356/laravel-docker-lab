<?php

namespace App\Http\Controllers\Investors\Auth;

use App\Http\Controllers\Controller;
use App\Support\Elm\AbsherOtp\TwoFactorAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VerifyTwoFactorAuth extends Controller
{
    public $twoFactorAuthenticate;

    public function __construct()
    {
        $this->twoFactorAuthenticate = new TwoFactorAuth();
    }

    public function verifyCode(Request $request)
    {
        $requestedData = $request->all();

        $data = $this->validate($request, $this->twoFactorAuthenticate->getRules($requestedData));

        return DB::transaction(function () use ($data) {

           return $this->twoFactorAuthenticate->verify($data);

        });
    }
}
