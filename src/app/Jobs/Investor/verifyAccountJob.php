<?php

namespace App\Jobs\Investor;

use App\Enums\Role;
use App\Models\Users\User;
use App\Services\Investors\VerifyAccount;
use App\Services\LogService;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class verifyAccountJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $verifyAccount;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->verifyAccount = new VerifyAccount();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            User::role(Role::Investor)
                ->join('investor_profiles', 'investor_profiles.user_id', '=', 'users.id')
                ->where('investor_profiles.is_identity_verified', '!=', 1)
                ->where('investor_profiles.is_kyc_completed', '!=', 1)
                ->whereNotNull('data')
                ->select('users.*')
                ->groupBy('users.id')
                ->lazy(100)
                ->each(function ($user) {
                    $this->verifyAccount->verify($user);
                });

        } catch (ValidationException $e) {
            $this->logService->log('VERIFY-ACCOUNT-VALIDATION ', $e);
        } catch (VerificationSmsException $e) {
            $this->logService->log('VERIFY-ACCOUNT-Verification ', $e);
        } catch (\Exception $e) {
            $this->logService->log('VERIFY-ACCOUNT-EXC', $e);
        }
    }
}
