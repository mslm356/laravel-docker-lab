<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingInvestorRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_investor_records', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subscription_token_id');
            $table->unsignedBigInteger('listing_id');
            $table->unsignedBigInteger('investor_id');
            $table->decimal('amount_without_fees', 64, 0, true);
            $table->decimal('admin_fees', 64, 0, true);
            $table->decimal('vat_amount', 64, 0, true);
            $table->string('currency');
            $table->decimal('admin_fees_percentage');
            $table->decimal('vat_percentage');
            $table->timestamps();

            $table->foreign('listing_id')->references('id')->on('listings');
            $table->foreign('investor_id')->references('id')->on('users');
            $table->foreign('subscription_token_id', 'sub_token_id_foreign')->references('id')->on('investor_subscription_tokens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_investor_records');
    }
}
