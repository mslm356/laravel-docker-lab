<?php

namespace App\Enums\Anb;

use BenSampo\Enum\Enum;

final class Webhook extends Enum
{
    const StatementCreated = 'statement.created';
    const TransactionReceived = 'transaction.received';
    const PaymentReceived = 'payment.received';
    const PaymentCompleted = 'payment.completed';
}
