<?php

use App\Models\Users\User;
use Illuminate\Support\Str;
use App\Enums\Role as EnumsRole;
use App\Enums\Banking\WalletType;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Migrations\Migration;

class AddingBimUserAsInvestor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::create([
            'first_name' => "Aseel",
            'last_name' => "Investor",
        ]);

        $user->assignRole(
            Role::where([
                'name' => EnumsRole::PayoutsCollector,
                'guard_name' => 'web'
            ])->first()
        );

        $user->createWallet([
            'name' => WalletType::Investor,
            'slug' => WalletType::Investor,
            'uuid' => (string) Str::uuid(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = User::where('email', 'bim@bimsaudi.com')->first();
        $user->investor()->forceDelete();
        $user->removeRole(EnumsRole::PayoutsCollector);
        $user->forceDelete();
    }
}
