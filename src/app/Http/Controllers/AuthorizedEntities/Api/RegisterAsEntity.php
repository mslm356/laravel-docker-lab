<?php

namespace App\Http\Controllers\AuthorizedEntities\Api;

use App\Actions\AuthorizedEntities\CreateEntity;
use App\Actions\AuthorizedEntities\CreateUserInEntity;
use App\Enums\AuthorizedEntityStatus;
use App\Enums\Role as EnumsRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorizedEntities\RegisterEntityRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RegisterAsEntity extends Controller
{
    /**
     * Create entity
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(RegisterEntityRequest $request)
    {

        $fileInputs = [
            'cr',
            'cma_license',
        ];

        foreach ($fileInputs as $file) {

            $responseOfCheckFile = $this->checkExtensions($request[$file]);

            if (!$responseOfCheckFile['status']) {
                return apiResponse(400, __('messages.file_not_valid'));
            }
        }

        $data = $request->validated();

        DB::transaction(function () use ($request, $data) {


            $entity = CreateEntity::run($data + [
                    'status' => AuthorizedEntityStatus::PendingReview
                ]);

            $user = CreateUserInEntity::run($entity, $data);

            $entity->update([
                'creator_id' => $user->id,
                'creator_role_id' => Role::findByName(EnumsRole::AuthorizedEntityAdmin, 'web')->id
            ]);

            Auth::guard('web')->login($user);

            $request->session()->regenerate();
        });

        return response()->json();
    }

    public function checkExtensions($files)
    {

        $response = ['status' => true];

        $extensions = ['pdf', 'png', 'jpg', 'jpeg', 'heic', 'heif'];

        if (is_array($files)) {

            foreach ($files as $file) {

                $extension = strtolower($file->getClientOriginalExtension());

                if (!checkFileExtensions($extension, $extensions)) {
                     $response['status'] = false;
                     return $response;
                }
            }
        } else {

            $extension = strtolower($files->getClientOriginalExtension());

            if (!checkFileExtensions($extension, $extensions)) {
                 $response['status'] = false;
                return $response;
            }
        }

        return $response;
    }
}
