<?php

namespace App\Http\Controllers\Admins\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadEditorImages extends Controller
{
    /**
     * Show Admin Profile.
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $file = $this->validate($request, [
            'upload' => ['image'],
        ])['upload'];

        $path = $file->store('editor-images', $publicDisk = config('filesystems.public'));

        $url = Storage::disk($publicDisk)->url($path);

        if ($publicDisk === 'public') {
            $url = asset($url);
        }

        return response()->json([
            'url' => $url
        ]);
    }
}
