<?php

use App\Models\Accounting\Vat;
use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateAccountingSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('accounting.vat', Vat::first()->id);
    }

    public function down(): void
    {
        $this->migrator->delete('accounting.vat');
    }
}
