<?php

namespace App\Enums\Notifications;

use BenSampo\Enum\Enum;

final class NotificationTypes extends Enum
{
    const Fund = 'Fund';
    const FundReport = 'FundReport';
    const Investment = 'Investment';
    const Deposit = 'Deposit';
    const ApproveProfessionalAccount = 'ApproveProfessionalAccount';
    const RejectProfessionalAccount = 'RejectProfessionalAccount';
    const ApproveBankAccount = 'ApproveBankAccount';
    const RejectBankAccount = 'RejectBankAccount';
    const Info = 'Info';
    const ApproveProfile = 'approveProfile';
    const ReminderProfile = 'reminderProfile';
    const Wallet = 'Wallet';
    const Registration = 'Registration';
    const Membership = 'MemberShip';
    const Suitability = 'Suitability';
    const SubscriptionForm = 'SubscriptionForm';
    const CancelListingInvesting = 'CancelListingInvesting';
}
