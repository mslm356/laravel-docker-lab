<?php

return [
    'debit' => [
        'invest_in_listing' => 'شراء :shares وحدة في :name',
        'invest_in_listing_general' => 'شراء وحدات في صندوق',
        'payout_distribution_in_general' => 'توزيع أرباح صندوق',
//        'payout_distribution' => 'توزيع أرباح (#:payout_id) إلى :user_name (#:id)',
        'payout_distribution' => 'إيداع العوائد النقدية لصندوق (#:fund_title) وتوزيع المستحقات فى حسابك  :amount  ريال سعودى ',
        'payout_distribution_dis' => 'إيداع التوزيعات النقدية لصندوق (#:fund_title) وتوزيع الأرباح فى حسابك  :amount  ريال سعودى ',
        //'payout_distribution' => 'إيداع العوائد النقدية لصندوق (#:fund_title) وتوزيع المستحقات فى حسابك  :amount  ريال سعودى ',
        'invest_in_listing_admin_fee_general' => 'الرسوم الإدارية للاستثمار في صندوق',
        'invest_in_listing_admin_fee' => 'الرسوم الإدارية للاستثمار بعدد :shares وحدة في الصندوق :listing',
        'invest_in_listing_vat_of_admin_fee_general' => 'ضريبة القيمة المضافة للرسوم الإدارية للاستثمار في صندوق',
        'invest_in_listing_vat_of_admin_fee' => 'ضريبة القيمة المضافة للرسوم الإدارية للاستثمار بعدد :shares وحدة في الصندوق :listing',
    ],
    'credit' => [
        'invest_in_listing' => 'المستثمر :user_name (#:user_id) استثمر في الصندوق',
        'invest_in_listing_general' => 'استثمار في صندوق',
        'payout_distribution_in_general' => 'توزيع أرباح',
        'payout_distribution' => 'توزيع أرباح من صندوق :title',
        'invest_in_listing_admin_fee_general' => 'الرسوم الإدارية للاستثمار في صندوق',
        'invest_in_listing_admin_fee' => 'الرسوم الإدارية لاستثمار :user_name (#:user_id) في الصندوق :listing بعدد :shares وحدة',
        'invest_in_listing_vat_of_admin_fee_general' => 'ضريبة القيمة المضافة للرسوم الإدارية للاستثمار في صندوق',
        'invest_in_listing_vat_of_admin_fee' => 'ضريبة القيمة المضافة للرسوم الإدارية لاستثمار :user_name (#:user_id) في الصندوق :listing بعدد :shares وحدة',
    ],
    'investor_ext_trans_in_general' => 'تحويل خارجي إلى حساب بنكي',
    'investor_ext_trans' => 'تحويل خارجي إلى حساب :iban في :name',
    'urway_deposit' => 'إيداع من اجل الاستثمار بواسطه :card_brand',
    'hyperpay_deposit' => 'إيداع عن طريق المدفوعات الرقمية',
    'sdad_deposit' => 'إيداع عن طريق سداد',
    'deposit' => 'إيداع ',
    'compensation' => 'تعويضات',
    'refund_after_ext_tranfer_failure_in_general' => 'حوالة عكسية',
    'refund_after_ext_tranfer_failure' => 'حوالة عكسية من (بنك: :bank, آيبان: :iban)'
];
