<?php

namespace App\Console\Commands\Taqnyt;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Elm\AbsherOtpReason;
use App\Enums\IdentitySourceType;
use App\Models\Country;
use App\Models\Users\User;
use App\Support\Elm\AbsherOtp\Exceptions\CustomerException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Elm\Yaqeen\Exceptions\BirthDateException;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use App\Support\Elm\Yaqeen\Exceptions\MissingInfoRelatedToIdException;
use App\Support\Elm\Yaqeen\Yaqeen;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use App\Traits\SendMessage;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class SendSms extends Command
{

    use SendMessage;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taqnyt:sms {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $yaqeenInfoService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->yaqeenInfoService = new YaqeenInfoService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        $user = User::find($this->option('id'));

        if (!$user) {
            dd('user not found');
        }

        $code = generateRandomCode(4);

        $text1 = 'OTP is: ' . $code;
        $text2 = ' . فضلًا قم بإدخال ';
        $text3 = ' أصيل .. منصة الاستثمار في الأصول من اجل اختبار ارسال رسالة ';
        $message = $text2 . $text1 . $text3;

        $response = $this->sendOtpMessage($user->phone_number, $message);

        dd($response);
    }
}
