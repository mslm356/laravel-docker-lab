<?php

namespace App\Console\Commands\Investors;

use App\Enums\Role;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Services\Investors\OverViewTimeLineServices;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Validation\ValidationException;

class ShowOverView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:over-view {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dashboard over view';
    public $overViewTimeLineServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->overViewTimeLineServices = new OverViewTimeLineServices();
    }

    public function handle()
    {

        try {

            $user = User::find($this->option('user_id'));

            if (!$user) {
                dd('user not valid');
            }

            dd($this->overViewTimeLineServices->getStatistics($user));

        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

    }

}
