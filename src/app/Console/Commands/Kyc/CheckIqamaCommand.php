<?php

namespace App\Console\Commands\Kyc;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Notifications\NotificationTypes;
use App\Mail\kyc\IdExpired;
use App\Mail\kyc\Kyc3Years;
use App\Models\Users\User;
use App\Services\KYC\UpdateKycServices;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use App\Models\Investors\InvestorProfile;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CheckIqamaCommand extends Command
{

    use SendMessage, NotificationServices;

    public $updateKyc;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-iqama-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->updateKyc = new UpdateKycServices();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            $this->updateKyc->updateKYC();
            dd('done');

        } catch (\Exception $e) {
            dd($e->getMessage());
        }


    }
}
