<?php

namespace App\Mail\SubscriptionForm;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/app/overview';
        $name = $this->user ? $this->user->full_name : '---';

        return $this->view('emails.subscription_form.new_request', compact('profileUrl', 'name'))
            ->subject(__('emails/subscription_form/new_request.subject'));
    }
}
