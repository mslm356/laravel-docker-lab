<?php

namespace App\Console\Commands\Investors;

use App\Models\Users\User;
use Illuminate\Support\Str;
use App\Settings\ZatcaSettings;
use Illuminate\Console\Command;
use App\Models\Listings\Listing;
use App\Enums\Banking\WalletType;
use Bavix\Wallet\Models\Transfer;
use Illuminate\Support\Facades\DB;
use App\Settings\InvestingSettings;
use App\Settings\AccountingSettings;
use App\Support\Zatca\EInvoice\Order;
use App\Enums\Banking\TransactionReason;
use App\Models\VirtualBanking\Transaction;
use App\Actions\Investors\CheckIfCanInvest;
use App\Support\Zatca\EInvoice\PurchaseLine;
use App\Models\Listings\ListingInvestorRecord;
use App\Actions\Listings\GetCurrentInvestments;
use App\Models\Users\InvestorSubscriptionToken;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Jobs\Zatca\GenerateEInvoiceAfterInvesting;
use App\Actions\Investors\IsFirstInvestmentInListing;
use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;

class InvestWithAllInvestorBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investors:invest-all-balance {--userId=} {--listingId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invest with the all user balance in the listing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listingId = $this->option('listingId');
        $userId = $this->option('userId');

        DB::transaction(function () use ($listingId, $userId) {
            $listing = Listing::findOrFail($listingId);
            $user = User::findOrFail($userId);
            $userWallet = $user->getWallet(WalletType::Investor);
            $userWallet = $user->getWallet(WalletType::Investor);

            $vatPercentage = optional(app(AccountingSettings::class)->vat)->percentage ?? '0';
            $feePercentage = app(InvestingSettings::class)->administrative_fees_percentage;

            $feeAmount = $listing->share_price->multiply($feePercentage)->divide('100');
            $vatAmount = $feeAmount->multiply($vatPercentage)->divide('100');

            $toBeInvestedShares = (int) floor(
                money($userWallet->balance)
                    ->divide(
                        money_sum($listing->share_price, $feeAmount, $vatAmount)->getAmount()
                    )
                    ->getAmount()
            );

            $currentInvest = GetCurrentInvestments::run($listing)->lockForUpdate()->get();

            $case = CheckIfCanInvest::run($listing, $user, $currentInvest, $toBeInvestedShares);

            if (is_int($case)) {
                return $this->error(trans('messages.invest_policy_' . $case));
            }

            $token =  InvestorSubscriptionToken::create([
                'user_id' => $user->id,
                'listing_id' => $listing->id,
                'shares' => $toBeInvestedShares,
                'reference' => Str::random(25),
                'expired_at' => now()
            ]);

            $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing, $toBeInvestedShares);

            $identity = $user->nationalIdentity;

            $investingRecord = ListingInvestorRecord::create([
                'investor_id' => $user->id,
                'listing_id' => $listing->id,
                'amount_without_fees' => $amountWithoutFees = $toBeInvestedAmountAndFees['amount']->getAmount(),
                'admin_fees' => $adminFees = $toBeInvestedAmountAndFees['fee']->getAmount(),
                'vat_amount' => $vatAmount = $toBeInvestedAmountAndFees['vat']->getAmount(),
                'vat_percentage' => $vatPercentage = $toBeInvestedAmountAndFees['vatPercentage'],
                'admin_fees_percentage' => $feePercentage = $toBeInvestedAmountAndFees['feePercentage'],
                'subscription_token_id' => $token->id,
                'currency' => defaultCurrency(),
            ]);

            $listingWallet = $listing->getWallet(WalletType::ListingEscrow);

            $userWallet->transfer(
                $listingWallet,
                $amountWithoutFees,
                $meta = [
                    'reason' => TransactionReason::InvestingInListing,
                    'reference' => transaction_ref(),
                    'value_date' => now('UTC')->toDateTimeString(),
                    'listing_id' => $listing->id,
                    'listing_title_en' => $listing->title_en,
                    'listing_title_ar' => $listing->title_ar,
                    'user_id' => $user->id,
                    'user_name' => "{$identity->first_name} {$identity->last_name}",
                    'shares' => $toBeInvestedShares,
                    'investing_record_id' => $investingRecord->id,
                ]
            );

            $adminFeeTransaction = $userWallet->transfer(
                $listing->getWallet(WalletType::ListingEscrowAdminFee),
                $adminFees,
                array_merge($meta, [
                    'reason' => TransactionReason::InvestingInListingAdminFee,
                    'reference' => $adminFeesTransactionRef = transaction_ref(),
                    'fee_percentage' => $feePercentage,
                ])
            );

            $userWallet->transfer(
                $listing->getWallet(WalletType::ListingEscrowVat),
                $vatAmount,
                array_merge($meta, [
                    'reason' => TransactionReason::InvestingInListingVatOfAdminFee,
                    'reference' => transaction_ref(),
                    'vat_percentage' => $vatPercentage,
                ])
            );

            if (IsFirstInvestmentInListing::run($currentInvest, $user->id)) {
                $user->investments()
                    ->attach($listing->id, [
                        'invested_amount' => $toBeInvestedAmountAndFees['amount']->getAmount(),
                        'invested_shares' => $toBeInvestedShares,
                        'currency' => defaultCurrency()
                    ]);
            } else {
                DB::table('listing_investor')
                    ->where('listing_id', $listing->id)
                    ->where('investor_id', $user->id)
                    ->update([
                        'invested_shares' => DB::raw("invested_shares + {$toBeInvestedShares}"),
                        'invested_amount' => DB::raw("invested_amount + {$toBeInvestedAmountAndFees['amount']->getAmount()}"),
                    ]);
            }

            $token->update([
                'expired_at' => now()
            ]);

            $subscriptionFormData = GetSubscriptionFormData::run(
                $token,
                $user,
                $listing,
                $toBeInvestedAmountAndFees['total'],
                $currentInvest->where('investor_id', $user->id)
            );

            ExportSubFormAndAttachToInvestingRecord::dispatch(
                $investingRecord,
                $token,
                $user,
                $listing,
                $subscriptionFormData
            );

            GenerateEInvoiceAfterInvesting::dispatch(
                $user,
                new Order(
                    $adminFeesTransactionRef,
                    [
                        new PurchaseLine(
                            [
                                'en' => "({$feePercentage}%) of investing in {$listing->title_en}",
                                'ar' => "(%{$feePercentage}) من مبلغ الاستثمار في {$listing->title_ar}",
                            ],
                            1,
                            $toBeInvestedAmountAndFees['fee'],
                            $vatPercentage,
                            null
                        )
                    ],
                    now('Asia/Riyadh'),
                    $investingRecord
                ),
                app(ZatcaSettings::class)->seller,
                'zatca-e-invoices',
                $adminFeeTransaction
            );
        });

        return true;
    }
}
