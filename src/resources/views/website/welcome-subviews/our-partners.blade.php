<div class="bg-white">
    <div class="max-w-screen-xl mx-auto py-12 px-4 sm:px-6 lg:px-8">
        <h1 class="text-6xl text-center mb-6">Our Partners</h1>
        <div class="grid grid-cols-2 gap-8 md:grid-cols-6 lg:grid-cols-3 mb-10">
            <div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
                <img class="h-16" src="https://tailwindui.com/img/logos/tuple-logo.svg" alt="Tuple">
            </div>
            <div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
                <img class="h-16" src="https://tailwindui.com/img/logos/mirage-logo.svg" alt="Mirage">
            </div>
            <div class="col-span-2 flex justify-center md:col-span-3 lg:col-span-1">
                <img class="h-16" src="https://tailwindui.com/img/logos/workcation-logo.svg" alt="Workcation">
            </div>
        </div>
        <div class="grid grid-cols-2 gap-8 md:grid-cols-6 lg:grid-cols-3">
            <div class="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
                <img class="h-16" src="https://tailwindui.com/img/logos/statickit-logo.svg" alt="StaticKit">
            </div>
            <div class="col-span-1 flex justify-center md:col-span-3 lg:col-span-1">
                <img class="h-16" src="https://tailwindui.com/img/logos/transistor-logo.svg" alt="Transistor">
            </div>
            <div class="col-span-2 flex justify-center md:col-span-3 lg:col-span-1">
                <img class="h-16" src="https://tailwindui.com/img/logos/workcation-logo.svg" alt="Workcation">
            </div>
        </div>
    </div>
</div>