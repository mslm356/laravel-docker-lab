@component('mail::message')
# {{ __('emails/enquiries/new-reply.subject', ['id' => $reply->enquiry_id]) }}

{{ __('emails/enquiries/new-reply.body') }}

@component('mail::panel')
{{ $reply->message }}
@endcomponent

{{ __('emails/enquiries/new-reply.original_ticket') }}

@component('mail::panel')
{{ $enquiry->message }}
@endcomponent

{{ __('emails/enquiries/new-reply.reply_by_reply_to_this') }}<br>

{{ __('emails/common.thanks') }}<br>
{{ config('app.name') }}
@endcomponent
