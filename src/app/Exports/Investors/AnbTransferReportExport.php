<?php

namespace App\Exports\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class AnbTransferReportExport implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('anb_transfers')->where('created_at', '>=', $this->data['start_date'])
            ->where('created_at', '<=', $this->data['end_date'])->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'External Id',
            'Initiator Id',
            'Transaction Id',
            'Bank Account Id',
            'To Bank',
            'To Account',
            'Amount',
            'Currency',
            'Processing Status',
            'Data',
            'Created_at',
            'Updated_at',
        ];
    }

    public function map($anb_transfer): array
    {
        return [
            $anb_transfer->id,
            $anb_transfer->external_id,
            $anb_transfer->initiator_id,
            $anb_transfer->transaction_id,
            $anb_transfer->bank_account_id,
            $anb_transfer->to_bank,
            $anb_transfer->to_account,
            ( money($anb_transfer->amount)->formatByDecimal() / 100 ),
            $anb_transfer->currency,
            $anb_transfer->processing_status,
            $anb_transfer->data,
            $anb_transfer->created_at,
            $anb_transfer->updated_at,
        ];
    }
}
