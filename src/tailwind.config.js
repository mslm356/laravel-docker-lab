const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './resources/views/**/*.blade.php',
        './resources/views/teaser/**/*.blade.php',
    ],
    theme: {
        extend: {
            colors: {
                primary: '#345F94',
                'primary-600': '#22577A',
                'primary-inv': '#fff'
            },
            fontFamily: {
                sans: ["'TheSansArabic SemiLight'", ...defaultTheme.fontFamily.sans],
                heading: ['TheSansArabic']
            }
        },
    },
    variants: {
        padding: ['responsive', 'hover', 'focus', 'direction'],
        margin: ['responsive', 'hover', 'focus', 'direction'],
        inset: ['responsive', 'hover', 'focus', 'direction'],
        translate: ['responsive', 'hover', 'focus', 'direction'],
        textAlign: ['responsive', 'hover', 'focus', 'direction'],
        cursor: ['responsive', 'hover', 'focus', 'disabled'],
        backgroundColor: ['responsive', 'hover', 'focus', 'disabled'],
        flexDirection: ['responsive', 'hover', 'focus', 'direction'],
        display: ['responsive', 'group-hover'],
    },
    plugins: [
        require('@tailwindcss/ui'),
        require('tailwindcss-dir')(),
    ],
}
