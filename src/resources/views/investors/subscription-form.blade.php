<!DOCTYPE html>
<html dir="rtl" lang="ar">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>web site</title>
    <style>


        @include('components.print-components.watermark',
    [
    'website' => 'https://investaseel.sa' ,
    'user_name' => $data['investor_name_en'] ,
    'date'=>\Carbon\Carbon::now()->format('Y-m-d H:i:s')
    ]
    )


        .nav_logo {
            padding-top: 0px;
            text-align: center;
            margin-bottom: 20px;
        }

        .nav_logo img {
            width: 150px;
            margin: auto;
            margin-bottom: 20px;
        }

        .nav_logo span {
            display: block;
        }

        .prs-10 {
            padding: 0 10px;
        }

        .item_fei {
            margin-bottom: 10px;
            background: #F6F6F6;
            height: 50px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 1;
            -ms-flex: 1;
            flex: 1;
            padding: 0 10px;
            border-radius: 4px;
            position: relative;
        }

        .item_fei.active::after {
            content: " ";
            width: 20px;
            height: 20px;
            background-image: url(../../img/active.png);
            background-repeat: no-repeat;
            background-size: contain;
            position: absolute;
            left: 10px;
        }

        .item_fei > span {
            width: 50px;
            height: 50px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
        }

        .item_fei > span img {
            width: 22px;
            height: 22px;
        }

        .name_t {
            margin-bottom: 10px;
        }

        .name_t h6 {
            font-size: 14px;
        }

        .item-2 {
            -webkit-box-align: start;
            -ms-flex-align: start;
            align-items: start;
        }

        .item-2 div {
            -webkit-box-flex: 1;
            -ms-flex: 1;
            flex: 1;
        }

        footer {
            border-bottom: 4px solid #0565FF;
            padding-bottom: 10px;
        }

        footer h6 {
            text-align: center;
            margin-bottom: 10px;
        }

        /*# sourceMappingURL=style.css.map */
    </style>
    <style>
        body {
            font-family: 'HelveticaNeueLT' !important;
        }

        html {
            height: 100%;
        }
    </style>

    <script src="https://cdn.tailwindcss.com"></script>

</head>


<body>

<div class="nav_logo">
    <img src="{{ asset('images/aseel-blue.png') }}" width="120"/>
    <span style="font-weight: bold;">نموذج الاشتراك</span>
    <span style="font-weight: bold;">Subscription form</span>
</div>

<div class="  prs-10">
    <div class="min-title grow mb-4">
        <h6 style="font-weight: bold;">تفاصيل المستثمر</h6>
        <h6 style="font-weight: bold;">Investor Details</h6>

    </div>
    <div class="flex flex-row prs-10">
        <div class="" style="width: calc(100% / 3);">
            <div class="name_t">
                <h6>رقم المستثمر</h6>
                <h6>Investor No </h6>
            </div>
            <div class="item_fei">
                <span><img src="{{ asset('images/invest_num.png')}}"></span>
                <div>
                    <h6>{{$data['investor_id']}}</h6>
                </div>
            </div>
        </div>
        <div class="grow grow prs-10">
            <div class="name_t">
                <h6>اسم المستثمر</h6>
                <h6>Investor Name </h6>
            </div>
            <div class="item_fei item-2">
      <span>
        <img src="{{ asset('images/i2.png')}}">
      </span>
                <div>
                    <h6>{{ $data['investor_name_ar'] }}</h6>
                    <span>{{ $data['investor_name_en'] }}</span>
                </div>
            </div>
        </div>

    </div>
    <div class="flex flex-row  prs-10">
        <div class="" style="width: calc(100% / 3);">
            <div class="name_t">
                <h6>نوع المستثمر</h6>
                <h6>Investor Type </h6>
            </div>
            <div class="item_fei active">
                <span><img src="{{ asset('images/i3.png')}}"></span>

                @if($data['investor_type'] == 'company')
                    <div>
                        <h6>شركه</h6>
                        <span>{{ $data['investor_type'] }}</span>
                    </div>
                @else
                    <div>
                        <h6>فردي</h6>
                        <span>{{ $data['investor_type'] }}</span>
                    </div>
                @endif
            </div>
        </div>


        <div class="grow grow prs-10">
            <div class="name_t">
                <h6>رقم الهوية / رقم السجل تجارى</h6>
                <h6>ID</h6>
            </div>
            <div class="item_fei active">
              <span>
                <img src="{{ asset('images/i4.png')}}">
              </span>
                <div>
                    <h6>{{$data['investor_national_id']}}</h6>
                </div>
            </div>
        </div>


        <div class="grow grow prs-10">
            <div class="name_t">
                <h6>الجنسية</h6>
                <h6>Nationality</h6>
            </div>
            <div class="item_fei active">
      <span>
        <img src="{{ asset('images/i5.png')}}">
      </span>
                <div>
                    <h6>{{$data['nationality']}}</h6>
                </div>
            </div>
        </div>
    </div>

    <div class="min-title grow  mb-4">
        <h6 style="font-weight: bold;"> تفاصيل الاستثمار</h6>
        <h6 style="font-weight: bold;"> Investment Details</h6>

    </div>
    <div class="flex flex-row prs-10">

        <div class="grow">
            <div class="name_t">
                <h6> مدير الصندوق</h6>
                <h6>Fund Manger </h6>
            </div>
            <div class="item_fei">
                <span><img src="{{ asset('images/i6.png')}}"></span>

                <div>
                    <h6>{{$data['authoirzedEntity']}}</h6>
                </div>
            </div>
        </div>

        <div class="grow grow prs-10">
            <div class="name_t">
                <h6> اسم الصندوق</h6>
                <h6>Fund Name </h6>
            </div>
            <div class="item_fei">
                <span><img src="{{ asset('images/i7.png')}}"></span>

                <div>
                    <h6>{{$data['fund_name_ar']}}</h6>
                    <span>{{$data['fund_name_en']}}</span>
                </div>
            </div>
        </div>


        <div class="grow grow prs-10">
            <div class="name_t">
                <h6> عدد الوحدات </h6>
                <h6> Number of Units </h6>
            </div>
            <div class="item_fei">
                <span><img src="{{ asset('images/i8.png')}}"></span>

                <div>
                    <h6>{{$data['currentInvestmentUnits']}}</h6>
                </div>
            </div>
        </div>
    </div>

    <div class="min-title grow  mb-4">
        <h6 style="font-weight: bold;"> تفاصيل الدفع</h6>
        <h6 style="font-weight: bold;">Payment Details</h6>

    </div>
    <div class="flex flex-row prs-10">
        <div class="grow grow prs-10">
            <div class="name_t">
                <h6> المبلغ</h6>
                <h6> Amount </h6>
            </div>
            <div class="item_fei item-2">
      <span>
        <img src="{{ asset('images/i9.png')}}">
      </span>
                <div>
                    <span>{{$data["amount"]}}</span>
                    <h6>{{$data["amount_words"]}} </h6>
                </div>
            </div>
        </div>

    </div>
    <div class="flex flex-row prs-10">
        <div class="grow">
            <div class="name_t">
                <h6> العمله</h6>
                <h6>Currency </h6>
            </div>
            <div class="item_fei">
                <span><img src="{{ asset('images/i9.png')}}"></span>

                <div>
                    <h6>ريال سعودي</h6>
                    <span>SAR</span>
                </div>
            </div>
        </div>

        <div class="grow grow prs-10">
            <div class="name_t">
                <h6>تاريخ التنفيذ</h6>
                <h6>Created At</h6>
            </div>
            <div class="item_fei ">
      <span>
        <img src="{{ asset('images/i11.png')}}">
      </span>
                <div>
                    <h6>{{$data['value_date']}}</h6>
                </div>
            </div>
        </div>
    </div>


    <footer>

        <h6>بهذا نفوضكم باستثمار المبلغ الموضح أعلاه طبقاً للأحكام والشروط التي تمت قراءتها وفهمها و وقعت بقبولها و
            استلمت نسخة منه</h6>
        <h6>I (We) hereby authorize you to invest the amount specified above in accordance with the terms and conditions
            of the fund which I have read, understood and signed as confirmation of acceptance and received a copy
            therof </h6>
    </footer>

</div>

</body>

</html>
<!--  -->
