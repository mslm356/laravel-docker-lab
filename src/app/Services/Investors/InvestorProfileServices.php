<?php


namespace App\Services\Investors;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Actions\Investors\Yaqeen\UpdateYaqeenData;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Investors\Registration\CurrentInvestment;
use App\Enums\Investors\Registration\Education;
use App\Enums\Investors\Registration\InvestmentObjective;
use App\Enums\Investors\Registration\InvestorEntity;
use App\Enums\Investors\Registration\Level;
use App\Enums\Investors\Registration\MoneyRange;
use App\Enums\Investors\Registration\NetWorth;
use App\Enums\Investors\Registration\Occupation;
use App\Enums\Notifications\NotificationTypes;
use App\Jobs\Aml\CreateScreen;
use App\Mail\Investors\ApproveProfile;
use App\Models\Investors\InvestorProfileLog;
use App\Support\Aml\AmlServices;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use BenSampo\Enum\Rules\EnumValue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class InvestorProfileServices
{

    use NotificationServices, SendMessage ;


    public $amlServices;

    public function __construct()
    {
        $this->amlServices = new AmlServices();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function validator(array $data)
    {
        return Validator::make($data, [
            'education_level' => ['required', 'integer', new EnumValue(Education::class, false)],
            'occupation' => ['required', 'integer', new EnumValue(Occupation::class, false)],
            'net_worth' => ['required', 'integer', new EnumValue(NetWorth::class, false)],
            'annual_income' => ['required', 'integer', new EnumValue(MoneyRange::class, false)],
            'expected_annual_invest' => ['required', 'integer', new EnumValue(MoneyRange::class, false)],
            'expected_invest_amount_per_opportunity' => ['required', 'integer', new EnumValue(MoneyRange::class, false)],
            'invest_objective' => ['required', 'array', 'in:' . implode(',', InvestmentObjective::getValues())],
            'current_invest' => ['required', 'array', 'in:' . implode(',', CurrentInvestment::getValues())],
            'is_on_board' => ['required', 'boolean'],
            'general_info.is_individual' => ['required', 'integer', new EnumValue(InvestorEntity::class, false)],
            'general_info.any_xp_in_fin_sector' => ['required', 'boolean'],
            'general_info.any_xp_related_to_fin_sector' => ['required', 'boolean'],
            'general_info.have_you_invested_in_real_estates' => ['required', 'boolean'],
            'general_info.investment_xp' => ['required', 'integer', new EnumValue(Level::class, false)],
            'financial_info.years_of_inv_in_securities' => ['nullable', 'numeric'],

            'general_info.is_entrusted_in_public_functions' => ['required', 'boolean'],
            'general_info.have_relationship_with_person_in_public_functions' => ['required', 'boolean'],
            'general_info.is_beneficial_owner' => ['required', 'boolean'],
            'general_info.identity_of_beneficial_owner' => ['exclude_if:general_info.is_beneficial_owner,1,true', 'required', 'string'],
            'financial_info.extra_financial_information' => ['nullable', 'string', 'max:2000'],
            'is_normal_kyc_data' => ['nullable'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @param \App\Support\VirtualBanking\BankService $wallet
     * @param \App\Models\Users\User
     * @return array
     */
    public function create(array $data, $user)
    {
        $data['financial_info']['years_of_inv_in_securities'] = isset($data['financial_info']['years_of_inv_in_securities']) ?
            $data['financial_info']['years_of_inv_in_securities'] : 0;


        $investorData = [
            'education_level' => $data['education_level'],
            'occupation' => $data['occupation'],
            'net_worth' => $data['net_worth'],
            'annual_income' => $data['annual_income'],
            'expected_annual_invest' => $data['expected_annual_invest'],
            'expected_invest_amount_per_opportunity' => $data['expected_invest_amount_per_opportunity'],
            'is_on_board' => $data['is_on_board'],
            'current_invest' => $data['current_invest'] ?? [],
            'invest_objective' => $data['invest_objective'] ?? [],
            'data->years_of_inv_in_securities' => (int)$data['financial_info']['years_of_inv_in_securities'],
            'data->investment_xp' => (int)$data['general_info']['investment_xp'],
            'data->is_individual' => (int)$data['general_info']['is_individual'],
            'data->any_xp_in_fin_sector' => (bool)$data['general_info']['any_xp_in_fin_sector'],
            'data->any_xp_related_to_fin_sector' => (bool)$data['general_info']['any_xp_related_to_fin_sector'],
            'data->have_you_invested_in_real_estates' => (bool)$data['general_info']['have_you_invested_in_real_estates'],
            'data->is_entrusted_in_public_functions' => (bool)$data['general_info']['is_entrusted_in_public_functions'],
            'data->have_relationship_with_person_in_public_functions' => (bool)$data['general_info']['have_relationship_with_person_in_public_functions'],
            'data->is_beneficial_owner' => $isBeneficialOwner = (bool)$data['general_info']['is_beneficial_owner'],
            'data->identity_of_beneficial_owner' => $isBeneficialOwner === false ? $data['general_info']['identity_of_beneficial_owner'] : null,
            'data->extra_financial_information' => $data['financial_info']['extra_financial_information'] ?? null,
            'is_kyc_completed' => true,
            'is_normal_kyc_data' => isset($data['is_normal_kyc_data']) ? $data['is_normal_kyc_data'] : true,
            'status' => $user->investor && $user->investor->is_identity_verified ? InvestorStatus::Approved : InvestorStatus::PendingReview, // InvestorStatus::Approved,
            'last_kyc_update_date' => now(),
            'unsuitability_confirmation' => false
        ];


        $investorProfile = $user->investor->update($investorData);

        InvestorProfileLog::create(['user_id' => $user->id, 'data' => json_encode($investorData), 'type' => 'kyc_update']);

        //      create aml screen
        CreateScreen::dispatch($user)->onQueue('aml');

        if (config('mail.MAIL_STATUS') == 'ACTIVE') {
            Mail::to($user)->queue(new ApproveProfile());
        }

        $this->sendSmsData($user);

        $this->send(NotificationTypes::ApproveProfile, $this->notificationData($user), $user->id);

        return $investorProfile;
    }

    public function checkIqamaExpire($user)
    {
        $todayInHijri = Hijri::Date('Y-m-d', Carbon::now()->format('Y-m-d'));

        if (!$user->nationalIdentity) {
            return true;
        }


        if ($user->nationalIdentity->id_expiry_date > $todayInHijri) {
            return false;
        }

        UpdateYaqeenData::run($user);

        if (substr($user->nationalIdentity->birth_date, 5) == '03-01') {
            UpdateYaqeenData::run($user, -2);
        }

        if ($user->nationalIdentity->id_expiry_date > $todayInHijri) {
            return false;
        }


        if (Str::startsWith($user->nationalIdentity->nin, '2')) {
            return false;
        }


        return true;
    }


    public function notificationData($user)
    {
        return [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => 'Approval of your profile',
            'title_ar' => 'الموافقة على ملفك الشخصى',
            'content_en' => 'We would like to inform you that your registration with us is confirmed',
            'content_ar' => 'حبينا نبشرك بأن تسجيلك معنا مؤكد',
        ];


    }

    public function sendSmsData($user)
    {
        $content_en = 'Welcome to Aseel Financial Platform Be the first to invest in the opportunities that Aseel offer by downloading the application and ensbling app notifications';
        $content_ar = 'حياك الله مستثمرنا الكريم في منصة أصيل المالية كن أول المستثمرين في الفرص التي ستطرحها أصيل من خلال تحميل التطبيق وتفعيل الاشعارات';
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }


    public function confirmUnsuitability($user)
    {
        $is_suitabile = $user->investor->update(['unsuitability_confirmation' => 1]);

        if ($is_suitabile) {
            $this->sendUnsuitabilityNotification($user);
        }
    }


    public function sendUnsuitabilityNotification($user)
    {
        $data = [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => 'Customer Suitability',
            'title_ar' => 'ملاءمة العميل',
            'content_en' => 'Suitability assessment was approved',
            'content_ar' => 'تمت الموافقة على تقييم الملاءمة',
        ];

        $this->send(NotificationTypes::Suitability, $data, $user->id);

    }

}
