<?php

namespace App\Services\Sdad\Repository;

use App\Services\LogService;
use App\Services\Sdad\Contracts\EdaatProviderClientRepositoryContract;
use Illuminate\Support\Facades\Http;

class EdaatProviderClientRepository implements EdaatProviderClientRepositoryContract
{
    public $url;
    public $method;
    public $service_name;
    public $response;
    public $client;
    public $request_body;
    public $auth_repo;
    public $auth_required;
    public $headers;
    public $content_type;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    /**
     * @inheritDoc
     * service name must be identical to a service in config file
     */
    public function initiation(string $service_name): ?EdaatProviderClientRepositoryContract
    {
        //TODO try and catch
        $this->auth_repo = new EdaatAuthRepository();
        $this->service_name = $service_name;
        $this->content_type = $this->get_content_type($this->service_name);
        $this->url = $this->get_url($this->service_name);
        $this->method = $this->get_method($this->service_name);
        $this->auth_required = $this->check_auth($this->service_name);
        $this->client = Http::withHeaders($this->headers_preparation());
        if ($this->auth_required) {
            $token = $this->auth_repo->getValidToken()->token;
            $this->client = $this->client->withToken($token);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function send(): ?EdaatProviderClientRepositoryContract
    {
        try {
            if ($this->method == 'post') {
                if ($this->content_type == 'form') {
                    $this->response = $this->client->asForm()->post($this->url, $this->request_body);
                } else {
                    $this->response = $this->client->post($this->url, $this->request_body);
                }
            } else {
                $this->response = $this->client->get($this->url);
            }

            $this->logService->log(
                'SDAD-SERVER-RESPONSE',
                null,
                ['REQUEST && RESPONSE', $this->request_body, $this->response->body(), $this->response->status()]
            );

            return $this;
        } catch (\Exception $e) {
            $this->logService->log('Sdad-client_repo', $e);
            throw new \Exception('Sdad Provider client error');
        }

    }

    /**
     * @inheritDoc
     */
    public function getResponse(): ?object
    {
        if ($this->response) {
            return json_decode($this->response->body());
        } else {
            return null;
        }

    }

    /**
     * @inheritDoc
     */
    public function reset(): EdaatProviderClientRepositoryContract
    {
        // TODO: Implement reset() method.
    }


    /**
     * @param bool $auth_required
     * @return string[]
     * prepare the required headers
     */
    public function headers_preparation($headers = []): array
    {
        $base_headers = [
        ];
        $headers = array_merge($base_headers, $headers);

        return $headers;
    }

    /**
     * @param $service_name
     * @return string
     * get the service end-point according to the env config
     */
    public function get_url($service_name): string
    {
        if (config('edaat.mode') == 'test') {
            return config('edaat.test.services.' . $service_name . '.url');
        } else {
            return config('edaat.live.services.' . $service_name . '.url');
        }
    }

    /**
     * @param $service_name
     * @return string
     * get the service HTTP Method according to the env config
     */
    public function get_method($service_name): string
    {
        if (config('edaat.mode') == 'test') {
            return config('edaat.test.services.' . $service_name . '.method');
        } else {
            return config('edaat.live.services.' . $service_name . '.method');
        }
    }

    /**
     * @param $service_name
     * @return string
     * check if auth is required  according to the env config
     */
    public function check_auth($service_name): string
    {
        if (config('edaat.mode') == 'test') {
            return config('edaat.test.services.' . $service_name . '.auth_required');
        } else {
            return config('edaat.live.services.' . $service_name . '.auth_required');
        }
    }

    /**
     * @param $service_name
     * @return string
     * get headers according to the env config
     */
    public function get_content_type($service_name): string
    {
        if (config('edaat.mode') == 'test') {
            return config('edaat.test.services.' . $service_name . '.content_type');
        } else {
            return config('edaat.live.services.' . $service_name . '.content_type');
        }
    }

    /**
     * set request body
     * @param array $data
     * @return EdaatProviderClientRepositoryContract
     */
    public function body(array $data): EdaatProviderClientRepositoryContract
    {
        $this->request_body = $data;
        return $this;

    }

    /**
     * generate url with parameters
     * @param array $data
     * @return EdaatProviderClientRepositoryContract
     */
    public function url_with_parameter(array $data): EdaatProviderClientRepositoryContract
    {
        $this->request_body = $data;
        return $this;
    }
}
