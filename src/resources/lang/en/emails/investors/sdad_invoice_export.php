<?php

return [
    'subject' => 'SADAD Invoice Number',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => ' A SADAD invoice has been issued to add credit to your wallet',
    'message_2' => ' SADAD invoice number  :invoice_id',
    'message_3' => ' In the amount of  :amount SAR',
    'message_4' => 'Operation date and time  :time',
    'message_5' => ' Thank you for dealing with Aseel platform. ',
    'link_text' => 'To view your control panel, please go to',
    'view' => 'View dashboard',
    'sdad_invoice' => 'Biller No. :903',
    'service_type' => 'Service Type : EDAAT',
    'expire' => 'payment number expiration time is 30 minutes.',
];
