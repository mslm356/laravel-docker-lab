<?php

namespace App\Http\Controllers\MobileApp\Investors\Faqs;

use App\Http\Controllers\Controller;
use App\Models\FAQ\Faq;
use App\Services\LogService;

class FaqsController extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function index()
    {
        try {
            $data = Faq::get()->map(fn ($faq) => [
                'id' => $faq->id,
                'question' => $faq->question,
                'answer' => $faq->answer
            ]);

            return apiResponse(200, __('mobile_app/message.success'), $data);

        } catch (\Exception $e) {
            $code = $this->logService->log('GET-FAQS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }
}
