<?php


namespace App\Services\MemberShip;

use App\Enums\MemberShips\MemberShipType;
use App\Enums\Notifications\NotificationTypes;
use App\Services\LogService;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use Illuminate\Support\Facades\Mail;

class AlertUser
{
    use NotificationServices, SendMessage;

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function handle($membership, $action)
    {
        try {
            if (!$membership || !$membership->user) {
                return false;
            }

            $user = $membership->user;
            $membershipName = __(MemberShipType::getKey($membership->member_ship), [], $user->locale);

            $actions = [

                'create' => [
                    'email' => 'CreateRequest',
                    'notification' => $this->notificationData($action, $membership->membership_name_key),
                    'sms' => __('membership.sms_create', ['membership' => $membershipName], $user->locale),
                ],

                'downGrad' => [
                    'email' => 'DownGrade',
                    'notification' => $this->notificationData($action, $membership->membership_name_key),
                    'sms' => __('membership.sms_downGrad', ['membership' => $membershipName], $user->locale),
                ],

                'accept' => [
                    'email' => 'AcceptRequest',
                    'notification' => $this->notificationData($action, $membership->membership_name_key),
                    'sms' => __('membership.sms_accept', ['membership' => $membershipName], $user->locale),
                ],
            ];

            $emailClassPath = '\App\Mail\MemberShip\\' . $actions[$action]['email'];
            Mail::to($user)->queue(new $emailClassPath($user, $membershipName));

            $this->send(NotificationTypes::Membership, $actions[$action]['notification'], $user->id);

            $this->sendOtpMessage($user->phone_number, $actions[$action]['sms']);

        } catch (\Exception $e) {
            $this->logService->log('MEMBERSHIP-ALERT-USER-EXC', $e);
        }
    }

    public function notificationData($action, $membershipName)
    {
        return [
            'title_ar'  => __('membership.default_notify_title', [], 'ar'),
            'title_en'  => __('membership.default_notify_title', [], 'en'),
            'content_ar'  => __('membership.notify_' . $action, ['membership' => __($membershipName, [], 'ar')], 'ar'),
            'content_en'  => __('membership.notify_' . $action, ['membership' => __($membershipName, [], 'en')], 'en'),
            'channel' => 'mobile'
        ];
    }
}
