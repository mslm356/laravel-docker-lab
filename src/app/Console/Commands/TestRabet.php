<?php

namespace App\Console\Commands;

use App\Support\Elm\Rabet\RabetService;
use Illuminate\Console\Command;

class TestRabet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rabetService = new RabetService();
       $responce= $rabetService->checkRabetPhone(1081735944,966501415347);

       dd($responce);
    }
}
