<?php

namespace App\Http\Controllers\MobileApp\Investors\Investments;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Investing\InvestingRecordResource;
use App\Http\Resources\Investors\MyInvestments;
use App\Http\Resources\Investors\TransactionResource;
use App\Models\Listings\Listing;
use App\Services\Investors\AbsherOtpService;
use App\Services\Investors\Investment\SendOtpServices;
use App\Services\Investors\Investment\SubscriptionTokenServices;
use App\Services\Investors\Investment\VerifyOtpServices;
use App\Services\Investors\InvestmentServices;
use App\Services\Investors\TransactionServices;
use App\Services\LogService;
use App\Services\MemberShip\SpecialCanInvest;
use App\Support\Users\UserAccess;
use Illuminate\Http\Request;

class InvestmentsController extends Controller
{

    public $transactionServices;
    public $investmentServices;
    public $logService;
    public $userAccess;
    public $absherOtpService;
    public $subscriptionTokenServices;
    public $verifyOtpServices;
    public $sendOtpServices;
    public $specialCanInvest;

    public function __construct()
    {
        $this->transactionServices = new TransactionServices();
        $this->investmentServices = new InvestmentServices();
        $this->logService = new LogService();
        $this->userAccess = new UserAccess();
        $this->absherOtpService = new AbsherOtpService();
        $this->subscriptionTokenServices = new SubscriptionTokenServices();
        $this->verifyOtpServices = new VerifyOtpServices();
        $this->sendOtpServices = new SendOtpServices();
        $this->specialCanInvest = new SpecialCanInvest();
    }

    public function getTransactions(Request $request)
    {
        try {
            $user = getAuthUser('api');
            $transactions = $this->transactionServices->myTransactions($request, $user);

            return apiResponse(200, __('mobile_app/message.success'), TransactionResource::collection($transactions));

        } catch (\Exception $e) {
            $code = $this->logService->log('getTransactions', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function getMyInvestingRecords(Request $request)
    {
        try {
            $user = getAuthUser('api');
            $investingRecords = $this->transactionServices->getMyInvestingRecords($request, $user);

            return apiResponse(
                200,
                __('mobile_app/message.success'),
                InvestingRecordResource::collection($investingRecords->with('listing')->simplePaginate())
            );

        } catch (\Exception $e) {
            $code = $this->logService->log('getMyInvestingRecords', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function balanceInquiry(Request $request)
    {
        try {

            $user = getAuthUser('api');

            $data = $this->transactionServices->getBalanceInquiry($user);

            $dataResponse = [
                'balance' => $data['total'],
                'balance_fmt' => cleanDecimal(number_format($data['total'], 2)),
                'total_withdrawal' => number_format(abs(money($data['total_withdrawal'])->formatByDecimal()), 2)
            ];

            return apiResponse(200, __('mobile_app/message.success'), $dataResponse);

        } catch (\Exception $e) {
            $code = $this->logService->log('balanceInquiry', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function getMyInvestment(Request $request)
    {
        try {
            $user = getAuthUser('api');

            $listing = $user->investments()
                ->where('payout_done', 0)
                ->wherePivot('invested_shares', '>', 0)
                ->with('votes')
                ->orderBy('pivot_id', 'desc')
                //->toScopes(OrderByDateScope::class)
                ->get();

            return apiResponse(200, __('mobile_app/message.created_successfully'), MyInvestments::collection($listing));

        } catch (\Exception $e) {
            $code = $this->logService->log('getMyInvestment', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function convertSharesToAmount(Request $request, $listing)
    {

        try {
            $user = null;

            $listing = Listing::find($listing);

            if (!$listing) {
                return apiResponse(200, __('mobile_app/message.not_found'));
            }

            $this->validate($request, ['shares' => ['required', 'integer']]);

            $responseData = $this->investmentServices->shareAmount($request, $listing, $user);

            $responseData['urway'] = false;
            $responseData['cardTokens'] = [];

            return apiResponse(200, __('mobile_app/message.success'), $responseData);

        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'convertSharesToAmount');
        }
    }

    public function createSubscriptionToken(Request $request, Listing $listing)
    {

        try {

            $request->request->add(['device' => 'mobile']);

            $response = $this->subscriptionTokenServices->handle($request, $listing, "api");

            return $this->handelStatusResponce($response);


        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'CREATE-SUBSCRIPTION-TOKEN');

        }
    }

    public function sentOtp(Request $request)
    {

        try {

            $user = getAuthUser('api');

            $response = $this->sendOtpServices->handle($request, $user);

            return $this->handelStatusResponce($response);
        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'INVESTMENTS-SEND-OTP');
        }
    }

    public function verifyOtp(Request $request)
    {

        try {

            $user = getAuthUser('api');

            $response = $this->verifyOtpServices->handle($request, $user);
            return $this->handelStatusResponce($response);

        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'INVESTMENTS-verifyOtp-Exception');
        }
    }
}
