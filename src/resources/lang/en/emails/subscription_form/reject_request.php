<?php

return [
    'subject' => 'Your request of subscription form not approved',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'We apologize for not accepting your request',
    'message_2' => ' to open an investment account for :reason',
    'message_3' => 'Thank you for trusting Aseel platform',
    'link_text' => 'Check your profile',
];
