@component('mail::message')
# {{ __('emails/enquiries/enquiry-received.subject', ['id' => $enquiry->id]) }}

{{ __('emails/enquiries/enquiry-received.body') }}

{{ __('emails/common.thanks') }}<br>
{{ config('app.name') }}
@endcomponent
