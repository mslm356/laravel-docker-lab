<?php

namespace App\Actions\Listings\Wallets;

use App\Models\Listings\Listing;
use Lorisleiva\Actions\Concerns\AsAction;

class GetListingBalance
{
    use AsAction, MapsWalletType;

    /**
     * Handle creating listing wallet
     *
     * @param \App\Models\Listings\Listing $listing
     * @param string $type 'SPV' or 'ESCROW'
     *
     * @return string
     */
    public function handle(Listing $listing, $type)
    {
        $mappedType = $this->getMappedWalletType($type);

        $wallet = $listing->getWallet($mappedType);

        if ($wallet) {
            return money($wallet->balance);
        }

        return null;
    }
}
