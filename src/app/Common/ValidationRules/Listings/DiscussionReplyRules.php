<?php

namespace App\Common\ValidationRules\Listings;

class DiscussionReplyRules
{
    static public function get()
    {
        return [
            'comment' => ['required', 'string', 'max:1000'],
            'replier_name' => ['required', 'string', 'max:100'],
            'replier_role.en' => ['required', 'string', 'max:100'],
            'replier_role.ar' => ['required', 'string', 'max:100'],
        ];
    }
}
