<?php

namespace App\Http\Controllers\Investors\Investing;

use App\Services\Investors\TransactionServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Investing\InvestingRecordResource;

class GetMyInvestingRecords extends Controller
{

    public $transactionServices;

    public function __construct()
    {
        $this->transactionServices = new TransactionServices();
    }

    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');

        $investingRecords = $this->transactionServices->getMyInvestingRecords($request, $user);

        return InvestingRecordResource::collection($investingRecords
            ->with('listing')
            ->simplePaginate());
    }
}
