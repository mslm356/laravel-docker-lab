<?php

namespace App\Console\Commands\Funds;

use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckInvestedFundPrercentage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-fund-percent {--listing_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $listing = Listing::find($this->option('listing_id'));

        if (!$listing) {
            dd('fund not valid');
        }

       $total_investment_shares=ListingInvestor::where('listing_id',$listing->id)->get()->sum('invested_shares');

        echo 'listing->total_invested_shares ---->  '.$listing->total_invested_shares." \n";
        echo 'sum invested_shares om listing investor ---->  '.$total_investment_shares." \n";

        $diff=$listing->total_invested_shares - $total_investment_shares;
        echo 'listing->total_invested_shares - $total_investment_shares ---->  '.$diff." \n";




    }
}
