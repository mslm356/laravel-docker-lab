<?php


namespace App\Services\Investors;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\OverviewStatistic;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class OverViewTimeLineServices
{

    public function index($user)
    {
        $wallet = $user->getWallet(WalletType::Investor);

        $acc = money(0);

        $transactions = Transaction::where('wallet_id', $wallet->id)->get();

        $balanceTransactions = $transactions->map(function ($transaction) {
            return [
                'x' => $transaction->created_at->format('Y-m-d'),
                'y' => money($transaction->amount),
            ];
        })->groupBy('x')
            ->map(function ($l, $date) use (&$acc) {
                $yTotal = money_sum(...$l->pluck('y')->all());

                return [
                    'x' => $date,
                    'y' => ($acc = $acc->add($yTotal))->formatByDecimal()
                ];
            })
            ->values()
            ->prepend($startingPoint = ['y' => 0, 'x' => $user->created_at->format('Y-m-d')]);

        $acc = money(0);

        $invTransactions = $transactions->filter(
            fn ($l) => $l->type === TransactionReason::InvestingInListing
        )
            ->map(function ($transaction) {
                return [
                    'x' => $transaction->created_at->format('Y-m-d'),
                    'y' => money($transaction->amount)->absolute(),
                ];
            })
            ->groupBy('x')
            ->map(function ($l, $date) use (&$acc) {
                $yTotal = money_sum(...$l->pluck('y')->all());

                return [
                    'x' => $date,
                    'y' => ($acc = $acc->add($yTotal))->formatByDecimal()
                ];
            })
            ->values()
            ->prepend($startingPoint = ['y' => 0, 'x' => $user->created_at->format('Y-m-d')]);

        $accTransactions = $transactions->filter(
            fn ($transaction) => (int)$transaction->type !== TransactionReason::InvestingInListing
        )
            ->map(function ($transaction) {
                return [
                    'date' => $transaction->created_at,
                    'value' => money($transaction->amount),
                ];
            });

        $listings = $user->investments()
            ->with([
                'valuations' => function ($query) {
                    $query->oldest('date');
                }
            ])
            ->get();

        $listings->each(function ($listing) use (&$accTransactions) {
            if ($listing->valuations->isEmpty()) {
                return;
            }

            $previous = $listing->target;

            foreach ($listing->valuations as $valuation) {
                $accTransactions->push([
                    'value' => unrealizedPL(
                        $listing->pivot->invested_shares,
                        $listing->total_shares,
                        $previous,
                        $valuation->value
                    ),
                    'date' => $valuation->date
                ]);

                $previous = $valuation->value;
            }
        });

        $acc = money(0);

        $accTransactions = $accTransactions->sortBy(fn ($g) => $g['date']->timestamp)
            ->values()
            ->groupBy(fn ($transaction) => $transaction['date']->format('Y-m-d'))
            ->map(function ($l, $date) use (&$acc) {
                $valueTotal = money_sum(...$l->pluck('value')->all());

                return [
                    'x' => $date,
                    'y' => ($acc = $acc->add($valueTotal))->formatByDecimal()
                ];
            })
            ->values()
            ->prepend($startingPoint);

        $lastAccTransaction = $accTransactions->last();
        $lastBalanceTransaction = $balanceTransactions->last();
        $lastInvTransaction = $invTransactions->last();

        if ($lastAccTransaction && $lastAccTransaction['x'] !== ($today = now()->format('Y-m-d'))) {
            $accTransactions->push([
                'y' => $lastAccTransaction['y'],
                'x' => $today
            ]);
        }

        if ($lastBalanceTransaction && $lastBalanceTransaction['x'] !== ($today = now()->format('Y-m-d'))) {
            $balanceTransactions->push([
                'y' => $lastBalanceTransaction['y'],
                'x' => $today
            ]);
        }

        if ($lastInvTransaction && $lastInvTransaction['x'] !== ($today = now()->format('Y-m-d'))) {
            $invTransactions->push([
                'y' => $lastInvTransaction['y'],
                'x' => $today
            ]);
        }

        $responseDate = [
            'balance_ledgers' => $balanceTransactions,
            'account_value' => $accTransactions,
            'inv_ledgers' => $invTransactions,
            'labels' => $accTransactions->pluck('x')
                ->merge($balanceTransactions->pluck('x'))
                ->merge($invTransactions->pluck('x'))
                ->unique()
                ->sortBy(fn ($date) => Carbon::createFromFormat('Y-m-d', $date)->timestamp)
                ->values()
        ] ;

        return $responseDate;
    }

    public function getStatistics($user)
    {

        $wallet = DB::table('wallets')
            ->where('holder_type', User::class)
            ->where('slug', WalletType::Investor)
            ->where('holder_id', $user->id)
            ->select('balance', 'id')
            ->first();

        $listingInvestors = DB::table('listing_investor')
            ->where('investor_id', $user->id)
            ->join('listings', 'listing_investor.listing_id', '=', 'listings.id')
            ->select(
                'listing_investor.listing_id',
                'listing_investor.invested_shares',
                'listing_investor.invested_amount',
                'listing_investor.currency',
                'listings.target',
                'listings.total_shares',
            )
            ->get();

        $listingInvestors = json_decode($listingInvestors, true);

        $listingIds = array_column($listingInvestors, 'listing_id');

        $userId = $user->id;

        $payouts = DB::table('payouts')
            ->whereIn('payouts.listing_id', $listingIds)
            ->where('payouts.type', 'payout')
            ->join('investor_payout', function ($join) use ($userId) {
                $join->on('payouts.id', '=', 'investor_payout.payout_id')
                    ->where('investor_payout.investor_id', $userId)
                    ->where('investor_payout.paid', '>', 0);
            })
            ->select('payouts.id', 'payouts.listing_id', 'investor_payout.paid')
            ->get();

        $payouts = json_decode($payouts, true);

        $listingValuations = DB::table('listing_valuations')
            ->whereIn('listing_id', $listingIds)
            ->whereIn('id', function ($query) {
                $query->selectRaw('max(listing_valuations.id)')->from('listing_valuations')->groupBy('listing_id');
            })
            ->select('id', 'listing_id', 'value')
            ->get();
        $listingValuations = json_decode($listingValuations, true);


        $balance = money($wallet->balance);

        $unrealizedGain = money(0);
        $totalInvestedAmount = money(0);
        $totalInvestmentForDividends = money(0);
        $totalPayout = money(0);

        foreach ($listingInvestors as $listingInvestor) {

            $listing_id = $listingInvestor['listing_id'];

            $listingPayout = Arr::where($payouts, function ($value, $key) use ($listing_id) {
                return $value['listing_id'] == $listing_id;
            });

            $listingPayout = Arr::first($listingPayout);

            $listingValuation = Arr::where($listingValuations, function ($value, $key) use ($listing_id) {
                return $value['listing_id'] == $listing_id;
            });


            $listingValuation = Arr::first($listingValuation);

            $sharePrice = money($listingInvestor['target'], defaultCurrency())->divide($listingInvestor['total_shares']);

            if (!empty($listingValuation) and empty($listingPayout)) {
                $unrealizedGain = $unrealizedGain->add(newUnrealizedPL($listingInvestor['invested_shares'], $sharePrice, money($listingValuation['value'])));
            }

            if (empty($listingPayout)) {
                $totalInvestedAmount = $totalInvestedAmount->add(money($listingInvestor['invested_amount'], $listingInvestor['currency']));
            }

            if (!empty($listingPayout)) {
                $totalInvestmentForDividends = $totalInvestmentForDividends->add(money($listingInvestor['invested_amount'], $listingInvestor['currency']));
                $paid = money($listingPayout['paid']);
                $totalPayout = $totalPayout->add($paid);
            }
        }

        $balance_fmt = money($wallet->balance)->formatByDecimal();
        $responseData = [
            'total_account_value' =>
                number_format($totalInvestedAmount->add($balance, $unrealizedGain)->formatByDecimal(), 2)
            ,
            'total_investments' =>  number_format($totalInvestedAmount->formatByDecimal(), 2),
            'dividends' => $this->dividends($totalPayout, $totalInvestmentForDividends, $userId, $listingIds),
            'balance_fmt' => number_format($balance_fmt, 2),
            'unrealized_gain' => 0, //  cleanDecimal(number_format($unrealizedGain->formatByDecimal(), 2)),
        ];

        return $responseData;

        //  return response()->json(['data' => $responseData]);
    }


    public function newGetStatistic($user)
    {

        $wallet = DB::table('wallets')
            ->where('holder_type', User::class)
            ->where('slug', WalletType::Investor)
            ->where('holder_id', $user->id)
            ->select('balance', 'id', 'is_overview_statistics_consistent')
            ->first();

        if ($wallet->is_overview_statistics_consistent) {
            $data=$this->getOverviewStatisticsData($user->id, $wallet);
            if ($data) {
                return $data;
            }
        }



        $listingInvestors = DB::table('listing_investor')
            ->where('investor_id', $user->id)
            ->join('listings', 'listing_investor.listing_id', '=', 'listings.id')
            ->select(
                'listing_investor.listing_id',
                'listing_investor.invested_shares',
                'listing_investor.invested_amount',
                'listing_investor.currency',
                'listings.target',
                'listing_investor.paid_payout',
                'listings.valuation_unit',
                'listings.total_shares',
            )
            ->get();

        $listingInvestors = json_decode($listingInvestors, true);

        $listingIds = array_column($listingInvestors, 'listing_id');

        $userId = $user->id;


        //        $payouts = DB::table('payouts')
        //            ->whereIn('payouts.listing_id', $listingIds)
        //            ->where('payouts.type', 'payout')
        //            ->join('investor_payout', function ($join) use ($userId) {
        //                $join->on('payouts.id', '=', 'investor_payout.payout_id')
        //                    ->where('investor_payout.investor_id', $userId)
        //                    ->where('investor_payout.paid','>', 0);
        //            })
        //            ->select('payouts.id', 'payouts.listing_id', 'investor_payout.paid')
        //            ->get();
        //
        //        $payouts = json_decode($payouts, true);


        $balance = money($wallet->balance);

        $unrealizedGain = money(0);
        $totalInvestedAmount = money(0);
        $totalInvestmentForDividends = money(0);
        $totalPayout = money(0);

        foreach ($listingInvestors as $listingInvestor) {

            $listing_id = $listingInvestor['listing_id'];

            //            $listingPayout = Arr::where($payouts, function ($value, $key) use ($listing_id) {
            //                return $value['listing_id'] == $listing_id;
            //            });

            //            $listingPayout = Arr::first($listingPayout);


            $sharePrice = money($listingInvestor['target'], defaultCurrency())->divide($listingInvestor['total_shares']);

            if ($listingInvestor['valuation_unit'] != null and $listingInvestor['paid_payout'] == 0) {
                $unrealizedGain = $unrealizedGain->add(newUnrealizedPL($listingInvestor['invested_shares'], $sharePrice, money($listingInvestor['valuation_unit'])));
            }

            if ($listingInvestor['paid_payout'] == 0) {
                $totalInvestedAmount = $totalInvestedAmount->add(money($listingInvestor['invested_amount'], $listingInvestor['currency']));
            }

            if ($listingInvestor['paid_payout'] > 0) {
                $totalInvestmentForDividends = $totalInvestmentForDividends->add(money($listingInvestor['invested_amount'], $listingInvestor['currency']));
                $paid = money($listingInvestor['paid_payout']);
                $totalPayout = $totalPayout->add($paid);
            }
        }


        $dividends=$this->dividends($totalPayout, $totalInvestmentForDividends, $userId, $listingIds, 'money');
        $this->saveStatistics($user, $dividends, $totalInvestedAmount, $unrealizedGain);

        $balance_fmt = money($wallet->balance)->formatByDecimal();
        $responseData = [
            'total_account_value' =>
                number_format($totalInvestedAmount->add($balance, $unrealizedGain)->formatByDecimal(), 2)
            ,
            'total_investments' =>  number_format($totalInvestedAmount->formatByDecimal(), 2),
            'dividends' => number_format($dividends->formatByDecimal(), 2),
            'balance_fmt' => number_format($balance_fmt, 2),
            'unrealized_gain' => 0, //  cleanDecimal(number_format($unrealizedGain->formatByDecimal(), 2)),
        ];

        return $responseData;

        //  return response()->json(['data' => $responseData]);
    }







    public function getOverviewStatisticsData($user_id, $wallet)
    {
        $statistics=OverviewStatistic::where('user_id', $user_id)->first();

        if (!$statistics) {
            return false;
        }

        $totalInvestedAmount=money($statistics->total_investments);

        return  [
            'total_account_value' =>number_format($totalInvestedAmount->add(money($wallet->balance), money($statistics->unrealized_gain))->formatByDecimal(), 2),
            'total_investments' =>  number_format($totalInvestedAmount->formatByDecimal(), 2),
            'dividends' =>number_format(money($statistics->dividends)->formatByDecimal(), 2),
            'balance_fmt' =>number_format(money($wallet->balance)->formatByDecimal(), 2),
            'unrealized_gain' => 0,
            'from_aggregate'=>false
        ];

    }


    public function saveStatistics($user, $dividends, $totalInvestedAmount, $unrealizedGain)
    {
        $data=[
            'dividends'=>(int)strval(str_replace('.', '', $dividends->formatByDecimal())),
            'total_investments'=>(int)strval(str_replace('.', '', $totalInvestedAmount->formatByDecimal())),
            'unrealized_gain'=>(int)strval(str_replace('.', '', $unrealizedGain->formatByDecimal()))
        ];

        OverviewStatistic::updateOrCreate(['user_id'=>$user->id], $data);

        DB::table('wallets')
            ->where('holder_type', User::class)
            ->where('slug', WalletType::Investor)
            ->where('holder_id', $user->id)
            ->update(['is_overview_statistics_consistent'=>true]);


    }
    public function calGains($item, $unrealizedGain)
    {

        $listingValuation = $item->valuation_unit;

        $sharePrice = money($item->target, defaultCurrency())->divide($item->total_shares);

        if ($listingValuation != null) {
            $unrealizedGain = $unrealizedGain->add(newUnrealizedPL($item->invested_shares, $sharePrice, money($listingValuation)));
        }


        return $unrealizedGain;
    }


    public function dividends($totalPayout, $totalInvestedAmount, $userId, $listingIds, $return_type='number_format')
    {

        $distributions = DB::table('investor_payout')
            ->where('investor_payout.investor_id', $userId)
            ->whereIn('payout_id', function ($query) use ($listingIds) {
                $query->select('id')
                    ->from('payouts')
                    ->whereIn('payouts.listing_id', $listingIds)
                    ->where('payouts.type', 'distributed');
            })->sum('paid');

        $dividends = $totalPayout->subtract($totalInvestedAmount);

        $distributions = money($distributions);

        $dividends = $dividends->add($distributions);

        if ($return_type != 'number_format') {
            return $dividends;
        }

        return  number_format($dividends->formatByDecimal(), 2);
    }


    public function checkListingHasPayout($listingId, $userId)
    {

        $payouts = DB::table('payouts')
            ->where('payouts.listing_id', $listingId->id)
            ->where('payouts.type', 'payout')
            ->join('investor_payout', function ($join) use ($userId) {
                $join->on('payouts.id', '=', 'investor_payout.payout_id')
                    ->where('investor_payout.investor_id', $userId->id)
                    ->where('investor_payout.paid', '>', 0);
            })->count();

        return $payouts;

        //        dd($payouts, $userId);

        //       $payouts = $listing->payouts()->where('type', 'payout')->get();

        //       foreach ($payouts as $payout) {
        //
        //           $investor = $payout->investors()
        //               ->where('users.id', $user->id)
        //               ->wherePivot('paid', '>', 0)
        //               ->first();
        //
        //           if ($investor) {
        //               return true;
        //           }
        //       }

        //       return false;
    }
}
