<?php

namespace App\Models\Accounting;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vat extends AppModel
{
    use SoftDeletes;
}
