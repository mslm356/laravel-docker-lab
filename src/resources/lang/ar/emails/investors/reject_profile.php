<?php

return [
    'subject' => 'عدم الموافقة على الحساب الاستثماري',
    'welcome_message' => 'مستثمرنا الكريم  :name',
    'message_1' => 'نعتذر عن عدم قبول طلبكم لفتح الحساب الاستثماري',
    'message_2' => ' وذلك بسبب أن  :reason',
    'message_3' => 'شكراً لتعاملكم مع منصة أصيل',
    'link_text' => 'عرض لوحه التحكم',
];
