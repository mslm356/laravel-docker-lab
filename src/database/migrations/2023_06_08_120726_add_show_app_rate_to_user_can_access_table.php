<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShowAppRateToUserCanAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_can_accesses', function (Blueprint $table) {
            $table->boolean('show_app_rate')->default(false)->after('unsuitability_confirmation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_can_accesses', function (Blueprint $table) {
            $table->dropColumn('show_app_rate');

        });
    }
}
