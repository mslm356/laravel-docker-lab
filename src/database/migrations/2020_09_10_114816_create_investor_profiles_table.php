<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor_profiles', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->primary();
            $table->unsignedTinyInteger('level')->nullable();
            $table->unsignedTinyInteger('education_level')->nullable();
            $table->unsignedTinyInteger('occupation')->nullable();
            $table->json('invest_objective')->nullable();
            $table->json('current_invest')->nullable();
            $table->unsignedTinyInteger('net_worth')->nullable();
            $table->unsignedTinyInteger('annual_income')->nullable();
            $table->unsignedTinyInteger('expected_invest_amount_per_opportunity')->nullable();
            $table->unsignedTinyInteger('expected_annual_invest')->nullable();
            $table->boolean('is_kyc_completed')->nullable();
            $table->boolean('is_identity_verified')->nullable();
            $table->boolean('is_on_board')->nullable();
            $table->unsignedTinyInteger('status');
            $table->unsignedTinyInteger('review_type')->nullable();
            $table->unsignedBigInteger('reviewer_id')->nullable();
            $table->text('reviewer_comment')->nullable();
            $table->json('data')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor_profiles');
    }
}
