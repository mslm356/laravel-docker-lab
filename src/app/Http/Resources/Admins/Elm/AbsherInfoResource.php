<?php

namespace App\Http\Resources\Admins\Elm;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\NationalIdentities\NationalIdentityResource;
use App\Http\Resources\Common\Elm\AbsherInfoResource as CommonAbsherInfoResource;

class AbsherInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isAbherVerified = $this->investor->is_identity_verified;

        return array_merge(
            (new CommonAbsherInfoResource($this->resource))->toArray($request),
            [
                'identity' => $this->when(
                    $isAbherVerified,
                    fn () => array_merge(
                        (new NationalIdentityResource($this->nationalIdentity))->toArray($request),
                        [
                            'first_name' => $this->getNameInBothLocales('first_name'),
                            'second_name' => $this->getNameInBothLocales('second_name'),
                            'third_name' => $this->getNameInBothLocales('third_name'),
                            'forth_name' => $this->getNameInBothLocales('forth_name'),
                        ]
                    )
                ),
            ]
        );
    }

    public function getNameInBothLocales($attribute)
    {
        return implode(' / ', $this->nationalIdentity->getTranslations($attribute));
    }
}
