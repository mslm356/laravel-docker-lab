<?php

namespace App\Http\Resources\Investors\Profile;

use App\Enums\Investors\Registration\CurrentInvestment;
use App\Enums\Investors\Registration\Education;
use App\Enums\Investors\Registration\InvestmentObjective;
use App\Enums\Investors\Registration\InvestorEntity;
use App\Enums\Investors\Registration\Level;
use App\Enums\Investors\Registration\MoneyRange;
use App\Enums\Investors\Registration\NetWorth;
use App\Enums\Investors\Registration\Occupation;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class KycResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $investor = $this->investor;

        if($investor && $investor->is_kyc_completed) {

            return [
                'education' => ['values'=>$this->mapEnumValues(Education::class),'answer'=>$investor->education_level],
                'level' => ['values'=>$this->mapEnumValues(Level::class),'answer'=>$investor->level],
                'occupation' =>['values'=>$this->mapEnumValues(Occupation::class),'answer'=>$investor->occupation],
                'investment_objectives' => ['values'=>$this->mapEnumValues(InvestmentObjective::class),'answer'=>array_map(
                    fn ($objective) => $objective,
                    ($investor->invest_objective)?$investor->invest_objective:[]
                )],
                'current_investements' => ['values'=>$this->mapEnumValues(CurrentInvestment::class),'answer'=>array_map(
                    fn ($current) => $current,
                    ($investor->current_invest)?$investor->current_invest:[]
                )],
                'net_worth' => ['values'=>$this->mapEnumValues(NetWorth::class),'answer'=>$investor->net_worth],

                'annual_income' =>['values'=>$this->mapEnumValues(MoneyRange::class),'answer'=>$investor->annual_income],
                'expected_invest_amount_per_opportunity' =>['values'=>$this->mapEnumValues(MoneyRange::class),'answer'=>$investor->expected_invest_amount_per_opportunity],
                'expected_annual_invest' =>['values'=>$this->mapEnumValues(MoneyRange::class),'answer'=>$investor->expected_annual_invest],
                'is_on_board' => ['answer'=>(int)$investor->is_on_board],
                'entity_type' => ['values'=>$this->mapEnumValues(InvestorEntity::class),'answer'=> Arr::get($investor->data, 'is_individual')],
                'any_xp_in_fin_sector' => ['answer'=>(int)Arr::get($investor->data, 'any_xp_in_fin_sector')],
                'any_xp_related_to_fin_sector' => ['answer'=>(int)Arr::get($investor->data, 'any_xp_related_to_fin_sector')],

                'have_you_invested_in_real_estates' => ['answer'=>(int)Arr::get($investor->data, 'have_you_invested_in_real_estates')],
                'investment_xp' => ['answer'=>Arr::get($investor->data, 'investment_xp')],
                'years_of_inv_in_securities' => ['answer'=>Arr::get($investor->data, 'years_of_inv_in_securities')],

                'is_entrusted_in_public_functions' => ['answer'=>(int)Arr::get($investor->data, 'is_entrusted_in_public_functions')],
                'have_relationship_with_person_in_public_functions' => ['answer'=>(int)Arr::get($investor->data, 'have_relationship_with_person_in_public_functions')],
                'is_beneficial_owner' => ['answer'=>$isOwner = (int)Arr::get($investor->data, 'is_beneficial_owner')],
                'identity_of_beneficial_owner' => ['answer'=>$this->when(
                    $isOwner === 0,
                    fn () => Arr::get($investor->data, 'identity_of_beneficial_owner')
                )],
                'extra_financial_information' => ['answer'=>Arr::get($investor->data, 'extra_financial_information')],
            ];
        }


        return $this->ifNotInvestorPresent();


    }

    public function ifNotInvestorPresent()
    {
        return [
            'education' => ['values'=>$this->mapEnumValues(Education::class),'answer'=>null],
            'level' => ['values'=>$this->mapEnumValues(Level::class),'answer'=>null],
            'occupation' =>['values'=>$this->mapEnumValues(Occupation::class),'answer'=>null],
            'investment_objectives' => ['values'=>$this->mapEnumValues(InvestmentObjective::class),'answer'=>[]],
            'current_investements' => ['values'=>$this->mapEnumValues(CurrentInvestment::class),'answer'=>[]],
            'net_worth' => ['values'=>$this->mapEnumValues(NetWorth::class),'answer'=>null],

            'annual_income' =>['values'=>$this->mapEnumValues(MoneyRange::class),'answer'=>null],
            'expected_invest_amount_per_opportunity' =>['values'=>$this->mapEnumValues(MoneyRange::class),'answer'=>null],
            'expected_annual_invest' =>['values'=>$this->mapEnumValues(MoneyRange::class),'answer'=>null],
            'is_on_board' => ['answer'=>null],
            'entity_type' => ['values'=>$this->mapEnumValues(InvestorEntity::class),'answer'=> null],
            'any_xp_in_fin_sector' => ['answer'=>null],
            'any_xp_related_to_fin_sector' => ['answer'=>null],

            'have_you_invested_in_real_estates' => ['answer'=>null],
            'investment_xp' => ['answer'=>null],
            'years_of_inv_in_securities' => ['answer'=>null],

            'is_entrusted_in_public_functions' => ['answer'=>null],
            'have_relationship_with_person_in_public_functions' => ['answer'=>null],
            'is_beneficial_owner' => ['answer'=>$isOwner = null],
            'identity_of_beneficial_owner' => ['answer'=>null],
            'extra_financial_information' => ['answer'=>null],
        ];
    }

    // todo this will moved to shared service if old version of kyc option still applied
    public function mapEnumValues($class)
    {
        try {
            $alert_messages = $class::getAlertMessages();
        } catch (\Exception $exception) {
            $alert_messages=[];
        }


        return array_values(
            array_map(
                function ($value) use ($class, $alert_messages) {
                    return [
                        'name' => $class::getDescription($value),
                        'key' => $value,
                        'alert'=>isset($alert_messages[$value])?$alert_messages[$value]:null
                    ];
                },
                $class::asArray()
            )
        );
    }


}
