<?php

namespace App\Console\Commands\Anb;

use App\Jobs\Anb\AnbEodStatement;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Http\HttpException;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class GetSpecialEodStmt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:get-special-eod {--date=} {--page=} {--order=} {--take=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get PAYMENT ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $response = AnbApiUtil::endOfDayStatements(
                $this->option('date'), $this->option('page'), $this->option('order'), $this->option('take')
            );

            foreach ($response['data'] as $statementResponse) {
                $eodStatement = AnbApiUtil::endOfDayStatement($statementResponse['id']);
                dump($eodStatement);
            }

        } catch (HttpException $e) {
            dd($e->getResponse()->json());
        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }

    }
}
