<?php

namespace App\Jobs\Anb;

use App\Models\Anb\AnbTransfer;
use Illuminate\Bus\Queueable;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Enums\AnbPaymentStatus;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class CheckAnbPaymentsStatus implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        //        dd(1);
        DB::transaction(function () {
            AnbTransfer::whereNotNull('external_id')
                ->whereIn('processing_status', [
                    AnbPaymentStatus::INITIATED, AnbPaymentStatus::QUEUED, AnbPaymentStatus::PENDING
                ])
                ->lazyById()
                ->each(function ($item) {
                    $transfer = AnbTransfer::find($item->id);

                    $payment = AnbApiUtil::payment($transfer->external_id);

                    if ($payment['status'] === $transfer->processing_status) {
                        return;
                    }

                    $transfer->processing_status = $payment['status'];

                    if (in_array($payment['status'], AnbPaymentStatus::canceledOrFailedStatuses())) {
                        $transfer->data = array_merge(
                            $transfer->data,
                            [
                                'failure_reason' => $payment['reasonOfFailure'] ?? null,
                            ]
                        );
                    }

                    $transfer->save();
                });
        });
    }
}
