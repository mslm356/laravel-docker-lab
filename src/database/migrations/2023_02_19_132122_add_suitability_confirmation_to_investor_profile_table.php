<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSuitabilityConfirmationToInvestorProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->boolean('unsuitability_confirmation')->default(0)->after('is_kyc_completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_profile', function (Blueprint $table) {
            //
        });
    }
}
