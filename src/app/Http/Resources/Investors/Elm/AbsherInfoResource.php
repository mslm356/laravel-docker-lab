<?php

namespace App\Http\Resources\Investors\Elm;

use App\Http\Resources\Common\Elm\AbsherInfoResource as CommonAbsherInfoResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AbsherInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonAbsherInfoResource($this->resource))->toArray($request),
            []
        );
    }
}
