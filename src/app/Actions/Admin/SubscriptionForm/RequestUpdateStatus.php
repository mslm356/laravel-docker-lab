<?php

namespace App\Actions\Admin\SubscriptionForm;

use Carbon\Carbon;
use App\Traits\SendMessage;
use App\Services\LogService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use App\Traits\NotificationServices;
use Illuminate\Support\Facades\Mail;
use App\Models\SubscriptionFormRequest;
use App\Jobs\RequestStmt\DeleteOldFiles;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Support\PdfGenerator\PdfGenerator;
use App\Enums\SubscriptionForm\RequestStatus;
use App\Mail\SubscriptionForm\ApproveRequest;
use App\Enums\Notifications\NotificationTypes;
use App\Actions\Investors\SubscriptionForm\RenderAllSubscriptionForm;
use App\Actions\Investors\SubscriptionForm\GetAllSubscriptionFormData;
use Illuminate\Support\Str;

class RequestUpdateStatus
{
    use AsAction, SendMessage, NotificationServices;

    public function handle($user, $data, $subscriptionFormRequest)
    {

        //        if ($data['status'] == RequestStatus::Rejected) {
        //
        //            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
        //                Mail::to($user)->queue(new RejectRequest($subscriptionFormRequest, $user));
        //            }
        //
        //            $this->sendRejectionSms($user);
        //            $this->send(NotificationTypes::SubscriptionForm, $this->rejectNotificationData($subscriptionFormRequest), $user->id);
        //        }

        $subscriptionFormRequest->status = $data['status'];
        $subscriptionFormRequest->reviewer_comment = $data['reviewer_comment'];
        $subscriptionFormRequest->reviewer_id = $data['reviewer_id'];
        $subscriptionFormRequest->save();
        $filePassword =  Str::random(8);

        if ($data['status'] == RequestStatus::Approved) {
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                $filePath = $this->generateRequestFile($subscriptionFormRequest, $user, $filePassword);
                Mail::to($user)->queue(new ApproveRequest($user, $filePath));
                DeleteOldFiles::dispatch($filePath)->delay(Carbon::now()->addMinutes(5));
            }
            $this->sendApprovedSms($user, $filePassword);
            $this->send(NotificationTypes::SubscriptionForm, $this->approveNotificationData($subscriptionFormRequest), $user->id);
        }

        return true;
    }

    public function sendRejectionSms($user)
    {
        $name = $user ? $user->full_name : '---';
        $content_ar_1 = "  تم تلقي طلبكم لإضافة حسابكم البنكي في منصة أصيل إلى المحفظة الشخصية سوف يتم تبليغكم في حال الرد من قبل الفريق المختص شكراً لتعاملك مع منصة أصيل المالية ";
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar_1;
        $content_en = "Our dear investor " . $name . " Your request to add your bank account has been received, and the team will notify you once it is reviewed Thank you for dealing with Aseel platform";
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }

    public function sendApprovedSms($user, $filePassword)
    {
        $name = $user ? $user->full_name : '---';
        $content_ar_1 = '';
        $content_en = '';

        if(config('app.add_password_to_subscription_form_file')) {
            $content_ar_1 = " '$filePassword' تم اصدار كشف عمليات الاستثمار في منصة أصيل وتم ارساله الى البريد الإلكتروني المسجل لدينا الرقم السري لي المف كشف الحساب هو  ";
            $content_en = "Our dear investor " . $name . " We have issued your account statement, and it has been sent to the email registered with us, the file password is '$filePassword'.";
        } else {
            $content_ar_1 = "تم اصدار كشف عمليات الاستثمار في منصة أصيل وتم ارساله الى البريد الإلكتروني المسجل لدينا";
            $content_en = "Our dear investor " . $name . " We have issued your account statement, and it has been sent to the email registered with us.";
        }

        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar_1;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }

    public function approveNotificationData($subscriptionFormRequest)
    {
        return [
            'action_id' => $subscriptionFormRequest->id,
            'channel' => 'mobile',
            'title_en' => 'Aseel',
            'title_ar' => 'منصة أصيل',
            'content_en' => 'We have issued your account statement, and it has been sent to the email registered with us.',
            'content_ar' => 'تم اصدار كشف عمليات الاستثمار في منصة أصيل وتم ارساله الى البريد الإلكتروني المسجل لدينا.',
        ];
    }

    public function rejectNotificationData($subscriptionFormRequest)
    {
        return [
            'action_id' => $subscriptionFormRequest->id,
            'channel' => 'mobile',
            'title_en' => 'Approve Subscription Form Request',
            'title_ar' => 'رفض طلب نموذج اشتراك',
            'content_en' => 'An opportunity to invest in the platform has been presented',
            'content_ar' => 'تم رفض طلب نموذج الاشتراك في المنصة',
        ];
    }

    public function generateRequestFile($subscriptionFormRequest, $user, $filePassword)
    {

        $sumOfInvestedAmount = money(
            DB::table('listing_investor')
                ->where('investor_id', $user->id)
                ->sum('invested_amount'),
            defaultCurrency()
        )->formatByDecimal();

        $subscriptionFormData = GetAllSubscriptionFormData::run($user, $sumOfInvestedAmount, $subscriptionFormRequest);

        $todayDate = now('Asia/Riyadh')->format('Y-m-d');

        $fileName = "{$todayDate}-{$user->id}-{$subscriptionFormRequest->id}-subscriptionForm.pdf";

        $targetPath = "stmt-requests/" . $fileName;

        $pdf = Pdf::loadView('investors.new_subscription_form', ['data' => $subscriptionFormData]);

        if(config('app.add_password_to_subscription_form_file')) {
            $pdf->setEncryption($filePassword);
        }

        $pdf->save($targetPath, 'oci');

        return $targetPath;
    }

    public function generateRequestJson(SubscriptionFormRequest $subscriptionFormRequest)
    {
        try {
            $user = $subscriptionFormRequest->user;
            $sumOfInvestedAmount = money(
                DB::table('listing_investor')
                    ->where('investor_id', $user->id)
                    ->sum('invested_amount'),
                defaultCurrency()
            )->formatByDecimal();

            $subscriptionFormData = GetAllSubscriptionFormData::run($user, $sumOfInvestedAmount, $subscriptionFormRequest, true);
            return apiResponse(200, '', $subscriptionFormData);
        } catch (\Exception $e) {
            $logService = new LogService();
            $code = $logService->log('SEND-SUBSCRIPTION-FORM-WEB', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }
}
