<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'anb' => [
        'client_id' => env('ANB_CLIENT_ID'),
        'secret' => env('ANB_SECRET'),
        'api_url' => env('ANB_API_URL'),
        'base_api_url' => env('ANB_BASE_API_URL', env('ANB_API_URL')),
        'original_host' => env('ANB_ORIGINAL_HOST'),
        'account_number' => env('ANB_ACCOUNT_NUMBER'),
        'account_prefix' => env('ANB_ACCOUNT_PREFIX')
    ],
    'nabaa' => [
        'url' => env('NABAA_URL', 'https://api.absher.sa/AbsherSmsNotification?wsdl'),
        'client_id' => env('NABAA_CLIENT_ID'),
        'client_auth' => env('NABAA_ClIENT_AUTH'),
        'sms_verification_template' => env('NABAA_SMS_VERIFICATION_TEMPLATE'),
        'sms_verification_code_length' => env('NABAA_SMS_VERIFICATION_CODE_LENGTH'),
        'cert_password' => env('NABAA_CERT_PASSWORD'),
        'mode' => env('NABAA_MODE', 'nabaa')
    ],
    'yaqeen' => [
        'url' => env('YAQEEN_URL', 'https://api.absher.sa/AbsherSmsNotification?wsdl'),
        'user_name' => env('YAQEEN_USER_NAME'),
        'password' => env('YAQEEN_PASSWORD'),
        'charge_code' => env('YAQEEN_CHARGE_CODE'),
        'mode' => env('YAQEEN_MODE', 'fake')
    ],
    'absher_otp' => [
        'url' => env('ABSHER_OTP_URL', 'https://otp.absher.sa/AbsherOTPService?wsdl'),
        'client_id' => env('ABSHER_OTP_CLIENT_ID'),
        'client_auth' => env('ABSHER_OTP_CLIENT_AUTH'),
        'mode' => env('ABSHER_OTP_MODE', 'fake'),
        'taqniyat_mode' => env('TAQNIYAT_MODE', 'active'),
        'template' => [
            'id' => env('ABSHER_OTP_TEMPLATE_ID'),
            'params' => [
                'contact' => env('ABSHER_OTP_TEMPLATE_PARAM_CONTACT'),
            ]
        ],
    ],
];
