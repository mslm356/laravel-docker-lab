<?php

namespace App\Support\Banking\MT940;

use Jejik\MT940\Parser\AbstractParser;

class AnbPullParser extends AbstractParser
{
    public function accept(string $text): bool
    {
        return true;
    }

    public function getAllowedBLZ(): array
    {
        return [];
    }
}
