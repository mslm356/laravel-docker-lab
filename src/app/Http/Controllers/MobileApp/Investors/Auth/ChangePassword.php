<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Services\LogService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ChangePassword extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    /**
     * Show Investor Profile.
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = getAuthUser('api');
        // $user = $request->user();

        try {

            $rules = [
                'old_password' => ['required', 'string'],
                'password' => ['required', 'string', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/', 'confirmed'],
            ];

            $data = $this->validate($request, $rules);

            if (!Hash::check($data['old_password'], $user->password)) {
                return apiResponse(400, __('mobile_app/message.old_password_not_valid'));
            }

            $user->password = Hash::make($data['password']);
            $user->save();

            return apiResponse(200, __('done.successfully'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-CHANGE-PASSWORD', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage(). ' : '.$code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('CHANGE-PASSWORD', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later'. ' : '.$code));
        }
    }
}
