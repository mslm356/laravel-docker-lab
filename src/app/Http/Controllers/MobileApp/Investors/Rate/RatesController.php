<?php

namespace App\Http\Controllers\MobileApp\Investors\Rate;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Services\LogService;
use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;

class RatesController extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();

    }

    public function store(Request $request)
    {
        try {

            $this->validate($request, [
                'rate' => ['required','numeric', 'integer','min:0','max:5'],
                'comment' => ['nullable', 'string', 'max:500',textValidation()],
            ]);

            $user = getAuthUser('api');


            $repeatedRate = Rate::where('user_id', $user->id)->first();
            if($repeatedRate) {
                return apiResponse(400, __('messages.repeated_rate'));
            }

            $data = $request->all();
            $data['user_id'] = $user->id;

            Rate::create($data);

            return apiResponse(200, __('mobile_app/message.created_successfully'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-STORE-NEW-RATE', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);

        } catch (\Exception $e) {
            $code = $this->logService->log('STORE-NEW-RATE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }
}
