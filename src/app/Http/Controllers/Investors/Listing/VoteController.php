<?php

namespace App\Http\Controllers\Investors\Listing;

use App\Enums\Listings\VoteAnswers;
use App\Http\Controllers\Controller;
use App\Models\InvestorVote;
use App\Models\Listings\Listing;
use App\Models\Vote;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class VoteController extends Controller
{

    public $voteServices;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function store(Request $request)
    {
        try {

            $data = $this->validator($request->all())->validate();

            $auth = $request->user('sanctum');

            $vote = Vote::find($request['vote_id']);

            $fundInvestments = $auth->investments()->where('listing_id', $vote->fund_id)->count();

            if (!$fundInvestments) {
                return apiResponse(400, __('messages.have_not_investments'));
            }

            $repeatedVote = InvestorVote::where('vote_id', $vote->id)->where('investor_id', $auth->id)->first();

            if ($repeatedVote) {
                return apiResponse(400, __('messages.repeated_vote'));
            }

            $endedVote = Vote::where('id', $request['vote_id'])->where('status', 1)->first();

            if ($endedVote) {
                return apiResponse(400, __('messages.voting_ended'));
            }

            $data['investor_id'] = $auth->id;

            InvestorVote::create($data);

            return apiResponse(200, __('messages.thanks_for_voting'));

        } catch (ValidationException $e) {

            $errors = handleValidationErrors($e->errors());
            $code = $this->logService->log('ValidationException-store-votes', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);

        } catch (\Exception $e) {
            $code = $this->logService->log('store-vote-answers', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function validator($data)
    {
        return Validator::make($data, [
            'vote_id' => ['required', 'integer', 'exists:votes,id'],
            'answer' => ['required', 'integer', 'in:1,2,3'],
        ]);
    }

    public function getListingVotes(Request $request, Listing $listing)
    {
        try {

            $auth = getAuthUser('web');

            $votes = Vote::query()
                ->with(['InvestorVotes' => function ($query) use ($auth) {
                    $query->where('investor_id', $auth->id);
                }])
                ->where('fund_id', $listing->id)
                ->orderBy('id', 'desc')
                ->select(['id', 'question'])
                ->get();

            $data = [];

            foreach ($votes as $vote) {

                $userVote = $vote->InvestorVotes->first();

                $data[] = [
                    'id' => $vote->id,
                    'vote' => $vote->question,
                    'answer' => $this->getAnswerValue($vote, $userVote),
                    'answerOptions' => $this->getAnswerOptionValue($vote, $userVote),
                ];
            }

            return apiResponse(200, __('mobile_app/message.success'), $data);

        } catch (\Exception $e) {
            $code = $this->logService->log('investor-votes', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function getAnswerValue($vote, $userVote)
    {
        if ($vote->status == 1 && !$userVote) {
            return  __('messages.voting_ended');
        }

        if ($vote->status == 1) {
            return $userVote ? $userVote->vote_answer : __('messages.refrain');
        }

        if ($vote->status == 0) {
            return $userVote ? $userVote->vote_answer : null;
        }
    }

    public function getAnswerOptionValue($vote, $userVote)
    {

        if ($vote->status == 1 || $userVote || ($vote->status == 0 && $userVote)) {
            return null;
        } else {
            return $this->getAnswerOptions();
        }
    }

    public function getAnswerOptions()
    {
        return [
            [
                'label' => __('messages.approved'),
                'value' => VoteAnswers::Approved
            ],
            [
                'label' => __('messages.rejected'),
                'value' => VoteAnswers::Rejected
            ]
        ];
    }
}
