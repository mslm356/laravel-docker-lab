<?php

namespace App\Console\Commands\Membership;

use App\Models\Users\User;
use App\Support\Users\UserAccess\ManageUserAccess;
use Illuminate\Console\Command;

class BlockUserMembership extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'block-membership {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user=User::find($this->option('user_id'));

        if (!$user)
            dd('user not found');

        $user->update(['member_ship' => 0]);

        $manageUserAccess =  new ManageUserAccess();
        $manageUserAccess->handle($user);

        $user->refresh();

        dd('done current membership is = ' . $user->member_ship);

    }
}
