<?php

namespace App\Mail\ProfessionalRequest;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectUpgradeRequest extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $reason;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $reason)
    {
        $this->user = $user;
        $this->reason = $reason;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/sign-up/profile';
        $name = $this->user ?  $this->user->full_name : '---';
        $reason = $this->reason ?  $this->reason : '---';

        return $this->view('emails.professional_request.reject_upgrade_request', compact('profileUrl', 'name', 'reason'))
            ->subject(__('emails/professional_request/reject_upgrade_request.subject'));
    }
}
