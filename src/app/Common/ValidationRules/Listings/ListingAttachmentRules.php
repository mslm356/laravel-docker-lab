<?php

namespace App\Common\ValidationRules\Listings;

use BenSampo\Enum\Rules\EnumValue;
use App\Enums\Listings\AttachmentType;

class ListingAttachmentRules
{
    static public function get()
    {
        return [
            'attachments.*.file' => ['required', 'mimes:jpg,jpeg,png,pdf,docx,doc,xls,xlsx', 'max:10240'],
            'attachments.*.type' => ['required', new EnumValue(AttachmentType::class)],
        ];
    }
}
