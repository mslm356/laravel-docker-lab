<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{ asset('/images/aseel-blue.png') }}" class="logo" alt="Aseel Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
