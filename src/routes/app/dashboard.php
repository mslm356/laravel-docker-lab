<?php

use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\Investments\OverViewTimeLineController;
use \App\Http\Controllers\MobileApp\Investors\Dashboard\DashboardsController;

Route::get('my-dashboard',[ DashboardsController::class , 'getDashboardData']);

Route::get('my-home',[ DashboardsController::class , 'getMainData'])->middleware(['jwt_check:api']);

Route::middleware(['auth:api', 'jwt_check:api', 'role:' .Role::Investor])->group(function () {

    Route::get('overview-timeline',[ OverViewTimeLineController::class , 'index']);
    Route::get('dashboard-overview-statistics',[ OverViewTimeLineController::class , 'newGetStatistic']);
    Route::get('v2/dashboard-overview-statistics',[ OverViewTimeLineController::class , 'overViewStatistics']);

});
