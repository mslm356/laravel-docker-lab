<?php

return [
    'dear' => 'Dear :name ',
    'subject' => 'Complaint :ticketNumber',
    'message' => 'You have received complaint number :ticketNumber',
];
