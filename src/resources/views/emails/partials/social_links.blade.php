
<a href="https://twitter.com/investaseel" style="text-decoration: none;">
    <img src="{{config('mail.img_domain')}}/images/social/twitter.png" style="width: 30px; height: 30px" alt="">
</a>

<a href="https://www.instagram.com/investaseel/" style="text-decoration: none;">
    <img src="{{config('mail.img_domain')}}/images/social/instagram.png" style="width: 30px; height: 30px" alt="">
</a>

<a href="https://www.linkedin.com/company/aseel/" style="text-decoration: none;">
    <img src="{{config('mail.img_domain')}}/images/social/linkedin.png" style="width: 30px; height: 30px" alt="">
</a>
