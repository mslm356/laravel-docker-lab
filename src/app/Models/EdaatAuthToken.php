<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EdaatAuthToken extends Model
{
    use HasFactory;


    protected $fillable = [
        'token',
        'response',
        'expire_in',
    ];

    protected $casts = [
        'response' => 'object',
        'expire_in' => 'datetime',
    ];

}
