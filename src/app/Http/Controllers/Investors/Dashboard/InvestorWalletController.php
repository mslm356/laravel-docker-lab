<?php

namespace App\Http\Controllers\Investors\Dashboard;

use App\Enums\Banking\WalletType;
use App\Exceptions\FailedProcessException;
use App\Exceptions\NoBalanceException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Common\TransferFeeResource;
use App\Models\BankAccount;
use App\Models\Users\User;
use App\Rules\Investors\MyActiveBankAccount;
use App\Rules\NumericString;
use App\Services\Investors\TransactionServices;
use App\Services\Investors\TransferServices;
use App\Services\LogService;
use App\Support\VirtualBanking\BankService;
use App\Support\VirtualBanking\Helpers\LocalSaudiTransferFee;
use App\Support\Zoho\JournalPrepareData\InvestorTransfer;
use Cknow\Money\Money;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class InvestorWalletController extends Controller
{

    public $transactionServices;
    public $investorTransfer;
    public $logService;
    public $transferServices;
    //    public $userAccess;

    public function __construct()
    {
        $this->investorTransfer = new InvestorTransfer();
        $this->transactionServices = new TransactionServices();
        $this->logService = new LogService();
        $this->transferServices = new TransferServices();
    }

    /**
     * Transfer Money from wallet to another wallet
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Support\VirtualBanking\BankService
     * @return \Illuminate\Http\JsonResponse
     */
    public function transfer(Request $request)
    {

        try {
            $user = $request->user('sanctum');

            $data = Validator::make($request->all(), [
                'amount' => ['required', new NumericString, 'gt:1'],
                'bank_account_id' => ['required', 'integer', new MyActiveBankAccount($user)],
                'fee' => ['required', 'numeric', Rule::in([
                    LocalSaudiTransferFee::FEE_0,
                    LocalSaudiTransferFee::FEE_0_5,
                    LocalSaudiTransferFee::FEE_1,
                    LocalSaudiTransferFee::FEE_5,
                    LocalSaudiTransferFee::FEE_7,
                ])],
            ])->validate();

            $responseOfTransfer = $this->transferServices->transferMoney($data, $user);

            $responseData = [
                'message' => trans('messages.transfer'),
                'show_request_msg' => isset($responseOfTransfer['status']) && !$responseOfTransfer['status'] ? true : false,
                'request_msg' => __('The transfer process may take between 3-5 business days'),
                'request_alert' => isset($responseOfTransfer['msg']) ? __($responseOfTransfer['msg']) : '',
            ];

            return response()->json($responseData, 200);

        } catch (NoBalanceException $e) {
            $code = $this->logService->log('NoBalanceException-TRANSFER-WEB', $e);
            return apiResponse(400, __('messages.no_enough_balance') . ' : ' . $code);
        } catch (FailedProcessException $e) {
            $code = $this->logService->log('FailedProcessException-TRANSFER-WEB', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (ValidationException $e) {
            $message =  count($e->errors()) > 0 && isset(array_values($e->errors())[0][0]) ? array_values($e->errors())[0][0] : $e->getMessage() ;
            $code = $this->logService->log('ValidationException-TRANSFER-WEB', $e);
            return response()->json(['message' => $message , 'errors' => $e->errors()], 400);
        } catch (\Exception $e) {
            $code = $this->logService->log('TRANSFER-WEB', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    /**
     * Get transfer with fee
     *
     * @param \Cknow\Money\Money $amount
     * @param string $fee
     * @return \Cknow\Money\Money
     */
    public function getTransferFinalAmount(Money $amount, string $fee)
    {
        $feeWithVat = LocalSaudiTransferFee::feeWithVat($fee);

        return $amount; //->subtract($feeWithVat);
    }

    /**
     * Get transfer options
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\Common\TransferFeeResource
     */
    public function getTransferFeeOptions(Request $request)
    {

        $data = $this->validate($request, [
            'bank_account_id' => ['required', 'integer', new MyActiveBankAccount($request->user('sanctum'))],
            'amount' => ['required', new NumericString, 'gt:1'],
        ]);

        $bankAccount = BankAccount::find($data['bank_account_id']);
        $feeOptions = (new BankService())->getTransferFeeOptions($bankAccount->bank, money_parse_by_decimal($data['amount'], defaultCurrency()));

        return TransferFeeResource::collection(collect($feeOptions));
    }

    /**
     * Virtual account balance inquire
     * @param \App\Support\VirtualBanking\BankService
     * @param string iban
     * @return \Illuminate\Http\JsonResponse
     */

    public function balanceInquiry(Request $request, $user = null)
    {
        $user = $user ? User::findOrFail($user) : $request->user('sanctum');

        $data = $this->transactionServices->getBalanceInquiry($user);

        return response()->json([
            'status' => ($data['wallet_status'])?true:false,
            'balance' => $data['total'],
            'balance_fmt' => number_format($data['total'], 2),
            'total_withdrawal' => number_format(abs(money($data['total_withdrawal'])->formatByDecimal()), 2)
        ]);
    }

    public function changeWalletStatus(Request $request, User $user)
    {
        $data = $this->validate($request, [
            'status' => ['required', 'boolean'],
        ]);

        $wallet = $user->getWallet(WalletType::Investor);

        $wallet->status=$data['status'];
        $wallet->save();

        return apiResponse(200, __('Done successfully'));
    }
}
