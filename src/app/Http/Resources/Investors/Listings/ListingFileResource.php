<?php

namespace App\Http\Resources\Investors\Listings;

use Illuminate\Http\Resources\Json\JsonResource;

class ListingFileResource extends JsonResource
{
    public $appType;

    public function __construct($resource, $appType=null)
    {
        $this->appType = $appType;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'url' => "" ,
            'get_file_url' => ($request->guard == 'api')? route('files.get.url', ['file'=> $this->id]) :route('investor.files.get.url', ['file'=> $this->id]) ,
        ];
    }
}
