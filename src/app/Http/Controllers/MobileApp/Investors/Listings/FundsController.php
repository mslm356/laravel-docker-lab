<?php

namespace App\Http\Controllers\MobileApp\Investors\Listings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Listings\ListingDetailsWithAuthResource;
use App\Http\Resources\Investors\Listings\ListingDetailsWithoutAuthResource;
use App\Http\Resources\Investors\Listings\ListingListItemResource;
use App\Http\Resources\Investors\Listings\ListingResource;
use App\Services\Investors\ListingsServices;
use App\Services\LogService;
use Illuminate\Support\Facades\Auth;

class FundsController extends Controller
{

    public $listingsServices;
    public $logService;

    public function __construct()
    {
        $this->listingsServices = new ListingsServices();
        $this->logService = new LogService();
    }

    public function allListings()
    {
        try {

            $listings = $this->listingsServices->Listings();

            $data = [];

            foreach ($listings as $listing) {
                $data[] = $this->listingsServices->listingResource($listing, $user);
            }

            return apiResponse(200, __('mobile_app/message.created_successfully'), $data);

        } catch (\Exception $e) {
            $code = $this->logService->log('SHOW-ALL-LISTING', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    public function showListing($listing)
    {
        try {
            $user = Auth::guard('sanctum')->user();
            $listing = $this->listingsServices->show($listing, $user);

            $type = 'mobile';
            return apiResponse(200, __('mobile_app/message.created_successfully'), new ListingResource($listing, $type));

        } catch (\Exception $e) {
            $code = $this->logService->log('SHOW-LISTING', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    //  New for caching
    public function allFunds()
    {
        try {
            $listings = $this->listingsServices->Listings();
            $data= ListingListItemResource::collection($listings);

            return apiResponse(200, __('mobile_app/message.created_successfully'), $data);

        } catch (\Exception $e) {
            $code = $this->logService->log('SHOW-ALL-FUNDS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    public function fundDetailsWithAuth($listing)
    {
        try {

            $listing = $this->listingsServices->show($listing);

            $type = 'mobile';

            return apiResponse(
                200,
                __('mobile_app/message.created_successfully'),
                new ListingDetailsWithAuthResource($listing, $type, 'api')
            );

        } catch (\Exception $e) {
            $code = $this->logService->log('FUND-DETAILS-AUTH', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    public function fundDetailsWithoutAuth($listing)
    {
        try {

            $listing = $this->listingsServices->show($listing, $user=null);

            return apiResponse(
                200,
                __('mobile_app/message.created_successfully'),
                new ListingDetailsWithoutAuthResource($listing, 'mobile')
            );

        } catch (\Exception $e) {
            $code = $this->logService->log('SHOW-LISTING-WITHOUT-AUTH', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }
}
