<?php

namespace App\Support\Users\UserAccess\Modules;

use App\Enums\Investors\UserAccessMessages;

class Investment
{

    public function handle($user, $module, $userDefaultData)
    {
        $response = 1;

        if ($userDefaultData) {
            $response = $this->defaultValidation($user, $userDefaultData);
        }

        return $response;
    }

    public function defaultValidation($user, $userDefaultData)
    {

        $profile = $userDefaultData['profile'];
        $nationalIdentity = $userDefaultData['nationalIdentity'];

        if (!$profile) {
            return UserAccessMessages::ProfileNotValid;
        }

        if (!$nationalIdentity || !$nationalIdentity->nin) {
            return UserAccessMessages::NinNotValid;
        }

        if (!$profile->is_kyc_completed) {
            return UserAccessMessages::KycCompleted;
        }

        if (!$profile->is_identity_verified) {
            return UserAccessMessages::IdentityVerified;
        }

        if ($profile->status != 3) {
            return UserAccessMessages::ApprovedStatus;
        }

        if ($nationalIdentity->nationality == 'الولايات المتحدة' || $nationalIdentity->nationality == 'الولايات  المتحدة') {
            return UserAccessMessages::AmericanNationalty;
        }


        if (!$this->checkSuitability($profile, $user)) {
            return UserAccessMessages::SuitabilityStatus;
        }

        return 200;
    }


    public function checkSuitability($profile, $user)
    {
        return (getKycSuitability($profile, $user) || (!getKycSuitability($profile, $user) && $profile->unsuitability_confirmation)) ? true : false;
    }
}
