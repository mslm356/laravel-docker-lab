<?php

namespace App\Models\Enquiries;

use App\Models\AppModel;
use App\Models\Users\User;
use App\Models\ContactReason;
use App\Models\Enquiries\Traits\HasTokens;
use App\Models\Enquiries\Traits\MutatesAttributes;
use Illuminate\Contracts\Translation\HasLocalePreference;

class Enquiry extends AppModel implements HasLocalePreference
{
    use MutatesAttributes, HasTokens;

    /**
     * Get all the replies under the enquiry
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(EnquiryReply::class, 'enquiry_id', 'id');
    }

    /**
     * Get the reason of contacting
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reason()
    {
        return $this->belongsTo(ContactReason::class, 'reason_id', 'id');
    }

    /**
     * Get the user who sent the enquiry
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get the user's preferred locale.
     *
     * @return string
     */
    public function preferredLocale()
    {
        return $this->user_id ? $this->user->preferredLocale() : $this->locale;
    }
}
