<?php

namespace App\Http\Resources\Admins\Listings;

use App\Http\Resources\Common\Listings\ListingValuationResource as CommonListingValuationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingValuationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingValuationResource($this->resource))->toArray($request),
            []
        );
    }
}
