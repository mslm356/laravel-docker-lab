<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use \App\Enums\HyperPay\HyperPayStatus;

class CreateHyperPayTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hyper_pay_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->string('checkout_id')->nullable();
            $table->string('payment_method')->nullable();
            $table->decimal('amount', 64, 0);
            $table->json('checkout_request')->nullable();
            $table->json('checkout_response')->nullable();
            $table->tinyInteger('status')->default(HyperPayStatus::Pending);
            $table->json('payment_status_response')->nullable();
            $table->json('webhook_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hyper_pay_transactions');
    }
}
