<?php

namespace App\Http\Controllers\Investors\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\MyInvestments;
use App\Support\QueryScoper\Scopes\Investor\MyInvestments\OrderByDateScope;
use Illuminate\Http\Request;

class GetMyInvestments extends Controller
{

    public function __invoke(Request $request)
    {
        $listing = $request->user()
            ->investments()
            ->wherePivot('invested_shares', '>', 0)
            ->with('images')
            ->toScopes(OrderByDateScope::class)
            ->get();

        return MyInvestments::collection($listing);
    }
}
