<?php

namespace App\Models\MemberShip;

use App\Enums\MemberShips\MemberShipType;
use App\Models\AppModel;
use App\Models\Users\User;

class MemberShipRequests extends AppModel
{
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getMembershipNameAttribute()
    {
        if ($this->member_ship == MemberShipType::Diamond) {
            return __("Diamond");
        } elseif ($this->member_ship == MemberShipType::Gold) {
            return __("Gold");
        } else {
            return __("Regular");
        }
    }

    public function getMembershipNameKeyAttribute()
    {
        if ($this->member_ship == MemberShipType::Diamond) {
            return 'Diamond';
        } elseif ($this->member_ship == MemberShipType::Gold) {
            return 'Gold';
        } else {
            return 'Regular';
        }
    }
}
