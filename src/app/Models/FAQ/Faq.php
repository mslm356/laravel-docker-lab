<?php

namespace App\Models\FAQ;

use App\Casts\LocalizedAttribute;
use App\Scopes\FaqOrderScope;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "faqs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_en', 'question_ar', 'answer_en', 'answer_ar', 'weight'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];


    protected $casts = [
        'question' => LocalizedAttribute::class,
        'answer' => LocalizedAttribute::class
    ];

    /**
     * Order by weight (heigher weight comes first)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return void
     */
    /*public function scopeWeight($query)
    {
        return $query->orderBy('weight', 'desc');
    }*/

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FaqOrderScope());
    }

}
