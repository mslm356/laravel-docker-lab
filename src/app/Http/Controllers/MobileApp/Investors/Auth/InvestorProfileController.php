<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Enums\Banking\VirtualAccountType;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Elm\AbsherInfoResource;
use App\Models\Users\UserCanAccess;
use App\Services\Investors\InvestorProfileServices;
use App\Services\Investors\UpgradeProfileServices;
use App\Services\LogService;
use App\Services\MemberShip\GetLastMemberShip;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class InvestorProfileController extends Controller
{

    public $investorProfileServices;
    public $logService;
    public $lastMemberShip;
    public $upgradeProfileServices;

    public function __construct()
    {
        $this->lastMemberShip = new GetLastMemberShip();
        $this->investorProfileServices = new InvestorProfileServices();
        $this->logService = new LogService();
        $this->upgradeProfileServices = new UpgradeProfileServices();
    }

    public function updateInvestorProfile(Request $request)
    {
        try {
            $data = $this->investorProfileServices->validator($request->all())->validate();
            $user = getAuthUser('api');

            if ($this->investorProfileServices->checkIqamaExpire($user)) {
                return apiResponse(400, __('mobile_app/message.iqama_date_expire'));
            }


            $data = $this->investorProfileServices->create($data, $user);
            return apiResponse(200, __('mobile_app/message.success'), ['kyc_suitability'=>getKycSuitability($user->investor, $user)]);

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            $code = $this->logService->log('ValidationException-UPDATE-INVESTOR-PROFILE', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('UPDATE-INVESTOR-PROFILE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function showMyProfile(Request $request)
    {
        try {
            $user = getAuthUser('api');
            // $user = $request->user();
            $responseData = $this->returnedProfileData($user);

            return apiResponse(200, __('mobile_app/message.success'), $responseData);

        } catch (\Exception $e) {
            $code = $this->logService->log('SHOW-INVESTOR-PROFILE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function returnedProfileData($user)
    {
        $responseData = [
            'first_name' => $user->nationalIdentity ? $user->nationalIdentity->first_name : null,
            'second_name' => $user->nationalIdentity ? $user->nationalIdentity->second_name : null,
            'third_name' => $user->nationalIdentity ? $user->nationalIdentity->third_name : null,
            'last_name' => $user->nationalIdentity ? $user->nationalIdentity->forth_name : null,
            'membership' => $this->lastMemberShip->handle($user),
            'id_expiry_date' => $user->nationalIdentity ? $user->nationalIdentity->id_expiry_date : null,
            'gender' => $user->nationalIdentity ? $user->nationalIdentity->gender_identity : null,
            'nationality' => $user->nationalIdentity ? $user->nationalIdentity->nationality : null,
            'national_id' => $user->nationalIdentity ? $user->nationalIdentity->nin : null,
            'birth_date_type' =>  $user->nationalIdentity ? $user->nationalIdentity->birth_date_type : null,
            'birth_date' =>  $user->nationalIdentity ? $user->nationalIdentity->birth_date : null,
            'email' => $user->email,
            'virtaul_account' => $this->getVirtualAccount($user),
        ];

        return $responseData;
    }

    public function getVirtualAccount($user)
    {
        $wallet = $user->getWallet(WalletType::Investor);

        if ($wallet === null) {
            return null;
        }

        $anbVirtualAccount = $wallet->virtualAccounts()->where('type', VirtualAccountType::Anb)->first();

        $user_can_access=UserCanAccess::where('user_id', $user->id)->first();

        if ($user_can_access && $user_can_access->is_kyc_completed) {
            return [
                'number' => AnbVA::toAccountNumber($anbVirtualAccount->identifier)/*.' '. __('mobile_app/message.your_investment_account_in_anp')*/,
                'iban' => $anbVirtualAccount->identifier,
            ];
        }

        return ['number'=>'****************', 'iban'=>'************************'];
    }

    public function showAbsherInfo(Request $request)
    {
        try {
            $user = getAuthUser('api');

            return apiResponse(200, __('mobile_app/message.success'), new AbsherInfoResource($user));

        } catch (\Exception $e) {
            $code = $this->logService->log('SHOW-ABSHER-INFO', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function deleteAccount(Request $request)
    {
        try {
            $user = getAuthUser('api');

            if (!$user) {
                return apiResponse(400, __('messages.user_not_fount'));
            }

            if (!$user->hasRole(Role::Investor) || !$user->nationalIdentity) {
                return apiResponse(400, __('mobile_app/message.not_permission'));
            }

            if ($user->investor && $user->investor->status == 8) {
                return apiResponse(400, __('messages.deleted_account'));
            }

            $userWallet = $user->getWallet(WalletType::Investor);
            if ($userWallet &&  $userWallet->balance != 0 )
                return apiResponse(400, __('messages.wallet_has_balance'));


            $hasInvestments = $user->investingRecords;

            if ($hasInvestments->count()) {
                return apiResponse(400, __('messages.has_investments'));
            }

            $oldPhoneNumber = optional($user->phone_number)->getRawNumber();

            $this->upgradeProfileServices->updatePhone($oldPhoneNumber, $user);

            $this->upgradeProfileServices->updateNationality(optional($user->nationalIdentity)->nin, $user);

            $this->upgradeProfileServices->updateEmail($user->email, $user);

            $this->upgradeProfileServices->updateStatus($user);

            auth('api')->logout(true);

            return apiResponse(200, __('mobile_app/message.success'));

        } catch (\Exception $e) {
            $code = $this->logService->log('DELETE-USER-ACCOUNT', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);

        }
    }


    public function confirmUnsuitability(Request $request)
    {
        $this->investorProfileServices->confirmUnsuitability(getAuthUser('api'));
        return apiResponse(200, '');
    }

}
