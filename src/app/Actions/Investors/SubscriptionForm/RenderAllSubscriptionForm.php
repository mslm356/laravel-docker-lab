<?php

namespace App\Actions\Investors\SubscriptionForm;

use App\Models\Listings\ListingInvestor;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Listings\GetCurrentInvestments;
use App\Models\Users\InvestorSubscriptionToken;

class RenderAllSubscriptionForm
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param array $data
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function handle($data)
    {
        return view('investors.new_subscription_form', compact('data'));
    }

    public function asController($subscriptionFormData)
    {
        return $this->handle($subscriptionFormData);
    }

//    public function authorize(ActionRequest $request)
//    {
//        return $request->user()->id === $request->route('token')->user_id &&
//            $request->route('token')->fund_id === $request->route('listing')->id;
//    }
}
