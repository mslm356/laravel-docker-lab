<?php

namespace App\Rules\Investors;

use App\Models\Users\User;
use App\Models\BankAccount;
use App\Enums\BankAccountStatus;
use Illuminate\Contracts\Validation\Rule;

class MyActiveBankAccount implements Rule
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return BankAccount::where('status', BankAccountStatus::Approved)
            ->where('accountable_id', $this->user->id)
            ->where('accountable_type', $this->user->getMorphClass())
            ->where('id', $value)
            ->limit(1)
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.exists');
    }
}
