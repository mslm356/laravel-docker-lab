<?php

namespace App\Http\Resources\Common;

use App\Enums\InvestorNetWorth;
use App\Enums\Investors\Registration\InvestmentObjective;
use Illuminate\Http\Resources\Json\JsonResource;

class InvestorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'investor' => $this->when($this->investor, function () {
                return array_merge($this->investor->toArray(), [
                    'net_worth' => InvestorNetWorth::getDescription($this->investor->net_worth),
                    'invest_objective' => $this->investor->more_info === null ? [] : collect(
                        $this->investor->more_info->get('invest_objective', [])
                    )
                        ->map(fn ($value) => InvestmentObjective::getDescription($value))
                ]);
            }),
        ]);
    }
}
