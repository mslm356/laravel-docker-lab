<?php

namespace App\Console\Commands\Aml;


use App\Enums\Investors\InvestorStatus;
use App\Enums\Role;
use App\Models\AML\AmlUser;
use App\Models\Investors\InvestorProfile;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Aml\AmlServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckCreateScreen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aml:check-create-screen {--customer_reference_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check users has aml';
    public $amlServices;
    public $logServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->amlServices = new AmlServices();
        $this->logServices = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            if ($this->option('customer_reference_id')) {

                $usersNotHaveAmlIds = [$this->option('customer_reference_id')];

            } else {

                $usersNotHaveAml = DB::table('investor_profiles')
//                ->where('is_kyc_completed', 1)
                    ->leftJoin('aml_users', function ($join) {
                        $join->on('investor_profiles.user_id', '=', 'aml_users.user_id');
                    })->whereNull('aml_users.user_id')
                    ->select('investor_profiles.user_id')
                    ->get();

                $usersNotHaveAmlIds = array_column(json_decode($usersNotHaveAml,1), 'user_id');

            }

            $this->createAml($usersNotHaveAmlIds);

            dump('done success');

        } catch (\Exception $e) {
            $this->logServices->log('Check-AML-EXCEPTION', $e);
            dd($e->getFile(), $e->getLine(), $e->getMessage());
        }
    }

    public function createAml ($usersNotHaveAmlIds) {

        foreach ($usersNotHaveAmlIds as $index =>  $userId) {

            $userId = (int) $userId;

            $data = [
                'type' => 'inQuery',
                'customer_reference_id' => $userId
            ];

            $response = $this->amlServices->handle($data);

            if (!is_array($response)) {
                $response = json_decode($response,1);
            }

            if (isset($response['errors'])) {
//                dump('no query result');
//                $this->logServices->log('CHECK-AML-ERROR-CONNECTING', null, ['aml user', 'RESPONSE' => $response]);
                continue;
            }

            $this->amlServices->createAmlUsers($response, $userId);

            $amlUser = DB::table('aml_users')
                ->where('user_id', $userId)
                ->select('customer_reference_id', 'query_id', 'risk_result', 'screen_result')
                ->first();

            if (!$amlUser) {
                $this->logServices->log('CHECK-AML-USER-NOT-VALID', null, ['aml user', 'aml user' => $amlUser, 'user' => $userId]);
                continue;
            }

            if ($amlUser->risk_result == "High") {

                InvestorProfile::query()
                    ->where('user_id', $userId)
                    ->update(['status' => InvestorStatus::Suspended]);

                DB::table('users')
                    ->where('id', $userId)
                    ->update(['jwt_token' => null]);
            }

            if ($amlUser->screen_result == 1 && $amlUser->risk_result) {
                $this->amlServices->approveAml($amlUser);
            }

            dump($index);
        }
    }

}
