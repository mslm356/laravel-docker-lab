<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>كشف حساب</title>
    <style>
        @include('components.print-components.watermark',
      [
      'website' => 'https://investaseel.sa' ,
      'user_name' => $data['investor_name_en'] ,
      'date'=> $data['send_request_date']
      ])
      body {
            font-family: "HelveticaNeueLT" !important;
        }

        html {
            height: 100%;
        }
    </style>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="flex flex-col gap-3 w-full max-w-4xl m-auto p-4 justify-start items-start">
<div class="m-auto text-center w-full max-w-[400px]">
    <img
        class="w-[150px] m-auto mb-[20px]"
        src="{{config('mail.img_domain')}}/images/identity/logo-dark.png"
        width="120"
    />
    <div class="text-3xl text-[#011291] font-semibold">كشف حساب</div>
    <div class="text-xl text-[#0366ff] font-medium">Account statement</div>
    <div
        style="direction: ltr"
        class="flex items-center justify-center p-3 rounded-lg bg-[#eef5ff] mt-5 gap-5"
    >
        <span class="text-lg text-black font-medium"> Date: {{$data['value_date']}} </span>
        <span class="text-lg text-black font-medium"> Time: {{$data['value_time']}} </span>
    </div>
    <div
        style="direction: ltr"
        class="flex justify-center items-center py-2 px-3 rounded-lg bg-[#011291] mt-5 gap-4"
    >
        <img class="w-[20px]" src="{{config('mail.img_domain')}}/images/identity/logo-dark.png" width="20" />
        <span class="text-lg text-white font-semibold">
          Investor Details | تفاصيل المستثمر
        </span>
    </div>
</div>

<div class="flex flex-col w-full gap-3">
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] pr-3"
        >
            <h6 class="font-semibold text-md">الإسم</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[350px]"
        >
            <h6 class="font-semibold text-md"> {{$data['investor_name_ar']}} </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">Name</h6>
        </div>
    </div>
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] pr-3"
        >
            <h6 class="font-semibold text-md">رقم الهوية</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[350px]"
        >
            <h6 class="font-semibold text-md"> {{$data['investor_national_id']}} </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">ID Number</h6>
        </div>
    </div>
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] pr-3"
        >
            <h6 class="font-semibold text-md">رصيد المحفظة</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[350px]"
        >
            <h6 class="font-semibold text-md"> {{number_format($data["wallet"],2)}} </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">Wallet Balance</h6>
        </div>
    </div>
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] pr-3"
        >
            <h6 class="font-semibold text-md">مجموع الاستثمارات</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[350px]">
            <h6 class="font-semibold text-md"> {{number_format($data['userInvestments'],2)}} </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[205px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">Total Investments</h6>
        </div>
    </div>

    <div
        style="direction: ltr"
        class="flex justify-center items-center py-2 px-3 rounded-lg bg-[#011291] mt-3 gap-3"
    >
        <img class="w-[20px]" src="{{config('mail.img_domain')}}/images/identity/logo-dark.png" width="20" />
        <span class="text-lg text-white font-semibold">
          Investments | عمليات الاستثمار
        </span>
    </div>
    <div class="flex justify-between items-center gap-3 text-white">
        <div
            class="flex flex-col justify-center w-[40px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center">#</h6>
        </div>
        <div
            class="flex flex-col justify-center w-[120px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center">
                Date / التاريخ
            </h6>
        </div>
        <div
            class="flex flex-col justify-center w-[300px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center">
                Fund Name / اسم الصندوق
            </h6>
        </div>
        <div
            class="flex flex-col justify-center w-[110px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center h-4">
                عدد الوحدات
            </h6>
            <h6 class="w-full font-semibold text-md text-center">
                No. of Units
            </h6>
        </div>
        <div
            class="flex flex-col justify-center w-[145px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center h-4">
                مبلغ الوحدات
            </h6>
            <h6 class="w-full font-semibold text-md text-center">Units Costs</h6>
        </div>
    </div>

    @if(count($data))
        @foreach($data['listingInvestors'] as $key => $value)
            <div class="flex justify-between items-center gap-3">

                <div
                    style="border: 1px solid #0366ff"
                    class="flex w-[40px] rounded-lg p-1"
                >
                    <h6 class="w-full font-semibold text-md text-center">{{ $key + 1 }}</h6>
                </div>
                <div
                    style="border: 1px solid #0366ff"
                    class="flex w-[120px] rounded-lg p-1"
                >
                    <h6 class="w-full font-semibold text-md text-center">{{\Illuminate\Support\Carbon::parse($value->created_at)->format('Y-m-d')}}</h6>
                </div>
                <div
                    style="border: 1px solid #0366ff"
                    class="flex w-[300px] rounded-lg p-1"
                >
                    <h6 class="w-full font-semibold text-md text-center">
                        {{$value->title_ar}} <br>
                        {{$value->title_en}}

                    </h6>
                </div>
                <div style="border: 1px solid #0366ff" class="flex w-[110px] rounded-lg p-1">
                    <h6 class="w-full font-semibold text-md text-center">{{$value->invested_shares}}</h6>
                </div>
                <div
                    style="border: 1px solid #0366ff"
                    class="flex w-[145px] rounded-lg p-1"
                >
                    <h6 class="w-full font-semibold text-md text-center">{{number_format($value->totalAmount , 2)}}</h6>
                </div>

            </div>
        @endforeach
    @endif

</div>
</body>
</html>
