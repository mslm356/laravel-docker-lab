<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\AlertMessgesInterface;
use App\Enums\Investors\Registration\Interfaces\DBColumInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class Education extends Enum implements LocalizedEnum,AlertMessgesInterface,DBColumInterface
{
    const Postgraduate =   1;
    const Undergraduate =   2;
    const HighSchool =   3;
    const Other =   4;

    function getDBColumnName()
    {
        return 'education_level';
    }

    function getAlertMessages()
    {
    }


}
