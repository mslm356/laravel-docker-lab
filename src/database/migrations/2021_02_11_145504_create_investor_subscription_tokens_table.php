<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorSubscriptionTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor_subscription_tokens', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->unique('inv_sub_tokens_reference_unique');
            $table->unsignedBigInteger('listing_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shares');
            $table->timestamp('expired_at')->nullable();
            $table->timestamp('authorization_expiry')->nullable();
            $table->timestamps();

            $table->foreign('listing_id')->references('id')->on('listings');
            $table->foreign('user_id')->references('id')->on('users');
        });

        DB::unprepared('ALTER TABLE investor_subscription_tokens AUTO_INCREMENT = 17558');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor_subscription_tokens');
    }
}
