<?php

namespace App\Http\Controllers\MobileApp\Investors\Sdad;

use App\Jobs\ChargeWallet;
use App\Models\EdaatInvoice;
use App\Services\Sdad\Enums\InvoiceCodesStatusesEnums;
use App\Support\Users\UserAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChargeWalletController
{
    public $userAccess;

    public function __construct()
    {
        $this->userAccess = new UserAccess();
    }

    public function create_invoice(Request $request)
    {

        return apiResponse(400, __('The service is now stopped'));

        Validator::make($request->all(), [
            'amount' => 'required|numeric|max:10000000'
        ])->validate();

        try {

            $user = getAuthUser('api');

            if ($request->amount < config('edaat.transfer_amount_limit')) {
                return apiResponse(400, __('The value must be greater than or equal to 20000'));
            }

            if (!$this->checkAttemptsCountLastMonth($user)) {
                return apiResponse(400, __('You have reached the monthly limit for the SADAD invoice (10 transactions)'));
            }

            if (!$this->limitAttempts($user)) {
                return apiResponse(400, __('You have reached the limit for the SADAD invoice (2 transactions)'));
            }

            ChargeWallet::dispatch($user, $request->all())->onQueue('sdad');

        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

        return apiResponse(200, __('messages.invoice_will_be_exported'), []);
    }

    public function limitAttempts($user)
    {

        $attemptsCount = config('edaat.limit_attempts');

        $userInvoicesCount = EdaatInvoice::query()
            ->where('user_id', $user->id)
//            ->where('status', InvoiceCodesStatusesEnums::PAID)
            ->whereDate('created_at', now()->format('Y-m-d'))
            ->count();

        return $userInvoicesCount >= $attemptsCount ? false : true;
    }

    public function checkAttemptsCountLastMonth($user)
    {
        $attemptsCount = config('edaat.limit_attempts_in_month');

        $userInvoicesCount = EdaatInvoice::where('user_id', $user->id)
            ->where('created_at', '>', now()->subDays(30)->endOfDay())
            ->where('status', InvoiceCodesStatusesEnums::EXPORT)
            ->count();

        return $userInvoicesCount > $attemptsCount ? false : true;
    }
}
