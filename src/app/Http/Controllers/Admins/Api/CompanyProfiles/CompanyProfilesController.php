<?php

namespace App\Http\Controllers\Admins\Api\CompanyProfiles;

use App\Enums\Investors\InvestorStatus;
use App\Http\Controllers\Controller;
use App\Mail\companyProfile\Approve;
use App\Mail\companyProfile\Reject;
use App\Mail\Investors\RejectProfile;
use App\Models\CompanyProfile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class CompanyProfilesController extends Controller
{

    public function approveProfile(Request $request, CompanyProfile $companyProfile)
    {
        $rules = $this->rules();

        $this->validate($request, $rules);

        try {

            DB::beginTransaction();

            $companyAccount = $companyProfile->companyAccount;

            if (!$companyAccount || !$companyAccount->investor) {
                return validationResponse(422, __('sorry, account profile not valid'));
            }

            if ($request['status'] == InvestorStatus::Approved && $companyProfile->status != InvestorStatus::Approved )
            {
                $companyAccount->investor->kyc_suitability = true;
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($companyProfile->companyAccount)->queue(new Approve());
                }
            }
            if ($request['status'] == InvestorStatus::Rejected && $companyProfile->status != InvestorStatus::Rejected )
            {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($companyProfile->companyAccount)->queue(new Reject( $companyProfile));
                }
            }

            $companyProfile->status = $request['status'];
            $companyProfile->save();

            $companyAccount->investor->status = $request['status'];
            $companyAccount->investor->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return validationResponse(422, __('sorry please try later'));
        }

        return response()->json();
    }

    public function rules()
    {
        $rules = [
            'status' => ['required', 'integer', 'in:1,4,3'],
        ];

        return $rules;
    }
}
