<?php


namespace App\Services\Investors\Users;

use App\Models\Users\User;
use Illuminate\Support\Facades\Validator;

class ForgetPasswordService
{

    public $lastForgetService;

    public function __construct()
    {
        $this->lastForgetService = new LastForgetService();
    }

    public function handle($data, $module)
    {
        $response = ['status' => true];

        $modules = ['investor', 'mobile', 'ap'];

        if (!in_array($module, $modules)) {
            return $response;
        }

        $method = $modules[$module];

        $response = $this->$method($data);

        return $response;
    }

    public function mobile($data)
    {

        Validator::make($data, [
            'phone' => ['required', 'string', 'phone:phone_country_iso2']
        ])->validate();

        $phoneNumber = phone($data['phone'], "SA");

        $responseOfLastForget = $this->lastForgetService->handle($phoneNumber, 'password');

        if (isset($responseOfLastForget['status']) && !$responseOfLastForget['status']) {
            $msg = isset($responseOfLastForget['msg']) ? $responseOfLastForget['msg'] : 'please try later';
            return apiResponse(400, __($msg));
        }

        $user = User::where('phone_number', $phoneNumber)->first();
    }

    public function investor()
    {

        dd(1);
    }

    public function ap()
    {

        dd(1);
    }


}
