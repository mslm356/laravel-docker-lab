<?php

namespace App\Enums\Elm;

use BenSampo\Enum\Enum;

final class AbsherOtpReason extends Enum
{
    const InvestingInFund = 1;
    const VerifyIdentity = 2;
    const VerifyLogin = 3;
    const ForgetPassword = 4;
}
