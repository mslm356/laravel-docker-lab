<?php

namespace App\Models\Listings;

use App\Models\File;
use App\Enums\FileType;
use App\Models\AppModel;
use App\Casts\Money\MoneyCast;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Models\Zatca\ZatcaInvoice;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ListingInvestorRecord extends AppModel
{
    use HasFactory;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'amount_without_fees' => MoneyCast::class . ':currency',
        'admin_fees' => MoneyCast::class . ':currency',
        'vat_amount' => MoneyCast::class . ':currency',
    ];

    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }

    public function zatcaInvoice()
    {
        return $this->morphOne(ZatcaInvoice::class, 'invoicable');
    }

    public function subscriptionForm()
    {
        return $this->morphOne(File::class, 'fileable')->where('type', FileType::SubscriptionForm);
    }

    public function subscriptionToken()
    {
        return $this->belongsTo(InvestorSubscriptionToken::class,'subscription_token_id');
    }

    public function investor()
    {
        return $this->belongsTo(User::class,'investor_id');
    }
}
