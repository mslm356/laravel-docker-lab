<?php

namespace App\Console\Commands;

use App\Actions\ExportSubscriptionForm;
use App\Mail\SubscriptionForm\ApproveRequest;
use App\Models\Campaigns\Campaign;
use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\WalletType;
use App\Enums\MemberShips\MemberShipType;
use App\Enums\Notifications\NotificationTypes;
use App\Enums\Role;
use App\Jobs\AbsherTest;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use App\Mail\AuthorizedEntities\Listings\CreateReport;
use App\Models\Anb\AnbTransfer;
use App\Models\Investors\InvestorProfile;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Services\Campaigns\EmailType;
use App\Services\Campaigns\InvestorTypes\ManageData;
use App\Services\Campaigns\NotificationType;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\Aml\AmlServices;
use App\Support\Banking\MT940\AnbEodBalance;
use App\Support\Banking\MT940\AnbEodParser;
use App\Support\Banking\MT940\AnbEodTransaction;
use App\Support\PdfGenerator\PdfGenerator;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Mailgun\Mailgun;
use Mailgun\Message\MessageBuilder;


use Illuminate\Support\Facades\Redis;
use Illuminate\Validation\ValidationException;
use Jejik\MT940\Reader;
use Kreait\Firebase\Factory;

class TestLog extends Command
{

    use NotificationServices, SendMessage;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'toIban {--number=} {--user_id=} {--path=}';
    public $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $code = "000.100.000";

           dd(preg_match("/^(000\.000\.|000\.200\.1|100\.[36])/", $code));

            $s = DB::table('hyper_pay_transactions')
                ->where('status', 3)
                ->get();

            dd($s);

            $s = Str::random(32);
            dd($s);

//            $amlUser = DB::table('aml_users')
//                ->where('user_id', 4)
//                ->select('customer_reference_id', 'query_id', 'risk_result', 'screen_result')
//                ->first();
//
//            $data = [
//                'type' => 'approveAml',
//            ];
//
//            $amlUser = (array) $amlUser;
//
//            $data = array_merge($data, $amlUser);
//
//           $amlServices = new AmlServices();
//
//            $response = $amlServices->handle($data);
//
//            dd($data);

            $this->testPdf(5);

            dd('done');

            $iban = AnbVA::toIban($this->option('number'));
            $vaccount = VirtualAccount::where('identifier', $iban)->first();

            $wallet = null;
            $user = null;

            if ($vaccount) {

                $wallet = $vaccount->wallet;
                $user = User::where('id', $wallet->holder_id)->first();
            }


//            dump(config('sms.SMS_RECIPIENTS'));
//            dd($this->sendOtpMessage($this->option('number'), 'test'));

//            dump(Redis::connection($this->option('number'))->keys("*"));

            $user = User::find($this->option('user_id'));

            $notificationData = $this->notificationData();
            $this->send(NotificationTypes::Membership, $notificationData, $user->id);

            dd('done');

//            $this->testPdf($this->option('number'));
//
//            $s = User::query()->whereHas('investor', function ($q) {
//                $q->where('level', 2);
//            })->orWhereHas('memberShips', function ($m) {
//                $m->whereIn('status', [1, 3]);
//            })->each(function ($user) {
//
//                $this->count++;
//
//                $notificationData = $this->notificationData();
//                $this->send(NotificationTypes::Membership, $notificationData, $user->id);
//
//            });
//
//
//            dd('done ' . $this->count);

//            $s = DB::table('users')
//                ->join('member_ship_requests', function ($join) {
//                    $join->on('member_ship_requests.user_id', '=', 'users.id');
//                })
//                ->join('investor_profiles', function ($join) {
//                    $join->on('investor_profiles.user_id', '=', 'users.id');
//                })
////                ->where('investor_profiles.level', 2)
//                ->select('users.*', 'investor_profiles.level as levels')
//                ->groupBy('users.id')
//                ->having('levels', '=', 2)
//                ->toSql();

//            dd($s);
//
//
//            $W = User::
////                ->join('national_identities', 'national_identities.user_id', '=', 'users.id')
//            join('investor_profiles', 'investor_profiles.user_id', '=', 'users.id')
//                ->where('investor_profiles.is_identity_verified', '!=', 1)
//                ->where('investor_profiles.is_kyc_completed', '!=', 1)
//                ->whereNotNull('data')
//                ->select('users.*')
//                ->groupBy('users.id')->toSql();
//
//            dd($W);

//                ->lazy(100)
//                ->each(function ($user) {
//
//                    dd($user->investor);
//
//                    $this->verifyAccount->verify($user);
//                });


//            $anb = AnbTransfer::find(23);
//
//            dd($anb->amount);


//            dd(User::class);

//           $q =  User::query()
//               ->doesntHave('bankAccounts')
//               ->join('wallets', function ($join) {
//                    $join->on('users.id', '=', 'wallets.holder_id')
//                        ->where('wallets.holder_type', User::class)
//                        ->where('balance', '>', 0);
//                })
////                ->join('bank_accounts', function ($join) {
////                    $join->on('users.id', '!=', 'bank_accounts.accountable_id')
////                        ->where('bank_accounts.accountable_type', User::class);
////                })
////               ->groupBy('users.id')
//               ->select('users.id','users.first_name', 'users.last_name', 'users.phone_number','users.email', 'wallets.balance')
//               ->get();
//
//           dd($q);


//            $listing = Listing::find(8);
//
//            dd(now('Asia/Riyadh'), $listing->start_at, Carbon::parse($listing->start_at)->timezone('Asia/Riyadh'));
//
//            dd($listing->start_at->isBefore(now()) );


//            $hijriDate = Hijri::Date('m-Y', $this->option('number'));
//
//            dd($hijriDate);

            /* $collector = User::role(Role::AnbStatementCollector)->first();

             dd($collector->getWallet(WalletType::AnbStatementCollecter));*/


//            $iban = AnbVA::toIban($this->option('number'));
//            $vaccount = VirtualAccount::where('identifier', $iban)->first();
//
//            $wallet = null;
//            $user = null;
//
//            if ($vaccount) {
//
//                $wallet = $vaccount->wallet;
//                $user = User::where('id', $wallet->holder_id)->first();
//            }
//
//
//            dd($iban, $wallet ? $wallet->balance : '', $wallet ? $wallet->id : '', $user);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }


    public function notificationData()
    {

        return [
            'title_ar' => __('membership.notify_title', [], 'ar'),
            'title_en' => __('membership.notify_title', [], 'ar'),
            'content_ar' => __('membership.notify_cmd', [], 'ar'),
            'content_en' => __('membership.notify_cmd', [], 'ar'),
            'channel' => 'mobile'
        ];
    }


    public function testPdf ($investor_id)
    {

        $profile = InvestorProfile::query()
            ->where('user_id', $investor_id)
            ->where('is_kyc_completed', 1)
            ->first();

        if (!$profile) {
            dd('profile not valid');
//            throw ValidationException::withMessages(['message' => 'profile not valid']);
        }

        $todayDate = now('Asia/Riyadh')->format('Y-m-d');

        $html = $this->html($profile)->render();

        $originalFileName = "{$todayDate}-{$profile->user_id}-kyc.pdf";

        $targetPath = "kyc-pdf/$originalFileName";

        PdfGenerator::outputFromHtml($html, $targetPath, [
            'gotoOptions' => [
                'waitUntil' => 'networkidle0'
            ]
        ]);

        $user = User::find($investor_id);
        $listing = Listing::find(11);

        Mail::to($user)->send(new \App\Mail\Funds\CreateFund($user, $listing, $targetPath));

        dd($originalFileName, $targetPath);
    }

    public function html ($profile)
    {
        return view('investors.kyc-pdf', compact('profile'));
    }

}
