<?php

namespace App\Models\VirtualBanking;

use Bavix\Wallet\Internal\MathInterface;
use Bavix\Wallet\Services\WalletService;
use Bavix\Wallet\Models\Transaction as ModelsTransaction;

class Transaction extends ModelsTransaction
{
    /**
     * @return float|int
     */
    public function getAmountFloatAttribute()
    {
        $math = app(MathInterface::class);
        $decimalPlacesValue = app(WalletService::class)
            ->getWallet($this->wallet)
            ->decimal_places;
        $decimalPlaces = $math->powTen($decimalPlacesValue);

        return $math->div($this->amount, $decimalPlaces, $decimalPlacesValue);
    }
}
