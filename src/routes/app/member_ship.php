<?php

use \App\Enums\Role;
use App\Http\Controllers\MobileApp\Investors\MemberShips\MemberShipController;


Route::middleware(['auth:api', 'jwt_check:api', 'role:' .Role::Investor])->group(function () {
    Route::get('membership-accept',[ MemberShipController::class , 'approveMemberShip']);
    Route::post('update-membership-status',[ MemberShipController::class , 'updateMembershipStatus']);
});
