<?php

namespace App\Console\Commands\Anb;

use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Http\HttpException;
use Illuminate\Console\Command;

class DeleteWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:delete-webhook {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete ANB webhook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');

        try {
            AnbApiUtil::deleteWebhook($id);
            $this->info("Webhook deleted successfully.");
        } catch (HttpException $th) {
            $this->error($th->getResponse()->body());
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
        }
    }
}
