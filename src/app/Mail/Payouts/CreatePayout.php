<?php

namespace App\Mail\Payouts;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreatePayout extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $listing;
    public $amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Listing $listing, $amount)
    {
        $this->user = $user;
        $this->listing = $listing;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';

        $listing = $this->listing;
        $amount = $this->amount;

        $profileUrl = config('app.investor_url') . '/app/overview';

        return $this->view('emails.payouts.create', compact('name', 'listing', 'profileUrl', 'amount'))
            ->subject(__('emails/payouts/create.subject'));
    }
}
