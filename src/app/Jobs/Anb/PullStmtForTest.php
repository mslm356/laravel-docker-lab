<?php

namespace App\Jobs\Anb;

use App\Enums\Banking\BankCode;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Models\Anb\AnbStatementEvent;
use App\Models\Anb\AnbStatementTransaction;
use App\Models\Anb\AnbTransfer;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Models\VirtualBanking\Wallet;
use App\Support\Anb\AnbApiUtil;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Support\Zoho\CustomerPayments;
use Bavix\Wallet\Models\Transaction;
use Carbon\CarbonImmutable;
use Cknow\Money\Money;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PullStmtForTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $zohoCustomerPayments;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->zohoCustomerPayments = new CustomerPayments();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $accountNumber = config('services.anb.account_number');

            $event = AnbStatementEvent::latest('ended_at')
                ->latest('statement_date')
                ->where([
                    'bank_code' => BankCode::Anb,
                    'account_number' => $accountNumber
                ])
                ->first();

            if ($event === null) {
                $this->pullStatementForDate($accountNumber, now(), null);
            } elseif ($event->statement_date->isBefore(now()->startOfDay()) || $event->statement_date->isSameDay(now()->startOfDay())) {
                $latestStatementDate = $event->statement_date->copy();
                $offset = $event->offset;
                do {
                    $this->pullStatementForDate($accountNumber, $latestStatementDate->copy(), $offset);
                    $latestStatementDate->addDay();
                    $offset = null;
                } while ($latestStatementDate->isBefore(now()->startOfDay()) || $latestStatementDate->isSameDay(now()->startOfDay()));
            }

        } catch (\Exception $e) {
            Log::info('pull-Statement-Anb', [$e->getMessage(), $e->getFile(), $e->getLine()]);
        }
    }

    /**
     * Pull statement for the given date
     *
     * @param string $accountNumber
     * @param \Carbon\Carbon $date
     * @param string|null $offset
     * @return void
     */
    public function pullStatementForDate($accountNumber, $date, $offset)
    {
        $event = new AnbStatementEvent([
            'id' => (string) Str::uuid(),
            'statement_date' => $date->copy()->startOfDay(),
            'account_number' => $accountNumber,
            'bank_code' => BankCode::Anb,
            'started_at' => now()
        ]);

        $msg = AnbApiUtil::statement(
            $accountNumber,
            $from = $date->copy()->timezone('Asia/Riyadh')->toDateString(),
            $from,
            null,
            $offset
        );

        if ($msg === null) {
            $event->ended_at = now();
            $event->offset = $offset;
            $event->save();
            return;
        }

        $event->ended_at = now();
        $event->offset = $msg['offset'];

        $transactions = $msg['statement']['transactions'];
        $isCompleted = $msg['completed'];

        DB::transaction(function () use ($transactions, $event) {
            $toBeUpsertedTransactions = [];

            // We cache wallets during each DB::transaction so that if multiple transactions happen to
            // the same Virtual Account, the wallet don't get locked because of the "lockForUpdate"
            $this->resetWalletsCache();

            foreach ($transactions as $anbTransaction) {
                $narr2 = $anbTransaction['narrative']['narr2'] ?? null;
                $narr3 = $anbTransaction['narrative']['narr3'] ?? null;
                $narr3Reference = $this->getNarr3Reference($narr3);
                $bankRef = $anbTransaction['refNum'] ?? null;
                $amount = $anbTransaction['amount']['amount'];
                $currency = $anbTransaction['amount']['currencyCode'];
                $money = Money::parseByDecimal($amount, $currency);

                $transaction = $this->mapTransactionForLaterUpserting($event, $anbTransaction, $money);
                $toBeUpsertedTransactions[] = $this->prepareTransactionForUpserting($transaction);

                $transactionExists = Transaction::where([
                    'external_reference' => $bankRef
                ])
                    ->whereIn('reason', [
                        TransactionReason::AnbStatement,
                        TransactionReason::RefundAfterAnbTransferFailure
                    ])
                    ->limit(1)
                    ->exists();

                if ($transactionExists) {
                    continue;
                }

                $virtualAccount = null;
                $wallet = null;
                $type = null;
                $transfer = null;

                if (
                    $narr2 && AnbVA::isVA($narr2) &&
                    ($virtualAccount = VirtualAccount::where('identifier', AnbVA::toIban($narr2))->first())
                ) {
                    $type = 'deposit';

                    $wallet = $this->findOrCacheWallet($virtualAccount->wallet_id);
                } elseif (
                    $narr3Reference &&
                    $transfer = AnbTransfer::where('external_trans_reference', $narr3Reference)->first()
                ) {
                    if ($transfer->data['is_refunded'] ?? null) {
                        continue;
                    } else {
                        $type = 'reversal';
                        $wallet = $this->findOrCacheWallet($transfer->transaction->wallet_id);
                    }
                }

                if ($wallet === null) {
                    $wallet = $this->findOrCacheCollectorWallet();
                }

                if ($money->isPositive()) {
                    if ($type === null || $type === 'deposit') {
                        $wallet->deposit(
                            $money->absolute()->getAmount(),
                            [
                                'vaccount_identifier' => $virtualAccount->identifier ?? null,
                                'vaccount' => optional($virtualAccount)->toArray(),
                                'reference' => transaction_ref(),
                                'reason' => TransactionReason::AnbStatement,
                                'details' => [
                                    'version' => 1,
                                    'data' => $anbTransaction,
                                    'source' => 'pull_statement'
                                ],
                                'external_reference' => $bankRef,
                                'value_date' => $transaction['value_date']->toDateTimeString(),
                            ]
                        );

                        $wallet->refreshBalance();

                        //                        $this->zohoDeposit($wallet, $amount);

                    } else {
                        $refundTransaction = $wallet->deposit(
                            $money->absolute()->getAmount(),
                            [
                                'reason' => TransactionReason::RefundAfterAnbTransferFailure,
                                'value_date' => now()->toDateTimeString(),
                                'reference' => transaction_ref(),
                                'transaction_id' => $transfer->transaction->id,
                                'anb_transfer_id' => $transfer->id,
                                'transaction_reference' => $transfer->transaction->reference,
                                'from_bank_code' => $transfer->to_bank,
                                'from_iban' => $transfer->to_account,
                                'external_reference' => $bankRef,
                                'value_date' => $transaction['value_date']->toDateTimeString(),
                                'details' => [
                                    'version' => 1,
                                    'data' => $anbTransaction,
                                    'source' => 'pull_statement'
                                ],
                            ]
                        );

                        $wallet->refreshBalance();

                        $transfer->data = array_merge($transfer->data, [
                            'is_refunded' => true,
                            'refund_transaction_id' => $refundTransaction->id
                        ]);

                        $transfer->save();
                    }
                }
            }

            $event->save();

            AnbStatementTransaction::upsert(
                $toBeUpsertedTransactions,
                ['reference'],
                [
                    'event_id',
                    'from_account',
                    'reference',
                    'amount',
                    'currency',
                    'value_date',
                    'post_date',
                    'narrative',
                    'extra_data'
                ]
            );
        });

        // Check if there are more recordings or the end date is not equal
        // to today's date so we want to fetch all the records up to date
        if (!$isCompleted) {
            $this->pullStatementForDate($accountNumber, $date->copy(), $event->offset);
        }
    }

    public function mapTransactionForLaterUpserting($event, $anbTransaction, $money)
    {
        $transactionDate = !isset($anbTransaction['valueDate']) ? null : CarbonImmutable::createFromFormat(
            'Y-m-d',
            $anbTransaction['valueDate'],
            'Asia/Riyadh'
        )
            ->setTime(0, 0)
            ->timezone('UTC');

        return [
            'event_id' => $event->id,
            'from_account' => $anbTransaction['srcAcctNum'] ?? null,
            'reference' => $anbTransaction['refNum'] ?? null,
            'amount' => $money->getAmount(),
            'currency' => $money->getCurrency()->getCode(),
            'value_date' => $transactionDate,
            'post_date' => !isset($anbTransaction['postDate']) || !isset($anbTransaction['postingTime']) ?
                null : CarbonImmutable::createFromFormat(
                    'Y-m-d H:i:s',
                    "{$anbTransaction['postDate']} {$anbTransaction['postingTime']}",
                    'Asia/Riyadh'
                )
                    ->setTime(0, 0)
                    ->timezone('UTC'),
            'narrative' => $anbTransaction['narrative'] ?? [],
            'extra_data' => Arr::except(
                $anbTransaction,
                [
                    'srcAcctNum', 'refNum', 'amount', 'valueDate', 'postDate', 'postingTime', 'narrative'
                ]
            )
        ];
    }

    public function prepareTransactionForUpserting($unpreparedTransaction)
    {
        $transaction = $unpreparedTransaction;

        $transaction['post_date'] = $transaction['post_date']->toDateTimeString();
        $transaction['narrative'] = json_encode($transaction['narrative']);
        $transaction['extra_data'] = json_encode($transaction['extra_data']);
        $transaction['value_date'] = $transaction['value_date']->toDateString();

        return $transaction;
    }

    public function resetWalletsCache()
    {
        $this->cachedCollectorWallet = null;
        $this->cachedWallets = [];
    }

    public function findOrCacheWallet($walletId)
    {
        if (($this->cachedWallets[$walletId] ?? null) === null) {
            $this->cachedWallets[$walletId] = Wallet::find($walletId);
        }

        return $this->cachedWallets[$walletId];
    }

    public function findOrCacheCollectorWallet()
    {
        if (!$this->cachedCollectorWallet) {
            $collector = User::role(Role::AnbStatementCollector)->first();

            $this->cachedCollectorWallet = $collector->getWallet(WalletType::AnbStatementCollecter);
        }

        return $this->cachedCollectorWallet;
    }

    public function getNarr3Reference($narr3)
    {

        if (!$narr3) {
            return null;
        }

        $narr3Data = explode('/', $narr3);

        return isset($narr3Data[0]) ? $narr3Data[0] : null;
    }

    public function zohoDeposit($wallet, $amount)
    {

        try {

            $user = User::find($wallet->holder_id);
            $this->zohoCustomerPayments->createCustomerPayment($user, $amount);

        } catch (\Exception $e) {
            Log::error('ZOHO-CUSTOMER-PAYMENT-PULL-STMT', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }
}
