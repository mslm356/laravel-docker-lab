<?php

namespace App\Http\Middleware;

use App\Models\Users\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $variables)
    {

        $variables = explode(':', $variables);

        $guard = $variables[0];
        $roleName = $variables[1];

        $user = getAuthUser($guard);

        $hasInvestorRole = DB::table('model_has_roles')
            ->where('model_id', $user->id)
            ->where('model_type', User::class)
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->where('roles.name', $roleName)
            ->count();

        if (!$hasInvestorRole) {
            return apiResponse(400, __('mobile_app/message.not_permission'));
        }

        return $next($request);
    }
}
