<?php

namespace App\Console\Commands;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\WalletType;
use App\Enums\MemberShips\MemberShipType;
use App\Enums\Notifications\NotificationTypes;
use App\Enums\Role;
use App\Jobs\AbsherTest;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use App\Models\Anb\AnbTransfer;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class deleteReportNotificationCommand extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete-report-notification {{--listing_id=}}';
    public $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete report notification';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $listing = Listing::find($this->option('listing_id'));
            if(!$listing)
                dd('Listing Not Found');

            $users = optional($listing->investors()->where('invested_shares' , 0))->pluck('investor_id')->toArray();

          $result =  DB::table('notifications')
                ->whereIn('notifiable_id', $users)
                ->where('notifiable_type','App\Models\Users\User')
                ->where('type','App\Notifications\FundReport')
                ->whereJsonContains('data->action_id', (int)$this->option('listing_id'))
                ->count();

          dd($result);

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }



}
