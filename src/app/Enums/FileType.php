<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class FileType extends Enum
{
    const ListingAttachment = 1;
    const SubscriptionForm = 2;
    const TransactionReceipt = 3;
    const ListingReport = 4;
    const BankAccountAttachment = 5;
    const AuthorizedEntityCR = 6;
    const AuthorizedEntityCMALicense = 7;
    const InvestorUpgradeProof = 8;
    const TransactionZatcaEInvoice = 9;
    const BannerImage = 10;
    const FundInvestorsReport = 11;
    const InvestorsComplaint = 12;
}
