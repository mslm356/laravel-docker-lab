<?php

namespace App\Mail\SubscriptionForm;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ApproveRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $path;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $path)
    {
        $this->user = $user;
        $this->path = $path;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/app/overview';
        $name = $this->user ? $this->user->full_name : '---';
        $path =  $this->path ;

        return $this->view('emails.subscription_form.approve_request', compact('profileUrl', 'name'))
            ->subject(__('emails/subscription_form/approve_request.subject'))
            ->attachFromStorageDisk("oci", $path);
    }
}
