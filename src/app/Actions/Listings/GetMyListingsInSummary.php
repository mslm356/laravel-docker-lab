<?php

namespace App\Actions\Listings;

use App\Enums\InvestmentType;
use App\Models\Listings\Listing;
use Closure;
use Lorisleiva\Actions\Concerns\AsAction;

class GetMyListingsInSummary
{
    use AsAction;

    public function handle($constraints = null, $mapFn = null)
    {
        return Listing::with( 'coverImage') // investors
            ->when($constraints instanceof Closure, $constraints)
            ->get()
            ->map(function ($listing) use ($mapFn) {

                $data = [
                    'id' => $listing->id,
                    'title' => $listing->title,
                    'deadline' => $listing->deadline->format('Y-m-d'),
                    'target' => clean_decimal($listing->target->formatByDecimal()),
                    'type' => InvestmentType::getDescription($listing->investment_type),
                    'raised' => number_format($listing->total_invested_amount), //number_format($listing->investors->sum('pivot.invested_amount')),
                    'enabled' => $listing->is_visible,
                    'image' => optional($listing->coverImage)->url
                ];

                if ($mapFn instanceof Closure) {
                    $data = $mapFn($listing, $data);
                }

                return $data;
            });
    }
}
