<?php

namespace App\Console\Commands\Absher;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Elm\AbsherOtpReason;
use App\Enums\IdentitySourceType;
use App\Models\Country;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Elm\AbsherOtp\Exceptions\CustomerException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Elm\Yaqeen\AuthBodyParams;
use App\Support\Elm\Yaqeen\Exceptions\BirthDateException;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use App\Support\Elm\Yaqeen\Exceptions\MissingInfoRelatedToIdException;
use App\Support\Elm\Yaqeen\Repositories\YaqeenRepository;
use App\Support\Elm\Yaqeen\Yaqeen;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use App\Support\Elm\Yaqeen\YaqeenSoapClient;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class GetInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absher:info {--id=} {--date=} {--type=} {--locale=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $yaqeenInfoService;
    public $logService;

    public function __construct()
    {
        parent::__construct();
        $this->yaqeenInfoService = new YaqeenInfoService();
        $this->logService = new LogService();
    }

    public function handle()
    {

        //1958-10-20

//        dd(shell_exec('varnishadm -T 10.0.3.158:6082 -S /etc/varnish/secret ban "req.http.host == api-pre.investaseel.com  && req.url == /api/app/listing/32"'));
//
//        dd(1);

//        $hijriDate = Hijri::Date('d-m-Y', $this->option('date'));
//        dd($hijriDate);

        $data = [
            'nin'=> $this->option('id'),
//            'birthDateInstance'=> \Illuminate\Support\Carbon::createFromFormat('Y-m-d', $this->option('date'))->format('m-Y'),
            'birth_date' => $this->option('date'),
            'type'=> $this->option('type'),
            'locale'=> $this->option('locale'),
        ];


        if ($this->option('type') == 'city') {

            $info = $this->getCitizenInfo($data['nin'], $data['birth_date']);
            $address = $this->getCitizenAddress($data['nin'], $data['birth_date'], $data['locale']);

        }else {

            $info = $this->getInfoByIqama($data['nin'], $data['birth_date']);
            $address = $this->getAddressInfoByIqama($data['nin'], $data['birth_date'], $data['locale']);
        }

        dd($info, $address);
    }

    public function getCitizenInfo($nin, $dateOfBirth)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'CitizenInfoRequest' => array_merge(
                AuthBodyParams::get(),
                [
                    'nin' => $nin,
                    'dateOfBirth' => $dateOfBirth,
                    'referenceNumber' => (string) Str::uuid(),
                ]
            )
        ];

        $this->logService->log('getCitizenInfo',null, ['test yaqeen' ,'request_body' => $requestBody]);
        $response = $client->__soapCall('getCitizenInfo', [$requestBody]);

        return $response;
    }

    public function getInfoByIqama($iqamaNumber, $dateOfBirth)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'AlienInfoByIqamaRequest' => array_merge(
                AuthBodyParams::get(),
                [
                    'dateOfBirth' => $dateOfBirth,
                    'iqamaNumber' => $iqamaNumber,
                    'referenceNumber' => (string) Str::uuid(),
                ]
            )
        ];

        $this->logService->log('getInfoByIqama',null, ['test yaqeen' ,'request_body' => $requestBody]);
        $response = $client->__soapCall('getAlienInfoByIqama', [$requestBody]);

        return $response;
    }

    public function getCitizenAddress($nin, $dateOfBirth, $locale)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'CitizenInfoAddressRequest' => array_merge(
                AuthBodyParams::get(),
                [
                    'nin' => $nin,
                    'dateOfBirth' => $dateOfBirth,
                    'referenceNumber' => (string) Str::uuid(),
                    'addressLanguage' => $locale,
                ],
            )
        ];

        $this->logService->log('getCitizenAddress',null, ['test yaqeen' ,'request_body' => $requestBody]);
        $response = $client->__soapCall('getCitizenInfoAddress', [$requestBody]);

        return $response;
    }

    public function getAddressInfoByIqama($iqama, $dateOfBirth, $locale)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'AlienInfoByIqamaAddressRequest' => array_merge(
                [

                    'iqamaNumber' => $iqama,
                    'dateOfBirth' => $dateOfBirth,
                    'referenceNumber' => (string) Str::uuid(),
                    'addressLanguage' => $locale,

                ],
                AuthBodyParams::get(),
            )
        ];

        $this->logService->log('getAddressInfoByIqama',null, ['test yaqeen' ,'request_body' => $requestBody]);
        $response = $client->__soapCall('getAlienInfoByIqamaAddress', [$requestBody]);

        return $response;
    }

}
