<?php

use App\Enums\InvestmentType;
use App\Enums\Banking\BankCode;
use App\Enums\BankAccountStatus;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Investors\Registration\Level;
use App\Enums\Investors\Registration\NetWorth;
use App\Enums\Investors\Registration\Education;
use App\Enums\Investors\Registration\MoneyRange;
use App\Enums\Investors\Registration\Occupation;
use App\Enums\Investors\Registration\CashOutPeriod;
use App\Enums\Investors\Registration\InvestorEntity;
use App\Enums\Investors\Registration\CurrentInvestment;
use App\Enums\Investors\Registration\InvestmentObjective;
use App\Enums\Elm\NatheerNotifiaction;

return [
    BankCode::class => [
        BankCode::Alawwal => 'البنك الاول',
        BankCode::Albilad => 'بنك البلاد',
        BankCode::Anb => 'البنك العربي الوطني',
        BankCode::Aljazira => 'بنك الجزيرة',
        BankCode::MuscatBank => 'بنك مسقط',
        BankCode::Bnp => 'بي ان بي باريبا',
        BankCode::Alfarnsi => 'البنك السعودي الفرنسي',
        BankCode::ChaseMorgan => 'جي بي مورغان تشيس بنك',
        BankCode::Deutsche => 'دويتشة بنك',
        BankCode::Emirates => 'بنك الإمارات دبي الوطني',
        BankCode::GulfInt => 'بنك الخليج الدولي',
        BankCode::IndCommChina => 'البنك الصناعي والتجاري الصيني',
        BankCode::Alinma => 'بنك الإنماء',
        BankCode::BahrainBank => 'بنك البحرين الوطني',
        BankCode::KuwaitBank => 'بنك الكويت الوطني',
        BankCode::PakistanBank => 'بنك باكستان الوطني',
        BankCode::Ncb => 'البنك الأهلي السعودي',
        BankCode::RiyadhBank => 'بنك الرياض',
        BankCode::Rajhi => 'مصرف الراجحي',
        BankCode::Sabb => 'البنك السعودي البريطاني',
        BankCode::Sama => 'البنك السعودي المركزي',
//        BankCode::Samba => 'مجموعة سامبا المالية',
        BankCode::Sib => 'البنك السعودي للإستثمار',
        BankCode::TurkeyZiraat => 'بنك الزراعات التركي',
        BankCode::QatarBank => 'بنك قطر الوطني',
        BankCode::MUFGBank => 'بنك إم يو أف جي (MUFG)',
        BankCode::AbuDhabiBank => 'بنك أبو ظبي الأول',
    ],
    InvestmentType::class => [
        InvestmentType::Development => 'تطوير',
        InvestmentType::IncomeGenerating => 'مدرة للدخل',
        InvestmentType::InfrastructureDevelopment => 'أرض خام',
        InvestmentType::PrivateEquity => 'ملكيه خاصه',
    ],
    CashOutPeriod::class => [
        CashOutPeriod::ShortTerm => 'قصيرة المدى (أقل من سنة)',
        CashOutPeriod::MediumTerm => 'متوسطة المدى (من سنة إلى خمس سنوات)',
        CashOutPeriod::LongTerm => 'طويلة المدى (أكثر من خمس سنوات)',
    ],
    InvestmentObjective::class => [
        InvestmentObjective::ProtectionOfCapital => 'حماية رأس المال',
        InvestmentObjective::Income => 'تحقيق الدخل',
        InvestmentObjective::Balanced => 'الاستثمار المتوازن',
        InvestmentObjective::GrowthOfCapital => 'نمو رأس المال',
    ],
    Level::class => [
        Level::Low => 'منخفضة',
        Level::Medium => 'متوسطة',
        Level::High => 'عالية',
    ],
    Education::class => [
        Education::Postgraduate => 'دراسات عليا',
        Education::Undergraduate => 'جامعي',
        Education::HighSchool => 'ثانوي',
        Education::Other => 'أخرى',
    ],
    Occupation::class => [
        Occupation::Retired => 'متقاعد',
        Occupation::Employee => 'موظف',
        Occupation::Employer => 'صاحب عمل',
        Occupation::Unemployed => 'غير موظف',
        Occupation::Student => 'طالب',
        Occupation::Other => 'أخرى',
    ],
    MoneyRange::class => [
        MoneyRange::Between1000To50k => '1,000 - 50,000',
        MoneyRange::Between50kTo120k => '50,001 - 100,000',
        MoneyRange::Between120kTo250k => '100,001 - 200,000',
        MoneyRange::Between250kTo500k => '200,001 - 500,000',
        MoneyRange::Over500k => 'أكثر من 500,000',
    ],
    CurrentInvestment::class => [
        CurrentInvestment::RealEstate => 'العقار',
        CurrentInvestment::CaptialMarket => 'سوق الأسهم',
        CurrentInvestment::PrivateEquity => 'الملكية الخاصة',
        CurrentInvestment::Other => 'أخرى',
    ],
    NetWorth::class => [
        NetWorth::LessThan5MillionsSAR => 'أقل من 5 مليون ر.س',
        NetWorth::MoreThan5MillionsSAR => 'أكثر من 5 مليون ر.س',
    ],
    BankAccountStatus::class => [
        BankAccountStatus::PendingReview => 'في انتظار المراجعة',
        BankAccountStatus::InReview => 'تحت المراجعة',
        BankAccountStatus::Rejected => 'مرفوضة',
        BankAccountStatus::Approved => 'مقبولة',
    ],
    InvestorEntity::class => [
        InvestorEntity::Firm => 'شركة',
        InvestorEntity::Individual => 'فرد'
    ],
    InvestorStatus::class => [
        InvestorStatus::InReview => 'قيد المراجعة',
        InvestorStatus::PendingReview => 'في انتظار المراجعة',
        InvestorStatus::Rejected => 'مرفوض',
        InvestorStatus::Approved => 'موافق عليه',
        InvestorStatus::AutoAmlCheckFailed => 'فشل التحقق من AML',
        InvestorStatus::Suspended => 'فشل التحقق محظور',
    ],
    BankAccountStatus::class => [
        BankAccountStatus::InReview => 'قيد المراجعة',
        BankAccountStatus::PendingReview => 'في انتظار المراجعة',
        BankAccountStatus::Rejected => 'مرفوض',
        BankAccountStatus::Approved => 'موافق عليه',
    ],

    NatheerNotifiaction::class => [
        NatheerNotifiaction::Death_Indicator_Changed => 'تغير حالة الحياة',
        NatheerNotifiaction::Final_ExitVisa => 'اصدار تأشرية خروج نهائي',
        NatheerNotifiaction::Crossing_out_with_Final_ExitVisa => 'إشعار بالخروج النهائي بعد تجاوز الحدود',
        NatheerNotifiaction::Final_ExitVisa_Canceled => 'إلغاء تأشرية خروج نهائي',
        NatheerNotifiaction::DL_Issued_Renewed => 'رخصة تجديد و إصدار سياقة مركبة',
        NatheerNotifiaction::IQAMA_Renewed => 'تجديد إقامة',
        NatheerNotifiaction::IQAMA_Issued_for_borderNumber => 'إصدار رقم إقامة لحامل رقم الحدود',
        NatheerNotifiaction::Exit_with_ReEntry_Visa_but_has_not_returned_after_Visa_expiry => 'إشعار في حال عدم رجوع المقيم بعد تاريخ العودة',
    ],


];
