<?php

namespace App\Enums\Campaigns;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class CampaignUserStatus extends Enum
{

    const Pending = 1;
    const Sent = 2;
    const Failed = 3;
}
