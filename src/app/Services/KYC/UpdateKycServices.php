<?php


namespace App\Services\KYC;


use Alkoumi\LaravelHijriDate\Hijri;
use App\Mail\kyc\IdExpired;
use App\Models\Investors\InvestorProfile;
use App\Models\Users\NationalIdentity;
use Carbon\Carbon;
use App\Mail\kyc\Kyc3Years;
use App\Traits\SendMessage;
use App\Traits\NotificationServices;
use Illuminate\Support\Facades\Mail;
use App\Jobs\NotificationFirebaseBulk;
use App\Enums\Notifications\NotificationTypes;

class UpdateKycServices
{

    use SendMessage, NotificationServices;


    function updateKYC()
    {
        $this->expiredNationalIdenity();
        $this->kyc3Years();
    }


    public function expiredNationalIdenity()
    {
        $usersCollection = collect();
        $now=Hijri::Date('Y-m-d', Carbon::now()->addDays(7));


        NationalIdentity::join('investor_profiles', 'national_identities.user_id', '=', 'investor_profiles.user_id')
            ->where('national_identities.id_expiry_date', '<', $now)
            ->where('investor_profiles.is_kyc_completed',true)
            ->with('user')
            ->lazy()
            ->each(function ($national) use ($usersCollection) {

                $user=$national->user;
                $user->investor->is_kyc_completed=0;
                $user->investor->updated_at=Carbon::now();
                $user->investor->save();

                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new IdExpired($user));
                }


                $this->sendSms($user,'expire_id_date');

                if($user->firebase_token) {
                    $usersCollection->push($user);
                }
            });


        $notification = new NotificationFirebaseBulk(NotificationTypes::Info, $this->notificationData('expire_id_date'), $usersCollection);
        dispatch($notification)->onQueue('notify');
        return true;
    }


    public function kyc3Years()
    {
        $usersCollection = collect();
        $now=Hijri::Date('Y-m-d', Carbon::now()->addDays(7));


        //        InvestorProfile::join('national_identities', 'investor_profiles.user_id', '=', 'national_identities.user_id')
        //            ->where('national_identities.id_expiry_date','<',$now)
        //            ->orWhere('investor_profiles.last_kyc_update_date','<',Carbon::now()->subDays( 363*2))
        //            ->update([ 'investor_profiles.is_kyc_completed' => false]);


        InvestorProfile::where('investor_profiles.last_kyc_update_date', '<', Carbon::now()->subDays(363*3))
            ->where('is_kyc_completed',true)
            ->with('user')
            ->lazy()
            ->each(function ($profile) use ($usersCollection) {

                $profile->is_kyc_completed=0;
                $profile->updated_at=Carbon::now();
                $profile->save();

                //                $user=\App\Models\Users\User::where('id',$profile->user_id)->first();
                $user=$profile->user;
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new Kyc3Years($user));
                }

                $this->sendSms($user);

                if($user->firebase_token) {
                    $usersCollection->push($user);
                }
            });

        $notification = new NotificationFirebaseBulk(NotificationTypes::Info, $this->notificationData(), $usersCollection);
        dispatch($notification)->onQueue('notify');
        return true;
    }


    public function notificationData($type='3years')
    {

        if ($type == '3years')
        {
            $notify_content_ar = "نظراً لمرور ٣ سنوات على آخر تحديث لملفكم الشخصي يرجى  تحديث بياناتكم عن طريق اكمال الملف الشخصي";
            $notify_content_en = "3 years have passed since the last update of your profile, please update the KYC form in your profile";
        }
        else
        {
            $notify_content_ar = "نظراً لانتهاء تاريخ الهوية المسجلة لدينا يرجى تحديث بياناتكم لتفعيل حسابكم";
            $notify_content_en = "Due to the expiration of your registered ID, please update the KYC form in your profile";
        }


        return [
            'title_ar' => 'عضويات أصيل', [], 'ar',
            'title_en' => 'Aseel', [], 'ar',
            'content_ar' => $notify_content_ar, [], 'ar',
            'content_en' => $notify_content_en, [], 'ar',
            'channel' => 'mobile'
        ];
    }


    public function sendSms($user,$type='3years')
    {



        if ($type == '3years')
        {
            $content_ar_1 = "نظراً لمرور ٣ سنوات على آخر تحديث لملفكم الشخصي يرجى  تحديث بياناتكم عن طريق اكمال الملف الشخصي شكرا لتعاملكم مع منصة أصيل";
            $content_en_1 = "  3 years have passed since the last update of your profile, please update the KYC form in your profile Thank you for trusting Aseel ";
        }
        else
        {
            $content_ar_1 = "نظراً لانتهاء تاريخ الهوية المسجلة لدينا يرجى تحديث بياناتكم لتفعيل حسابكم";
            $content_en_1 = "Due to the expiration of your registered ID, please update the KYC form in your profile";
        }


        $now = Carbon::now();

        $name = $user ? $user->full_name : '---';

        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar_1;
        $content_en = "Our dear investor " . $name . $content_en_1;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }


        $this->sendOtpMessage($user->phone_number, $message);


    }


}
