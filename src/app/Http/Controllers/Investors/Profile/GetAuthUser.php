<?php

namespace App\Http\Controllers\Investors\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\AuthResource;
use Illuminate\Http\Request;

class GetAuthUser extends Controller
{
    /**
     * Show Investor Profile.
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = $request->user()->load(['investor']);

        return new AuthResource($user);
    }
}
