<?php

namespace App\Actions\Investors;

use Lorisleiva\Actions\Concerns\AsAction;

class IsFirstInvestmentInListing
{
    use AsAction;

    /**
     * Check if it is first time the investor invests in a listing
     *
     * @param \Illuminate\Support\Collection $currentInvest
     * @param int $investorId
     *
     * @return bool
     */
    public function handle($currentInvest, $investorId)
    {
        return $currentInvest->where('investor_id', $investorId)
            ->count() === 0;
    }
}
