<?php

use App\Models\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('iso2', 2);
            $table->string('name_en');
            $table->string('name_ar');
        });

        $en = json_decode(Storage::disk('local')->get('countries-en.json'),true);
        $ar = json_decode(Storage::disk('local')->get('countries-ar.json'),true);

        foreach ($en as $iso2 => $value) {
            Country::create([
                'iso2' => $iso2,
                'name_en' => $value,
                'name_ar' => $ar[$iso2],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
