<?php

return [
    'subject' => 'Welcome to Aseel',
    'message' => 'Our honorable investor, Aseel platform, will provide you with unique opportunities to invest',
    'link_text' => 'You can complete your profile via the link',
    'social_text' => 'Be the first to invest in the opportunities that Aseel will offer by following us on social media',
    'get_started' => 'Start investing',
    'complete_profile' => 'Complete your profile'
];
