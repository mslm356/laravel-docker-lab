<?php

namespace App\Http\Requests\Complaint;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = textValidation();
        return [
            'subject' => "string|$regex|max:150",
            'description' => "string|$regex|max:1000",
            'comment' => "string|$regex|max:1000",
            'complaint_type_id' => "integer|exists:complaint_types,id",
            'status' => "integer|required|exists:complaint_statuses,id",
        ];
    }
}
