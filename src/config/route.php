<?php

return [
    /**
     * This specify which group is enabled
     *
     * Groups are: admin, investor, authorized_entity
     */
    'enabled_route_groups' => explode(',', env('ENABLED_ROUTE_GROUPS', '')),
];
