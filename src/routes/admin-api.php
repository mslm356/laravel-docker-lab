<?php

use App\Http\Controllers\Admins\Api\Aml\AmlController;

use App\Http\Controllers\Admins\Api\Anb\Accounts\GetAccounts;
use App\Http\Controllers\Admins\Api\Anb\EodStatements\EodStatementController;
use App\Http\Controllers\Admins\Api\Anb\EodStatements\EodTransactionController;
use App\Http\Controllers\Admins\Api\Anb\EodStatements\ExportAnbEodStatementTransactions;
use App\Http\Controllers\Admins\Api\Anb\EodStatements\ExportAnbEodTransactions;
use App\Http\Controllers\Admins\Api\AuthorizedEntities\EntityController;
use App\Http\Controllers\Admins\Api\AuthorizedEntities\EntityUserController;
use App\Http\Controllers\Admins\Api\AuthorizedEntities\GetApprovedEntitiesWithBankAccounts;
use App\Http\Controllers\Admins\Api\FAQ\FaqController;
use App\Http\Controllers\Admins\Api\Logs\LogController;
use App\Http\Controllers\Admins\Api\UploadEditorImages;
use App\Http\Controllers\Admins\Api\Vats\VatController;
use App\Http\Controllers\Admins\Api\Dashboard\GetOverview;
use App\Http\Controllers\Admins\Api\Votes\VotesController;
use App\Actions\Admin\SubscriptionForm\RequestUpdateStatus;
use App\Http\Controllers\Admins\Api\Logs\WorkerLogController;
use App\Http\Controllers\Admins\Api\Reports\ReportsController;
use App\Http\Controllers\Admins\Api\Listings\ListingController;
use App\Http\Controllers\Admins\Api\Enquiries\EnquiryController;
use App\Http\Controllers\Admins\Api\Listings\GetListingRevisions;
use App\Http\Controllers\Admins\Api\Listings\UpdateListingStatus;
use App\Http\Controllers\Admins\Api\Campaigns\CampaignsController;
use App\Http\Controllers\Admins\Api\Listings\DiscussionController;
use App\Http\Controllers\Admins\Api\Reports\ReportTypesController;
use App\Http\Controllers\Admins\Api\Complaints\ComplaintController;
use App\Http\Controllers\Admins\Api\Dashboard\GetDashboardListings;
use App\Http\Controllers\Admins\Api\Listings\ListingImageController;
use App\Http\Controllers\Admins\Api\Enquiries\EnquiryReplyController;
use App\Http\Controllers\Admins\Api\Investors\GetInvestorInvestments;
use App\Http\Controllers\Admins\Api\Listings\ListingPayoutController;
use App\Http\Controllers\Admins\Api\Listings\ListingReportController;
use App\Http\Controllers\Admins\Api\Listings\ListingWalletController;
use App\Http\Controllers\Admins\Api\MemberShips\MemberShipController;
use App\Http\Controllers\Admins\Api\WebsiteContents\ContentController;
use App\Http\Controllers\Admins\Api\BankAccounts\BankAccountController;
use App\Http\Controllers\Admins\Api\CompanyProfiles\CompanyProfilesController;
use App\Http\Controllers\Admins\Api\ContactUsReasons\ContactReasonController;
use App\Http\Controllers\Admins\Api\DeleteFile;
use App\Http\Controllers\Admins\Api\DownloadFile;
use App\Http\Controllers\Admins\Api\GetAuthUser;
use App\Http\Controllers\Admins\Api\Investors\ExportInvestorTransactions;
use App\Http\Controllers\Admins\Api\Investors\InvestorController as InvestorsInvestorController;
use App\Http\Controllers\Admins\Api\Investors\ProfessionalUpgrade\UpgradeRequestController;
use App\Http\Controllers\Admins\Api\Listings\DiscussionReplyController;
use App\Http\Controllers\Admins\Api\Listings\ExportListingTransactions;
use App\Http\Controllers\Admins\Api\Listings\Investors\ExportInvestorsTransactionsReportController;
use App\Http\Controllers\Admins\Api\Listings\ListingAttachmentController;
use App\Http\Controllers\Admins\Api\Listings\ListingValuationController;
use App\Http\Controllers\Admins\Api\PropertyTypes\PropertyTypeController;
use App\Http\Controllers\Admins\Api\RoleController;
use App\Http\Controllers\Admins\Api\SubscriptionForm\RequestsController;
use App\Http\Controllers\Admins\Api\Transfer\TransferRequestsController;
use App\Http\Controllers\Investors\Dashboard\GetTransactionsHistory;
use App\Http\Controllers\Investors\Dashboard\InvestorWalletController;
use Illuminate\Support\Facades\Route;




//Route::get('/special-eod-mt940', SpecialEodStmtMt940::class);//->middleware('permission:download_kyc_pdf');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('complaints/types', [ComplaintController::class, 'types'])->middleware('permission:view-all-complaints');

    Route::get('complaints', [ComplaintController::class, 'index'])->middleware('permission:view-all-complaints');
    Route::get('complaints/{complaint}/edit', [ComplaintController::class, 'edit'])->middleware('permission:show-complaints');
    Route::put('complaints/{complaint}', [ComplaintController::class, 'update']);//->middleware('permission:update-complaints');

    Route::get('complaints/investor/{investor}', [ComplaintController::class, 'investor'])->middleware('permission:get-investor-complaints');

    // Admin Profile
    Route::get('/auth', GetAuthUser::class);

    //Roles & Permissions Routes
    Route::resource('roles', RoleController::class)->middleware('permission:manage-roles');
    Route::get('get-permissions', [RoleController::class, 'getPermissions'])->middleware('permission:manage-roles');
    Route::get('admins', [RoleController::class, 'getAdmins'])->middleware('permission:list-admins');
    Route::post('admins/assign-role/{id}', [RoleController::class, 'assignRole'])->middleware('permission:admin-assign-role');
    ;

    Route::post('/editor-images', UploadEditorImages::class);//->middleware('permission:upload-editor-images');

    Route::apiResource('property-types', PropertyTypeController::class)
        ->only(['index', 'show', 'update', 'store'])->middleware('permission:fund-types');

    Route::put('/bank-accounts/{bank_account}/status', [BankAccountController::class, 'updateStatus'])
        ->name('bank_accounts.status.update')->middleware('permission:update-bank-account-status');

    Route::post('/bank-accounts/{bank_account}/update', [BankAccountController::class, 'update'])
        ->name('bank_accounts.update')->middleware('permission:bankEditor');

    Route::apiResource('bank-accounts', BankAccountController::class)
        ->only(['index', 'show'])->middleware('permission:bank-accounts');

    Route::get('campaigns/options', [ CampaignsController::class, 'getOptions']);

    Route::apiResource('campaigns', CampaignsController::class)->only(['index', 'show', 'update'])->middleware('permission:campaign');

    Route::post('campaigns/store', [CampaignsController::class, 'store'])->middleware('permission:campaign-create');
    Route::post('campaign/stop', [CampaignsController::class, 'stopCampaign'])->middleware('permission:campaign-stop');
    Route::get('campaigns/{campaign}/status', [CampaignsController::class, 'showCampaignUser'])->middleware('permission:campaign-status');

    //     Subscription Form Requests
    //    Route::put('subscription-form-update-status/{subscriptionFormRequest}', [ RequestsController::class, 'updateStatus'])->middleware('permission:update-account-statement-status');
    //    Route::get('subscription-form-show/{subscription_form_id}', [RequestsController::class, 'show'])->middleware('permission:account-statement-view');
    //    Route::get('subscription-form-index', [RequestsController::class, 'index'])->middleware('permission:account-statement');

    Route::put('subscription-form-update-status/{subscriptionFormRequest}', [ RequestsController::class, 'updateStatus']);
    Route::get('subscription-form-show/{subscription_form_id}', [RequestsController::class, 'show']);
    Route::get('subscription-form-index', [RequestsController::class, 'index']);

    Route::get('/get-subscription-form-request/{subscriptionFormRequest}', [RequestUpdateStatus::class, 'generateRequestJson']);

    Route::get('/authorized-entities/approved-with-bank-accounts', GetApprovedEntitiesWithBankAccounts::class)
        ->name('authorized_entities.approved_with_bank_accounts')->middleware('permission:authorized-entities-approved_with_bank_accounts');


    Route::get('/memberships/{user}', [ MemberShipController::class,'index'])->middleware('permission:investors-membership');
    ;
    Route::get('/investor-membership/{user}', [ MemberShipController::class,'show'])->middleware('permission:investors-membership');
    ;
    Route::put('/membership/{user}/store', [ MemberShipController::class, 'store'])->middleware('permission:create-investor-membership');
    ;



    Route::put('/authorized-entities/{authorized_entity}/status', [EntityController::class, 'updateStatus'])
        ->name('authorized_entities.status.update')->middleware('permission:authorized-entities-id');

    Route::apiResource('authorized-entities', EntityController::class)
        ->only(['index', 'show', 'update', 'store'])->middleware('permission:authorized-entities');

    Route::apiResource('authorized-entities.users', EntityUserController::class)
        ->only(['index', 'show', 'update', 'store'])->scoped()->middleware('permission:authorized-entities-users');

    Route::apiResource('contact-reasons', ContactReasonController::class)
        ->only(['index', 'show', 'update', 'store'])->middleware('permission:contact-reasons');

    Route::apiResource('enquiries', EnquiryController::class)
        ->only(['index', 'show'])->middleware('permission:enquiries');

    Route::apiResource('website-contents', ContentController::class)
        ->only(['show', 'update'])
        ->scoped([
            'website_content' => 'name'
        ])->middleware('permission:pages');

    Route::post('/enquiries/{enquiry}/replies', [EnquiryReplyController::class, 'store'])
        ->name('enquiries.replies.store')->middleware('permission:create-enquiries-replies');

    Route::post('/listings/{listing}/discussions/{discussion}/replies', [DiscussionReplyController::class, 'store'])->middleware('permission:create-listing-discussion-replies');
    Route::put('/listings/{listing}/discussions/{discussion}/replies/{reply}', [DiscussionReplyController::class, 'update'])->middleware('permission:update-listing-discussion-reply');
    Route::delete('/listings/{listing}/discussions/{discussion}/replies/{reply}', [DiscussionReplyController::class, 'destroy'])->middleware('permission:detele-listing-discussion-replies');

    // TODO:need review
    Route::post('/listings/{listing}/payouts/{payout}/pay', [ListingPayoutController::class, 'pay'])
        ->middleware('permission:listing-payouts-pay');
    Route::post('/listings/{listing}/payouts/{payout}/approval', [ListingPayoutController::class, 'payoutApproval']);

    Route::get('/listings/{listing}/wallets', [ListingWalletController::class, 'index'])->middleware('permission:list-listing-wallets');
    Route::get('/listings/{listing}/balance', [ListingWalletController::class, 'getBalance'])->middleware('permission:show-listing-balance');
    // TODO:need review after investing
    Route::get('/listings/{listing}/investors', [ListingController::class, 'getInvestors'])->middleware('permission:list-listing-investors');
    Route::get('/listings/{listing}/investors/export', ExportInvestorsTransactionsReportController::class)->middleware('permission:export-listing-investors');
    // TODO:need review after investing
    Route::get('/listings/{listing}/transactions', [ListingWalletController::class, 'getTransactionHistory'])->middleware('permission:list-listing-transactions');
    Route::get('/listings/{listing}/transactions/export', ExportListingTransactions::class)->middleware('permission:export-listing-discussions');
    Route::get('/listings/{listing}/discussions', [DiscussionController::class, 'index'])->middleware('permission:funds-id-discussions');
    Route::get('/listings/{listing}/discussions/{discussion}', [DiscussionController::class, 'show'])->middleware('permission:show-listing-discussion');
    Route::delete('/listings/{listing}/discussions/{discussion}', [DiscussionController::class, 'destroy'])->middleware('permission:delete-listing-discussion');
    Route::put('/listings/{listing}/status', UpdateListingStatus::class)->middleware('permission:update-listing-status');
    // TODO:need review
    Route::get('/listings/{listing}/revisions', GetListingRevisions::class)->middleware('permission:funds-id-revisions');


    Route::apiResource('listings.images', ListingImageController::class)->only(['store', 'destroy']);//->middleware('permission:upload-images');
    Route::post('sort-images', [ListingImageController::class,'sortImages']);//->middleware('permission:upload-images');
    Route::apiResource('listings.reports', ListingReportController::class)->only(['store', 'update', 'index', 'show'])->middleware('permission:funds-id-reports');
    Route::apiResource('listings.valuations', ListingValuationController::class)->middleware('permission:funds-id-valuations');
    Route::apiResource('listings.files', ListingAttachmentController::class)->only(['store'])->middleware('permission:manage-listings-files');

    Route::put('/listing-report/{report}/update-status', [ListingReportController::class, 'updateStatus'])
    ->middleware('permission:funds-id-reports')
    ->name('listings.status.update');

    // TODO:need review
    Route::apiResource('listings.payouts', ListingPayoutController::class)->only(['index', 'store', 'show']);
//        ->middleware('permission:funds-id-payouts');
    Route::apiResource('listings', ListingController::class)->middleware('permission:funds');

    Route::get('/anb/accounts', GetAccounts::class)->middleware('permission:anb');
    Route::get('/anb/eod-transactions', [EodTransactionController::class, 'index'])->middleware('permission:anb-transactions');
    Route::get('/anb/eod-transactions/export', ExportAnbEodTransactions::class)->middleware('permission:export-eod-statements');
    Route::get('/anb/eod-statements', [EodStatementController::class, 'index'])->middleware('permission:list-eod-statements');
    Route::get('/anb/eod-statements/{statement}/transactions', [EodStatementController::class, 'showTransactions'])->middleware('permission:show-eod-statement-transactions');
    Route::get('/anb/eod-statements/{statement}/transactions/export', ExportAnbEodStatementTransactions::class)->name('eod_statements.transactions.export')->middleware('permission:export-eod-statements-transactions');
    Route::get('/anb/eod-statements/{statement}', [EodStatementController::class, 'show'])->middleware('permission:show-eod-statement');

    Route::get('/files/{file}/download', DownloadFile::class)->name('files.download')->middleware('permission:download-files');
    Route::delete('/files/{file}', DeleteFile::class)->middleware('permission:delete-files');

    Route::get('/logs', [LogController::class, 'getLogs'])->middleware('permission:logs');
    Route::delete('/logs', [LogController::class, 'clearLogs'])->middleware('permission:delete-logs');

    Route::get('/worker-logs', [WorkerLogController::class, 'getLogs'])->middleware('permission:worker-logs');
    Route::delete('/worker-logs', [WorkerLogController::class, 'clearLogs'])->middleware('permission:delete-worker-logs');

    Route::post('/wallet/listing/{listing}', [ListingWalletController::class, 'store'])->middleware('permission:list-listing-wallets');

    // Investor
    Route::get('/investors/upgrade-requests', [UpgradeRequestController::class, 'index'])->middleware('permission:professional-requests');
    Route::get('/investors/upgrade-profile-requests/{id}', [UpgradeRequestController::class, 'showUpgradeRequestListByInvestor'])->middleware('permission:professional-requests-id');
    Route::get('/investors/upgrade-requests/{upgrade_request}', [UpgradeRequestController::class, 'show'])->middleware('permission:show-investor-upgrade-request');
    Route::put('/investors/upgrade-requests/{upgrade_request}/status', [UpgradeRequestController::class, 'updateStatus'])->middleware('permission:update-investor-upgrade-request-status');
    Route::get('/investors', [InvestorsInvestorController::class, 'index'])->middleware('permission:investors');
    // TODO: need review after investing
    Route::get('/investors/{user}/investments', GetInvestorInvestments::class)->middleware('permission:list-investor-investments');
    Route::get('/investors/{user}/transactions', GetTransactionsHistory::class)->middleware('permission:list-investor-transactions');
    Route::post('/investors/{user}/transactions/export', ExportInvestorTransactions::class)->middleware('permission:export-investor-transactions');
    Route::get('/investors/{user}/balance-inquiry', [InvestorWalletController::class, 'balanceInquiry'])->middleware('permission:list-investor-balance-inquiry');
    Route::post('/investors/{user}/change-wallet-status', [InvestorWalletController::class, 'changeWalletStatus'])->middleware('permission:change-wallet-status');
    Route::put('/investors/{user}/status', [InvestorsInvestorController::class, 'updateStatus'])->middleware('permission:update-investor-status');
    Route::get('/investors/{user}', [InvestorsInvestorController::class, 'show'])->middleware('permission:investors-id');
    Route::post('/investors/delete', [InvestorsInvestorController::class, 'deleteAccount'])->middleware('permission:delete-investor');
    Route::post('/investors/{user}/update', [InvestorsInvestorController::class, 'update'])->middleware('permission:investors-editor');
    Route::post('/investors/rabet/{user}', [InvestorsInvestorController::class, 'checkPhoneWithRabet']);



    Route::apiResource('faqs', FaqController::class)->middleware('permission:faqs');

    Route::apiResource('vats', VatController::class)->except('update')->middleware('permission:manage-vats');

    // AML Check
    Route::post('/aml/users/{user}/check', [AmlController::class, 'checkUser'])->middleware('permission:aml-check-user');
    Route::get('/aml/users/{user}/list', [AmlController::class, 'getUserList'])->middleware('permission:aml-list-user');

    Route::get('/dashboard/overview', GetOverview::class)->middleware('permission:dashboard');
    Route::get('/dashboard/listings', GetDashboardListings::class)->middleware('permission:dashboard');

    // REPORTS
    Route::get('/report-types/', [ReportTypesController::class, 'reportTypes'])->middleware('permission:export-reports');
    Route::get('/report/export', ReportsController::class)->middleware('permission:export-reports');

    Route::post('/company-profile-approve/{companyProfile}', [CompanyProfilesController::class, 'approveProfile'])
        ->middleware('permission:approve-company-profile');

    // Votes

    Route::post('/listings/{listing}/votes-store', [VotesController::class, 'store'])->middleware('permission:votes-id');
    Route::post('/vote/{vote}/end', [VotesController::class, 'endVote'])->middleware('permission:end-listing-vote');
    Route::get('/listings/{listing}/votes', [VotesController::class, 'index'])->middleware('permission:votes');
    Route::get('/vote/{vote}', [VotesController::class, 'show'])->middleware('permission:votes');

    Route::get('/vote/{vote}/download', [VotesController::class, 'download'])->middleware('permission:votes');



    //    Transfer Requests    //
    Route::get('/transfer-requests', [ TransferRequestsController::class, 'index'])->middleware('permission:transfer_review');
    Route::get('/transfer-requests/{transferRequest}', [TransferRequestsController::class, 'show'])->middleware('permission:show-transfer-request');
    Route::put('/transfer-requests/{transferRequest}/status', [TransferRequestsController::class, 'updateStatus'])->middleware('permission:transfer_accept');
    Route::put('/accept-transfer-requests', [TransferRequestsController::class, 'accept'])->middleware('permission:transfer_accept');


    Route::get('/download-kyc-pdf/{investor_id}', [InvestorsInvestorController::class, 'downloadKycPdf']);//->middleware('permission:download_kyc_pdf');


});
