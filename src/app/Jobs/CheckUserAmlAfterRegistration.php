<?php

namespace App\Jobs;

use App\Actions\Aml\CheckAmlForUser;
use App\Enums\AML\AmlStatus;
use App\Enums\AML\AmlType;
use App\Enums\Investors\InvestorStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckUserAmlAfterRegistration implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            $amlHistory = CheckAmlForUser::run($this->user, AmlType::AutoAssessment);

            if (config('aml.auto_update_investor_status_enabled')) {
                $status = null;

                if ($amlHistory->checks->some(fn ($check) => $check->status === AmlStatus::Found)) {
                    $status = InvestorStatus::Rejected;
                } elseif ($amlHistory->checks->every(fn ($check) => $check->status === AmlStatus::Passed)) {
                    $status = InvestorStatus::Approved;
                } elseif ($amlHistory->checks->some(fn ($check) => $check->status === AmlStatus::NotChecked)) {
                    $status = InvestorStatus::AutoAmlCheckFailed;
                }
            }
        });
    }
}
