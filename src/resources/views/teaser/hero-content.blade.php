<main class="mt-8 mx-auto max-w-screen-xl px-4 sm:mt-12 sm:px-6 md:mt-20 xl:mt-24">
    <div class="lg:grid lg:grid-cols-12 lg:gap-8">
        <div class="sm:text-center md:max-w-2xl md:mx-auto lg:col-span-6 lg:ltr:text-left lg:rtl:text-right">
            <h2
                class="mt-1 text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:leading-none sm:text-6xl lg:text-5xl xl:text-6xl">
                {{ __('teaser.heading_p1') }}
                <br class="hidden md:inline">
                <span class="md:block md:pt-3">{{ __('teaser.heading_p2') }}</span>
            </h2>
            <div class="inline-flex mt-4">
                <div
                    class="px-3 py-0.5 text-lg font-semibold uppercase tracking-wide text-primary bg-primary-inv rounded-full">
                    {{ __('teaser.coming_soon') }}
                </div>
            </div>
            <p class="mt-3 text-lg text-white sm:mt-5 sm:text-3xl">
                {{ __('teaser.description') }}
            </p>
        </div>
        <div
            id="app"
            class="mt-12 relative sm:max-w-lg sm:mx-auto lg:mt-0 lg:max-w-none lg:mx-0 lg:col-span-6 lg:flex lg:items-center">
            <teaser-registration 
                :country-codes='@json($countryCodes)'
                :countries='@json($countries)' 
                :t='@json(__('teaser.pre_register'))' 
            />
        </div>
    </div>
</main>