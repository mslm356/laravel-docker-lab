<?php

namespace App\Http\Controllers\Admins\Api\Anb\Accounts;

use App\Http\Controllers\Controller;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class GetAccounts extends Controller
{
    /**
     * Get statements
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        $accounts = null;

        if ($isLocal = App::environment('local')) {
            $accounts = Cache::get('anb-accounts');
        }

        if ($accounts === null) {
            $accounts = Auth::instance()->getAccounts();

            foreach ($accounts as $key => $account) {
                $balanceResponse = AnbApiUtil::balance($account['accountNumber']);
                $accounts[$key]['balance'] = [
                    'value' => $balanceResponse['clearedBalance'],
                    'currency' => $balanceResponse['currency']
                ];
                $accounts[$key]['account_number'] = $account['accountNumber'];
                unset($accounts[$key]['accountNumber']);
            }

            if ($isLocal) {
                Cache::put('anb-accounts', $accounts, now()->addHour());
            }
        }

        return response()->json([
            'data' => $accounts
        ]);
    }
}
