<?php

namespace App\Exports\Investors;

use App\Enums\Investors\InvestorStatus;
use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProfileWithKyc implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::role(Role::Investor)
            ->where('users.created_at', '>=', $this->data['start_date'])
            ->where('users.created_at', '<=', $this->data['end_date'])
            ->leftJoin('investor_profiles', function ($join) {
                $join->on('users.id', '=', 'investor_profiles.user_id');
            })
            ->select('users.id', 'users.first_name', 'users.last_name', 'users.email',
                     'investor_profiles.is_kyc_completed', 'investor_profiles.status')
            ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'First Name',
            'Last Name',
            'Email',
            'KYC Completed',
            'Status',
        ];
    }

    public function map($user): array
    {
        return [
            $user->id,
            $user->first_name,
            $user->last_name,
            $user->email,
            $user->is_kyc_completed ? 'Completed' : 'inCompleted',
            InvestorStatus::getDescription($user->status) ,
        ];
    }
}
