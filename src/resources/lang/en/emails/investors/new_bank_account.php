<?php

return [
    'subject' => 'Your IBAN adding request has been received',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'Your request to add your bank account',
    'message_2' => 'has been received, and the team will notify you once it is reviewed',
    'message_3' => 'Thank you for dealing with Aseel platform',
    'link_text' => 'To view your control panel, please go to the link',
    'view' => 'View control panel',
];
