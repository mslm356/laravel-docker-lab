<?php

namespace App\Support\Aml;

use Illuminate\Support\Facades\App;

class AmlManager
{
    /**
     * Return all parsers to parse every AML list
     *
     * @return \App\Support\Aml\AmlListInterface[]
     */
    public function getAllLists()
    {
        $lists = [];

        $lists[] = App::make("App\Support\Aml\EUList");
        $lists[] = App::make("App\Support\Aml\MOIList");
        $lists[] = App::make("App\Support\Aml\UNList");
        $lists[] = App::make("App\Support\Aml\USList");

        return $lists;
    }
}
