<?php

namespace App\Http\Controllers\Investors\Profile\Kyc;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Profile\KycResource;
use Illuminate\Http\Request;

class InvestorKycController extends Controller
{
    public function __invoke(Request  $request)
    {
        $user = $request->user();

        return new KycResource($user);
    }
}
