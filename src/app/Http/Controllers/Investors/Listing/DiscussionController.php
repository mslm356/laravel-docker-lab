<?php

namespace App\Http\Controllers\Investors\Listing;

use App\Enums\Listings\DiscussionStatus;
use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Listings\ListingDiscussionResource;
use App\Mail\Funds\Discuss;
use App\Models\Listings\Listing;
use App\Services\Investors\DiscussionServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role as ModelsRole;

class DiscussionController extends Controller
{

    public $discussionServices;

    public function __construct()
    {
        $this->discussionServices = new DiscussionServices();
    }

    /**
     * Listings list
     *
     * @param
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index($listing)
    {
        $listing = Listing::where('id', $listing)->where('is_visible', true)->first();
        if(!$listing)
            return response()->json(['message' => __('mobile_app/message.not_found')], 400);

        $discussions = $this->discussionServices->index($listing);
        return ListingDiscussionResource::collection($discussions);
    }

    /**
     * Store the discussion in the storage
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $listing)
    {
        $comment = $this->validate($request, [
            'comment' => ['required', 'string', 'max:2000']
        ])['comment'];

        return DB::transaction(function () use ($listing, $comment, $request) {
            $listing = Listing::where('id', $listing)
                ->where('is_visible', true)->sharedLock()->firstOrFail();

            $discussion = $listing->rootDiscussions()->create([
                'text' => $comment,
                'status' => DiscussionStatus::PendingReply,
                'user_id' => $request->user()->id,
                'user_role_id' => ModelsRole::findByName(Role::Investor, 'web')->id
            ]);

            Mail::to($request->user())->queue(new Discuss($request->user()));

            return new ListingDiscussionResource($discussion);
        });
    }
}
