<?php

namespace App\Http\Controllers\MobileApp\Investors;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Services\Files\GenerateFileUrl;
use App\Services\LogService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class GenerateFileUrlController extends Controller
{

    public $generateFileUrl;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->generateFileUrl = new GenerateFileUrl();
    }

    public function __invoke(Request $request, File $file)
    {
        try {

            $user = getAuthUser('api');
            return $this->generateFileUrl->getUrl($file, $user);

        } catch (AuthorizationException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }
    }
}
