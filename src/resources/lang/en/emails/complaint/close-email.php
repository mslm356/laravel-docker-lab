<?php

return [
    'dear' => 'Dear :name ',
    'subject' => 'Ticket Closed - :ticketNumber',
    'message' => 'If you have any further questions, please reach out to us.',
];
