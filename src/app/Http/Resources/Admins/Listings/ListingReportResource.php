<?php

namespace App\Http\Resources\Admins\Listings;

use App\Enums\Listings\ReportEnum;
use App\Enums\Role;
use App\Http\Resources\Admins\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'type' => $this->user && $this->user->hasRole(Role::AuthorizedEntityAdmin) ? "Authorized Entity" : "Admin",
            'status' => [
                'value' => $this->status,
                'description' => ReportEnum::getDescription($this->status)
            ],
            'files' => FileResource::collection($this->files)
        ];
    }
}
