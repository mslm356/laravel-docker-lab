<?php

namespace App\Http\Controllers\MobileApp\Investors\Discussions;

use App\Enums\Listings\DiscussionStatus;
use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Listings\ListingDiscussionResource;
use App\Mail\Funds\Discuss;
use App\Models\Listings\Listing;
use App\Services\Investors\DiscussionServices;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Role as ModelsRole;

class DiscussionsController extends Controller
{

    public $discussionServices;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->discussionServices = new DiscussionServices();

    }

    public function index($listing)
    {
        try {
            $listing = Listing::where('id', $listing)->where('is_visible', true)->first();
            if (!$listing) {
                return apiResponse(400, __('mobile_app/message.not_found'));
            }

            $discussions = $this->discussionServices->index($listing);
            $result = ListingDiscussionResource::collection($discussions);

            return apiResponse(200, __('mobile_app/message.success'), $result);

        } catch (\Exception $e) {
            $code = $this->logService->log('Discussion-index', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    public function store(Request $request, $listing)
    {
        try {
            $comment = $this->validate($request, [
                'comment' => ['required', 'string', 'max:2000']])['comment'];

            $listing = Listing::where('id', $listing)->where('is_visible', true)->sharedLock()->first();
            if (!$listing) {
                return apiResponse(400, __('mobile_app/message.not_found'));
            }

            return DB::transaction(function () use ($listing, $comment, $request) {
                $discussion = $listing->rootDiscussions()->create([
                    'text' => $comment,
                    'status' => DiscussionStatus::PendingReply,
                    'user_id' => getAuthUser('api')->id,
                    'user_role_id' => ModelsRole::findByName(Role::Investor, 'web')->id
                ]);

                Mail::to($request->user())->queue(new Discuss(getAuthUser('api')));

                return new ListingDiscussionResource($discussion);
            });

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-store-Discussion', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage(). ' : ' .  $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('store-Discussion', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }
}
