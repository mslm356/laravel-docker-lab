<?php


namespace App\Services\Investors;

class ListingReportServices
{

    public function ListingReportResource($report, $guard)
    {

        return [
            'id' => $report->id,
            'title' => $report->title,
            'description' => $report->description,
            'files' => $report->files->map(
                fn ($file) => [
                    'name' => $file->name,
                    'url' =>  $guard == 'api' ? url('api/app/files/' . $file->id . '/get-url') : route('investor.files.get.url', ['file'=> $file->id]),
                    'get_url' =>  $guard == 'api' ? url('api/app/files/' . $file->id . '/get-url') : route('investor.files.get.url', ['file'=> $file->id]),
                ]
            ),
            'created_at' => $report->created_at->format('Y-m-d'),
        ];

    }

}
