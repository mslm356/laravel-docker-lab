<?php

namespace App\Providers;

use App\Models\Investors\InvestorProfile;
use App\Models\MemberShip\MemberShipRequests;
use App\Models\VirtualBanking\Transaction;
use App\Observers\InvestorProfileObserver;
use App\Observers\MemberShipRequestObserver;
use App\Observers\TransactionObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Transaction::observe(TransactionObserver::class);
        MemberShipRequests::observe(MemberShipRequestObserver::class);
        InvestorProfile::observe(InvestorProfileObserver::class);
    }
}
