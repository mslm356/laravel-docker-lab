<?php

namespace App\Models;

class WebsiteContent extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'collection',
    ];
}
