<?php

namespace Database\Seeders;

use App\Models\Users\User;
use Illuminate\Database\Seeder;

class NatherUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create(['first_name'=>'Natheer Account','type'=>'admin','email'=>'natheer@aseel.com']);
    }
}
