<?php

namespace App\Mail\AuthorizedEntities\Listings;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreateReport extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $listing;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Listing $listing)
    {
        $this->user = $user;
        $this->listing = $listing;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $username = $this->user ? $this->user->full_name : '---';
        $fund_title = $this->listing && $this->listing->title_ar ? $this->listing->title_ar : $this->listing->title_en;

        return $this->view('emails.authorized-entities.listings.create_report', compact('username', 'fund_title'))
            ->subject(__('emails/funds/report.subject', ['fund_title'=> $fund_title]));
    }
}
