<?php

return [
    'property_type' => 'نوع العقار',
    'funding_target' => 'الهدف',
    'gross_yield' => 'العائد الإجمالي',
    'dividend_yield' => 'العائد الربحي',
    'days_remaining' => 'الأيام المتبقية',
    'hours_remaining' => 'الساعات المتبقية',
    'days' => 'يوم',
    'hours' => 'ساعة',
    'days' => '{1} يوم|[2,10] :count أيام|[11,*] :count يوم',
    'hours' => '{1} ساعة|[2,10] :count ساعات|[11,*] :count ساعة',
    'more_detail' => 'المزيد من التفاصيل',
    'get_started' => 'ابدأ الآن',
    'login' => 'تسجيل الدخول',
    'register' => 'التسجيل',
    'contact_us' => 'اتصل بنا',
    'faqs' => 'الأسئلة الأكثر شيوعًا',
    'investment_type' => 'نوع الاستثمار',
    'is_closed' => 'مغلقة',
    'closing_soon' => 'ستغلق قريبًا',
    'listing_status' => 'الحالة',
];
