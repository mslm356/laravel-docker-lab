<?php

return [
    'subject' => 'Support Team',
    'welcome_message' => 'Our honorable investor :name',
    'message' => 'We are pleased to receive your inquiry, our team will serve you as soon as possible',
    'end_message' => 'Hey, you',
];
