<?php

namespace App\Common\ValidationRules\Listings;

use App\Enums\WalletType;
use BenSampo\Enum\Rules\EnumValue;

class ListingWalletRules
{
    static public function get()
    {
        return [
            'type' => ['required', 'in:SPV,ESCROW']
        ];
    }
}
