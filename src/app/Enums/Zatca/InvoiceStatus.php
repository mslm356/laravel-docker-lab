<?php

namespace App\Enums\Zatca;

use BenSampo\Enum\Enum;

final class InvoiceStatus extends Enum
{
    const Initiated = 1;
    const Unassigned = 2;
    const Assigned = 3;
}
