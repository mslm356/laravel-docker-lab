<?php

namespace App\Jobs;

use App\Services\LogService;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class AbsherTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels , NotificationServices;

    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->logService->log('TEST ABSHER JOBS', null, ['TEST JOB IS DONE']);
    }
}
