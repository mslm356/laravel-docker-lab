<?php

namespace App\Actions\Listings\Payouts;

use App\Models\Payouts\Payout;
use App\Models\Listings\Listing;
use Lorisleiva\Actions\Concerns\AsAction;

class GetPayouts
{
    use AsAction;

    public function handle(Listing $listing, $type=null)
    {

        $rawQuery = 'id, total, payout_date,listing_id,unit_price,invested_units,type,
                    (select count(*) from investor_payout where investor_payout.payout_id = payouts.id and investor_payout.shares > 0 ) as owners,
                    (select count(*) from investor_payout where
                    investor_payout.payout_id = payouts.id and investor_payout.paid <> 0 ) as paid_owners'
        ;

        $payouts = Payout::with('listing', 'listing.investors')
            ->selectRaw($rawQuery)
            ->where('listing_id', $listing->id)
            ->latest();

        if ($type) {
            $payouts->where('type', $type);
        }

        $payouts = $type == 'payout' ? $payouts->where('id', '!=', 67)->first() : $payouts->simplePaginate();

        return $payouts;
    }
}
