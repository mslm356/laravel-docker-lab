<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResponseToAmlUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aml_users', function (Blueprint $table) {
            $table->json('response')->nullable()->after('user_id');
            $table->boolean('screen_result')->default(true)->after('user_id');
            $table->string('risk_result')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aml_users', function (Blueprint $table) {
            $table->dropColumn('risk_result');
            $table->dropColumn('screen_result');
            $table->dropColumn('response');
        });
    }
}
