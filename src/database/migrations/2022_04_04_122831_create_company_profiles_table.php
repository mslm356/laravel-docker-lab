<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('user_company_id')->nullable();
            $table->string('name')->nullable();
            $table->string('commercial_number')->nullable();
            $table->string('address')->nullable();
            $table->date('establishment_date')->nullable();
            $table->string('company_phone')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('office_phone')->nullable();
            $table->string('contact_person_name')->nullable();
            $table->string('mailing_address')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1 => pending');
            $table->json('data')->nullable();
            $table->string('account_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('custodian_name')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profiles');
    }
}
