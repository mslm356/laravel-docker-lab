<?php

namespace App\Models\Users;

use App\Models\AppModel;

class UserCanAccess extends AppModel
{
    protected $guarded = [];
}
