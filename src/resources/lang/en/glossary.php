<?php

return [
    'property_type' => 'Property type',
    'funding_target' => 'Funding target',
    'gross_yield' => 'Gross yield',
    'dividend_yield' => 'Dividend yield',
    'days_remaining' => 'Days remaining',
    'hours_remaining' => 'Hours remaining',
    'days' => '{1} A day|[2,*] :count days',
    'hours' => '{1} An hour|[2,*] :count hours',
    'more_detail' => 'More details',
    'get_started' => 'Get Started',
    'login' => 'Login',
    'register' => 'Register',
    'contact_us' => 'Contact us',
    'faqs' => 'Frequenlty Asked Questions',
    'investment_type' => 'Investment type',
    'listing_status' => 'Status',
    'is_closed' => 'Closed',
    'closing_soon' => 'Closing soon'
];
