<?php

namespace App\View\Components\Website;

use Illuminate\View\Component;

class ListingListItem extends Component
{
    public $name;
    public $value;
    public $tooltipId;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $value, $tooltipId = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->tooltipId = $tooltipId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.website.listing-list-item');
    }
}
