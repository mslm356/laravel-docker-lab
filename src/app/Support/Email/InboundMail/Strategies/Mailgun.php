<?php

namespace App\Support\Email\InboundMail\Strategies;

use App\Support\Email\InboundMail\InboundMailInterface;
use Illuminate\Support\Arr;

class Mailgun implements InboundMailInterface
{
    public function transform($data)
    {
        return [
            'message' => Arr::get($data, 'body-plain'),
        ];
    }

    public function verifySource($data)
    {
        $timestamp = Arr::get($data, 'timestamp');
        $token = Arr::get($data, 'token');
        $signature = Arr::get($data, 'signature');

        $encodedValue = hash_hmac('sha256', $timestamp . $token, config('inbound-mail.services.mailgun.signing_key'));

        return hash_equals($signature, $encodedValue);
    }

    public function verifySender($data, $expectedToken)
    {
        return Arr::get($data, 'token') === $expectedToken;
    }

    public function getFromAddress($data)
    {
        return Arr::get($data, 'sender');
    }

    public function getToAddress($data)
    {
        return Arr::get($data, 'recipient');
    }

    public function failureHttpCode()
    {
        return 406;
    }

    public function successResponse($data)
    {
        return response()->json([], 202);
    }

    public function failureResponse($data)
    {
        return response()->json([], $this->failureHttpCode());
    }
}
