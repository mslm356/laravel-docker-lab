<?php

namespace App\Http\Controllers\Investors\Auth;

use App\Models\Country;
use App\Services\Investors\RegisterServices;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Enums\IdentitySourceType;
use App\Enums\Elm\AbsherOtpReason;
use App\Support\Elm\Yaqeen\Yaqeen;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Models\Users\NationalIdentity;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Illuminate\Validation\ValidationException;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use Illuminate\Auth\Access\AuthorizationException;
use App\Support\Elm\Yaqeen\Exceptions\BirthDateException;
use App\Support\Elm\AbsherOtp\Exceptions\CustomerException;
use App\Support\Elm\Yaqeen\Exceptions\MissingInfoRelatedToIdException;
use Illuminate\Validation\Rule;

class SendSmsVerificationCode extends Controller
{
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Support\VirtualBanking\BankService
     * @return \Illuminate\Http\Response
     */

    public $registerServices;

    public function __construct()
    {
        $this->registerServices = new RegisterServices();
    }

    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');

        if ($user->investor && $user->investor->is_kyc_completed) {
            throw new AuthorizationException;
        }

        $data = $this->validate($request, $this->getRules($user));

        if ($this->registerServices->checkAgeOfUser($data['birth_date'], Str::startsWith($data['nin'], '1') ? "hijri" : "gregorian") == 0) {
            throw ValidationException::withMessages([
                'birth_date' => __('mobile_app/message.invalid_registration_age'),
            ]);
        }

        return DB::transaction(function () use ($data, $user) {
            $birthDateInstance = Carbon::createFromFormat('Y-m-d', $data['birth_date']);
            $info = null;
            $addresses = [];

            $yaqeen = Yaqeen::instance();

            try {
                if (Str::startsWith($data['nin'], '1')) {
                    $info = $yaqeen->getCitizenInfo($data['nin'], $birthDateInstance->format('m-Y'));
                } else {
                    $info = $yaqeen->getInfoByIqama($data['nin'], $birthDateInstance->format('m-Y'));
                }
            } catch (IdException $th) {
                throw ValidationException::withMessages([
                    'nin' => __('messages.invalid_nin'),
                ]);
            } catch (BirthDateException $th) {
                throw ValidationException::withMessages([
                    'birth_date' => __('messages.invalid_nin_birth_date'),
                ]);
            }

            try {
                if (Str::startsWith($data['nin'], '1')) {
                    $citizenAddresses = $yaqeen->getCitizenAddress($data['nin'], $birthDateInstance->format('m-Y'), 'A');

                    $addresses = array_map(function ($address) {
                        return collect($address)
                            ->map(fn ($el) => ['ar' => $el])
                            ->toArray();
                    }, $citizenAddresses);
                } else {
                    $citizenAddresses = $yaqeen->getAddressInfoByIqama($data['nin'], $birthDateInstance->format('m-Y'), 'A');

                    $addresses = array_map(function ($address) {
                        return collect($address)
                            ->map(fn ($el) => ['ar' => $el])
                            ->toArray();
                    }, $citizenAddresses);
                }
            } catch (IdException $th) {
                throw ValidationException::withMessages([
                    'nin' => __('messages.invalid_nin'),
                ]);
            } catch (BirthDateException $th) {
                throw ValidationException::withMessages([
                    'birth_date' => __('messages.invalid_nin_birth_date'),
                ]);
            } catch (MissingInfoRelatedToIdException $th) {
                throw ValidationException::withMessages([
                    'nin' => __('messages.no_national_addresses_found_in_yaqeen'),
                ]);
            }

            try {
                $verificationMsg = VerificationSms::instance()->send(
                    $data['nin'],
                    $user,
                    [
                        'reason' => __('absher_otp_purpose.' . AbsherOtpReason::VerifyIdentity, [], $user->locale ?? App::getLocale())
                    ]
                );
            } catch (CustomerException $th) {
                throw ValidationException::withMessages([
                    'nin' => __('messages.no_phone_found_in_absher_otp'),
                ]);
            }

            $user->investor->update([
                'is_identity_verified' => false,
                'data->identity_verification_tmp' => [
                    'vid' => $verificationMsg->id,
                    'identity' => $info + [
                        'user_id' => $user->id,
                        'nin_country_id' => Country::where('iso2', 'SA')->first()->id,
                        'source' => IdentitySourceType::Absher,
                    ],
                    'addresses' => $addresses

                ]
            ]);

            return response()->json([
                'data' => [
                    'vid' => $verificationMsg->id,
                    'nin' => $data['nin']
                ]
            ], 201);
        }, 1);
    }

    public function getRules($user)
    {
        $uniqueNinRule = Rule::unique(NationalIdentity::class, 'nin');

        if ($user->nationalIdentity) {
            $uniqueNinRule = $uniqueNinRule->ignoreModel($user->nationalIdentity);
        }

        return [
            'nin' => ['required', 'string', 'regex:/(1|2)\d{9}/', $uniqueNinRule],
            'birth_date' => ['required', 'string', 'date_format:Y-m-d'],
            'birth_date_type' => ['required', 'in:hijri,gregorian']
        ];
    }
}
