<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Role;
use Permission;

class PermissionReload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:reload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'reseed system permissions and assign all permissions to admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        set_time_limit(180);
        \Artisan::call('db:seed', [
            '--class' => 'PermissionsSeeder'
        ]);
        $this->info("Permissions Reloaded Successfully");
    }
}
