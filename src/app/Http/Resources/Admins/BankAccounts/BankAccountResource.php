<?php

namespace App\Http\Resources\Admins\BankAccounts;

use App\Http\Resources\Admins\BankAccounts\BankAccountListItemResource;
use App\Http\Resources\Admins\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BankAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return (new BankAccountListItemResource($this->resource))->toArray($request) + [
                'attachments' => FileResource::collection($this->attachments)
            ];
    }
}
