<?php

namespace App\Console\Commands\Anb;

use App\Enums\Banking\BankCode;
use App\Jobs\Anb\AnbEodStatement;
use App\Models\Anb\AnbTransfer;
use App\Models\BankAccount;
use App\Models\Users\User;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Enums\AnbPaymentStatus;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:check-channel {--amount=} {--date=} {--bank_id=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get PAYMENT ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $amount = money_parse_by_decimal($this->option('amount'), defaultCurrency());

            $valueDateForChannel = Carbon::parse($this->option('date'))->format('dmy');

            $bankAccount = BankAccount::find($this->option('bank_id'));

            $channel = AnbApiUtil::paymentChannel($valueDateForChannel, $amount->formatByDecimal(), $bankAccount->bank);

            $ch2 = $this->getChannel($amount, $bankAccount->bank);

            dd($channel, $ch2);

        }catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }

    }

    public function getChannel ($amount, $toBankCode)
    {
        $fromBankCode = BankCode::Anb;

        if ($fromBankCode === $toBankCode) {
            return 'ANB';
        }elseif ($amount->lessThanOrEqual(money('2000000'))) {
            return 'IPS';
        }else {
            return 'SARIE';
        }
    }

}
