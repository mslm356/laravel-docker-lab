<?php

return [
    'get_in_touch' => 'Get in touch',
    'description' => 'Nullam risus blandit ac aliquam justo ipsum. Quam mauris volutpat massa dictumst amet. Sapien tortorlacus arcu.',
    'postal_address' => 'Postal address',
    'phone_number' => 'Phone number',
    'email' => 'Email',
    'calling_center' => 'Calling center',
];