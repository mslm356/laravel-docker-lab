<?php

namespace App\Actions\Transactions;

use App\Models\Anb\AnbTransfer;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateRefundTransactionsFromAnbTransfer
{
    use AsAction;

    /**
     * Check if the investor whether the investor can invest
     *
     * @param \App\Models\Listings\Listing $listing
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $currentInvest
     * @param int $toBeInvested
     *
     * @return true|int
     * @throws \App\Exceptions\NoBalanceException
     * @throws \App\Exceptions\NoInvestorWalletFound
     */
    public function handle(AnbTransfer $transfer, $transactionReason, $amount)
    {
        $transaction = $transfer->transaction()->first();

        $transaction = $transaction->wallet->deposit($amount, [
            'reason' => $transactionReason,
            'value_date' => now()->toDateTimeString(),
            'reference' => transaction_ref(),
            'transaction_id' => $transaction->id,
            'anb_transfer_id' => $transfer->id,
            'transaction_reference' => $transaction->reference,
            'from_bank_code' => $transfer->to_bank,
            'from_iban' => $transfer->to_account,
            'source' => 'daily-job'
        ]);

        $transfer->data = array_merge($transfer->data, [
            'is_refunded' => true,
            'refund_transaction_id' => $transaction->id
        ]);

        return $transfer;
    }
}
