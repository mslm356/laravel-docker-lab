<?php


namespace App\Services\MemberShip;

use App\Enums\MemberShips\MemberShipShowPopup;
use App\Enums\MemberShips\MemberShipStatus;
use Illuminate\Support\Facades\DB;

class GetLastMemberShip
{
    public function handle($user)
    {
        $membership = [
            'current' => $user->member_ship,
            'new_request' => [
                'id' => null,
                'type' => null
            ],
            'show_popup' => false
        ];

        $lastMemberShip = DB::table('member_ship_requests')
            ->where('user_id', $user->id)
            ->orderBy('id', 'desc')
            ->first();

        if ($lastMemberShip && $lastMemberShip->status == MemberShipStatus::Pending) {

            $membership['new_request']['id'] = $lastMemberShip->id;
            $membership['new_request']['type'] = $lastMemberShip->member_ship;
            $membership['show_popup'] = $lastMemberShip->show_popup == MemberShipShowPopup::Show ? true : false;

            if ($lastMemberShip->show_popup == MemberShipShowPopup::Show) {
                $lastMemberShip->show_popup = MemberShipShowPopup::Hide;

                DB::table('member_ship_requests')
                    ->where('id', $lastMemberShip->id)
                    ->update(['show_popup'=> MemberShipShowPopup::Hide]);
            }
        }

        return $membership;
    }
}
