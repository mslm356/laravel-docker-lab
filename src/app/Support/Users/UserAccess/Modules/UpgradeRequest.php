<?php

namespace App\Support\Users\UserAccess\Modules;

use App\Enums\Investors\UserAccessMessages;

class UpgradeRequest
{

    public function handle($user, $module, $userDefaultData)
    {
        $response = 0;

        if ($userDefaultData) {
            $response = $this->defaultValidation($userDefaultData);
        }

        return $response;
    }

    public function defaultValidation($userDefaultData)
    {
        $profile = $userDefaultData['profile'];
        $nationalIdentity = $userDefaultData['nationalIdentity'];

        if (!$profile) {
            return UserAccessMessages::ProfileNotValid;
        }

        if (!$nationalIdentity || !$nationalIdentity->nin) {
            return UserAccessMessages::NinNotValid;
        }

        if (!$profile->is_kyc_completed) {
            return UserAccessMessages::KycCompleted;
        }

        if (!$profile->is_identity_verified) {
            return UserAccessMessages::IdentityVerified;
        }

        if ($profile->status != 3) {
            return UserAccessMessages::ApprovedStatus;
        }

        return 200;
    }
}
