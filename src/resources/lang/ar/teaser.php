<?php

return [
    'coming_soon' => 'انتظرونا قريبًا',
    'pre_register' => [
        'description' => 'سجل الآن وسنقوم بتنبيهك عندما يتم الإطلاق!',
        'form' => [
            'first_name' => 'الاسم الأول',
            'last_name' => 'الاسم الأخير',
            'email' => 'البريد الإلكتروني',
            'password' => 'كلمة المرور',
            'confirm_password' => 'تأكيد كلمة المرور',
            'phone_number' => 'رقم الهاتف',
            'country_code' => 'رمز الدولة',
            'nationality' => 'الجنسية',
            'register' => 'التسجيل',
            'is_living_in_ksa' => 'هل أنت مُقِيم بالسعودية؟',
            'registered_successfully' => 'تم التسجيل بنجاح. سنتواصل معك قريبًا!'
        ]
    ],
    'heading_p1' => 'استثمر',
    'heading_p2' => 'في الأصول',
    'description' => 'نوع محفظتك الاستثمارية واستثمر في الشئ الأصيل الملموس وجيبك مرتاح!'
];
