<?php

namespace App\Models\AML;

use App\Models\AppModel;
use App\Models\Users\User;

class AmlHistory extends AppModel
{
    /**
     * Get all the listing details
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checks()
    {
        return $this->hasMany(AmlListCheck::class, 'history_id', 'id');
    }

    /**
     * Get the user of history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investor()
    {
        return $this->belongsTo(User::class, 'investor_id', 'id');
    }
}
