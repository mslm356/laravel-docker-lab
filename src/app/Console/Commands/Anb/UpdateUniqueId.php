<?php

namespace App\Console\Commands\Anb;

use App\Enums\Banking\BankCode;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Models\Anb\AnbStatementEvent;
use App\Models\Anb\AnbStatementTransaction;
use App\Models\Anb\AnbTransfer;
use App\Models\BankAccount;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Models\VirtualBanking\Wallet;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Auth;
use App\Support\Anb\Http\AnbHttp;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Support\Zoho\CustomerPayments;
use Bavix\Wallet\Models\Transaction;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Cknow\Money\Money;
use Dflydev\DotAccessData\Data;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UpdateUniqueId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb-stmt:update-unique-id {--from=} {--to=} {offset} {--process_type=}';
    protected $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Anb pull stmt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $accountNumber = config('services.anb.account_number');

            $offset = $this->argument('offset') ? substr($this->argument('offset'),7) : null; // "04-09-2022SD4292659      2   04-09-202214:04:170000000111870305.23";

            do {

                $msg = AnbApiUtil::statement(
                    $accountNumber,
                    $this->option('from'),
                    $this->option('to'), 100, $offset
                );

                $offset = $msg['offset'];

                dump($offset, $msg['completed']);

                if ($this->showTransaction($msg)) {
                    break;
                }

            } while ($msg['completed'] != true);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }

    public function showTransaction($msg)
    {

        $transactions = $msg['statement']['transactions'];

        foreach ($transactions as $index => $anbTransaction) {

            if ($anbTransaction['partTrnType'] != strtoupper($this->option('process_type'))) {
                continue;
            }

            if (!isset($anbTransaction['uniqueId'])) {
                dump('unique id not found');
                continue;
            }

            if (strtolower($this->option('process_type')) == 'credit') {

               $transaction = $this->checkTransactionExists($anbTransaction);

               if ($transaction) {

                   $transaction->unique_id = $anbTransaction['uniqueId'];
                   $transaction->save();

                   dump('saved successfully');

               }else {

                   dump('transaction not found');
               }
            }
        }
    }

    public function checkTransactionExists ($anbTransaction) {

        $bankRef = $anbTransaction['refNum'] ?? null;

        $postDate = Carbon::parse($anbTransaction['valueDate']);

        $postDatY = $postDate->format('Y');
        $postDatM = $postDate->format('m');
        $postDatD = $postDate->format('d');

        $transactionExists = Transaction::where('external_reference', $bankRef)
            ->whereDay('created_at', $postDatD)
            ->whereMonth('created_at', $postDatM)
            ->whereYear('created_at', $postDatY)
            ->whereIn('reason', [
                TransactionReason::AnbStatement,
                TransactionReason::RefundAfterAnbTransferFailure
            ])
            ->limit(1)
            ->first();

        return $transactionExists;
    }
}
