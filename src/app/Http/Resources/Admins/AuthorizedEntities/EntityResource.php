<?php

namespace App\Http\Resources\Admins\AuthorizedEntities;

use App\Enums\AuthorizedEntityStatus;
use App\Enums\FileType;
use App\Enums\Role;
use App\Http\Resources\Admins\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EntityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cr_number' => $this->cr_number,
            'description' => $this->description,
            'cma_lisence' => FileResource::collection(
                $this->files->filter(function ($file) {
                    return $file->type === FileType::AuthorizedEntityCMALicense;
                })
            ),
            'cr' => FileResource::collection(
                $this->files->filter(function ($file) {
                    return $file->type === FileType::AuthorizedEntityCR;
                })
            ),
            'status' => [
                'description' => AuthorizedEntityStatus::getDescription($this->status),
                'value' => $this->status,
            ],
            'reviewer_name' => !$this->reviewer ? null : [
                'name' => $this->reviewer->full_name
            ],
            'creator' => [
                'name' => $this->creator->full_name,
                'role' => Role::getDescription($this->creatorRole->name),
                'phone_number' => $this->creator->phone_number,
            ]
        ];
    }
}
