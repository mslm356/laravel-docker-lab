<?php

namespace App\Console\Commands\Zoho;

use App\Enums\Zoho\AccountTypes;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\ZohoPayment;
use App\Support\Zoho\Account;
use App\Support\Zoho\Auth;
use App\Support\Zoho\CustomerPayments;
use App\Support\Zoho\Settings;
use App\Support\Zoho\Tax;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearData extends Command
{

    public $zohoSettings;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:clear-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clear zoho id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('users')->update(['zoho_id' => null]);
        DB::table('listings')->update(['zoho_id' => null]);
        DB::table('listings')->update(['zoho_item_id' => null]);
        DB::table('transactions')->update(['zoho_id' => null]);
        ZohoPayment::query()->truncate();
    }
}
