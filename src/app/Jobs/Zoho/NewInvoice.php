<?php

namespace App\Jobs\Zoho;

use App\Support\Zoho\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NewInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $zohoCustomer;
    public $listing;
    public $user;
    public $quantity;
    public $transaction;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listing, $user, $quantity, $transaction = null)
    {
        $this->zohoCustomer = new Invoice();
        $this->listing = $listing;
        $this->user = $user;
        $this->quantity = $quantity;
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $invoice_id = $this->zohoCustomer->store($this->listing, $this->user, $this->quantity);

        if ($invoice_id && $this->transaction) {
            $this->transaction->zoho_id = $invoice_id;
            $this->transaction->save();
        }
    }
}
