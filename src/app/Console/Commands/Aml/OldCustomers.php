<?php

namespace App\Console\Commands\Aml;


use App\Enums\Role;
use App\Models\AML\AmlUser;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Aml\AmlServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class OldCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aml:old-customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create old customers in aml';
    public $amlServices;
    public $logServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->amlServices = new AmlServices();
        $this->logServices = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {
//
//            $amlUsersIds =  AmlUser::query()->select('user_id')->get()->toArray(); //DB::table('aml_users')->select('id')->get();
//
//            $ids = array_column($amlUsersIds, 'user_id');

            User::role(Role::Investor)
                ->whereDoesntHave('amlUser')
                ->join('investor_profiles', function ($join) {
                    $join->on('users.id', '=', 'investor_profiles.user_id')
                        ->where('investor_profiles.is_kyc_completed', true);
                })
                ->lazyById()
                ->each(function ($user) {
                    $data = [
                        'type' => 'screen',
                        'user' => $user
                    ];

                    $response = $this->amlServices->handle($data);

                    dump($response);
                });

        } catch (\Exception $e) {
            $this->logServices->log('AML-JOB-EXCEPTION', $e);
        }


//        $amlOldCustomersJob = (new \App\Jobs\Aml\OldCustomers());
//        dispatch($amlOldCustomersJob);
    }
}
