<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLastMembershipRequestAndLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_can_accesses', function (Blueprint $table) {
            $table->json('last_membership_request')->nullable()->after('bankAccount');
            $table->bigInteger('upgrade_request_level')->nullable()->after('last_membership_request');

            $table->boolean('is_kyc_completed')->default(0)->after('upgrade_request_level');
            $table->boolean('kyc_suitability')->default(0)->after('is_kyc_completed');
            $table->boolean('unsuitability_confirmation')->default(0)->after('kyc_suitability');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
