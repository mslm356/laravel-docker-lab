<?php

return [
    'subject' => 'Complete your profile',
    'message' => 'You are one step away from entering the investment world',
    'user_text' => 'Our honorable investor: :name We are waiting for you on Aseel platform',
    'link_text' => 'Complete your profile to start your investment journey with us through the following link',
    'complete_profile' => 'Complete your profile'
];
