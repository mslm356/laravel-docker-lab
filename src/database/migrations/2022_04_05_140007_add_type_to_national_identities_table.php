<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToNationalIdentitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('national_identities', function (Blueprint $table) {
            $table->string('type')->default('user');
            $table->dropUnique(['nin', 'nin_country_id']);
//            $table->dropUnique(['nin', 'nin_country_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('national_identities', function (Blueprint $table) {
            //
        });
    }
}
