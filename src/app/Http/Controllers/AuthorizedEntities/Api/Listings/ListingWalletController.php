<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Enums\Banking\VirtualAccountType;
use Illuminate\Support\Facades\Validator;
use App\Actions\Listings\Wallets\GetListingBalance;
use App\Actions\Listings\Wallets\CreateListingWallet;
use App\Http\Resources\AuthorizedEntities\TransactionResource;
use App\Common\ValidationRules\Listings\ListingWalletRules;
use App\Actions\Listings\Wallets\GetListingTransactionsHistory;
use App\Actions\Listings\Wallets\MapsWalletType;
use App\Enums\Banking\WalletType;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\CanAccessListing;
use App\Http\Resources\AuthorizedEntities\Listings\ListingWalletResource;

class ListingWalletController extends Controller
{
    use CanAccessListing, MapsWalletType;

    /**
     * Create Escrow or SPV wallet for property
     * @param \Illuminate\Http\Request $request
     * @param \App\Support\VirtualBanking\BankService
     * @param \App\Models\Listings\Listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $wallets = $listing->wallets()
            ->whereIn('slug', [
                WalletType::ListingEscrow,
                WalletType::ListingSpv
            ])
            ->with('virtualAccounts')
            ->get();

        return ListingWalletResource::collection($wallets);
    }

    /**
     * Create Escrow or SPV wallet for property
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Support\VirtualBanking\BankService
     * @param \App\Models\Listings\Listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        Validator::make($request->all(), ListingWalletRules::get())->validate();

        return DB::transaction(function () use ($request, $listing) {
            CreateListingWallet::run($listing, $request->input('type'));

            return response()->json([], 201);
        });
    }

    /**
     * get listing balance
     *
     * @param App\Models\Listings\Listing $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBalance(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $type = $this->validate($request, ListingWalletRules::get())['type'];

        $balance = GetListingBalance::run($listing, $type);

        if ($balance !== null) {
            return response()->json([
                'balance' => $balance->formatByDecimal()
            ]);
        }

        return response()->json([
            'message' => trans('messages.missing_account')
        ], 400);
    }

    /**
     * get listing transactions
     *
     * @param \Illuminate\Http\Request $request
     * @param App\Models\Listings\Listing $listing
     * @param App\Support\VirtualBanking\BankService $wallet
     * @param App\Enums\WalletType $accType
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactionHistory(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $type = $this->validate($request, ListingWalletRules::get())['type'];

        $transactions = GetListingTransactionsHistory::run($listing, $type);

        if ($transactions !== null) {
            return TransactionResource::collection($transactions);
        }

        return response()->json([
            'message' => trans('messages.missing_account')
        ], 400);
    }
}
