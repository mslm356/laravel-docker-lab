<?php

namespace App\Http\Resources\Investors\Listings;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class ListingTimelineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $appLocal = App::getLocale();

        $invAppLocale = ($appLocal === 'en' ? 'ar' : 'en');

        $timeline = $this->all();

        $details = [];

        foreach ($timeline as $item) {
            $newItem = [];
            $newItem['description'] = $item['description_' . $appLocal] ?? $item['description_' . $invAppLocale] ?? null;
            $newItem['title'] = $item['title_' . $appLocal] ?? $item['title_' . $invAppLocale] ?? null;
            $newItem['date'] = $item['date'];

            $details[] = $newItem;
        }

        return $details;
    }
}
