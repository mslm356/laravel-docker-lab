<?php

namespace App\Actions\Investors;

use App\Models\File;
use App\Enums\FileType;
use App\Models\Users\User;
use Illuminate\Support\Str;
use App\Enums\Banking\WalletType;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Models\VirtualBanking\Transaction;
use App\Support\PdfGenerator\PdfGenerator;
use App\Support\Transactions\Descriptions\DescriptionManager;

class ExportTransactionReceipt
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param array $data
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function handle(Transaction $transaction, $storagePath, User $user)
    {
        $html = view('investors.receipt', [
            'user_name_en' => $user->getAbsherName('ar'),
            'transaction' => $transaction,
            'description' => DescriptionManager::getDescription($transaction, 'ar')
        ])
            ->render();

        return PdfGenerator::outputFromHtml($html, $storagePath, [
            'gotoOptions' => [
                'waitUntil' => 'networkidle0'
            ]
        ]);
    }

    public function asJob( $transaction, User $user)
    {
        $this->handle($transaction, $storagePath = "investor-receipts/{$transaction->id}.pdf",$user);

        DB::transaction(function () use ($transaction, $storagePath) {
            $file = File::create([
                'id' => (string)Str::uuid(),
                'name' => "receipt-{$transaction->id}.pdf",
                'fileable_id' => $transaction->id,
                'fileable_type' => Transaction::class,
                'path' => $storagePath,
                'extra_info' => collect([]),
                'type' => FileType::TransactionReceipt,
            ]);

            $transaction->update([
                'receipt_file_id' => $file->id,
            ]);
        });
    }

    public function authorize(ActionRequest $request)
    {
        $transaction = $request->route('transaction');

        return $transaction->wallet_id === $request->user('sanctum')->getWallet(WalletType::Investor)->id;
    }
}
