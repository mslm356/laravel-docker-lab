<?php

namespace App\Services\Campaigns\InvestorTypes;

use Illuminate\Support\Facades\DB;

class UnsuitableInvestorType
{
    public function getUsersData(array $selectedData, array $customData = [])
    {
        $data = DB::table('investor_profiles')
            ->where('investor_profiles.unsuitability_confirmation', 0)
            ->where(function ($q) {

                $q->where('annual_income', 1)
                    ->orWhereJsonContains('data->investment_xp', '!=', 3)
                    ->orWhereJsonContains('data->years_of_inv_in_securities', 0);

            })
            ->join('users', 'users.id', '=', 'investor_profiles.user_id')
            ->select($selectedData)
            ->orderBy('investor_profiles.user_id', 'asc')
            ->get();

        return json_decode($data, 1);
    }

}
