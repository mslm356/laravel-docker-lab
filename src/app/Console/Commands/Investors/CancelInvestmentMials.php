<?php

namespace App\Console\Commands\Investors;

use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Mail\Investors\CancelInvestment;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Validation\ValidationException;

class CancelInvestmentMials extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investment:cancel-mails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send mails to cancel investment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        try {

            $userIds = [
                44,
//                46694, 46447, 46312, 46302,
//                45831, 45760, 45745, 45674, 45612, 45534, 45366, 45333, 45199, 44988, 44936,
//                44754, 44542, 44437, 44367, 44321, 44211, 44207, 43883, 43853, 43700, 43665, 43593, 43371,
//                42948, 42451, 41864, 41012, 40971, 40873, 40593, 40120, 39977, 37741, 37732, 37719, 37253, 36964, 36867, 36623, 36500,
//                36483, 36452, 36226, 36120, 36069, 35857, 35803, 35796, 35745, 35361, 35252, 35251, 35189, 35097, 34998, 34658, 34331, 34276, 34191, 33666, 33542,
//                32898, 32616, 31722, 31145, 31112, 31061, 30068, 29822, 29483, 29201, 28708, 28046, 26837, 26794, 26687, 26497, 26366, 26355, 25924, 25598, 24926,
//                24358, 24304, 24222, 23942, 23903, 23643, 23430, 23401, 23354, 22987, 22604, 22436, 21637, 21359, 21116, 21079, 20914, 20842, 20631, 20221, 20207,
//                20195, 19881, 19724, 19690, 19438, 19380, 18555, 18539, 18254, 18132, 18049, 18034, 17940, 17913, 17690, 17503, 17471, 16850, 16836, 16486, 15963,
//                15594, 15148, 14898, 14871, 14770, 13919, 12096, 11026, 9507, 9479, 9465, 8908, 8475, 8390, 8218, 7781, 6587, 6537, 6107, 5263, 5050, 4941, 4899,
//                4885, 4042, 3824, 2574, 2068, 1572, 1414, 1374, 718, 605, 337, 269, 210, 205, 111, 107
            ];

            foreach ($userIds as $userId) {

                $shares = InvestorSubscriptionToken::query()
                    ->where('listing_id', 23)
                    ->whereNotNull('expired_at')
                    ->where('user_id', $userId)
                    ->sum('shares');

                $user = User::find($userId);

                $userWallet = $user->getWallet(WalletType::Investor);

                $iban = VirtualAccount::where('wallet_id', $userWallet->id)->first()->identifier;

                $sharesAmount = 1000 * $shares;

                $amount = ($sharesAmount * 1.02) + ($sharesAmount * 0.02 * 0.15);

                $amount = money($amount, defaultCurrency())->formatByDecimal();
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user->email)->send(new CancelInvestment($user, $amount, $iban));
                }

                dump('send done to user ' . $userId);
            }

        }  catch (\Exception $e) {
             dd( $e->getMessage(), $e->getFile(), $e->getLine());
        }

    }
}
