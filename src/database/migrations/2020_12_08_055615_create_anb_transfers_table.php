<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnbTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anb_transfers', function (Blueprint $table) {
            $table->id();
            $table->string('external_id')->nullable();
            $table->unsignedBigInteger('initiator_id');
            $table->unsignedBigInteger('transaction_id');
            $table->string('to_bank');
            $table->string('to_account');
            $table->decimal('amount', 64, 0);
            $table->string('currency');
            $table->string('processing_status')->nullable();
            $table->json('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anb_transfers');
    }
}
