<?php

namespace App\Http\Controllers\Admins\Api\Enquiries;

use App\Enums\EnquiryStatus;
use Illuminate\Http\Request;
use App\Mail\Enquiries\NewReply;
use App\Models\Enquiries\Enquiry;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\Enquiries\EnquiryReply;
use Illuminate\Support\Facades\Validator;
use App\Support\Email\InboundMail\InboundMail;
use App\Http\Resources\Admins\EnquiryReplyResource;

class EnquiryReplyController extends Controller
{
    /**
     * Store the reply in the storage
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Enquiry $enquiry)
    {
        return DB::transaction(function () use ($request, $enquiry) {
            $data = $this->validator($request->all())->validate();

            $reply = EnquiryReply::create([
                'enquiry_id' => $enquiry->id,
                'sender_id' => ($user = $request->user())->id,
                'message' => $data['message'],
            ]);

            $enquiry->update([
                'status' => EnquiryStatus::Open
            ]);
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($enquiry->email)->send(new NewReply($enquiry, $reply));
            }

            $reply->setRelation('sender', $user);

            return new EnquiryReplyResource($reply);
        });
    }

    /**
     * Store the reply in the storage
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeEmailReply(Request $request)
    {
        return DB::transaction(function () use ($request) {
            try {
                $inboundMail = new InboundMail();

                $reqData = $request->all();

                if (!$inboundMail->verifySource($reqData)) {
                    return $inboundMail->failureResponse($reqData);
                }

                $enquiry = $inboundMail->getEnquiry($reqData);

                EnquiryReply::create([
                    'enquiry_id' => $enquiry->id,
                    'sender_id' => $enquiry->user_id,
                    'message' => $inboundMail->transform($reqData)['message']
                ]);

                $enquiry->update([
                    'status' => EnquiryStatus::Waiting
                ]);
            } catch (\Throwable $th) {
                return $inboundMail->failureResponse($reqData);
            }

            return $inboundMail->successResponse($reqData);
        });
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, [
            'message' => ['required', 'string','max:200',textValidation()],
        ]);
    }
}
