<?php

namespace App\Jobs;

use App\Models\Listings\Listing;
use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Kreait\Firebase\Factory;

class UpdateFundPercentageFirebase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    public $percentage;
    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $percentage)
    {
        $this->id = $id;
        $this->percentage = $percentage;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $listing =Listing::where('id', $this->id)
            ->first();

        $factory = (new Factory)->withServiceAccount(base_path() . '/firebase.json');
        $factory = $factory->withDatabaseUri('https://aseelapp-c1cc2-default-rtdb.firebaseio.com/');
        $database = $factory->createDatabase();
        $database->getReference("/$this->id/")->set((int)$listing->invest_percent);
    }
}
