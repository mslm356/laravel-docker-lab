<?php

namespace App\Http\Resources\AuthorizedEntities\Listings;

use App\Http\Resources\Common\Listings\ListingValuationResource as CommonListingValuationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ValuationListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingValuationResource($this->resource))->toArray($request),
            [
                'value_fmt' => cleanDecimal(number_format($this->value->formatByDecimal()))
            ]
        );
    }
}
