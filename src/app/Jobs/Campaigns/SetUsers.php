<?php

namespace App\Jobs\Campaigns;

use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SetUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $campaign;
    public $data;
    public $alertClass;
    public $logService;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $campaign, $alertClass)
    {
        $this->alertClass = $alertClass;
        $this->campaign = $campaign;
        $this->data = $data;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $className = "App\\Services\\Campaigns\\" . $this->alertClass . "Type";

            $generator = new $className($this->data, $this->campaign);

            $dataUsedToSend = $generator->handleData();

            $this->saveCampaignUsers($dataUsedToSend['investor_data'], $this->campaign->id);

            $start_at = $this->data['start_at'] ? Carbon::parse($this->data['start_at']) : now()->addSeconds(2);

            $sendAlertJob = (new SendAlert($this->data, $this->campaign, $this->alertClass));
            dispatch($sendAlertJob)
                ->onQueue('campaigns');

        } catch (\Exception $e) {
            $this->logService->log('Campaigns:set-users-ex', $e);
        }
    }

    public function saveCampaignUsers($investor_data, $campaignId)
    {
        foreach ($investor_data as $data) {
            DB::table('campaign_users')
                ->insert([
                    'campaign_id'=> $campaignId,
                    'user_id'=> $data['id'],
                    'user_data'=> json_encode($data),
                    'created_at'=> now(),
                    'updated_at'=> now(),
                ]);
        }
    }


}
