<?php

namespace App\Console\Commands\Kyc;

use App\Jobs\Investor\InvestorProfileLogJob;
use App\Models\Investors\InvestorProfile;
use App\Models\Investors\InvestorProfileLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InvestorProfileLogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kyc:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        InvestorProfile::query()
            ->lazyById()
            ->each(function ($investor) {
                InvestorProfileLog::create(['user_id'=>$investor->user_id,'data'=>json_encode($investor),'type'=>'kyc_update']);
                $investor->update(['last_kyc_update_date'=>$investor->created_at]);
            });

//        InvestorProfileLogJob::dispatch();

    }
}
