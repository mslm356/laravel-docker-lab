<?php

return [
    'get_in_touch' => 'تواصل معنا',
    'description' => 'نسعد بتواصلكم معنا عبر الوسائل التالية:',
    'postal_address' => 'العنوان البريدي',
    'phone_number' => 'رقم الهاتف',
    'email' => 'البريد الإلكتروني',
    'calling_center' => 'مركز الاتصال',
];