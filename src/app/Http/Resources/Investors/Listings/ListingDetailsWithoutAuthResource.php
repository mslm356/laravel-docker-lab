<?php

namespace App\Http\Resources\Investors\Listings;

use App\Enums\InvestmentType;
use App\Http\Resources\Admins\Listings\ListingEditImagesResource;
use App\Services\MemberShip\SpecialCanInvest;
use Carbon\Carbon;
use App\Http\Resources\Common\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Admins\PropertyTypeResource;
use App\Http\Resources\Investors\Listings\ListingTimelineResource;

class ListingDetailsWithoutAuthResource extends JsonResource
{

    public $type;
    public $specialCanInvest;

    public function __construct($resource, $type = null)
    {
        $this->type = $type;
        $this->specialCanInvest = new SpecialCanInvest();
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $images = $this->images->sortBy("index");
        $listingTags = $this->listing_tags;

        $opened_member_ships=($this->opened_member_ships)?json_decode($this->opened_member_ships):[];

        $data = [
            'id' => $this->id,
            'title' => $this->title,
            'target' => $this->target->formatByDecimal(),
            'target_fmt' => cleanDecimal(number_format($this->target->formatByDecimal())),
            'gross_yield' => $this->gross_yield,
            'dividend_yield' => $this->dividend_yield,
            'start_date' => $this->start_time,
            'administrative_fees_percentage' => $this->administrative_fees_percentage,
            'expected_net_return' => number_format($this->expected_net_return, 2) . " " . " %",
            'deadline' => $this->deadline,
            'fund_duration' => $this->fund_duration . " " . trans("messages.month"),
//            'min_investable_shares' => $minInvestableShares,
//            'max_investable_shares' => $maxInvestableShares,
            'min_inv_share' => $this->min_inv_share,
            'min_inv_amount' => $minInvestmentAmount = $this->share_price->multiply($this->min_inv_share)->formatByDecimal(),
            'min_inv_amount_fmt' => number_format($minInvestmentAmount, 2),
            'max_inv_share' => $this->max_inv_share,
            'total_shares' => $this->total_shares,
            'rent_amount' => $this->rent_amount,
            'created_at' => $this->created_at->format('Y-m-d h:i'),
            'updated_at' => $this->updated_at->format('Y-m-d h:i'),
            'city' => new CityResource($this->whenLoaded('city')),
            'type' => new PropertyTypeResource($this->whenLoaded('type')),
            'investment_type' => InvestmentType::getDescription($this->investment_type),
            'complete_percent' => $this->fund_percent,
            'images' => ListingEditImagesResource::collection($images),
            'can_invest' =>  false,
            'investment_valid'=>$this->can_invest,
            'opened_member_ships'=>(Carbon::parse($this->early_investment_date)->isBefore(now()))?$opened_member_ships:[],
            'unit_price' =>  number_format($this->share_price->formatByDecimal(), 2),
            'authorized_entity' => $this->whenLoaded('authorizedEntity', fn () => optional($this->authorizedEntity)->name),
            'is_completed' => $this->is_completed,
            'is_closed' => $this->is_closed,
        ];

        return array_merge($data, $listingTags);
    }



    public function AdjustProprty($category)
    {
        return $this->whenLoaded('details', function () use ($category) {
            $item = $this->details->firstWhere('category', $category);
            return $item ? $item['value']->toArray() : null;
        });
    }

    public function filterAttachments($type)
    {
        $appType = $this->type;

        return ListingFileResource::collection(
            $this->attachments->filter(fn ($item) => $item->extra_info->get('type') === $type)->values(),
            $appType
        );
    }

    public function getMinAndMaxInvestableShares($user)
    {
        if (!$user) {
            return [$this->min_inv_share, $this->max_inv_share];
        }

        $investor = $this->investors->firstWhere('id', $user->id);

        if (!$investor) {
            return [$this->min_inv_share, $this->max_inv_share];
        }

        $investedShares = $investor->pivot->invested_shares;
        $remainingShares = $this->max_inv_share - $investedShares;

        if ($investedShares < $this->min_share) {
            return [$this->min_inv_share, $remainingShares];
        }

        if ($investedShares < $this->max_inv_share) {
            return [1, $remainingShares];
        }

        return [0, 0];
    }

}
