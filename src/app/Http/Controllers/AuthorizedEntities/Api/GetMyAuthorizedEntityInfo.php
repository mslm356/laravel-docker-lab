<?php

namespace App\Http\Controllers\AuthorizedEntities\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorizedEntities\AuthorizedEntityResource;

class GetMyAuthorizedEntityInfo extends Controller
{
    /**
     * Show Admin Profile.
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');

        return new AuthorizedEntityResource($user->authorizedEntity);
    }
}
