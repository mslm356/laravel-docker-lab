<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Http\Controllers\Controller;
use App\Rules\UniquePhoneNumber;
use App\Services\Investors\Users\LastForgetService;
use App\Services\LogService;
use App\Support\Elm\AbsherOtp\forgetEmailService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class forgetEmail extends Controller
{
    public $forgetEmailService;
    public $logService;
    public $lastForgetService;

    public function __construct()
    {
        $this->forgetEmailService = new forgetEmailService();
        $this->logService = new LogService();
        $this->lastForgetService = new LastForgetService();
    }

    public function forgetPhone(Request $request)
    {

        try {

            $data = $this->validate($request, $this->forgetEmailService->getNinRules());

            $responseOfLastForget = $this->lastForgetService->handle($data['nin'],'phone');

            if (isset($responseOfLastForget['status']) && !$responseOfLastForget['status']) {
                $msg = isset($responseOfLastForget['msg']) ? $responseOfLastForget['msg'] : 'please try later';
                return apiResponse(400, __($msg));
            }

            $msg = $this->forgetEmailService->sendOtp($data);

            if (!$msg) {
                return response()->json(['message' => 'data not valid'], 400);
            }

            return apiResponse(200, __('mobile_app/message.success'), $msg->id);

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-FORGET-PHONE', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('FORGET-PHONE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }

    public function resetPhone(Request $request)
    {
        try {

            $rules = $this->forgetEmailService->getRules();

            $rules ['phone_country_iso2'] = ['required_with:phone_number', 'string', 'size:2'];
            $rules ['phone'] = ['required', 'string', 'phone:phone_country_iso2', new UniquePhoneNumber('phone_country_iso2')];

            unset($rules['email']);

            $data = $this->validate($request, $rules);

            $data['phone_number'] = phone($data['phone'], $data['phone_country_iso2']);

            $this->forgetEmailService->verify($data, 'mobile-app');

            return apiResponse(200, __('mobile_app/message.success'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-RESET-PHONE', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('RESET-PHONE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }
}
