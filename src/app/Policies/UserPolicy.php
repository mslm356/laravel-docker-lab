<?php

namespace App\Policies;

use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canAccessUserWithinAuthorizedEntity(User $user, User $authorizedEntityUser)
    {
        return $user->authorized_entity_id === $authorizedEntityUser->authorized_entity_id;
    }
}
