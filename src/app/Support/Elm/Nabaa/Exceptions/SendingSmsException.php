<?php

namespace App\Support\Elm\Nabaa\Exceptions;

use Exception;

class SendingSmsException extends Exception
{
    public function __construct()
    {
        parent::__construct('Failed to send SMS verification code');
    }
}
