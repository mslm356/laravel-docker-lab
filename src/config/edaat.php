<?php

return [


    'test' =>
        [
            'base' => env('EDAAT_BASE_URL_TEST', 'https://api-test.edaaat.com'),
            'services' => [
                'auth' => [
                    'url' => env('EDAAT_BASE_URL_TEST', 'https://api-test.edaaat.com') . '/auth',
                    'method' => 'post',
                    'auth_required' => false,
                    'content_type' => 'form',

                ],
                'single_invoice_create' => [
                    'url' => env('EDAAT_BASE_URL_TEST', 'https://api-test.edaaat.com') . '/api/v2/Invoices/Single',
                    'method' => 'post',
                    'content_type' => 'json',
                    'auth_required' => true
                ],
                'single_invoice_brief' => [
                    'url' => env('EDAAT_BASE_URL_TEST', 'https://api-test.edaaat.com') . '/api/v2/Invoices/{invoice_id}/Brief',
                    'method' => 'get',
                    'content_type' => 'json',
                    'auth_required' => true
                ],
            ],
        ],

    'live' =>
        [
            'base' => env('EDAAT_BASE_URL_TEST', 'https://api.edaaat.com'),
            'services' => [
                'auth' => [
                    'url' => env('EDAAT_BASE_URL_TEST', 'https://api.edaaat.com') . '/auth',
                    'method' => 'post',
                    'auth_required' => false,
                    'content_type' => 'form',

                ],
                'single_invoice_create' => [
                    'url' => env('EDAAT_BASE_URL_TEST', 'https://api.edaaat.com') . '/api/v2/Invoices/Single',
                    'method' => 'post',
                    'content_type' => 'json',
                    'auth_required' => true
                ],
                'single_invoice_brief' => [
                    'url' => env('EDAAT_BASE_URL_TEST', 'https://api.edaaat.com') . '/api/v2/Invoices/{invoice_id}/Brief',
                    'method' => 'get',
                    'content_type' => 'json',
                    'auth_required' => true
                ],
            ],
        ],

    'wallet_charge_code' => 'charge_wallet',
    'mode' => env('EDAAT_MODE', 'test'),
    'username' => env('EDAAT_USER_NAME'),
    'password' => env('EDAAT_USER_PASSWORD'),
    'grant_type' => env('EDAAT_USER_GRANT_TYPE', 'password'),

    'attempts_count' => env('EDAAT_ATTEMPTS_COUNT', 5),
    'expire_time' => env('EXPIRE_TIME', 35),
    'limit_attempts' => env('EDAAT_LIMIT_ATTEMPTS', 2),
    'job_expire_time' => env('JOB_EXPIRE_TIME', 40),
    'transfer_amount_limit' => env('TRANSFER_AMOUNT_LIMIT', 20000),
    'limit_attempts_in_month' => env('LIMIT_ATTEMPT_LAST_MONTH', 10),


];
