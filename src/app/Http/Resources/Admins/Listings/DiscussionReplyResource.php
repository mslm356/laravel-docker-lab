<?php

namespace App\Http\Resources\Admins\Listings;

use App\Enums\Role as EnumsRole;
use App\Http\Resources\Common\Listings\DiscussionReplyResource as CommonDiscussionReplyResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Permission\Models\Role;

class DiscussionReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $canEdit = $this->user_role_id === Role::findByName(EnumsRole::Admin, 'web')->id; // TODO: update when we have multiple permissions and roles

        return array_merge(
            (new CommonDiscussionReplyResource($this->resource))->toArray($request),
            [
                'can_edit' => $canEdit,
            ],
        );
    }
}
