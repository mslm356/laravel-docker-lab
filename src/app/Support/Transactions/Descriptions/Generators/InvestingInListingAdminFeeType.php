<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorBase;
use Illuminate\Support\Arr;

class InvestingInListingAdminFeeType extends GeneratorBase
{
    protected function generateForCredit(Transaction $transaction, $locale)
    {
        $items = Arr::only($transaction->meta, [
            'user_id', 'user_name', 'listing_title_en', 'listing_title_ar', 'shares'
        ]);

        if (count($transaction->meta) !== 5) {
            return __('transaction-description.credit.invest_in_listing_admin_fee_general');
        }

        $userId = $items['user_id'];
        $userName = $items['user_name'];
        $listing = $items[locale_suff('listing_title')];
        $shares = $items['shares'];

        return __(
            'transaction-description.credit.invest_in_listing_admin_fee',
            [
                'user_id' => $userId,
                'user_name' => $userName,
                'listing' => $listing,
                'shares' => $shares
            ],
            $locale
        );
    }

    protected function generateForDebit(Transaction $transaction, $locale)
    {
        $items = Arr::only($transaction->meta, [
            'shares',
            'listing_title_en',
            'listing_title_ar',
        ]);

        if (count($items) !== 3) {
            return __('transaction-description.debit.invest_in_listing_admin_fee_general');
        }

        $shares = $items['shares'];
        $listing = $items[locale_suff('listing_title')];

        return __(
            'transaction-description.debit.invest_in_listing_admin_fee',
            compact('shares', 'listing'),
            $locale
        );
    }
}
