<?php

namespace App\Actions\Listings\Wallets;

use Illuminate\Support\Str;
use App\Models\Listings\Listing;
use App\Enums\Banking\VirtualAccountType;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Enums\Banking\VirtualAccountStatus;
use App\Support\VirtualBanking\BankService;

class CreateListingWallet
{
    use AsAction, MapsWalletType;

    /**
     * Handle creating listing wallet
     *
     * @param \App\Models\Listings\Listing $listing
     * @param string $type 'SPV' or 'ESCROW'
     *
     * @return \Bavix\Wallet\Models\Wallet
     */
    public function handle(Listing $listing, $type)
    {
        $mappedType = $this->getMappedWalletType($type);

        $wallets = config('wallet.wallet.model')::where([
            'holder_type' => $listing->getMorphClass(),
            'holder_id' => $listing->id,
        ])
            ->get();

        $wallet = $wallets->firstWhere('type', $mappedType);

        if ($wallet) {
            // TODO change to useful exception
            throw new \Exception();
        }

        $wallet = $listing->createWallet([
            'name' => $mappedType,
            'slug' => $mappedType,
            'uuid' => (string) Str::uuid(),
        ]);

        $suffix = str_pad($wallets->count() + 1, 2, '0', STR_PAD_LEFT);

        $account = (new BankService())->openVirtualAccount($listing, null, $suffix);

        $wallet->virtualAccounts()->create([
            'identifier' => $account['iban'],
            'status' => VirtualAccountStatus::Active,
            'type' => VirtualAccountType::Anb
        ]);

        return $wallet;
    }
}
