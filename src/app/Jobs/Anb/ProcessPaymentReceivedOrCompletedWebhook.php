<?php

namespace App\Jobs\Anb;

use App\Actions\Transactions\CreateRefundTransactionsFromAnbTransfer;
use App\Enums\Banking\BankCode;
use App\Enums\Banking\TransactionReason;
use App\Models\Anb\AnbTransfer;
use Illuminate\Bus\Queueable;
use App\Support\Anb\Enums\AnbPaymentStatus;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class ProcessPaymentReceivedOrCompletedWebhook implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        DB::transaction(function () {
            $payment = $this->data['payment'];

            $transfer = AnbTransfer::where('external_id', $payment['id'])
                ->where('from_bank', BankCode::Anb)
                ->lockForUpdate()
                ->first();

            if (!$transfer) {
                return;
            }

            $transfer->processing_status = $payment['status'];

            if ($payment['status'] === AnbPaymentStatus::FAILED) {
                $transfer->data = array_merge(
                    $transfer->data,
                    $payment['status'] === AnbPaymentStatus::FAILED ? [
                        'failure_reason' => $payment['reasonOfFailure']
                    ] : []
                );

                $transfer = CreateRefundTransactionsFromAnbTransfer::run(
                    $transfer,
                    TransactionReason::RefundAfterAnbTransferFailure,
                    $transfer->amount
                );
            }

            $transfer->data = array_merge(
                $transfer->data,
                [
                    'fee' => $payment['fee'] ?? null,
                    'iban' => $payment['iban'] ?? null,
                ]
            );

            $transfer->save();
        });
    }
}
