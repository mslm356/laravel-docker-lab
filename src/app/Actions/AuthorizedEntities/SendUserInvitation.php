<?php

namespace App\Actions\AuthorizedEntities;

use App\Enums\Role as EnumsRole;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Mail\AuthorizedEntities\InviteUser;
use App\Support\Users\Invitations\Invitation;

class SendUserInvitation
{
    use AsAction;

    /**
     * Create new entity
     *
     * @param \App\Models\Users\User $user
     * @param \App\Models\Users\User $inviter
     * @return void
     */
    public function handle($user, $inviter)
    {
        $invitationLink = Invitation::createInvitationLink(
            $user->email,
            Role::findByName(EnumsRole::AuthorizedEntityAdmin, 'web'),
            $inviter
        );

        if (config('mail.MAIL_STATUS') == 'ACTIVE') {
            Mail::to($user)
                ->send(
                    new InviteUser($user, $invitationLink)
                );
        }
    }
}
