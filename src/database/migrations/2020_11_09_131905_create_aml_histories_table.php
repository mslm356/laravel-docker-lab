<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmlHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aml_histories', function (Blueprint $table) {
            $table->id();
            $table->date('check_date');
            $table->unsignedBigInteger('investor_id');
            $table->unsignedTinyInteger('type');
            $table->unsignedBigInteger('initiator_id')->nullable();
            $table->timestamps();

            $table->foreign('initiator_id')->references('id')->on('users');
            $table->foreign('investor_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aml_histories');
    }
}
