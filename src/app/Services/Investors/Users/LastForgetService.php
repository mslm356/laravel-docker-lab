<?php


namespace App\Services\Investors\Users;


use App\Models\Users\LastForget;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use Carbon\Carbon;

class LastForgetService
{

    public function handle($value, $module)
    {
        $response = ['status' => true];

        $modules = [
            'password' => 'password',
            'email' => 'email',
            'phone' => 'phone',
        ];

        if (!in_array($module, $modules)) {
            return $response;
        }

        $method = $modules[$module];

        $response = $this->$method($value);

        return $response;
    }

    public function saveLastForget($user_id, $module)
    {

        $lastForget = LastForget::where('user_id', $user_id)->first();

        if (!$lastForget) {

            return LastForget::create([
                'user_id' => $user_id,
                $module . '_count' => 1,
                $module . '_date' => now(),
            ]);
        }

        $lastDateKey = $module . '_date';
        $lastCountKey = $module . '_count';

        $lastDate = Carbon::parse($lastForget->$lastDateKey);

        $anotherAttemptTime = config('lastForget.' . $module . '.another_attempt_minutes');
        $defaultAttempts = config('lastForget.' . $module . '.attempts');
        $defaultWaitMinutes = config('lastForget.' . $module . '.attempts');

        $diffInMin = $lastDate->diffInMinutes(now());

        if ($defaultAttempts < $lastForget->$lastCountKey && $diffInMin < $defaultWaitMinutes) {
            return $lastForget;
        }

        $oldAttemptsValue = $module . '_count';

        $data = [
            $module . '_count' => $diffInMin < $anotherAttemptTime ? $lastForget->$oldAttemptsValue + 1 : 1,
            $module . '_date' => now(),
        ];

        $lastForget->update($data);

        return $lastForget;
    }

    public function password($value)
    {
        $response = ['status' => true];

        $user = User::where('email', $value)->orWhere('phone_number', $value)->first();

        if (!$user) {
            $response['status'] = false;
            $response['msg'] = 'user not valid';
            return $response;
        }

        $lastLogin = $this->saveLastForget($user->id, 'password');

        if (!$this->checkAttempts($lastLogin, 'password')) {
            $response['status'] = false;
            $response['msg'] = __('messages.please_try_later_after_15_min');
            return $response;
        }

        return $response;
    }

    public function email($value)
    {
        $response = ['status' => true];

        $nin = NationalIdentity::where('nin', $value)->first();

        if (!$nin) {
            $response['status'] = false;
            $response['msg'] = 'national identity not valid';
            return $response;
        }

        $lastLogin = $this->saveLastForget($nin->user_id, 'email');

        if (!$this->checkAttempts($lastLogin, 'email')) {
            $response['status'] = false;
            $response['msg'] = 'please try later after a few minutes';
            return $response;
        }

        return $response;
    }

    public function phone($value)
    {
        $response = ['status' => true];

        $nin = NationalIdentity::where('nin', $value)->first();

        if (!$nin) {
            $response['status'] = false;
            $response['msg'] = 'national identity not valid';
            return $response;
        }

        $lastLogin = $this->saveLastForget($nin->user_id, 'phone');

        if (!$this->checkAttempts($lastLogin, 'phone')) {
            $response['status'] = false;
            $response['msg'] = 'please try later after a few minutes';
            return $response;
        }

        return $response;
    }

    public function checkAttempts($lastLogin, $module)
    {

        $defaultAttempts = config('lastForget.phone.attempts');

        $attemptKey = $module . '_count';

        $userAttempts = $lastLogin->$attemptKey;

        if ($userAttempts > $defaultAttempts) {
            return false;
        }

        return true;
    }
}
