<?php

namespace App\View\Components\SubscriptionForm;

use Illuminate\View\Component;

class Title extends Component
{
    public $field;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.subscription-form.title');
    }
}
