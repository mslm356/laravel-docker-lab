<?php

namespace App\Actions\Listings;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Support\Arr;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Validation\ValidationException;

class ValidateReservedSharesAreWithinLimit
{
    use AsAction;

    /**
     * Create or update listing
     *
     * @param \App\Models\Listings\Listing $listing
     * @param int $updatedReservedShares
     * @param \Illuminate\Support\Collection $currentInvestments
     * @return \App\Models\Listings\Listing
     * @throws \Illuminate\Validation\ValidationException
     */
    public function handle($listing, $updatedReservedShares)
    {
        // greater than 1 as PayoutsCollector account is always present by default
        if ($listing->reserved_shares !== $updatedReservedShares && $listing->investingStarted()) {
            $currentInvest = GetCurrentInvestments::run($listing)
                ->where('investor_id', '!=', User::role(Role::PayoutsCollector)->first()->id)
                ->lockForUpdate()
                ->get();

            $investedShares = $currentInvest->sum('invested_shares');

            $totalInvestedShares = $investedShares + $updatedReservedShares;

            if ($totalInvestedShares > $listing->total_shares) {
                throw ValidationException::withMessages([
                    'listing.reserved_shares' => __('validation.max.numeric', [
                        'attribute' => Arr::get(__('validation.attributes'), 'listing.reserved_shares', ''),
                        'max' => $listing->total_shares  - $investedShares
                    ])
                ]);
            }
        }
    }
}
