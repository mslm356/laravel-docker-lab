<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Enums\Banking\BankCode;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorInterface;
use Illuminate\Support\Arr;

class AnbExternalTransferType implements GeneratorInterface
{
    /**
     * Generate description
     *
     * @return string
     */
    public function generate(Transaction $transaction, $locale = null): string
    {
        $items = Arr::only($transaction->meta, ['iban', 'bank_code']);

        if (count($items) < 2) {
            return __('transaction-description.investor_ext_trans_in_general');
        }

        $iban = $items['iban'];
        $name = BankCode::getDescription($items['bank_code']);

        return __('transaction-description.investor_ext_trans', ['name' => $name, 'iban' => $iban], $locale);
    }
}
