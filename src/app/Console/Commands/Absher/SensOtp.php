<?php

namespace App\Console\Commands\Absher;

use App\Enums\Elm\AbsherOtpReason;
use App\Models\Users\User;
use App\Support\Elm\AbsherOtp\AbsherOtpSoapClient;
use App\Support\Elm\AbsherOtp\AuthBodyParams;
use App\Support\Elm\AbsherOtp\ErrorCodes;
use App\Support\Elm\AbsherOtp\Exceptions\ConfigurationException;
use App\Support\Elm\AbsherOtp\Exceptions\CustomerException;
use App\Support\Elm\AbsherOtp\Exceptions\ExternalSystemException;
use App\Support\Elm\AbsherOtp\Exceptions\InternalConnectionException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Elm\Yaqeen\Yaqeen;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;

class SensOtp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absher:send-otp {--nin=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $meta = [
            'reason' => __('absher_otp_purpose.' . AbsherOtpReason::VerifyIdentity, [], $user->locale ?? App::getLocale())
        ];

        $nin = $this->option('nin');

        try {
            $code = generateRandomCode(6);
            $client = AbsherOtpSoapClient::instantiate();

            $response = $client->__soapCall(
                'sendOTPWithDynamicTemplate',
                [
                    $requestBody = array_merge(
                        AuthBodyParams::get(),
                        [
                            'operatorId' => $nin,
                            'customerId' => $nin,
                            'language' => App::getLocale(),
                            'reason' => $meta['reason'],
                            'otpCode' => $code,
                            'otpType' => 'PROVIDED_4TO6_DIGITS',
                            'otpTemplate' => [
                                'otpTemplateId' => config('services.absher_otp.template.id'),
                                'otpParams' => [
                                    [
                                        'Name' => 'Param1',
                                        'Value' => $meta['reason'],

                                    ],
                                    [
                                        'Name' => 'Param2',
                                        'Value' => 'testing',
                                    ],
                                ],
                            ]
                        ]
                    )
                ]
            );

            dd($response,$code);

        } catch (SoapFault $th) {
            if ($fault = $th->detail->SendOTPFault ?? null) {
                $exceptionArgs = [$requestBody, $fault->ErrorCode, $fault->ErrorType, $fault->ErrorMessage, $th->faultstring, $th->faultactor];
                if (in_array($fault->ErrorCode, ErrorCodes::configurationCodes)) {
                    throw new ConfigurationException(...$exceptionArgs);
                } else if (in_array($fault->ErrorCode, ErrorCodes::customerCodes)) {
                    throw new CustomerException(...$exceptionArgs);
                } else {
                    throw new ExternalSystemException(...$exceptionArgs);
                }
            } else {
                throw new InternalConnectionException($th->getMessage());
            }
        }
    }
}
