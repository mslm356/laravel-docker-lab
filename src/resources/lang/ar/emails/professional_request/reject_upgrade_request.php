<?php

return [

    'subject' => 'عدم الموافقة على طلب ترقية الحساب',
    'welcome_message' => ':name' .' مستثمرنا الكريم  ' ,
    'message' =>'  نعتذر عن عدم قبول طلبكم لترقية الحساب إلى "مستثمر محترف"، وذلك بسبب أن   ',
    'reason' => ':reason  ',
    'thanks_message' => 'شكراً لتعاملكم مع منصة أصيل',
    'info_message' => 'وللاطلاع على لوحه التحكم الخاصة بك، يرجى الدخول إلى الرابط',
    'control_panel_btn' => 'عرض لوحه التحكم',
];
