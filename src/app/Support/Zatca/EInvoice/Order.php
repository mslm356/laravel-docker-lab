<?php

namespace App\Support\Zatca\EInvoice;

use Carbon\Carbon;
use App\Enums\Zatca\InvoiceStatus;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Zatca\ZatcaInvoice;

class Order
{
    /**
     * Transaction Reference
     *
     * @var string
     */
    protected $reference;

    /**
     * Purchased items
     *
     * @var \App\Support\Zatca\EInvoice\PurchaseLine[]
     */
    protected $items;

    /**
     * Order date
     *
     * @var \Carbon\Carbon
     */
    protected $invoiceDate;

    /**
     * Invoice
     *
     * @var \App\Models\Zatca\ZatcaInvoice
     */
    protected $invoice;

    function __construct(string $reference, array $items, Carbon $invoiceDate, ListingInvestorRecord $investingRecord)
    {
        $this->reference = $reference;
        $this->items = $items;
        $this->invoiceDate = $invoiceDate;
        $this->generateInvoice($investingRecord);
    }

    /**
     * Get order reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Get invoice
     *
     * @return \App\Models\Zatca\ZatcaInvoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Get purchased items
     *
     * @return \App\Support\Zatca\EInvoice\PurchaseLine[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Get order date
     *
     * @return \Carbon\Carbon
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * Get order subtotal (without VAT or without discount)
     *
     * @return \Cknow\Money\Money
     */
    public function getSubtotal()
    {
        return collect($this->getItems())->reduce(
            function ($carry, $item) {
                /** @var \App\Support\Zatca\EInvoice\PurchaseLine $item */
                return $carry->add($item->getLineSubtotal());
            },
            money(0)
        );
    }

    /**
     * Get order total amount
     *
     * @return \Cknow\Money\Money
     */
    public function getTotalAmount()
    {
        return collect($this->getItems())->reduce(
            function ($carry, $item) {
                /** @var \App\Support\Zatca\EInvoice\PurchaseLine $item */
                return $carry->add($item->getLineTotal());
            },
            money(0)
        );
    }

    /**
     * Get order total amount without VAT
     *
     * @return \Cknow\Money\Money
     */
    public function getTotalWithoutVat()
    {
        return $this->getSubtotal()->subtract($this->getTotalDiscount());
    }

    /**
     * Get order total discount
     *
     * @return \Cknow\Money\Money
     */
    public function getTotalDiscount()
    {
        return collect($this->getItems())->reduce(
            function ($carry, $item) {
                /** @var \App\Support\Zatca\EInvoice\PurchaseLine $item */
                return $carry->add($item->getTotalDiscountAmount());
            },
            money(0)
        );
    }

    /**
     * Get order total VAT
     *
     * @return \Cknow\Money\Money
     */
    public function getTotalVat()
    {
        return collect($this->getItems())->reduce(
            function ($carry, $item) {
                /** @var \App\Support\Zatca\EInvoice\PurchaseLine $item */
                return $carry->add($item->getTotalVatAmount());
            },
            money(0)
        );
    }

    /**
     * Generate Invoice for invoice number
     *
     * @param \App\Models\Listings\ListingInvestorRecord $investingRecord
     * @return \App\Models\Zatca\ZatcaInvoice
     */
    public function generateInvoice(ListingInvestorRecord $investingRecord)
    {
        // $zatcaInvoice = ZatcaInvoice::where('status', InvoiceStatus::Unassigned)
        //     ->lockForUpdate()
        //     ->first();

        // $modelData = [
        //     'status' => InvoiceStatus::Initiated,
        //     'invoicable_id' => $investingRecord->id,
        //     'invoicable_type' => $investingRecord->getMorphClass(),
        // ];

        // if ($zatcaInvoice === null) {
        //     $zatcaInvoice = ZatcaInvoice::create($modelData);
        // } else {
        //     $zatcaInvoice->update($modelData);
        // }

        $this->invoice = ZatcaInvoice::create([
            'invoicable_id' => $investingRecord->id,
            'invoicable_type' => $investingRecord->getMorphClass(),
        ]);

        return $this->invoice;
    }

    /**
     * Unassign the invoice to be used later after exception
     *
     * @param \App\Models\Listings\ListingInvestorRecord $investingRecord
     * @return \App\Models\Zatca\ZatcaInvoice
     */
    public function unassignInvoiceUponException()
    {
        if ($this->invoice->wasRecentlyCreated) {
            $this->invoice = ZatcaInvoice::create([
                'id' => $this->invoice->id,
                'status' => InvoiceStatus::Unassigned
            ]);
        } else {
            $this->invoice->update([
                'status' => InvoiceStatus::Unassigned
            ]);
        }
    }
}
