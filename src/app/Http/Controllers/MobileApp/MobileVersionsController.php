<?php

namespace App\Http\Controllers\MobileApp;

use App\Http\Controllers\Controller;
use App\Models\MobileVersion;
use App\Services\LogService;
use App\Traits\SendMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class MobileVersionsController extends Controller
{

    use SendMessage;

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function updateVersion(Request $request)
    {
        $rules = [
            'mobile_version' => 'required',
            'platform' => 'required|string|in:ios,android',
        ];

        try {

            $data = Validator::make($request->all(), $rules)->validate();

            $version = MobileVersion::where('platform', $data['platform'])->where('version', $data['mobile_version'])->first();

            if ($version && $version->order) {

                $latestVersion = MobileVersion::where('platform', $data['platform'])->where('order', '>', $version->order)->where('is_required', 1)->count();

                if ($latestVersion) {
                    return apiResponse(200, 'success', ['check_update' => true, 'url' => $version->url]);
                }
            }

            if (!$version) {

                MobileVersion::create([

                    'platform' => $data['platform'],
                    'version' => $data['mobile_version'],
                    'order' => null,
                ]);
            }

        } catch (ValidationException $e) {
            $code = $this->logService->log('UPDATE-MOBILE-VERSIONS', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('UPDATE-MOBILE-VERSIONS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
        return apiResponse(200, 'success', ['check_update' => false]);
    }

    public function checkUpdate(Request $request)
    {
        $rules = [
            'mobile_version' => 'required',
            'platform' => 'required|string|in:ios,android',
        ];
        try {

            $data = Validator::make($request->all(), $rules)->validate();

            $version = MobileVersion::where('platform', $data['platform'])
                ->where('version', $data['mobile_version'])->first();

            if ($version && $version->order) {

                $latestVersion = MobileVersion::where('platform', $data['platform'])->where('order', '>', $version->order)
                    ->where('is_required', 1)->count();

                if ($latestVersion) {
                    return apiResponse(200, 'success', ['check_update' => true, 'url' => $version->url]);
                }
            }

            if (!$version) {

                MobileVersion::create([

                    'platform' => $data['platform'],
                    'version' => $data['mobile_version'],
                    'order' => null,
                ]);
            }

        } catch (ValidationException $e) {
            $code = $this->logService->log('UPDATE-MOBILE-VERSIONS', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('UPDATE-MOBILE-VERSIONS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
        return apiResponse(200, 'success', ['check_update' => false]);

    }
}
