<?php

namespace App\Mail\Investors;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newBankAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';
        $profileUrl = config('app.investor_url') . '/app/overview';

        return $this->view('emails.investors.new_bank_account', compact('profileUrl', 'name'))
            ->subject(__('emails/investors/new_bank_account.subject'));
    }
}
