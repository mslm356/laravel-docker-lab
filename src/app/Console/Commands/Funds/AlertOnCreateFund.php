<?php

namespace App\Console\Commands\Funds;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Console\Command;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\Mail;
use App\Jobs\NotificationFirebaseBulk;
use App\Enums\Notifications\NotificationTypes;

class AlertOnCreateFund extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert-fund:create {--listingId=} {--type=} {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send mails when create new fund to investors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listing = Listing::findOrFail($this->option('listingId'));

        if (!$listing) {
            dd('listing id not valid');
        }

        $users = User::role(Role::Investor);

        if ($this->option('user_id')) {
            $users->where('id', $this->option('user_id'));
        }

        $users->lazyById();

        $users->each(function ($user) use ($listing) {

            if ($this->option('type') == 'email') {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new \App\Mail\Funds\CreateFund($user, $listing));
                }
            }else {

                dd('please add type option (email, notification)');
            }
        });

        if ($this->option('type') == 'notification') {
            $notification = new NotificationFirebaseBulk(NotificationTypes::Fund, $this->notificationData($listing), $users);
            dispatch($notification)->onQueue('notify');
        }

        return true;
    }

    public function notificationData($listing)
    {
        return [
            'action_id'=> $listing->id,
            'channel'=>'mobile',
            'title_en'=>'New Fund',
            'title_ar'=>'فرصة جديده',
            'content_en'=>'An opportunity to invest in the platform has been presented',
            'content_ar'=>'تم طرح فرصة للاستثمار في المنصة',
        ];
    }
}
