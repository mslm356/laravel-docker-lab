<?php

namespace App\Exports\Wallets;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\DescriptionManager;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class WalletTransactionsExport implements FromCollection, WithHeadings, WithMapping
{
    protected $startDate;
    protected $endDate;
    protected $walletIds;

    public function __construct($startDate, $endDate, $walletIds)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->walletIds = $walletIds;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        $transactions = Transaction::whereBetween(
            'created_at',
            [
                $this->startDate, $this->endDate
            ]
        )
            ->where('confirmed', true);

        if(is_array($this->walletIds))
            $transactions = $transactions->whereIn('wallet_id', $this->walletIds);

        else
            $transactions = $transactions->where('wallet_id', $this->walletIds);

        return $transactions->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Description',
            'Credit',
            'Debit',
            'Created At',
        ];
    }

    public function map($transaction): array
    {
        $amount = abs($transaction->amount_float);

        return [
            $transaction->id,
            DescriptionManager::getDescription($transaction),
            $transaction->type === Transaction::TYPE_DEPOSIT ? $amount : "0",
            $transaction->type === Transaction::TYPE_WITHDRAW ? $amount : "0",
            $transaction->created_at->toDateTime(),
        ];
    }
}
