<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Listings\ReportEnum;
use App\Enums\Role;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'type' => $this->user && $this->user->hasRole(Role::AuthorizedEntityAdmin) ? "Authorized Entity" : "Admin",
            'status' => [
                'value' => $this->status,
                'description' => ReportEnum::getDescription($this->status)
            ],
            'description' => $this->description,
        ];
    }
}
