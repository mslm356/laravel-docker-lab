<?php

return [

    'subject' => 'Your upgrade request has been denied',
    'welcome_message' => 'Dear investor  :name',
    'message' => 'We apologize for not accepting your application to upgrade your account, ',
    'reason' => 'please see the request for rejection :reason',
    'thanks_message' => 'Thank you for your dealings with Aseel platform',
    'info_message' => 'To access your control panel, please click on the link',
    'control_panel_btn' => 'View the control panel',
];
