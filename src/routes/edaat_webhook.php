<?php

use App\Http\Controllers\Webhooks\EdaatWebhookController;
use Illuminate\Support\Facades\Route;

Route::post('bill-confirmation', [EdaatWebhookController::class,'bill_confirmation']);
Route::post('payment-notification', [EdaatWebhookController::class,'payment_notification']);
