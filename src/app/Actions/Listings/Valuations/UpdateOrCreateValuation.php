<?php

namespace App\Actions\Listings\Valuations;

use App\Enums\Banking\WalletType;
use App\Jobs\Listings\AfterValuation;
use App\Models\Listings\ListingInvestor;
use App\Models\Users\User;
use Illuminate\Support\Arr;
use App\Models\Listings\ListingValuation;
use Cknow\Money\Money;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdateOrCreateValuation
{
    use AsAction;

    public function handle($data, $listing, $valuation = null)
    {
        $modelData = Arr::only(
            $data,
            ['value', 'date']
        );

        $modelData['value'] = Money::parseByDecimal(
            $modelData['value'],
            $defaultCurrency = defaultCurrency()
        );

        if ($valuation) {

            $valuation->update($modelData);

            $lastValuation = ListingValuation::query()->where('listing_id', $listing->id)->latest()->first();

            if ($lastValuation && $lastValuation->id == $valuation->id) {
                $listing->valuation_unit = $modelData['value']->getAmount();
                $listing->save();
            }

        } else {
            $valuation = ListingValuation::create(
                $modelData + [
                    'listing_id' => $listing->id,
                    'currency' => $defaultCurrency
                ]
            );

            $listing->valuation_unit = $modelData['value']->getAmount();
            $listing->save();
        }


        dispatch(new AfterValuation($listing))->onQueue('payouts');




        return $valuation;
    }
}
