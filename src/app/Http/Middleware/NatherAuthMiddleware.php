<?php

namespace App\Http\Middleware;

use App\Services\LogService;
use Closure;
use Illuminate\Http\Request;

class NatherAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $logServices = new LogService();



        $header = $request->header('Authorization');
        $local_header='Basic '.base64_encode(config('natheer.username_for_us').':'.config('natheer.password_for_us'));

        if($header != $local_header)
        {
//            $request->header_data=$header;
            $logServices->log('NATHEER-NOTIFICATION-Middleware', null, ['response', $request->all()]);
            return "UNAUTHORIZED";
        }

        return $next($request);
    }
}
