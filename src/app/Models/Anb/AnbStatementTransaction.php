<?php

namespace App\Models\Anb;

use App\Casts\Money\MoneyCast;
use App\Models\AppModel;

class AnbStatementTransaction extends AppModel
{
    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = null;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'value_date' => 'datetime',
        'post_date' => 'datetime',
        'extra_data' => 'array',
        'narrative' => 'array',
        'amount' => MoneyCast::class . ':currency',
    ];
}
