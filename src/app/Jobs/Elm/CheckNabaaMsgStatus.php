<?php

namespace App\Jobs\Elm;

use Illuminate\Bus\Queueable;
use App\Models\Elm\NabaaMessage;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Support\Elm\Nabaa\AuthBodyParams;
use App\Support\Elm\Nabaa\NabaaSoapClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckNabaaMsgStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            NabaaMessage::whereNull('detailed_status')
                ->lazyById()
                ->each(function ($msg) {
                    if ($msg->batch_id === null) {
                        $statusResponse = $this->getStatus($msg->batch_number);

                        if (!($batchId = $statusResponse->BatchIdentifier)) {
                            $msg->update([
                                'status' => $statusResponse->Status,
                                'status_description' => $statusResponse->statusDescription
                            ]);

                            return;
                        }

                        $msg->batch_id = $batchId;
                    }

                    $detailedResponse = $this->getDetailedStatus($batchId);

                    $msg->detailed_status = $detailedResponse->Recipients;

                    $msg->save();
                });
        });
    }

    protected function getStatus($batchNumber)
    {
        $client = NabaaSoapClient::instantiate();

        $response = $client->__soapCall('GetStatus', array_merge(
            [
                'BatchNumber' => $batchNumber,
            ],
            AuthBodyParams::get()
        ));

        return $response;
    }

    protected function getDetailedStatus($batchId)
    {
        $client = NabaaSoapClient::instantiate();

        $response = $client->__soapCall('GetDetailedStatus', array_merge(
            [
                'BatchIdentifier' => $batchId,
            ],
            AuthBodyParams::get()
        ));

        return $response;
    }
}
