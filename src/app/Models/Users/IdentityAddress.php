<?php

namespace App\Models\Users;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class IdentityAddress extends AppModel
{
    use HasFactory, HasTranslations;

    protected $translatable = [
        'post_code',
        'additional_number',
        'building_number',
        'city',
        'district',
        'location',
        'street_name',
        'unit_number',
    ];

    protected $cast = [
        'data' => 'array'
    ];

    public function getLocale(): string
    {
        return 'ar';
    }

    public function identity()
    {
        return $this->belongsTo(NationalIdentity::class, 'identity_id', 'id');
    }
}
