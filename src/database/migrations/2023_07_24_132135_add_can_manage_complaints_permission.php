<?php

use Spatie\Permission\Models\Permission;
use Illuminate\Database\Migrations\Migration;

class AddCanManageComplaintsPermission extends Migration
{

    protected $permissions = [
        'view-all-complaints',
        // 'create-complaints',
        'show-complaints',
        'update-complaints',
        // 'delete-complaints',
        'get-investor-complaints',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = 'complaints';

        foreach ($this->permissions as $permission) {
            Permission::create(['name' => $permission, 'group' => $group]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = Permission::whereIn(
            'name',
            $this->permissions
        )->get();

        foreach ($permissions as $permission) {
            $permission->delete();
        }
    }
}
