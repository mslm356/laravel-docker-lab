<?php

namespace App\Mail\Investors;

use App\Models\EdaatInvoice;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SdadInvoiceExport extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $invoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, EdaatInvoice $invoice)
    {
        $this->user = $user;
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/sign-up/profile';
        $name = $this->user ? $this->user->full_name : '---';

        return $this->view('emails.investors.sdad_invoice_export', compact('profileUrl', 'name'))
            ->subject(__('emails/investors/sdad_invoice_export.subject'));
    }
}
