<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShowPopupToMemberShipRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_ship_requests', function (Blueprint $table) {
            $table->tinyInteger('show_popup')->default(\App\Enums\MemberShips\MemberShipShowPopup::Show);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_ship_requests', function (Blueprint $table) {
            $table->dropColumn('show_popup');
        });
    }
}
