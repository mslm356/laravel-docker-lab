<?php

namespace App\Http\Controllers\Admins\Api;

use App\Enums\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\AuthResource;
use Illuminate\Auth\Access\AuthorizationException;

class GetAuthUser extends Controller
{
    /**
     * Show Admin Profile.
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');

        if ($user->can('access-admin-portal')) {
            return new AuthResource($user);
        }

        throw new AuthorizationException();
    }
}
