<?php

namespace App\Models;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Role;

class AuthorizedEntity extends AppModel
{
    use HasFactory;

    /**
     * Get entity banks accounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function bankAccounts()
    {
        return $this->morphMany(BankAccount::class, 'accountable');
    }

    /**
     * Get the users who are under this entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'authorized_entity_id', 'id');
    }


    /**
     * Get the listing who are under this entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listing()
    {
        return $this->hasMany(Listing::class, 'authorized_entity_id', 'id');
    }


    /**
     * Get the creator who created the entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Get the creator who created the entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creatorRole()
    {
        return $this->belongsTo(Role::class, 'creator_role_id', 'id');
    }

    /**
     * Get the reviewer who approved/rejected of the account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id', 'id');
    }

    /**
    * Get the files of uploaded by the entity
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
