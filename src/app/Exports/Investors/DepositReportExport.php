<?php

namespace App\Exports\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class DepositReportExport implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return User::role(Role::Investor)
            ->join('transactions', function ($join) {
                $join->on('users.id', '=', 'transactions.payable_id')
                    ->where('transactions.payable_type', (new User)->getMorphClass())
                    ->where('transactions.type', 'deposit')
                    ->where('transactions.created_at', '>=', $this->data['start_date'])
                    ->where('transactions.created_at', '<=', $this->data['end_date']);
            })
            ->groupBy('users.id')
            ->select('users.id', 'users.first_name', 'users.last_name', 'users.email', DB::raw('Sum(transactions.amount) as amount'))
            ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'First Name',
            'Last Name',
            'Email',
            'Amount',
        ];
    }

    public function map($user): array
    {
        return [
            $user->id,
            $user->first_name,
            $user->last_name,
            $user->email,
            ( money($user->amount)->formatByDecimal() / 100 ),
        ];
    }
}
