<?php


namespace App\Services\Investors;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Services\LogService;
use Carbon\Carbon;

class URwayServices
{
    public $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }

    public function urwayData($subscription_token, $total, $p_id)
    {
        $merchant_key = config('urWay.merchant_key');
        $terminalId = config('urWay.terminalId');
        $password = config('urWay.password');

        $total = $total->formatByDecimal();

        $data = [
            'transid' => "", //$p_id,
            'trackid' => $subscription_token,
            'terminalId' => $terminalId,
            'action' => '10',
            'merchantIp' => '',
            'password' => $password,
            'country' => 'SA',
            'currency' => 'SAR',
            'amount' => $total,
            'requestHash' => hash('sha256', $subscription_token . '|' . $terminalId . '|' . $password . '|' . $merchant_key . '|' . $total . '|SAR'),
            'address' => '',
            'city' => '',
            'udf1' => '1',
        ];

        return json_encode($data);
    }

    public function transactionStatus($subscription_token, $total, $p_id)
    {

        $urwayTransaction = $subscription_token->urwayTransaction()->where('status', 'pending')->first();

        if (!$urwayTransaction) {
            $this->logServices->log('URWAY-CHECK-STATUS', null, ['urway transaction not valid']);
            return false;
        }

        $responseOfCallUrway = $this->callUrway($subscription_token->reference, $total, $p_id);

        if (isset($responseOfCallUrway['status']) && !$responseOfCallUrway['status']) {

            $urwayTransaction->response = $responseOfCallUrway['data'];
            $urwayTransaction->status = 'failed';
            $urwayTransaction->save();
            return false;
        }

        $result = $responseOfCallUrway['data'];

        $this->updateUrwayTransaction($urwayTransaction, $p_id, $result);

        if (isset($result['responseCode']) && $result['responseCode'] === '000' && $result['result'] === 'Successful') {

            $this->depositUrwayAmount($subscription_token, $total, $result);

            $urwayTransaction->status = 'paid';
            $urwayTransaction->save();

            return true;

        } else {

            $urwayTransaction->status = 'failed';
            $urwayTransaction->save();
        }

        return false;
    }

    public function updateUrwayTransaction($transaction, $p_id, $checkTransactionStatusResult)
    {
        $transaction->response = $checkTransactionStatusResult;
        $transaction->pay_id = $p_id;
        $transaction->save();
    }

    public function depositUrwayAmount($subscription_token, $total, $urwayResult)
    {

        $user = $subscription_token->user;

        $userWallet = $user->getWallet(WalletType::Investor);

        $userWallet->deposit(
            $total->getAmount(),
            [
                'reference' => transaction_ref(),
                'reason' => TransactionReason::UrwayReason,
                'rrn' => $urwayResult['rrn'],
                'details' => [
                    'version' => 1,
                    'source' => 'urway',
                    'tranid' => $urwayResult['tranid'],
                    'trackid' => $urwayResult['trackid'],
                    'rrn' => $urwayResult['rrn'],
                ],
                'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
            ]
        );

        $userWallet->refreshBalance();
    }

    public function callUrway($token, $total, $p_id)
    {

        $response = [
            'status' => true
        ];

        try {

            $data = $this->urwayData($token, $total, $p_id);

            $urwayUrl = config('urWay.urwayUrl');

            $ch = curl_init($urwayUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );

            //        execute post
            $server_output = curl_exec($ch);

            //        close connection
            curl_close($ch);

            $response['data'] = json_decode($server_output, true);

            return $response;

        } catch (\Exception $e) {

            $response['status'] = false;
            $response['data'] = ['message' => $e->getMessage(),  'file'=> $e->getFile(), 'line'=> $e->getLine()];
            return $response;
        }

    }
}
