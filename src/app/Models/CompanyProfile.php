<?php

namespace App\Models;

use App\Enums\CompanyProfile\ApproveRequest;
use App\Models\Users\User;

class CompanyProfile extends AppModel
{
    protected $table = 'company_profiles';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function companyAccount()
    {
        return $this->belongsTo(User::class, 'user_company_id');
    }

    public function getProfileStatusAttribute()
    {
        if ($this->status == ApproveRequest::Approved) {
            return __('Approved');
        } elseif ($this->status == ApproveRequest::Rejected) {
            return __('Rejected');
        } else {
            return __('Pending');
        }
    }
}
