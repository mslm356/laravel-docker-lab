<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Styles -->
    <link href="{{ asset('css/website.css') }}" rel="stylesheet">
    <style>
        body {
            font-family: 'HelveticaNeueLT' !important;
        }

    </style>
</head>

<body>
    <div dir="rtl">
        <div class="my-6 space-y-4">
            <x-subscription-form.field-inline :field="['en' => 'Subscription form Number', 'ar' => 'رقم نموذج الاشتراك']">{{ $data['id'] }}
            </x-subscription-form.field-inline>

            <div class="mt-4 space-y-2 md:flex md:space-y-0">
                <x-subscription-form.checkbox :isChecked="$data['is_new_investor']" label="مستثمر جديد (New investor)"></x-subscription-form.field>

                        <x-subscription-form.checkbox :isChecked="!$data['is_new_investor']"
                            label="مستثمر حالي (Existing Investor)">
                            </x-subscription-form.field>
            </div>
            <x-subscription-form.field-inline :field="['en' => 'Investor No.', 'ar' => 'رقم المستثمر']">
                {{ $data['investor_id'] }}</x-subscription-form.field-inline>

            <x-subscription-form.title :field="['en' => 'Investment Particulars', 'ar' => 'تفاصيل الاسثتمار']">
            </x-subscription-form.title>

            <x-subscription-form.field :field="['en' => 'Fund name', 'ar' => 'اسم الصندوق']">{{ $data['fund_name'] }}
            </x-subscription-form.field>
            <x-subscription-form.field dark :field="['en' => 'Fund code', 'ar' => 'رقم الصندوق']">
                {{ $data['fund_id'] }}</x-subscription-form.field>
            <x-subscription-form.field :field="['en' => 'Currency', 'ar' => 'العملة']">ريال سعودي
            </x-subscription-form.field>
            <x-subscription-form.field dark :field="['en' => 'Amount (in figure)', 'ar' => 'المبلغ بالأرقام']">
                {{ $data['amount'] }}</x-subscription-form.field>
            <x-subscription-form.field :field="['en' => 'Amount (in words)', 'ar' => 'المبلغ كتابتة']">
                {{ $data['amount_words'] }} </x-subscription-form.field>
            <x-subscription-form.field dark :field="['en' => 'Value date', 'ar' => 'تاريخ التنفيذ']">
                {{ $data['value_date'] }}</x-subscription-form.field>

            <x-subscription-form.title :field="['en' => 'Payment Instructions', 'ar' => 'تعليمات الدفع']">
            </x-subscription-form.title>

            <x-subscription-form.field
                :field="['en' => 'Debit my Aseel account number', 'ar' => 'قيدًا من حسابي لدى أصيل رقم']">
                {{ $data['wallet_id'] }}</x-subscription-form.field>
            <x-subscription-form.field dark :field="['en' => 'Account currency', 'ar' => 'عملة الحساب']">ريال سعودي
            </x-subscription-form.field>

            <x-subscription-form.title :field="['en' => 'Investor\'s Particulars', 'ar' => 'تفاصيل المستثمر']">
            </x-subscription-form.title>

            <x-subscription-form.field :field="['en' => 'Type of investor', 'ar' => 'نوع المستثمر']">
                <div class="pr-4 space-y-2">
                    <x-subscription-form.checkbox :isChecked="$data['investor_type'] === 'individual'"
                        label="فرد (Individual)">
            </x-subscription-form.field>
            <x-subscription-form.checkbox :isChecked="$data['investor_type'] === 'company'" label="شركة (Company)">
                </x-subscription-form.field>
                <x-subscription-form.checkbox :isChecked="$data['investor_type'] === 'others'" label="أخرى (Others)">
                    </x-subscription-form.field>
        </div>
        </x-subscription-form.field>
        <x-subscription-form.field dark :field="['en' => 'Name', 'ar' => 'الاسم']">{{ $data['investor_name'] }}
        </x-subscription-form.field>
        <x-subscription-form.field :field="['en' => 'ID/Iqama/C.R. No.', 'ar' => 'بطاقة/إقامة/سجل تجاري رقم']">
            {{ $data['investor_national_id'] }}</x-subscription-form.field>
        <x-subscription-form.field dark :field="['en' => 'Nationality', 'ar' => 'الجنسية']">
            {{ $data['nationality'] }}</x-subscription-form.field>
        <x-subscription-form.field dark :field="['en' => 'City', 'ar' => 'المدينة']">{{ $data['investor_city'] }}
        </x-subscription-form.field>
        <x-subscription-form.field :field="['en' => 'P.Code', 'ar' => 'الرمز البريدي']">
            {{ $data['investor_po_code'] }}</x-subscription-form.field>
        <x-subscription-form.field dark :field="['en' => 'Mobile', 'ar' => 'جوال']">
            <div dir="ltr">{{ $data['investor_phone'] }}</div>
        </x-subscription-form.field>

        <div class="space-y-2 md:space-y-0 md:flex md:space-x-2">
            <div class="md:w-1/2">
                بهذا نفوضكم بإستثمار المبلغ الموضح أعلاه طبقًا لأحكام والشروط التي قراءتها وفهمتها ووقعت بقبولها و
                استلمت نسخة منها.
            </div>
            <div class="text-left md:w-1/2">
                I (We) hereby authorize you to invest the amount specified above in accordance with the terms and
                conditions of the fund which I have read, understood and signed as confirmation of acceptance and
                received a copy therof.
            </div>
        </div>
    </div>
    </div>
    <script src="{{ asset('js/iframeResizer.contentWindow.js') }}"></script>
</body>

</html>
