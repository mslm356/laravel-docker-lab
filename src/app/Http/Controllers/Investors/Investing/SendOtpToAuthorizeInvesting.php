<?php

namespace App\Http\Controllers\Investors\Investing;

use App\Http\Controllers\Controller;
use App\Services\Investors\Investment\SendOtpServices;
use Illuminate\Http\Request;

class SendOtpToAuthorizeInvesting extends Controller
{

    public $sendOtpServices;

    public function __construct()
    {
        $this->sendOtpServices = new SendOtpServices();
    }

    /**
     * Invest in the given listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, $token)
    {

        try {

            $request->request->add(['token' => $token]);
            $user = getAuthUser('web');

            $response = $this->sendOtpServices->handle($request,$user);

            return $this->handelStatusResponce($response);


        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'INVESTMENTS-SEND-OTP-INVESTMENTS');
        }
    }
}
