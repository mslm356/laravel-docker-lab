<?php

namespace App\Http\Resources\Admins\Complaints;

use Illuminate\Http\Resources\Json\JsonResource;

class ComplaintTypesListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'value' => $this->id,
            'label' => $this->name,
        ];
    }
}
