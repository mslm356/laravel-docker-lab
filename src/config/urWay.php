<?php


if (config('app.urway_mode') == 'fake') {

    return [
        /*
         |--------------------------------------------------------------------------
         | URWAY INTEGRATION TESTING
         |--------------------------------------------------------------------------
         */

        'merchant_key' => '163360ebc0183b9545e35f99f164fdf4224feb2ee55c5292bd2d96a7319cf83c',
        'terminalId' => 'investasee',
        'password' => 'investasee@123',
        'urwayUrl'=>'https://payments-dev.urway-tech.com/URWAYPGService/transaction/jsonProcess/JSONrequest'

    ];

} else {

    return [
        /*
         |--------------------------------------------------------------------------
         | URWAY INTEGRATION PRODUCTION
         |--------------------------------------------------------------------------
         */

        'merchant_key' => '431b9e5c845f5e61706d73fd1aeedee71694b930441335e6af368d802ff0c165',
        'terminalId' => 'aseel',
        'password' => 'aseel@URWAY_123',
        'urwayUrl' => 'https://payments.urway-tech.com/URWAYPGService/transaction/jsonProcess/JSONrequest'

    ];


}




