<?php

namespace App\Actions\Investors;

use App\Enums\Banking\WalletType;
use App\Exceptions\NoBalanceException;
use App\Exceptions\NoInvestorWalletFound;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Investors\InvestorLevel;
use App\Settings\InvestingSettings;
use function Doctrine\Common\Cache\Psr6\get;

class CheckIfCanInvestTesting
{
    use AsAction;

    /**
     * Check if the investor whether the investor can invest
     *
     * @param \App\Models\Listings\Listing $listing
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $currentInvest
     * @param int $toBeInvested
     *
     * @return true|int
     * @throws \App\Exceptions\NoBalanceException
     * @throws \App\Exceptions\NoInvestorWalletFound
     */
    public function handle($listing, $user, $currentInvest, $toBeInvestedShares)
    {

        $totalFundInvShares = $currentInvest->sum('invested_shares');
        $currentUserInvestments = $currentInvest->where('investor_id', $user->id);
        $currentUserInvestments2 = $currentInvest->where('investor_id', $user->id)->get();
        $totalInvestorInvShares = $currentUserInvestments->sum('invested_shares');

        $totalInvestorInvShares += $toBeInvestedShares;

        // Investor try to invest with amount less than
        // the min invest amount for the property
        if ($toBeInvestedShares < $listing->min_inv_share) {
            return 2;
        }

        // Investor try to invest with amount greater than
        // the max invest amount for the property
        if ($totalInvestorInvShares > $listing->max_inv_share) {
            return 3;
        }

        $toBeInvestedAmountWithFees = CalculateInvestingAmountAndFees::run($listing, $toBeInvestedShares)['amount'];
        if ($user->investor->level === InvestorLevel::Beginner) {
            $currentInvestedAmount = $currentUserInvestments2->reduce(fn ($acc, $investment) => $acc->add($investment->invested_amount), money(0));
            $beginnerLimit = money(app(InvestingSettings::class)->beginner_limit_per_listing);
            $toBeTotalInvestedAmount = $currentInvestedAmount->add($toBeInvestedAmountWithFees);
            if ($toBeTotalInvestedAmount->greaterThan($beginnerLimit)) {
                return 4;
            }
        }

        $totalInvestedShares = $totalFundInvShares + $toBeInvestedShares;

        // Property total invest will exceed its target
        if ($totalInvestedShares > $listing->total_shares) {
            return 1;
        }

        $wallet = $user->getWallet(WalletType::Investor);

        if (!$wallet) {
            throw new NoInvestorWalletFound();
        }

        $balance = money($wallet->balance);


        if ($balance->lessThan($toBeInvestedAmountWithFees)) {
            return 5;
//            throw new NoBalanceException();
        }

        return true;
    }
}

