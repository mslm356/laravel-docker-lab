<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthorizedEntities\Api\GetAuthUser;
use App\Http\Controllers\AuthorizedEntities\Api\GetPropertyTypes;
use App\Http\Controllers\AuthorizedEntities\Api\RegisterAsEntity;
use App\Http\Controllers\AuthorizedEntities\Api\UploadEditorImages;
use App\Http\Controllers\Common\BankAccounts\BankAccountController;
use App\Http\Controllers\AuthorizedEntities\Api\UpdateEntityProfile;
use App\Http\Controllers\AuthorizedEntities\Api\Dashboard\GetOverview;
use App\Http\Controllers\AuthorizedEntities\Api\GetMyAuthorizedEntityInfo;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ListingController;
use App\Http\Controllers\AuthorizedEntities\Api\EntityUsers\AcceptInvitation;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\GetListingRevisions;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\DiscussionController;
use App\Http\Controllers\AuthorizedEntities\Api\Dashboard\GetDashboardListings;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ListingImageController;
use App\Http\Controllers\AuthorizedEntities\Api\EntityUsers\EntityUserController;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ListingPayoutController;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ListingReportController;
use App\Http\Controllers\Admins\Api\DeleteFile;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ListingWalletController;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\DiscussionReplyController;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ListingValuationController;
use App\Http\Controllers\AuthorizedEntities\Api\BankAccounts\GetApprovedBankAccounts;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ExportListingTransactions;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\ListingAttachmentController;
use App\Http\Controllers\AuthorizedEntities\Api\InvestorInvestments;

Route::post('/sign-up/invitations', AcceptInvitation::class)->middleware('recaptcha.v3');
Route::post('register', RegisterAsEntity::class)->middleware('recaptcha.v3');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/editor-images', UploadEditorImages::class);

    Route::get('/property-types', GetPropertyTypes::class);

    Route::get('/bank-accounts/approved', GetApprovedBankAccounts::class);
    Route::apiResource('bank-accounts', BankAccountController::class)->only(['index', 'store']);
    Route::apiResource('users', EntityUserController::class)->only(['index', 'store', 'update', 'show']);

    Route::post('/listings/{listing}/discussions/{discussion}/replies', [DiscussionReplyController::class, 'store']);
    Route::put('/listings/{listing}/discussions/{discussion}/replies/{reply}', [DiscussionReplyController::class, 'update']);
    Route::delete('/listings/{listing}/discussions/{discussion}/replies/{reply}', [DiscussionReplyController::class, 'destroy']);
    Route::get('/listings/{listing}/discussions', [DiscussionController::class, 'index']);
    Route::get('/listings/{listing}/discussions/{discussion}', [DiscussionController::class, 'show']);
    Route::delete('/listings/{listing}/discussions/{discussion}', [DiscussionController::class, 'destroy']);

    // TODO: need review
    Route::get('/listings/{listing}/revisions', GetListingRevisions::class);
    Route::get('/listings/{listing}/wallets', [ListingWalletController::class, 'index']);
    Route::post('/listings/{listing}/wallets', [ListingWalletController::class, 'store']);
    Route::get('/listings/{listing}/transactions', [ListingWalletController::class, 'getTransactionHistory']);
    Route::get('/listings/{listing}/transactions/export', ExportListingTransactions::class);
    Route::get('/listings/{listing}/balance', [ListingWalletController::class, 'getBalance']);
    // TODO: need review after investing
    Route::get('/listings/{listing}/investors', [ListingController::class, 'getInvestors']);
    // TODO: need review after investing
    Route::post('/listings/{listing}/payouts/{payout}/pay', [ListingPayoutController::class, 'pay']);

    Route::apiResource('listings.files', ListingAttachmentController::class)->except(['index', 'update']);
    // TODO: need review after investing
    Route::apiResource('listings.payouts', ListingPayoutController::class)->only(['index', 'store', 'show']);
    Route::apiResource('listings.reports', ListingReportController::class)->except(['destroy']);
    Route::delete('/files/{file}', DeleteFile::class);
    Route::apiResource('listings.valuations', ListingValuationController::class);
    Route::apiResource('listings.images', ListingImageController::class)->except(['index', 'update']);
    Route::post('sort-images', [ ListingImageController::class,'sortImages' ]);

    Route::apiResource('listings', ListingController::class)->except(['destroy']);

    Route::get('/auth', GetAuthUser::class);
    Route::get('/investors/investments', [ InvestorInvestments::class, 'Search']);
    Route::get('/entity/profile', GetMyAuthorizedEntityInfo::class);
    Route::put('/entity/profile', UpdateEntityProfile::class);

    Route::get('/dashboard/listings', GetDashboardListings::class);
    Route::get('/dashboard/overview', GetOverview::class);
});
