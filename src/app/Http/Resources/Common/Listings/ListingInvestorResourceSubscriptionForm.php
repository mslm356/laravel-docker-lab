<?php

namespace App\Http\Resources\Common\Listings;

use Illuminate\Support\Arr;
use App\Enums\Investors\Data\InvestorKycEnum;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingInvestorResourceSubscriptionForm extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'id' => $this->id,
                'listing_id' => $this->listing_id,
                'investor_id' => $this->investor_id,
                'invested_shares' => $this->invested_shares,
                'invested_amount' => $this->invested_amount,
                'paid_payout' => $this->paid_payout,
                'currency' => $this->currency,
                'created_at' => $this->created_at->format('Y-m-d'),
                'updated_at' => $this->updated_at->format('Y-m-d'),
                'listing' => $this->whenLoaded('listing', function () {
                    return [
                        'title_ar' => $this->listing->title_ar,
                        'title_en' => $this->listing->title_en,
                    ];
                }),
            ];
    }
}
