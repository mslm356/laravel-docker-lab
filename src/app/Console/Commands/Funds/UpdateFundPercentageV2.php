<?php

namespace App\Console\Commands\Funds;

use App\Enums\Role;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Services\CacheServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Jobs\UpdateFundPercentageFirebase;

class UpdateFundPercentageV2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'funds:update-percentage-v2 {--listing_id=} {--shares=}';

    public $cacheServices;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update fund percentage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->cacheServices = new CacheServices();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $listing = Listing::find($this->option('listing_id'));

        if (!$listing) {
            dd('fund not valid');
        }

        $shares =  DB::transaction(function () use ($listing) {

            $listingInvestorShares = DB::table('listing_investor')
                ->where('listing_id', $this->option('listing_id'))
                ->sum('invested_shares');

            $investment_shares = $listingInvestorShares + $this->option('shares');

            $shares = $listing->total_shares - $investment_shares;

            $listing->update([
                'investment_shares' => $shares
            ]);

            return $shares;

        }, 1);

        $this->cacheServices->updateFundCache($this->option('listing_id'));

        UpdateFundPercentageFirebase::dispatch($listing->id, $listing->percentage)->onQueue('firebase');

        dd($shares);
    }
}
