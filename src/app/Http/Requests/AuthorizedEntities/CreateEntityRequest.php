<?php

namespace App\Http\Requests\AuthorizedEntities;

use Illuminate\Foundation\Http\FormRequest;

class CreateEntityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:100',textValidation()],
            'cr_number' => ['required', 'string', 'max:100'],
            'description' => ['required', 'string', 'max:2000',textValidation()],
//            'cma_license' => ['required', 'mimes:png,jpeg,pdf,jpg', 'max:2048'],
            'cma_license' => ['required' ,'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/heif,image/heic', 'max:4096'],
            'cr' => ['required', 'array', 'min:1', 'max:10'],
        ];
    }
}
