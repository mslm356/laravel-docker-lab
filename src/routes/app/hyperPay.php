<?php

use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\HyperPay\HyperPayController;


Route::middleware(['auth:api', 'jwt_check:api', 'role:' . Role::Investor ])->group(function () {

    Route::post('payment-checkout',[ HyperPayController::class , 'checkout']);
    Route::post('get-payment-status',[ HyperPayController::class , 'getPaymentStatus']);
    Route::get('get-payment-methods',[ HyperPayController::class , 'getPaymentMethods']);

});
