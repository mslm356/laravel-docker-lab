<?php

namespace App\Models;

use App\Models\Users\User;
use App\Support\QueryScoper\HasScopes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BankAccount extends AppModel
{
    use HasFactory, HasScopes;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_approved' => 'boolean',
    ];

    /**
     * Get to which this account belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function accountable()
    {
        return $this->morphTo('accountable');
    }

    /**
     * Get the reviewer (approved/rejected) of the account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id', 'id');
    }

    /**
     * Get attachments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
