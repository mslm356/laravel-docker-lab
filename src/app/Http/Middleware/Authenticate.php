<?php

namespace App\Http\Middleware;
use App\Exceptions\AuthenticationException;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use PHPUnit\Util\Exception;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return investor_url('login');
        }
    }

//    protected function unauthenticated($request, array $guards)
//    {
//        throw new AuthenticationException(
//            'Unauthenticated.', $guards, $this->redirectTo($request)
//        );
//
//    }

}
