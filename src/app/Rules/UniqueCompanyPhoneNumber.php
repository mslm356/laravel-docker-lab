<?php

namespace App\Rules;

use App\Models\CompanyProfile;
use Illuminate\Contracts\Validation\Rule;

class UniqueCompanyPhoneNumber implements Rule
{
    protected $countryAttribute;
    protected $ignore;
    protected $attribute;

    public function __construct($countryAttribute)
    {
        $this->countryAttribute = $countryAttribute;
    }

    public function ignore(CompanyProfile $companyProfile)
    {
        $this->ignore = $companyProfile;

        return $this;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        $countryCode = request()->input($this->countryAttribute);

        return CompanyProfile::where('phone_number', phone($value, $countryCode))
            ->when(isset($this->ignore), function ($query) {
                $query->where('id', '!=', $this->ignore->id);
            })
            ->count() === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique', [
            'attribute' => __('validation.attributes')[$this->attribute]
        ]);
    }
}
