<?php

namespace App\Actions\Investors;

use App\Actions\Listings\GetCurrentInvestments;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use App\Models\Users\InvestorSubscriptionToken;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateSubscriptionToken
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param \App\Models\Users\User $user
     * @param \App\Models\Listings\Listing $listing
     * @param float $amount
     * @param \Illuminate\Support\Collection $currentInvestments
     * @return void
     */
    public function handle(User $user, Listing $listing, $shares)
    {
        return InvestorSubscriptionToken::create([
            'user_id' => $user->id,
            'listing_id' => $listing->id,
            'shares' => $shares
        ]);
    }

    public function asController(Request $request, Listing $listing)
    {
        $data = Validator::make($request->input(), [
            'shares' => ['required', 'integer', 'min:1'],
        ])
            ->validate();

        $case = CheckIfCanInvest::run(
            $listing,
            $request->user('sanctum'),
            GetCurrentInvestments::run($listing)->get(),
            $data['shares']
        );

        if ($case === true) {
            return response()->json([
                'data' => $this->handle(
                    $request->user(),
                    $listing,
                    $data['shares'],
                )['id'],
            ]);
        }

        return response()->json([
            'message' => trans('messages.invest_policy_' . $case)
        ], 400);
    }
}
