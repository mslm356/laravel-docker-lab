<?php

namespace App\Jobs\Investor;

use App\Models\Investors\InvestorProfile;
use App\Models\Investors\InvestorProfileLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvestorProfileLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        InvestorProfile::query()
           ->lazyById()
           ->each(function ($investor) {
               InvestorProfileLog::create(['user_id'=>$investor->user_id,'data'=>json_encode($investor),'type'=>'kyc_update']);
               $investor->update(['last_kyc_update_date'=>$investor->created_at]);
           });
    }
}
