<?php

namespace App\Exports\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class TransactionReportExport implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('transactions')->where('created_at', '>=', $this->data['start_date'])
            ->where('created_at', '<=', $this->data['end_date'])->get();
    }

    public function headings(): array/**/
    {
        return [
            '#',
            'Payable Type',
            'Payable Id',
            'Wallet Id',
            'Type',
            'Amount',
            'Confirmed',
            'Meta',
            'Value Date',
            'External Reference',
            'Reason',
            'Reference',
            'Uuid',
            'Created_at',
            'Updated_at',
        ];
    }

    public function map($transaction): array
    {
        return [
            $transaction->id,
            $transaction->payable_type,
            $transaction->payable_id,
            $transaction->wallet_id,
            $transaction->type,
            ( money($transaction->amount)->formatByDecimal()  ),
            $transaction->confirmed,
            $transaction->meta,
            $transaction->value_date,
            $transaction->external_reference,
            $transaction->reason,
            $transaction->reference,
            $transaction->uuid,
            $transaction->created_at,
            $transaction->updated_at,
        ];
    }
}
