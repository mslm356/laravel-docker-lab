<?php

namespace App\Settings;

use App\Models\Accounting\Vat;
use App\Settings\Casters\EloquentCaster;
use Spatie\LaravelSettings\Settings;

class AccountingSettings extends Settings
{
    public Vat $vat;

    public static function casts(): array
    {
        return [
            'vat' => EloquentCaster::class . ':' . Vat::class
        ];
    }

    public static function group(): string
    {
        return 'accounting';
    }
}
