<?php

use App\Models\Investors\InvestorUpgradeItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestorUpgradeRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor_upgrade_items', function (Blueprint $table) {
            $table->id();
            $table->json('text');
            $table->boolean('is_active');
            $table->timestamps();
        });

        foreach ($this->getItems() as $item) {
            InvestorUpgradeItem::create($item);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor_upgrade_items');
    }

    public function getItems()
    {
        return [
            [
                'text' => [
                    'en' => 'أن يكون قد قام بصفقات في أسـواق الأوراق الماليـة لا يقـل مجموع قيمتهـا عـن أربعين مليون ريال سعودي و لا تقـل عـن عشـر صـفقات في كـل ربـع سـنة خلال الأثني عشر شهراً الماضية.',
                    'ar' => 'أن يكون قد قام بصفقات في أسـواق الأوراق الماليـة لا يقـل مجموع قيمتهـا عـن أربعين مليون ريال سعودي و لا تقـل عـن عشـر صـفقات في كـل ربـع سـنة خلال الأثني عشر شهراً الماضية.',
                ],
                'is_active' => true
            ],
            [
                'text' => [
                    'en' => 'ألا تقل قيمة صافي أصوله عن خمسة ملايين ريال سعودي.',
                    'ar' => 'ألا تقل قيمة صافي أصوله عن خمسة ملايين ريال سعودي.',
                ],
                'is_active' => true
            ],
            [
                'text' => [
                    'en' => 'أن يعمـل أو سـبق لـه العمـل مـدة ثلاث سـنوات علـى الأقل في القطـاع المالي في وظيفة مهنية تتعلق بالاستثمار في الأوراق المالية.',
                    'ar' => 'أن يعمـل أو سـبق لـه العمـل مـدة ثلاث سـنوات علـى الأقل في القطـاع المالي في وظيفة مهنية تتعلق بالاستثمار في الأوراق المالية.',
                ],
                'is_active' => true
            ],
            [
                'text' => [
                    'en' => 'أن يكون حاصلاً علـى شـهادة مهنيـة متخصصـة في مجال أعمـال الأوراق المالية معتمدة من جهة معترف بها دوليًا.',
                    'ar' => 'أن يكون حاصلاً علـى شـهادة مهنيـة متخصصـة في مجال أعمـال الأوراق المالية معتمدة من جهة معترف بها دوليًا.',
                ],
                'is_active' => true
            ],
            [
                'text' => [
                    'en' => 'أن يكون حاصلاً على الشهادة العامة للتعامل في الأوراق المالية المعتمدة من قبـل الهيئة، على ألا يقل دخله السـنوي عـن سـتمائة ألـف ريـال سـعودي في السنتين الماضيتين.',
                    'ar' => 'أن يكون حاصلاً على الشهادة العامة للتعامل في الأوراق المالية المعتمدة من قبـل الهيئة، على ألا يقل دخله السـنوي عـن سـتمائة ألـف ريـال سـعودي في السنتين الماضيتين.',
                ],
                'is_active' => true
            ],
        ];
    }
}
