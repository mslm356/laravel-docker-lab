<?php

namespace App\View\Components\SubscriptionForm;

use Illuminate\View\Component;

class Checkbox extends Component
{
    public $isChecked;
    public $label;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($label, $isChecked = false)
    {
        $this->label = $label;
        $this->isChecked = $isChecked;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.subscription-form.checkbox');
    }
}
