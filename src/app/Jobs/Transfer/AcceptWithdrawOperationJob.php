<?php

namespace App\Jobs\Transfer;

use App\Enums\Transfer\TransferStatus;
use App\Exceptions\FailedProcessException;
use App\Exceptions\NoBalanceException;
use App\Models\Transfer\TransferRequest;
use App\Services\Investors\TransferServices;
use App\Services\LogService;
use App\Services\Transfer\ApproveWithdrawOperations;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AcceptWithdrawOperationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var LogService
     */
    public $logService;
    public $withdrawOperations;
    public $transferRequestId;
    public $transferServices;


    public function __construct($transferRequestId)
    {
        $this->logService = new LogService();
        $this->transferRequestId = $transferRequestId;
        $this->withdrawOperations = new ApproveWithdrawOperations();
        $this->transferServices = new TransferServices();
    }

    public function handle()
    {
        try {

            $transferRequest = TransferRequest::where('status', TransferStatus::Pending)
                ->where('id', $this->transferRequestId)
                ->first();

            if (!$transferRequest) {
                $this->logService->log(
                    'TRANSFER-REQUEST-24-HOURS-REQUEST-NOT-FOUND',
                    null,
                    ['request id not found', $this->transferRequestId]
                );
                return false;
            }

            $this->withdrawOperations->accept($transferRequest);

        } catch (NoBalanceException $e) {
            $this->logService->log('NoBalanceException-TRANSFER-Admin', $e);
        } catch (FailedProcessException $e) {
            $this->logService->log('FailedProcessException-TRANSFER-Admin', $e);
        } catch (\Exception $e) {
            $this->logService->log('JOB-ACCEPT-WITHDRAW-OPERATION-ERROR-24', $e);
        }
    }

}
