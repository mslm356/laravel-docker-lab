<?php

namespace App\Console\Commands\Investors;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Enums\Role;
use App\Jobs\Funds\RegenerateSubscriptionPdf;
use App\Jobs\ReminderProfile;
use App\Models\Listings\ListingInvestor;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;

class GeneratePdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $regenerateSubscriptionPdf = ( new RegenerateSubscriptionPdf());
        dispatch($regenerateSubscriptionPdf);

    }


}


