<?php

namespace Database\Seeders;

use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(config('app.env') != 'production') {
            $admin = [
                'first_name' => 'admin',
                'last_name' => 'can',
                'email' => 'admin@admin.com',
                'email_verified_at' => now(),
                'password' => Hash::make('Test^^778'), // password
                'remember_token' => Str::random(10),
                'type' => 'user'
            ];

            $user = User::create($admin);

            $user->roles()->sync(Role::where('name', 'Admin')->first());

            $complaintAdmin = [
                'first_name' => 'admin',
                'last_name' => 'can',
                'email' => 'complaint@admin.com',
                'email_verified_at' => now(),
                'password' => Hash::make('Test^^778'), // password
                'remember_token' => Str::random(10),
                'type' => 'user'
            ];

            $user = User::create($complaintAdmin);
            $user->roles()->sync(Role::where('name', 'Admin')->first());

            $user->permissions()->sync(Permission::where('name', 'manage-complaints')->first());
        }
    }
}
