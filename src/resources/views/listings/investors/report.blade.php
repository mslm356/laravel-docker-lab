<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        @include('components.print-components.watermark',
        [
        'website' => 'https://investaseel.sa' ,
        'user_name' => $user_name_en ,
        'date'=> \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]
        )
        @font-face {
            font-family: 'HelveticaNeueLT';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/HelveticaNeueLTArabic-Roman.woff2'))) }}") format('woff2');
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'HelveticaNeueLT';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/HelveticaNeueLTArabic-Bold.woff2'))) }}") format('woff2');
            font-weight: bold;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Alata';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/Alata-Regular.woff2'))) }}") format('woff2');
            font-weight: bold;
            font-style: normal;
            font-display: swap;
        }

        body {
            font-family: 'HelveticaNeueLT';
        }

    </style>
</head>
<body>
<div>
    <div>

        <div
            style="display: flex; align-content: center ;align-items: center ; flex-direction: column; margin-top: 50px">
            {{--            TODO:Dynamic Logo URL--}}
            {{--            <img style="width: 150px; height: auto" src="{{asset('images/aseel-blue.png')}}" />--}}
            <img style="width: 150px; height: auto"
                 src="https://admin-pre.investaseel.com/static/media/aseel-logo-dark.074f0b67.png"/>
            <h3>
                Fund:{{$listing->title_en}}-#{{$listing->id}}-Investors
            </h3>
        </div>


        <div>


            <!-- table s -->
            <table border="1" cellpadding="3" style="width: 100%; text-align: center">
                <tbody>
                <tr>
                    <th style="width:40%">Name</th>
                    <th style="width:15%">ID</th>
                    <th style="width:10%">Units</th>
                    <th style="width:20%">Invested Amount</th>
                    <th style="width:15%">KYC Status</th>
                    <th style="width:15%">Type</th>
                </tr>
                @foreach($investors as $investor)
                    <tr>
                        <td>{{$investor['full_name']}}</td>
                        <td>{{$investor['nin']}}</td>
                        <td>{{$investor['invested_shares']}}</td>
                        <td>{{$investor['invested_amount']}}</td>
                        <td>{{$investor['kyc_status']}}</td>
                        <td>{{$investor['type']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
    </div>
</div>

</body>
