<?php

namespace App\Support\Users\Invitations;

use BenSampo\Enum\Enum;

final class ValidationResponse extends Enum
{
    /**
     * Constant representing an invalid token.
     *
     * @var string
     */
    const INVALID_TOKEN = 'invalid_token';

    /**
     * Constant representing an invalid token.
     *
     * @var string
     */
    const INVALID_INVITATION = 'invalid_invitation';

    /**
     * Constant representing that the user has accepted invitation.
     *
     * @var string
     */
    const USER_EXISTS = 'user_exists';

    /**
     * Constant representing an successful invitation.
     *
     * @var string
     */
    const INVITATION_ACCEPTED = 'invitation_accepted';
}
