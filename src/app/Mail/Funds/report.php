<?php

namespace App\Mail\Funds;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class report extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $listing;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Listing $listing)
    {
        $this->user = $user;
        $this->listing = $listing;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';
        $fund_title = $this->listing && $this->user->locale == "ar" ? $this->listing->title_ar : $this->listing->title_en;

        $listingUrl = investor_url('funds/' . $this->listing->id);

        return $this->view('emails.funds.report', compact('name', 'listingUrl', 'fund_title'))
            ->subject(__('emails/funds/report.subject', ['fund_title'=> $fund_title]));
    }
}
