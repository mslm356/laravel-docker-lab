<?php

namespace App\Http\Controllers\Admins\Api;

use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Support\Facades\Storage;

class DeleteFile extends Controller
{
    /**
     * Delete file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(File $file)
    {
        $isDeleted = $file->deleteWithFile();

        if (!$isDeleted) {
            return response()->json([
                'message' => __('messages.failed_to_delete_file')
            ], 400);
        }

        return response()->json();
    }

}
