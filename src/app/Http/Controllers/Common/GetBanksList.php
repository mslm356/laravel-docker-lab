<?php

namespace App\Http\Controllers\Common;

use App\Enums\Banking\BankCode;
use App\Http\Controllers\Controller;

class GetBanksList extends Controller
{
    /**
     * Listings list
     *
     * @param
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function __invoke()
    {
        $banks = collect(BankCode::asSelectArray())->map(function ($name, $code) {
            return [
                'code' => $code,
                'name' => $name
            ];
        })->values()->all();

        return response()->json([
            'data' => $banks
        ]);
    }
}
