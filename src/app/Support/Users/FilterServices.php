<?php


namespace App\Support\Users;

use App\Enums\Investors\Filter\FilterTypes;
use App\Enums\Role;
use App\Models\SubscriptionFormRequest;
use App\Models\Users\User;

class FilterServices
{
    public function filter($request)
    {
        $investors = User::role(Role::Investor)->with('investor')
            ->join('investor_profiles', 'users.id', '=', 'investor_profiles.user_id');

        if ($request->has("value") && $request->value != null) {
            if ($request->has("type") && $request->type == FilterTypes::Id) {
                $investors->join('national_identities', function ($join) use ($request) {
                    $join->on('users.id', '=', 'national_identities.user_id')
                        ->where('national_identities.nin', $request->value);
                });
            }


            if ($request->has("type") && $request->type == FilterTypes::Name) {
                $investors->whereRaw("concat(first_name, ' ', last_name) like '%$request->value%' ");
            }

            if ($request->has("type") && $request->type == FilterTypes::Email) {
                $investors->where("email", "like", "%$request->value%");
            }
            if ($request->has("type") && $request->type == FilterTypes::Phone) {
                $investors->where("phone_number", "like", "%$request->value%");
            }
            if ($request->has("type") && $request->type == FilterTypes::All) {
                $investors->where(function ($q) use ($request) {
                    $q->orWhereRaw("concat(first_name, ' ', last_name) like '%$request->value%' ")
                        ->orWhere('email', 'like', '%' . $request['value'] . '%')
                        ->orWhere('phone_number', 'like', '%' . $request['value'] . '%');
                });

            }
        }

        if ($request->has("kyc") && $request->kyc != null) {
            if ($request->kyc == 2) {
                $request->kyc = 0;
            }

            $investors->where('investor_profiles.is_kyc_completed', $request->kyc);
        }

        if ($request->has("type_user") && $request['type_user'] != null) {
            $investors->where('users.type', $request['type_user']);
        } else {
            $investors->where('users.type', 'user');
        }

        if ($request->has("status") && $request->status != null) {
            $investors->where('investor_profiles.status', $request->status);
        }

        if ($request->has("member_ship") && $request->member_ship != null) {
            $investors->where('member_ship', $request->member_ship);
        }


        if ($request->has("aml") && $request->aml != null) {
            $investors->join('aml_histories', function ($join) use ($request) {
                $join->on('users.id', '=', 'aml_histories.investor_id')
                    ->where('aml_histories.type', $request->aml);
            });
        }
        $investors->select("users.*");

        return $investors;
    }

    public function filterWithSubscriptionForm($request)
    {
        $subscriptionRequests = SubscriptionFormRequest::query();

        if ($request->has("type") && $request->type == 2) {
            $subscriptionRequests->join('users', function ($join) use ($request) {
                $join->on('subscription_form_requests.user_id', '=', 'users.id')
                    ->whereRaw("concat(users.first_name, ' ', users.last_name) like '%$request->value%' ");
            });
        }

        if ($request->has("type") && $request->type == 1) {
            $subscriptionRequests->join('national_identities', function ($join) use ($request) {
                $join->on('subscription_form_requests.user_id', '=', 'national_identities.user_id')
                    ->where('national_identities.nin', $request->value);
            });
        }

        if ($request->has("status") && $request->status != null) {
            $subscriptionRequests->where('subscription_form_requests.status', $request->status);
        }

        $subscriptionRequests->select("subscription_form_requests.*");

        return $subscriptionRequests;
    }

}
