<?php


namespace App\Support\Zoho;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Auth
{
    public function generateToken($code)
    {
        $response = Http::asForm()
            ->withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ])
            ->post('https://accounts.zoho.com/oauth/v2/token?', [
                'code' => $code,
                'grant_type' => 'authorization_code',
                'client_id' => config('zoho.client_id'),
                'client_secret' => config('zoho.client_secret'),
                'redirect_uri' => 'https://asdfg.investaseel.com/',
                'scope' => 'ZohoBooks.fullaccess.all',
            ]);

        return json_decode($response, true);
    }

    public function generateRefreshToken($tokenData)
    {
        $response = Http::asForm()
            ->withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ])
            ->post('https://accounts.zoho.com/oauth/v2/token?', [
                'refresh_token' => $tokenData->refresh_token,
                'grant_type' => 'refresh_token',
                'client_id' => config('zoho.client_id'),
                'client_secret' => config('zoho.client_secret'),
                'redirect_uri' => 'https://asdfg.investaseel.com/',
            ]);

        $responseData = json_decode($response->body(), true);

        $this->saveToken($responseData, $tokenData->id);

        return json_decode($response->body(), true)['access_token'];
    }

    public function getToken($code = null)
    {
        try {

            $tokenBody = DB::table('zoho_tokens')->first();

            if ($tokenBody) {

                if ($this->checkTokenIsExpire($tokenBody->expire_in)) {

                    return $this->generateRefreshToken($tokenBody);
                }

                return $tokenBody->access_token;
            }

            $tokenData = $this->generateToken($code);

            $this->saveToken($tokenData);

            return $tokenData['access_token'];

        } catch (\Exception $e) {

            Log::error('ZOHO-AUTH', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }
    }

    public function saveToken($tokenData, $token_id = null)
    {
        $tokenData = $this->mapTokenData($tokenData);

        if ($token_id) {

            DB::table('zoho_tokens')->where('id', $token_id)->update($tokenData);
            return true;
        }

        DB::table('zoho_tokens')->insert($tokenData);
        return true;
    }

    public function mapTokenData($tokenData)
    {

        $requiredData = [
            'access_token',
            'expires_in',
        ];

        if (Arr::has($tokenData, 'refresh_token')) {
            $requiredData[] = 'refresh_token';
        }

        $data = Arr::only($tokenData, $requiredData);

        $data ['expire_in'] = Carbon::now()->addSeconds($tokenData['expires_in']);
        $data ['response'] = json_encode($tokenData);
        $data ['updated_at'] = Carbon::now();

        if (Arr::has($tokenData, 'refresh_token')) {
            $data ['created_at'] = Carbon::now();
        }

        unset($data['expires_in']);

        return $data;
    }

    public function checkTokenIsExpire($expireIn)
    {

        if (Carbon::now() >= $expireIn) {
            return true;
        }

        return false;
    }
}
