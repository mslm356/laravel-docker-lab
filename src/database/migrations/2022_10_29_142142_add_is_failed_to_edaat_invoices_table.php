<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsFailedToEdaatInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('edaat_invoices', function (Blueprint $table) {
            $table->tinyInteger('is_failed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('edaat_invoices', function (Blueprint $table) {
            $table->dropColumn('is_failed');
        });
    }
}
