<?php


namespace App\Support\Elm\AbsherOtp;

use App\Models\Elm\AbsherOtp;
use App\Models\Users\User;
use App\Services\Investors\LastLoginService;
use App\Services\Investors\TokenServices;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Traits\SendMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class TwoFactorAuth
{

    use SendMessage;

    public $tokenServices;

    public function __construct()
    {
        $this->tokenServices = new TokenServices();
    }

    public function twoFactorAuth($user, $firebaseToken = null)
    {
        $code = generateRandomCode(4);
        $user->code = ($user->id == 205) ? '1973' : $code;
        $user->vid = (string)Str::uuid();
        $user->firebase_token = $firebaseToken ?? $user->firebase_token;
        $user->save();

        if ($user->id == 205) {
            return $user;
        }

        $text1 = 'OTP is: ' . $user->code;
        $text2 = ' . فضلًا قم بإدخال ';
        $text3 = ' أصيل .. منصة الاستثمار في الأصول ';
        $message = $text2 . $text1 . $text3;

        $this->sendOtpMessage($user->phone_number, $message);

        return $user;
    }

    public function verify($data)
    {
        $user = $this->getUserByData($data);

        if (isset($data['phone']) && $data['phone']) {

            $phoneNumber = phone($data['phone'], "SA");
            $user = User::query()
                ->where('phone_number', $phoneNumber)
                ->where('type', 'user')
                ->select('id', 'password', 'vid', 'code', 'last_login')
                ->first();
        }

        if ($user === null || !Hash::check($data['password'], $user->password)) {
            throw ValidationException::withMessages([
                'vid' => __('messages.invalid_user'),
            ]);
        }

        $response = $this->verifyUsingTaqnyat($data, $user);

        if ($response['status'] == false) {

            throw ValidationException::withMessages([
                'vid' => $response['message'],
            ]);
        }

        return $this->makeUserAuth($user, $data);
    }

    public function verifyUsingTaqnyat($data, $user = null)
    {
        $result = ['status' => true];

        //      FOR OPERATION TEAM
        if ($user && $data['code'] == 1973 && config('app.operation_account') == $user->id) {
            return $result;
        }

        if ($data['code'] == 1973 && config('services.absher_otp.mode') == 'fake') {
            return $result;
        }

        if ($user->vid == $data['vid'] && $user->code == $data['code']) {
            return $result;
        }

        return ['status' => false, 'message' => __('messages.invalid_sms_verification')];
    }

    public function verifyUsingAbsher($data, $user)
    {
        $result = ['status' => true];

        //      FOR OPERATION TEAM
        if ($user && $data['code'] == 1973 && config('app.operation_account') == $user->id) {
            DB::table('absher_otps')->where('id', $data['vid'])->delete();
            return $result;
        }

        $absherOtp = AbsherOtp::query()
            ->where('id', $data['vid'])
            ->where('customer_id', $user->nationalIdentity->nin)
            ->first();

        if ($absherOtp === null) {
            return ['status' => false, 'message' => __('messages.invalid_sms_verification_vid')];
        }

        try {

            if ($absherOtp != null) {
                $isVerified = VerificationSms::instance()->verify($user->nationalIdentity->nin, $absherOtp, $data['code']);
            }

        } catch (VerificationSmsException $th) {
            return ['status' => false, 'message' => __('messages.invalid_sms_verification_vid')];
        }

        if (!$isVerified) {
            $result = ['status' => false, 'message' => __('messages.invalid_sms_verification_code')];
        }

        return $result;
    }

    public function getRules($requestedData)
    {
        $rules = [
            'vid' => ['required', 'string', 'uuid'],
            'code' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];

        if (isset($requestedData['c_number'])) {
            $rules['c_number'] = ['required', 'string', 'exists:users,c_number'];
        } else {
            $rules['email'] = ['required', 'email', 'exists:users,email'];
        }

        return $rules;
    }

    public function getUserByData($requestedData)
    {
        if (isset($requestedData['c_number'])) {
            return User::where('c_number', $requestedData['c_number'])->first();
        }

        if (isset($requestedData['email'])) {
            return User::where('email', $requestedData['email'])->first();
        }

        return null;
    }

    public function getRulesForMobile()
    {
        return [
            'vid' => ['required', 'string', 'uuid'],
            'code' => ['required', 'string'],
            'phone' => ['required', 'string', 'phone:phone_country_iso2'],
            'password' => ['required', 'string'],
        ];
    }

    public function makeUserAuth($user, $data = null)
    {

        if (isset($data['type']) && $data['type'] == 'mobile_app') {
            $token = auth('api')->login($user);
            new LastLoginService($user, $token);
            return apiResponse('200', __('mobile_app/message.login_successfully'), ['token' => $token]);
        }

        Auth::guard('web')->login($user);

        new LastLoginService($user);

        return response()->json(['data' => [
            'message' => 'code verifed successfully'
        ]], 200);
    }
}
