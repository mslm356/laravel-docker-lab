<?php

namespace App\Support\Elm\Nabaa;

class AuthBodyParams
{
    public static function get()
    {
        return [
            'ClientId' => config('services.nabaa.client_id'),
            'ClientAuthorization' => config('services.nabaa.client_auth'),
        ];
    }
}
