<?php


namespace App\Services;

use Illuminate\Support\Facades\Log;

class CacheServices
{
    public function flushCache($url = null, $header = null)
    {
        $urlParams = '&& req.url ~ ^';

        $mainUrl = $url ? $url : '/api/*';

        $mainCommand = 'varnishadm -T '.config('varnish.varnish_ip').' -S /etc/varnish/secret ban ';

        $params = 'req.http.host == '.config('varnish.varnish_host').' ' . $urlParams . $mainUrl; // todo

        $command = $mainCommand . '"' . $params . '"';


        shell_exec($command);
        exec($command);

        //        Log::error('COMMAND', [shell_exec($command), exec($command)]);
    }

    public function updateFundCache($listing_id)
    {
        if (config('app.cache_enable')) {
            $this->flushCache('/api/app/fund-details/' . $listing_id);
            $this->flushCache('/api/app/all-funds/*');
            $this->flushCache('/api/app/auth-fund-details/' . $listing_id);
            $this->flushCache('/api/investor/fund-details/' . $listing_id);
        }
    }

}
