<?php

namespace App\Support\Anb;

use Illuminate\Support\Carbon;
use App\Support\Anb\Http\AnbHttp;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;

class Auth
{
    private static $instance;
    protected $accessToken;
    protected $tokenType;
    protected $issuedAt;
    protected $expiresIn;
    protected $accounts;

    private function __construct(array $response)
    {
        $this->accessToken = $response['access_token'];
        $this->tokenType = $response['token_type'];
        $this->issuedAt = Carbon::parse($response['issued_at']);

        if (preg_match_all('/\d{1,}\s[A-Z][A-Z]\d{1,}/', $response['user']['accounts'], $accountsAndIbans)) {
            foreach ($accountsAndIbans[0] as $accountAndIban) {
                [$accountNumber, $iban] = explode(' ', $accountAndIban);

                $this->accounts[] = compact('accountNumber', 'iban');
            }
        }
    }

    public function getToken()
    {
        return $this->accessToken;
    }

    public function getTokenType()
    {
        return $this->tokenType;
    }

    public function getAccounts()
    {
        return $this->accounts;
    }

    public static function instance($refresh = false)
    {
        if ($refresh) {
            static::refreshAccess();
            return self::$instance;
        }

        if (self::$instance) {
            return self::$instance;
        }

        if ($encryptedAccess = Cache::get('_anb_access')) {
            try {
                $access = json_decode(Crypt::decryptString($encryptedAccess), true);
                self::$instance = new Auth($access);
            } catch (\Throwable $th) {
                static::refreshAccess();
            }
        } else {
            static::refreshAccess();
        }

        return self::$instance;
    }

    protected static function refreshAccess()
    {
        $response = AnbHttp::asForm()
            ->signed(false)
            ->post('/b2b-auth/oauth/accesstoken', [
                'grant_type' => 'client_credentials',
                'client_id' => config('services.anb.client_id'),
                'client_secret' => config('services.anb.secret'),
            ]);

        $responseBody = $response->json();

        Cache::put('_anb_access', Crypt::encryptString(json_encode($responseBody)), $responseBody['expires_in']);

        self::$instance = new Auth($response->json());
    }
}
