<?php

namespace App\Http\Controllers\Admins\Api\Investors;

use App\Models\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Investors\InvestorInvestmentResource;

class GetInvestorInvestments extends Controller
{
    /**
     * Get Investors list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(User $user)
    {
        return InvestorInvestmentResource::collection($user->investmentsListUsingListingInvestor);
    }
}
