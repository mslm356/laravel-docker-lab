<?php


namespace App\Services\Investors\Investment;

class HandleResponseService
{
    public function responseValues($response, $status, $message, $data)
    {
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $data;

        return $response;
    }
}
