<?php

use \App\Http\Controllers\MobileApp\Investors\staticUrls\StaticUrlsController;


Route::get('/static-urls', [StaticUrlsController::class, 'index']);

