<?php

namespace App\Console\Commands\Kyc;

use App\Models\Investors\InvestorProfile;
use Illuminate\Console\Command;

class ChangeKycOption extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:kyc-option';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command that handle change kyc fileds that affect in investment operation ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // get all restricted columns and it's values
        $values=$this->getALertRestrictedValues();

        // update all investor that has only one restriction value
        if(count($values) > 0) // if there is no restricted columns do nothing
        InvestorProfile::where(function ($query) use($values){
            foreach ($values as $key=>$value)
            {
                $query->orWhereIn($key,$value);
            }
        })->update(['is_normal_kyc_data'=>0]);


      //  update all investor that not has any restriction value
        InvestorProfile::where(function ($query) use($values){
                    foreach ($values as $key=>$value)
                    {
                        $query->whereNotIn($key,$value);
                    }
                })->update(['is_normal_kyc_data'=>1]);


    dd('done isa');
    }

    function getALertRestrictedValues()
    {
        $values=[];

        // get all alert classes
        $classes= array_filter(
            get_declared_classes(),
            function ($className) {
                return in_array('App\Enums\Investors\Registration\Interfaces\AlertMessgesInterface', class_implements($className));
            }
        );


        // get  restricted values
        foreach ($classes as $class)
        {
            try {
                $values[$class::getDBColumnName()]=array_keys($class::getAlertMessages());

            }catch (\Exception $e){
                 dd('error : this class '.$class. ' should  implement DBColumInterface that have getDBColumnName function');
            }
        }

        return $values;
    }
}
