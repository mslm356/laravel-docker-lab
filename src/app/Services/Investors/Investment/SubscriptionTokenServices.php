<?php


namespace App\Services\Investors\Investment;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\CheckIfCanInvest;
use App\Models\Users\InvestorSubscriptionToken;
use App\Services\Investors\AbsherOtpService;
use App\Services\LogService;
use App\Services\MemberShip\SpecialCanInvest;
use App\Support\Users\UserAccess;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SubscriptionTokenServices
{

    public $userAccess;
    public $specialCanInvest;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->userAccess = new UserAccess();
        $this->absherOtpService = new AbsherOtpService();
        $this->specialCanInvest = new SpecialCanInvest();
    }

    public function handle($request, $listing, $type = null)
    {

        $response = ['status' => true];

        $data = $this->dataValidation($request);


        $user = getAuthUser($type);

        if (!$this->specialCanInvest->handle($listing, $user)) {
            return $this->responseValues($response, false, __('you not have access'), null);
        }

        $case = $this->canInvest($user, $listing, $data['shares']);

        if ($case === true || ($case == 5 && isset($data['payment_type']) && $data['payment_type'] == 'urway')) {

            $tokenData = [
                'user_id' => $user->id,
                'listing_id'=> $listing->id,
                'shares'=> $data['shares'],
                'payment_type'=> $data['payment_type'],
            ];

            $token = $this->createToken($tokenData);

            $mobileCase = isset($data['device']) && $data['device'] == 'mobile';

            if ($mobileCase) {
                $amountAndFees = CalculateInvestingAmountAndFees::run($listing, $request['shares']);
                //                $nin = $user->nationalIdentity->nin;
                $nin=($request->user_can_access_data->national_identity_data)?json_decode($request->user_can_access_data->national_identity_data)->nin:$user->nationalIdentity->nin;
                $absherOtp = $this->absherOtpService->createOtp($user, $token, $listing->sms_delay, $nin);
            }

            $responseData =  [
                'token' => $token->reference,
                'total' => $mobileCase ? $amountAndFees['total']->formatByDecimal() : 0,
                'shares' => $request['shares'],
                'vid' => $mobileCase ? $absherOtp->id : null,
            ];

            return $this->responseValues($response, true, 'mobile_app/message.success', $responseData);
        }

        return $this->responseValues($response, false, 'messages.invest_policy_' . $case, []);
    }

    public function dataValidation($request)
    {

        $rules = [
            'payment_type' => 'nullable|string|in:wallet',
            'shares' => 'required|integer|min:1',
            'delay' => 'nullable|integer|min:0',
            'device' => 'nullable|string',
        ];

        $data = Validator::make($request->all(), $rules)->validate();

        $data['payment_type'] = isset($data['payment_type']) ? $data['payment_type'] : 'wallet';

        return $data;
    }

    public function expireOldTokens($user_id, $listing_id)
    {

        DB::table('investor_subscription_tokens')
            ->where('user_id', $user_id)
            ->where('listing_id', $listing_id)
            ->where('is_expire', false)
            ->update(['is_expire' => true]);

    }

    public function responseValues($response, $status, $message, $data)
    {

        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $data;

        return $response;
    }

    public function canInvest($user, $listing, $shares)
    {
        return CheckIfCanInvest::run(
            $listing,
            $user,
            null,
            $shares
        );
    }

    public function createToken(array $tokenData)
    {

        $data = array_merge($tokenData, [
            'reference' => Str::random(25),
            'is_expire' => false,
        ]);

        return InvestorSubscriptionToken::create($data);
    }

}
