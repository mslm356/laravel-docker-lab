<?php

namespace App\Http\Controllers\Investors\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\TransactionResource;
use App\Models\Users\User;
use App\Services\Investors\TransactionServices;
use Illuminate\Http\Request;

class GetTransactionsHistory extends Controller
{
    public $transactionServices;

    public function __construct()
    {
        $this->transactionServices = new TransactionServices();
    }

    /**
     * Virtual account history
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Users\User|null $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, $user = null)
    {
        $user = $user ? User::findOrFail($user) : $request->user('sanctum');
        $transactions = $this->transactionServices->myTransactions($request, $user, true);
        return TransactionResource::collection($transactions);
    }
}
