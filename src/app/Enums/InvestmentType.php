<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class InvestmentType extends Enum implements LocalizedEnum
{
    const Development =   1;
    const IncomeGenerating =   2;
    const InfrastructureDevelopment =   3;
    const PrivateEquity =   4;
}
