<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaidPayoutToListingInvestorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listing_investor', function (Blueprint $table) {
            $table->decimal('paid_payout', 64, 0)->default(0)->after('invested_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listing_investor', function (Blueprint $table) {
            $table->dropColumn('paid_payout');
        });
    }
}
