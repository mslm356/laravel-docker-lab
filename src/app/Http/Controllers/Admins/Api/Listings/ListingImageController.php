<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Http\Resources\Admins\Listings\ListingImagesResource;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Listings\ListingImage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Actions\Listings\Images\DeleteListingImage;
use App\Actions\Listings\Images\StoreListingImages;
use App\Common\ValidationRules\Listings\ListingImageRules;

class ListingImageController extends Controller
{
    /**
     * Store the images in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        return DB::transaction(function () use ($request, $listing) {
            $data = $this->validator($request->all())->validate();



            $result = StoreListingImages::run($listing, Arr::get($data, 'files', []));
            $newImages = ListingImage::whereIn("name", $result)->where('listing_id', $listing->id)->get();

            return response()->json([
                'images' => ListingImagesResource::collection($newImages)
            ], 201);

        });
    }

    public function sortImages(Request $request)
    {
        $request->validate(['imagesToBeSorting' => "nullable"]);

        foreach ($request->imagesToBeSorting as $image) {
            if ($image) {
                ListingImage::where("id", $image['id'])->update(['index' => $image['index']]);

                if ($image['index'] == 0)
                    $this->updateDefaultImage($image['id']);
            }

        }
        return response()->json([], 200);
    }

    function updateDefaultImage($id)
    {
        $first_image=ListingImage::where("id", $id)->first();
        if ($first_image)
        Listing::where('id',$first_image->listing_id)->update(['default_image'=>$first_image->path]);
    }
    /**
     * Delete the image
     *
     * @param \App\Models\Listings\ListingImage $image
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, ListingImage $image)
    {
        return DB::transaction(function () use ($image) {
            DeleteListingImage::run($image);

            $first_image=ListingImage::orderBy('index')->first();

            if ($first_image)
                $this->updateDefaultImage($first_image->id);
            else
                Listing::where('id',$image->listing_id)->update(['default_image'=>null]);


            return response()->json();
        });
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, ListingImageRules::get());
    }
}
