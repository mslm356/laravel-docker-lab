<?php

namespace App\Console;

use App\Jobs\Anb\AnbEodStatement;
use App\Jobs\Anb\AnbPullStatement;
use App\Jobs\Anb\CheckAnbPaymentsStatus;
use App\Jobs\Edaat\RefreshEdaatAuthJob;
use App\Jobs\Investor\verifyAccountJob;
use App\Jobs\Sdad\UpdateExpiredInvoices;
use App\Jobs\Transfer\AcceptWithdrawOperation;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\Investor\CheckIqamaDateJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new RefreshEdaatAuthJob, 'edaat_refresh_token')->daily();
        $schedule->job(new AnbPullStatement, 'anb_bank_statement')->everyTwoMinutes();
//       $schedule->job(new UpdateExpiredInvoices, 'anb_bank_eod')->dailyAt('05:10');
//        $schedule->job(new CheckAnbPaymentsStatus, 'anb_bank_payments')->everyFifteenMinutes();
        $schedule->job(new AnbEodStatement, 'anb_bank_eod')->at('15:10')->timezone('Asia/Riyadh');
//        $schedule->job(new verifyAccountJob, 'verify_account')->at('01:10')->timezone('Asia/Riyadh');
        $schedule->job(new CheckIqamaDateJob, 'require_kyc_update')->weekly()->tuesdays()->at('16:00'); //Run the task every Sunday at 20:00


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
