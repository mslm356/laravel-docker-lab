<?php

namespace App\Support\Anb;

use App\Enums\Banking\BankCode;
use Exception;
use App\Support\Anb\Http\AnbHttp;
use App\Support\Anb\Http\HttpException;
use Illuminate\Support\Facades\Log;

class AnbApiUtil
{
    const FAILED_TO_SIGN = 1;
    const FAILED_TO_TRANSFER = 2;
    const FAILED_TO_VERIFY = 4;
    const FAILED_TO_PULL_STATEMENT = 5;
    const FAILED_TO_PULL_EOD_STATEMENT = 6;

    /**
     * Sign the given message
     *
     * @param string $body
     * @return bool
     * @throws \Exception
     */
    public static function transfer(
        $to,
        $amount,
        $ref,
        $valueDate,
        $beneficiary,
        $fromAcc = null,
        $comments = null,
        $distBankCode = null,
        $narrative,
        $feeIncluded = false,
        $purposeOfTransfer = '39',
        $currency = 'SAR',
        $channel = null
    ) {
        if ($valueDate->timezone !== 'Asia/Riyadh') {
            $valueDate->timezone = 'Asia/Riyadh';
        }

        $fmtDate = $valueDate->format('ymd');

        if (is_null($fromAcc)) {
            $fromAcc = config('services.anb.account_number');
        }

        if (is_null($comments)) {
            $comments = AnbNarrative::make($ref);
        }

        if (is_null($distBankCode)) {
            $distBankCode = BankCode::Anb;
        }

        $reqOptions = [
            'uri' => '/payment/json',
            'body' => [
                'sequenceNumber' => (string)$ref,
                'amount' => (string)$amount,
                'valueDate' => $fmtDate,
                'feeIncluded' => false,
                'orderingParty' => 'Aseel Financial',
                'orderingPartyAddress1' => 'Al Malqa Dsitrict',
                'orderingPartyAddress2' => 'Riyadh',
                'orderingPartyAddress3' => 'Kingdom of Saudi Arabia',
                'debitAccount' => $fromAcc,
                'channel' =>  $channel ?? static::paymentChannel($fmtDate, $amount, $distBankCode),
                'creditAccount' => $to,
                'beneficiaryName' => $beneficiary['name'],
                'beneficiaryAddress1' => $beneficiary['city'],
                'beneficiaryAddress2' => 'Kingdom of Saudi Arabia',
                'narrative' => $narrative ?? '',
                'purposeOfTransfer' => $purposeOfTransfer,
                'transactionComment' => $comments,
                'currency' => $currency,
                'destinationBankBIC' => $distBankCode
            ]
        ];


        Log::info('Anb-Transfer-Request-data-from-wallets', [$reqOptions]);

        $response = AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->post($reqOptions['uri'], $reqOptions['body'])
            ->json();

        Log::info('Anb-Transfer-Response-data', [$response]);

        return $response;
    }

    /**
     * get payment channel
     *
     * @param string $valueDate
     * @param float $amount
     * @param string $distinationBank
     * @return string
     * @throws \Exception
     */
    public static function paymentChannel($valueDate, $amount, $distinationBank)
    {
        Log::info('Anb-Channel-request-data', [$valueDate, $amount, $distinationBank, 'url' => '/payment/channel']);

        $response = AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->post('/payment/channel', [
                'valueDate' => $valueDate,
                'amount' => (string)$amount,
                'destinationBankBIC' => $distinationBank
            ])
            ->json('channel');

        Log::info('Anb-Channel-Response', [ $valueDate, $amount, $distinationBank, $response]);

        return $response;
    }

    /**
     * get account balance
     *
     * @param string $account
     * @return array
     * @throws \Exception
     */
    public static function balance($account)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->get('/report/account/balance', [
                'accountNumber' => $account,
            ])
            ->json();
    }

    public static function cancel($id)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->put('/payment/'. $id .'/cancel')
            ->json();
    }

    /**
     * Get statement
     *
     * @param string $account
     * @param string $fromDate
     * @param string $toDate
     * @param string|int $max
     * @param string|int $offset
     * @param float $amount
     * @param string $distinationBank
     * @return mix
     * @throws \Exception
     */
    public static function statement($account, $fromDate, $toDate, $max = null, $offset = null)
    {
        try {
            $body = AnbHttp::withToken(
                Auth::instance()->getToken(),
                Auth::instance()->getTokenType(),
                ['method_name' => 'pull_stmt', 'version' => 'v2'],
            )
                ->get(
                    '/report/account/statement',
                    array_filter([
                        'fromDate' => $fromDate,
                        'toDate' => $toDate,
                        'accountNumber' => $account,
                        'max' => $max,
                        'offset' => $offset,
                        'type' => 'JSON'
                    ])
                )
                ->body();

            $bodyWithFloatsToStrings = preg_replace(
                '/"amount":([+-]?[0-9]+\.?[0-9]*|\.[0-9]+)/',
                '"amount":"$1"',
                $body
            );

            return json_decode($bodyWithFloatsToStrings, true, 512);
        } catch (HttpException $th) {
            if ($th->getResponse()->status() === 400) {
                return null;
            }

            throw $th;
        }
    }

    /**
     * End of the day statements
     *
     * @param string $date
     * @param int $page
     * @param string $order
     * @param int $take
     * @return mixed
     * @throws \App\Support\Anb\Http\HttpException
     */
    public static function endOfDayStatements($date, $page = 1, $order = 'DESC', $take = 10)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->get('/report/eod-statement', array_filter([
                'order' => $order,
                'page' => $page,
                'take' => $take,
                'date' => $date,
            ]))
            ->json();
    }

    /**
     * End of the day statement
     *
     * @param string $id
     * @return mixed
     * @throws \App\Support\Anb\Http\HttpException
     */
    public static function endOfDayStatement($id)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->get('/report/eod-statement/' . $id)
            ->json();
    }

    /**
     * Payment enquiry
     *
     * @param string $id
     * @return mixed
     * @throws \App\Support\Anb\Http\HttpException
     */
    public static function payment($id)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->get('/payment/' . $id)
            ->json();
    }

    /**
     * Payment enquiry
     *
     * @param string $id
     * @return mixed
     * @throws \App\Support\Anb\Http\HttpException
     */
    public static function newPayment($id, $type)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->get('/payment/' . $id . '?type=' . $type)
            ->json();
    }

    /**
     * Create webhook
     *
     * @param string $url
     * @param string $description
     * @param array $events
     * @return mixed
     * @throws \App\Support\Anb\Http\HttpException
     */
    public static function createWebhook($url, $description, $events)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->post('/webhook', [
                'description' => $description,
                'events' => $events,
                'url' => $url
            ])
            ->json();
    }

    /**
     * Get webhooks
     *
     * @return mixed
     * @throws \App\Support\Anb\Http\HttpException
     */
    public static function listWebhooks()
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->get('/webhook')
            ->json();
    }

    /**
     * Delete webhook
     *
     * @param string $id
     * @return mixed
     * @throws \App\Support\Anb\Http\HttpException
     */
    public static function deleteWebhook($id)
    {
        return AnbHttp::withToken(
            Auth::instance()->getToken(),
            Auth::instance()->getTokenType()
        )
            ->delete("/webhook/{$id}")
            ->json();
    }
}
