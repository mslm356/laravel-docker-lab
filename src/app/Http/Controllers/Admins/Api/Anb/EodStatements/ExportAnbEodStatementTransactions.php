<?php

namespace App\Http\Controllers\Admins\Api\Anb\EodStatements;

use App\Exports\AnbEodStatements\AnbEodStatementTransactionsExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Anb\AnbEodStatement;
use Maatwebsite\Excel\Facades\Excel;

class ExportAnbEodStatementTransactions extends Controller
{
    /**
     * Export ANB EOD statement transactions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, AnbEodStatement $statement)
    {
        $data  = $this->validate($request, [
            'extension' => ['required', 'in:xlsx,csv']
        ]);

        return Excel::download(
            new AnbEodStatementTransactionsExport($statement),
            "anb-eod-stmt-transactions-{$statement->id}.{$data['extension']}"
        );
    }
}
