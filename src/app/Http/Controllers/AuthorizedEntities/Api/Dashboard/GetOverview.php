<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Dashboard;

use App\Enums\AML\AmlStatus;
use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Models\AML\AmlListCheck;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestor;
use App\Models\Listings\ListingValuation;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;

class GetOverview extends Controller
{
    /**
     * Get the paginated enquiries
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        $listings = Listing::where('authorized_entity_id', authorizedEntityId())->count();

        $totalInvested = ListingInvestor::whereHas(
            'listing',
            function ($query) {
                $query->where('authorized_entity_id', authorizedEntityId());
            }
        )
            ->whereNotIn(
                'listing_investor.investor_id',
                User::role([Role::PayoutsCollector])->get()->pluck('id')
            )
            ->sum('invested_amount');

        $totalAum = ListingValuation::select('listing_valuations.value')
            ->leftJoin('listing_valuations as valuations2', function ($join) {
                $join->on('listing_valuations.listing_id', '=', 'valuations2.listing_id')
                    ->on('listing_valuations.date', '<', 'valuations2.date');
            })
            ->whereHas('listing', function ($query) {
                $query->where('authorized_entity_id', authorizedEntityId());
            })
            ->whereNull('valuations2.id')
            ->sum('listing_valuations.value');

        return response()->json([
            'data' => [
                'listings_count' => $listings,
                'total_invested' => cleanDecimal(number_format($totalInvested, 2)),
                'total_aum' => cleanDecimal(number_format($totalAum, 2)),
            ]
        ]);
    }
}
