<?php

namespace App\Jobs;

use App\Models\Users\User;
use App\Services\LogService;
use App\Services\Sdad\Services\CreateSingleInvoiceService;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ChargeWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationServices;

    public $user;
    public $data;
    public $create_invoice_service;
    /**
     * @var LogService
     */
    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, array $request_data)
    {
        $this->user = $user;
        $this->data = $request_data;
        $this->create_invoice_service = new CreateSingleInvoiceService();
        $this->logService = new LogService();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->create_invoice_service->create_invoice($this->user, $this->data);
        } catch (\Exception $e) {
            $this->logService->log('Sdad-ChargeWallet-Job-ERROR', $e);
        }
    }


}
