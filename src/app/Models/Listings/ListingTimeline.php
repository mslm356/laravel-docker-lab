<?php

namespace App\Models\Listings;

use App\Models\AppModel;

class ListingTimeline extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];
}
