<?php

namespace App\Http\Controllers\Admins\Api\Complaints;

use App\Models\Complaint;
use App\Models\Users\User;
use App\Traits\SendMessage;
use Illuminate\Http\Request;
use App\Models\ComplaintType;
use App\Http\Controllers\Controller;
use App\Traits\NotificationServices;
use App\Jobs\CloseComplaintEmailToUserJob;
use App\Http\Resources\Admins\FileResource;
use App\Enums\Notifications\NotificationTypes;
use App\Http\Requests\Complaint\UpdateRequest;
use App\Http\Resources\Admins\Complaints\ComplaintsListItemResource;
use App\Http\Resources\Admins\Complaints\ComplaintTypesListItemResource;

class ComplaintController extends Controller
{
    use SendMessage, NotificationServices;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->input('per_page', 10);
        $sortBy = $request->input('sort_by', 'created_at');
        $sortOrder = $request->input('sort_order', 'desc');
        // add filter by status
        $complaint = Complaint::orderBy($sortBy, $sortOrder)->paginate($perPage);

        return ComplaintsListItemResource::collection($complaint);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Complaint  $complaint
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Complaint $complaint)
    {
        return [
            'id' => $complaint->id,
            'subject' => $complaint->subject,
            'description' => $complaint->description,
            'owner_id' => $complaint->user->id,
            'owner' => $complaint->user_name,
            'reviewer' => $complaint->reviewer->full_name ?? '',
            'reviewer_comment' => $complaint->reviewer_comment ?? '',
            'status' => [
                'value' => $complaint->status->id,
                'description' => $complaint->status->name,
            ],
            'ticket_type' => $complaint->type_name,
            'created_at' => $complaint->created_at,
            'attachments' => FileResource::collection($complaint->files)
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Complaint $complaint)
    {

        $data = $request->validated();
        $data['reviewer_id'] = auth()->user()->id;
        $data['status_id'] = $data['status'];
        $data['reviewer_comment'] = $data['comment'];

        $complaint->update($data);

        $user = $complaint->user;

        $this->dispatch(new CloseComplaintEmailToUserJob($complaint));
        $this->sendSmsData($user, $complaint);
        $this->send(NotificationTypes::Info, $this->notificationData($user, $complaint), $user->id);

        return response()->json(['data' => $complaint, 'message' => __('app.complaintUpdated')]);
    }

    public function types()
    {
        return ComplaintTypesListItemResource::collection(ComplaintType::all());
    }

    public function investor(User $investor, Request $request)
    {
        $perPage = $request->input('per_page', 10);

        return ComplaintsListItemResource::collection(Complaint::where('user_id', $investor->id)->paginate($perPage));
    }

    public function sendSmsData($user, $complaint)
    {
        $content_en = "Your ticket has been resolved. $complaint->id
            '$complaint->reviewer_comment'
            If you have any further questions, please reach out to us.
        ";
        $content_ar = "تم إغلاق التذكرة. $complaint->id
            '$complaint->reviewer_comment'
            إذا كان لديكم أي أسئلة، فلا تترددوا في الاتصال بنا.
        ";

        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendMessages($user->phone_number, $message);
    }

    public function notificationData($user, $complaint)
    {
        return [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => 'Aseel',
            'title_ar' => 'منصة أصيل',
            'content_en' => " Your ticket has been resolved. $complaint->id
                '$complaint->reviewer_comment'
                If you have any further questions, please reach out to us.
            ",
            'content_ar' => "تم إغلاق التذكرة  $complaint->id
                '$complaint->reviewer_comment'
            إذا كان لديكم أي أسئلة، فلا تترددوا في الاتصال بنا.
            ",
        ];
    }
}
