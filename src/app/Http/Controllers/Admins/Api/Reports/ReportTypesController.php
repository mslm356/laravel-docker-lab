<?php

namespace App\Http\Controllers\Admins\Api\Reports;

use App\Actions\Reports\ExportInvestorDepositss;
use App\Exports\Investors\DepositReportExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportTypesController extends Controller
{
    public function reportTypes () {

        $types = [
            [
                'value' => 'Investors',
                'label' => 'Investors',
            ],
            [
                'value' => 'Transactions',
                'label' => 'Transactions',
            ],
            [
                'value' => 'Wallets',
                'label' => 'Wallets',
            ],
            [
                'value' => 'AnbTransfer',
                'label' => 'AnbTransfer',
            ],
            [
                'value' => 'Transaction Balance',
                'label' => 'Transaction Balance',
            ],
            [
                'value' => 'Investors profile with kyc status',
                'label' => 'Investors profile with kyc status',
            ],
        ];

        return response()->json(['data' => $types], 200);
    }
}
