<?php

namespace App\Http\Controllers\Investors\Profile\ProfessionalUpgrade;

use App\Http\Controllers\Controller;
use App\Models\Investors\InvestorUpgradeItem;
use Illuminate\Http\Request;

class GetUpgradeItems extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');
        return response()->json([
            'data' => InvestorUpgradeItem::where('is_active', true)->where("type",$user->type)->get()->map(fn ($item) => [
                'id' => $item->id,
                'text' => $item->text,
            ])
        ]);
    }
}
