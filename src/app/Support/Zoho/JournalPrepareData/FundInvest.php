<?php


namespace App\Support\Zoho\JournalPrepareData;


use App\Models\ZohoSettings;
use Carbon\Carbon;

class FundInvest
{

    public function prepareFundInvestData ($user, $listing, $firstAccount, $secondAccount, $amount) {

        $firstAccountName = ZohoSettings::where('key', $firstAccount)->first();
        $secondAccountName = ZohoSettings::where('key', $secondAccount)->first();

        $firstAccountValue =  $firstAccountName ? $firstAccountName->value : '';
        $secondAccountValue =  $secondAccountName ? $secondAccountName->value : '';

        $data = [
            'journal_date' => Carbon::now()->format('Y-m-d'),
            'line_items' => [
                [
                    'account_id' => $firstAccountValue,
                    'customer_id' => $user ? $user->zoho_id : '',
                    'description' => 'get amount from ' . $firstAccount,
                    'debit_or_credit' => 'debit',
                    'amount' => $amount,
                ],
                [
                    'account_id' => $secondAccountValue,
                    'customer_id' => $listing ? $listing->zoho_id : '',
                    'description' => 'add amount to ' . $secondAccount,
                    'debit_or_credit' => 'credit',
                    'amount' => $amount,
                ]
            ],
        ];

        return $data;
    }

}
