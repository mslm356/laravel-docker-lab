<?php

namespace App\Http\Controllers\Investors;

use App\Actions\CreateNewEnquiry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StoreEnquiry extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke(Request $request)
    {
        CreateNewEnquiry::run($request->user(), $request->input());

        return response()->json([], 201);
    }
}
