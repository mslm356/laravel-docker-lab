<?php

namespace App\Models\Revisions;

trait Revisionable
{
    public function createRevision($data)
    {
        return $this->revisions()->create(
            array_merge(
                $data,
                [
                    'model_state' => $this->getModelStateForRevision()
                ]
            )
        );
    }

    public function revisions()
    {
        return $this->morphMany(RevisionHistory::class, 'revisionable')->orderBy('created_at', 'desc');
    }

    public function getModelStateForRevision()
    {
        return [
            'originals' => $this->getOriginal(),
            'attributes' => $this->getAttributes(),
        ];
    }
}
