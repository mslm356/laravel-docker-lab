<?php

namespace App\Http\Controllers\MobileApp\Investors\Investments;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\CheckIfCanInvest;
use App\Actions\Investors\CheckIfCanInvestTesting;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Actions\Investors\IsFirstInvestmentInListing;
use App\Actions\Listings\GetCurrentInvestments;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Notifications\NotificationTypes;
use App\Exceptions\NoBalanceException;
use App\Exceptions\NoInvestorWalletFound;
use App\Http\Controllers\Controller;
use App\Jobs\Absher\SendAbsherOtp;
use App\Jobs\Zatca\GenerateEInvoiceAfterInvesting;
use App\Mail\Investors\NewInvestmentTransaction;
use App\Models\Elm\AbsherOtp;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Urway\CardToken;
use App\Models\Urway\UrwayTransaction;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Services\CacheServices;
use App\Services\Investors\InvestmentServices;
use App\Services\Investors\TransactionServices;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Users\UserAccess;
use App\Support\Zatca\EInvoice\Order;
use App\Support\Zatca\EInvoice\PurchaseLine;
use App\Support\Zoho\JournalPrepareData\FundInvest;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use App\Jobs\UpdateFundPercentageFirebase;

class TestInvestInListingController extends Controller
{
    use NotificationServices;

    const FAILURE_REASON_EXPIRED_AUTHORIZEATION_FOR_TOKEN = 1000;

    public $transactionServices;
    public $investmentServices;
    public $fundInvest;
    public $cacheServices;
    public $urwayServices;
    public $logServices;
    public $userAccess;

    public function __construct()
    {
        $this->transactionServices = new TransactionServices();
        $this->investmentServices = new InvestmentServices();
        $this->fundInvest = new FundInvest();
        $this->cacheServices = new CacheServices();
        $this->urwayServices = new URwayServices();
        $this->logServices = new LogService();
        $this->userAccess = new UserAccess();
    }

    public function subscriptionToken(Request $request)
    {

        try {

            $rules = [
                'payment_type' => 'nullable|string|in:wallet,urway',
                'shares' => 'required|integer|min:1',
                'listing_id' => 'required|integer|min:1',
                'user_id' => 'required|integer|exists:users,id',
            ];

            $data = Validator::make($request->all(), $rules)->validate();

            $listing = Listing::find($data['listing_id']);

            if (!$listing) {
                return apiResponse(400, __('mobile_app/message.not_found'));
            }

            $user = User::find($data);

            if (!$user) {
                return apiResponse(400, __('mobile_app/message.not_found'));
            }

            $case = $this->createSubscriptionToken($request, $listing);

            if ($case === true || ($case == 5 && isset($data['payment_type']) && $data['payment_type'] == 'urway')) {
                $token = $this->handle(
                    $user,
                    $listing,
                    $request->shares,
                    $data['payment_type'] ? $data['payment_type'] : "wallet"
                );

                if (isset($data['payment_type']) && $data['payment_type'] == 'urway') {
                    $this->createUrwayTransaction($listing, $token);
                }

                $amountAndFees = CalculateInvestingAmountAndFees::run($listing, $request['shares']);

                return apiResponse(
                    200,
                    __('mobile_app/message.success'),
                    [
                        'token' => $token->reference,
                        'total' => $amountAndFees['total']->formatByDecimal(),
                        'shares' => $request['shares'],
                    ]
                );
            }

            return apiResponse(400, trans('messages.invest_policy_' . $case));

        } catch (NoInvestorWalletFound $e) {
            $code = $this->logServices->log('INVESTMENTS-NO-INVESTOR-WALLET-FOUND', $e);
            return apiResponse(400, __('messages.no_active_wallet_found_for_investor' . ' : ' . $code));
        } catch (NoBalanceException $e) {
            $code = $this->logServices->log('INVESTMENTS-NO-BALANCE', $e);
            return apiResponse(400, __('messages.no_enough_balance' . ' : ' . $code));
        } catch (ValidationException $e) {
            $code = $this->logServices->log('CREATE-SUBSCRIPTION-TOKEN', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logServices->log('CREATE-SUBSCRIPTION-TOKEN', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }

    public function sentOtp(Request $request)
    {

        $rules = [
            'token' => 'required|string|exists:investor_subscription_tokens,reference',
            'delay' => 'required|integer'
        ];

        try {

            $data = Validator::make($request->all(), $rules)->validate();

            $token = InvestorSubscriptionToken::where('reference', $data['token'])->first();

            if ($token->expired_at) {
                throw new AuthorizationException;
            }

            return DB::transaction(function () use ($request, $token, $data) {
                $user = $token->user;

                $sendAbsherOtp = (new SendAbsherOtp($user, $token, $data['delay']));
                dispatch($sendAbsherOtp);

                return apiResponse(
                    200,
                    __('mobile_app/message.otp_sent_successfully'),
                );
            });

        } catch (ValidationException $e) {
            $code = $this->logServices->log('ValidationException-INVESTMENTS-SEND-OTP', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (AuthorizationException $e) {
            $code = $this->logServices->log('INVESTMENTS-SEND-OTP-INVESTMENTS-AUTHORIZATION-EXCEPTION', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (\Exception $e) {
            $code = $this->logServices->log('INVESTMENTS-SEND-OTP', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }

    public function verifyOtp(Request $request)
    {
        $rules = [
            'token' => 'required|string|exists:investor_subscription_tokens,reference',
            'code' => ['required', 'string'],
            'vid' => ['required', 'string']
        ];

        try {

            $data = Validator::make($request->all(), $rules)->validate();

            $token = InvestorSubscriptionToken::where('reference', $data['token'])->first();

            if ($token->expired_at) {
                throw new AuthorizationException;
            }

            return DB::transaction(function () use ($request, $token) {

                $data = $this->validate($request, [
                    'code' => ['required', 'string'],
                    'vid' => ['required', 'string']
                ]);

                $absherOtpMsg = AbsherOtp::lockForUpdate()->findOrFail($data['vid']);

                $absherOtpMsgSubscriptionTokenId = $absherOtpMsg->data['subscription_token_id'] ?? null;

                if ($absherOtpMsgSubscriptionTokenId && $absherOtpMsgSubscriptionTokenId !== $token->id) {

                    return apiResponse(400, __('messages.invalid_sms_verification_vid'));
                }

                $user = getAuthUser('api');

                $isVerified = VerificationSms::instance()->verify($user->nationalIdentity->nin, $absherOtpMsg, $data['code']);

                if ($isVerified === false) {
                    throw ValidationException::withMessages([
                        'code' => __('messages.invalid_sms_verification_code'),
                    ]);
                }

                $token->update(['authorization_expiry' => $expiry = now()->addMinutes(3)]);

                $result = [
                    'expires_in' => now()->diffInSeconds($expiry)
                ];

                return apiResponse(200, __('mobile_app/message.code_verified_successfully'), $result);
            });

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            $code = $this->logService->log('ValidationException-INVESTMENTS-verifyOtp', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (AuthorizationException $e) {
            $code = $this->logService->log('AuthorizationException-INVESTMENTS-verifyOtp', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (VerificationSmsException $e) {
            $code = $this->logService->log('INVESTMENTS-verifyOtp', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (\Exception $e) {
            $code = $this->logService->log('INVESTMENTS-verifyOtp', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }


    public function investment(Request $request)
    {

        $rules = [
            'listing_id' => 'required|integer|exists:listings,id',
            'user_id' => 'required|integer|exists:users,id',
            'shares' => 'required|integer|max:0',
        ];

        try {

            $data = Validator::make($request->all(), $rules)->validate();

            $token = $data['token'];

            return DB::transaction(function () use ($request, $token, $data) {

                $user = User::find($request['user_id']);

                $responseOfUserAccess = $this->userAccess->handle($user, 'investment');

                if (isset($responseOfUserAccess['status']) && !$responseOfUserAccess['status']) {

                    $msg = isset($responseOfUserAccess['message']) ? $responseOfUserAccess['message'] : 'you not have access';
                    return apiResponse(400, __($msg));
                }

                $token = InvestorSubscriptionToken::lockForUpdate()
                    ->where('reference', $token)
                    ->where('user_id', $user->id)
                    ->whereNull('expired_at')
                    ->first();

                if (!$token) {
                    return apiResponse(400, trans('messages.token_not_valid'));
                }

                $listingConditions = [
                    ['id', $token->listing_id],
                    ['is_visible', true],
                    ['is_closed', false]
                ];

                if (in_array($token->listing_id, config('app.listing_can_invested')) && in_array($user->id, config('app.users_can_invest'))) {
                    unset($listingConditions[2]);
                }

                $listing = Listing::where($listingConditions)->findOrFail($token->listing_id);

                $toBeInvestedShares = $token->shares;

                $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing, $toBeInvestedShares);

                if ($token->payment_type == 'urway') {

                    if ($this->checkAgeOfUser($user) == 0) {
                        return apiResponse(400, __('mobile_app/message.invalid_age'));
                    }

                    $transactionStatus = $this->urwayServices->transactionStatus($token, $toBeInvestedAmountAndFees['total'], $data['p_id']);

                    if (!$transactionStatus) {
                        return apiResponse(400, trans('mobile_app/message.invest_policy_6'));
                    }

                    $this->createCardToken($user, $request);
                }

                if (!$token || $token->authorization_expiry === null || now()->isAfter($token->authorization_expiry)) {

                    return apiResponse(
                        400,
                        __('messages.investor_subscription_token_auth_expired'),
                        ['reason' => static::FAILURE_REASON_EXPIRED_AUTHORIZEATION_FOR_TOKEN]
                    );

                }

                if ($user->investor && !$user->investor->is_kyc_completed) {
                    return apiResponse(400, __('messages.complete_your_profile'));
                }

                if (!($listingWallet = $listing->getWallet(WalletType::ListingEscrow))) {
                    Log::critical(
                        'MISSING_ESCROW_ACCOUNT',
                        ["Missing escrow account for listing #{$listing->id}"]
                    );

                    return apiResponse(400, trans('messages.missing_escrow'));
                }

                $currentInvest = GetCurrentInvestments::run($listing)->lockForUpdate()->get();

                $case = CheckIfCanInvest::run($listing, $user, $currentInvest, $toBeInvestedShares);

                if ((is_int($case) && $token->payment_type != 'urway') || is_int($case) && $case != 5 && $token->payment_type == 'urway') {
                    return apiResponse(400, trans('messages.invest_policy_' . $case));
                }

                $userWallet = $user->getWallet(WalletType::Investor);

                $identity = $user->nationalIdentity;

                $investingRecord = ListingInvestorRecord::create([
                    'investor_id' => $user->id,
                    'listing_id' => $listing->id,
                    'amount_without_fees' => $amountWithoutFees = $toBeInvestedAmountAndFees['amount']->getAmount(),
                    'admin_fees' => $adminFees = $toBeInvestedAmountAndFees['fee']->getAmount(),
                    'vat_amount' => $vatAmount = $toBeInvestedAmountAndFees['vat']->getAmount(),
                    'vat_percentage' => $vatPercentage = $toBeInvestedAmountAndFees['vatPercentage'],
                    'admin_fees_percentage' => $feePercentage = $toBeInvestedAmountAndFees['feePercentage'],
                    'subscription_token_id' => $token->id,
                    'currency' => defaultCurrency(),
                ]);

                $userWallet->transfer(
                    $listingWallet,
                    $amountWithoutFees,
                    $meta = [
                        'reason' => TransactionReason::InvestingInListing,
                        'reference' => transaction_ref(),
                        'value_date' => now('UTC')->toDateTimeString(),
                        'listing_id' => $listing->id,
                        'listing_title_en' => $listing->title_en,
                        'listing_title_ar' => $listing->title_ar,
                        'user_id' => $user->id,
                        'user_name' => "{$identity->first_name} {$identity->last_name}",
                        'shares' => $toBeInvestedShares,
                        'investing_record_id' => $investingRecord->id,
                    ]
                );

                $adminFeeTransaction = $userWallet->transfer(
                    $listing->getWallet(WalletType::ListingEscrowAdminFee),
                    $adminFees,
                    array_merge($meta, [
                        'reason' => TransactionReason::InvestingInListingAdminFee,
                        'reference' => $adminFeesTransactionRef = transaction_ref(),
                        'fee_percentage' => $feePercentage,
                    ])
                );

                $userWallet->transfer(
                    $listing->getWallet(WalletType::ListingEscrowVat),
                    $vatAmount,
                    array_merge($meta, [
                        'reason' => TransactionReason::InvestingInListingVatOfAdminFee,
                        'reference' => transaction_ref(),
                        'vat_percentage' => $vatPercentage,
                    ])
                );

                if (IsFirstInvestmentInListing::run($currentInvest, $user->id)) {
                    $user->investments()
                        ->attach($listing->id, [
                            'invested_amount' => $toBeInvestedAmountAndFees['amount']->getAmount(),
                            'invested_shares' => $toBeInvestedShares,
                            'currency' => defaultCurrency()
                        ]);
                } else {
                    DB::table('listing_investor')
                        ->where('listing_id', $listing->id)
                        ->where('investor_id', $user->id)
                        ->update([
                            'invested_shares' => DB::raw("invested_shares + {$toBeInvestedShares}"),
                            'invested_amount' => DB::raw("invested_amount + {$toBeInvestedAmountAndFees['amount']->getAmount()}"),
                        ]);
                }

                $token->update([
                    'expired_at' => now()
                ]);

                $listing->update([
                    'invest_percent' => $listing->percentage
                ]);

                UpdateFundPercentageFirebase::dispatch($listing->id, $listing->percentage)->onQueue('firebase');

                $subscriptionFormData = GetSubscriptionFormData::run(
                    $token,
                    $user,
                    $listing,
                    $toBeInvestedAmountAndFees['total'],
                    $currentInvest->where('investor_id', $user->id)
                );

                ExportSubFormAndAttachToInvestingRecord::dispatch(
                    $investingRecord,
                    $token,
                    $user,
                    $listing,
                    $subscriptionFormData
                );

                GenerateEInvoiceAfterInvesting::dispatch(
                    $user,
                    new Order(
                        $adminFeesTransactionRef,
                        [
                            new PurchaseLine(
                                [
                                    'en' => "({$feePercentage}%) of investing in {$listing->title_en}",
                                    'ar' => "(%{$feePercentage}) من مبلغ الاستثمار في {$listing->title_ar}",
                                ],
                                1,
                                $toBeInvestedAmountAndFees['fee'],
                                $vatPercentage,
                                null
                            )
                        ],
                        now('Asia/Riyadh'),
                        $investingRecord
                    ),
                    app(ZatcaSettings::class)->seller,
                    'zatca-e-invoices',
                    $adminFeeTransaction
                );

                $notificationData = $this->notificationData($listing, $toBeInvestedAmountAndFees['total']->formatByDecimal(), $toBeInvestedShares);

                $this->send(NotificationTypes::Investment, $notificationData, $user->id);

                $this->cacheServices->updateFundCache($listing->id);
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new NewInvestmentTransaction($user, $listing, $toBeInvestedShares, $toBeInvestedAmountAndFees['total']));
                }
                return apiResponse(200, trans('messages.invest_success'), [
                    'invested_shares' => $token->shares
                ]);
            });

        } catch (ValidationException $e) {
            $code = $this->logServices->log('INVESTMENT-MOBILE', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (AuthorizationException $e) {
            $code = $this->logServices->log('INVESTMENT-MOBILE-AUTHORIZE', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (VerificationSmsException $e) {
            $code = $this->logServices->log('INVESTMENT-MOBILE-VERIFY', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (NoInvestorWalletFound $e) {
            $code = $this->logServices->log('INVESTMENT-MOBILE-NO-WALLET', $e);
            return apiResponse(400, __('messages.no_active_wallet_found_for_investor') . ' : ' . $code);
        } catch (NoBalanceException $e) {
            $code = $this->logServices->log('INVESTMENT-MOBILE-NO-BALANCE', $e);
            return apiResponse(400, __('messages.no_enough_balance') . ' : ' . $code);
        } catch (\Exception $e) {
            $code = $this->logServices->log('INVESTMENT-MOBILE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function checkAgeOfUser($user)
    {
        $today = Carbon::now();
        $todayInHijri = Hijri::Date('Y-m-d', $today->format('Y-m-d'));
        $result = 0;
        $birth_date = $user->nationalIdentity ? $user->nationalIdentity->birth_date : null;
        $start_date = new \DateTime($birth_date);
        $birth_date_type = $user->nationalIdentity ? $user->nationalIdentity->birth_date_type : null;
        if ($birth_date_type == "hijri") {
            $end = new \DateTime($todayInHijri);
            $result = optional($start_date->diff($end))->y < 18 ? 0 : 1;
        } else {
            $end = new \DateTime($today);
            $result = optional($start_date->diff($end))->y < 18 ? 0 : 1;
        }
        return $result;

    }

    public function notificationData($listing, $amount, $shares)
    {

        $content_ar_1 = ' تم إستلام مبلغ قدره: ' . $amount;
        $content_ar_2 = ' للاستثمار في فرصة: ' . $listing->title_ar;
        $content_ar_3 = ' عدد الوحدات ' . $shares;

        return [
            'action_id' => $listing->id,
            'channel' => 'mobile',
            'title_en' => 'Received Investment Amount',

            'title_ar' => 'تأكيد إستلام مبلغ الاستثمار',
            'content_en' => 'An investment amount of has been received : ' . $amount . ' to invest in opportunity ' . $listing->title_en . ' and number of units: ' . $shares,
            'content_ar' => $content_ar_1 . $content_ar_2 . $content_ar_3,
        ];
    }

    public function createCardToken($user, $request)
    {
        if (!$request->sub_mask && !$request->card_token) {
            return false;
        }

        $cardToken = null;

        $repeatedSubMusk = CardToken::where("sub_mask", $request->sub_mask)->where("user_id", $user->id)->first();

        if ($repeatedSubMusk) {
            $cardToken = $repeatedSubMusk->update([
                "sub_mask" => $request->sub_mask,
                "card_token" => $request->card_token,
                "user_id" => $user->id,
            ]);
        } else {
            $cardToken = CardToken::create([
                "sub_mask" => $request->sub_mask,
                "card_token" => $request->card_token,
                "user_id" => $user->id,
            ]);
        }
        return $cardToken;
    }

    public function createSubscriptionToken(Request $request, Listing $listing)
    {
        $data = $request->validate(['shares' => ['required', 'integer', 'min:1']]);

        $case = CheckIfCanInvestTesting::run(
            $listing,
            getAuthUser('api'),
            GetCurrentInvestments::run($listing),
            $data['shares']
        );

        return $case;

    }

    public function handle(User $user, Listing $listing, $shares, $payment_type = null)
    {
        return InvestorSubscriptionToken::create([
            'user_id' => $user->id,
            'listing_id' => $listing->id,
            'shares' => $shares,
            'reference' => Str::random(25),
            'payment_type' => $payment_type ? $payment_type : 'wallet'
        ]);
    }

    public function createUrwayTransaction($listing, $subscriptionToken)
    {

        $toBeInvestedAmountWithFees = CalculateInvestingAmountAndFees::run($listing, $subscriptionToken->shares)['total'];

        UrwayTransaction::create([
            'subscription_id' => $subscriptionToken->id,
            'amount' => $toBeInvestedAmountWithFees->getAmount() // ->formatByDecimal(),
        ]);
    }
}
