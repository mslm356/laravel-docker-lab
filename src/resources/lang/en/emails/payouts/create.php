<?php

return [
    'subject' => 'New Transaction in your account deposit returns',
    'subject_dis' => 'New Transaction in your account deposit distributions',
    'welcome_message' => ' Our dear investor ' ,
    'username' => ':name',
    'message_1' => 'You have a new transaction in your account',
    'message_2' => ' Deposit returns on investment opportunity  ' ,
    'message_2_dis' => ' Dividend for opportunity ' ,
    'listingName' => ':listing_name' ,
    'listing_num' => ':listing_num' . ' number ' ,
    'amount' => ':amount' . ' SAR ' ,
    'link_description' => 'To view your control panel, please visit the link',
    'dashboard_link' => 'view your control panel',
];
