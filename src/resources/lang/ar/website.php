<?php

return [
    'be_part_of_it_l1' => 'استثمر',
    'be_part_of_it_l2' => 'في الأصول',
    'buy_a_piece_of_it' => 'اشتر جزءً منها.',
    'invest_in_real_estate' => 'أصيل.. تسهل لك الاستثمار في القطاع العقاري من خلال فرص ذات عوائد مجدية.',
    'become_an_investor' => 'سجل كمستثمر',
    'how_it_works' => 'كيف تستثمر في أصيل؟',
    'cme_licensed' => 'أصيل منصة إلكترونية مرخصة من هيئة السوق المالية',
    'learn_more' => 'اقرأ المزيد',
    'listings' => 'الصناديق الاستثمارية',
    'listings_description' => 'استثمر في أي صندوق استثماري  بمبلغ ١٠٠٠ ريال سعودي كحد أدنى واحصل على العوائد وأنت في أي مكان في دول الخليج!',
    'how_it_works_list' => [
        'register' => [
            'title' => 'سجّل',
            'description' => 'كمستثمر في منصة أصيل لتتمكن من الاطلاع على كافة المعلومات التفصيلية عن الفرص الاستثمارية العقارية والتي تم اختيارها بعناية.',
        ],
        'charge_balance' => [
            'title' => 'اشحن',
            'description' => 'اشحن رصيدك من خلال الآيبان الخاص بك والموجود في لوحة التحكم، لتتمكن من الاشتراك في أي فرصة بكل سهولة.',
        ],
        'explore' => [
            'title' => 'استكشف',
            'description' => 'ابحث عن الفرصة الاستثمارية التي تتوافق مع أولوياتك من حيث العوائد المتوقعة، وكذلك المدة الزمنية، وموقع الفرصة ونوعيتها.',
        ],

        'invest' => [
            'title' => 'استثمر',
            'description' => 'عند اختيارك للفرصة والموافقة على الشروط والأحكام سيتم استثمار الوحدات التي تم الاشتراك بها لصالح هذا المشروع، وتصبح لديك ملكية موثقة في الفرصة العقارية.',
        ],
        'enjoy' => [
            'title' => 'استمتع بالعوائد',
            'description' => 'تابع سير التقدم من خلال لوحة التحكم الخاصة بك حتى يتم الإعلان عن الانتهاء من الفرصة الاستثمارية، ومن ثم استمتع بالعوائد المالية المودعة  في حسابك البنكي الخاص بمنصة أصيل.',
        ],
    ],
    'newsletter' => [
        'title' => 'هل تريد أن تصلك آخر الأخبار؟',
        'cta' => 'اشترك الآن في النشرة البريدية ليصلك جديدنا',
        'notify_me' => 'اشتراك',
        'email_placeholder' => 'أدخل بريدك الإلكتروني',
    ],
    'navlinks' => [
        'how_it_works' => 'كيف نعمل',
        'be_a_partner' => 'كن شريكًا',
        'invest' => 'استثمر',
        'login' => 'تسجيل الدخول',
        'signup' => 'التسجيل',
        'about_us' => 'عن أصيل',
        'dashboard' => 'لوحة التحكم',
        'logout' => 'تسجيل الخروج',
        'funds' => 'الفرص'
    ],
    'defs' => [
        'dividend_yield' => 'Financial ratio that shows how much is paid in dividends each year relative to the capital invested. Note: this is an estimate.',
        'gross_yield' => 'Financial ratio that shows how much is paid in dividends each year relative to the capital invested. Note: this is an estimate.',
    ],
    'footer' => [
        'links' => [
            'legal' => 'القانون',
            'privacy_policy' => 'سياسة الخصوصية',
            'sharia_compliance' => 'التوافق مع الشريعة الإسلامية',
            'terms_and_conditions' => 'الشروط والأحكام',
            'code_of_conducts' => 'القواعد والأخلاق',
            'disclosure_policy' => 'سياسة الإفصاح',
            'about_us' => 'عن أصيل',
            'contact_us' => 'اتصل بنا',
            'faqs' => 'الأسئلة الأكثر شيوعًا',
            'members_council' => 'مجلس الأعضاء',
        ],
        'headings' => [
            'legal' => 'القانون',
            'company' => 'الشركة',
            'code_of_conducts' => 'القواعد والأخلاق',
            'disclosure_policy' => 'سياسة الإفصاح',
        ],
    ],
    'all_rights_reserved' => ':date أصيل. جميع الحقوق محفوظة.',
    'cannot_find_an_answer' => 'لم تحصل على الإجابة التي تريد معرفتها؟ تواصل مع ',
    'cannot_find_an_answer_customer_support' => 'فريق الدعم الفني'
];
