<?php

namespace App\View\Components\Zatca;

use Illuminate\View\Component;

class DualLocale extends Component
{
    public $ar;
    public $en;
    public $fontSize;
    public $isBold;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($ar, $en, $fontSize = null, $color = null, $isBold = true)
    {
        $this->ar = $ar;
        $this->en = $en;
        $this->fontSize = $fontSize;
        $this->color = $color;
        $this->isBold = $isBold;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.zatca.dual-locale');
    }
}
