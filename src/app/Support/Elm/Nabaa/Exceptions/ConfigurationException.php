<?php

namespace App\Support\Elm\Nabaa\Exceptions;

use Exception;

class ConfigurationException extends Exception
{
    protected $requestBody;
    protected $errorCode;
    protected $errorType;
    protected $errorMsg;

    public function __construct(array $requestBody, $errorCode, $errorType, $errorMsg)
    {
        $this->requestBody = $requestBody;
        $this->errorCode = $errorCode;
        $this->errorType = $errorType;
        $this->errorMsg = $errorMsg;

        parent::__construct("Failed to send SMS");
    }

    /**
     * Get the exception's context information.
     *
     * @return array
     */
    public function context()
    {
        return [
            'request_body' => json_encode($this->requestBody),
            'error_code' => $this->errorCode,
            'error_type' => $this->errorType,
            'error_msg' => $this->errorMsg,
        ];
    }
}
