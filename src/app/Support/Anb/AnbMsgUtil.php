<?php

namespace App\Support\Anb;

use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AnbMsgUtil
{
    /**
     * Sign the given message
     *
     * @param string $body
     * @return string
     */
    public static function signMsg($reqVerb, $uri, $body)
    {
        $host = config('services.anb.original_host');

        $uri = ltrim($uri, '/');

        $uri = "/{$uri}";

        $stringToSign = "{$reqVerb}|{$host}|{$uri}|{$body}";

        $secret = config('services.anb.secret');

        $hashedContent = hash_hmac('sha256', $stringToSign, $secret);

        if ($hashedContent === false) {
            return false;
        }

        return base64_encode($hashedContent);
    }

    /**
     * Make directory with today's string
     *
     * @return string
     */
    public static function makeDirectory()
    {
        $date = now()->format('ymd');

        Storage::disk('local')->makeDirectory("anb/{$date}");

        return $date;
    }

    /**
     * Get the signed body after stripping out the unnecessary headers to be sent the bank
     *
     * @param string $filename
     * @return string
     */
    public static function getMsgWithoutHeaders($filename)
    {
        $signedPayloadArr = file($filename);

        // remove headers in the payload file to keep only the base 64 data
        for ($i = 1; $i <= 5; $i++) {
            array_shift($signedPayloadArr);
        }

        // convert array to string
        return implode('', $signedPayloadArr);
    }

    /**
     * Formate response body to be used in openssl_pkcs7_verify
     *
     * @param string $body
     * @return string
     */
    public static function formatResponseBody($body)
    {
        // Each line should contain maximum 64 character due to some PHP limitations
        // see openssl_pkcs7_verify for more details
        $fmtPayload = chunk_split($body, 64, "\n");

        // add necessary headers
        $headers = <<<HEADERS
MIME-Version: 1.0
Content-Disposition: attachment; filename="smime.p7m"
Content-Type: application/x-pkcs7-mime; smime-type=signed-data; name="smime.p7m"
Content-Transfer-Encoding: base64
HEADERS;

        $fmtPayload = $headers . "\n\n" . $fmtPayload;

        $ref = (string)Str::uuid();

        $date = self::makeDirectory();

        $signedResponsePath = storage_path("app/anb/{$date}/{$ref}.fmtmsg");

        file_put_contents($signedResponsePath, $fmtPayload);

        return $signedResponsePath;
    }

    /**
     * Vertify incoming request
     *
     * @param string $body
     * @return false|string
     */
    public static function verifyIncomingResponse($body)
    {
        $ref = (string)Str::uuid();

        $date = self::makeDirectory();

        $signedMsgPath = self::formatResponseBody($body);

        $date = self::makeDirectory();

        $certPath = storage_path("app/anb/{$date}/{$ref}.cert");

        file_put_contents($certPath, '');

        $decodedPath = storage_path("app/anb/{$date}/{$ref}.decoded");

        $isVerified =  openssl_pkcs7_verify(
            $signedMsgPath,
            PKCS7_NOVERIFY,
            $certPath
        );

        if (!$isVerified) {
            return false;
        }

        openssl_pkcs7_verify(
            $signedMsgPath,
            PKCS7_NOVERIFY,
            $certPath,
            [],
            $certPath,
            $decodedPath
        );

        $incomingCert = openssl_x509_parse(file_get_contents($certPath));

        $anbCert = openssl_x509_parse(file_get_contents(storage_path('anb/keys/b2b.anb.com.sa.pem')));

        unlink($certPath);
        unlink($signedMsgPath);

        if (
            $incomingCert['name'] !== $anbCert['name']
            || $incomingCert['serialNumber'] !== $anbCert['serialNumber']
        ) {
            return false;
        }

        $responseMsg = file_get_contents($decodedPath);

        unlink($decodedPath);

        return $responseMsg;
    }
}
