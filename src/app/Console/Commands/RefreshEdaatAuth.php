<?php

namespace App\Console\Commands;

use App\Services\Sdad\Repository\EdaatAuthRepository;
use Illuminate\Console\Command;

class RefreshEdaatAuth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edaat_auth:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'refresh edaat bearer token in the DB when it expired .';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $repo=new EdaatAuthRepository();
        $repo->refreshToken();
    }
}
