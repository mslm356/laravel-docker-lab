<?php

namespace App\Http\Resources\Admins\Investors;

use Illuminate\Http\Resources\Json\JsonResource;

class InvestorInvestmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'title' => $this->title,
            'invested_shares' => $this->pivot->invested_shares,
            'invested_amount' => $this->pivot->invested_amount->formatByDeciaml()->getAmount(),
            'invested_amount_fmt' => $this->pivot->invested_amount->format(),
        ];
    }
}
