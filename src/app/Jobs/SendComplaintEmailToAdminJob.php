<?php

namespace App\Jobs;

use App\Models\Complaint;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\Complaint\SendReceivedComplaintEmailToTheAdmin;

class SendComplaintEmailToAdminJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $complaint;

    /**
     * Create a new job instance.
     *
     * @param string $email
     */
    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        $adminEmail = config('mail.complaint_admin_email');

        $admins = [
            'Abdullah@investaseel.sa',
            'rawan@investaseel.sa'
        ];

        foreach ($admins as $admin) {
            Mail::to($admin)->send(new SendReceivedComplaintEmailToTheAdmin($this->complaint, $admin));
        }
    }
}
