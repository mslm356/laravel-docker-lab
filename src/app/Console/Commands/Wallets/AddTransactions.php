<?php

namespace App\Console\Commands\Wallets;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AddTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:transaction {--nin=} {--amount=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $nin = NationalIdentity::where('nin', $this->option('nin'))->first();

        if (!$nin) {
            dd('nin not valid');
        }

        $user = $nin->user;

        if (!$user) {
            dd('user not valid');
        }

        $wallet = $user->getWallet(WalletType::Investor);

        if (!$wallet) {
            dd('wallet not valid');
        }

        $amount = money_parse_by_decimal($this->option('amount'), defaultCurrency());

        $transaction = $wallet->deposit(
            $amount->getAmount(),
            [
                'reference' => transaction_ref(),
                'reason' => TransactionReason::AnbStatement,
                'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
                'source'=> 'cons-v2'
            ]
        );
        
        dump('deposit done success');
    }
}
