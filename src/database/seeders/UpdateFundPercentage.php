<?php

namespace Database\Seeders;

use App\Models\Listings\Listing;
use Illuminate\Database\Seeder;

class UpdateFundPercentage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listings = Listing::get();

        foreach ($listings as $listing) {
            $listing->update([
                'invest_percent' => $listing->percentage
            ]);
        }
    }
}
