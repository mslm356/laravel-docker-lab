<?php

namespace App\Services\Sdad\Contracts;

use Illuminate\Database\Eloquent\Model;

interface EdaatAuthRepositoryContract
{

    /**
     * @param array $credentials
     * @return Model|null
     */
    public function makeAuth(): ?Model;

    /**
     * @return Model|null
     */
    public function getValidToken(): ?Model;

    /**
     * @param Model $model
     * @return bool|null
     */
    public function checkAuth(Model $model): ?bool;

    public function refreshToken();
}
