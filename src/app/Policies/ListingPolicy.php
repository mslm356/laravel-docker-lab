<?php

namespace App\Policies;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ListingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function accessListingWhichInvestedIn(User $user, Listing $listing)
    {
        return $user->investments()
            ->wherePivot('invested_shares','>',0)
            ->where('listings.id', $listing->id)
            ->first();
    }


    public function accessListing(User $user, Listing $listing)
    {
        return $listing->is_visible;
    }

    /**
     * Can Authorized Entity access listing
     *
     * @param \App\Models\Users\User $user
     * @param \App\Models\Listings\Listing $listing
     * @return boolean
     */
    public function accessListingByAuthorizedEntity(User $user, Listing $listing)
    {
        return $user->authorized_entity_id === $listing->authorized_entity_id;
    }
}
