<?php

namespace App\Http\Resources\Admins\Listings;

use App\Http\Resources\Common\Listings\ListingListItemResource as CommonListingListItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingListItemResource($this->resource))->toArray($request),
            []
        );
    }
}
