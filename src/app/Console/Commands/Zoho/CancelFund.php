<?php

namespace App\Console\Commands\Zoho;

use App\Enums\Zoho\AccountTypes;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Support\Zoho\Account;
use App\Support\Zoho\Auth;
use App\Support\Zoho\CustomerPayments;
use App\Support\Zoho\Settings;
use App\Support\Zoho\Tax;
use Illuminate\Console\Command;

class CancelFund extends Command
{

    public $zohoSettings;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:cancel-fund {--fund=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cancel fund with zoho';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->zohoSettings = new Settings();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $fund = Listing::find($this->option('fund'));

//        $zoho = new Auth();
//        $data = $zoho->getToken($this->option('code'));
//
//        dd($data);
    }
}
