<?php


namespace App\Support\Wathq;


use App\Services\LogService;
use Illuminate\Support\Facades\Http;

class CompanyInfo
{

    public $base_url;
    public $logServices;
    public $apiKey;

    public function __construct()
    {
        $this->base_url = 'https://api.wathq.sa/v5/commercialregistration';
//        $this->base_url = 'https://api.wathq.sa/v5/commercialregistration';
        $this->apiKey = 'EQyJKd2YOemrZlCmtWTv4Ru6zdmGGF3V';
        $this->logServices = new  LogService();
    }

    public function handle($data)
    {
        try {

            $types = [
                'full_info' => 'fullInfo',
                'base_info' => 'baseInfo',
            ];

            if (!isset($data['type']) || !isset($types[$data['type']])) {
                $code = $this->logServices->log('WATHQ-FUNCTION-TYPE', null, ['some thing went wrong', $data]);
                return $this->handleResponse('func type not found : ' . $code, 400);
            }

            $methodName = $types[$data['type']];

            return $this->$methodName($data);

        } catch (\Exception $e) {

            $code = $this->logServices->log('WATHQ-EXCEPTION', $e, ['some thing went wrong', $data]);

            $msg = 'some thing went wrong, please try later : ' . $code;

            return $this->handleResponse($msg, 400);
        }
    }

    public function baseInfo($data)
    {
        $cr = isset($data['cr']) ? $data['cr'] : null;

        $response = Http::asForm()
            ->withHeaders([
                'Accept' => 'application/json',
                'apiKey' => $this->apiKey,
            ])
            ->get($this->base_url . '/info/' . $cr);

        $responseData = json_decode($response->body(), 2);
        $responseData['reference_number'] = $response->header('THIQAH-API-ApiMsgRef');
        $responseData['response_status'] = $response->status();

        return $responseData;
    }

    public function fullInfo($data)
    {

        $url = $this->base_url . '/fullinfo/' . $data['cr'];

        $response = Http::asForm()
            ->withHeaders([
                'Accept' => 'application/json',
                'apiKey' => $this->apiKey,
            ])
            ->get($url);

        dd(json_decode($response->body()));

        return $response;
    }

    public function handleResponse($msg, $status)
    {

        return [
            'message' => $msg,
            'response_status' => $status
        ];
    }

}
