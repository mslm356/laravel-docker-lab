<?php

return [
    'subject' => 'تم صدور تقرير عن فرصة ":fund_title "
    الاستثمارية',
    'message' => 'تم اضافه تقرير من قبل ال "  :username " ',
    'message_1' => 'علي الفرصه ":fund_title "',
    'message_2' => 'في انتظار المراجعه',
    'end_message' => 'شكراً لثقتك في منصة أصيل',
];
