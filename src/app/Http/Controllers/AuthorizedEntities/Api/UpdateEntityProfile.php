<?php

namespace App\Http\Controllers\AuthorizedEntities\Api;

use App\Actions\AuthorizedEntities\UpdateEntity;
use App\Enums\AuthorizedEntityStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorizedEntities\UpdateEntityRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\DB;

class UpdateEntityProfile extends Controller
{
    /**
     * Update entity
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(UpdateEntityRequest $request)
    {
        $authorizedEntity = authorizedEntity();

        $fileInputs = [
            'cr',
            'cma_license',
        ];

        foreach ($fileInputs as $file) {

            $responseOfCheckFile = $this->checkExtensions($request[$file]);

            if (!$responseOfCheckFile['status']) {
                return apiResponse(400, __('messages.file_not_valid'));
            }
        }

        if (!in_array(
            $authorizedEntity->status,
            [AuthorizedEntityStatus::PendingReview]
        )) {
            throw new AuthorizationException();
        }

        DB::transaction(function () use ($authorizedEntity, $request) {
            $entity = UpdateEntity::run($authorizedEntity, $request->validated());

            $entity->update([
                'status' => AuthorizedEntityStatus::PendingReview,
                'reviewer_id' => null,
                'reviewer_comment' => null
            ]);
        });

        return response()->json();
    }

    public function checkExtensions($files)
    {

        $response = ['status' => true];

        $extensions = ['pdf', 'png', 'jpg', 'jpeg', 'heic', 'heif'];

        if (is_array($files)) {

            foreach ($files as $file) {

                $extension = strtolower($file->getClientOriginalExtension());

                if (!checkFileExtensions($extension, $extensions)) {
                    $response['status'] = false;
                    return $response;
                }
            }
        } else {

            $extension = strtolower($files->getClientOriginalExtension());

            if (!checkFileExtensions($extension, $extensions)) {
                $response['status'] = false;
                return $response;
            }
        }

        return $response;
    }

}
