<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorInterface;

class CompensationType implements GeneratorInterface
{
    /**
     * Generate description
     *
     * @return string
     */
    public function generate(Transaction $transaction, $locale = null): string
    {
        return __('transaction-description.compensation');
    }
}
