<?php


namespace App\Support\Zoho;


use App\Enums\Banking\TransactionReason;
use App\Enums\Zoho\AccountTypes;
use App\Jobs\Zoho\CustomerPayment;
use App\Jobs\Zoho\ManualJournalJob;
use App\Jobs\Zoho\NewCustomer;
use App\Jobs\Zoho\NewInvoice;
use App\Jobs\Zoho\NewItem;
use App\Models\Listings\Listing;
use App\Services\LogService;
use App\Settings\AccountingSettings;
use App\Support\Zoho\JournalPrepareData\FundInvest;
use App\Support\Zoho\JournalPrepareData\InvestorTransfer;

class Transactions
{

    public $logServices;
    public $zohoCustomer;
    public $fundInvest;
    public $investorTransfer;
    public $zohoItem;
    public $zohoInvoice;
    public $zohoCustomerPayments;
    public $zohoManualJournal;

    public function __construct()
    {
        $this->logServices = new LogService();
        $this->zohoCustomer = new Customer();
        $this->fundInvest = new FundInvest();
        $this->investorTransfer = new InvestorTransfer();
        $this->zohoItem = new Item();
        $this->zohoCustomerPayments = new CustomerPayments();
        $this->zohoInvoice = new Invoice();
        $this->zohoManualJournal = new ManualJournal();
    }

    public function handle($transaction)
    {

        $user = $transaction->payable;

        if (!$user) {
            $this->logServices->log('ZOHO-TRANSACTION-USER-INVALID', null, ['user not valid']);
            return false;
        }

        $this->createZohoCustomer($user);
        $this->createZohoFund($transaction);

        $reasons = [
            TransactionReason::AnbStatement => 'createCustomerPayment',
            TransactionReason::InvestingInListing => 'createJournal',
            TransactionReason::AnbExternalTransfer => 'createJournal',
            TransactionReason::RefundAfterAnbTransferFailure => 'createCustomerPayment',
            TransactionReason::InvestingInListingAdminFee => 'createInvoice',
            TransactionReason::UrwayReason => 'createCustomerPayment',
        ];

        $fun = $reasons[$transaction->reason];
        $this->$fun($transaction);
    }

    public function createZohoCustomer($user)
    {

        if ($user->zoho_id == null) {

//            $this->logServices->log('customer-check', null, ['test check user' , $user->id]);

            $this->zohoCustomer->saveCustomer($user, \App\Enums\Zoho\NewCustomer::Customer);
        }
    }

    public function createZohoFund($transaction)
    {

        $fund_id = isset($transaction->meta['listing_id']) ? $transaction->meta['listing_id'] : null;

        $fund = Listing::find($fund_id);

        if ($fund && $fund->zoho_id == null) {


            $this->zohoCustomer->saveCustomer($fund, \App\Enums\Zoho\NewCustomer::Fund);

//            $zohoNewCustomerJob = (new NewCustomer($fund, \App\Enums\Zoho\NewCustomer::Fund));
//            dispatch($zohoNewCustomerJob);

            $this->zohoItem->store($fund);

//            $zohoNewItemJob = (new NewItem($fund));
//            dispatch($zohoNewItemJob);
        }

    }

    public function createCustomerPayment($transaction)
    {

        $user = $transaction->payable;
        $amount = money($transaction->amount, defaultCurrency())->absolute()->formatByDecimal();

        $payment_id = $this->zohoCustomerPayments->createCustomerPayment($user, $amount);

        if ($payment_id && $transaction) {
            $transaction->zoho_id = $payment_id;
            $transaction->save();
        }
    }

    public function createInvoice($transaction)
    {

        $user = $transaction->payable;
        $listingId = isset($transaction->meta["listing_id"]) ? $transaction->meta["listing_id"] : null;
        $listing = Listing::find($listingId);

        if (!$listing) {
            $this->logServices->log('ZOHO-TRANSACTION-INVOICE-LISTING-INVALID', null, ['listing not valid', $listingId, $transaction]);
            return false;
        }

        $shares = $transaction->meta["shares"];

        $invoice_id = $this->zohoInvoice->store($listing, $user, $shares);

        if ($invoice_id && $transaction) {
            $transaction->zoho_id = $invoice_id;
            $transaction->save();
        }
    }

    public function createJournal($transaction)
    {

        $user = $transaction->payable;
        $listingId = isset($transaction->meta["listing_id"]) ? $transaction->meta["listing_id"] : null;
        $listing = Listing::find($listingId);

        if (!$listing) {
            $this->logServices->log('ZOHO-TRANSACTION-JOURNAL-LISTING-INVALID', null, ['listing not valid', $listingId, $transaction]);
            return false;
        }

        $journalData = $this->journalData($transaction, $user, $listing);

        if (!$journalData) {
            $this->logServices->log('ZOHO-TRANSACTION-JOURNAL-DATA-INVALID', null, ['journal data not valid', $journalData, $transaction]);
            return false;
        }

        $journal_id = $this->zohoManualJournal->store($journalData);

        if ($journal_id && $transaction) {
            $transaction->zoho_id = $journal_id;
            $transaction->save();
        }
    }

    public function journalData ($transaction, $user, $listing) {

        $amount = money($transaction->amount, defaultCurrency())->absolute()->formatByDecimal();

        if ($transaction->reason == TransactionReason::InvestingInListing) {

            return $this->fundInvest->prepareFundInvestData($user, $listing, AccountTypes::InvestorWallet,
                AccountTypes::FundWallet, $amount);

        }elseif ($transaction->reason == TransactionReason::InvestingInListing) {

            return $this->investorTransfer->prepareFundInvestData($user,AccountTypes::InvestorWallet,
                AccountTypes::ANBBank, $amount);
        }

        return false;
    }

}
