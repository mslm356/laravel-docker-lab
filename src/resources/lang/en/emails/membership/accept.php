<?php

return [
    'subject' => 'Early Investment',
    'title' => 'Congrats! Your account has been upgraded to membership (:membership)',
    'name' => ':name',
    'name_title' => 'Our investor',
    'desc' => 'Due to the volume of your investments in Aseel platform, we are pleased to inform you that you are eligible to upgrade your membership
    The membership gives you access to more benefits on the platform
    Please visit the Dashboard on the website or More in the application',
];
