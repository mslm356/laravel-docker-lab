<?php

namespace App\Rules\Listings;

use Illuminate\Contracts\Validation\Rule;

class MaxTotalShares implements Rule
{
    protected $minSharePrice;
    protected $targetAttribute;
    protected $target;

    public function __construct($targetAttribute, $minSharePrice = null)
    {
        $this->target = request()->input($targetAttribute);
        $this->minSharePrice = $minSharePrice ?? config('system.listings.min_share_price');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_numeric($this->target)) {
            return false;
        }
        // TODO: need review with adding Money
        return ($this->target / $value) >= $this->minSharePrice;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.lte.numeric', [
            'value' => $this->target / $this->minSharePrice
        ]);
    }
}
