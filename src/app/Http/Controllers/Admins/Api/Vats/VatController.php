<?php

namespace App\Http\Controllers\Admins\Api\Vats;

use App\Rules\NumericString;
use Illuminate\Http\Request;
use App\Models\Accounting\Vat;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Vat\VatResource;

class VatController extends Controller
{
    /**
     * Show Admin Profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return VatResource::collection(
            Vat::all()
        );
    }

    /**
     * Show VAT.
     *
     * @return \App\Http\Resources\Admins\Vat\VatResource
     */
    public function show(Vat $vat)
    {
        return new VatResource($vat);
    }

    /**
     * Create VAT
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name' => ['required', 'string'],
            'percentage' => ['required', new NumericString]
        ]);

        Vat::create([
            'name' => $data['name'],
            'percentage' => $data['percentage'],
        ]);

        return response()->json([], 201);
    }

    /**
     * Delete VAT
     *
     * @param \App\Models\Accounting\Vat $vat
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Vat $vat)
    {
        $vat->delete();

        return response()->json([], 201);
    }
}
