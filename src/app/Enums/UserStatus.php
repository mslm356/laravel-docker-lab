<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class UserStatus extends Enum
{
    const Pending =   1;
    const Active =   2;
    const Rejected =   3;
    const Frozen =   4;
}
