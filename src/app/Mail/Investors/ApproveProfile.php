<?php

namespace App\Mail\Investors;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApproveProfile extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/app/overview';

        return $this->view('emails.investors.approve_profile', compact('profileUrl'))
            ->subject(__('emails/investors/approve_profile.subject'));
    }
}
