<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueIdToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('unique_id')->nullable();
        });

//        \Illuminate\Support\Facades\DB::statement("ALTER TABLE `transactions` ADD COLUMN `unique_id` varchar(255) GENERATED ALWAYS AS (`meta` ->> '$.unique_id') STORED NULL AFTER `meta`;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('unique_id');
        });
    }
}
