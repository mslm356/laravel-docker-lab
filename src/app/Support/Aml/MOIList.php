<?php

namespace App\Support\Aml;

use App\Enums\AML\AmlList;

class MOIList implements AmlListInterface
{
    protected $list = null;

    protected $fileName = null;

    /**
     * Get the list
     *
     * @return \SimpleXMLElement
     */
    public function getLists()
    {
        return [];
    }

    /**
     * Check the infomation in the list
     *
     * @param string|null $firstName
     * @param string|null $secondName
     * @param string|null $thirdName
     * @param string|null $fourthName
     * @param string|null $nationality
     * @param string|null $dob
     * @param string|null $nationalId
     * @param string|null $passportId
     * @return array
     */
    public function check($firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId)
    {
        return [];
    }

    /**
     * get List index
     *
     * @return int
     */
    function getListIndex(): int
    {
        return AmlList::MOI;
    }
}
