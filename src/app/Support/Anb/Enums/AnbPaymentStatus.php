<?php

namespace App\Support\Anb\Enums;

use BenSampo\Enum\Enum;

final class AnbPaymentStatus extends Enum
{
    const INITIATED = 'INITIATED';
    const QUEUED = 'QUEUED';
    const CANCELED = 'CANCELED';
    const PENDING = 'PENDING';
    const FAILED = 'FAILED';
    const PROCESSED = 'PROCESSED';
    const PAID = 'PAID';

    public static function inProcessOrPaidStatuses()
    {
        return [
            static::INITIATED,
            static::QUEUED,
            static::PENDING,
            static::PROCESSED,
            static::PAID
        ];
    }

    public static function canceledOrFailedStatuses()
    {
        return [
            static::FAILED,
            static::CANCELED,
        ];
    }

    public static function pendingStatuses()
    {
        return [
            static::INITIATED,
            static::QUEUED,
            static::PENDING,
        ];
    }
}
