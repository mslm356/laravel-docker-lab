<?php

namespace App\Support\Elm\Yaqeen;

use App\Support\Elm\Yaqeen\Repositories\FakeRepository;
use App\Support\Elm\Yaqeen\Repositories\YaqeenRepository;

class Yaqeen
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        $instance = null;
        if (config('services.yaqeen.mode') === 'fake') {
            $instance = new FakeRepository;
        } else {
            $instance = new YaqeenRepository;
        }

        self::$instance = $instance;

        return self::$instance;
    }
}
