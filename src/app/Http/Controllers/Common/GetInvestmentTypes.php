<?php

namespace App\Http\Controllers\Common;

use App\Enums\InvestmentType;
use App\Http\Controllers\Controller;

class GetInvestmentTypes extends Controller
{
    public function __invoke()
    {
        return response()->json([
            'data' => collect(InvestmentType::asArray())->map(fn ($value) => [
                'name' => InvestmentType::getDescription(($value)),
                'value' => $value
            ])->values()
        ]);
    }
}
