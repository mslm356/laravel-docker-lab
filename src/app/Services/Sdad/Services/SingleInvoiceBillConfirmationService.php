<?php

namespace App\Services\Sdad\Services;

use App\Services\LogService;
use App\Services\Sdad\Enums\InvoiceCodesStatusesEnums;
use App\Services\Sdad\Repository\BillConfirmationRepository;
use App\Services\Sdad\Repository\SingleInvoiceInquiryRepository;
use App\Services\Sdad\Repository\SingleInvoicesRepository;
use App\Traits\NotificationServices;

class SingleInvoiceBillConfirmationService
{
    use NotificationServices;

    public $bill_confirmation_repo;
    public $invoice_inquiry_repo;
    public $invoice_create_repo;
    public $logService;

    public function __construct()
    {
        $this->invoice_create_repo = new SingleInvoicesRepository();
        $this->bill_confirmation_repo = new BillConfirmationRepository();
        $this->invoice_inquiry_repo = new SingleInvoiceInquiryRepository();
        $this->logService = new LogService();


    }

    public function create_bill_confirmation(array $request_objects): ?bool
    {
        try {

            foreach ($request_objects as $confirmation_obj) {
                $bill_confirmation = $this->bill_confirmation_repo->create($confirmation_obj);
                $invoice = $bill_confirmation->invoice;
                if ($invoice->status == InvoiceCodesStatusesEnums::CREATED) {
                    $inquiry_result = $this->invoice_inquiry_repo->initiate_provider($invoice->edaat_invoice_id)->inquiry($invoice->edaat_invoice_id);
                    $inquiry_data = [
                        'status' => $inquiry_result->Body->StatusCode,
                        'invoice_brief_result' => $inquiry_result
                    ];
                    $this->invoice_create_repo->update($invoice->id, $inquiry_data);
                }
            }
            return true;
        } catch (\Exception $e) {
            $this->logService->log('Sdad-Bill-Confirmation-Service-ERROR', $e, ['issue', json_encode($request_objects)]);

            throw new \Exception('SingleInvoiceBillConfirmationService create_bill_confirmation Creation error');
        }
    }
}
