<?php

namespace App\Http\Resources\Investors;

use Illuminate\Http\Resources\Json\JsonResource;

class MyUnitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dividends = 0;

        $this->payouts->each(function ($payout) use (&$dividends) {
            $dividends += $payout->investors->sum('pivot.paid');
        });

        $investedAmount = $this->pivot->invested_amount;
        $investedShares = $this->pivot->invested_shares;

        $valuationValue = $this->valuation_unit ? $this->valuation_unit->formatByDecimal() : $this->Share_price->formatByDecimal();

        $valuationInvested = $valuationValue * $investedShares;

        $unrealizedPL = calculateValuation($valuationInvested, $investedAmount);
        $unrealizedPLPercent =  $investedAmount > 0 ? ($unrealizedPL/($investedAmount/100)) : 0 ;

        return [
            'id' => $this->id,
            'title' => $this->title,
            'has_vote' => $this->votes &&  optional($this->votes)->count() ?  true : false,
            'capital_invested' => number_format(money($investedAmount)->formatByDecimal(), 2),
            'dividends' => number_format($dividends, 2),
            'acquisition_cost' => $acquisitionCost = number_format($this->target->formatByDecimal(), 2),
            'work_date' => optional(\DateTime::createFromFormat('Y-m-d h:i:s', $this->work_date))->format("Y/m/d") ,
            'shares' => $investedShares,
            'valuation' => $this->valuation_unit ? number_format($this->valuation_unit->formatByDecimal(), 2) : number_format($this->share_price->formatByDecimal(), 2),
            'market_value' => number_format($valuationInvested, 2),
            'unrealized_pl' => (int)$unrealizedPL,
            'unrealized_pl_fmt' =>  number_format($unrealizedPL, 2) .' ('. $unrealizedPLPercent * 100 . '%)',
            'unit_price' => number_format($this->share_price->formatByDecimal(), 2),
        ];
    }
}
