<?php

namespace App\Actions\Listings\Images;

use App\Models\Listings\ListingImage;
use Illuminate\Support\Facades\Storage;
use Lorisleiva\Actions\Concerns\AsAction;

class DeleteListingImage
{
    use AsAction;

    public function handle(ListingImage $image)
    {
        $path = $image->path;

        $image->delete();

        Storage::disk(config('filesystems.public'))->delete($path);
    }
}
