<?php


namespace App\Support\Zoho;


use App\Models\ZohoSettings;

class Settings
{
    public function saveSettings($key, $value)
    {

        $setting = ZohoSettings::where('key', $key)->first();

        if ($setting) {

            $setting->value = $value;
            $setting->save();
            return true;
        }

        ZohoSettings::create([
            'key' => $key,
            'value' => $value ,
        ]);

        return true;
    }
}
