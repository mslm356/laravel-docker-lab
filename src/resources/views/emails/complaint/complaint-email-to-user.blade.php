@extends('emails.partials.main')
@section('content')
    <tr> {{ __('emails/complaint/user-email.dear', ['name' => $complaint->user->first_name], $complaint->user->locale) }} </tr>
    <br>

    <tr> {{ __('emails/complaint/user-email.message', ['ticketNumber' => $complaint->id], $complaint->user->locale) }} </tr>
    <br>
    <tr> {{ __('emails/complaint/user-email.thanks', [], $complaint->user->locale) }} </tr>
{{--    <br>--}}
@endsection


