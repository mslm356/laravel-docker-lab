<?php

namespace App\Console\Commands\Temp;

use App\Models\Listings\ListingInvestor;
use App\Models\Payouts\Payout;
use Illuminate\Console\Command;

class CalculateOldPayout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-old-payout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $payouts=Payout::where('type','payout')->with('investors')->get();
        $count=0;
       foreach ($payouts as $payout)
       {

           foreach ($payout->investors as $investor_payout)
           {
               if ($investor_payout->pivot->paid > 0)
               {
                   ListingInvestor::where('listing_id',$payout->listing_id)
                       ->where('investor_id',$investor_payout->pivot->investor_id)
                       ->update(['paid_payout'=>$investor_payout->pivot->paid]);
                   $count++;
               }
           }
       }

    dd('done'. ' ' . $count);
    }
}
