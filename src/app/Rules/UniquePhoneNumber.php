<?php

namespace App\Rules;

use App\Models\Users\User;
use Illuminate\Contracts\Validation\Rule;

class UniquePhoneNumber implements Rule
{
    protected $countryAttribute;
    protected $ignore;
    protected $attribute;

    public function __construct($countryAttribute)
    {
        $this->countryAttribute = $countryAttribute;
    }

    public function ignore(User $user)
    {
        $this->ignore = $user;

        return $this;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        $countryCode = request()->input($this->countryAttribute);

        return User::where('phone_number', phone($value, $countryCode))
            ->when(isset($this->ignore), function ($query) {
                $query->where('id', '!=', $this->ignore->id);
            })
            ->count() === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique', [
            'attribute' => __('validation.attributes')[$this->attribute]
        ]);
    }
}
