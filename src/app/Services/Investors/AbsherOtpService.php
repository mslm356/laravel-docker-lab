<?php


namespace App\Services\Investors;

use App\Enums\Elm\AbsherOtpReason;
use App\Jobs\Absher\SendOtp;
use App\Models\Elm\AbsherOtp;
use App\Services\Investors\Investment\HandleResponseService;
use App\Services\LogService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class AbsherOtpService
{

    public $logService;

    public function __construct()
    {
        $this->handleResponseService = new HandleResponseService();
        $this->logService = new LogService();
    } public $handleResponseService;

    public function createOtp($user, $token=null, $delay=null, $nin=null)
    {
        $code = config('services.absher_otp.mode') === 'fake' ? '1973' : generateRandomCode(6);
        $absherOtp = AbsherOtp::create([
            'id' => (string)Str::uuid(),
            'user_id' => $user->id,
            'code' => $code,
            'verification_code' => Hash::make($code),
            'customer_id' => $nin ?? $user->nationalIdentity->nin,
            'data'=> ['subscription_token_id' => $token ? $token->id : null]
        ]);

        $this->absherJob($user, $token, $absherOtp, $delay, $nin);

        return $absherOtp;
    }

    public function absherRequest($absherOtp, $nin, $delay)
    {

        $delay = $delay ?? 0 ;

        $url = config('app.absher_middleware_send_url') . '?nationalId=' . $nin . '&vId=' . $absherOtp->id . '&description="ar"' . '&delay=' . $delay;

        $response = Http::async()->get($url)->then(function ($response) use ($url) {
            $this->logService->log('ABSHER-MIDDLEWARE-send', null, ['test', $response->body(), $url]);
        });

        $response->wait();
    }

    public function absherJob($user, $token, $absherOtp, $delay, $nin)
    {
        $ip_server = (config('app.env') !='local')?$_SERVER['SERVER_ADDR']:'127.0.0.1:8000';

        $sendOtpJob = (new SendOtp($user, $token, $absherOtp, AbsherOtpReason::InvestingInFund, 0, $nin));
        $delay=json_decode($delay);

        if ($delay && $delay->max > 0) {
            $randomNumber = rand($delay->min, $delay->max);
            dispatch($sendOtpJob)->onQueue('absher-' . $ip_server)->delay(now()->addSeconds($randomNumber));
        } else {
            dispatch($sendOtpJob)->onQueue('absher-' . $ip_server);
        }



    }

}
