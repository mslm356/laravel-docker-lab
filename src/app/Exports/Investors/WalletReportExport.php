<?php

namespace App\Exports\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class WalletReportExport implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('wallets')
            ->where('created_at', '>=', $this->data['start_date'])
            ->where('created_at', '<=', $this->data['end_date'])
            ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Uuid',
            'Holder Type',
            'Holder Id',
            'Name',
            'Slug',
            'Description',
            'Meta',
            'Balance',
            'Decimal Places',
            'Created_at',
            'Updated_at',
        ];
    }

    public function map($wallet): array
    {
        return [
            $wallet->id,
            $wallet->uuid,
            $wallet->holder_type,
            $wallet->holder_id,
            $wallet->name,
            $wallet->slug,
            $wallet->description,
            $wallet->meta,
            $wallet->balance,
            $wallet->decimal_places,
            $wallet->created_at,
            $wallet->updated_at,
        ];
    }
}
