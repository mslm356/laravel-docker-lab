<?php


namespace App\Support\Aml;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\AML\AmlApprove;
use App\Enums\Investors\InvestorStatus;
use App\Models\AML\AmlFocalHistory;
use App\Models\AML\AmlUser;
use App\Models\Users\User;
use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class AmlServices
{

    public $base_url;
    public $logServices;

    public function __construct()
    {
        $this->base_url = 'https://app.sa.focal.mozn.sa/api/v2/';
        $this->logServices = new  LogService();
    }

    public function handle(array $data)
    {
        try {

            $types = [
                'screen' => 'screen',
                'inQuery' => 'getQueryByCustomer',
                'approveAml' => 'approveAmlScreens',
            ];

            if (!isset($data['type']) || !isset($types[$data['type']])) {
                $code = $this->logServices->log('AML-FUNCTION-TYPE-NOT-FOUND', null, ['some thing went wrong', $data]);
                return $this->handleResponse('func type not found : ' . $code, 400);
            }

            $methodName = $types[$data['type']];

            return $this->$methodName($data);

        } catch (\Exception $e) {

            $code = $this->logServices->log('AML-EXCEPTION', $e, ['some thing went wrong', $data, ['user_id' => $data['user']->id]]);

            $msg = 'some thing went wrong, please try later : ' . $code;

            return $this->handleResponse($msg, 400);
        }
    }

    public function accessToken()
    {

        try {

            $data = [
                'username' => 'ali@investaseel.sa',
                'password' => 'AC/nT2Z`k#7Jmvs4?',
            ];

            $response = Http::asForm()
                ->withHeaders([
                    'Accept' => 'application/json',
                ])
                ->post($this->base_url . 'login/access-token', $data);

            $body = json_decode($response->body(), true);

            if (!isset($body['access_token']) || !isset($body['token_type'])) {
                $this->logServices->log('AML-ERROR-ACCESS-TOKEN', null, ['AML - ACCESS TOKEN NOT VALID']);
                return null;
            }

            return $body['token_type'] . ' ' . $body['access_token'];

        } catch (\Exception $e) {
            $this->logServices->log('AML-EXCEPTION', $e);
            return null;
        }
    }

    public function screen($data)
    {
        try {

            if (!isset($data['user'])) {
                $code = $this->logServices->log('AML-SCREEN-USER', null, ['aml screen user not valid', $data]);
                $msg = 'aml screen user not valid : ' . $code;
                return $this->handleResponse($msg, 400);
            }

            $user = $data['user'];

            if (!$user || !$user->nationalIdentity) {
                $code = $this->logServices->log('AML-SCREEN-USER-IDENTITY', null, ['aml screen user identity not valid', $data]);
                $msg = 'aml screen user identity not valid : ' . $code;
                return $this->handleResponse($msg, 400);
            }

            $nationalIdentity = $user->nationalIdentity;

            $date = explode('-', $nationalIdentity->birth_date);
            $birthDate = $nationalIdentity->birth_date_type == 'gregorian' ? $nationalIdentity->birth_date : Hijri::DateToGregorianFromDMY($date[2], $date[1], $date[0]);
            $values = [
                'customer_reference_id' => $user->id,
                'fname' => $nationalIdentity->getTranslation('first_name', 'en'),
                'mname' => $nationalIdentity->getTranslation('second_name', 'en') . ' ' . $nationalIdentity->getTranslation('third_name', 'en'),
                'lname' => $nationalIdentity->getTranslation('forth_name', 'en'),
                'date' => Carbon::parse($birthDate)->format('Y m d'),
                'iden' => $nationalIdentity->nin,
            ];

            $values = json_encode($values);

            $response = $this->callAml('POST', 'screen/individual?with_risk_assesment_result=true', $values, true);

            $response = json_decode($response, true);

            if (!isset($response['detail'])) {

                $this->createAmlUsers($response, $user->id);
                return $this->handleResponse(__('mobile_app/message.created_successfully'), 201, $response);
            }

            $this->logServices->log('AML-SCREEN-FAILED-RESPONSE', null, ['failed response', $response, ['user_id' => $data['user']->id]]);

            return $this->handleResponse(__('mobile_app/message.try_later'), 400);

        } catch (\Exception $e) {

            $code = $this->logServices->log('AML-SCREEN-EXCEPTION', $e, ['user_id' => $data['user']->id]);
            $msg = 'aml exception sorry please try later : ' . $code;
            return $this->handleResponse($msg, 400);
        }
    }

    public function createAmlUsers($response, $user_id)
    {
        if (!is_array($response)) {
            $response = json_decode($response,1);
        }

        $data = [
            'query_id' => $response["query_id"],
            'customer_reference_id' => $response["customer_reference_id"],
            'query_status' => $response["query_status"],
            'user_id' => $user_id,
            'response' => json_encode($response),
            'risk_result' => isset($response['risk_assessment']['risk_result']) ? $response['risk_assessment']['risk_result'] : null,
            'screen_result' => empty($response['screen_result']),
            'approve_status' => AmlApprove::Pending
        ];

        $amlUser = AmlUser::where('query_id', $response["query_id"])->where('user_id', $response["customer_reference_id"])->first();
        if (!$amlUser) {
            $amlUser = AmlUser::create($data);
        }

        return $amlUser;
    }

    public function getQueryByCustomer($data)
    {
        try {

            if (!isset($data['customer_reference_id'])) {
                $code = $this->logServices->log('AML-SCREEN-QUERY', null, ['aml screen customer reference id not valid', $data]);
                $msg = 'aml screen  customer reference id not valid : ' . $code;
                return $this->handleResponse($msg, 400);
            }

            $response = $this->callAml('GET', 'screen/' . $data['customer_reference_id'] . '?with_risk_assesment_result=true', null, true);

            return $response;

        } catch (\Exception $e) {

            dump($e->getMessage(), $e->getFile(), $e->getLine());
            $code = $this->logServices->log('AML-SCREEN-QUERY-EXCEPTION', $e);
        }
    }

    public function testWebhook($data)
    {
        try {

            $response = $this->callAml('POST', 'webhook/test', json_encode($data), true);

            $response = json_decode($response, true);

            $this->createAmlHistory($response);

            return $this->handleResponse(__('mobile_app/message.created_successfully'), 201, $response);

        } catch (\Exception $e) {
            $code = $this->logServices->log('AML-TEST-WEBHOOK-EXCEPTION', $e);
            return $this->handleResponse('func type not found : ' . $code, 400);
        }
    }

    public function createAmlHistory($response)
    {
        $data = [
            'query_id' => $response["query_id"],
            'customer_reference_id' => $response["customer_reference_id"],
            'decision' => isset($response["decision"]) ? $response["decision"] : null,
            'meta' => json_encode($response),
        ];

        $amlUser = AmlFocalHistory::create($data);

        $user = User::find($response["customer_reference_id"]);

        if (!$user || !$user->investor) {
            $this->logServices->log('AML-HISTORY-WEBHOOK', null, ['user or profile not valid', $data]);
            return false;
        }

        $profile = $user->investor;

        $statusValues = [
            'approved' => InvestorStatus::Approved,
            'rejected' => InvestorStatus::AutoAmlCheckFailed,
        ];

        if (isset($statusValues[strtolower($data['decision'])])) {

            $profile->status = $statusValues[strtolower($data['decision'])];
            $profile->save();
        }

        return $amlUser;
    }

    public function callAml($method, $url, $data, $is_auth)
    {
        $ch = curl_init($this->base_url . $url);

        $headers = [
            'Content-Type: application/json',
        ];

        if ($is_auth) {
            array_push($headers, 'authorization: ' . $this->accessToken());
        }

        if ($data != null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            array_push($headers, 'Content-Length: ' . strlen($data));
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return $server_output;
    }

    public function handleResponse($msg, $status, $data = null)
    {
        return [
            'message' => $msg,
            'response_status' => $status,
            'data' => $data
        ];
    }

    public function approveAmlScreens($data)
    {
        try {

            $body = [
                'customer_reference_id' => isset($data['customer_reference_id']) ? $data['customer_reference_id'] : null,
//                'query_uuid' => isset($data['query_id']) ? $data['query_id'] : null,
                'comment' => 'approve this customer_reference_id',
            ];

            $body = json_encode($body);

            $response = $this->callAml('POST', 'screen/approve', $body, true);

            return $response;

        } catch (\Exception $e) {

            dump($e->getMessage(), $e->getFile(), $e->getLine());
            $code = $this->logServices->log('AML-APPROVE-SERVICE-EXCEPTION', $e);
        }
    }

    public function approveAml ($amlUser) {

        $data = [
            'type' => 'approveAml',
        ];

        $amlUser = (array) $amlUser;

        $data = array_merge($data, $amlUser);

        $response = $this->handle($data);

        $updatedData = [
            'approve_response' => json_encode($response),
            'approve_status' => $response == '"Success"' ?  1 : 0,
        ];

        DB::table('aml_users')
            ->where('customer_reference_id', $amlUser['customer_reference_id'])
            ->update($updatedData);

        return true;
    }

}
