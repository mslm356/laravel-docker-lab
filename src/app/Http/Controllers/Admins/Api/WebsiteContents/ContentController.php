<?php

namespace App\Http\Controllers\Admins\Api\WebsiteContents;

use App\Http\Controllers\Controller;
use App\Models\WebsiteContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    /**
     * Get the content of the required page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(WebsiteContent $websiteContent)
    {
        return $websiteContent;
    }

    /**
     * Update the given content
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, WebsiteContent $websiteContent)
    {
        return DB::transaction(function () use ($request, $websiteContent) {
            $data = $this->validator($request->all())->validate();

            $websiteContent->update([
                'content' => collect($data['content'])
            ]);

            return response()->json([
                'data' => $websiteContent
            ]);
        });
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, [
            'content.en' => ['required', 'string',textValidation(), "max:1000"],
            'content.ar' => ['required', 'string',textValidation(), "max:1000"],
        ]);
    }
}
