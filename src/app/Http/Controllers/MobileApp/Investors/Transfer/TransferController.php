<?php

namespace App\Http\Controllers\MobileApp\Investors\Transfer;

use App\Exceptions\FailedProcessException;
use App\Exceptions\NoBalanceException;
use App\Http\Controllers\Controller;
use App\Rules\Investors\MyActiveBankAccount;
use App\Rules\NumericString;
use App\Services\Investors\TransferServices;
use App\Services\LogService;
use App\Support\VirtualBanking\Helpers\LocalSaudiTransferFee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class TransferController extends Controller
{

    public $transferServices;
    public $logService;

    public function __construct()
    {
        $this->transferServices = new TransferServices();
        $this->logService = new LogService();
    }

    public function getTransferFeeOptions(Request $request)
    {
        try {

            $data = $this->validate($request, [
                'bank_account_id' => ['required', 'integer', new MyActiveBankAccount(getAuthUser('api'))],
                'amount' => ['required', new NumericString, 'gt:1'],
            ]);

            $feeOptions = $this->transferServices->feesOptions($data);
            return apiResponse(200, __('mobile_app/message.done_successfully'), ['feeOptions' => $feeOptions]);

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-GET-TRANSFER-FEE-OPTIONS', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('GET-TRANSFER-FEE-OPTIONS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function transfer(Request $request)
    {
        try {

            $user = getAuthUser('api');

            $data = Validator::make($request->all(), [
                'amount' => ['required', new NumericString, 'gt:1'],
                'bank_account_id' => ['required', 'integer', new MyActiveBankAccount($user)],
                'fee' => ['required', 'numeric', Rule::in([
                    LocalSaudiTransferFee::FEE_0,
                    LocalSaudiTransferFee::FEE_0_5,
                    LocalSaudiTransferFee::FEE_1,
                    LocalSaudiTransferFee::FEE_5,
                    LocalSaudiTransferFee::FEE_7,
                ])],
            ])->validate();

            $response = $this->transferServices->transferMoney($data, $user);

            $responseData = [
                'message' => trans('messages.transfer'),
                'show_request_msg' => isset($response['status']) && !$response['status'] ? true : false,
                'request_msg' => __('The transfer process may take between 3-5 business days'),
                'request_alert' => isset($response['msg']) ? __($response['msg']) : '',
            ];

            return apiResponse(200, trans('messages.transfer'), $responseData);

        } catch (NoBalanceException $e) {
            $code = $this->logService->log('NoBalanceException-TRANSFER-MOBILE', $e);
            return apiResponse(400, __('messages.no_enough_balance') . ' : ' . $code);
        } catch (FailedProcessException $e) {
            $code = $this->logService->log('FailedProcessException-TRANSFER-MOBILE', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-TRANSFER-MOBILE', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('TRANSFER-MOBILE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }
}
