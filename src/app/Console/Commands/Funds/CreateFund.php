<?php

namespace App\Console\Commands\Funds;

use App\Enums\Role;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CreateFund extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'funds:create {--listingId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send mails when create new fund to investors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listing = Listing::findOrFail($this->option('listingId'));

        User::role(Role::Investor)
            ->lazyById()
            ->each(function ($user) use ($listing) {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new \App\Mail\Funds\CreateFund($user, $listing));
                }
            });

        return true;
    }
}
