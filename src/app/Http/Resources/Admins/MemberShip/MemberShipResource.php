<?php

namespace App\Http\Resources\Admins\MemberShip;

use App\Enums\MemberShips\MemberShipStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class MemberShipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'type' => $this->member_ship,
            'status' => $this->getStatus($this->status),
            'initiator_type' => $this->initiator_type,
            'created_by' => $this->createdBy ? $this->createdBy->full_name : '---',
            'comment' => $this->comment,
            'expire_at' => $this->expire_at,
            'created_at' => $this->created_at->format('Y-m-d h:i'),
            'updated_at' => $this->updated_at->format('Y-m-d h:i') ,
        ];
    }


    public function getStatus ($value) {

        $status = [
            'value'=> 1,
            'description'=> __('Pending'),
        ];

        if ($value == MemberShipStatus::Rejected) {
            $status['value'] = $value;
            $status['description'] = __('Rejected');
        }

        elseif ($value == MemberShipStatus::Approved)  {
            $status['value'] = $value;
            $status['description'] = __('Approved');
        }

        elseif ($value == MemberShipStatus::Expired)  {
            $status['value'] = $value;
            $status['description'] = __('Expired');
        }

        else {
            return $status;
        }

        return $status;
    }
}
