<?php

namespace App\Console\Commands\Reports\Investors;

use App\Exports\Investors\DepositReportExport;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class InvestorDepositTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:investor-deposit-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get investors with amount of deposit transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Excel::store(
            new DepositReportExport,
            "mgmnt-reports/investor-deposit-reports-" . now('Asia/Riyadh')->toDateTimeString() . ".xlsx",
            config('filesystems.private')
        );
    }
}
