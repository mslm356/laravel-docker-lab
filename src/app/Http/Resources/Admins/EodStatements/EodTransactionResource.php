<?php

namespace App\Http\Resources\Admins\EodStatements;

use Illuminate\Http\Resources\Json\JsonResource;

class EodTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'mark' => $this->mark,
            'amount' => $this->amount->format(),
            'code' => $this->code,
            'customer_ref' => $this->customer_ref,
            'bank_ref' => $this->bank_ref,
            'value_date' => $this->value_date->timezone('Asia/Riyadh')->toDateString(),
            'description' => $this->description,
            'is_transferred' => $this->is_transferred,
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
