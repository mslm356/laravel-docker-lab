<?php

namespace App\Exports\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class OlderThan18 implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct()
    {
//        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        return DB::table('users')// User::role(Role::Investor)
            ->join('national_identities', function ($join) {
                $join->on('users.id', '=', 'national_identities.user_id')
                    ->where(function ($q) {

                        $q->where(function ($m) {
                            $now = Carbon::now()->subYears(18);
                            $m->where('national_identities.birth_date_type', 'gregorian')
                                ->where('national_identities.birth_date', '>', $now);
                        });

                        $q->orWhere(function ($h) {
                            $nowHijry = '1425/12/19';
                            $h->where('national_identities.birth_date_type', 'hijri')
                                ->where('national_identities.birth_date', '>', $nowHijry);
                        });
                    });
            })

            ->leftJoin('transactions', function ($join) {
                $join->on('users.id', '=', 'transactions.payable_id')
                    ->where('transactions.payable_type', (new User)->getMorphClass())
                    ->where('transactions.reason', 2);
            })

            ->join('wallets', function ($join) {
                $join->on('users.id', '=', 'wallets.holder_id')
                    ->where('wallets.holder_type', 'App\Models\Users\User');
            })

            ->groupBy('users.id')

            ->select('users.id', 'users.first_name', 'users.last_name', 'users.email', 'users.phone_number',
                'national_identities.birth_date','wallets.balance',
                DB::raw('Count(transactions.id) as transaction_count'),
            )
            ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'First Name',
            'Last Name',
            'Email',
            'Phone',
            'birth_date',
            'transaction_count',
            'amount',
        ];
    }

    public function map($user): array
    {
        return [
            $user->id,
            $user->first_name,
            $user->last_name,
            $user->email,
            $user->phone_number,
            $user->birth_date,
            $user->transaction_count,
            $user->balance,
        ];
    }
}
