<?php

namespace App\Http\Requests\AuthorizedEntities;

use App\Http\Requests\AuthorizedEntities\Users\InviteUserToEntityRequest;
use Illuminate\Foundation\Http\FormRequest;

class RegisterEntityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            (new CreateEntityRequest())->rules(),
            (new InviteUserToEntityRequest())->rules(),
            [
                'password' => ['required', 'string', 'max:100']
            ]
        );
    }
}
