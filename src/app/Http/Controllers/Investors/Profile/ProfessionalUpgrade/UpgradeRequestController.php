<?php

namespace App\Http\Controllers\Investors\Profile\ProfessionalUpgrade;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\ProfessionalUpgrade\UpgradeRequestListItemResource;
use App\Http\Resources\Investors\ProfessionalUpgrade\UpgradeRequestResource;
use App\Models\Investors\InvestorUpgradeRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class UpgradeRequestController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user('sanctum');
        return UpgradeRequestListItemResource::collection(
            InvestorUpgradeRequest::where('user_id', $user->id)->where('type',$user->type)->get()
        );
    }

    public function show(Request $request, $upgradeRequest)
    {
        $user = $request->user('sanctum');
        $upgradeRequest = InvestorUpgradeRequest::where('type',$user->type)->where('id',$upgradeRequest)->first();
        if(!$upgradeRequest)
            return response()->json(['data' => ['message' => trans("messages.data_not_fount")]], 404);

        if ($upgradeRequest->user_id !== $user->id) {
            throw new AuthorizationException;
        }

        return new UpgradeRequestResource($upgradeRequest);
    }
}
