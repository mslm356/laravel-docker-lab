<?php

namespace App\Support\Elm\AbsherOtp\SmsStrategies;

use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use App\Support\Elm\AbsherOtp\ErrorCodes;
use App\Models\Elm\AbsherOtp as ElmAbsherOtp;
use App\Support\Elm\AbsherOtp\AuthBodyParams;
use App\Support\Elm\AbsherOtp\AbsherOtpSoapClient;
use App\Support\Elm\AbsherOtp\Exceptions\CustomerException;
use App\Support\Elm\AbsherOtp\Exceptions\SendingSmsException;
use App\Support\Elm\AbsherOtp\Exceptions\ConfigurationException;
use App\Support\Elm\AbsherOtp\Exceptions\ExternalSystemException;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\Exceptions\InternalConnectionException;
use SoapFault;

class AbsherOtp
{
    const EXPIRED = 1000;
    const INVALID_MSG_TYPE = 1001;
    const INVALID_MSG = 1002;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    /**
     * Sign the given message
     *
     * @param string $nationalId
     * @param \App\Models\Users\User $user
     * @return \App\Models\Elm\AbsherOtp
     * @throws \App\Support\Elm\AbsherOtp\Exceptions\SendingSmsException
     */
    public function send($nationalId, User $user, $meta = [])
    {
        $client = AbsherOtpSoapClient::instantiate();

        try {

            $code = generateRandomCode(6);
            $response = $client->__soapCall(
                'sendOTPWithDynamicTemplate',
                [
                    $requestBody = array_merge(
                        AuthBodyParams::get(),
                        [
                            'operatorId' => $nationalId,
                            'customerId' => $nationalId,
                            'language' => $user->locale ?? App::getLocale(),
                            'reason' => $meta['reason'],
                            'otpCode' => $code,
                            'otpType' => 'PROVIDED_4TO6_DIGITS',
                            'otpTemplate' => [
                                'otpTemplateId' => config('services.absher_otp.template.id'),
                                'otpParams' => [
                                    [
                                        'Name' => 'Param1',
                                        'Value' => $meta['reason'],
                                    ],
                                    [
                                        'Name' => 'Param2',
                                        'Value' => __('app.name'),
                                    ],
                                ],
                            ]
                        ]
                    )
                ]
            );

            $this->logService->log('ABSHER-SEND-OTP', null, [$response, $requestBody]);

            if ((int)$response->status != 0) {
                throw new SendingSmsException();
            }

            $msg = ElmAbsherOtp::create([
                'id' => (string)Str::uuid(),
                'user_id' => $user->id,
                'customer_id' => $nationalId,
                'transaction_id' => $response->transactionId,
                'verification_code' => Hash::make($response->verificationCode),
                'code' => substr($response->verificationCode, 0, 4),
                'data' => array_merge(
                    [
                        'response' => [
                            'status' => $response->status,
                            'client_id' => $response->clientId,
                            'customer_id' => $response->customerId,
                        ],
                    ],
                    Arr::except($meta, 'reason')
                )
            ]);

            return $msg;
        } catch (SoapFault $th) {
            if ($fault = $th->detail->SendOTPFault ?? null) {

                $this->logService->log('ABSHER-FAILED-SOAP-true', null, [$fault, $th, $th->detail]);

                $exceptionArgs = [$requestBody, $fault->ErrorCode, $fault->ErrorType, $fault->ErrorMessage, $th->faultstring, $th->faultactor];
                if (in_array($fault->ErrorCode, ErrorCodes::configurationCodes)) {
                    throw new ConfigurationException(...$exceptionArgs);
                } elseif (in_array($fault->ErrorCode, ErrorCodes::customerCodes)) {
                    throw new CustomerException(...$exceptionArgs);
                } else {
                    throw new ExternalSystemException(...$exceptionArgs);
                }
            } else {
                $this->logService->log('ABSHER-FAILED-SOAP', null, [$th]);
                throw new InternalConnectionException($th->getMessage());
            }
        } catch (\Exception $e) {
            $this->logService->log('ABSHER-FAILED', $e);
        }
    }

    public function verify($nationalId, ElmAbsherOtp $msg, $enteredCode)
    {
        if (($msg->expired_at ?? null) !== null) {
            throw new VerificationSmsException($msg, static::EXPIRED);
        }

        if ($msg->customer_id !== $nationalId) {
            throw new VerificationSmsException($msg, static::INVALID_MSG);
        }

        if (false === Hash::check($enteredCode, $msg->verification_code)) {
            return false;
        }

        $msg->delete();

        //        $msg->update([
        //            'expired_at' => now()
        //        ]);

        return true;
    }

    public function sendToAbsher(User $user, $meta = [], $delay=null, $nin=null, $code = null)
    {
        $client = AbsherOtpSoapClient::instantiate();

        $nationalId = $nin ?? $user->nationalIdentity->nin;

        $response = $client->__soapCall(
            'sendOTPWithDynamicTemplate',
            [
                $requestBody = array_merge(
                    AuthBodyParams::get(),
                    [
                        'operatorId' => $nationalId,
                        'customerId' => $nationalId,
                        'language' => $user->locale ?? App::getLocale(),
                        'reason' => $meta['reason'],
                        'otpCode' => $code,
                        'otpType' => 'PROVIDED_4TO6_DIGITS',
                        'otpTemplate' => [
                            'otpTemplateId' => config('services.absher_otp.template.id'),
                            'otpParams' => [
                                [
                                    'Name' => 'Param1',
                                    'Value' => $meta['reason'],
                                ],
                                [
                                    'Name' => 'Param2',
                                    'Value' => __('app.name'),
                                ],
                            ],
                        ]
                    ]
                )
            ]
        );

        $this->logService->log('ABSHER-SEND-OTP', null, [$response, $requestBody]);

        if ((int)$response->status != 0) {
            throw new SendingSmsException();
        }

        $data = [
            'request' => $requestBody,
            'response' => $response,
        ];

        return $data;
    }
}
