<?php

use App\Jobs\Anb\AnbPullStatement;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('{any}', config('app.home_url'))->where('any', '^(?!api).*');

//Route::get('/report/export', [\App\Http\Controllers\Admins\Api\Reports\ReportsController::class, 'reportExport']);

/*Route::get('local/temp/{path}', function (string $path){
    return \Illuminate\Support\Facades\Storage::disk('local')->download($path);
})->name('local.temp');*/
