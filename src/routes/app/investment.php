<?php

use App\Http\Controllers\MobileApp\Investors\Investments\InvestmentsController;
use \App\Http\Controllers\MobileApp\Investors\Investments\DashboardController;
use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\Investments\InvestInListingController;
use App\Http\Controllers\MobileApp\Investors\Sdad\ChargeWalletController;
use \App\Http\Controllers\MobileApp\Investors\GenerateFileUrlController;
use \App\Actions\Investors\{GetFileUrl,GetInvestorFileUrl};

Route::post('/shares-to-amount/{listingId}', [InvestmentsController::class, 'convertSharesToAmount']);

Route::middleware(['auth:api', 'jwt_check:api', 'role:' . Role::Investor])->group(function () {

    Route::post('investor/sdad/export/invoices', [ChargeWalletController::class, 'create_invoice']);
    Route::get('transactions', [InvestmentsController::class, 'getTransactions']);
    Route::get('my-units/{id}', [DashboardController::class, 'getMyUnits']);
    Route::get('my-investments', [InvestmentsController::class, 'getMyInvestment']);
    Route::get('listing-reports/{listing}', [DashboardController::class, 'ListingReport']);
    Route::get('/investing-records', [InvestmentsController::class, 'getMyInvestingRecords']);
    Route::get('/balance-inquiry', [InvestmentsController::class, 'balanceInquiry']);

    Route::post('/listings/{listing}/subscription-token', [InvestmentsController::class, 'createSubscriptionToken'])->middleware('user-can-access:investment:api');
    Route::post('/investment/send-otp', [InvestmentsController::class, 'sentOtp']);
    Route::post('/investment/verify-otp', [InvestmentsController::class, 'verifyOtp']);
    Route::post('/investment', InvestInListingController::class)->middleware('user-can-access:investment:api');
    Route::get('/files/{file}/get-url', GetFileUrl::class)->name('files.get.url');
    Route::get('/files/investor-record/{investor_record_id}/get-url', GetInvestorFileUrl::class)->name('investor-files.get.url');


});
