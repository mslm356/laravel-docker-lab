<?php

namespace App\Console\Commands\Funds;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Banking\TransactionReason;
use App\Enums\Notifications\NotificationTypes;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Services\MemberShip\CreateRequest;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AlertCancelListingInvesting extends Command
{
    use NotificationServices;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert-listing-investing:cancel {--listing_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cancel listing investing cmd';

    public $createRequest;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->createRequest = new CreateRequest();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        if (!$this->option('listing_id')) {
            dd('listing_id required');
        }

        DB::table('listing_investor')
            ->where('listing_id', $this->option('listing_id'))
            ->join('users', function ($join) {
                $join->on('users.id', '=', 'listing_investor.investor_id');
            })
            ->join('listings', function ($join) {
                $join->on('listings.id', '=', 'listing_investor.listing_id');
            })
            ->join('wallets', function ($join) {
                $join->on('wallets.holder_id', '=', 'listing_investor.investor_id')
                    ->where('wallets.holder_type', User::class);
            })
            ->select(
                [
                    'listing_investor.id as listing_investor_id', 'listing_investor.listing_id', 'listing_investor.investor_id',
                    'listing_investor.invested_shares', 'listing_investor.invested_amount', 'users.email', 'users.locale', 'users.member_ship',
                    DB::raw('concat(users.first_name, " ", users.last_name) as full_name'),
                    DB::raw('listings.target/listings.total_shares as share_price'),
                    'listings.title_en', 'listings.title_ar', 'wallets.id as wallet_id',
                    'listings.administrative_fees_percentage', 'listings.vat_percentage'
                ])
            ->orderBy('listing_investor.id')
            ->lazy(500)
            ->each(function ($listingInvesting, $index) {

//                $listingInvesting->share_price = money($listingInvesting->share_price, defaultCurrency());
//
//                $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listingInvesting, $listingInvesting->invested_shares);
//
//                $total = $toBeInvestedAmountAndFees['total']->getAmount();

//                Mail::to($listingInvesting->email)->queue(new \App\Mail\Funds\CancelListingInvesting($listingInvesting, $total, $time));
//
                $notificationData = $this->notificationData($listingInvesting);
                $this->send(NotificationTypes::CancelListingInvesting, $notificationData, $listingInvesting->investor_id);

                var_dump('id => ' . $listingInvesting->investor_id . '  index => ' . $index);

            });
    }

    public function notificationData($listingInvesting)
    {

        return [
            'action_id' => $listingInvesting->investor_id,
            'channel' => 'mobile',
            'title_en' => 'العضوية الذهبية',
            'title_ar' => 'العضوية الذهبية',
            'content_en' => 'الآن عضويتك تمكنك من الاستثمار المبكر في صندوق مكين الشرق اطلع على الفرصة الآن!',
            'content_ar' => 'الآن عضويتك تمكنك من الاستثمار المبكر في صندوق مكين الشرق اطلع على الفرصة الآن!',
        ];
    }
}
