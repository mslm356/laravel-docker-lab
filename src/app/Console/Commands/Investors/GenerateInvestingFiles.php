<?php

namespace App\Console\Commands\Investors;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Actions\Listings\GetCurrentInvestments;
use App\Enums\Role;
use App\Jobs\Zatca\GenerateEInvoiceAfterInvesting;
use App\Models\Elm\AbsherOtp;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\Zatca\ZatcaInvoice;
use App\Settings\ZatcaSettings;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Zatca\EInvoice\Order;
use App\Support\Zatca\EInvoice\PurchaseLine;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Validation\ValidationException;

class GenerateInvestingFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investing:files {--token_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'verify account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        try {

            return DB::transaction(function () {

                $token = InvestorSubscriptionToken::find($this->option('token_id'));

                if (!$token) {
                    dd('token not valid');
                }

                $user = $token->user;

                if (!$user) {
                    dd('user not valid');
                }

                $listing = $token->listing;

                if (!$listing) {
                    dd('listing not valid');
                }

                $investingRecord = $token->listingRecord;

                if (!$investingRecord) {
                    dd('investing Record not valid');
                }

                DB::table('files')->where('fileable_type', ListingInvestorRecord::class)
                    ->where('fileable_id', $investingRecord->id)->delete();


                $e_invoice = ZatcaInvoice::where('invoicable_type',  ListingInvestorRecord::class)->where('invoicable_id', $investingRecord->id)->first();

                if ($e_invoice && $e_invoice->pdfFile) {
                    $e_invoice->pdfFile->delete();
                }

                if ($e_invoice) {
                    $e_invoice->delete();
                }

                $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing, $token->shares);

                $currentInvest = GetCurrentInvestments::run($listing)->lockForUpdate()->get();

                $subscriptionFormData = GetSubscriptionFormData::run(
                    $token,
                    $user,
                    $listing,
                    $toBeInvestedAmountAndFees['amount'],
                    $currentInvest->where('investor_id', $user->id)
                );

                ExportSubFormAndAttachToInvestingRecord::dispatch(
                    $investingRecord,
                    $token,
                    $user,
                    $listing,
                    $subscriptionFormData
                );

                $feePercentage = $toBeInvestedAmountAndFees['feePercentage'];
                $vatPercentage = $toBeInvestedAmountAndFees['vatPercentage'];

//                GenerateEInvoiceAfterInvesting::dispatch(
//                    $user,
//                    new Order(
//                        transaction_ref(),
//                        [
//                            new PurchaseLine(
//                                [
//                                    'en' => "({$feePercentage}%) of investing in {$listing->title_en}",
//                                    'ar' => "(%{$feePercentage}) من مبلغ الاستثمار في {$listing->title_ar}",
//                                ],
//                                1,
//                                $toBeInvestedAmountAndFees['fee'],
//                                $vatPercentage,
//                                null
//                            )
//                        ],
//                        now('Asia/Riyadh'),
//                        $investingRecord
//                    ),
//                    app(ZatcaSettings::class)->seller,
//                    'zatca-e-invoices',
//                    null
//                );

            });

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }

    }


}
