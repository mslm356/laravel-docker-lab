<div class="relative bg-white" x-data="{isOpen: {solutions: false, more: false}, isMenuOpen: false}">
    <div class="px-4 mx-auto max-w-7xl sm:px-6">
        <div class="flex items-center justify-between py-6 border-b-2 border-gray-100 lg:justify-start md:space-x-10">
            <div class="rtl:ml-4 ltr:mr-4">
                <a href="{{ route('home_page') }}" class="flex">
                    <img class="w-auto h-12 sm:h-12" src="{{ asset('images/logo_symbol.png') }}" alt="Workflow">
                </a>
            </div>
            <nav class="hidden space-x-10 rtl:space-x-reverse lg:flex lg:flex-1">
                @foreach ($websiteNavLinks as $link)
                    @if (in_array($link['id'], ['about_us', 'funds', 'how_it_works']))
                        <a href="{{ $link['link'] }}"
                            class="text-base font-medium leading-6 text-gray-500 transition duration-150 ease-in-out hover:text-gray-900 focus:outline-none focus:text-gray-900">
                            {{ $link['text'] }}
                        </a>
                    @endif
                @endforeach
            </nav>
            <div class="-my-2 -mr-2 lg:hidden">
                <button @click="isMenuOpen = true" type="button"
                    class="inline-flex items-center justify-center p-2 text-gray-400 transition duration-150 ease-in-out rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500">
                    <svg class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                </button>
            </div>
            <nav class="hidden space-x-10 rtl:space-x-reverse lg:flex">
                @foreach ($websiteNavLinks as $link)
                    @if (in_array($link['id'], ['login', 'signup', 'locale', 'dashboard']))
                        <a href="{{ $link['link'] }}"
                            class="text-base font-medium leading-6 text-gray-500 transition duration-150 ease-in-out hover:text-gray-900 focus:outline-none focus:text-gray-900">
                            {{ $link['text'] }}
                        </a>
                    @endif
                @endforeach
            </nav>
        </div>
    </div>

    <div x-show="isMenuOpen" x-transition:enter="duration-200 ease-out" x-transition:enter-start="opacity-0 scale-90"
        x-transition:enter-end="opacity-100 scale-100" x-transition:leave="duration-100 ease-in"
        x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-90"
        class="absolute inset-x-0 top-0 z-50 p-2 transition origin-top-right transform lg:hidden">
        <div class="rounded-lg shadow-lg">
            <div class="bg-white divide-y-2 rounded-lg ring-1 ring-black ring-opacity-5 divide-gray-50">
                <div class="px-5 pt-5 pb-6 space-y-6">
                    <div class="flex items-center justify-between">
                        <div>
                            <img class="w-auto h-8" src="{{ asset('images/logo_symbol.png') }}" alt="Workflow">
                        </div>
                        <div class="-mr-2">
                            <button @click="isMenuOpen = false" type="button"
                                class="inline-flex items-center justify-center p-2 text-gray-400 transition duration-150 ease-in-out rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500">
                                <svg class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div>
                        @foreach ($websiteNavLinks as $navLink)
                            <a href="{{ $navLink['link'] }}"
                                class="{{ $loop->first ? '' : 'mt-1 ' }}block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out">{{ $navLink['text'] }}</a>
                        @endforeach
                        @auth
                            <form method="POST" action="{{ route('api.auth.logout') }}">
                                @csrf
                                <button type="submit"
                                    class="block px-3 py-2 text-base font-medium text-gray-700 transition duration-150 ease-in-out rounded-md hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50">{{ __('website.navlinks.logout') }}</a>
                            </form>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
