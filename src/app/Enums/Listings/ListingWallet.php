<?php

namespace App\Enums\Listings;

use BenSampo\Enum\Enum;

final class ListingWallet extends Enum
{
    const Escrow = 'ESCROW';
    const EscrowVat = 'ESCROW_VAT';
    const EscrowAdminFee = 'ESCROW_ADMIN_FEE';
    const Spv = 'SPV';
}
