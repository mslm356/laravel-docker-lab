<?php

namespace App\Services\Campaigns\InvestorTypes;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ManageData
{

    public $variables;

    public function __construct ()
    {
        $this->variables = ['first_name', 'last_name', 'email', 'phone_number', 'nin'];
    }

    public function manageData ($requestData, $selectedColumns)
    {

        $className = "App\\Services\\Campaigns\\InvestorTypes\\" . ucfirst($requestData['investors_type']) . "Type";

        if (!class_exists($className)) {
            throw ValidationException::withMessages(['message'=>'class not valid ' . $requestData['investors_type'] ]);
        }

        $generator = new $className();

        $investor_ids = isset($requestData['investor_ids']) ? $requestData['investor_ids'] : '';

        $customData = ['custom_ids' => $investor_ids];

        $extraSelectedColumn = $this->getVarsFromText($requestData['description_en'], $requestData['description_ar']);

        $selectedColumns = array_unique(array_merge($extraSelectedColumn, $selectedColumns));

        $data = [
            'investorData' => $generator->getUsersData($selectedColumns, $customData),
            'extraSelectedColumn' => $extraSelectedColumn,
        ];

        return $data;
    }

    public function getVarsFromText($text_en, $text_ar)
    {

        $extraSelectedColumn = [];

        foreach ($this->variables as $variable) {

            if (strpos($text_en, '{{'.$variable.'}}') and !in_array($variable, $extraSelectedColumn)) {
                $extraSelectedColumn[] = $variable;
            }
        }

        foreach ($this->variables as $variable) {

            if (strpos($text_ar, '{{'.$variable.'}}') and !in_array($variable, $extraSelectedColumn)) {
                $extraSelectedColumn[] = $variable;
            }
        }

        return $extraSelectedColumn;
    }

    public function getCampaignUsers ($campaignId)
    {
        $data =  DB::table('campaign_users')
            ->where('campaign_id', $campaignId)
            ->select('user_data')
            ->get();

        $data = array_map(function ($arra) {
            return json_decode($arra['user_data'],1);
        }, json_decode($data, 1));

        return $data;
    }

    public function dataUsedToSend ($campaign)
    {

        $data['user_data'] = $this->getCampaignUsers($campaign->id);
        $data['varsInText'] = $this->getVarsFromText($campaign->description_en, $campaign->description_ar);

        return $data;
    }

}
