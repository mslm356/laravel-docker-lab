<?php

namespace App\Models\Enquiries;

use Illuminate\Database\Eloquent\Model;

class EnquiryToken extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function enquiry()
    {
        return $this->belongsTo(Enquiry::class, 'enquiry_id', 'id');
    }

    static public function findToken($token)
    {
        [$id, $plainText] = explode('-', $token, 2);

        if ($instance = static::find($id)) {
            return hash_equals($instance->token, static::hash($plainText)) ? $instance : null;
        }

        return null;
    }

    static public function hash($text)
    {
        return hash('sha256', $text);
    }
}
