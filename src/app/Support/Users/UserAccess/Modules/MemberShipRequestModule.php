<?php

namespace App\Support\Users\UserAccess\Modules;

class MemberShipRequestModule
{

    public function handle($user, $module, $userDefaultData)
    {
        return $this->updateLastMembershipRequest($user);
    }

    public function updateLastMembershipRequest($user)
    {
        $lastMemberShip = $user->memberShips()->orderBy('id', 'desc')->first();

        if ($user->member_ship == 0 || !$lastMemberShip)
        {
            return ['last_membership_request' => null];
        }

        $data = [
            'id' => $lastMemberShip->id,
            'current_member_ship' => $user->member_ship,
            'member_ship' => $lastMemberShip->member_ship,
            'status' => $lastMemberShip->status,
            'type' => $lastMemberShip->status,
            'show_popup' => $lastMemberShip->show_popup
        ];

        return ['last_membership_request' => json_encode($data)];
    }

}
