<?php

namespace App\Console\Commands\Payout;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Notifications\NotificationTypes;
use App\Mail\Investors\NewTransaction;
use App\Models\Listings\Listing;
use App\Models\Payouts\Payout;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Services\LogService;
use App\Traits\NotificationServices;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SpecialPayout extends Command
{

    use NotificationServices;

    public $logService;
    public $count = 0;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payout:special {--listing_id=} {--share_price=} {--payout_type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'special payout';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            $nins = [

                1103777197,
                1080011073,
                1075608610,
                1098948282,
                1071087066,
                1036720611,
                1045636501,
                1030662116,
                1084963196,
                1106497314,
                1089445785,
                1095518526,
                1004217293,
                1079340384,
                1079467708,
                1079241327,
                1038491880,
                1087101778,
                1077897971,
                1059542736,
                1003253679,
                1071794836,
                1079267314,
                1011726864,
                1086132766,
                1051104170,
                1016315697,
                1007584608,
                1077370151,
                1069475380,
                1015683780,
                1098316555,
                1027132974,
                1117721462,
                1091953529,
                2037804982,
                1042208023,
                1050244894,
                1068166972,
                1069255337,
                1048522336,
                1011331483,
                1090395375,
                1015516816,
                1027784303,
                2181458536,
                1098585969,
                1005014970,
                1056249822,
                1112961576,
                1006670655,
                1041949718,
                1059008522,
                1068726197,
                1039072689,
                1060409503,
                1071779571,
                1021726409,
                1059323509,
                1055336943,
                1006993594
            ];

            $investorsIds = DB::table('national_identities')
                ->whereIn('nin', $nins)
                ->select('user_id')
                ->pluck('user_id')
                ->toArray();

            if (!$this->option('share_price') || !$this->option('payout_type')) {
                dd('options is required');
            }

            $listing = Listing::find($this->option('listing_id'));

            if (!$listing) {
                dd('listing not found');
            }

            $investedShares = DB::table('listing_investor')
                ->where('listing_id', $listing->id)
                ->whereIn('investor_id', $investorsIds)
                ->sum('invested_shares');

            $total = $this->option('share_price') * $investedShares;

            $wallet = $listing->getWallet(WalletType::ListingSpv);

            $amount = money_parse_by_decimal($total, defaultCurrency());

            if ($amount->greaterThan(money($wallet->balance))) {
                dd(trans('messages.no_enough_balance'));
            }

            $totalInstance = (money_parse_by_decimal($total, $defaultCurrency = defaultCurrency()));


            /* to roll back every action when single action not complete we have to use ===>

             DB::transaction(function () use ($listing, $totalInstance, $investorsIds , $investedShares) {*/


            $payout = $this->createPayout($listing, $totalInstance, $this->option('payout_type'));

//            $payout = Payout::find(5);

            $this->payForInvestors($payout, $investorsIds, $investedShares, $totalInstance);

            $this->updateWalletBalance($payout);

            dump('number of investors with success payout ' . $this->count);


            //  });


        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }

    public function createPayout($listing, $totalInstance, $payoutType)
    {

        $payout = $listing->payouts()->create([
            'total' => $totalInstance->getAmount(),
            'currency' => defaultCurrency(),
            'payout_date' => Carbon::today(),
            'type' => $payoutType ? $payoutType : 'payout',
        ]);

        return $payout;
    }

    public function payForInvestors($payout, $investorsIds, $investedShares, $totalInstance)
    {

        $listing = $payout->listing;

        if ($investedShares == 0) {
            dd('invested shares equal zero');
        }

        $payoutPerShare = money_parse_by_decimal($this->option('share_price'), $defaultCurrency = defaultCurrency()); // $totalInstance->divide($investedShares);

        foreach ($investorsIds as $index => $investorsId) {

            $investor = User::find($investorsId);

            if (!$investor) {
                dump('investor not valid ' . $investorsId);
                continue;
            }

            $investorShares = DB::table('listing_investor')
                ->where('listing_id', $listing->id)
                ->where('investor_id', $investorsId)
                ->sum('invested_shares');


            if ($investorShares <= 0) {
                dump('investor shares equal zero ' . $investorsId);
                continue;
            }

            $paid = ($payoutPerShare->multiply($investorShares))->getAmount();

            $investorPayouts[$investorsId] = [
                'paid' => $paid,
                'expected' => $paid,
                'currency' => defaultCurrency(),
                'shares' => $investorShares
            ];

            $this->saveTransactions($payout, $paid, $investor);

            DB::table('listing_investor')
                ->where('listing_id', $listing->id)
                ->where('investor_id', $investorsId)
                ->update(['paid_payout' => $paid]);

            $this->count++;
        }

        $payout->investors()->sync($investorPayouts);
    }

    public function saveTransactions($payout, $paid, $investor)
    {
        if ($paid == 0) {
            dump('paid is zero for investor => ' . $investor->id);
        }

        $wallet = $payout->listing->getWallet(WalletType::ListingSpv);
        $investorWallet = $investor->getWallet(WalletType::Investor);

        if (!$wallet || !$investorWallet) {
            return false;
        }

        $meta = [
            'reason' => TransactionReason::PayoutDistribution,
            'reference' => transaction_ref(),
            'value_date' => now('UTC')->toDateTimeString(),
            'listing_id' => $payout->listing_id,
            'listing_title_en' => $payout->listing->title_en,
            'listing_title_ar' => $payout->listing->title_ar,
            'payout_id' => $payout->id,
            'payout_type' => $payout->type,
            'user_id' => $investor->id,
            'source' => 'special_payout',
            'user_name' => $investor->full_name
        ];

        DB::table('transactions')->insert([
            'payable_type' => Listing::class,
            'payable_id' => $payout->listing_id,
            'wallet_id' => $wallet->id,
            'type' => 'withdraw',
            'amount' => ($paid * -1),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('transactions')->insert([
            'payable_type' => User::class,
            'payable_id' => $investor->id,
            'wallet_id' => $investorWallet->id,
            'type' => 'deposit',
            'amount' => ($paid),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        $investorBalance = DB::table('transactions')
            ->where('payable_id', $investor->id)
            ->where('payable_type', User::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('holder_id', $investor->id)
            ->where('holder_type', User::class)
            ->update(['balance' => $investorBalance, 'is_overview_statistics_consistent' => false]);


        /*
         this query is better than up two queries

          DB::table('wallets')
            ->where('holder_id', $investor->id)
            ->where('holder_type', User::class)
            ->update([
                'balance' => DB::raw("balance + $paid")
                , 'is_overview_statistics_consistent' => false
            ]);
        */


        return true;
    }

    public function updateWalletBalance($payout)
    {

        $wallet = $payout->listing->getWallet(WalletType::ListingSpv);

        $walletBalance = DB::table('transactions')
            ->where('payable_id', $payout->listing_id)
            ->where('wallet_id', $wallet->id)
            ->where('payable_type', Listing::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('id', $wallet->id)
            ->update(['balance' => $walletBalance, 'is_overview_statistics_consistent' => false]);


        /*
         this query is better than up two queries if we path the amount in function and path it instead of paid value

         DB::table('wallets')
            ->where('id', $wallet->id)
            ->update(['balance' => DB::raw("balance - $paid"), 'is_overview_statistics_consistent' => false]);

        */

    }
}
