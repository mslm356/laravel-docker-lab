<?php

namespace App\Http\Controllers\Admins\Api\Dashboard;

use App\Enums\AML\AmlStatus;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Models\AML\AmlListCheck;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingValuation;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;

class GetOverview extends Controller
{
    /**
     * Get the paginated enquiries
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        $registrationsCount = User::role([Role::Investor], 'web')->count();

        $investorsCompletedKycCount = User::role([Role::Investor], 'web')
            ->whereHas('investor', function ($query) {
                $query->where('is_kyc_completed', true);
            })
            ->count();

        $investorsWhoInvested = User::role([Role::Investor], 'web')
            ->has('investments')
            ->count();

        $enabledInvestors = User::role([Role::Investor], 'web')
            ->whereHas('investor', function ($query) {
                $query->where('status', InvestorStatus::Approved);
            })
            ->count();

        $disabledInvestors = User::role([Role::Investor], 'web')
            ->whereHas('investor', function ($query) {
                $query->whereIn('status', [
                    InvestorStatus::PendingReview,
                    InvestorStatus::InReview,
                    InvestorStatus::Rejected,
                    InvestorStatus::Suspended
                ]);
            })
            ->count();

        $amlFlags = AmlListCheck::where('status', AmlStatus::Found)->count();

        $listings = Listing::count();

        $totalInvested = money(
            DB::table('listing_investor')
                ->whereNotIn(
                    'investor_id',
                    User::role([Role::PayoutsCollector])->get()->pluck('id')
                )
                ->sum('invested_amount'),
            defaultCurrency()
        )->formatByDecimal();

        $totalAum = ListingValuation::select('listing_valuations.value')
            ->leftJoin('listing_valuations as valuations2', function ($join) {
                $join->on('listing_valuations.listing_id', '=', 'valuations2.listing_id')
                    ->on('listing_valuations.date', '<', 'valuations2.date');
            })
            ->whereNull('valuations2.id')
            ->sum('listing_valuations.value');

        $totalAmount = DB::table("wallets")->where('balance','>' ,0)
            ->where('holder_type',"App\Models\Users\User")
            ->whereNotIn('holder_id',[1,2])
            ->sum("balance");

        return response()->json([
            'data' => [
                'registrations_count' => $registrationsCount,
                'prospective_investors_count' => $investorsCompletedKycCount,
                'enabled_investors_count' => $enabledInvestors,
                'disabled_investors_count' => $disabledInvestors,
                'aml_flags_count' => $amlFlags,
                'listings_count' => $listings,
                'total_amount' => clean_decimal(number_format(($totalAmount / 100) ,2) ),
                'total_invested' => cleanDecimal(number_format($totalInvested, 2)),
                'total_aum' => cleanDecimal(number_format($totalAum, 2)),
                'investors_who_invested_count' => $investorsWhoInvested
            ]
        ]);
    }
}
