@extends('layouts.website')

@section('content')
    <div class="px-4 py-10 mx-auto prose prose-primary max-w-7xl sm:py-16 sm:px-6">
        {!! preg_replace("/&nbsp;/", " ", $section->content[App::getLocale()]) !!}
    </div>
@endsection
