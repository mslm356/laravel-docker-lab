<?php

namespace App\Http\Controllers\Admins\Api\AuthorizedEntities;

use App\Services\Files\CheckFile;
use Illuminate\Http\Request;
use App\Models\AuthorizedEntity;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Enums\AuthorizedEntityStatus;
use App\Actions\AuthorizedEntities\CreateEntity;
use App\Actions\AuthorizedEntities\UpdateEntity;
use App\Enums\Role as EnumsRole;
use App\Http\Resources\Admins\AuthorizedEntities\EntityResource;
use App\Http\Requests\Admins\AuthorizedEntities\CreateEntityRequest;
use App\Http\Requests\Admins\AuthorizedEntities\UpdateEntityRequest;
use App\Http\Resources\Admins\AuthorizedEntities\EntitytListItemResource;
use Spatie\Permission\Models\Role;

class EntityController extends Controller
{

    public $checkFile;

    public function __construct()
    {
        $this->checkFile = new CheckFile();
    }


    /**
     * Show all the entities
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return EntitytListItemResource::collection(
            AuthorizedEntity::orderBy('id', 'desc')->paginate()
        );
    }


    public function show(AuthorizedEntity $authorizedEntity)
    {
        return new EntityResource(
            $authorizedEntity->load('files', 'reviewer', 'creator', 'creatorRole')
        );
    }

    public function store(CreateEntityRequest $request)
    {
        foreach ($request->cr ?? [] as $file) {
            $this->checkFile->handle($file, null);
        }

        return DB::transaction(function () use ($request) {
            CreateEntity::run(
                $request->validated() + [
                    'status' => AuthorizedEntityStatus::PendingReview,
                    'creator_id' => $request->user('sanctum')->id,
                    'creator_role_id' => Role::findByName(EnumsRole::Admin, 'web')->id,
                ]
            );

            return response()->json();
        });
    }

    /**
     * Update the authorized entity
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\AuthorizedEntity $authorizedEntity
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function update(UpdateEntityRequest $request, AuthorizedEntity $authorizedEntity)
    {
        DB::transaction(function () use ($authorizedEntity, $request) {
            UpdateEntity::run($authorizedEntity, $request->validated());
        });

        return response()->json();
    }

    /**
     * Update authorized entity
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\AuthorizedEntity $authorizedEntity
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, AuthorizedEntity $authorizedEntity)
    {
        $data = $this->validate($request, [
            'status' => ['required', 'integer', new EnumValue(AuthorizedEntityStatus::class, false)],
            'comment' => [
                'exclude_unless:status,' . AuthorizedEntityStatus::Rejected . ',' . AuthorizedEntityStatus::NeedMoreInformation,
                'nullable', 'string', 'max:1000'
            ]
        ]);

        return DB::transaction(function () use ($data, $authorizedEntity, $request) {
            $authorizedEntity->update([
                'status' => $data['status'],
                'reviewer_comment' => in_array(
                    $data['status'],
                    [AuthorizedEntityStatus::Rejected, AuthorizedEntityStatus::NeedMoreInformation]
                ) ?
                    $data['comment'] : null,
                'reviewer_id' => $request->user('sanctum')->id,
            ]);

            return response()->json();
        });
    }
}
