<?php

namespace App\Models\VirtualBanking;

use Bavix\Wallet\Models\Wallet as ModelsWallet;

class Wallet extends ModelsWallet
{
    /**
     * @var array
     */
    protected $fillable = [
        'holder_type',
        'holder_id',
        'name',
        'slug',
        'description',
        'meta',
        'balance',
        'decimal_places',
        'uuid'
    ];

    /**
     * Get virtual accounts of the wallet
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function virtualAccounts()
    {
        return $this->hasMany(VirtualAccount::class, 'wallet_id', 'id');
    }
}
