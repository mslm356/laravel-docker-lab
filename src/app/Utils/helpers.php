<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use Bavix\Wallet\Internal\MathInterface;
use Bavix\Wallet\Services\WalletService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Carbon;

/**
 * This is a custom helper file
 */
if (!function_exists('json_response')) {
    /**
     * This will be the helper function that i will use to prepare
     * the response object
     */
    function json_response($data, $statusCode = 200)
    {
        $data = ["data" => $data];
        return response()->json($data, $statusCode);
    }
}

if (!function_exists('error_response')) {
    /**
     * General error function
     * mixed $data
     * number $statusCode
     */
    function error_response($data, $statusCode = 400)
    {
        $errors = [];
        if (is_array($data)) {
            foreach ($data as $err) {
                if (is_array($err)) {
                    foreach ($err as $massage) {
                        array_push($errors, $massage);
                    }
                } else {
                    array_push($errors, $err);
                }
            }
        } else {
            $errors[] = $data;
        }
        $data = ["data" => ["errors" => $errors]];
        return response()->json($data, $statusCode);
    }
}


if (!function_exists('invalid_credentials')) {
    /**
     * General error function
     * mixed $data
     * number $statusCode
     */
    function invalid_credentials($data, $statusCode = 401)
    {
        return error_response($data, $statusCode);
    }
}

if (!function_exists('unauthorized')) {
    /**
     * General error function
     * mixed $data
     * number $statusCode
     */
    function unauthorized($data, $statusCode = 403)
    {
        return error_response($data, $statusCode);
    }
}


if (!function_exists('per_page')) {
    function per_page()
    {
        $request = request();
        if ($request->has('limit')) {
            return (int)$request->limit;
        }
        return config('custom.pagination.per_page');
    }
}

if (!function_exists('get_education_level')) {
    function get_education_level()
    {
        return [
            'Postgraduate-degree',
            'Undergraduate-degree',
            'High-school'
        ];
    }
}

if (!function_exists('get_occupation')) {
    function get_occupation()
    {
        return [
            'Retired',
            'Business-owner',
            'Employed',
            'Unemployed',
            'Student'
        ];
    }
}

if (!function_exists('get_current_invest')) {
    function get_current_invest()
    {
        return [
            'Real-estate',
            'Capital-markets',
            'Private-equity'
        ];
    }
}

if (!function_exists('investor_url')) {
    /**
     * Get a fully qualified url for the given paths to the investor portal
     *
     * @param array|string $paths
     * @return string
     */
    function investor_url($paths = null)
    {
        if (is_array($paths)) {
            $path = implode('/', $paths);
        } elseif (is_null($paths)) {
            $path = '';
        } else {
            $path = $paths;
        }

        return rtrim(config('app.investor_url'), '/') . '/' . $path . '?lang=' . App::getLocale();
    }
}


if (!function_exists('locale_suff')) {
    /**
     * Suffix the given attribute name with the current locale
     *
     * @param string $name
     * @return string
     */
    function locale_suff($name, $locale = null)
    {
        if ($locale === null) {
            $locale = App::getLocale();
        }

        return $name . '_' . $locale;
    }
}

if (!function_exists('clean_decimal')) {
    /**
     * Remove decimal points if needed
     *
     * @param float $lat
     * @param float $lng
     * @return float
     */
    function clean_decimal($number)
    {
        if (Str::endsWith($number, '.0')) {
            return str_replace('.0', '', $number);
        } elseif (Str::endsWith($number, '.00')) {
            return str_replace('.00', '', $number);
        } else {
            return $number;
        }
    }
}

if (!function_exists('unrealizedPL')) {
    /**
     * Remove decimal points if needed
     *
     * @param float $lat
     * @param float $lng
     * @return \Cknow\Money\Money
     */
    function unrealizedPL($units, $totalUnits, $initialValuation, $latestValuation)
    {
        $sharePercentage = $units / $totalUnits;

        return $latestValuation->multiply($sharePercentage)->subtract(
            $initialValuation->multiply($sharePercentage)
        );
    }
}

if (!function_exists('newUnrealizedPL')) {
    /**
     * Remove decimal points if needed
     *
     * @param float $lat
     * @param float $lng
     * @return \Cknow\Money\Money
     */
    function newUnrealizedPL($units, $defaultUnitPrice, $latestValuation)
    {
        return $latestValuation->subtract($defaultUnitPrice)->multiply($units);
    }
}

if (!function_exists('calculateValuation')) {
    function calculateValuation($valuationInvested, $investedAmount)
    {
        return ($valuationInvested - ($investedAmount / 100));
    }
}

if (!function_exists('cleanDecimal')) {
    /**
     * Remove decimal points if needed
     *
     * @param float $number
     * @return float
     */
    function cleanDecimal($number)
    {
        if (Str::endsWith($number, '.0')) {
            return str_replace('.0', '', $number);
        } elseif (Str::endsWith($number, '.00')) {
            return str_replace('.00', '', $number);
        } else {
            return $number;
        }
    }
}

if (!function_exists('authorizedEntity')) {
    /**
     * Get user's authorized entity
     *
     * @return \App\Models\AuthorizedEntity
     */
    function authorizedEntity()
    {
        $authorizedEntity = optional(request()->user('sanctum'))->authorizedEntity;

        if ($authorizedEntity === null) {
            throw new AuthorizationException;
        }

        return $authorizedEntity;
    }
}

if (!function_exists('authorizedEntityId')) {
    /**
     * Get user's authorized entity ID
     *
     * @return \App\Models\AuthorizedEntity
     */
    function authorizedEntityId()
    {
        $id = optional(request()->user('sanctum'))->authorized_entity_id;

        if ($id === null) {
            throw new AuthorizationException;
        }

        return $id;
    }
}

if (!function_exists('randomDigitsStr')) {
    /**
     * Generate random string of digits
     *
     * @return string
     */
    function randomDigitsStr($length = 4)
    {
        $numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

        $code = '';

        for ($i = 0; $i < $length; $i++) {
            $code .= Arr::random($numbers);
        }

        return $code;
    }
}

if (!function_exists('transaction_ref')) {
    /**
     * Generate transaction reference
     *
     * @return string
     */
    function transaction_ref()
    {
        return strtoupper(Str::random(16));
    }
}

if (!function_exists('moneyIntToFloat')) {
    /**
     * Generate transaction reference
     *
     * @return string
     */
    function moneyIntToFloat($amount, $wallet)
    {
        $math = app(MathInterface::class);

        $decimalPlacesValue = app(WalletService::class)
            ->getWallet($wallet)
            ->decimal_places;

        $decimalPlaces = $math->powTen($decimalPlacesValue);

        return $math->div($amount, $decimalPlaces);
    }
}

if (!function_exists('defaultCurrency')) {
    /**
     * Get the default currency
     *
     * @return string
     */
    function defaultCurrency()
    {
        return config('money.defaultCurrency');
    }
}

if (!function_exists('walletIdentifier')) {
    /**
     * Create wallet identifier
     *
     * @return string
     */
    function walletIdentifier()
    {
        return strtoupper(Str::random(16));
    }
}

if (!function_exists('ksaDateToUtcCarbon')) {
    /**
     * Create Carbon instance (UTC) from KSA date string (Y-m-d)
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    function ksaDateToUtcCarbon($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date, 'Asia/Riyadh')->setTime(0, 0)->timezone('UTC');
    }


    if (!function_exists('generateRandomCode')) {
        function generateRandomCode($digits)
        {
            return rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        }
    }

}

if (!function_exists('validationResponse')) {
    function validationResponse($status, $message)
    {
        $data = [
            'message' => $message,
            'errors' => [
                'message' => [$message]
            ]
        ];

        return response()->json($data, $status);
    }
}


if (!function_exists('apiResponse')) {
    function apiResponse($status, $msg, $data = 'no-data')
    {
        if ($data == 'no-data') {

            return response()->json(['status' => $status, 'message' => $msg], $status);

        } else {

            return
                response()->json(
                    [
                        'status' => $status,
                        'message' => $msg,
                        'data' => $data,

                    ],
                    $status
                );

        }
    }
};

if (!function_exists('handleValidationErrors')) {
    function handleValidationErrors($validationErrors)
    {
        $errors = [];

        foreach ($validationErrors as $index => $error) {

            $errors[$index]['key'] = $index;
            $errors[$index]['value'] = $error;
        }

        return array_values($errors);
    }
};

if (!function_exists('handleValidationErrorsWithWeb')) {
    function handleValidationErrorsWithWeb($validationErrors)
    {
        $errors = [];

        foreach ($validationErrors as $index => $error) {

            $errors['key'] = $index;
            $errors['value'] = $error;
        }

        return array_values($errors);
    }
};

if (!function_exists('paginationMetaData')) {
    function paginationMetaData($pagination)
    {

        $meta = [
            'current_page' => $pagination->currentPage(),
//            'last_page' => $pagination->lastPage(),
            'per_page' => $pagination->perPage(),
            'total' => (array_key_exists('total', $pagination->toArray()))?$pagination->total():null,
            'options' => $pagination->getOptions(),
            'has_other_pages' => $pagination->hasMorePages(),
        ];

        $links = [
            'prev' => $pagination->previousPageUrl(),
            'next' => $pagination->nextPageUrl(),
        ];

        return [
            'links' => $links,
            'meta' => $meta,
        ];
    }
};

if (!function_exists('simplePaginationMetaData')) {
    function simplePaginationMetaData($pagination)
    {

        $meta = [
            'current_page' => $pagination->currentPage(),
//            'last_page' => $pagination->lastPage(),
            'per_page' => $pagination->perPage(),
//            'total' => $pagination->total(),
            'options' => $pagination->getOptions(),
            'has_other_pages' => $pagination->hasMorePages(),
        ];

        $links = [
            'prev' => $pagination->previousPageUrl(),
            'next' => $pagination->nextPageUrl(),
        ];

        return [
            'links' => $links,
            'meta' => $meta,
        ];
    }
};

if (!function_exists('checkFileExtensions')) {
    function checkFileExtensions($extension, $extensions)
    {
        if (in_array($extension, $extensions)) {
            return true;
        }

        return false;
    }
};


if (!function_exists('generatePinCode')) {
    function generatePinCode($digits = 6)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(1, 9);
            $i++;
        }

        return $pin;
    }
};


if (!function_exists('getKycSuitability')) {
    function getKycSuitability($investor, $user=null)
    {
        if ($user && $user->type == 'company') {
            return true;
        }


        return !(Arr::get($investor->data, 'investment_xp') != 3 || $investor->annual_income == 1 || Arr::get($investor->data, 'years_of_inv_in_securities') == 0);
    }
};

//if (!function_exists('calculate')) {
//    /**
//     * Remove decimal points if needed
//     *
//     * @param  float  $lat
//     * @param  float  $lng
//     * @return \Cknow\Money\Money
//     */
//    function unrealizedPL($units, $totalUnits, $initialValuation, $latestValuation)
//    {
//        $sharePercentage = $units / $totalUnits;
//
//        return $latestValuation->multiply($sharePercentage)->subtract(
//            $initialValuation->multiply($sharePercentage)
//        );
//    }
//}

if (!function_exists('getAuthUser')) {
    function getAuthUser($guard)
    {
        return auth($guard)->user();

    }
}

if (!function_exists('getTokenPayload')) {
    function getTokenPayload($request)
    {
        $token = $request->header('Authorization');

        $tokenParts = explode(".", $token);
        $tokenHeader = base64_decode($tokenParts[0]);
        $tokenPayload = base64_decode($tokenParts[1]);

        dd($tokenPayload);
    }
}

if (!function_exists('textValidation')) {
    function textValidation()
    {
        //  return "regex:~^[a-z0-9٠-٩\-+,()/'\s\p{Arabic}]{1,60}$~iu";
        return "regex:/(^([a-zA-Z0-9- . , -+,()\s\p{Arabic}]+)(\d+)?$)/u";
//        return "regex:/[a-zA-Z0-9\s]+/";
    }

}

