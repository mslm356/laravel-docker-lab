<?php

namespace App\Actions\Investing;

use App\Settings\InvestingSettings;
use App\Settings\AccountingSettings;
use Lorisleiva\Actions\Concerns\AsAction;

class CalculateInvestingAmountAndFees
{
    use AsAction;

    /**
     * Calculate investing amount with fees
     *
     * @param \App\Models\Listings\Listing $listing
     * @param string $shares
     *
     * @return \Cknow\Money\Money
     */
    public function handle($listing, $shares)
    {
//        $vatPercentage = optional(app(AccountingSettings::class)->vat)->percentage ?? '0';
//        $feePercentage = app(InvestingSettings::class)->administrative_fees_percentage;

        $feePercentage = $listing->administrative_fees_percentage;
        $vatPercentage = ($listing->vat_percentage)?$listing->vat_percentage:15;

//        $vatPercentage = optional(app(AccountingSettings::class)->vat)->percentage ?? '0';


        $amount = $listing->share_price->multiply($shares);

        $feeAmount = $amount->multiply($feePercentage)->divide('100');
        $vatAmount = $feeAmount->multiply($vatPercentage)->divide('100');

        return [
            'amount' => $amount,
            'fee' => $feeAmount,
            'vat' => $vatAmount,
            'total' => money_sum($amount, $feeAmount, $vatAmount),
            'vatPercentage' => $vatPercentage,
            'feePercentage' => $feePercentage,
        ];
    }
}
