<?php

namespace App\Http\Resources\Investors;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'commercial_number' => $this->commercial_number,
            'address' => $this->address,
            'establishment_date' => $this->establishment_date,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'company_phone' => $this->company_phone,
            'office_phone' => $this->office_phone,
            'contact_person_name' => $this->contact_person_name,
            'contact_address' => $this->mailing_address,
            'status' => $this->profile_status,
            'account_name' => $this->account_name,
            'account_number' => $this->account_number,
            'custodian_name' => $this->custodian_name,
            'custodian_address' => $this->custodian_name,
            'created_at' => $this->created_at ? $this->created_at->format('Y-m-d') : '---',
        ];
    }
}
