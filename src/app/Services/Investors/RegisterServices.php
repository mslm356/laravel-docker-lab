<?php


namespace App\Services\Investors;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\VirtualAccountStatus;
use App\Enums\Banking\VirtualAccountType;
use App\Enums\Banking\WalletType;
use App\Enums\Investors\InvestorLevel;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Investors\Registration\CashOutPeriod;
use App\Enums\Investors\Registration\CurrentInvestment;
use App\Enums\Investors\Registration\Education;
use App\Enums\Investors\Registration\InvestmentObjective;
use App\Enums\Investors\Registration\Level;
use App\Enums\Investors\Registration\MoneyRange;
use App\Enums\Investors\Registration\NetWorth;
use App\Enums\Investors\Registration\Occupation;
use App\Enums\Role as EnumsRole;
use App\Jobs\ReminderProfile;
use App\Mail\Investors\WelcomeToAseel;
use App\Models\City;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Rules\UniquePhoneNumber;
use App\Services\LogService;
use App\Support\VirtualBanking\BankService;
use App\Traits\SendMessage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class RegisterServices
{
    use SendMessage;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @return array
     */
    public function rules($type = null)
    {
        $rules = [
            'phone_country_iso2' => !$type ? ['required_with:phone_number', 'string', 'size:2'] : [],
            'phone' => ['required'/*, 'string'*/, 'phone:phone_country_iso2', new UniquePhoneNumber('phone_country_iso2')],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'password' => ['required', 'string', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/', 'confirmed'],
            'device_token' => 'nullable|string'
        ];

        return $rules;
    }

    public function getRulesOfNin($type = null)
    {
        return [
            'nin' => ['required', 'string', 'regex:/(1|2)\d{9}/', 'unique:national_identities,nin'],
            'birth_date' => ['required', 'string', 'date_format:Y-m-d'],
            'birth_date_type' => isset($type) && $type ? [] : ['required', 'in:hijri,gregorian']
        ];
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\Users\User
     */
    public function create(array $data, $type = null)
    {
        $user = User::create([
            'email' => isset($data['email']) ? $data['email'] : null,
            'password' => Hash::make($data['password']),
            'phone_number' => !$type ? phone($data['phone'], $data['phone_country_iso2']) : phone($data['phone'], "SA"),
            'locale' => App::getLocale(),
            'firebase_token' => $type == 'mobile_app' && isset($data['device_token']) ? $data['device_token'] : null
        ]);

        $user->assignRole(
            Role::where([
                'name' => EnumsRole::Investor,
                'guard_name' => 'web'
            ])->first()
        );

        $user->investor()->create([
            'level' => InvestorLevel::Beginner,
            'is_kyc_completed' => false,
            'status' => InvestorStatus::PendingReview,
        ]);

        $wallet = $user->createWallet([
            'name' => WalletType::Investor,
            'slug' => WalletType::Investor,
            'uuid' => (string)Str::uuid(),
        ]);

        $account = (new BankService())->openVirtualAccount($user);

        $vaccount = $wallet->virtualAccounts()->create([
            'identifier' => $account['iban'],
            'status' => VirtualAccountStatus::Active,
            'type' => VirtualAccountType::Anb
        ]);

        return $user;
    }

    public function alertUser($user)
    {

        try {
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($user)->queue(new WelcomeToAseel($user));

                $this->sendSmsData($user);

                $reminderToCompleteProfileJob = (new ReminderProfile($user))->delay(\Carbon\Carbon::now()->addDays(4));
                dispatch($reminderToCompleteProfileJob);

            }

        } catch (\Exception $e) {
            return false;
        }
    }

    public function sendSmsData($user)
    {
        $content_en = 'Welcome to Aseel Financial Platform In order to be able to view the details of opportunities and investments, please complete the KYC form';
        $content_ar =  'حياك الله مستثمرنا الكريم في منصة أصيل المالية حتى تتمكن من الاطلاع على تفاصيل الفرص والاستثمار يرجى اكمال نموذج اعرف عميلك'   ;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);
    }

    public function getKyc()
    {
        $options = collect();

        $options->put(
            'MONEY_RANGE',
            $this->mapEnumValues(MoneyRange::class)
        );

        $options->put(
            'LEVEL',
            $this->mapEnumValues(Level::class)
        );

        $options->put(
            'INVESTMENT_OBJECTIVE',
            $this->mapEnumValues(InvestmentObjective::class)
        );

        $options->put(
            'CASH_OUT_PERIOD',
            $this->mapEnumValues(CashOutPeriod::class)
        );

        $options->put(
            'OCCUPATION',
            $this->mapEnumValues(Occupation::class)
        );

        $options->put(
            'CITIES',
            City::limit(50)->get()->map(fn ($city) => [
                'name' => $city->name,
                'id' => $city->id
            ])
        );

        $options->put(
            'NET_WORTH',
            $this->mapEnumValues(NetWorth::class)
        );

        $options->put(
            'EDUCATION',
            $this->mapEnumValues(Education::class)
        );

        $options->put(
            'CURRENT_INVESTMENT',
            $this->mapEnumValues(CurrentInvestment::class)
        );

        return $options;
    }

    /**
     * Map enum values to match the RegistrationOption objects
     *
     * @param string $class
     * @return array
     */
    public function mapEnumValues($class)
    {
        try {
            $alert_messages = $class::getAlertMessages();
        } catch (\Exception $exception) {
            $alert_messages=[];
        }


        return array_values(
            array_map(
                function ($value) use ($class, $alert_messages) {
                    return [
                        'name' => $class::getDescription($value),
                        'key' => $value,
                        'alert'=>isset($alert_messages[$value])?$alert_messages[$value]:null
                    ];
                },
                $class::asArray()
            )
        );
    }

    public function checkAgeOfUser($birth_date, $birth_date_type)
    {
        $today = \Carbon\Carbon::now();
        $todayInHijri = Hijri::Date('Y-m-d', $today->format('Y-m-d'));
        $result = 0;
        $start_date = new \DateTime($birth_date);
        if ($birth_date_type == "hijri") {
            $end = new \DateTime($todayInHijri);
            $result = optional($start_date->diff($end))->y < 18 ? 0 : 1;
        } else {
            $end = new \DateTime($today);
            $result = optional($start_date->diff($end))->y < 18 ? 0 : 1;
        }
        return $result;

    }

    public function updateUserInfo($user, $info, $token=null)
    {
        $user->first_name = isset($info['first_name']) && isset($info['first_name'][$user->locale]) ? $info['first_name'][$user->locale] : null;
        $user->last_name = isset($info['second_name']) && isset($info['second_name'][$user->locale]) ? $info['second_name'][$user->locale] : null;
        $user->jwt_token = $token ? $token : null;
        $user->save();
    }

    public function getRules()
    {
        return [
            'vid' => ['required', 'string', 'uuid'],
            'code' => ['required', 'string'],
            'nin' => ['required', 'string', 'regex:/(1|2)\d{9}/'],
        ];
    }

    public function deleteOldIdentities($user)
    {
        $oldIdentities = NationalIdentity::where('user_id', $user->id)->get();

        foreach ($oldIdentities as $oldIdentity) {
            $oldIdentity->addresses()->delete();
            $oldIdentity->delete();
        }
    }

}
