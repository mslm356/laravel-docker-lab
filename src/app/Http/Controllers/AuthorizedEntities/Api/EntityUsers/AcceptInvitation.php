<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\EntityUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Support\Users\Invitations\Invitation;
use App\Support\Users\Invitations\ValidationResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;

class AcceptInvitation extends Controller
{
    /**
     * Get the users of the entity
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $data = $this->validate($request, [
            'token' => ['required', 'string'],
            'password' => ['required', Password::default(), 'confirmed'],
            'email' => ['required', 'email']
        ]);

        return DB::transaction(function () use ($data) {
            $response = Invitation::accept(
                $data['email'],
                $data['token'],
                function ($user, $invitation) use ($data) {
                    $user->update([
                        'password' => Hash::make($data['password'])
                    ]);
                }
            );

            if ($response === ValidationResponse::INVITATION_ACCEPTED) {
                return response()->json();
            }

            throw ValidationException::withMessages([
                'email' => __('messages.invalid_authorized_entity_invitation'),
                'reason' => $response
            ]);
        });
    }
}
