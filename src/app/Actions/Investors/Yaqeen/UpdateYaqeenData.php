<?php

namespace App\Actions\Investors\Yaqeen;

use App\Services\LogService;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdateYaqeenData
{
    use AsAction;

    public function handle($user,$dif_birth_days=0)
    {
        $yaqeenInfoService = new YaqeenInfoService();


        try {

            $birth_date= Carbon::createFromFormat('Y-m-d', $user->nationalIdentity->birth_date);

            if ($dif_birth_days > 0)
                $birth_date=$birth_date->addDays($dif_birth_days);
            elseif($dif_birth_days < 0)
                $birth_date=$birth_date->subDays(abs($dif_birth_days));


            $yaqeen_request_data=['nin'=>$user->nationalIdentity->nin,'birthDateInstance'=>$birth_date];

//            echo $user->nationalIdentity->nin."\n";
//            $this->logService->log('Yaqeen-command-exception',null, $yaqeen_request_data);


            $info = $yaqeenInfoService->yaqeenInfo($yaqeen_request_data);

            $addresses = $yaqeenInfoService->yaqeenAddress($yaqeen_request_data);

            DB::beginTransaction();

            if($info)
            {
                $info['yaqeen_confirmation_at']=Carbon::now();
                $user->nationalIdentity->update($info);
            }

            if($addresses && count($addresses) > 0)
            {
                $user->nationalIdentity->addresses()->delete();
                $user->nationalIdentity->addresses()->createMany($addresses);

            }

            DB::commit();

        }catch (\Exception $e)
        {
            DB::rollBack();

            $logService=new LogService();
            $logService->log('YAQEEN-UPDATE-DATA-EX', $e);
        }


    }
}
