<?php

return [
    'subject' => 'Welcome to Aseel',
    'message_1' => 'Our honorable investor, Aseel platform',
    'message_2' => 'will provide you with unique opportunities to invest',
    //'message_3' => 'We are glad to have you in Aseel platform',
    'link_text' => 'You can complete your profile via the link',
    'message_4' => 'Be the first to invest in the opportunities that Aseel will offer by following us on social media'
];
