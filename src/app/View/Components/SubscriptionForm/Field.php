<?php

namespace App\View\Components\SubscriptionForm;

use Illuminate\View\Component;

class Field extends Component
{
    public $field;
    public $dark;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($field, $dark = false)
    {
        $this->field = $field;
        $this->dark = $dark;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.subscription-form.field');
    }
}
