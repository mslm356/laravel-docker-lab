<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Listings\AttachmentType;
use App\Enums\Listings\DetailCategory;
use App\Enums\MemberShips\MemberShipType;
use App\Http\Resources\Admins\Listings\ListingEditImagesResource;
use App\Http\Resources\Investors\Listings\TeamResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EditListingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $images = $this->images->sortBy("index");
        $memberShips = json_decode($this->opened_member_ships);

        return [
            'listing' => [
                'title_en' => $this->title_en,
                'title_ar' => $this->title_ar,
                'city_id' => $this->city_id,
                'type_id' => $this->type_id,
                'investment_type' => $this->investment_type,
                'target' => $this->target->formatByDecimal(),
                'total_shares' => $this->total_shares,
                'min_inv_share' => $this->min_inv_share,
                'max_inv_share' => $this->max_inv_share,
                'reserved_shares' => $this->reserved_shares,
                'deadline' => $this->deadline->toDateString(),
                'gross_yield' => $this->gross_yield,
                'dividend_yield' => $this->dividend_yield,
                'rent_amount' => $this->rent_amount,
                'bank_account_id' => $this->bank_account_id,
                'authorized_entity_id' => $this->authorized_entity_id,
                'is_closed' => $this->is_closed,
                'is_visible' => $this->is_visible,
                'is_completed' => $this->is_completed,
                'start_at' => $this->start_time,
                'administrative_fees_percentage' => $this->administrative_fees_percentage,
                'work_date' => $this->work_date,
                'expected_net_return' => $this->expected_net_return,
                'fund_duration' => $this->fund_duration,
                'early_investment_date' => $this->early_start_time,
                'investor_membership' =>  is_array($memberShips) && in_array(MemberShipType::Regular , $memberShips) ? true : false,
                'gold_membership' =>  is_array($memberShips) && in_array(MemberShipType::Gold , $memberShips) ? true : false,
                'diamond_membership' =>  is_array($memberShips) && in_array(MemberShipType::Diamond , $memberShips) ? true : false,
                'upgraded_membership' =>  is_array($memberShips) && in_array(MemberShipType::Upgraded , $memberShips) ? true : false,
            ],
            'team' => $this->getDetailByKey(DetailCategory::Team, []),
            'timeline' => $this->getDetailByKey(DetailCategory::Timeline, []),
            'property_details' => array_merge(
                $this->getDetailByKey(
                    DetailCategory::PropertyDetails,
                    [
                        'description_en' => '', 'description_ar' => ''
                    ]
                ),
                [
                    'files' => $this->filterAttachments(AttachmentType::Details)
                ],
            ),
            'financial_details' => array_merge(
                $this->getDetailByKey(
                    DetailCategory::FinancialDetails,
                    [
                        'content_en' => '',
                        'content_ar' => '',
                    ]
                ),
                [
                    'files' => $this->filterAttachments(AttachmentType::FinancialDetails)
                ]
            ),
            'terms_conditions' => $this->getDetailByKey(
                DetailCategory::TermsAndConditions,
                [
                    'description_en' => '',
                    'description_ar' => '',
                ]
            ),
            'risks' => array_merge(
                $this->getDetailByKey(
                    DetailCategory::Risks,
                    [
                        'content_en' => '',
                        'content_ar' => ''
                    ]
                ),
                [
                    'files' => $this->filterAttachments(AttachmentType::Risks)
                ]
            ),
            'due_diligence' => array_merge(
                $this->getDetailByKey(
                    DetailCategory::DueDiligence,
                    [
                        'content_en' => '',
                        'content_ar' => ''
                    ]
                ),
                [
                    'files' => $this->filterAttachments(AttachmentType::DueDiligence)
                ]
            ),
            'location' => array_merge(
                $this->getDetailByKey(
                    DetailCategory::Location,
                    [
                        'description_en' => '',
                        'description_ar' => '',
                        'gmap_url' => ''
                    ]
                ),
                [
                    'files' => $this->filterAttachments(AttachmentType::Location)
                ]
            ),
            'proof_docs' => [
                'files' => $this->filterAttachments(AttachmentType::ProofDocs)
            ],
            'images' => ListingEditImagesResource::collection($images) /*$images->map(fn ($img) => ['id' => $img->id, 'url' => $img->url, 'name' => $img->name])*/,
            'investing_started' => $this->investingStarted(),
        ];
    }

    /**
     * Helper function to return listing details
     * category on the object root
     */
    public function getDetailByKey($category, $default = null)
    {
        return $this->whenLoaded('details', function () use ($category, $default) {
            $item = $this->details->firstWhere('category', $category);

            if ($item) {
                return $item['value']->toArray();
            }

            if ($default instanceof \Closure) {
                return $default();
            }

            return $default;
        });
    }

    public function filterAttachments($type)
    {
        return $this->attachments->filter(fn($item) => $item->extra_info->get('type') === $type)->values();
    }
}
