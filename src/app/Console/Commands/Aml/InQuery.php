<?php

namespace App\Console\Commands\Aml;


use App\Enums\Role;
use App\Models\AML\AmlUser;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Aml\AmlServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InQuery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aml:in-query {--customer_reference_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create old customers in aml';
    public $amlServices;
    public $logServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->amlServices = new AmlServices();
        $this->logServices = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            if ($this->option('customer_reference_id')) {

                $data = [
                    'type' => 'inQuery',
                    'customer_reference_id' => $this->option('customer_reference_id')
                ];

                $response = json_decode($this->amlServices->handle($data),1);

                dd($response);
            }

            DB::table('aml_users')
                ->whereNull('risk_result')
                ->select('customer_reference_id')
                ->orderBy('id')
                ->lazy(50000)
                ->each(function ($aml, $key) {

                    $data = [
                        'type' => 'inQuery',
                        'customer_reference_id' => $aml->customer_reference_id
                    ];

                    $response = json_decode($this->amlServices->handle($data),1);

                    $data = [
                      'response' => json_encode($response),
                      'risk_result' => isset($response['risk_assessment']['risk_result']) ? $response['risk_assessment']['risk_result'] : null,
                      'screen_result' => empty($response['screen_result']),
                    ];

                    DB::table('aml_users')
                        ->where('customer_reference_id', $aml->customer_reference_id)
                        ->update($data);

                    dump($key);

                });

        } catch (\Exception $e) {
            $this->logServices->log('AML-JOB-EXCEPTION', $e);
            dd($e->getFile(), $e->getLine(), $e->getMessage());
        }
    }
}
