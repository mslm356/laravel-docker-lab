<?php

namespace App\Support\Elm\Yaqeen;

use SoapClient;
use Illuminate\Support\Facades\App;

class YaqeenSoapClient
{
    public static function instantiate()
    {
        return new SoapClient(config('services.yaqeen.url'), [
            'trace' => App::environment('local'),
            'cache_wsdl' => WSDL_CACHE_NONE,
            'soap_version' => SOAP_1_1,
            'location' => rtrim(config('services.yaqeen.url'), '?wsdl')
        ]);
    }
}
