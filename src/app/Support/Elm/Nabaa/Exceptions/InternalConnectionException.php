<?php

namespace App\Support\Elm\Nabaa\Exceptions;

use Exception;

class InternalConnectionException extends Exception
{
    protected $reason;

    public function __construct($reason)
    {
        $this->reason = $reason;
        parent::__construct('Failed to connect to Nabaa');
    }

    public function context()
    {
        return [
            'reason' => $this->reason
        ];
    }
}
