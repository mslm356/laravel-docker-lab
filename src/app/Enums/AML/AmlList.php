<?php

namespace App\Enums\AML;

use BenSampo\Enum\Enum;

final class AmlList extends Enum
{
    const UN =   1;
    const US =   2;
    const EU =   3;
    const MOI =   4;
}
