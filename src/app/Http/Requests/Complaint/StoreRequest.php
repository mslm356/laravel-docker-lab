<?php

namespace App\Http\Requests\Complaint;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = textValidation();
        return [
            'subject' => "required|string|max:150|$regex",
            'description' => "required|string|$regex|max:1000",
            'reviewer_comment' => "string|$regex|max:1000",
            'file' => "file|mimes:jpeg,png,pdf|max:2048",
            'complaint_type_id' => "integer|required|exists:complaint_types,id",
        ];
    }
}
