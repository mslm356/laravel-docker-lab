<?php

namespace App\Http\Resources\Common\BankAccounts;

use App\Enums\BankAccountStatus;
use App\Enums\Banking\BankCode;
use Illuminate\Http\Resources\Json\JsonResource;

class BankAccountListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'iban' => $this->iban,
            'status' => [
                'description' => BankAccountStatus::getDescription($this->status),
                'value' => $this->status,
            ],
            'bank' => BankCode::getDescription($this->bank),
            'reviewer_comment' => $this->reviewer_comment,
            'create_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
