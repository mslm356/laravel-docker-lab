<?php

namespace App\Console\Commands\Investors;

use App\Enums\Role;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Validation\ValidationException;

class VerifyAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:verify {--phone=} {--country_code=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'verify account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        try {

            $phone = phone($this->option('phone'), $this->option('country_code'));

            $user = User::where('phone_number', $phone)->first();

            if (!$user || !$user->investor) {
                dd('user not valid or profile');
            }

            return DB::transaction(function () use ($user) {

                if (!$user->investor->data) {
                    dd('identity data not valid');
                }

                if ($user->investor->is_identity_verified) {
                    dd('account is verified');
                }

                $nationality = NationalIdentity::query()
                    ->where('nin', $user->investor->data['identity_verification_tmp']['identity']['nin'])
                    ->count();

                if ($nationality) {
                    dd('nin already exists');
                }

                $this->deleteOldIdentities($user);

                $identity = NationalIdentity::create($user->investor->data['identity_verification_tmp']['identity']);

                foreach ($user->investor->data['identity_verification_tmp']['addresses'] as $address) {
                    $identity->addresses()->create($address);
                }

                $user->investor->update([
                    'is_identity_verified' => true,
                    'data->identity_verification_tmp' => null,
                ]);
            });

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage(), ['errors' => $errors]);
        } catch (VerificationSmsException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

    }

    public function deleteOldIdentities($user)
    {
        $oldIdentities = NationalIdentity::where('user_id', $user->id)->get();

        foreach ($oldIdentities as $oldIdentity) {
            $oldIdentity->addresses()->delete();
            $oldIdentity->delete();
        }
    }

}
