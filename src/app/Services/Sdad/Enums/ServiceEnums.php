<?php

namespace App\Services\Sdad\Enums;

class ServiceEnums
{
    const AUTH = 1;
    const SINGLE_INVOICE_CREATE = 2;
    const SINGLE_INVOICE_BRIEF= 3;

    public static function all(): array
    {
        return [
            self::AUTH,
            self::SINGLE_INVOICE_CREATE,
            self::SINGLE_INVOICE_BRIEF,

        ];
    }

    public static function service_name(int $service_enum): ?string
    {
        switch ($service_enum) {
            case self::AUTH :
                $service_name = 'auth';
                break;
            case self::SINGLE_INVOICE_CREATE :
                $service_name = 'single_invoice_create';
                break;
            case self::SINGLE_INVOICE_BRIEF :
                $service_name = 'single_invoice_brief';
                break;
            default:
                $service_name = null;

        }
        return $service_name;
    }
}
