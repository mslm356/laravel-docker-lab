<?php

return [
    'subject' => 'استلام طلب كشف حساب',
    'welcome_message' => 'عزيزنا الكريم  :name',
    'message_1' => 'تم استلام طلب كشف عمليات الاستثمار في منصة أصيل',
    'message_2' => ' وسيتم الرد عليكم قريبا من قبل الفريق ',
    'message_3' => '  أطيب التحيات،',
    'message_4' => ' أصيل المالية ',
    'link_text' => 'عرض لوحه التحكم',
];
