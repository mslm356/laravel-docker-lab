<?php

namespace App\Http\Resources\Common\Elm;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\NationalIdentities\NationalIdentityResource;

class AbsherInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isAbherVerified = $this->investor->is_identity_verified;

        return [
            'is_identity_verified' => $isAbherVerified,
            'identity' => $this->when($isAbherVerified, fn () => new NationalIdentityResource($this->nationalIdentity)),
        ];
    }
}
