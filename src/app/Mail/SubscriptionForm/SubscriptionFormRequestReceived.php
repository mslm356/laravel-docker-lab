<?php

namespace App\Mail\SubscriptionForm;

use App\Models\SubscriptionFormRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscriptionFormRequestReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $subscriptionFormRequest;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SubscriptionFormRequest $subscriptionFormRequest)
    {
        $this->subscriptionFormRequest = $subscriptionFormRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscriptionFormRequest = $this->subscriptionFormRequest;

        return $this->view('emails.subscription_form.subscription_form_request_received', compact('subscriptionFormRequest'))
            ->subject("Account statement received $subscriptionFormRequest->id");
    }
}
