<?php

namespace App\Http\Middleware;

use Closure;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class LocalizeApiRequests
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $supportedLocales = array_keys(LaravelLocalization::getSupportedLocales());

        $headerLocale = $request->header('X-App-Locale');
        $headerLocale_2 = $request->header('lang');

        $appLocale = null;

        if (!is_null($headerLocale) && in_array($headerLocale, $supportedLocales)) {
            $appLocale = $headerLocale;

        } elseif (!is_null($headerLocale_2) && in_array($headerLocale_2, $supportedLocales)) {
            $appLocale = $headerLocale_2;

        } else {
            $appLocale = config('app.locale');
        }

        LaravelLocalization::setLocale($appLocale);

        return $next($request);
    }
}
