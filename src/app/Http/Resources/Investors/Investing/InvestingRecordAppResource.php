<?php

namespace App\Http\Resources\Investors\Investing;

use Illuminate\Http\Resources\Json\JsonResource;

class InvestingRecordAppResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'listing' => $this->whenLoaded('listing', fn () => $this->listing->title),
            'amount_without_fees' => $this->amount_without_fees->formatByDecimal(),
            'vat_amount' => $this->vat_amount->formatByDecimal(),
            'admin_fees' => $this->admin_fees->formatByDecimal(),
            'zatca_invoice' => $this->getZatcaFilePath(),
            'subscription_form' => $this->getSubscriptionFilePath(),
            'zatca_invoice_link' => $this->zatcaInvoice && $this->zatcaInvoice->pdfFile ? route('investor.files.get.url', ['file'=> $this->zatcaInvoice->pdfFile->id]) : null,
            'subscription_form_link' => $this->subscriptionForm ? route('investor.files.get.url', ['file'=> $this->subscriptionForm->id]) : null,
            'created_at' => $this->created_at ? $this->created_at->timezone('Asia/Riyadh')->format('Y-m-d h:i:s A') : null,
        ];
    }

    public function getZatcaFilePath()
    {

        if ($this->zatcaInvoice && $this->zatcaInvoice->pdfFile) {
            return $this->zatcaInvoice->pdfFile->temp_full_path;
        }

        return null;
    }

    public function getSubscriptionFilePath()
    {

        if ($this->subscriptionForm) {
            return $this->subscriptionForm->temp_full_path;
        }

        return null;
    }
}
