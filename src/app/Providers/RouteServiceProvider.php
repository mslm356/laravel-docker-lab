<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Traits\LoadsTranslatedCachedRoutes;

class RouteServiceProvider extends ServiceProvider
{
    use LoadsTranslatedCachedRoutes;

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $enabledRoutes = config('route.enabled_route_groups', []);

        $this->mapApiRoutes();

        if (in_array('investor', $enabledRoutes)) {
            $this->mapInvestorsApiRoutes();
        }

        if (in_array('admin', $enabledRoutes)) {
            $this->mapAdminsApiRoutes();
        }

        if (in_array('authorized_entity', $enabledRoutes)) {
            $this->mapAuthorizedEntitiesApiRoutes();
        }
        if (in_array('edaat-webhook', $enabledRoutes)) {
            $this->mapEdaatWebhookApiRoutes();
        }

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        // XssSanitizer
        Route::middleware(['web', 'headers_middleware'])
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        // XssSanitizer
        Route::prefix('api')
            ->middleware(['api', 'headers_middleware'])
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapEdaatWebhookApiRoutes()
    {
        // XssSanitizer
        Route::prefix('api/edaat-webhook')
            ->middleware(['api', 'headers_middleware'])
            ->group(base_path('routes/edaat_webhook.php'));
    }
    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapInvestorsApiRoutes()
    {
        // XssSanitizer
        Route::prefix('api/investor')
            ->middleware(['api','investor_log_activity','session_handel', 'headers_middleware'])
            ->name('investor.')
            ->group(base_path('routes/investor-api.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminsApiRoutes()
    {
        // XssSanitizer
        Route::prefix('api/admin')
            ->middleware(['api', 'headers_middleware'])
            ->name('admin.')
            ->group(base_path('routes/admin-api.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAuthorizedEntitiesApiRoutes()
    {
        // XssSanitizer
        Route::prefix('api/authorized-entity')
            ->middleware(['api', 'headers_middleware'])
            ->name('authorized_entity.')
            ->group(base_path('routes/authorized-entity-api.php'));
    }
}
