<?php

namespace App\Http\Resources\Admins\Listings;

use App\Enums\Listings\VoteAnswers;
use App\Models\InvestorVote;
use Illuminate\Http\Resources\Json\JsonResource;

class VotesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $listing = $this->listing;
        $listingInvestorsCount = $listing->investors->count();
        $voteInvestorsCount = $this->InvestorVotes->count();

        $percentage = $listingInvestorsCount ? number_format(($voteInvestorsCount * 100)  / $listingInvestorsCount, 2) : 0;

        $answersCount = $this->popularAnswer($this);
        $popularAnswer = array_keys($answersCount, max($answersCount))[0];

        $answerPercentage = $voteInvestorsCount ? ($answersCount[$popularAnswer] * 100) / $voteInvestorsCount : 0;
        return [
            'id' => $this->id,
            'vote' => $this->question,
            //'percentage' => $percentage . "%" ,
            'approvedCount' => $this->popularAnswer($this)['Approved'] ,
            'rejectedCount' => $this->popularAnswer($this)['Rejected'] ,
            'refrainCount' => ($listingInvestorsCount > 0 ? ($listingInvestorsCount - 1) : $listingInvestorsCount) - ($this->popularAnswer($this)['Approved'] + $this->popularAnswer($this)['Rejected']) ,
            'total_investors' => $listingInvestorsCount > 0 ? ($listingInvestorsCount - 1) : $listingInvestorsCount ,
            'answer' =>  __('messages.' . strtolower($popularAnswer)),
            'status' =>  $this->status == 0 ? __('messages.still_working') : __('messages.ended'),
            'is_ended' =>  $this->status == 0 ? false : true ,
            //'answer_percentage' => $this->getCount($listing,$this)
        ];
    }

    public function popularAnswer($vote)
    {
        $data = [
            'Approved' => InvestorVote::query()->where('vote_id', $vote->id)->where('answer', VoteAnswers::Approved)->count(),
            'Rejected' => InvestorVote::query()->where('vote_id', $vote->id)->where('answer', VoteAnswers::Rejected)->count(),
            'Refrain' => InvestorVote::query()->where('vote_id', $vote->id)->where('answer', VoteAnswers::Refrain)->count(),
        ];

        return $data;
    }

    /*    public function getQueryResult($listing)
        {
            $query = DB::table('investor_votes')->leftJoin('users', 'investor_votes.investor_id', '=', 'users.id')
                ->leftJoin('listing_investor', 'listing_investor.investor_id', '=', 'users.id')
                ->select('users.id',
                    DB::raw('CONCAT(users.first_name, " ", users.last_name) AS full_name'),
                    'users.phone_number', 'email', 'answer', 'listing_investor.invested_shares')
                ->where('listing_investor.listing_id', $listing->id)->get();

            return  $query;
        }

        public function getCount($listing,$vote)
        {
            $listingInvestors = $listing->investors ? $listing->investors->pluck('id')->toArray() : null ;
            $investorVotes = InvestorVote::where('vote_id',$vote->id)->whereIn('investor_id',$listingInvestors)->count();
            $data = [
                'approvedCount' =>  $this->getQueryResult($listing)->where('answer',1)->count()  ,
                'rejectedCount' =>  $this->getQueryResult($listing)->where('answer',2)->count()  ,
                'refrainCount' =>  count($listingInvestors) - $investorVotes  ,
            ];

            return $data;
        }*/
}
