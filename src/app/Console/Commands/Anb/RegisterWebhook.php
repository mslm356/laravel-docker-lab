<?php

namespace App\Console\Commands\Anb;

use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Http\HttpException;
use Illuminate\Console\Command;

class RegisterWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:register-webhook {--description=} {--url=} {--events=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register ANB webhooks for events: transaction.received,payment.received,payment.completed,statement.created';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = $this->option('url');
        $events = explode(',', $this->option('events'));
        $description = $this->option('description');

        try {
            $response = AnbApiUtil::createWebhook($url, $description, $events);
            $this->info("Webhook created successfully. [id: {$response['id']}]");
        } catch (HttpException $th) {
            $this->error($th->getResponse()->body());
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
        }
    }
}
