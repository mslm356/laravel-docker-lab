<?php

namespace App\Settings;

use App\Settings\Casters\ZatcaSellerCaster;
use App\Support\Zatca\EInvoice\Seller;
use Spatie\LaravelSettings\Settings;

class ZatcaSettings extends Settings
{
    public Seller $seller;

    public static function casts(): array
    {
        return [
            'seller' => ZatcaSellerCaster::class
        ];
    }

    public static function group(): string
    {
        return 'zatca';
    }
}
