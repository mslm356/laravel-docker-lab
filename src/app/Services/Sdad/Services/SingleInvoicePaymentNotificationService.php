<?php

namespace App\Services\Sdad\Services;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\EdaatInvoice;
use App\Services\LogService;
use App\Services\Sdad\Enums\InvoiceCodesStatusesEnums;
use App\Services\Sdad\Repository\SingleInvoiceInquiryRepository;
use App\Services\Sdad\Repository\SingleInvoicesRepository;
use App\Traits\NotificationServices;
use Carbon\Carbon;

class SingleInvoicePaymentNotificationService
{
    use NotificationServices;

    public $invoice_inquiry_repo;
    public $invoice_create_repo;
    public $logService;

    public function __construct()
    {
        $this->invoice_create_repo = new SingleInvoicesRepository();
        $this->invoice_inquiry_repo = new SingleInvoiceInquiryRepository();
        $this->logService = new LogService();
    }

    public function make_transaction(array $request_objects): ?bool
    {
        try {
            foreach ($request_objects as $payment_obj) {
                $invoice = EdaatInvoice::query()
                    ->where('status', '!=', InvoiceCodesStatusesEnums::EXPIRED)
                    ->where('id', $payment_obj['InternalCode'])
                    ->first();

                if (!$invoice) {
                    $this->logService->log('SDAD-GET-INVOICE', null, ['issue', $payment_obj]);
                    continue;
                }

                if ($invoice->status == InvoiceCodesStatusesEnums::PAID) {
                    $this->logService->log('SDAD-INVOICE-ALREADY-PAID', null, ['issue', $payment_obj, $invoice->status]);
                    continue;
                }

                $responseOfEdaatInvoice = $this->getEdaatInvoice($invoice);

                if (isset($responseOfEdaatInvoice['status']) && !$responseOfEdaatInvoice['status']) {
                    $this->logService->log('SDAD-EDAAT-GET-INVOICE', null, ['issue', $payment_obj, $responseOfEdaatInvoice]);
                    continue;
                }

                if (!isset($responseOfEdaatInvoice['inquiry_result'])) {
                    $this->logService->log('SDAD-INQUIRY_RESULT-KEY-NOT-FOUND', null, ['issue', $payment_obj, $responseOfEdaatInvoice]);
                    continue;
                }

                $inquiry_result = $responseOfEdaatInvoice['inquiry_result'];

                $inquiry_data = [
                    'status' => $inquiry_result->Body->StatusCode,
                    'invoice_brief_result' => $inquiry_result
                ];

                $invoice = $this->invoice_create_repo->update($invoice->id, $inquiry_data);

                if ($inquiry_result->Body->StatusCode != InvoiceCodesStatusesEnums::PAID) {
                    $this->logService->log('SDAD-STATUS-NOT-PAID', null, ['issue', $inquiry_result]);
                    continue;
                }

                $user = $invoice->user;

                $userWallet = $user->getWallet(WalletType::Investor);

                $created_at = Carbon::parse($payment_obj['PaymentDate'])->format('Y-m-d');

                $userWallet->deposit(
                    $invoice->amount * 100,
                    [
                        'reference' => transaction_ref(),
                        'reason' => TransactionReason::SdadReason,
                        'details' => [
                            'version' => 1,
                            'source' => 'sdad',
                            'eptn' => $payment_obj['EPTN'],
                            'invoice_id' => $payment_obj['InvoiceNo'],
                            'data' => $payment_obj,
                        ],
                        'value_date' => $created_at,
                    ]
                );

                $userWallet->refreshBalance();
            }

            return true;

        } catch (\Exception $e) {
            $this->logService->log('Sdad-Payment-Notification-Service-ERROR', $e, ['issue', json_encode($request_objects)]);
            throw new \Exception('SingleInvoicePaymentNotificationService  error');
        }
    }

    public function getEdaatInvoice($invoice)
    {

        try {
            return [
                'status' => true,
                'inquiry_result' => $this->invoice_inquiry_repo->initiate_provider($invoice->edaat_invoice_id)->inquiry($invoice->edaat_invoice_id),
            ];

        } catch (\Exception $e) {

            $invoice->is_failed = 1;
            $invoice->save();

            $response['status'] = false;
            $response['message'] = $e->getMessage();

            return $response;
        }
    }


}
