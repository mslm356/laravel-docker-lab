<?php

namespace App\Support\Users\Invitations;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string accept($email, $token, $action)
 * @method static string createInvitationLink($email, $role, $inviter)
 *
 * @see \App\Support\Users\Invitations\InvitationManager
 */
class Invitation extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'invitation';
    }
}
