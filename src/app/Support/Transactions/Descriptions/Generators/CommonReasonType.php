<?php
namespace App\Support\Transactions\Descriptions\Generators;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorInterface;

class CommonReasonType implements GeneratorInterface
{
    /**
     * Generate description
     *
     * @return string
     */
    public function generate(Transaction $transaction, $locale = null): string
    {
        $gaurd=(request()->has('guard'))?request()->guard:'web';
        $locale=getAuthUser($gaurd)->locale;
        return isset($transaction->meta['reason_'.$locale])?$transaction->meta['reason_'.$locale]:'---';
    }
}
