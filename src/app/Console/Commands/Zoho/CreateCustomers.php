<?php

namespace App\Console\Commands\Zoho;

use App\Enums\Banking\WalletType;
use App\Jobs\Zoho\CreateOldUsers;
use App\Models\Users\User;
use App\Support\Zoho\Customer;
use App\Support\Zoho\CustomerPayments;
use Illuminate\Console\Command;

class CreateCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:create-customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->zohoCustomer = new Customer();
        $this->customerPayment = new CustomerPayments();
    }

    public $zohoCustomer;
    public $customerPayment;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            User::query()
                ->whereNull('zoho_id')
                ->lazyById()
                ->each(function ($user) {

                    $wallet = $user->getWallet(WalletType::Investor);

                    if (!$user->zoho_id && $wallet && $wallet->balance > 0) {

                        $this->zohoCustomer->saveCustomer($user, \App\Enums\Zoho\NewCustomer::Customer);
//
//                        if ($wallet && $wallet->balance > 0) {
//                            $this->customerPayment->createCustomerPayment($user, money($wallet->balance)->formatByDecimal());
//                        }
                    }
                });

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }


//        $CreateOldUsersJob = (new CreateOldUsers());
//        dispatch($CreateOldUsersJob);
    }
}
