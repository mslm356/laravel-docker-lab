<?php

namespace App\Http\Controllers\Common;

use App\Enums\FileType;
use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Http\Request;

class ViewFiles extends Controller
{

    public function __invoke(Request $request, File $file)
    {

        if (auth()->check() && $this->authorize($request)) {
            return response()->file(storage_path('app/' . $file->path));
        }

        $url = auth()->check() ? config('app.home_url') : config('app.investor_url') . '/app/overview';

        return redirect($url);
    }

    public function authorize($request)
    {
        $file = $request->route('file');

        if ($file->type === FileType::SubscriptionForm) {
            return $file->fileable->investor_id === $request->user()->id;
        } else if ($file->type === FileType::ListingReport) {
            return $request->user()->can('accessListingWhichInvestedIn', $file->fileable->listing);
        } else if ($file->type === FileType::TransactionReceipt) {
            return $file->fileable->iban === $request->user()->wallet->iban;
        } else if ($file->type === FileType::ListingAttachment) {
            return $file->fileable->is_visible ||
                $request->user()->can('accessListingWhichInvestedIn', $file->fileable);
        } else if ($file->type === FileType::TransactionZatcaEInvoice) {
            return $file->fileable->invoicable->investor_id === $request->user()->id;
        }

        return false;
    }
}
