<?php

return [
    'service' => env('WALLET_SERVICE', 'local'),
    'vat' => env('WALLET_VAT', 0),
    'beginner_limit_per_listing' => env('BEGINNER_LIMIT', 20000000)
];
