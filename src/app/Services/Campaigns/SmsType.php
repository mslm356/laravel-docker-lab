<?php

namespace App\Services\Campaigns;

use App\Enums\Campaigns\CampaignStatus;
use App\Enums\Campaigns\CampaignUserStatus;
use App\Jobs\Campaigns\SetUsers;
use App\Jobs\Campaigns\UpdateUserStatus;
use App\Services\Campaigns\InvestorTypes\ManageData;
use App\Services\LogService;
use App\Traits\SendMessage;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class SmsType
{
    use SendMessage;

    public $data;
    public $campaign;
    public $alertClass = 'Sms';
    public $logService;

    public function __construct($data, $campaign)
    {
        $this->data = $data;
        $this->campaign = $campaign;
        $this->logService = new LogService();
    }

    public function handle()
    {
        $this->saveCampaignUsers();
    }

    public function saveCampaignUsers()
    {
        $setUsersJob = (new SetUsers($this->data, $this->campaign, $this->alertClass));
        dispatch($setUsersJob)->onQueue('campaigns');
    }

    public function sendCampaign($dataUsedToSend, $campaign)
    {
        $numbers = implode(',', array_column($dataUsedToSend['user_data'], 'phone_number'));

        $message = $campaign->description_ar;

        $message = $this->sendMessages($numbers, $message);

        $this->updateCampaign($campaign, $message);
    }

    public function manageInvestorsData()
    {
        $selectedColumns = ['users.id', 'phone_number'];
        $manageData = new ManageData();
        return $manageData->manageData($this->data, $selectedColumns);
    }

    public function handleData()
    {

        $data = $this->manageInvestorsData();

        return [
            'investor_data' => $data['investorData'],
            'variablesInText' => isset($data['extraSelectedColumn']) ? $data['extraSelectedColumn'] : [],
        ];
    }

    public function updateCampaign($campaign, $message)
    {
        $campaign->status = CampaignStatus::Ended;
        $campaign->response = $message;
        $campaign->save();

        $updateUsersStatusJob = (new UpdateUserStatus($this->campaign, $this->alertClass));
        dispatch($updateUsersStatusJob)
            ->onQueue('campaigns')
            ->delay(Carbon::now()->addMinutes(5));
    }

    public function updateCampaignUsersStatus($campaign)
    {

        //        $st = json_decode('{"statusCode":201,"messageId":6366891254,"cost":"0.0870","currency":"SAR","totalCount":2,"msgLength":1,"accepted":"[966597337290,966598138217,]","rejected":"[]"}', true);

        $messageResponse = Arr::first(json_decode($campaign->response, true));

        $accepted = $messageResponse['accepted'];
        $rejected = $messageResponse['rejected'];

        $accepted = rtrim($accepted, ']');
        $accepted = ltrim($accepted, '[');

        $acceptedNumbers = explode(',', $accepted);

        $rejected = rtrim($rejected, ']');
        $rejected = ltrim($rejected, '[');

        $rejectedNumbers = explode(',', $rejected);

        foreach ($acceptedNumbers as $acceptedNumber) {
            $this->getCampaignUser($campaign, $acceptedNumber, CampaignUserStatus::Sent);
        }

        foreach ($rejectedNumbers as $rejectedNumber) {
            $this->getCampaignUser($campaign, $rejectedNumber, CampaignUserStatus::Failed);
        }
    }

    public function getCampaignUser($campaign, $number, $status)
    {

        $user = $campaign->users()->where('phone_number', $number)->first();

        if (!$user) {
            $this->logService->log(
                'Campaigns-Sms:update-users-status',
                null,
                ['user not found', $user, 'phone_number' => $number ]
            );

            return false;
        }

        $user->pivot->status = $status;
        $user->pivot->response = $number;
        $user->pivot->save();
    }

}
