<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_versions', function (Blueprint $table) {
            $table->id();
            $table->string('platform');
            $table->string('version');
            $table->integer('order')->nullable();
            $table->tinyInteger('is_required')->default(0)->comment('1=>required, 0=>not required');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_versions');
    }
}
