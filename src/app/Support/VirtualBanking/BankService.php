<?php

namespace App\Support\VirtualBanking;

class BankService
{
    /** @var \App\Support\VirtualBanking\BankServiceInterface */
    private $driver = null;

    function __construct($driver = null)
    {
        if (is_null($driver)) {
            $driver = config('virtual-account.service');
        }

        $driverClassName = $this->resolveClassName($driver);

        $this->setDriver($driverClassName);
    }

    function getDriver()
    {
        return $this->driver;
    }

    function resolveClassName($driverName)
    {
        return 'App\Support\VirtualBanking\Strategies\\' . ucfirst($driverName);
    }

    function setDriver($driverClassName)
    {
        return $this->driver = new $driverClassName;
    }

    /**
     * Create wallet (Virtual Account)
     * @return array
     */
    function openVirtualAccount($model, $id = null, $suffix = '01'): array
    {
        return $this->getDriver()
            ->openVirtualAccount($model, $id, $suffix);
    }

    /**
     * Transfer money
     *
     * @return array|false
     */
    function transfer(
        array $custInfo,
        string $toIban,
        $amount,
        $bankCode,
        $tranactionId,
        $valueDate,
        $isFeeIncluded,
        $transferPurpose,
        $currency,
        $bankAccount = null,
        $transfer = null
    ) {
        return $this->getDriver()
            ->transfer(
                $custInfo,
                $toIban,
                $amount,
                $bankCode,
                $tranactionId,
                $valueDate,
                $isFeeIncluded,
                $transferPurpose,
                $currency,
                $bankAccount,
                $transfer
            );
    }

    /**
     * Check wallet current balance
     * @return float
     */
    function checkBalance(string $iban): float
    {
        return $this->getDriver()
            ->checkBalance($iban);
    }

    /**
     * get wallet transaction history
     */
    function getHistory(string $iban, array $options): object
    {
        return $this->getDriver()
            ->getHistory($iban, $options);
    }

    /**
     * Get transfer fee options
     *
     * @return array
     */
    function getTransferFeeOptions($toBankCode, $amount)
    {
        return $this->getDriver()
            ->getTransferFeeOptions($toBankCode, $amount);
    }

    /**
     * Get transfer fee options
     *
     * @return array
     */
    function validateTransferFeeOption($toBankCode, $amount, $option)
    {
        return $this->getDriver()
            ->validateTransferFeeOption($toBankCode, $amount, $option);
    }

    /**
     * Suspend/ Stop wallet
     *
     */
    function changeStatus()
    {
    }
}
