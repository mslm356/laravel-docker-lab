<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToInvestorSubscriptionTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_subscription_tokens', function (Blueprint $table) {
            $table->dropindex(['is_expire']);
            $table->index(['user_id', 'reference']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_subscription_tokens', function (Blueprint $table) {
            $table->index(['is_expire']);
            $table->dropindex(['user_id', 'reference']);
        });
    }
}
