<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Actions\Listings\AfterCloseListingAction;
use App\Jobs\Funds\SendAlertMailsOnCreate;
use App\Jobs\Listings\FlushCache;
use App\Services\CacheServices;
use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\AuthorizedEntity;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Enums\AuthorizedEntityStatus;
use App\Enums\Listings\ListingReviewStatus;
use App\Actions\Listings\UpdateOrCreateListing;
use App\Common\ValidationRules\Listings\ListingRules;
use App\Actions\Listings\Wallets\CreateListingWallets;
use App\Http\Resources\Admins\Listings\ListingResource;
use App\Http\Resources\Admins\Listings\EditListingResource;
use App\Actions\Listings\ValidateReservedSharesAreWithinLimit;
use App\Http\Resources\Admins\Listings\ListingInvestorResource;
use App\Http\Resources\Admins\Listings\ListingListItemResource;

class ListingController extends Controller
{
    public $cacheServices;
    public $logServices;

    public function __construct()
    {
        $this->cacheServices = new CacheServices();
        $this->logServices = new LogService();
    }

    /**
     * Listings list
     *
     * @param
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {
        $listing = Listing::latest()->with('images')->paginate();

        return ListingListItemResource::collection($listing);
    }

    /**
     * Show Listing Deatils
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function show(Request $request, Listing $listing)
    {
        $listing->loadMissing([
            'details',
            'city',
            'type',
            'images',
            'attachments',
            'bankAccount',
            'authorizedEntity'
        ]);

        if ($request->input('edit')) {
            return new EditListingResource($listing);
        }
        return new ListingResource($listing);
    }

    /**
     * Show the investors listings
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInvestors(Listing $listing)
    {
        $listing->load('investors');

        return ListingInvestorResource::collection($listing->investors);
    }

    /**
     * Store the type in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $data = $this->validate(
            $request,
            array_merge(
                ListingRules::get(),
                ListingRules::getAdminListingRule(),
                ListingRules::getBankAccountIdRuleForAdmin($request),
                [
                    'listing.authorized_entity_id' => [
                        'required',
                        'integer',
                        Rule::exists(AuthorizedEntity::class, 'id')
                            ->where('status', AuthorizedEntityStatus::Approved)
                    ]
                ]
            )
        );

        $data['listing']['start_at'] = Carbon::parse($data['listing']['start_at'])->subHours(3);
        $data['listing']['early_investment_date'] = Carbon::parse($data['listing']['early_investment_date'])->subHours(3);
        $data['listing']['review_status'] = ListingReviewStatus::Approved;
        $data['listing']['opened_member_ships'] = json_encode($data['listing']['opened_member_ships']);

        try {

            DB::beginTransaction();
            $data['listing']['sms_delay'] = json_encode(['min' => 0, 'max' => 0]);

            $listing = UpdateOrCreateListing::run($data);

            CreateListingWallets::run($listing);

            DB::commit();

            $dates = $this->getDatesForFlushCache($listing, $data, 'create');
            $this->dispatchJobsForFlushFundCache($listing->id, $dates);

            $this->cacheServices->updateFundCache($listing->id);

            /*if ($listing->review_status === ListingReviewStatus::Approved && $listing->is_visible == 1 && $listing->is_closed == 0) {
              $sendAlertMailsOnCreateJob = (new SendAlertMailsOnCreate($listing));
              dispatch($sendAlertMailsOnCreateJob);
            */

            return response()->json([
                'id' => $listing->id
            ], 201);

        } catch (\Exception $e) {
            DB::rollBack();
            $code = $this->logServices->log('STORE-LISTING-ADMIN-EX', $e);
            return response()->json(['message' => 'sorry please try later : ' . $code], 400);
        }

//        return DB::transaction(function () use ($request) {
//            $data = $this->validate(
//                $request,
//                array_merge(
//                    ListingRules::get(),
//                    ListingRules::getAdminListingRule(),
//                    ListingRules::getBankAccountIdRuleForAdmin($request),
//                    [
//                        'listing.authorized_entity_id' => [
//                            'required',
//                            'integer',
//                            Rule::exists(AuthorizedEntity::class, 'id')
//                                ->where('status', AuthorizedEntityStatus::Approved)
//                        ]
//                    ]
//                )
//            );
//
//            $data['listing']['review_status'] = ListingReviewStatus::Approved;
//            $data['listing']['opened_member_ships'] = json_encode($data['listing']['opened_member_ships']);
//
//            $listing = UpdateOrCreateListing::run($data);
//
//            /*if ($listing->review_status === ListingReviewStatus::Approved && $listing->is_visible == 1 && $listing->is_closed == 0) {
//                $sendAlertMailsOnCreateJob = (new SendAlertMailsOnCreate($listing));
//                dispatch($sendAlertMailsOnCreateJob);
//            }*/
//
//            CreateListingWallets::run($listing);
//
//            return response()->json([
//                'id' => $listing->id
//            ], 201);
//        });
    }

    /**
     * update listing and its details
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $listing)
    {

        $listing = Listing::findOrFail($listing);

        $data = $this->validate(
            $request,
            array_merge(
                ListingRules::get($listing),
                ListingRules::getAdminListingRule($listing),
                ListingRules::getBankAccountIdRuleForAdmin($request),
                [
                    'listing.authorized_entity_id' => [
                        'required',
                        'integer',
                        Rule::exists(AuthorizedEntity::class, 'id')
                            ->where('status', AuthorizedEntityStatus::Approved)
                    ]
                ]
            )
        );

        $data['listing']['start_at'] = Carbon::parse($data['listing']['start_at'])->subHours(3);
        $data['listing']['early_investment_date'] = Carbon::parse($data['listing']['early_investment_date'])->subHours(3);
        $data['listing']['opened_member_ships'] = json_encode($data['listing']['opened_member_ships']);

        try {

            $dates = $this->getDatesForFlushCache($listing, $data, 'update');

            $listing = DB::transaction(function () use ($request, $listing, $data) {
                ValidateReservedSharesAreWithinLimit::run($listing, (int)$data['listing']['reserved_shares']);
                return UpdateOrCreateListing::run($data, $listing);
            });

            $this->dispatchJobsForFlushFundCache($listing->id, $dates);

            if ($listing->is_closed && $listing->invest_percent > 98)
                AfterCloseListingAction::run($listing);

            $this->cacheServices->updateFundCache($listing->id);

            return response()->json();

        }catch (\Exception $e) {
            $code = $this->logServices->log('UPDATE-LISTING-ADMIN-EX', $e);
            return response()->json(['message' => 'sorry please try later : ' . $code], 400);
        }
    }

    public function dispatchJobsForFlushFundCache($listingId, $dates)
    {
        foreach ($dates as $date) {
            $sendAlertMailsOnCreateJob = (new FlushCache($listingId));
            dispatch($sendAlertMailsOnCreateJob)->delay($date);
        }
    }

    public function getDatesForFlushCache($listing, $data, $type)
    {

        $dates = [];

        $keys = ['start_at', 'early_investment_date', 'deadline'];

        foreach ($keys as $key) {

            if (isset($data['listing'][$key]) && $data['listing'][$key]) {

                $date = Carbon::parse($data['listing'][$key]);

                if ($type == 'update') {

                    $dateInListing = $listing->$key ? Carbon::parse($listing->$key) : null;

                    if ($dateInListing && $date->equalTo($dateInListing)) {
                        continue;
                    }
                }

                if ($date->isAfter(now())) {
                    $dates[] = $date;
                }
            }
        }

        return $dates;
    }

}
