<?php

namespace App\Support\Anb\Http;

use Exception;

class HttpException extends Exception
{
    /**
     * Http instance response
     *
     * @var \Illuminate\Http\Client\Response
     */
    protected $response;

    /**
     * Undocumented function
     *
     * @param string $method
     * @param \Illuminate\Http\Client\Response $response
     */
    public function __construct($method, $response)
    {
        parent::__construct('Service unavailable');

        $this->response = $response;
        $this->method = $method;
    }

    public function context()
    {
        return [
            'status' => $this->response->status(),
            'method' => $this->method,
            'body' => $this->response->body()
        ];
    }

    public function getResponse()
    {
        return $this->response;
    }
}
