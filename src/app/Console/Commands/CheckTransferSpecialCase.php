<?php

namespace App\Console\Commands;

use App\Jobs\Anb\AnbEodStatement;
use App\Models\Anb\AnbTransfer;
use App\Models\Users\User;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Enums\AnbPaymentStatus;
use App\Support\Anb\Http\HttpException;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckTransferSpecialCase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:check-transfer-case';
    public $count;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get PAYMENT ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $data = AnbTransfer::query()->whereIn('transaction_id', [

                388098, 388097, 388099, 388101, 388107, 388110, 388111, 388114, 388115, 388120, 388121,
                388122, 388129, 388130, 388144, 388146, 388147, 388148, 388149, 388150, 388152, 388154,
                388155, 388157, 388158, 388159, 388160, 388161, 388162, 388163, 388166, 388168, 388169,
                388170, 388171, 388172, 388173, 388174, 388175, 388176, 388177, 388178, 388179, 388180,
                388181, 388182, 388183, 388184, 388185, 388188, 388191, 388192, 388194, 388195

            ])->select('id', 'transaction_id')->get();

            foreach ($data as $index => $anbTransfer) {

                try {

                    $this->count = DB::table('transactions')->where('id', $anbTransfer->transaction_id)->count();

                    $response = $this->getPayment($anbTransfer->id); // AnbApiUtil::newPayment($anbTransfer->id, 'sequence_number');

                    if (is_array($response)) {
                        dump($index . '-' .$response['status'] . ' anb id => ' . $anbTransfer->id . ' transaction => ' . $this->count);
                        continue;
                    }

                    if ($response->ok()) {

                        $responseData = $response->json();
                        dump($index . '-' .$responseData['status'] . ' anb id => ' . $anbTransfer->id . ' transaction => ' . $this->count);
                        continue;

                    } else {

                        $isDeleted = false;
                        $responseData = $response->json();
                        if ($this->count && isset($responseData['statusCode']) && $responseData['statusCode'] == 'E650002') {
                            $isDeleted = true;
                            DB::table('transactions')->where('id', $anbTransfer->transaction_id)->delete();
                        }

                        dump($index . ' - anb id => ' . $anbTransfer->id . ' payment not found ' . 'transaction => ' . $this->count . 'is deleted => ' . $isDeleted);

                    }

                } catch (\Exception $e) {
                    dd($e->getMessage());
                }
            }

            dd('done');

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }

    public function getPayment($transferId)
    {

        try {

            return AnbApiUtil::newPayment($transferId, 'sequence_number');

        } catch (HttpException $e) {
            return $e->getResponse();
        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }

}
