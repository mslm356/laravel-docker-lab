<?php

namespace App\Console\Commands\Users;

use App\Enums\Banking\VirtualAccountStatus;
use App\Enums\Banking\VirtualAccountType;
use App\Enums\Banking\WalletType;
use App\Enums\Investors\InvestorLevel;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Role as EnumsRole;
use App\Models\Users\User;
use App\Support\VirtualBanking\BankService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class CreateInvestor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investor:create {--email=} {--password=} {--first_name=} {--last_name=} {--phone=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create investor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->option('email');
        $phone = $this->option('phone');
        $first_name = $this->option('first_name') === null ? null : $this->option('first_name');
        $last_name = $this->option('last_name') === null ? null : $this->option('last_name');
        $passwordOption = $password = $this->option('password');

        if ($email === null) {
            return $this->error('Please enter an email address');
        }

        if ($phone === null) {
            return $this->error('Please enter an phone');
        }

        if ($password === null) {
            $password = Str::random();
        }


        $user = User::create([
            'email' => $email,
            'password' => Hash::make($password),
            'first_name' => $first_name ,
            'last_name' => $last_name,
            'phone_number' => phone($phone, "SA"),
            'locale' => App::getLocale()
        ]);

        $user->assignRole(
            Role::where([
                'name' => EnumsRole::Investor,
                'guard_name' => 'web'
            ])->first()
        );

        $user->investor()->create([
            'level' => InvestorLevel::Beginner,
            'is_kyc_completed' => false,
            'status' => InvestorStatus::PendingReview,
        ]);

        $wallet = $user->createWallet([
            'name' => WalletType::Investor,
            'slug' => WalletType::Investor,
            'uuid' => (string)Str::uuid(),
        ]);

        $account = (new BankService())->openVirtualAccount($user);

        $vaccount = $wallet->virtualAccounts()->create([
            'identifier' => $account['iban'],
            'status' => VirtualAccountStatus::Active,
            'type' => VirtualAccountType::Anb
        ]);


        if ($passwordOption === null) {
            $this->info("Password: {$password}");
        } else {
            $this->info('Admin created successfully');
        }
    }
}
