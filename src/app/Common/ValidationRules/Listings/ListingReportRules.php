<?php

namespace App\Common\ValidationRules\Listings;

class ListingReportRules
{
    static public function get()
    {
        return [
            'title_en' => ['required', 'string', 'max:200', textValidation() ],
            'title_ar' => ['required', 'string', 'max:200', textValidation() ],
            'description_en' => ['nullable', 'string', 'max:2000'],
            'description_ar' => ['nullable', 'string', 'max:2000'],
            'files' => ['nullable', 'array', 'min:1', 'max:10'],
            'files.*' => ['nullable','max:4096',
                'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/heic,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet/xlsx,application/vnd.ms-excel/xls,application/vnd.ms-word.document.macroenabled.12/doc,application/vnd.openxmlformats-officedocument.wordprocessingml.document/docx' /*'mimes:pdf,docx,xlsx,doc,xls,png,jpg,jpeg,gif'*/]
        ];
    }
}
