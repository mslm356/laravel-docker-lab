<?php

namespace App\Console\Commands\Urway;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Listings\Listing;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use Carbon\Carbon;
use Cknow\Money\Money;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class DepositByUrway extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'urway:deposit {--token=} {--p_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    public $urwayServices;
    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->urwayServices = new URwayServices();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return DB::transaction(function () {

            $token = InvestorSubscriptionToken::query()
                ->whereNotNull('authorization_expiry')
                ->whereNull('expired_at')
                ->where('reference', $this->option('token'))
                ->first();

            if (!$token) {
                dd('token not valid');
            }

            $listing = Listing::find($token->listing_id);

            if (!$listing) {
                dd('listing not valid');
            }

            $user = $token->user;

            if (!$user) {
                dd('user not valid');
            }

            $urwayTransaction = $token->urwayTransaction()->where('status', '!=', 'paid')->first();

            if (!$urwayTransaction) {
               dd('urway transaction not valid');
            }

            $toBeInvestedShares = $token->shares;

            $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing, $toBeInvestedShares);

            $total = $toBeInvestedAmountAndFees['total'];

            $userWallet = $user->getWallet(WalletType::Investor);

            $userWallet->deposit(
                $total->getAmount(),
                [
                    'reference' => transaction_ref(),
                    'reason' => TransactionReason::UrwayReason,
                    'details' => [
                        'version' => 1,
                        'source' => 'urway'
                    ],
                    'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
                ]
            );

            $userWallet->refreshBalance();

            $token->update([
                'expired_at' => now()
            ]);

            $urwayTransaction->status = 'paid';
            $urwayTransaction->pay_id = $this->option('p_id');
            $urwayTransaction->save();

        });

    }
}
