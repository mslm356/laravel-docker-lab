<?php

namespace App\Enums\Investors;

use BenSampo\Enum\Enum;

final class InvestorLevel extends Enum
{
    const Beginner = 1;
    const Professional = 2;
    const Company = 3;
}
