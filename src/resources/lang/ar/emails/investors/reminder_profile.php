<?php

return [
    'subject' => 'اكمل ملفك الشخصى',
    'message' => 'خطوة واحدة تفصلك عن الدخول إلى عالم الاستثمار',
    'user_text' => '  ننتظرك في منصة أصيل'.':name'.'مستثمرنا الكريم:  ',
    'link_text' => 'بادر بإكمال ملفك الشخصي لتبدأ معنا رحلة الاستثمار عبر الرابط التالي',
    'complete_profile' => 'اكمل ملفك الشخصى'
];
