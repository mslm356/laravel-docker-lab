<?php

namespace App\Http\Controllers\Investors\Profile;

use App\Enums\Banking\VirtualAccountStatus;
use App\Enums\Banking\VirtualAccountType;
use App\Enums\Banking\WalletType;
use App\Enums\CompanyProfile\ApproveRequest;
use App\Enums\Investors\InvestorLevel;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Role as EnumsRole;
use App\Http\Resources\Investors\CompanyProfileResource;
use App\Mail\companyProfile\Create;
use App\Models\CompanyProfile;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Rules\UniqueCompanyPhoneNumber;
use App\Support\VirtualBanking\BankService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class CompanyProfileController extends Controller
{
    public function store(Request $request)
    {
        $auth = $request->user('sanctum');

        if ($auth->companyProfiles()->whereIn('status', [ApproveRequest::Pending, ApproveRequest::InReview])->count()) {
            return validationResponse(422, __('sorry you already have pending request'));
        }

        if ($auth->type == 'company') {
            return validationResponse(422, __('sorry you can not create company'));
        }

        $rules = $this->rules();

        $requestData = $this->validate($request, $rules);

        try {

            DB::beginTransaction();

            $jsonData = json_encode($requestData);

            unset($requestData['owners'], $requestData['managers'], $requestData['registration_country'],
                $requestData['main_activity'], $requestData['employees_number'], $requestData['annual_turnover'],
                $requestData['phone_country_iso2']);

            $profileData = $requestData;

            $profileData['user_id'] = auth()->id();
            $profileData['data'] = $jsonData;
            $profileData['phone_number'] = phone($requestData['phone'], $request['phone_country_iso2']);
            $profileData['password'] = Hash::make($request['password']);

            unset($profileData['phone']);

            $companyProfile = CompanyProfile::create($profileData);

            $user = $this->create($companyProfile);

            $user->setRelation('investor', null);

            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($companyProfile->companyAccount)->queue(new Create());
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return validationResponse(422, __('sorry please try later'));
        }

        return response()->json();
    }

    public function rules()
    {
        $rules = [
            'name' => ['required', 'string', 'max:500', textValidation()],
            'commercial_number' => ['required', 'string', 'unique:company_profiles,commercial_number', textValidation()],
            'address' => ['required', 'string', 'max:500', textValidation()],
            'registration_country' => ['nullable', 'string', textValidation()],
            'main_activity' => ['required', 'string', 'max:500', textValidation()],
            'establishment_date' => ['required', 'date'],
            'company_phone' => ['nullable', 'string', 'max:20', textValidation()],
            'employees_number' => ['required', 'integer', 'min:1'],
            'annual_turnover' => ['nullable', 'numeric', 'min:1'],
            'owners' => ['required'],
            'owners.*.name' => ['required', 'string', 'max:500', textValidation()],

            'managers' => ['required'],
            'managers.*.name' => ['required', 'string', 'max:500', textValidation()],

//          STEP 2
            'phone' => ['required', 'string', 'phone:phone_country_iso2', new UniqueCompanyPhoneNumber('phone_country_iso2')],
            'phone_country_iso2' => ['required_with:phone', 'string', 'size:2'],

            'email' => ['required', 'email', 'unique:company_profiles,email', 'unique:users,email'],
            'password' => ['required', 'string', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/'],
            'account_name' => ['nullable', 'string', 'max:500', textValidation()],
            'account_number' => ['nullable', 'string', 'max:100', textValidation()],
            'custodian_name' => ['nullable', 'string', 'max:500', textValidation()],
            'custodian_address' => ['nullable', 'string', 'max:500', textValidation()],
        ];

        return $rules;
    }

    protected function create($companyProfile)
    {
        $creatorProfileData = $companyProfile->user->investor->toArray();

        $user = User::create([
            'email' => $companyProfile->email,
            'password' => $companyProfile->password,
            'first_name' => $companyProfile->name,
            'last_name' => ' ',
            'phone_number' => $companyProfile->phone_number,
            'locale' => $companyProfile->user ? $companyProfile->user->locale : App::getLocale(),
            'type' => 'company',
            'c_number' => $companyProfile->commercial_number,
        ]);

        $companyProfile->user_company_id = $user->id;
        $companyProfile->save();

        $user->assignRole(
            Role::where([
                'name' => EnumsRole::Investor,
                'guard_name' => 'web'
            ])->first()
        );

        $user->investor()->create([
            'level' => InvestorLevel::Beginner,
            'education_level' => $creatorProfileData['education_level'],
            'occupation' => $creatorProfileData['occupation'],
            'net_worth' => $creatorProfileData['net_worth'],
            'annual_income' => $creatorProfileData['annual_income'],
            'expected_annual_invest' => $creatorProfileData['expected_annual_invest'],
            'expected_invest_amount_per_opportunity' => $creatorProfileData['expected_invest_amount_per_opportunity'],
            'is_on_board' => $creatorProfileData['is_on_board'],
            'current_invest' => $creatorProfileData['current_invest'] ?? [],
            'invest_objective' => $creatorProfileData['invest_objective'] ?? [],
            'data->years_of_inv_in_securities' => (int)$creatorProfileData['data']['years_of_inv_in_securities'],
            'data->investment_xp' => (int)$creatorProfileData['data']['investment_xp'],
            'data->is_individual' => (int)$creatorProfileData['data']['is_individual'],
            'data->any_xp_in_fin_sector' => (bool)$creatorProfileData['data']['any_xp_in_fin_sector'],
            'data->any_xp_related_to_fin_sector' => (bool)$creatorProfileData['data']['any_xp_related_to_fin_sector'],
            'data->have_you_invested_in_real_estates' => (bool)$creatorProfileData['data']['have_you_invested_in_real_estates'],
            'data->is_entrusted_in_public_functions' => (bool)$creatorProfileData['data']['is_entrusted_in_public_functions'],
            'data->have_relationship_with_person_in_public_functions' => (bool)$creatorProfileData['data']['have_relationship_with_person_in_public_functions'],
            'data->is_beneficial_owner' => $isBeneficialOwner = (bool)$creatorProfileData['data']['is_beneficial_owner'],
            'data->identity_of_beneficial_owner' => $isBeneficialOwner === false ? $creatorProfileData['data']['identity_of_beneficial_owner'] : null,
            'data->extra_financial_information' => $creatorProfileData['data']['extra_financial_information'] ?? null,
            'is_kyc_completed' => true,
            'status' => InvestorStatus::PendingReview,
            'is_identity_verified' => 1,
        ]);

        $this->saveNationalIdentity($companyProfile);

        $wallet = $user->createWallet([
            'name' => WalletType::Investor,
            'slug' => WalletType::Investor,
            'uuid' => (string)Str::uuid(),
        ]);

        $account = (new BankService())->openVirtualAccount($user);

        $wallet->virtualAccounts()->create([
            'identifier' => $account['iban'],
            'status' => VirtualAccountStatus::Active,
            'type' => VirtualAccountType::Anb
        ]);

        return $user;
    }

    public function saveNationalIdentity($companyProfile)
    {

        $creatorNationalIdentityData = $companyProfile->user->nationalIdentity->toArray();

        $data = [
            'user_id' => $companyProfile->user_company_id,
            'nin_country_id' => $creatorNationalIdentityData['nin_country_id'],
            'source' => $creatorNationalIdentityData['source'],
            'nin' => $creatorNationalIdentityData['nin'],
            'first_name' => $creatorNationalIdentityData['first_name'],
            'second_name' => $creatorNationalIdentityData['second_name'],
            'third_name' => $creatorNationalIdentityData['third_name'],
            'forth_name' => $creatorNationalIdentityData['forth_name'],
            'id_expiry_date' => $creatorNationalIdentityData['id_expiry_date'],
            'birth_date_type' => $creatorNationalIdentityData['birth_date_type'],
            'birth_date' => $creatorNationalIdentityData['birth_date'],
            'gender' => $creatorNationalIdentityData['gender'],
            'nationality' => $creatorNationalIdentityData['nationality'],
            'type' => 'company',
        ];

        $identity = NationalIdentity::create($data);

        $addresses = $companyProfile->user->nationalIdentity->addresses->toArray();

        foreach ($addresses as $address) {
            unset($address['id']);
            $identity->addresses()->create($address);
        }

    }

    public function show(Request $request)
    {
        try {
            $auth = $request->user('sanctum');

            if ($auth->type != 'company') {

                return validationResponse(422, __('sorry user not valid'));
            }

            $companyProfile = $auth->companyProfile;

            return response()->json(new CompanyProfileResource($companyProfile), 200);

        } catch (\Exception $e) {
            return validationResponse(422, __('sorry please try later'));
        }
    }
}
