<?php


namespace App\Support\Zoho;


use App\Settings\InvestingSettings;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Item
{

    public $base_url;
    public $organization_id;
    public $auth;

    public function __construct()
    {
        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
    }

    public function prepareItemData($listing)
    {
        $feePercentage = app(InvestingSettings::class)->administrative_fees_percentage;

        $rate = $listing->share_price->multiply($feePercentage)->divide('100');

        $title = 'title_en';

        $data = [
            'name' => 'item - ' . $listing->$title,
            'rate' => $rate->formatByDecimal(),
            'product_type' => 'service',
        ];

        return $data;
    }

    public function store($listing)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->prepareItemData($listing);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'items?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                $listing->zoho_item_id = $responseData['item']['item_id'];
                $listing->save();

                return true;
            }

            Log::error('ZOHO-NEW-Item', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-NEW-Item', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }

    public function update($listing)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->prepareItemData($listing);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->put($this->base_url . 'items/' . $listing->zoho_item_id . '?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

//                $listing->zoho_item_id = $responseData['item']['item_id'];
//                $listing->save();

                return true;
            }

            Log::error('ZOHO-UPDATE-ITEM', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-UPDATE-ITEM', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return false;
    }

}
