<?php

use App\Models\WebsiteContent;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsiteContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_contents', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->longText('content');
            $table->timestamps();
        });

        $this->createSections();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_contents');
    }

    /**
     * Create new website content sections
     *
     * @return void
     */
    public function createSections()
    {
        WebsiteContent::create([
            'name' => 'about-us',
            'content' => [],
        ]);

        WebsiteContent::create([
            'name' => 'privacy-policy',
            'content' => [],
        ]);

        WebsiteContent::create([
            'name' => 'sharia-compliance',
            'content' => [],
        ]);

        WebsiteContent::create([
            'name' => 'terms-and-conditions',
            'content' => [],
        ]);

        WebsiteContent::create([
            'name' => 'code-of-conducts',
            'content' => [],
        ]);

        WebsiteContent::create([
            'name' => 'members-council',
            'content' => [],
        ]);

        WebsiteContent::create([
            'name' => 'disclosure-policy',
            'content' => [],
        ]);
    }
}
