<?php


namespace App\Services\Investors;

class TokenServices
{
    public function generateToken($user)
    {
        $token = $user->createToken($user->full_name);

        return $token->plainTextToken;
    }
}
