<?php

return [
    'subject' => 'Deposit Transaction',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'A transaction to your wallet has been made',
    'message_2' => ' Virtual IBAN  :iban',
    'message_3' => 'With a price of  :amount',
    'message_4' => 'The date and time of the operation  :time',
    'link_text' => 'To view your control panel, please visit the link',
    'view' => 'View dashboard',
];
