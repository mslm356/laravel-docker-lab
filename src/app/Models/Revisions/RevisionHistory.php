<?php

namespace App\Models\Revisions;

use App\Models\AppModel;
use App\Models\Users\User;
use Illuminate\Support\Str;
use App\Utils\Eloquent\HasVirtualColumn;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RevisionHistory extends AppModel
{
    use HasFactory, HasVirtualColumn;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'collection',
        'model_state' => 'collection'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($revision) {
            $revision->id = (string) Str::uuid();
        });
    }

    public function auditor()
    {
        return $this->belongsTo(User::class, 'auditor_id', 'id');
    }

    public static function getCustomColumns(): array
    {
        return [
            'id',
            'auditor_id',
            'revisionable_id',
            'revisionable_type',
            'created_at',
            'updated_at',
            'model_state'
        ];
    }
}
