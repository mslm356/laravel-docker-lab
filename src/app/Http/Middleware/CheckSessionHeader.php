<?php

namespace App\Http\Middleware;

use App\Models\Users\User;
use App\Services\LogService;
use App\Traits\SendMessage;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckSessionHeader
{
    use SendMessage;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try
        {


            if (!Auth::check() || in_array($request->header('Origin'),config('app.EXCEPT-DOMAINS')))
                return $next($request);

            $request_header_email=decrypt($request->header('Session-Key'));
        }catch (\Exception $e)
        {
            $request_header_email= null;

            $log_service=new LogService();
            $log_service->log('ENCRUPT-SESSION-KEY-PROBLEM',null,['sessionKey'=>$request->header('Session-Key'),'err_msg'=>$e->getMessage()]);
        }


        if (strtolower($request->user()->email) != strtolower($request_header_email))
        {
            $this->logProblem($request,$request_header_email);
            return response()->json(['message'=>'Unauthenticated.'], 401);
        }




        // Get the response from the next middleware or controller
        $response = $next($request);

        // Add the custom header to the response

//        if (Auth::check())
//            $response->header('Session-Key', encrypt($request->user()->email));

        return $response;

    }


    function logProblem($request,$request_header_email)
    {

        $user=$request->user();

        $log_service=new LogService();
        $data_session_data=[];

        try {
            $data_session_data['prev_url']=$request->session()->get('_previous')['url'];
            $data_session_data['session_id']=$request->session()->getId();

        }catch (\Exception $e)
        {
            $code=$log_service->log('PROBLEM-GET-SESSION',$e);

        }

        try {
            $data=array_merge([
                'auth_user_email'=>$user->email,
                'requested_email'=>($request_header_email !=null)?$request_header_email:'null',
                'called_endpoint'=>request()->route()->uri,
            ],$data_session_data);

            $code=$log_service->log('LOGIN-PROBLEM-INITIAL',null,$data);




            $request_header_user=User::where('email',$request_header_email)->first();

            $user->tokens->each(function ($token, $key) {
                $token->delete();
            });

            $request_header_user->tokens->each(function ($token, $key) {
                $token->delete();
            });


            $this->sendAlertMsg($code,$request_header_email);


        }catch (\Exception $e)
        {
            $log_service->log('LOGIN-PROBLEM-EXECPTION',$e);
        }

        \auth('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->json(['message'=>'Unauthenticated.'], 401);
    }

    function sendAlertMsg($code,$request_header_email)
    {

        $message='review graylog code = '.$code . ' reason';
        $message .= ($request_header_email != null)?' email is'.$request_header_email :'is normal case';

        $this->sendOtpMessage(config('app.ENG_MO_PHONE'), $message);

    }

}
