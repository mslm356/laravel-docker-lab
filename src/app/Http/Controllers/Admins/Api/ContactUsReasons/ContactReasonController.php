<?php

namespace App\Http\Controllers\Admins\Api\ContactUsReasons;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\ContactReasonResource;
use App\Models\ContactReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ContactReasonController extends Controller
{
    /**
     * Get all the contact us reasons
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return ContactReasonResource::collection(ContactReason::all());
    }

    /**
     * Show the stored contact reason
     *
     * @param \App\Models\PropertyType $propertyType
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ContactReason $contactReason)
    {
        return new ContactReasonResource($contactReason);
    }

    /**
     * Store the type in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $data = $this->validator($request->all())->validate();

            $reason = ContactReason::create($data);

            return new ContactReasonResource($reason);
        });
    }

    /**
     * Update the given reason in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ContactReason $contactReason
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, ContactReason $contactReason)
    {
        return DB::transaction(function () use ($request, $contactReason) {
            $data = $this->validator($request->all())->validate();

            $contactReason->update($data);

            return new ContactReasonResource($contactReason);
        });
    }

    /**
     * The reason validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, [
            'name_en' => ['required', 'string', 'max:50',textValidation()],
            'name_ar' => ['required', 'string', 'max:50',textValidation()],
            'is_active' => ['required', 'boolean']
        ]);
    }
}
