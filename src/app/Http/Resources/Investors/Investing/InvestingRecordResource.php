<?php

namespace App\Http\Resources\Investors\Investing;

use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Jobs\Zatca\GenerateEInvoiceAfterInvesting;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;
use phpDocumentor\Reflection\Types\This;

class InvestingRecordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $guard = $request->guard;

        return [
            'id' => $this->id,
            'listing' => $this->whenLoaded('listing', fn () => $this->listing->title),
            'amount_without_fees' => number_format($this->amount_without_fees->formatByDecimal(), 2),
            'vat_amount' => number_format($this->vat_amount->formatByDecimal(), 2),
            'admin_fees' => number_format($this->admin_fees->formatByDecimal(), 2),
           'zatca_invoice_link'=>null,
            'zatca_invoice'=>null,
            'subscription_form'=>null,
            'subscription_form_link' => $this->subscriptionFormLink($this->id, $guard),
            'created_at' => ($this->created_at)?$this->created_at->timezone('Asia/Riyadh')->format('Y-m-d h:i:s A'):null,  //->toDateTimeString(),
        ];
    }

    public function subscriptionFormLink($investor_record_id, $guard)
    {
        $investor_record_id=Crypt::encryptString($investor_record_id);
        return $guard == 'api' ? url('api/app/files/investor-record/' . $investor_record_id . '/get-url') : route('investor.investor-files.get.url', ['investor_record_id'=> $investor_record_id]);
    }

    public function zatcaInvoiceLink($zatcaInvoice, $guard)
    {

        if (!$zatcaInvoice || !$zatcaInvoice->pdfFile) {
            return null;
        }

        $pdfFile = $zatcaInvoice->pdfFile;

        return $guard == 'api' ? url('api/app/files/' . $pdfFile->id . '/get-url') : route('investor.files.get.url', ['file' => $pdfFile->id]) ;
    }
}
