<?php

namespace App\Http\Controllers\MobileApp\Investors\HyperPay;


use App\Enums\HyperPay\HyperPayStatus;
use App\Http\Controllers\Controller;
use App\Services\HyperPay\HyperPayService;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Validation\ValidationException;

class HyperPayController extends Controller
{

    public $logService;
    public $hyperPayService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->hyperPayService = new HyperPayService();
    }

    public function checkout(Request $request)
    {
        try {

            $rules = $this->hyperPayService->checkoutValidation();

            $data = Validator::make($request->all(), $rules)->validate();

            $guard = $request->guard ?? 'web';

            $user = getAuthUser($guard);

            $hyperPayTransaction = $this->hyperPayService->HandleCheckout($user, $data);
            $response = json_decode($hyperPayTransaction->checkout_response, 1);

            $returnedData = [
                'checkout_id' => $hyperPayTransaction->checkout_id,
                'buildNumber' => isset($response['buildNumber']) ? $response['buildNumber'] : null,
                'amount' => money($hyperPayTransaction->amount, defaultCurrency())->formatByDecimal(),
            ];

            $regex = "/^(000\.000\.|000\.200\.1|100\.[36])/";

            if ($this->hyperPayService->checkPaymentStatusResponse($response, $regex)) {
                return apiResponse(200, __("checkout done success"), $returnedData);
            }

            if (in_array($response['result']['code'], $this->hyperPayService->hyperPayStatusArray())) {
                return apiResponse(400, __("hyperpay/message." . $response['result']['code']));
            }

            return apiResponse(400, __('mobile_app/message.payment_failed_please_try_again_later'));


        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-HYPER-PAY-CHECKOUT', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $this->logService->log('HYPER-PAY-TRANSACTIONS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

    }

    public function getPaymentStatus(Request $request)
    {

        try {

            $rules = $this->hyperPayService->getPaymentStatusValidation();

            $data = Validator::make($request->all(), $rules)->validate();

            $guard = $request->guard ?? 'web';

            $user = getAuthUser($guard);

            $paymentStatus = $this->hyperPayService->HandleGetPaymentStatus($user, $data);

            if ($paymentStatus['status'] == HyperPayStatus::Paid) {
                return apiResponse(200, __("payment done successfully"));
            }

            if (in_array($paymentStatus['statusCode'], $this->hyperPayService->hyperPayStatusArray())) {
                return apiResponse(400, __("hyperpay/message." . $paymentStatus['statusCode']));
            }

            return apiResponse(400, __('mobile_app/message.payment_failed_please_try_again_later'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-HYPER-PAY-PAYMENT-STATUS', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $this->logService->log('HYPER-PAY-TRANSACTIONS-PAYMENT-STATUS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

    }

    public function getPaymentMethods(Request $request)
    {
        $methods = $this->hyperPayService->paymentApiMethods();
        return apiResponse(200, 'done success', $methods);
    }
}
