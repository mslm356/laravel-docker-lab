<?php

namespace App\Console\Commands\Funds;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Banking\TransactionReason;
use App\Enums\Notifications\NotificationTypes;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Services\MemberShip\CreateRequest;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class CancelListingInvesting extends Command
{
    use NotificationServices;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listing-investing:cancel {--listing_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cancel listing investing cmd';

    public $createRequest;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->createRequest = new CreateRequest();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        if (!$this->option('listing_id')) {
            dd('listing_id required');
        }



        $this->calculateAllAmountBefore();

        DB::table('listing_investor')
            ->where('listing_id', $this->option('listing_id'))
            ->where('invested_amount', '>', 0)

            ->join('users', function ($join) {
                $join->on('users.id', '=', 'listing_investor.investor_id');
            })
            ->join('listings', function ($join) {
                $join->on('listings.id', '=', 'listing_investor.listing_id');
            })
            ->join('wallets', function ($join) {
                $join->on('wallets.holder_id', '=', 'listing_investor.investor_id')
                    ->where('wallets.holder_type', User::class);
            })
            ->select(
                [
                    'listing_investor.id as listing_investor_id', 'listing_investor.listing_id', 'listing_investor.investor_id',
                    'listing_investor.invested_shares', 'listing_investor.invested_amount', 'users.email', 'users.locale', 'users.member_ship',
                    DB::raw('concat(users.first_name, " ", users.last_name) as full_name'),
                    DB::raw('listings.target/listings.total_shares as share_price'),
                    'listings.title_en', 'listings.title_ar', 'wallets.id as wallet_id',
                    'listings.administrative_fees_percentage', 'listings.vat_percentage'
                ])
            ->orderBy('listing_investor.id')
            ->lazy()
            ->each(function ($listingInvesting) {

                $listingInvesting->share_price = money($listingInvesting->share_price, defaultCurrency());

                $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listingInvesting, $listingInvesting->invested_shares);

                DB::transaction(function ($q) use ($listingInvesting, $toBeInvestedAmountAndFees) {

                    $this->saveTransactions($listingInvesting, $toBeInvestedAmountAndFees);

                    DB::table('listing_investor')
                        ->where('id', $listingInvesting->listing_investor_id)
                        ->update(['invested_shares' => 0, 'invested_amount' => 0, 'updated_at' => Carbon::now()]);

                    $this->createRequest->userCompensationWithGoldMembership($listingInvesting);

                }, 1);

                $total = $toBeInvestedAmountAndFees['total']->getAmount();
                $time = now('Asia/Riyadh')->format('Y-m-d H:i:s');

//                Mail::to($listingInvesting->email)->queue(new \App\Mail\Funds\CancelListingInvesting($listingInvesting, $total, $time));
//
//                $notificationData = $this->notificationData($listingInvesting, $total, $time);
//                $this->send(NotificationTypes::CancelListingInvesting, $notificationData, $listingInvesting->investor_id);

            });

        $this->calculateAllAmountAfter();

    }

    public function saveTransactions($listingInvesting, $toBeInvestedAmountAndFees)
    {

        $meta = [
            'reason' => TransactionReason::CommonReason,
            'reference' => transaction_ref(),
            'value_date' => now('UTC')->toDateTimeString(),
            'listing_id' => $listingInvesting->listing_id,
            'listing_title_en' => $listingInvesting->title_en,
            'listing_title_ar' => $listingInvesting->title_ar,
            'user_id' => $listingInvesting->investor_id,
            'source' => 'cancel_listing_investing_cmd',
            'user_name' => $listingInvesting->full_name,
            'reason_ar' => ' استرداد '.$listingInvesting->invested_shares.' وحدة في '.$listingInvesting->title_ar.' ( شامل قيمة الاشتراك وضريبة القيمة المضافة) ',
            'reason_en' => 'Refund '.$listingInvesting->invested_shares.' units in '.$listingInvesting->title_en.' ( including administration and VAT fees )',
        ];

//        DB::table('transactions')->insert([
//            'payable_type' => Listing::class,
//            'payable_id' => $payout->listing_id,
//            'wallet_id' => $wallet->id,
//            'type' => 'withdraw',
//            'amount' => ($paid * -1),
//            'confirmed' => 1,
//            'meta' => json_encode($meta),
//            'uuid' => Str::uuid(),
//            'created_at' => now(),
//            'updated_at' => now(),
//        ]);

        $total = $toBeInvestedAmountAndFees['total']->getAmount();

        DB::table('transactions')->insert([
            'payable_type' => User::class,
            'payable_id' => $listingInvesting->investor_id,
            'wallet_id' => $listingInvesting->wallet_id,
            'type' => 'deposit',
            'amount' => ($total),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $investorBalance = DB::table('transactions')
            ->where('payable_id', $listingInvesting->investor_id)
            ->where('payable_type', User::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('holder_id', $listingInvesting->investor_id)
            ->where('holder_type', User::class)
            ->update(['balance' => $investorBalance]);

        var_dump( 'invested amount  => ' . $total . ' for user id => ' . $listingInvesting->investor_id . ' --------------------------------');
    }

    public function notificationData($listingInvesting, $total, $time)
    {

        $name = $listingInvesting->full_name;
        $content_ar_1 = '  مستثمرنا العزيز:  ' . $name;

        $content_ar_2 = ' تم استرداد مبلغ وقدره: ' . $total;
        $content_ar_3 = ' (شامل قيمة الاشتراك وضريبة القيمة المضافة) ';
        $content_ar_4 = '  عدد الوحدات:  ' . $listingInvesting->invested_shares . ' ';
        $content_ar_5 = 'وذلك بسبب إنهاء '.$listingInvesting->title_ar.' ، حسب إفادة مدير الصندوق الخير كابيتال.';

        return [
            'action_id' => $listingInvesting->investor_id,
            'channel' => 'mobile',
            'title_en' => $listingInvesting->title_en.' investment amount refund',
            'title_ar' => ' استرداد مبلغ الاستثمار في '.$listingInvesting->title_ar,
            'content_en' => ' Dear Investor ' . $name . ' The investment amount has been refunded : ' . $total
                . '( including administration and VAT fees ) for '.$listingInvesting->invested_shares .'units Due to '.$listingInvesting->title_en.' being terminated,
                    according to the fund manager Alkhair Capital Saudi Arabia',
            'content_ar' => $content_ar_1 . $content_ar_2 . $content_ar_3 .  $content_ar_4 . $content_ar_5,
        ];
    }


    function calculateAllAmountBefore()
    {
        $listing_investor=DB::table('listing_investor')
            ->join('listings', function ($join) {
                $join->on('listings.id', '=', 'listing_investor.listing_id');
            })
            ->where('listing_investor.listing_id',$this->option('listing_id'))
            ->groupBy('listing_investor.listing_id')
            ->select(
                [
                    'listings.administrative_fees_percentage',
                    'listings.vat_percentage',
                    DB::raw('listings.target/listings.total_shares as share_price'),
                    DB::raw('SUM(listing_investor.invested_shares) as total_invested_shares'),
                ])->first();

        $listing_investor->share_price = money($listing_investor->share_price, defaultCurrency());

        $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing_investor, $listing_investor->total_invested_shares);

        echo  "total amount before operation ".$toBeInvestedAmountAndFees['total']->formatByDecimal()."\n\n";
        echo  "total invested_shares before operation ".$listing_investor->total_invested_shares ."\n\n";

    }

    function calculateAllAmountAfter()
    {
        $transactionAmount = DB::table('transactions')
            ->where('reason', 11)
            ->whereJsonContains('meta->listing_id', intval($this->option('listing_id')))
            ->sum('amount');

        echo 'total of transaction after operation done '. $transactionAmount."\n\n";
    }

}
