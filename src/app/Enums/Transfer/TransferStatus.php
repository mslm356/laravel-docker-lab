<?php

namespace App\Enums\Transfer;

use BenSampo\Enum\Enum;

final class TransferStatus extends Enum
{
    const Pending = 1;
    const InReview = 2;
    const Approved = 3;
    const Rejected = 4;
    const Refund = 5;

}
