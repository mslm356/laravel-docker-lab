<?php

namespace App\Exports\AnbEodStatements;

use App\Models\Anb\AnbEodStatement;
use App\Models\Anb\AnbEodTransaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AnbEodStatementTransactionsExport implements FromCollection, WithHeadings, WithMapping
{
    protected $eodStatement;

    public function __construct(AnbEodStatement $eodStatement)
    {
        $this->eodStatement = $eodStatement;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return AnbEodTransaction::where(
            'statement_id',
            $this->eodStatement->id
        )
            ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Customer Reference',
            'Bank Reference',
            'Description',
            'Code',
            'Mark',
            'Amount',
            'Value Date',
        ];
    }

    public function map($transaction): array
    {
        return [
            $transaction->id,
            $transaction->customer_ref,
            $transaction->bank_ref,
            $transaction->description,
            $transaction->code,
            $transaction->mark,
            $transaction->amount,
            $transaction->value_date->toDateString(),
        ];
    }
}
