<div class="relative w-full bg-left bg-no-repeat bg-cover bg-primary-500">
    <div class="absolute z-0 w-full h-full">
        <img class="hidden object-cover w-full h-full lg:block"
            src="{{ asset('images/ASEEL-WEB-01-B_' . App::getLocale() . '.webp') }}" alt="">
        <img class="object-cover w-full h-full lg:hidden" src="{{ asset('images/ASEEL-WEB-01-B_mobile.png') }}"
            alt="">
    </div>
    <div class="relative z-10 lg:px-10">
        @include('website.welcome-subviews.navbar')
        <main class="px-3 md:pt-24 lg:py-40 xl:py-72">
            <div class="h-full lg:flex lg:items-center">
                <div class="px-4 ltr:space-y-8 rtl:space-y-12 sm:text-center lg:ltr:text-left lg:rtl:text-right sm:px-0 lg:w-1/2">
                    <div>
                        <h2 class="mb-3 text-3xl leading-none text-primary-inv-500 sm:text-4xl xl:text-6xl">
                            {{ __('website.be_part_of_it_l1') }}
                        </h2>
                        <h2 class="text-4xl leading-none text-primary-inv-500 sm:text-5xl xl:text-7xl ltr:-mt-4">
                            {{ __('website.be_part_of_it_l2') }}
                        </h2>
                    </div>
                    <p class="text-lg leading-normal text-primary-inv-500 md:text-2xl xl:text-3xl lg:w-7/12">
                        {{ __('website.invest_in_real_estate') }}</p>

                    <div>
                        <div class="flex flex-col sm:flex-row sm:items-center sm:justify-center lg:justify-start">
                            @if (!Auth::check())
                                <div>
                                    <a href="{{ investor_url('sign-up') }}"
                                        class="inline-flex px-2.5 py-1.5 text-xl transition-all duration-300 ease-in-out rounded-lg shadow-lg bg-primary-inv-500 text-primary-900 md:text-2xl md:py-2 md:px-6">{{ __('website.become_an_investor') }}</a>
                                </div>
                            @endif
                            <div class="flex items-center mt-5 text-xs text-primary-inv-500 md:text-sm sm:mt-0">
                                <a href="#howItWorks"
                                    class="inline-flex md:py-2 border-b border-primary-inv-500 {{ Auth::check() ? '' : 'sm:ltr:ml-3 sm:rtl:mr-3' }}">{{ __('website.how_it_works') }}
                                </a>
                                <div class="ltr:ml-2 rtl:mr-2">></div>
                            </div>
                        </div>

                        <div class="w-1/2 md:w-1/3 sm:mx-auto lg:mx-0">
                            <p class="mt-4 text-xs leading-normal text-gray-400 md:my-5">
                                {{ __('website.cme_licensed') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="lg:hidden">
                    <img class="w-56 md:w-96 ltr:ml-auto rtl:mr-auto"
                        src="{{ asset('images/ASEEL-WEB-03_' . App::getLocale() . '.png') }}">
                </div>
            </div>
        </main>
    </div>
</div>
