<?php

namespace App\Actions\AuthorizedEntities;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdateUserInEntity
{
    use AsAction;

    /**
     * Update user under the entity
     *
     * @param \App\Models\Users\User $user
     * @param array $inputs
     * @return \App\Models\Users\User
     */
    public function handle($user, $data)
    {
        $user->update(
            Arr::only(
                $data,
                ['first_name', 'last_name', 'email']
            ) + [
                'phone_number' => phone(Arr::get($data, 'phone'), Arr::get($data, 'phone_country_code'))->formatE164()
            ]
        );

        return $user;
    }
}
