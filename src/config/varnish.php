<?php

return [
    /*
     * The hostname(s) this Laravel app is listening to.
     */
//    'host' => ['10.0.0.210'],
//    'host' => ['api-pre.investaseel.com'],
    'host' => ['10.0.3.158'],

    /*
     * The location of the file containing the administrative password.
     */
    'administrative_secret' => '/etc/varnish/secret',

    /*
     * The port where the administrative tasks may be sent to.
     */
    'administrative_port' => 6082,

    /*
     * The default amount of minutes that content rendered using the `CacheWithVarnish`
     * middleware should be cached.
     */
    'cache_time_in_minutes' => 60 * 24,

    /*
     * The name of the header that triggers Varnish to cache the response.
     */
    'cacheable_header_name' => 'X-Cacheable',

    'varnish_ip' => env('VARNISH_IP', "20.0.2.173:6082"),
    'varnish_host' => env('VARNISH_HOST', "api.investaseel.sa"),

];
