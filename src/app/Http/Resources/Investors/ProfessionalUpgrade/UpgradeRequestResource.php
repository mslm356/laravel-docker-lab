<?php

namespace App\Http\Resources\Investors\ProfessionalUpgrade;

use App\Http\Resources\Common\Investors\ProfessionalUpgrade\UpgradeRequestResource as CommonUpgradeRequestResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UpgradeRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonUpgradeRequestResource($this->resource))->toArray($request),
            [
                'items' => UpgradeRequestItemResource::collection($this->items),
                'reviewer_comment' => $this->reviewer_comment,
            ]
        );
    }
}
