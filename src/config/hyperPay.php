<?php

if (config('app.hyperPay_mode') == 'fake') {

    return [

        /*
         |--------------------------------------------------------------------------
         | HYPER PAY INTEGRATION TESTING
         |--------------------------------------------------------------------------
         */

        'apple_pay_entity' => env('APPLE_PAY_ENTITY_ID', '8ac7a4c78abc7226018ac2c6fd850422'),
        'visa_entity' => env('VISA_ENTITY_ID','8ac7a4c78abc7226018ac2c64715041e'),
        'base_url' => env( 'HYPER_PAY_BASE_URL','https://test.oppwa.com/v1/'),
        'password' => env('HYPER_PAY_PASSWORD','OGFjN2E0Yzc4YWJjNzIyNjAxOGFjMmM1YjYzZDA0MWF8Sm1BYk0zQTlwelh4MnI0Wg=='),
    ];

} else {

    return [

        /*
         |--------------------------------------------------------------------------
         | HYPER PAY INTEGRATION PRODUCTION
         |--------------------------------------------------------------------------
         */

        'apple_pay_entity' => env('APPLE_PAY_ENTITY_ID'),
        'visa_entity' => env('VISA_ENTITY_ID'),
        'base_url' => env( 'HYPER_PAY_BASE_URL','https://eu-prod.oppwa.com/v1/'),
        'password' => env('HYPER_PAY_PASSWORD'),
    ];


}




