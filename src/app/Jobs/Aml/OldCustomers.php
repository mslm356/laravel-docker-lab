<?php

namespace App\Jobs\Aml;

use App\Enums\Role;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Aml\AmlServices;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OldCustomers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationServices;

    public $amlServices;
    public $logServices;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->amlServices = new AmlServices();
        $this->logServices = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            User::role(Role::Investor)
                ->join('investor_profiles', function ($join) {
                    $join->on('users.id', '=', 'investor_profiles.user_id')
                        ->where('investor_profiles.is_kyc_completed', true);
                })
                ->lazyById()
                ->each(function ($user) {

                    $data = [
                        'type' => 'screen',
                        'user' => $user
                    ];

                    $this->amlServices->handle($data);
                });

        } catch (\Exception $e) {
            $this->logServices->log('AML-JOB-EXCEPTION', $e);
        }

    }
}
