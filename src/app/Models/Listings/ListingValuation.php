<?php

namespace App\Models\Listings;

use App\Casts\Money\MoneyCast;
use App\Models\AppModel;

class ListingValuation extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime',
        'value' => MoneyCast::class . ':currency',
    ];

    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }
}
