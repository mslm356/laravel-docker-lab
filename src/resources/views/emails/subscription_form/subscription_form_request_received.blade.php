@extends('emails.partials.main')
@section('content')
    <tr> You have received Account statement request {{$subscriptionFormRequest->id}} </tr>
    <br><br>
    <tr> At {{ \Carbon\Carbon::parse($subscriptionFormRequest->created_at)->timezone('Asia/Riyadh')->format('y-m-d h:i:s')}} </tr>
    <br><br>
    <tr> {{ __('emails/common.thanks', [], 'en') }} </tr>
    <br>
@endsection


