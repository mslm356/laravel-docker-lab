<?php

namespace App\Jobs\Zoho;

use App\Enums\Banking\WalletType;
use App\Models\Users\User;
use App\Support\Zoho\Customer;
use App\Support\Zoho\CustomerPayments;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateOldUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $zohoCustomer;
    public $customerPayment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->zohoCustomer = new Customer();
        $this->customerPayment = new CustomerPayments();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        User::query()
            ->lazyById()
            ->each(function ($user) {

                if (!$user->zoho_id) {

                    $this->zohoCustomer->saveCustomer($user, \App\Enums\Zoho\NewCustomer::Customer);

                    $wallet = $user->getWallet(WalletType::Investor);

                    if ($wallet && $wallet->balance != 0) {
                        $this->customerPayment->createCustomerPayment($user, money($wallet->balance)->formatByDecimal());
                    }
                }
            });
    }
}
