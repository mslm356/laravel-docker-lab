<?php


namespace App\Services\Investors;

use App\Enums\FileType;
use App\Enums\Investors\ProfileReviewType;
use App\Enums\Investors\UpgradeRequestStatus;
use App\Mail\ProfessionalRequest\UpgradeRequest;
use App\Models\File;
use App\Models\Investors\InvestorUpgradeItem;
use App\Models\Investors\InvestorUpgradeRequest;
use App\Models\Investors\InvestorUpgradeRequestItem;
use App\Services\Files\CheckFile;
use App\Services\LogService;
use App\Traits\SendMessage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UpgradeProfileServices
{
    use SendMessage;

    public $logService;
    public $checkFile;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->checkFile = new CheckFile();

    }

    public function store($data, $user)
    {
        try {

            return DB::transaction(function () use ($data, $user) {

                $upgradeRequest = InvestorUpgradeRequest::create([
                    'user_id' => $user->id,
                    'status' => UpgradeRequestStatus::PendingReview,
                    'type' => $user->type,
                ]);

                foreach ($data['items'] as $item) {
                    $upgradeRequestItem = InvestorUpgradeRequestItem::create([
                        'item_id' => $item['id'],
                        'request_id' => $upgradeRequest->id,
                        'type' => $user->type,
                    ]);

                    foreach ($item['files'] ?? [] as $file) {

                        $this->checkFile->handle($file, null);

                        File::upload($file, "investors/professional-upgrade/{$upgradeRequest->user_id}", [
                            'fileable_id' => $upgradeRequestItem->id,
                            'fileable_type' => $upgradeRequestItem->getMorphClass(),
                            'type' => FileType::InvestorUpgradeProof,
                        ]);
                    }
                }

                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new UpgradeRequest($user));
                }

                $this->sendSmsData($user);

                return true;
            });

        } catch (ValidationException $e) {
            $code = $this->logService->log('CREATE-UPGRADE-PROFILE-REQUEST-VALIDATION', $e);
            return response()->json(['message' => $e->getMessage() . ' : ' . $code, 'errors' => $e->errors()], 400);
        } catch (\Exception $e) {
            $code = $this->logService->log('CREATE-UPGRADE-PROFILE-REQUEST-WEB', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }

    public function sendSmsData($user)
    {
        $name = $user ? $user->full_name : '---';
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_en = 'Your request to upgrade your account on Aseel\'s platform to (a professional investor) has been received, and the team will notify you once it is reviewed. Thank you for your dealings with Aseel To access your Dashboard, please click on the link';
        $content_ar =  'تم تلقي طلبكم لترقية حسابكم في منصة أصيل إلى (مستثمر محترف)، سوف يتم تبليغكم في حال الرد من قبل الفريق المختص. شكراً لتعاملكم مع منصة أصيل'   ;

        $content_ar = $welcome_message_ar . $content_ar;
        $content_en = "Our dear investor " . $name  . $content_en;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }

    public function getRules()
    {
        return [
            'items' => ['required', 'array', 'min:1'],
            'items.*' => ['required', 'array', 'size:2'],
            'items.*.id' => ['required', 'integer', Rule::exists(InvestorUpgradeItem::class, 'id')->where('is_active', true)],
            'items.*.files' => ['required', 'array', 'min:1', 'max:7'], // todo check php.ini
            'items.*.files.*' => ['required'/*,'clamav'*/, 'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/heic', 'max:4096'],
        ];
    }

    public function updatePhone($oldPhoneNumber, $user)
    {
        $updatedPhone = substr($oldPhoneNumber, 4);
        $updatedPhone = "1" . $updatedPhone;
        $newPhoneNumber = phone($updatedPhone, 'EG');
        $user->update(["phone_number" => $newPhoneNumber]);
        return true;
    }

    public function updateNationality($nin, $user)
    {
        $newNinNumber = substr_replace($nin, 9, 0, 1);
        optional($user->nationalIdentity)->update(["nin" => $newNinNumber]);
        return true;
    }

    public function updateEmail($email, $user)
    {
        $user->update(["email" => $email . rand(0, 100000)]);
        return true;
    }

    public function updateStatus($user)
    {
        $user->investor->update(["status" => 8]);
        return true;
    }

    public function updateReviewerId($user, $authUser)
    {
        $user->investor->update([
            "reviewer_id" => $authUser->id,
            "review_type" => ProfileReviewType::DeleteAccount
        ]);
        return true;
    }
}
