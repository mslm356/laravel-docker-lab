<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintType extends Model
{
    use HasFactory;

    protected $table = 'complaint_types';


    public function getNameAttribute()
    {
        $name = app()->getLocale() . '_name';

        return $this->{$name};
    }
}
