<?php


namespace App\Services\Anb;

use App\Services\LogService;
use App\Support\Banking\MT940\AnbEodBalance;
use App\Support\Banking\MT940\AnbEodParser;
use App\Support\Banking\MT940\AnbEodTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Jejik\MT940\Reader;

class SpecialEodMt940Service
{
    public $logServices;
    public $bankBalanceService;

    public function __construct()
    {
        $this->logServices = new LogService();
        $this->bankBalanceService = new BankBalanceService();
    }

    public function readStatementTransactions($eodStatement)
    {

        $reader = new Reader();
        $reader->setTransactionClass(AnbEodTransaction::class);
        $reader->setOpeningBalanceClass(AnbEodBalance::class);
        $reader->setClosingBalanceClass(AnbEodBalance::class);
        $parser = new AnbEodParser($reader);
        $statements = $parser->parse($eodStatement['data']);

        $markCCount = 0;
        $markDCount = 0;

        foreach ($statements as $statement) {

            /** @var AnbEodTransaction $eodTransaction */
            foreach ($statement->getTransactions() as $eodTransaction) {

                $mark = $eodTransaction->getMark();

                if ($mark === 'C') {
                    $markCCount++;
                } else {
                    $markDCount++;
                }
            }
        }

        return ['C' => $markCCount, 'D' => $markDCount];
    }

    public function getTransactionData61($transaction, $dateFormat, $date, $meta)
    {

        $valueDate = $dateFormat;
        $entryDate = $date->format('m') . $date->format('d');
        $mark = $transaction->reason == 1 ? 'C' : 'D';
        $amount = number_format(str_replace('-', '', $transaction->amount / 100), 2, ',', '');
        $transactionType = 'NTRF';

        if ($transaction->reason == 1) {

            $customerReference = isset($meta['details']['data']['narrative']['narr2']) ? $meta['details']['data']['narrative']['narr2'] : null;
            $bankReference = isset($meta['details']['data']['narrative']['narr1']) ? '//' . $meta['details']['data']['narrative']['narr1'] : null;

        } else {

            $anbTransfer = DB::table('anb_transfers')
                ->where('transaction_id', $transaction->id)
                ->select('id', 'external_id', 'external_trans_reference')
                ->first();

            $customerReference = $anbTransfer ? 'AS:' . $anbTransfer->id : null;
            $bankReference = $anbTransfer ? '//' . $anbTransfer->external_trans_reference : null;
        }

        return ':61:' . $valueDate . $entryDate . $mark . $amount . $transactionType . $customerReference . $bankReference . '\\n';
    }

    public function getTransactionData86($transaction, $meta)
    {

        if ($transaction->reason == 1) {

            $t86 = $this->getTransactionDescription86InDeposit($meta);

        } else {
            $t86 = ':86:' . '\\n';
        }

        return $t86;
    }

    public function stmtDefaultData($dateFormat)
    {

        $s20 = ':20:RPM-' . $dateFormat . randomDigitsStr('6') . '\\n';
        $s25 = ':25:' . config('services.anb.account_number') . '\\n';
        $s28c = ':28c:176/001' . '\\n';
        $s60f = ':60f:C' . $dateFormat . 'SAR0,00' . '\\n';

        return $s20 . $s25 . $s28c . $s60f;
    }

    public function getTransactionDescription86InDeposit($meta)
    {

        $bank = isset($meta['details']['data']['bank']) ? '/BANK/' . $meta['details']['data']['bank'] : null;
        $iban = isset($meta['details']['data']['iban']) ? '/IBAN/' . $meta['details']['data']['iban'] : null;
        $orderingParty = isset($meta['details']['data']['orderingParty']) ? '/ORDP/' . $meta['details']['data']['orderingParty'] : null;
        $ntwrk = isset($meta['details']['data']['channel']) ? '/NTWRK/' . $meta['details']['data']['channel'] : null;
        $narr3 = isset($meta['details']['data']['narrative']['narr3']) ? '/NAR3/' . $meta['details']['data']['narrative']['narr3'] : null;

        return ':86:' . $orderingParty . $ntwrk . $bank . $iban . $narr3 . '\\n';
    }

    public function validator($data)
    {
        return Validator::make($data, [
            'key' => ['required', 'string'],
            'date' => ['required', 'date'],
            'order' => ['nullable', 'in:ASC,DESC'],
            'page' => ['nullable', 'integer'],
            'take' => ['nullable', 'integer'],
        ]);
    }

    public function transactionsToStmtValidation($data)
    {
        return Validator::make($data, [
            'key' => ['required', 'string'],
            'date' => ['required', 'date'],
//            'date_to' => ['required', 'date'],
        ]);
    }

    public function getBankBalance($date)
    {
        $date = Carbon::parse($date)->format('Y-m-d');

        $bankBalance = DB::table('bank_balances')
            ->where('date', $date)
            ->first();

        if (!$bankBalance) {
            $this->bankBalanceService->handle($date);
        }

        return DB::table('bank_balances')
            ->where('date', $date)
            ->first();
    }
}
