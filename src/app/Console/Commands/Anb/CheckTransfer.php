<?php

namespace App\Console\Commands\Anb;

use App\Jobs\Anb\AnbEodStatement;
use App\Models\Anb\AnbTransfer;
use App\Models\Users\User;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Enums\AnbPaymentStatus;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:check-transfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get PAYMENT ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $data = AnbTransfer::query()->whereNull('transaction_id')->get();

            foreach ($data as $process) {

//                sleep(1);

                try {

                    $response = AnbApiUtil::newPayment($process->id, 'sequence_number');

                }catch (\Exception $e) {
                    continue;
                }

                if (in_array($response['status'],  AnbPaymentStatus::inProcessOrPaidStatuses())) {

                    dump('id => ' . $process->id . ' status => ' . $response['status'] .
                        ' amount => ' . ($process->amount/100) . ' user_id => ' . $process->initiator_id . 'date => ' . $process->created_at);


                    $transactions = DB::table('transactions')
                        ->where('reason', 4)
                        ->where('amount', ($process->amount * -1))
                        ->where('payable_id', $process->initiator_id)
                        ->where('payable_type', User::class)
                        ->select('amount', 'reason', 'created_at')
                        ->get();

                    foreach ($transactions as $index=>$transaction) {
                        dump($index . ' amount => ' . $transaction->amount/100 .
                                 ' date => ' . $transaction->created_at);
                    }

                    dump('------------------------------------------------------------');
                }
            }

            dd('done');

        }catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }

    }
}
