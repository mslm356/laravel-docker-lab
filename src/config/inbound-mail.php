<?php

return [
    'service' => env('INBOUND_NAME_SERVICE', 'fake'),

    'services' => [
        'mailgun' => [
            'signing_key' => env('MAILGUN_WEBHOOK_SIGNING_KEY')
        ]
    ]
];
