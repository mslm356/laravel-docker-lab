<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorInterface;

class UrwayReasonType implements GeneratorInterface
{
    /**
     * Generate description
     *
     * @return string
     */
    public function generate(Transaction $transaction, $locale = null): string
    {
        $card_rand = $transaction->meta['cardBrand'] ?? 'Urway';
        return __('transaction-description.urway_deposit', ['card_brand' => $card_rand], $locale);
    }
}
