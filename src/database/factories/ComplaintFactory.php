<?php

namespace Database\Factories;

use App\Models\ComplaintStatus;
use App\Models\Users\User;
use App\Models\ComplaintType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComplaintFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subject' => $this->faker->name,
            'description' => $this->faker->text(),
            'complaint_type_id' => ComplaintType::first()->id,
            'user_id' => User::first()->id,
            'reviewer_id' => User::first()->id,
            'status_id' => ComplaintStatus::first()->id,
        ];
    }
}
