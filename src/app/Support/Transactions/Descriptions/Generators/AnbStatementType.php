<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorInterface;

class AnbStatementType implements GeneratorInterface
{
    /**
     * Generate description
     *
     * @return string
     */
    public function generate(Transaction $transaction, $locale = null): string
    {
        return __('transaction-description.deposit', [], $locale);
    }
}
