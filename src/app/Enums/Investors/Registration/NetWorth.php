<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\DBColumInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class NetWorth extends Enum implements LocalizedEnum,DBColumInterface
{
    const LessThan5MillionsSAR =   1;
    const MoreThan5MillionsSAR =   2;


    function getDBColumnName()
    {
        return 'net_worth';
    }

}
