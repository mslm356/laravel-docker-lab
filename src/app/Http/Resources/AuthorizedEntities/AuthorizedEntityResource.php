<?php

namespace App\Http\Resources\AuthorizedEntities;

use App\Enums\AuthorizedEntityStatus;
use App\Enums\FileType;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorizedEntityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cr_number' => $this->cr_number,
            'cr' => FileResource::collection($this->files->where('type', FileType::AuthorizedEntityCR)),
            'cma_license' => FileResource::collection($this->files->where('type', FileType::AuthorizedEntityCMALicense)),
            'description' => $this->description,
            'review_status' => [
                'value' => $this->status,
                'description' => AuthorizedEntityStatus::getDescription($this->status),
                'comment' => $this->reviewer_comment
            ]
        ];
    }
}
