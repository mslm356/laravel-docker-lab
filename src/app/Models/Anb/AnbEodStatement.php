<?php

namespace App\Models\Anb;

use App\Models\AppModel;
use App\Support\QueryScoper\HasScopes;

class AnbEodStatement extends AppModel
{
    use HasScopes;

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = null;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'bank_api_response' => 'array',
        'opening_balance' => 'array',
        'closing_balance' => 'array',
        'date' => 'datetime',
    ];

    /**
     * Get all the transactions of the statment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(AnbEodTransaction::class, 'statement_id', 'id');
    }
}
