<?php

namespace App\Http\Controllers\Investors\Profile\ProfessionalUpgrade;

use App\Services\Investors\UpgradeProfileServices;
use App\Services\LogService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enums\Investors\UpgradeRequestStatus;
use App\Models\Investors\InvestorUpgradeRequest;
use App\Actions\Investors\ProfessionalUpgrade\CheckIfCanSubmitNewRequest;
use Illuminate\Validation\ValidationException;

class CreateUpgradeRequest extends Controller
{

    public $upgradeProfileServices;
    public $logService;

    public function __construct()
    {
        $this->upgradeProfileServices = new UpgradeProfileServices();

        $this->logService = new LogService();
    }

    public function __invoke(Request $request)
    {
        try {

            $data = $request->validate($this->upgradeProfileServices->getRules());

            $user = $request->user('sanctum');

            if (!CheckIfCanSubmitNewRequest::run($user)) {
                return apiResponse(400, __('messages.investor_upgrade_request_already_exists'));
            }

            $repeatedUpgradeRequest = InvestorUpgradeRequest::query()
                ->whereIn('status', [UpgradeRequestStatus::Approved, UpgradeRequestStatus::InReview, UpgradeRequestStatus::PendingReview])
                ->where('user_id', $user->id)
                ->first();

            if ($repeatedUpgradeRequest) {
                return apiResponse(400, __('messages.repeated_upgrade_request'));
            }


            if (!$this->upgradeProfileServices->store($data, $user)) {
                return apiResponse(400, __('mobile_app/message.please_try_later'));
            }

            return response()->json();


        } catch (\ErrorException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (ValidationException $e) {

            $errors = $e->errors();
            return response()->json(['message'=> $e->getMessage(), 'errors' => $errors], 422);

        } catch (\Exception $e) {
            $code = $this->logService->log('STORE-UPGRADE-PROFILE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }
}
