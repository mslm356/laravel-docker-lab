<?php

namespace App\Http\Resources\Investors\ProfessionalUpgrade;

use Illuminate\Http\Resources\Json\JsonResource;

class UpgradeRequestItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'files' => $this->pivot->files->map(fn ($file) => [
                'id' => $file->id,
                'name' => $file->name
            ])
        ];
    }
}
