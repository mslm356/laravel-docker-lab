<?php

namespace App\Support\Email\InboundMail;

interface InboundMailInterface
{
    public function transform($data);

    public function verifySource($data);

    public function verifySender($data, $expectedToken);

    public function getFromAddress($data);

    public function getToAddress($data);

    public function failureHttpCode();

    public function successResponse($data);

    public function failureResponse($data);
}
