<?php

return [
    'be_part_of_it_l1' => 'Invest',
    'be_part_of_it_l2' => 'in assets.',
    'buy_a_piece_of_it' => 'Buy a piece of it.',
    'invest_in_real_estate' => 'Invest in the world\'s best cities.',
    'become_an_investor' => 'Become an investor',
    'how_it_works' => 'How it works',
    'cme_licensed' => 'Investments can result in total loss and may be impossible to resell.',
    'learn_more' => 'Learn more',
    'listings' => 'Funds',
    'listings_description' => 'Invest as little as SR 1000 and earn a return anywhere in GCC.',
    'how_it_works_list' => [
        'explore' => [
            'title' => 'Explore',
            'description' => 'Invest as little as SR 1000 and earn a return anywhere in GCC',
        ],
        'register' => [
            'title' => 'Register',
            'description' => 'Invest as little as SR 1000 and earn a return anywhere in GCC',
        ],
        'charge_balance' => [
            'title' => 'Deposit',
            'description' => 'Invest as little as SR 1000 and earn a return anywhere in GCC',
        ],
        'invest' => [
            'title' => 'Invest',
            'description' => 'Invest as little as SR 1000 and earn a return anywhere in GCC',
        ],
        'enjoy' => [
            'title' => 'Enjoy the returns',
            'description' => 'Invest as little as SR 1000 and earn a return anywhere in GCC',
        ],
    ],
    'defs' => [
        'dividend_yield' => 'Financial ratio that shows how much is paid in dividends each year relative to the capital invested. Note: this is an estimate.',
        'gross_yield' => 'Financial ratio that shows how much is paid in dividends each year relative to the capital invested. Note: this is an estimate.',
    ],
    'newsletter' => [
        'title' => 'Want to receive news and updates?',
        'cta' => 'Sign up for our newsletter to stay up to date.',
        'notify_me' => 'Notify me',
        'email_placeholder' => 'Enter your email',
    ],
    'navlinks' => [
        'how_it_works' => 'How it works',
        'be_a_partner' => 'Be a partner',
        'invest' => 'Invest',
        'login' => 'Login',
        'signup' => 'Sign up',
        'about_us' => 'About us',
        'dashboard' => 'Dashboard',
        'logout' => 'Logout',
        'funds' => 'Funds',
    ],
    'footer' => [
        'links' => [
            'legal' => 'Legal',
            'privacy_policy' => 'Privacy policy',
            'sharia_compliance' => 'Sharia\'a compliance',
            'terms_and_conditions' => 'Terms & conditions',
            'code_of_conducts' => 'Code of conducts',
            'disclosure_policy' => 'Disclosure policy',
            'about_us' => 'About us',
            'contact_us' => 'Contact us',
            'faqs' => 'FAQs',
            'members_council' => 'Council of members',
        ],
        'headings' => [
            'legal' => 'Legal',
            'company' => 'Company',
            'code_of_conducts' => 'Code of conducts',
            'disclosure_policy' => 'Disclosure policy',
        ],
    ],
    'all_rights_reserved' => ':date Aseel, Inc. All rights reserved.',
    'cannot_find_an_answer' => 'Can’t find the answer you’re looking for? Reach out to our ',
    'cannot_find_an_answer_customer_support' => 'customer support team'
];
