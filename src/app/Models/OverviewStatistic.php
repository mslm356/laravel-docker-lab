<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OverviewStatistic extends Model
{
    use HasFactory;
    public $fillable=['user_id','total_investments','dividends','unrealized_gain'];



}
