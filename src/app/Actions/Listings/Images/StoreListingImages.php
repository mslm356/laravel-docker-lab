<?php

namespace App\Actions\Listings\Images;

use App\Services\Files\CheckFile;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;

class StoreListingImages
{
    use AsAction;

    public $checkFile;

    public function __construct()
    {
        $this->checkFile = new CheckFile();
    }

    public function handle($listing, $files)
    {
        $newImages = [];
        foreach ($files as $file) {

            $this->checkFile->handle($file);

            $path = $file->store("listings/{$listing->id}/images", config('filesystems.public'));
            $listing->images()->create([
                'name' => $file->getClientOriginalName(),
                'path' => $path,
                'is_cover' => 0
            ]);
            $first_image=$listing->images()->orderBy('index')->first();

            if ($first_image)
            $listing->update(['default_image'=>$first_image->path]);

            $newImages[] = [$file->getClientOriginalName()];

        }
        return $newImages;
    }
}
