<?php

namespace App\Console\Commands\Kyc;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Actions\Investors\Yaqeen\UpdateYaqeenData;
use App\Models\Investors\InvestorProfile;
use App\Models\Investors\InvestorProfileLog;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\DB;

class UpdateYaqeenDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yaqeen-update {--days=} {--sleep=} {--dif_birth_days=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $yaqeenInfoService;
    public $logService;

    public function __construct()
    {
        parent::__construct();
        $this->yaqeenInfoService = new YaqeenInfoService();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $days=$this->option('days')?$this->option('days'):7;
        $sleep=$this->option('sleep')?$this->option('sleep'):0;
        $dif_birth_days=$this->option('dif_birth_days')?$this->option('dif_birth_days'):0;


        $now=Hijri::Date('Y-m-d', Carbon::now());

        User::whereHas('nationalIdentity',function ($query) use ($now,$days){
            $query->where('national_identities.id_expiry_date','<',$now)->where('yaqeen_confirmation_at','<',Carbon::now()->subDays($days));
        })
            ->with('nationalIdentity')
            ->lazyById()
            ->each(function ($user) use ($sleep,$dif_birth_days) {
                if ($user->nationalIdentity)
                {
                    UpdateYaqeenData::run($user,$dif_birth_days);
                    sleep($sleep);
                }
            });

    }


}
