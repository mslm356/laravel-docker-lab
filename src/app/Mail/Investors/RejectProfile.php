<?php

namespace App\Mail\Investors;

use App\Models\Investors\InvestorProfile;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectProfile extends Mailable
{
    use Queueable, SerializesModels;

    public $profile;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(InvestorProfile $profile)
    {
        $this->profile = $profile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->profile ? $this->user->full_name : '---';
        $profileUrl = config('app.investor_url') . '/app/overview';
        $reason = $this->profile ? $this->profile->reviewer_comment : '---';

        return $this->view('emails.investors.reject_profile', compact('profileUrl', 'name', 'reason'))
            ->subject(__('emails/investors/reject_profile.subject'));
    }
}
