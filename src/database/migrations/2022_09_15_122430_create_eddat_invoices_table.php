<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEddatInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edaat_invoices', function (Blueprint $table) {
            $table->bigIncrements('base_id');
            $table->uuid('id');
            $table->string('edaat_invoice_id')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->double('amount')->nullable();
            $table->json('invoice_brief_result')->nullable();
            $table->timestamp('expire_at')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edaat_invoices');
    }
}
