<?php

return [
    'subject' => 'Early Investment',
    'title' => 'Your membership is about to expire',
    'name' => ':name',
    'name_title' => 'Our investor',
    'desc' => 'Due to the volume of your investments in the Aseel platform in the last two years, your membership will be changed to regular membership
    Please visit the control panel on the website or more in the application',
];
