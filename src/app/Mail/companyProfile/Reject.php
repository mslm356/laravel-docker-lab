<?php

namespace App\Mail\companyProfile;

use App\Models\CompanyProfile;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Reject extends Mailable
{
    use Queueable, SerializesModels;

    public $profile;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CompanyProfile $profile)
    {
        $this->profile = $profile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->profile ? $this->profile->name : '---';
        $profileUrl = config('app.investor_url') . '/app/overview';
        $reason = $this->profile ? optional($this->profile->user->investor)->reviewer_comment : '---';


        return $this->view('emails.investors.reject_profile', compact('profileUrl', 'name', 'reason'))
            ->subject(__('emails/investors/reject_profile.subject'));
    }
}
