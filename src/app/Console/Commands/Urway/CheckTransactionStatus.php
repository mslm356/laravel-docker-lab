<?php

namespace App\Console\Commands\Urway;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use Carbon\Carbon;
use Cknow\Money\Money;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CheckTransactionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'urway:check {--transid=} {--token=} {--amount=} {--time=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    public $urwayServices;
    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->urwayServices = new URwayServices();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->transactionStatus($this->option('token'), $this->option('amount'), $this->option('transid'));
    }

    public function urwayData($subscription_token, $total, $p_id)
    {
        $merchant_key = config('urWay.merchant_key');
        $terminalId = config('urWay.terminalId');
        $password = config('urWay.password');

//        $total = $total->formatByDecimal();

        $data = [
            'transid' => "", //$p_id,
            'trackid' => $subscription_token,
            'terminalId' => $terminalId,
            'action' => '10',
            'merchantIp' => '',
            'password' => $password,
            'country' => 'SA',
            'currency' => 'SAR',
            'amount' => $total,
            'requestHash' => hash('sha256', $subscription_token . '|' . $terminalId . '|' . $password . '|' . $merchant_key . '|' . $total . '|SAR'),
            'address' => '',
            'city' => '',
            'udf1' => '1',
        ];

        return json_encode($data);
    }

    public function transactionStatus($subscription_token, $total, $p_id)
    {
        $data = $this->urwayData($subscription_token, $total, $p_id);

        $urwayUrl = config('urWay.urwayUrl');

//        $response = Http::withHeaders([
//            'Content-type' => 'application/json',
//        ])->post($urwayUrl, $data);
//
//        $result = json_decode($response->body(), true);

//        dd($result);


        $ch = curl_init($urwayUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT,$this->option('time'));    // 5000000
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );

        curl_setopt($ch, CURLOPT_TIMEOUT, $this->option('time'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->option('time'));

//        execute post
        $server_output = curl_exec($ch);

        if ($server_output === false) {
            dd(curl_error($ch), json_decode($data));
        }

        dd($server_output);

//        close connection
        curl_close($ch);

        $result = json_decode($server_output, true);

    }

}
