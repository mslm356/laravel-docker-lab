<?php

namespace App\Http\Controllers\Admins\Api\MemberShips;

use App\Enums\MemberShips\MemberShipAssessment;
use App\Enums\MemberShips\MemberShipShowPopup;
use App\Enums\MemberShips\MemberShipStatus;
use App\Enums\MemberShips\MemberShipType;
use App\Enums\Role;
use App\Http\Resources\Admins\MemberShip\MemberShipResource;
use App\Models\MemberShip\MemberShipRequests;
use App\Models\Users\User;
use App\Services\LogService;
use App\Services\MemberShip\CreateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberShipController extends Controller
{

    public $createRequestService;
    public $logService;

    public function __construct()
    {
        $this->createRequestService = new CreateRequest();
        $this->logService = new LogService();
    }

    public function index(User $user)
    {
        $memberShipRequests = $user->memberShips()->latest()->paginate(10);
        return MemberShipResource::collection($memberShipRequests);
    }

    public function show(User $user)
    {
        $memberShipRequest = $user->memberShips()
            ->where('status', MemberShipStatus::Approved)
            ->latest()
            ->first();

        if (!$memberShipRequest) {
            return response()->json(['data' => ['type' => '']], 400);
        }

        return new MemberShipResource($memberShipRequest);
    }

    public function store(Request $request, User $user)
    {
        try {

            if (!$user->hasRole(Role::Investor)) {
                return response()->json(['message' => __('sorry, this user not investor')], 400);
            }

            if ($user->member_ship == 0) {
                return response()->json(['message' => __('sorry, this user is blocked for membership')], 400);
            }


            $data = $this->validate($request, [
                'type' => ['required', 'integer', 'in:1,2,3'],
                'comment' => ['required', 'string', 'max:200',textValidation()]
            ]);

            if ($user->member_ship == $data['type']) {
                return response()->json(['message' => __('sorry, you already in this membership')], 400);
            }

            $membershipLevel = $user->member_ship > $data['type'] ? 'down' : 'up';

            if (!$this->createRequestService->canCreateMemberShipRequest($user->id, $request['type'], 'admin')) {
                return response()->json(['message' => __('sorry, you can not create new member request')], 400);
            }

            $membershipData = [
                'member_ship' => $membershipLevel == 'down' ? 1 : $data['type'],
                'user_id' => $user->id,
                'comment' => $data['comment'],
                'initiator_type' => MemberShipAssessment::AdminAssessment,
                'created_by' => auth()->id(),
                'expire_at' => now()->addMonths(6),
            ];

            $withAlert = true;
            $alertType = $data['type'] == MemberShipType::Regular ? 'downGrad' : 'create';

            if ($membershipLevel == 'down') {
                $membershipData['status'] = MemberShipStatus::Approved;
                $membershipData['show_popup'] = MemberShipShowPopup::Hide;
                $alertType = 'downGrad';
//                $withAlert = true;
            }

            $this->createRequestService->createRequest($membershipData, $withAlert, $alertType);

            if ($membershipLevel == 'down') {

                $user->member_ship = MemberShipType::Regular;
                $user->save();

                if ($data['type'] != MemberShipType::Regular) {

                    $membershipData['member_ship'] = $data['type'];
                    $membershipData['status'] = MemberShipStatus::Pending;

                    $this->createRequestService->createRequest($membershipData, $withAlert,'create');
                }
            }

            return response()->json();

        } catch (\Exception $e) {
            $code = $this->logService->log('MEMBER-SHIP-EXCEPTION-ADMIN', $e);
            return apiResponse(400, __('please try later') . ':' . $code);
        }
    }


}
