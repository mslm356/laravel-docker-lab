<?php

namespace App\Actions\Investors;

use App\Models\File;
use App\Models\Listings\ListingInvestorRecord;
use Illuminate\Support\Facades\Crypt;
use Lorisleiva\Actions\Concerns\AsAction;

class GetInvestorFileUrl
{
    use AsAction;

    public function handle($investor_record_id)
    {
        $investor_record_id=Crypt::decryptString($investor_record_id);

        $file=File::where('fileable_type',ListingInvestorRecord::class)->where('fileable_id',$investor_record_id)->first();

        if ($file)
        return apiResponse(200, 'done', ['file_url' => $file->temp_full_path]);

        $file=File::find(config('app.empty_file'));
        return apiResponse(200, 'done', ['file_url' => $file->temp_full_path]);

    }

}
