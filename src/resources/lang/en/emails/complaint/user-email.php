<?php

return [
    'dear' => 'Dear :name ',
    'subject' => 'Complaint Received :ticketNumber',
    'message' => 'We have received your complaint :ticketNumber and we will reply to you as soon as possible.',
    'thanks' => 'Thanks ,',
    'company' => 'Aseel Financial ',
];
