<?php

namespace App\Console\Commands\Aml;


use App\Enums\Investors\InvestorStatus;
use App\Enums\Role;
use App\Models\AML\AmlUser;
use App\Models\Investors\InvestorProfile;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Aml\AmlServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ApproveAmlScreen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aml:approve {--customer_reference_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create old customers in aml';
    public $amlServices;
    public $logServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->amlServices = new AmlServices();
        $this->logServices = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            if ($this->option('customer_reference_id')) {

                $data = [
                    'type' => 'approveAml',
                    'customer_reference_id' => $this->option('customer_reference_id')
                ];

                $amlUser = DB::table('aml_users')
//                    ->where('approve_response', '!=',"\"Success\"")
//                    ->wherenull('approve_response')
                    ->wherenotNull('risk_result')
                    ->where('customer_reference_id', $this->option('customer_reference_id'))
                    ->select('customer_reference_id', 'query_id', 'risk_result', 'screen_result')
                    ->first();

                if (!$amlUser) {
                    dd('customer_reference_id not valid');
                }

                $amlUser = (array) $amlUser;

                $data = array_merge($amlUser, $data);

                if ($amlUser['screen_result'] == 1) {

                    $response = $this->amlServices->handle($data);

                    $updatedData = [
                        'approve_response' => json_encode($response),
                        'approve_status' => $response == '"Success"' ?  1 : 0
                    ];

                    DB::table('aml_users')
                        ->where('customer_reference_id', $this->option('customer_reference_id'))
                        ->update($updatedData);

                    dd($response);
                }

                dd('customer_reference_id screen_result not true');
            }

            DB::table('aml_users')
                ->where('approve_status','!=',1)
                ->where('screen_result',1)
                ->whereNull('approve_response')
                ->wherenotNull('risk_result')
                ->select('customer_reference_id', 'query_id', 'risk_result', 'screen_result', 'user_id')
                ->lazyById(50000)
                ->each(function ($aml, $key) {

                    $this->approveAmlAfterKyc($aml);
                    dump(' index => ' . $key . ' => customer_reference_id ' . $aml->customer_reference_id);

                });

        } catch (\Exception $e) {
            $this->logServices->log('APPROVE-AML-EXCEPTION', $e);
            dd($e->getFile(), $e->getLine(), $e->getMessage());
        }
    }

    public function approveAmlAfterKyc($amlUser)
    {

        if ($amlUser->risk_result == "High") {

            InvestorProfile::query()
                ->where('user_id', $amlUser->user_id)
                ->update(['status' => InvestorStatus::Suspended]);

            DB::table('users')
                ->where('id', $amlUser->user_id)
                ->update(['jwt_token' => null]);
        }

        if ($amlUser->screen_result == 1 && $amlUser->risk_result) {
            $this->amlServices->approveAml($amlUser);
        }

        return true;
    }
}
