<?php

namespace App\Models\Listings;

use App\Models\AppModel;

class ListingDetail extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'collection'
    ];
}
