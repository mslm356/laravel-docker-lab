<?php

namespace App\Models;

use Illuminate\Support\Facades\App;

class Country extends AppModel
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get name attribute
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $locale = App::getLocale();

        return $this->{"name_{$locale}"};
    }
}
