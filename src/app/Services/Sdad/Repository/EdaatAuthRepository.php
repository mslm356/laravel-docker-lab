<?php

namespace App\Services\Sdad\Repository;

use App\Models\EdaatAuthToken;
use App\Services\Exeptions\NoResponseDataException;
use App\Services\LogService;
use App\Services\Sdad\Contracts\EdaatAuthRepositoryContract;
use App\Services\Sdad\Enums\ServiceEnums;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EdaatAuthRepository implements EdaatAuthRepositoryContract
{
    public $provider_repo;
    public $logService;

    public function __construct()
    {
        $this->provider_repo = new EdaatProviderClientRepository();
        $this->logService = new LogService();
    }

    /**
     * @inheritDoc
     */
    public function makeAuth(): ?Model
    {
        try {
            $data = [
                'grant_type' => config('edaat.grant_type'),
                'username' => config('edaat.username'),
                'password' => config('edaat.password'),
            ];
            $service_name = ServiceEnums::service_name(ServiceEnums::AUTH);
            $this->provider_repo
                ->initiation($service_name)
                ->body($data)
                ->send();

            if ($this->provider_repo->response->body()) {
                $response_data = $this->prepare_token_data($this->provider_repo->getResponse());

                EdaatAuthToken::truncate();
                $token = EdaatAuthToken::create($response_data);
                return $token;

            } else {
                throw new NoResponseDataException();
            }
        } catch (NoResponseDataException $e) {
            throw new NoResponseDataException();
        } catch (\Exception $e) {
            $this->logService->log('Sdad-auth_repo', $e, ['issue', json_encode($data)]);
            throw new \Exception('provider error');
        }
    }

    /**
     * @inheritDoc
     */
    public function checkAuth(Model $token = null): ?bool
    {
        $token = $token ?? EdaatAuthToken::latest()->first();
        if (!$token) {
            return false;
        }
        if ($token->expire_in->isPast()) {
            return false;
        }

        return true;
    }

    /**
     * @throws NoResponseDataException
     */
    public function getValidToken(): ?Model
    {
        $token = EdaatAuthToken::latest()->first();
        if ($this->checkAuth($token)) {
            return $token;
        } else {
            return $this->makeAuth();
        }
    }

    public function prepare_token_data(object $response)
    {
        $data = [
            'token' => $response->access_token,
            'expire_in' => Carbon::now()->addSeconds($response->expires_in),
            'response' => $response,
        ];

        return $data;
    }

    public function refreshToken()
    {
        $token = EdaatAuthToken::latest()->first();
        if (!$this->checkAuth($token)) {
            $this->makeAuth();
        }
    }
}
