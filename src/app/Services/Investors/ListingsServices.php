<?php


namespace App\Services\Investors;

use App\Enums\InvestmentType;
use App\Enums\Listings\ListingReviewStatus;
use App\Models\Listings\Listing;
use App\Services\MemberShip\SpecialCanInvest;
use Illuminate\Support\Facades\Storage;

class ListingsServices
{

    public function Listings()
    {

        $listing = Listing::query()
            ->leftJoin('cities', 'listings.city_id', '=', 'cities.id')
            ->leftJoin('property_types', 'listings.type_id', '=', 'property_types.id')
            ->where('is_visible', true)
            ->whereNotNull('authorized_entity_id')
            ->where('review_status', ListingReviewStatus::Approved)
            ->orderBy('id', 'desc')
            ->select('listings.*','changed_investment_precentage','cities.name_en as city_name_en','cities.name_ar as city_name_ar','property_types.name_en as property_type_en','property_types.name_ar as property_type_ar')
            ->simplePaginate();

        return $listing;
    }

    public function show($listing, $user = null)
    {
        $listing = Listing::with([
            'details:id,listing_id,category,value',
            'city:id,name_ar,name_en',
            'type:id,name_ar,name_en,is_active',
            'images:id,name,path,index,listing_id',
            'attachments:id,name,path,extra_info,type,fileable_id',
            'authorizedEntity:id,name',
        ])
            ->where('id', $listing)
            ->where('is_visible', true)
            ->whereNotNull('authorized_entity_id')
            ->where('review_status', ListingReviewStatus::Approved)
            ->select('id','investment_shares','changed_investment_precentage','review_status','title_ar','opened_member_ships','title_en','gross_yield','dividend_yield','start_at','expected_net_return','expected_net_return','fund_duration','min_inv_share',
                                'max_inv_share','total_shares','early_investment_date','administrative_fees_percentage','vat_percentage','rent_amount','investment_type','target','created_at','updated_at','city_id','type_id','authorized_entity_id','deadline','is_completed','is_closed','is_visible')
            ->firstOrFail();


        return $listing;
    }

    public function listingResource($listing, $user)
    {

        $specialCanInvestObj = new SpecialCanInvest();
        $specialCanInvest = $specialCanInvestObj->handle($listing, $user) ;

        $listingTags = $listing->Listing_tags;

        $listingTags['is_start_soon'] = $specialCanInvest ? false : $listing->start_soon;

        $data = [

            'id' => $listing->id,
            'title' => $listing->title,
            'target' => $listing->target->formatByDecimal(),
            'target_fmt' => cleanDecimal(number_format($listing->target->formatByDecimal())),
            'min_inv_amount' => $minInvestmentAmount = number_format($listing->share_price->multiply($listing->min_inv_share)->formatByDecimal()),
            'min_inv_amount_fmt' => $minInvestmentAmount,
//            'city' => $listing->whenLoaded('city', fn() => $listing->city->name),
//            'type' => $listing->whenLoaded('type', fn() => $listing->type->name),

            'city' => $listing->city ? $listing->city->name : '',
            'type' => $listing->type ? $listing->type->name : '',

            'start_date' => $listing->start_time,
            'expected_net_return' => number_format($listing->expected_net_return, 2) . " " . " %",
            'deadline' => $listing->deadline,
            'fund_duration' => $listing->fund_duration . " " . trans("messages.month"),
            'investment_type' => InvestmentType::getDescription($listing->investment_type),
            'complete_percent' => $listing->fund_percent,
            'images'=>($listing->default_image)?[Storage::disk(config('filesystems.public'))->url($listing->default_image)]:[],

            'can_invest' => $specialCanInvest,
            'authorized_entity' => $listing->authorizedEntity ? $listing->authorizedEntity->name : '',
            'is_completed' => $listing->is_completed,
            'is_closed' => $specialCanInvest ? false : true,
        ];

        return array_merge($data, $listingTags);

    }


}
