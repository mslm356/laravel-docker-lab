<?php


namespace App\Services\MemberShip;

use App\Enums\Investors\InvestorLevel;
use App\Enums\MemberShips\MemberShipType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SpecialCanInvest
{
    public function handle($listing, $user)
    {
        if (!$user || !$listing) {
            return false;
        }

        if ($listing->can_invest) {
            return true;
        }

        if (in_array($listing->id, config('app.listing_can_invested')) && in_array($user->id, config('app.users_can_invest'))) {
            return true;
        }

        if ($listing->is_closed) {
            return false;
        }

        $openedMemberShip = json_decode($listing->opened_member_ships);

        $earlyInvestmentDate = Carbon::parse($listing->early_investment_date);

        if (is_array($openedMemberShip) && $listing->early_investment_date &&
            in_array($user->member_ship, $openedMemberShip) &&
            $earlyInvestmentDate->isBefore(now())) {

            return true;
        }

        $profile = DB::table('investor_profiles')
            ->where('user_id', $user->id)
            ->select('level')
            ->first();

        if ($profile &&
            $profile->level == InvestorLevel::Professional &&
            is_array($openedMemberShip) &&
            in_array(MemberShipType::Upgraded, $openedMemberShip) &&
            $listing->early_investment_date  &&
            $earlyInvestmentDate->isBefore(now())) {
            return true;
        }

        return false;
    }

    public function checkMembershipLimit($listing, $investedShares, $currentInvestedShares)
    {
        $response = [
            'status' => false,
            'message_num' => 7,
        ];

        if (!$listing || $listing->total_shares <= 0) {
            return $response;
        }

        $start_at = Carbon::parse($listing->start_at);

        if ($start_at->isBefore(now())) {
            $response['status'] = true;
            return $response;
        }

        $percent = clean_decimal(($investedShares / $listing->total_shares * 100));

        if ($percent > (float)config('memberShip.membership_limit')) {
            $response['message_num'] = 8;
            return $response;
        }

        if ($percent <= config('memberShip.membership_limit')) {
            $response['status'] = true;
        }

        return $response;
    }
}
