<?php

namespace App\Mail\Complaint;

use App\Models\Complaint;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendReceivedComplaintEmailToTheUser extends Mailable
{
    use Queueable, SerializesModels;

    protected $complaint = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.complaint.complaint-email-to-user')
            ->with('complaint', $this->complaint)
            ->subject(__('emails/complaint/admin-email.subject', ['ticketNumber' => $this->complaint->id]));
    }
}
