<?php

namespace App\Enums\MemberShips;

use BenSampo\Enum\Enum;

final class MemberShipType extends Enum
{
    const Blocked = 0;
    const Regular = 1;
    const Gold = 2;
    const Diamond = 3;
    const Upgraded = 4;
}
