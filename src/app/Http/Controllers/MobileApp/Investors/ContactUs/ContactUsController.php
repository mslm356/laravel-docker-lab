<?php

namespace App\Http\Controllers\MobileApp\Investors\ContactUs;

use App\Actions\CreateNewEnquiry;
use App\Http\Controllers\Controller;
use App\Models\ContactReason;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class ContactUsController extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function store(Request $request)
    {
        try {

            CreateNewEnquiry::run(getAuthUser('api'), $request->input());
            return apiResponse(201, __('mobile_app/message.success'));

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage(), ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('CONTACT-US-STORE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    public function index()
    {
        try {
            $data = ContactReason::all()->map(fn($reason) => [
                'id' => $reason->id,
                'name' => $reason->name
            ]);
            return apiResponse(200, __('mobile_app/message.success'), $data);
        } catch (\Exception $e) {
            $code = $this->logService->log('CONTACT-US-INDEX', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

}
