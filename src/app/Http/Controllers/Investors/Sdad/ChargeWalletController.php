<?php

namespace App\Http\Controllers\Investors\Sdad;

use App\Jobs\ChargeWallet;
use App\Support\Users\UserAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChargeWalletController
{

    public $userAccess;

    public function __construct()
    {
        $this->userAccess = new UserAccess();
    }

    /**
     * create sdad invoice
     *
     * @param  $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function create_invoice(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'amount' => 'required|numeric|min:50|max:1000000',
            ]);

            if ($validator->fails()) {
                return apiResponse(422, '', $validator->errors());
            }

            $user = Auth::guard('sanctum')->user();

            ChargeWallet::dispatch($user, $request->all());

        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

        return apiResponse(200, __('messages.invoice_will_be_exported'), []);
    }

}
