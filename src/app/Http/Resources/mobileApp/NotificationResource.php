<?php

namespace App\Http\Resources\mobileApp;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $notification_type = explode('\\', $this->type);
        $data = json_decode($this->data, true);
        $lang = $request->header('lang') ? $request->header('lang') : app()->getLocale();

        return [
            'id'                => $this->notification_id,
            'notification_type' => isset($notification_type['2']) ? $notification_type['2'] : null,
            'is_read'           => $this->read_at ? true : false,
            'read_at'           => $this->read_at,
            'info'              => isset($data['action_id']) ? $data['action_id'] : null,
            'heading'           => isset($data['title_' . $lang]) ? $data['title_' . $lang] : null,
            'content'           => isset($data['content_' . $lang]) ? $data['content_' . $lang] : null,
            'created_at'        => Carbon::parse($this->created_at)->format('Y-m-d H:i'),
            'updated_at'        =>  Carbon::parse($this->updated_at)->format('Y-m-d H:i')
        ];
    }
}
