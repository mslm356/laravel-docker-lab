<?php

namespace App\Support\Banking\MT940;

use Jejik\MT940\BalanceInterface;
use Jejik\MT940\TransactionInterface;
use Jejik\MT940\Parser\AbstractParser;

class AnbEodParser extends AbstractParser
{
    /**
     * Create a Transaction from MT940 transaction text lines
     *
     * @param array $lines The transaction text at offset 0 and the description
     *                     at offset 1
     *
     * @throws \Exception
     */
    protected function transaction(array $lines): TransactionInterface
    {
        if (!preg_match(
            '/(\d{6})(\d{4})?((?:C|D|RD|RD)R?)([0-9,]{1,15})((?:N|F)[a-zA-Z]{3})([\S\s]{1,})\/\/([\S\s]{1,})/',
            $lines[0],
            $match
        )) {
            throw new \RuntimeException(sprintf('Could not parse transaction line "%s"', $lines[0]));
        }

        // Parse the amount
        $amount = str_replace(',', '.', $match[4]);

        if (in_array($match[3], array('D', 'DR', 'RD'))) {
            $amount = "-" . $amount;
        }

        // Parse dates
        $valueDate = \DateTime::createFromFormat('ymd', $match[1], new \DateTimeZone('Asia/Riyadh'));
        $valueDate->setTime(0, 0, 0)->setTimezone(new \DateTimeZone('UTC'));

        // parse transaction code
        $code = $match[5] ?? null;

        // parse customer ref
        $customerRef = $match[6] ?? null;

        // parse bank ref
        $bankRef = $match[7] ?? null;

        $bookDate = null;

        if ($match[2]) {
            // current|same year as valueDate
            $bookDate_sameYear = \DateTime::createFromFormat(
                'ymd',
                $valueDate->format('y') . $match[2],
                new \DateTimeZone('Asia/Riyadh')
            );
            $bookDate_sameYear->setTime(0, 0, 0)->setTimezone(new \DateTimeZone('UTC'));

            /* consider proper year -- $valueDate = '160104'(YYMMTT) & $bookDate = '1228'(MMTT) */
            // previous year bookDate
            $bookDate_previousYear = clone ($bookDate_sameYear);
            $bookDate_previousYear->modify('-1 year');

            // next year bookDate
            $bookDate_nextYear = clone ($bookDate_sameYear);
            $bookDate_nextYear->modify('+1 year');

            // bookDate collection
            $bookDateCollection = [];

            // previous year diff
            $bookDate_previousYear_diff = $valueDate->diff($bookDate_previousYear);
            $bookDateCollection[$bookDate_previousYear_diff->days] = $bookDate_previousYear;

            // current|same year as valueDate diff
            $bookDate_sameYear_diff = $valueDate->diff($bookDate_sameYear);
            $bookDateCollection[$bookDate_sameYear_diff->days] = $bookDate_sameYear;

            // next year diff
            $bookDate_nextYear_diff = $valueDate->diff($bookDate_nextYear);
            $bookDateCollection[$bookDate_nextYear_diff->days] = $bookDate_nextYear;

            // get the min from these diffs
            $bookDate = $bookDateCollection[min(array_keys($bookDateCollection))];
        }

        $description = isset($lines[1]) ? $lines[1] : null;
        /** @var \App\Support\Banking\MT940\AnbEodTransaction $transaction */
        $transaction = $this->reader->createTransaction();
        $transaction
            ->setAmountAsString($amount)
            ->setMark($match[3])
            ->setAmount((float)$amount)
            ->setContraAccount($this->contraAccount($lines))
            ->setValueDate($valueDate)
            ->setBookDate($bookDate)
            ->setCode($code)
            ->setRef($this->ref($lines))
            ->setBankRef($bankRef)
            ->setGVC($this->gvc($lines))
            ->setTxText($this->txText($lines))
            ->setPrimanota($this->primanota($lines))
            ->setExtCode($this->extCode($lines))
            ->setEref($this->eref($lines))
            ->setBIC($this->bic($lines))
            ->setIBAN($this->iban($lines))
            ->setAccountHolder($customerRef)
            ->setKref($this->kref($lines))
            ->setMref($this->mref($lines))
            ->setCred($this->cred($lines))
            ->setSvwz($this->svwz($lines))
            ->setDescription($this->description($description));

        return $transaction;
    }

    public function accept(string $text): bool
    {
        return true;
    }

    public function getAllowedBLZ(): array
    {
        return [];
    }

    /**
     * Parse a statement number
     *
     * @param string $text Statement body text
     */
    protected function statementNumber(string $text): ?string
    {
        $number = $this->getLine('28|28c|28|28C', $text);
        if (!is_null($number)) {
            return $number;
        }

        return null;
    }

    /**
     * Parse an account number
     *
     * @param string $text Statement body text
     */
    protected function accountNumber(string $text): ?string
    {
        if ($account = $this->getLine('25', $text)) {
            return $account;
        }

        return null;
    }

    /**
     * Create a Balance object from an MT940  balance line
     */
    protected function balance(BalanceInterface $balance, string $text): BalanceInterface
    {
        if (!preg_match('/(C|D)(\d{6})([A-Z]{3})([0-9,]{1,15})/', $text, $match)) {
            throw new \RuntimeException(sprintf('Cannot parse balance: "%s"', $text));
        }

        $amount = str_replace(',', '.', $match[4]);
        if ($match[1] === 'D') {
            $amount = "-" . $amount;
        }

        $date = \DateTime::createFromFormat('ymd', $match[2], new \DateTimeZone('Asia/Riyadh'));
        $date->setTime(0, 0, 0)->setTimezone(new \DateTimeZone('UTC'));

        /** @var \App\Support\Banking\MT940\AnbEodBalance $balance */
        $balance
            ->setAmountAsString($amount)
            ->setCurrency($match[3])
            ->setAmount((float)$amount)
            ->setDate($date);

        return $balance;
    }

    /**
     * Get the contents of an MT940 line
     *
     * The contents may be several lines long (e.g. :86: descriptions)
     *
     * @param string $text The text to search
     * @return string|null
     */
    protected function getTransactionLines($text): ?array {

        $amountLine = [];
        $pcre = '/(?:^|\r\n)\:(?:61)\:(.+)(?::?$|\r\n\:[[:alnum:]]{2,3}\:)/Us';
        if (preg_match_all($pcre, $text, $match)) {
            $amountLine = $match;
        }

        $multiPurposeField = [];
        $pcre = '/(?:^|\r\n)\:(?:86)\:(.+)(?:[\r\n])(?:\:(?:6[0-9]{1}[a-zA-Z]?)\:|(?:[\r\n]-$))/Us';

        if (preg_match_all($pcre, $text, $match)) {
            $multiPurposeField = $match;
        }

        if (($amountLine[1] ?? null) === null) {
            return [];
        }

        $count = count($amountLine[1]);
        $result = [];
        for ($i = 0; $i < $count; $i++) {
            $result[$i][] = trim($amountLine[1][$i]);
            $result[$i][] = trim(str_replace(':86:', '', $multiPurposeField[1][$i]));
        }

        return $result;
    }
}
