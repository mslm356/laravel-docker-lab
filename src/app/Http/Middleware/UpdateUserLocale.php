<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class UpdateUserLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $appLocale = App::getLocale();

        $user = getAuthUser('api');

        if (!$user) {
            $user = getAuthUser('web');
        }

        if ($user && $user->locale !== $appLocale) {
            $user->update([
                'locale' => $appLocale
            ]);
        }

        return $next($request);
    }
}
