<?php

namespace App\Support\Email\InboundMail\Strategies;

use App\Support\Email\InboundMail\InboundMailInterface;
use Illuminate\Support\Arr;

class Fake implements InboundMailInterface
{
    public function transform($data)
    {
        return [
            'message' => Arr::get($data, 'test_message'),
        ];
    }

    public function verifySource($data)
    {
        return true;
    }

    public function verifySender($data, $expectedToken)
    {
        return Arr::get($data, 'token') === $expectedToken;
    }

    public function getFromAddress($data)
    {
        return Arr::get($data, 'from');
    }

    public function getToAddress($data)
    {
        return Arr::get($data, 'to');
    }

    public function failureHttpCode()
    {
        return 411;
    }

    public function successResponse($data)
    {
        return response()->json([], 202);
    }

    public function failureResponse($data)
    {
        return response()->json([], $this->failureHttpCode());
    }
}
