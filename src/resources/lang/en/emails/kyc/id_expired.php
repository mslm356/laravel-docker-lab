<?php

return [
    'subject' => 'Updated Know Your Customer (KYC)',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'Due to the expiration of your registered ID',
    'message_2' => 'please update the KYC form in your profile',
    'message_3' => 'Thank you for trusting Aseel',
];
