<?php

namespace App\Console\Commands\Membership;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Jobs\AbsherTest;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use App\Models\Listings\Listing;
use App\Models\MemberShip\MemberShipRequests;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Services\MemberShip\AlertUser;
use App\Services\MemberShip\CreateRequest;
use App\Services\MemberShip\SpecialCanInvest;
use App\Settings\ZatcaSettings;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TestMemberShip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:member-ship';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create member ship request';

    public $logService;
    public $memberShipService;
//    public $specialCanInvest;
//    public $alertUser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
        $this->memberShipService = new CreateRequest();
//        $this->specialCanInvest = new SpecialCanInvest();
//        $this->alertUser = new AlertUser();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $this->memberShipService->autoRequest();

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }


}
