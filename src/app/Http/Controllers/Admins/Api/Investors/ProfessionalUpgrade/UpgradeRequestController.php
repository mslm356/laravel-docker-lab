<?php

namespace App\Http\Controllers\Admins\Api\Investors\ProfessionalUpgrade;

use App\Enums\BankAccountStatus;
use App\Enums\Investors\InvestorLevel;
use App\Enums\Investors\UpgradeRequestStatus;
use App\Enums\Notifications\NotificationTypes;
use App\Mail\ProfessionalRequest\ApproveUpgradeRequest;
use App\Mail\ProfessionalRequest\RejectUpgradeRequest;
use App\Models\Investors\InvestorProfile;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Investors\InvestorUpgradeRequest;
use App\Http\Resources\Admins\Investors\ProfessionalUpgrade\UpgradeRequestResource;
use App\Http\Resources\Admins\Investors\ProfessionalUpgrade\UpgradeRequestListItemResource;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UpgradeRequestController extends Controller
{

    use NotificationServices , SendMessage;

    public function index(Request $request)
    {
        $professionalUpgradeRequests = InvestorUpgradeRequest::query();


        if ($request->has('value') && $request['value'] != null) {
            $professionalUpgradeRequests->where(function ($query1) use ($request) {
                $query1->whereHas('reviewer', function ($query) use ($request) {
                    $query->whereRaw("concat(first_name, ' ', last_name) like '%$request->value%' ");
                })->orWhereHas('user', function ($q) use ($request) {
                    $q->whereRaw("concat(first_name, ' ', last_name) like '%$request->value%' ");
                });
            });
        }

        if ($request->has('status') && $request['status'] != null) {
            $professionalUpgradeRequests->where('status', $request['status']);
        }

        $professionalUpgradeRequests = $professionalUpgradeRequests->orderBy('id', 'desc')->paginate();

        return UpgradeRequestListItemResource::collection($professionalUpgradeRequests);
    }

    public function showUpgradeRequestListByInvestor($id)
    {
        return UpgradeRequestListItemResource::collection(
            InvestorUpgradeRequest::with(['reviewer', 'user'])->where('user_id', $id)->orderBy('id', 'desc')->paginate()
        );
    }

    public function show(InvestorUpgradeRequest $upgradeRequest)
    {
        return new UpgradeRequestResource(
            $upgradeRequest
        );
    }

    /**
     * Update status of the request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, InvestorUpgradeRequest $upgradeRequest)
    {

        $data = $this->validate($request, [
            'status' => ['required', 'integer', new EnumValue(UpgradeRequestStatus::class, false)],
            'comment' => ['exclude_unless:status,' . UpgradeRequestStatus::Rejected,'nullable', 'string','max:1000',textValidation()]
        ]);

        $data['reviewer_id'] = $request->user('sanctum')->id;

        $repeatedUpgradeProfile = InvestorUpgradeRequest::query()
            ->where('status', UpgradeRequestStatus::Approved)
            ->where('user_id', $upgradeRequest->user_id)
            ->where('id', '!=', $upgradeRequest->id)
            ->count();

        if ($repeatedUpgradeProfile && $request->status == UpgradeRequestStatus::Approved)
            return apiResponse(400, __('messages.investor_profile_already_upgraded'));

        if ($upgradeRequest->status == $request->status) {

            $upgradeRequest->update([
                'reviewer_comment' => isset($data['comment']) && $data['comment'] ?  $data['comment'] : null,
            ]);

            return apiResponse(400, __('messages.repeated_status'));
        }

        return DB::transaction(function () use ($upgradeRequest, $data) {
            $status = (int)$data['status'];

            $upgradeRequest->update([
                'status' => $status,
                'reviewer_comment' => isset($data['comment']) && $data['comment'] ?  $data['comment'] : null,
                'reviewer_id' => $data['reviewer_id'],
            ]);
            $profile=InvestorProfile::where('user_id',$upgradeRequest->user_id)->first();

            if ($status === UpgradeRequestStatus::Approved) {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($upgradeRequest->user)->queue(new ApproveUpgradeRequest($upgradeRequest->user));
                }
                    $this->sendSmsData($upgradeRequest->reviewer_comment , UpgradeRequestStatus::Approved , $upgradeRequest->user);

                $this->send(NotificationTypes::ApproveProfessionalAccount, $this->notificationData($upgradeRequest, UpgradeRequestStatus::Approved, $upgradeRequest->reviewer_comment), $upgradeRequest->user->id);
                $profile->level=InvestorLevel::Professional;
            } else {
                $profile->level=InvestorLevel::Beginner;
            }

            $profile->save();
            if ($status === UpgradeRequestStatus::Rejected) {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($upgradeRequest->user)->queue(new RejectUpgradeRequest($upgradeRequest->user, $upgradeRequest->reviewer_comment));
                }
                 $this->sendSmsData($upgradeRequest->reviewer_comment , UpgradeRequestStatus::Rejected , $upgradeRequest->user);

                $this->send(NotificationTypes::RejectProfessionalAccount, $this->notificationData($upgradeRequest, UpgradeRequestStatus::Rejected , $upgradeRequest->reviewer_comment), $upgradeRequest->user->id );
            }

            return response()->json(['data' => [
                'message' => 'Professional Request Updated Successfully'
            ]], 200);
        });
    }

    public function sendSmsData($reason , $status, $user)
    {
        $name = $user ? $user->full_name : '---';
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_en = $status == BankAccountStatus::Approved ? 'We congratulate you on accepting your application for (professional investor), Aseel wishes you all the best, Thank you for dealing with Aseel Platform' : "We apologize for not accepting your application to upgrade your account, please see the request for rejection reason is " .$reason." Thank you for dealing with Aseel platform";
        $content_ar = $status == BankAccountStatus::Approved ? ' نهنئكم بقبول طلبكم بالترقية إلى (مستثمر محترف)، منصة أصيل تتمنى لك كل التوفيق شكراً لتعاملك مع منصة أصيل المالية '   : ' شكراً لتعاملك مع منصة أصيل المالية  '.$reason. ' نعتذر عن عدم قبول طلبكم لترقية الحساب إلى (مستثمر محترف)، وذلك بسبب أن';

        $content_ar_1 = $welcome_message_ar  . $content_ar;
        $content_ar = $welcome_message_ar . $content_ar_1;
        $content_en = "Our dear investor " . $name  . $content_en;
        if ($user->locale = "ar")
            $message = $content_ar;

        else
            $message = $content_en;

        $this->sendOtpMessage($user->phone_number, $message);

    }

    public function notificationData($upgradeRequest, $status , $reason)
    {
        return [
            'action_id' => $upgradeRequest->id,
            'channel' => 'mobile',
            'title_en' => $status == UpgradeRequestStatus::Approved ? 'Your upgrade request has been approved' : 'Your upgrade request has been denied',
            'title_ar' => $status == UpgradeRequestStatus::Approved ? 'تمت الموافقة على طلب ترقية الحساب إلى مستثمر محترف' : 'عدم الموافقة على طلب ترقية الحساب',
            'content_en' => $status == UpgradeRequestStatus::Approved ? 'We congratulate you on accepting your application for (professional investor)' : '  We apologize for not accepting your application to upgrade your account  ' . $reason,
            'content_ar' => $status == UpgradeRequestStatus::Approved ? 'نهنئكم بقبول طلبكم بالترقية إلى (مستثمر محترف)، منصة أصيل تتمنى لك كل التوفيق' :  '  نعتذر عن عدم قبول طلبكم لترقية الحساب إلى مستثمر محترف  وذلك بسبب أن  ' . $reason,
        ];
    }


}
