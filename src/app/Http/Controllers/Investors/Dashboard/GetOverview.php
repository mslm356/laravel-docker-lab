<?php

namespace App\Http\Controllers\Investors\Dashboard;

use App\Services\Investors\OverViewTimeLineServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetOverview extends Controller
{
    public $overViewTimeLineServices;

    public function __construct()
    {
        $this->overViewTimeLineServices = new OverViewTimeLineServices();
    }

    /**
     * Get total account value
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');
        $responseData = $this->overViewTimeLineServices->newGetStatistic($user);

        return response()->json([ 'data' => $responseData ]);
    }
}
