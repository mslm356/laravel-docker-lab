<?php

namespace App\Http\Controllers;

use App\Enums\InvestmentType;
use App\Enums\Listings\ListingReviewStatus;
use App\Models\Listings\Listing;

class Welcome extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
        $listings = Listing::with(['type', 'coverImage', 'city'])
            ->where('is_visible', true)
            ->whereNotNull('authorized_entity_id')
            ->where('review_status', ListingReviewStatus::Approved)
            ->latest()
            ->get();

        $listings = $listings->map(function ($listing) {
            return (object)[
                'id' => $listing->id,
                'title' => $listing->title,
                'type_name' => $listing->type->name,
                'target' => $listing->target->formatByDecimal(),
                'gross_yield' => $listing->gross_yield,
                'dividend_yield' => $listing->dividend_yield,
                'days_remaining' => now()->diffInDays($listing->deadline, false),
                'hours_remaining' => now()->diffInHours($listing->deadline, false),
                'image_url' => optional($listing->coverImage)->url,
                'city_name' => $listing->city->name,
                'investment_type' => InvestmentType::getDescription($listing->investment_type),
                'can_invest' => $listing->can_invest
            ];
        });

        $howItWorks = [
            [
                'image' => asset('images/how-it-works/icon03.png'),
                'title' => __('website.how_it_works_list.register.title'),
                'description' => __('website.how_it_works_list.register.description'),
                'class' => '-mx-8 w-14',
                'isLandscape' => false,
            ],
            [
                'image' => asset('images/how-it-works/icon04.png'),
                'title' => __('website.how_it_works_list.charge_balance.title'),
                'description' => __('website.how_it_works_list.charge_balance.description'),
                'class' => '-mx-8 w-14 sm:-mx-10',
                'isLandscape' => false,
            ],
            [
                'image' => asset('images/how-it-works/icon01.png'),
                'title' => __('website.how_it_works_list.explore.title'),
                'description' => __('website.how_it_works_list.explore.description'),
                'class' => '-mx-6 w-14',
                'isLandscape' => false,
            ],
            [
                'image' => asset('images/how-it-works/icon05.png'),
                'title' => __('website.how_it_works_list.invest.title'),
                'description' => __('website.how_it_works_list.invest.description'),
                'class' => 'h-14 mt-6 -mx-8 sm:-mx-20',
                'isLandscape' => true,
            ],
            [
                'image' => asset('images/how-it-works/icon02.png'),
                'title' => __('website.how_it_works_list.enjoy.title'),
                'description' => __('website.how_it_works_list.enjoy.description'),
                'class' => 'h-20 -mx-2',
                'isLandscape' => true,
            ],
        ];

        return view('website.welcome', compact(['listings', 'howItWorks']));
    }
}
