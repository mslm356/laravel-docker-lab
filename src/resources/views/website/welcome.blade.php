@extends('layouts.website')

@section('content')
    <div class="py-2 text-center text-white bg-primary-500">
        الموقع نسخة تجريبية تحت الاختبار
    </div>
    @include('website.welcome-subviews.about-aseel')
    @include('website.welcome-subviews.listings')
    {{-- @include('website.welcome-subviews.why-invest') --}}
    @include('website.welcome-subviews.how-it-works')
    @include('website.welcome-subviews.newsletter')
    {{-- @include('website.welcome-subviews.aseel-in-numbers') --}}
    {{-- @include('website.welcome-subviews.featured-on') --}}
    {{-- @include('website.welcome-subviews.our-partners') --}}
@endsection
