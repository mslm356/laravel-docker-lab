<?php

namespace App\Support\Elm\AbsherOtp;

class ErrorCodes
{
    const configurationCodes = [
        '20009',
        '20001',
        '30001',
        '022',
        '024'
    ];

    const customerCodes = [
        '10006',
        '10007',

    ];

    const systemCodes = [
        '10008',
        '10004',
    ];
}
