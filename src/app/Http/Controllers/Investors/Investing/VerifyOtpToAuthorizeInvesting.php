<?php

namespace App\Http\Controllers\Investors\Investing;

use App\Http\Controllers\Controller;
use App\Services\Investors\Investment\VerifyOtpServices;
use Illuminate\Http\Request;

class VerifyOtpToAuthorizeInvesting extends Controller
{

    public $verifyOtpServices;

    public function __construct()
    {
        $this->verifyOtpServices = new VerifyOtpServices();
    }


    /**
     * Invest in the given listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, $token)
    {
        try {
            $request->request->add(['token' => $token]);
            $user = getAuthUser('web');
            $response = $this->verifyOtpServices->handle($request, $user);

            $response['message']=null;
            return $this->handelStatusResponce($response);

        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'INVESTMENTS-VERFY-OTP-INVESTMENTS');
        }
    }
}
