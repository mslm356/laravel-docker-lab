<?php

namespace App\Http\Controllers\Admins\Api\Listings\Investors;

use App\Actions\Listings\Reports\Investors\AttachInvestorsReportToUser;
use App\Models\Listings\Listing;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ExportInvestorsTransactionsReportController extends Controller
{
    /**
     * Get Investors list Report PDF
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Listing $listing)
    {

        $user = Auth::guard('sanctum')->user();

        if($user && $user->authorized_entity_id == 5) {
            return response()->json(['message' => __('service not available now') ], 422);
        }

        $file = AttachInvestorsReportToUser::run(
            $user,
            $listing
        );
        return $file->download();

    }
}
