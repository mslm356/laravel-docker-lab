<?php

namespace App\Console\Commands;

use App\Models\Listings\Listing;
use App\Models\Listings\ListingImage;
use Illuminate\Console\Command;

class UpdateDefaultImageForListing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listing:update-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $images=ListingImage::where('index',0)->get();
        $images=ListingImage::all();
        foreach($images->groupby('listing_id') as $image)
        {
            $first_image=$image->sortBy('index')->first();



            if ($first_image)
            {
                echo 'first image path'."\n".$first_image->path."\n\n";
                $listing=Listing::where('id',$first_image->listing_id)->first();
                $listing->default_image=$first_image->path;
                $listing->save();

                echo $listing->title_ar."\n".$listing->default_image."\n\n";

            }
            else{
                echo 'no image found';
            }

        }
    }
}
