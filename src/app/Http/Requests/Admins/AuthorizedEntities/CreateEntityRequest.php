<?php

namespace App\Http\Requests\Admins\AuthorizedEntities;

use App\Http\Requests\AuthorizedEntities\CreateEntityRequest as AuthorizedEntitiesCreateEntityRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateEntityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return (new AuthorizedEntitiesCreateEntityRequest())->rules();
    }
}
