<?php

return [
    'subject' => 'Reply to Ticket #:id',
    'body' => 'You have a new reply:',
    'original_ticket' => 'This is your original ticket message:',
    'reply_by_reply_to_this' => 'You can continue following up by replying to this message.'
];
