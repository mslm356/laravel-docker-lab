<?php

namespace App\Models\Investors;

use App\Models\AppModel;
use App\Models\Users\User;
use App\Models\AML\AmlHistory;
use App\Models\Users\NationalIdentity;

class InvestorProfile extends AppModel
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_saudi' => 'boolean',
        'data' => 'array',
        'is_active' => 'boolean',
        'current_invest' => 'array',
        'invest_objective' => 'array',
        'is_on_board' => 'boolean',
        'is_kyc_completed' => 'boolean',
        'is_identity_verified' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function nationalIdentity()
    {
        return $this->hasOne(NationalIdentity::class, 'user_id', 'id');
    }

    public function nationalIdentityForKYC()
    {
        return $this->hasOne(NationalIdentity::class, 'user_id');
    }

    /**
     * Get all aml history
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function amlHistory()
    {
        return $this->hasMany(AmlHistory::class, 'investor_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function GetIsVerifiedByAbsherAttribute()
    {
        return $this->data['is_verified_by_absher'] ?? false;
    }
}
