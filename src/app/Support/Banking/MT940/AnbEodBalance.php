<?php

namespace App\Support\Banking\MT940;

use Jejik\MT940\Balance;

class AnbEodBalance extends Balance
{
    /**
     * Debit/Credit/Reverse Credit/Reverse Debit
     *
     * @var string
     */
    protected $amountAsString = '0';

    /**
     * Setter for amount
     */
    public function setAmountAsString($amount)
    {
        $this->amountAsString = $amount;
        return $this;
    }

    /**
     * Getter for amount
     */
    public function getAmountAsString()
    {
        return $this->amountAsString;
    }
}
