<?php

namespace App\Models;

use App\Models\Users\User;

class UserFirebaseToken extends AppModel
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
