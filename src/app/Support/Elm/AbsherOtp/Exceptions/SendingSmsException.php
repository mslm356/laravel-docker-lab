<?php

namespace App\Support\Elm\AbsherOtp\Exceptions;

use Exception;

class SendingSmsException extends Exception
{
    public function __construct()
    {
        parent::__construct('Failed to send SMS verification code');
    }
}
