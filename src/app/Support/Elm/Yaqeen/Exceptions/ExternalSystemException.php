<?php

namespace App\Support\Elm\Yaqeen\Exceptions;

use Exception;

class ExternalSystemException extends Exception
{
    protected $errorCode;
    protected $errorType;
    protected $errorMsg;

    public function __construct($errorCode, $errorType, $errorMsg)
    {
        $this->errorCode = $errorCode;
        $this->errorType = $errorType;
        $this->errorMsg = $errorMsg;
        parent::__construct('System not available');
    }

    public function context()
    {
        return [
            'error_type' => $this->errorType,
            'error_code' => $this->errorCode,
            'error_msg' => $this->errorMsg,
        ];
    }
}
