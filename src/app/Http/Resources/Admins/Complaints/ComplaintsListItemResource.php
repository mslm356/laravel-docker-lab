<?php

namespace App\Http\Resources\Admins\Complaints;

use Illuminate\Http\Resources\Json\JsonResource;

class ComplaintsListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'owner' => $this->user_name,
            'subject' => $this->subject,
            'status' => [
                'value' => $this->status->id,
                'description' => $this->status->name,
            ],
            'ticket_type' =>$this->type_name,
            'created_at' =>$this->created_at,
            'reviewer' => $this->reviewer->full_name ?? '',
        ];
    }
}
