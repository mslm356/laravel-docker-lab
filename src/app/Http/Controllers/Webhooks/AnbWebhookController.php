<?php

namespace App\Http\Controllers\Webhooks;

use App\Enums\Anb\Webhook;
// use App\Models\AnbWebhook;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\Anb\AnbEodStatement;
use App\Jobs\Anb\ProcessPaymentReceivedOrCompletedWebhook;
use Illuminate\Support\Facades\Log;

class AnbWebhookController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $inputs = $request->input();

        switch ($inputs['type']) {
            case Webhook::StatementCreated:
                AnbEodStatement::dispatch();
                break;

            case Webhook::PaymentReceived:
            case Webhook::PaymentCompleted:
                break;

            default:
                Log::alert('Unhandled ANB webhook', $inputs);
                break;
        }

        return response()->json();
    }
}
