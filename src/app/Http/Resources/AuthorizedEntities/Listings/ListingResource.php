<?php

namespace App\Http\Resources\AuthorizedEntities\Listings;

use App\Enums\Banking\BankCode;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Listings\ListingResource as CommonListingResource;

class ListingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingResource($this->resource))->toArray($request),
            [
                'bank_account' => $this->bankAccount ?
                    BankCode::getDescription($this->bankAccount->bank) . ' - ' . $this->bankAccount->iban :
                    null
            ]
        );
    }
}
