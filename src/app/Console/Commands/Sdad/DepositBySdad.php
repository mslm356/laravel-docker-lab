<?php

namespace App\Console\Commands\Sdad;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Services\LogService;
use App\Services\Sdad\Enums\InvoiceCodesStatusesEnums;
use App\Services\Sdad\Repository\SingleInvoiceInquiryRepository;
use App\Services\Sdad\Repository\SingleInvoicesRepository;
use App\Services\Sdad\Services\SingleInvoicePaymentNotificationService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DepositBySdad extends Command
{

    public $payment_notification_service;
    public $invoice_create_repo;
    public $invoice_inquiry_repo;
    public $logService;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sdad:special {--internal_code=} {--deposit=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check sdad invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->payment_notification_service = new SingleInvoicePaymentNotificationService();
        $this->invoice_create_repo = new SingleInvoicesRepository();

        $this->invoice_inquiry_repo = new SingleInvoiceInquiryRepository();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            $invoice = $this->invoice_create_repo->show($this->option('internal_code'));

            if (!$invoice) {
                dd('invoice not valid');
            }

            if ($invoice->status != InvoiceCodesStatusesEnums::PAID) {

//                $inquiry_result = $this->invoice_inquiry_repo->initiate_provider($invoice->edaat_invoice_id)->inquiry($invoice->edaat_invoice_id);

//                dump($inquiry_result, ['status'=> $invoice->status,  'user_id' => $invoice->user->id]);

//                if ($inquiry_result->Body->StatusCode == InvoiceCodesStatusesEnums::PAID) {

                    if ($this->option('deposit') && $this->option('deposit') == 'add' ) {

                        $inquiry_data = [
                            'status' => 19,
//                            'invoice_brief_result' => $inquiry_result
                        ];

                        $invoice = $this->invoice_create_repo->update($invoice->id, $inquiry_data);

                        $user = $invoice->user;
                        $userWallet = $user->getWallet(WalletType::Investor);

                        $created_at = now()->format('Y-m-d');

                        $userWallet->deposit(
                            $invoice->amount * 100,
                            [
                                'reference' => transaction_ref(),
                                'reason' => TransactionReason::SdadReason,
                                'details' => [
                                    'version' => 1,
                                    'source' => 'sdad-cmd',
//                                    'eptn' => $payment_obj['EPTN'],
//                                    'invoice_id' => $inquiry_result->Body->InvoiceNo,
//                                    'data' => $inquiry_result,
                                ],
                                'value_date' => $created_at,
                            ]
                        );
                        $userWallet->refreshBalance();

                        dump('payment done');

                        return true;
                    }

                    dump('inQuery done');

                    return true;
//                } else {
//                    dump('edaat status not paid');
//                    return false;
//                }

            } else {
                dump('assel invoice status already paid');
                return false;
            }

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }

    }
}
