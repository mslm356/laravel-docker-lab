<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Actions\Listings\Wallets\ExportListingTransactions as WalletsExportListingTransactions;
use App\Actions\Listings\Wallets\MapsWalletType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Listings\Listing;

class ExportListingTransactions extends Controller
{
    use MapsWalletType;

    /**
     * Get Investors list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, Listing $listing)
    {
        $data  = $this->validate($request, [
            'start_date' => ['required', 'date_format:Y-m-d'],
            'end_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:start_date'],
            'extension' => ['required', 'in:xlsx,csv'],
            'wallet_type' => ['required', 'in:SPV,ESCROW']
        ]);

        return WalletsExportListingTransactions::run($listing, $data);
    }
}
