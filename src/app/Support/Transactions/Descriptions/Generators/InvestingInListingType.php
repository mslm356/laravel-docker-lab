<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorBase;
use Illuminate\Support\Arr;

class InvestingInListingType extends GeneratorBase
{
    protected function generateForCredit(Transaction $transaction, $locale)
    {
        $items = Arr::only($transaction->meta, ['user_id', 'user_name']);

        if (count($transaction->meta) < 2) {
            return __('transaction-description.credit.invest_in_listing_general');
        }

        $userId = $items['user_id'];
        $userName = $items['user_name'];

        return __('transaction-description.credit.invest_in_listing', [
            'user_id' => $userId,
            'user_name' => $userName,
        ], $locale);
    }

    protected function generateForDebit(Transaction $transaction, $locale)
    {
        $items = Arr::only($transaction->meta, ['shares', 'listing_title_en', 'listing_title_ar']);

        if (count($items) < 2) {
            return __('transaction-description.debit.invest_in_listing_general');
        }

        $shares = $items['shares'];
        $name = $items[locale_suff('listing_title')];

        return __('transaction-description.debit.invest_in_listing', ['name' => $name, 'shares' => $shares], $locale);
    }
}
