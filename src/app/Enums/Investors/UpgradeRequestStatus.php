<?php

namespace App\Enums\Investors;

use BenSampo\Enum\Enum;

final class UpgradeRequestStatus extends Enum
{
    const PendingReview = 1;
    const InReview = 2;
    const Approved = 3;
    const Rejected = 4;
}
