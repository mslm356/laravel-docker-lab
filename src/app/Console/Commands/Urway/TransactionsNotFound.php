<?php

namespace App\Console\Commands\Urway;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Exports\Wallets\FailedTransactionUrway;
use App\Models\Listings\Listing;
use App\Models\Urway\UrwayTransaction;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use Carbon\Carbon;
use Cknow\Money\Money;
use Dflydev\DotAccessData\Data;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class TransactionsNotFound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'urway:transactions-not-found';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'transactions-not-found';

    public $urwayServices;
    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->urwayServices = new URwayServices();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        return DB::transaction(function () {

            try {


                $items = [

//                    'jl3M4FEzAnJJOsMz8bWB0eRs9',
//                    '9xRD6UvWaFbOKwtXKtWisAaEc',
//                    'RuCRJMqMDoSIXEbPbYgQXtPKe',
//                    '28ENvgZhXlkQQYJmpvXHjDLQw',
//                    'gOjf6ZFAe3PWapLtaeFs4cU8U',
//                    'SkC3kx9lUU6Ncis6hibtOGDMD',
//                    'bDSdw4G1o7a1J4IA5Ya3xd9Z2',
//                    'f2RDGnxkGzpCttLrjt4l8ki6M',




//                    'Zn1yHxrqc1edlpBF2lGx0t6jc',
//'CiH3vCoAnAzyHJelQDjQJ6mtd',
//'hWsoiVSl7CuEwjio0n6VyLexo',
//'QsjDFLEOUmbWyNfNeeQAriAJx',
//'RTy4B0slWf6EYqO5wz9hdaade',
//'5iCuc3Rwhi0JWkpOzEe6cZRJ9',
//'hapkjAIqtRG278xr1DhnlVyM4',
//'ulh4750QBNk6UekI75BU4Mabh',
//'X597mdVpzNawqRt9F7oS6Vqxt'


//                    'Y0ELBEyR2LikG6qajx870mWIO',
//                    'rbzx8FUcfuqeu2gaw2GxPE3O7',
//                    'w0A3C5hXwtW9i0m9n2JraYQRO',
//                    'a9Fp73KReSNkhzMAIiN4sYDa1',
//                    'q5dcRUwprrQmAN5KaZHX39oTN',
//                    'GVIjqCLr3ox3CYW87Bsf4kFMQ',
//                    'VrrS1swtTdOrPj6M0NVJ8Svq5',
//                    'qtQ6tYbaImZFpLybtK6mnwIJf',
//                    'kmZh4OAEdzZY2FGcJLCsPCZ0R',
//                    '1oBFO2a0TlmEwcGa8Z40dVD2p',
//                    'WqhlmstUi6hGyqbhzP74vye1J',
//                    'bcvWiug9aZQ3rs5YmFEFC7TPs',
//                    'YjdCkxVxrnRsyG9qGLehQLz1s',
//                    'OIORS1Y2TIvCNjcJL2ZyA4OHX',
//                    'VDC31eWseP44HMs1zU5tZAhP8',
//                    'WJHlSZMOvgers2ItI2kVzV15N',
//                    'ZT2nIb57UP1xe7xGQMdnHZ2TQ',
//                    'RFjLFwkWVonfKrMnUzOQkEqLW',
//                    'D6zV3H40mCimdSwQ7o46HMvbs',
//                    'qe6UE8o3KJUjmyjcplpepN3ov',
//                    'Ghwh6G5S8mx7qjJRZhh1iVaOF',
//                    'aVoOOtju9aZROgKA0eZIiHOrs',
//                    'tw7JdWJBqCM163X7ng5tqogFl',
//                    'phtvMDVnPyvSIqSwH55TeSlFr',
//                    'oGRXeCdhytunAb0WIkVaIlcxA',
//                    '7g90SJ2kdCGoTg6oQGmw7oPFw',
//                    'YxxHRZAvUKBo4sksouAah6hQa',
//                    'J5lnZwvBbwAWs2NTHo8lYCwLC',
//                    'P196ffN2FIGSZoCw0nAljvxNF',
//                    'yVeeKDMyxtJ48qTK9gmu6ubyV',
//                    'n07CMGs738OBAeejfY0urwym8',
//                    'NqJxXUAqtUQjSxLwOVYuCohwC',
//                    'NqJxXUAqtUQjSxLwOVYuCohwC',
//                    'ejrAE6os7NkmNjppaZPbFJvnm',
//                    'SbUhTYvSDdRKRPQ4DjgLxRdOn',
//                    'gdiKbiKS8dBb4qHbDhv3PZlpG',
//                    'sR0UmKAROGF4ac4mdV7SZpqXh',
//                    '4XNN7RKPLQt5mQAh3Mxv7kEoD',
//                    'u6Jg0anEt2TIjSkTZMtaAdJCt',
//                    'c6rjAnYQRPpNV4KoYaIdpyJmA',
//                    'szcbldkrsZ1R6cQzzvgkk4KaD',
//                    '6rp2MT5l2C5WRV5MMW627yyGr',
//                    'hfZsDzitQ34Ys309d2dmEHPJD',
//                    'PR1cLoDOp3W9DJ0B6GH9mzqn9',
//                    'cedLhLmT0kKG6HeqXG2QpC6jy',
//                    '0T2GhVbq8bZfxRbjIJxcOJiPb',
//                    'kaFKiOVR44HmcGs3natOhaWi7',
//                    'V8SZMTlLOoW91dqrXAAgS6mvs',
//                    '6Z2eQdfaDQP0hg16sqIGl0ztR',
//                    'zn0EelO45ol694LPvvf4mdfq3',
//                    'YJx5Gl9Vx35xHwfn6Ddefyp26',
//                    'lx1jSrq2ETXEXNGa58MdGgFE9',
//                    'q5k73rmlcZ5gyiTFxMDzYbWyF',
//                    'CsTpmSF8nYtXQOMQqTuuOoUjU',
//                    'Z1XDkE1H9CvT2N4SSaAoLMjTk',
//                    'Pb9uTAsQSzkMKvbYdiHR9l7Yy',
//                    '0SJeH2faCeYzvK1HEK2Wf2ho5',
//                    '6368bEmTn9xlFCMa1SJtIDkYt',
//                    'ZmFZghjIEBGBdN1ae5QYgR8kD',
//                    'ybDXkiYgVImGcQld8GgoLoBA1',
//                    'JtEqNOXM0npwaJfHdZmlkV9Ad',
//                    'igS160MsMHFJfyFEbLLSowlFv',
//                    'QYCIKRACGMKNumodzNCkJ6F1g',
//                    'K0ZUQReoBE0jfN0gnADdKFnV7',
//                    'EngcHfnU4JxiJBthS1RU7aMRv',
//                    'GX8JC66kgI3lQJt3BrcFb2eEq',
//                    '97MZYe1VrYDmtuGbHawWi8Th4',
//                    'nXLFqEFgRbyJjbtZtN1Mli3xS',
//                    'sa4yyS2lGnF9XkcHputkFyu2z',
//                    'kPQ8Pvn4ykG57Wjaix0rMmVLy',
//                    'D7L5n1zP0uADfLSvU2WqGB48n',
//                    'L6EwMCpcUtZlDsrKfgS8Cxghj',
//                    '1whrbxD4pnwhooD0tH14dhLuG',
//                    'Xq5BjGeMVzoZ88HQpcXyPmjY7',
//                    'ddFp7PuEslOizLUilx4aTBuew',
//                    'pBwIsGcn3poO2FelLsJDp3nrJ',
//                    '3A5fqzEKWeCrKNOB0ve1pXxXO',
//                    'ISRXVMWJLjiEevKvH3w4Oa5nD',
//                    'N41fih8U2YlfXAS7tpYtZY5iV',
//                    'Iwpr8thropY457nJT62wpiyJZ',
//                    'LRqTsJfXubkvtJDr5k4CPNaVZ',
//                    'WCou3DGMdOBQYHVhzO8pgaMg0',
//                    'SqoHVmS80AaRGrO3myHgDc2BQ',
//                    'sCZYRP31gjrXmFJengR3RmDj4',
//                    'U6Q1WFSw9vN8b2jv4vvWBuKBb',
//                    'VJMAwGnnfdNCRQY4BTdn33eTc',
//                    'e8qbTfTwaNdoODiR8ZUm7KItY',
//                    'Lwre1LahJbWdx8O524t1zE1Z1',
//                    'BQcxMvYjwnfjsD2Uz7l0CiOqm',
//                    'qDNibYp4qS3TxrwSYhl86tBF9',
//                    '8RPf96LKAFaPe7VlufwOqciAZ',
//                    '0gFvp8CRVVtPFmuYMbvvJXUxL',
//                    'oSwNmfJqKLaQBPpSNJPdRXz2a',
//                    'sin2jwoDAxLQTzOEnTbL5fKyl',
//                    'fWpzoEFMtlTvIcrkhtvFkO94W',
//                    'WmBx66MoYoiiU92jaSWvxyT1w',
//                    'ywXgatT91bvTgM6XRhXE3GpDE',
//                    'EmaKKRfE0Ouw7xTxoF70Yf3s5',
//                    'uxQPwH4tm29bsuVcPfymeTJbx',
//                    'vnUSoRj8vGn7vkmegr25r5AAS',
//                    'BwDh3fMJ8dvEVW62J1l0kaE5P',
//                    'SvD1ZyvnDkgyS5QaZrng66696',
//                    'DZnNlho0Mnni24xj1d5VK8dy5',
//                    'dKfb9ZSXCsGdtXRBnTnRJD4IR',
//                    'hn4NxVNYgx9wFZBm76lfAfL7S',
//                    'Sv4POHS0bRYdZTR5VIIXoKE6l',
//                    'FpOsLiZ73n26tlUqtOj69r78t',
//                    '9VqA3eV7t3kSVwP1mwBXwh2ME',
//                    'YyFiN0DRdYLea3GDJlLukni5W',
//                    'UjnuVBGfWrmXemlhhbMKkbfM4',
//                    '4Won0FwJ51LDT9xbqmPAEHHzZ',
//                    'jHGjJd6kiN9K8Inumivw3p7YK',
//                    'efxT6cmlzVEpfwZgYONU9zyKG',
//                    '5d4Dkjbz512stMGFrY0aoxIfV',
//                    'ZWwbznul5r9QFVt1KV9S7nErR',
//                    'qvzKmzdXTYjqjLSxUVoZjxIoe',
//                    'MttCRd9uXnTpUj1bYVRubtTuG',
//                    'XhHdFe2sWLvr8sTVy3MWwkUuj',
//                    'YY5vsT8QACnjRPc7joqxPGoWH',
//                    'X9aBd6rAmHSTR5ZdyvhiO2gND',
//                    'mcYh9PLymZlok5tEpgUQhEnMy',
//                    'hKb6lDMqrN4OsmAa6cEIbd1Hr',
//                    'OOXXcHEGXf2CqyM0M93sJDXXc',
//                    'v0Re9eNnK8wwzWcQttVmHBRoW',
//                    'hEdqrpODoxBUWjuEibt6F5ifD',
//                    'lYmml47s3oIqpUJ7av2pyqgCW',
//                    '1BsiaMmmFyiRtMW8VDGflDoG9',
//                    'xbLkWYFFkAcFVjFv8PQ9bIrrL',
//                    'ejXROMHN8bdW4fHhszT1x2O8R',
//                    'qHZPlgh9PEIwXlXpc2DmFSnj1',
//                    'iet1aXZd5XisjKiewqboFCkW5',
//                    'zCs6slglE173DDS95ATHP0SqI',
//                    'LhZsK6c5KKyJjdyY431ie7ZJp',
//                    'cqQv129f4NcTcYwzhqSHot8iB',
//                    'L91LizkSjLqZk7fb0CFABtqy9',
//                    'hl8cQH5yazPzbOQrGYCftnMVY',
//                    'gnbugte1BZLBqBa3kwTZFFV9u',
//                    'kh8HGtsxUvx7AAhHk7dnhhLiQ',
//                    'Bjd5TMVWkQf6ss2gfzX7j0BYn',
//                    'fcQEsFRDbUu7DxO5EFf1PuLgP',
//                    'Y2z2swagdxSn42QfGU9BVjCS0',
//                    'fgt9iPjjzt3MjXRvOEk6lfisI',
//                    'bBIKHsynRHbba31Y0DlR5YNmz',
//                    'FwrwFL3975jcGMYqduKbpg83l',
//                    'fAPAaqrOlYhz4UB5XRTeLToAM',
//                    'BscWzOXlbJCKAyJVTQtQDSSbO',
//                    '4Ywflqtz7NXobDjzJg92m4Zwd',
//                    'lvyYi9okExlnX0lHCWRRxqioD',
//                    'xhAxlj3ojLZDMRmuHn1mPaKbi',
//                    'PKh0l8eFc44OONhGB2BxSQduI',
//                    'DUF5sH0GLZmagV3873VMmruKg',
//                    'LHMhQMPGAKcbgNWFWQGlYJ7wV',
//                    't3f3TgrMXteL12kSw3ImGoKDN',
//                    'pSDtS9Bf7HPYjqtEjxp2j9KIB',
//                    'NtODR8y3TyutVr2sdVrD1QQCI',
//                    'w1kBgste2kyTXaDqpShTyLNoe',
//                    'sAJbmfTrKMjFd28GXxHSTTHO6',
//                    '2qCzDTbYoYMULvlrFlOxe5cyD',
//                    '69DtyWCIvtMFBMHYk8WakMYM2',
//                    'y2c0wdL4BleU2gFpP9hlSlWk6',
//                    '77SppiLLePwLdZIUCBWlyIPPm',
//                    'lIPMEepOPVC0Mv3VlCi5b43cn',
//                    'HwmS0IOZtfy9N4tfS0dZM8j9E',
//                    'dDa2YiZyC22yHGUpyZWn3IAAY',
//                    'uMmBNKTt3N9wvQHkXMFEMmmyW',
//                    'TI7tUvUJkZWwXDcy5zIFsZJ69',
//                    'yZMmowM5V2dzoonlR5bUGh390',
//                    'eG7sldpG6MopVVyACeog4GK4H',
//                    'mZ3Oh2YH0IaGKA9ae68IyHNnq',
//                    'niWh2BXqGIqjvg2egMPIma4wm',
//                    'X6Nd51ghFdOv57CUgfO1KtjAv',
//                    'v51VUqqpPMhZbPCS8pAG5EikQ',
//                    'Z1YfTLMpXWWIufFv9gxYAaGSZ',
//                    'DyidGjej1KlSNat5Ty13ct2zG',
//                    'CP8tEy0Y6tzUlZFXq34gPWxl4',
//                    '9xRD6UvWaFbOKwtXKtWisAaEc',
//                    'jcCSkct01KGBOeYVURKrZpcpn',
//                    '9pKWnYZiZV9YcTBPq6OArQdS4',
//                    'FZniwzHLOGqDD9AFx3wKjDaQ8',
//                    'BEz59MDPV9ROpXuRxvV4esmmz',
//                    'ujkqn7RqcsPLhs6LN9gfZr94q',
//                    'HzGf7Y9oKppdO2UFh5YyUMO6E',
//                    'PMxim3Zp9UNPBWCkWMQnvnYXz',
//                    'YJhmE0Px7qEWxyZecuqxA8vTs',
//                    '5HaB5kVRnxhtX1uyeO5JUvfFr',
//                    '6NOFJHhJpuISgw78XsFIgskRk',
//                    'YYYufXtQwEQEqOolTsmQqV4Z5',
//                    'xkl4L4OS49SgcI3NLCTjzMIYI',
//                    'QDu29emQPUa7jmNZnlQVMajBt',
//                    'ZxP7OlZ6ElpRktgL1b8ESaBgF',
//                    'MEfLdOoIwT5sFzR44TdHpcYXD',
//                    'B6hwvuHze7gSPcvPnEf6zFU8c',
//                    'Q5YEvLWgIORWvtyOZDHRYx3Ln',
//                    'PHFirlofI3CjABpKroqiGNcHa',
//                    'atxgBX51vbVy8Y9WmyXfAaqvP',
//                    '78NnGLOA8LSrDIOQdHaKmLexJ',
//                    '2ZCjIOeQmOhUYaZQM5K4GZiEU',
//                    'ZFTs5ntzcEb0uWvwFxkxGjrVT',
//                    'clIBRX4ymcgHy0uubG8rwO0uR',
//                    'jS3PjOSKw11FiE53GcOukKu1Q',
//                    'MuFYz7mR3HYv8vLg7QiaLNK80',
//                    'bRHeAeg44mr9YxVVQ3eIslDmE',
//                    'M1za7dgpkaJVC1dzB707Xy1uO',
//                    'Jk89Tg8YjlVclDM283bXHsYLM',
//                    'AlGQarrZk2p7DxpyjKS0zPC8o',
//                    'CgeBs7wqB6a7dDKh85FlYAsrp',
//                    '6c1WcA2GmzI0oQUnjcbNhDRTM',
//                    'BdUD5gJrrWFm4zbLGdhrgd07Y',
//                    'BXen36oUeWekeGsiVFSNZWC4i',
//                    'gc8ecN08H08GSPnYbqVzE50lS',
//                    'uKx2YWomdJRfxRWWMPn6lgY0T',
//                    'jowyV22I3vC7nvhLdEg7FLTlX',
//                    'eP8gnqqEZszSs8wa3UL6p4Q2z',
//                    'pgiBF4MRIhicTpOmpqqq19K76',
//                    'mECDavfFk0Vt2fqFuw7klsgyD',
//                    '9vGKPnMsea1qI722DuUcYIWiX',
//                    'EJ8Sgo31ZSmcJvSIKdZPqR4MM'
                ];

                foreach ($items as $item) {

                    $token = InvestorSubscriptionToken::where('reference', $item)->first();

                    if (!$token) {
                        dump('token not found ' . $item);
                        continue;
                    }

                    $urwayTransaction = $token->urwayTransaction;

                    if (!$urwayTransaction) {
                        dump('urway transaction not found ' . $item);
                        continue;
                    }

                    $amount = \money($urwayTransaction->amount, defaultCurrency());

                    $responseOfUrway = $this->urwayServices->callUrway($token->reference, $amount, '');

                    if (isset($responseOfUrway['status']) && $responseOfUrway['status']) {

                        $result = $responseOfUrway['data'];

                        $urwayAmount = \money($result['amount'], defaultCurrency());

                        if ($urwayAmount->getAmount() != $amount->getAmount()) {
                            $this->logService->log('URWAY-TRANSACTIONS-Not-Found-AMOUNT', null, ['AMOUNT NOT EQUAL', $responseOfUrway, $item]);
                        }

                        if (isset($result['responseCode']) && $result['responseCode'] === '000' && $result['result'] === 'Successful') {

                            $transactionsCount = Transaction::where('reason', 8)->whereJsonContains('meta->details->trackid', $token->reference)->first();

                            if ($transactionsCount) {

                                if ($transactionsCount->amount != $urwayAmount->getAmount()) {

                                    $transactionsCount->amount = $urwayAmount->getAmount();
                                    $transactionsCount->save();

                                    dump('transaction amount updated ' . $item);
                                }

                                dump('transaction exists ' . $item);
                                continue;
                            }

                            $user = $token->user;

                            $userWallet = $user->getWallet(WalletType::Investor);

                            $userWallet->deposit(
                                $urwayAmount->getAmount(),
                                [
                                    'reference' => transaction_ref(),
                                    'reason' => TransactionReason::UrwayReason,
                                    'rrn' => $result['rrn'],
                                    'details' => [
                                        'version' => 1,
                                        'source' => 'urway',
                                        'tranid' => $result['tranid'],
                                        'trackid' => $result['trackid'],
                                        'rrn' => $result['rrn'],
                                        'urway_not_found' => 'failed',
                                    ],
                                    'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
                                ]
                            );

                            $userWallet->refreshBalance();

                            $urwayTransaction->status = 'refund';
                            $urwayTransaction->response = $result;
                            $urwayTransaction->save();

                            dump('insert successfully ' . $item);

                        } else {

                            $this->logService->log('URWAY-TRANSACTIONS-Not-Found-error-code', null, ['urway code result', $responseOfUrway]);
                        }

                    } else {

                        $this->logService->log('URWAY-TRANSACTIONS-Not-Found-RESPONSE', null, ['status false', $responseOfUrway]);
                    }
                }

            } catch (\Exception $e) {
                dump('error exception');
                $this->logService->log('UPDATE-FAILED-TRANSACTIONS-URWAY-EXCEPTION', $e);
            }

        });
    }
}
