<?php

use App\Enums\Role;
use App\Http\Controllers\Investors\ComplaintController;
use \App\Http\Controllers\MobileApp\Investors\Auth\forgetEmail;
use \App\Http\Controllers\MobileApp\Investors\Auth\GetAuthUser;
use App\Http\Controllers\MobileApp\Investors\Auth\KycController;
use \App\Http\Controllers\MobileApp\Investors\Auth\ChangePassword;
use App\Http\Controllers\MobileApp\Investors\Auth\LoginController;
use App\Http\Controllers\MobileApp\Investors\Auth\RegisterController;
use App\Http\Controllers\MobileApp\Investors\Auth\AbsherVerifyController;
use App\Http\Controllers\MobileApp\Investors\Auth\RefreshTokenController;
use \App\Http\Controllers\MobileApp\Investors\Auth\FirebaseTokenController;
use App\Http\Controllers\MobileApp\Investors\Auth\ForgetPasswordController;
use \App\Http\Controllers\MobileApp\Investors\Auth\InvestorProfileController;


Route::post('/register', [RegisterController::class, 'register']);
Route::post('/login', [LoginController::class, 'login']);
Route::post('login/verify-two-factor-auth', [LoginController::class, 'verifyCode']);

Route::post('/forget-password', [ForgetPasswordController::class, 'forgetPassword']);
Route::post('/verify-forget-password-code', [ForgetPasswordController::class, 'verifyForgetPasswordCode']);
Route::post('/reset-password', [ForgetPasswordController::class, 'resetPassword']);

Route::post('app-forget-phone', [forgetEmail::class, 'forgetPhone']);
Route::post('app-reset-phone', [forgetEmail::class, 'resetPhone']);
Route::post('refresh-token', [ RefreshTokenController::class, 'refreshToken']);//->middleware('jwt_check:api');


Route::middleware(['auth:api', 'jwt_check:api', 'role:' . Role::Investor])->group(function () {

    Route::post('/absher/verify', [AbsherVerifyController::class, 'verifyNin']);
    Route::post('/resend-verify-code', [AbsherVerifyController::class, 'resendAbsherSms']);
    Route::post('/save-password', [RegisterController::class, 'updatePassword']);

    Route::get('/profile-options', [KycController::class, 'index']);
    Route::get('/investor-kyc', [KycController::class,'getInvestorKyc']); // get kyc for investor


    Route::get('/register-terms-conditions', [KycController::class, 'getTermsConditions']);
    Route::put('/update-profile-investor', [ InvestorProfileController::class,'updateInvestorProfile']);
    Route::post('/confirm-unsuitability', [InvestorProfileController::class,'confirmUnsuitability']);


    Route::get('/my-profile', [ InvestorProfileController::class,'showMyProfile']);
    Route::get('/absher-info', [ InvestorProfileController::class,'showAbsherInfo']);
    Route::post('/app-logout', [LoginController::class, 'logout']);
    Route::post('/change-password', ChangePassword::class);

    Route::get('/auth', GetAuthUser::class);
    Route::post('/delete-account', [ InvestorProfileController::class,'deleteAccount']);

    Route::post('/update-firebase-token', [FirebaseTokenController::class, 'updateFirebase']);

//    Route::get('complaints/types', [ComplaintController::class, 'types']);
//
//    Route::post('complaints', [ComplaintController::class, 'store']);
});
