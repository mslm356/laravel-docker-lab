@extends('layouts.pdf')

@section('style')
    @include('components.print-components.watermark',
    [
    'website' => 'https://investaseel.sa' ,
    'user_name' => $user_name_en ,
    'date'=> \Carbon\Carbon::now()->format('Y-m-d H:i:s')
    ]
    )
@endsection
@section('content')
    <div dir="rtl">
        <div class="space-y-4">
            <img class="w-auto h-24 mx-auto" src="{{ asset('images/aseel-blue.png') }}" />

            <div class="px-6 py-5 text-center">
                <h3 class="text-2xl font-medium leading-6 text-gray-900">
                    الفاتورة
                </h3>
                <p class="mt-3 text-lg leading-5 text-gray-500">
                    Receipt
                </p>
            </div>

            <div>
                <x-subscription-form.field-inline :field="['en' => 'Transaction Date' , 'ar' => 'تاريخ العملية']">
                    {{ $transaction->created_at->format('Y-m-d') }}
                </x-subscription-form.field-inline>

                <x-subscription-form.field-inline :field="['en' => 'Reference' , 'ar' => 'الرقم المرجعي']">
                    {{ $transaction->reference }}
                </x-subscription-form.field-inline>
            </div>

            <div class="overflow-hidden bg-white border border-gray-300 rounded-lg">

                <div class="p-0">
                    <dl>
                        <div class="grid grid-cols-3 gap-4 px-6 py-5">
                            <dt class="self-center text-sm font-medium leading-5 text-gray-500">
                                نوع العملية <br>
                                Transaction type
                            </dt>
                            <dd class="self-center col-span-2 mt-0 text-sm leading-5 text-gray-900">
                                {{ $transaction->debit == 0.0 ? 'مدين' : 'دائن' }}
                            </dd>
                        </div>
                        <div class="grid grid-cols-3 gap-4 px-6 py-5 mt-0 border-t border-gray-200">
                            <dt class="self-center text-sm font-medium leading-5 text-gray-500">
                                المبلغ <br>
                                Amount
                            </dt>
                            <dd class="self-center col-span-2 mt-0 text-sm leading-5 text-gray-900">
                                {{ number_format(money($transaction->amount)->formatByDecimal(), 2) }}
                            </dd>
                        </div>
                        <div class="grid grid-cols-3 gap-4 px-6 py-5 mt-0 border-t border-gray-200">
                            <dt class="self-center text-sm font-medium leading-5 text-gray-500">
                                العملة <br>
                                Currency
                            </dt>
                            <dd class="self-center col-span-2 mt-0 text-sm leading-5 text-gray-900">
                                ريال سعودي
                            </dd>
                        </div>
                        <div class="grid grid-cols-3 gap-4 px-6 py-5 mt-0 border-t border-gray-200">
                            <dt class="self-center text-sm font-medium leading-5 text-gray-500">
                                وصف العملية <br>
                                Description
                            </dt>
                            <dd class="self-center col-span-2 mt-0 text-sm leading-5 text-gray-900">
                                {{ $description }}
                            </dd>
                        </div>
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection
