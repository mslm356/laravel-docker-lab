<?php

namespace App\Exports\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class TransactionBalanceReportExport implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $query = DB::table('transactions')
            ->select(DB::raw('DATE(created_at) as Date'),DB::raw('sum(amount)  as Amount'))
            ->whereIn('reason',[1,4,5,2,6,7])
            ->where('created_at', '>=', $this->data['start_date'])
            ->where('created_at', '<=', $this->data['end_date'])
            ->where('payable_type','App\Models\Users\User')
            ->groupBy('Date')->latest('Date')->get();
        return $query;
    }

    public function headings(): array
    {
        return [
            'Date',
            'Amount',
        ];
    }

    public function map($transaction): array
    {
        return [
            $transaction->Date,
            $transaction->Amount/100,
        ];
    }
}
