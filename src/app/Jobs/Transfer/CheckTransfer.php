<?php

namespace App\Jobs\Transfer;

use App\Services\Investors\TransferServices;
use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CheckTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $transferService;
    public $transferId;
    public $transfer_request_id;
    /**
     * @var LogService
     */
    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($transferId, $transfer_request_id = null)
    {
        $this->transferId = $transferId;
        $this->transfer_request_id = $transfer_request_id;
        $this->transferService = new TransferServices();
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $response = $this->transferService->checkTransferProcess($this->transferId, $this->transfer_request_id);
            DB::table('anb_transfers')
                ->where('id', $this->transferId)
                ->update(['job_response' => json_encode($response)]);

        } catch (\Exception $e) {
            $this->logService->log('CHECK-TRANSFER-JOB-ERROR', $e);
        }
    }
}
