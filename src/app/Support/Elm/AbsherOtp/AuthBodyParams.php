<?php

namespace App\Support\Elm\AbsherOtp;

class AuthBodyParams
{
    public static function get()
    {
        return [
            'clientId' => config('services.absher_otp.client_id'),
            'clientAuthorization' => config('services.absher_otp.client_auth'),
        ];
    }
}
