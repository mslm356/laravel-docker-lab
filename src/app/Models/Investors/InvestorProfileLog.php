<?php

namespace App\Models\Investors;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvestorProfileLog extends Model
{
    use HasFactory;

    protected $fillable=['user_id','data','type','message'];
}
