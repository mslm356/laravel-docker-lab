<?php

use App\Models\Users\User;
use Illuminate\Support\Str;
use App\Enums\Role as EnumRole;
use App\Enums\Banking\WalletType;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Migrations\Migration;

class AddAnbStatementCollectorAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            $user = User::create([
                'first_name' => "Statement",
                'last_name' => "Collector",
            ]);

            $user->assignRole(
                Role::firstOrCreate(
                    ['name' => EnumRole::AnbStatementCollector],
                    ['guard_name' => 'web']
                )
            );

            $user->createWallet([
                'name' => WalletType::AnbStatementCollecter,
                'slug' => WalletType::AnbStatementCollecter,
                'uuid' => (string) Str::uuid(),
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = User::role(EnumRole::AnbStatementCollector)->first();
        $user->removeRole(EnumRole::AnbStatementCollector);
        $user->forceDelete();
    }
}
