<?php

namespace App\Console\Commands\Investors;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\CheckIfCanInvest;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Actions\Investors\IsFirstInvestmentInListing;
use App\Actions\Listings\GetCurrentInvestments;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Notifications\NotificationTypes;
use App\Enums\Role;
use App\Exceptions\NoBalanceException;
use App\Exceptions\NoInvestorWalletFound;
use App\Jobs\Zatca\GenerateEInvoiceAfterInvesting;
use App\Mail\Investors\NewInvestmentTransaction;
use App\Models\Elm\AbsherOtp;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Services\CacheServices;
use App\Services\Investors\InvestmentServices;
use App\Services\Investors\TransactionServices;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Zatca\EInvoice\Order;
use App\Support\Zatca\EInvoice\PurchaseLine;
use App\Support\Zoho\JournalPrepareData\FundInvest;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class Investing extends Command
{
    use NotificationServices;

    const FAILURE_REASON_EXPIRED_AUTHORIZEATION_FOR_TOKEN = 1000;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'listing:invest {--shares=} {--user_id=} {--listing_id=} {--date=} {--notify=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'listing invest';

    public $transactionServices;
    public $investmentServices;
    public $fundInvest;
    public $cacheServices;
    public $urwayServices;
    public $logServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->transactionServices = new TransactionServices();
        $this->investmentServices = new InvestmentServices();
        $this->fundInvest = new FundInvest();
        $this->cacheServices = new CacheServices();
        $this->urwayServices = new URwayServices();
        $this->logServices = new LogService();
    }

    public function handle()
    {
        try {

            $data = [
                'user_id' => $this->option('user_id'),
                'listing_id' => $this->option('listing_id'),
//                'date' => Carbon::parse($this->option('date'))->addHours(11),
//                'notify' => $this->option('notify'),
            ];

//            if (!in_array($data['notify'], [1,0])) {
//                dd('notify should be =>  1,0');
//            }

            $user = User::where('id', $data['user_id'])->first();
            $listing = Listing::where('id', $data['listing_id'])->first();

            if (!$user) {
                dd('user not valid');
            }

            if (!$listing) {
                dd('Listing not valid');
            }

            return DB::transaction(function () use ($data, $user, $listing) {

                $token = InvestorSubscriptionToken::create([
                    'user_id' => $user->id,
                    'listing_id' => $listing->id,
                    'shares' => $this->option('shares'),
                    'reference' => Str::random(25),
                    'authorization_expiry' => Carbon::now(),
                    'payment_type' => 'wallet',
                ]);

                $listingConditions = [
                    ['id', $token->listing_id],
                    ['is_visible', true],
                    ['is_closed', false]
                ];

                if (in_array($token->listing_id, config('app.listing_can_invested')) && in_array($user->id, config('app.users_can_invest'))) {
                    unset($listingConditions[2]);
                }

                $listing = Listing::where($listingConditions)->find($token->listing_id);

                if (!$listing) {
                    dd('listing not valid');
                }

                $toBeInvestedShares = $token->shares;

                $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing, $toBeInvestedShares);

//                if (!$token || $token->authorization_expiry === null || now()->isAfter($token->authorization_expiry)) {
//                    dd('messages.investor_subscription_token_auth_expired');
//                }


                if ($user->investor && !$user->investor->is_kyc_completed) {
                    dd('messages.complete_your_profile');
                }

                if (!($listingWallet = $listing->getWallet(WalletType::ListingEscrow))) {
                    Log::critical(
                        'MISSING_ESCROW_ACCOUNT',
                        ["Missing escrow account for listing #{$listing->id}"]
                    );

                    dd('messages.missing_escrow');
                }

                $currentInvest = GetCurrentInvestments::run($listing)->get();

                $case = CheckIfCanInvest::run($listing, $user, $currentInvest, $toBeInvestedShares);

                if ((is_int($case) && $token->payment_type != 'urway') || is_int($case) && $case != 5 && $token->payment_type == 'urway') {
                    dd(trans('messages.invest_policy_' . $case));
                }

                $userWallet = $user->getWallet(WalletType::Investor);

                $identity = $user->nationalIdentity;

                $investingRecord = ListingInvestorRecord::create([
                    'investor_id' => $user->id,
                    'listing_id' => $listing->id,
                    'amount_without_fees' => $amountWithoutFees = $toBeInvestedAmountAndFees['amount']->getAmount(),
                    'admin_fees' => $adminFees = $toBeInvestedAmountAndFees['fee']->getAmount(),
                    'vat_amount' => $vatAmount = $toBeInvestedAmountAndFees['vat']->getAmount(),
                    'vat_percentage' => $vatPercentage = $toBeInvestedAmountAndFees['vatPercentage'],
                    'admin_fees_percentage' => $feePercentage = $toBeInvestedAmountAndFees['feePercentage'],
                    'subscription_token_id' => $token->id,
                    'currency' => defaultCurrency(),
                    'invested_shares' => $toBeInvestedShares,
                ]);

                $userWallet->transfer(
                    $listingWallet,
                    $amountWithoutFees,
                    $meta = [
                        'reason' => TransactionReason::InvestingInListing,
                        'reference' => transaction_ref(),
                        'value_date' => now('UTC')->toDateTimeString(),
                        'listing_id' => $listing->id,
                        'listing_title_en' => $listing->title_en,
                        'listing_title_ar' => $listing->title_ar,
                        'user_id' => $user->id,
                        'user_name' => "{$identity->first_name} {$identity->last_name}",
                        'shares' => $toBeInvestedShares,
//                        'notify' => $data['notify'],
                        'investing_record_id' => $investingRecord->id,
                    ]
                );

                $adminFeeTransaction = $userWallet->transfer(
                    $listing->getWallet(WalletType::ListingEscrowAdminFee),
                    $adminFees,
                    array_merge($meta, [
                        'reason' => TransactionReason::InvestingInListingAdminFee,
                        'reference' => $adminFeesTransactionRef = transaction_ref(),
                        'fee_percentage' => $feePercentage,
                    ])
                );

                $userWallet->transfer(
                    $listing->getWallet(WalletType::ListingEscrowVat),
                    $vatAmount,
                    array_merge($meta, [
                        'reason' => TransactionReason::InvestingInListingVatOfAdminFee,
                        'reference' => transaction_ref(),
                        'vat_percentage' => $vatPercentage,
                    ])
                );

                if (IsFirstInvestmentInListing::run($currentInvest, $user->id)) {
                    $user->investments()
                        ->attach($listing->id, [
                            'invested_amount' => $toBeInvestedAmountAndFees['amount']->getAmount(),
                            'invested_shares' => $toBeInvestedShares,
                            'currency' => defaultCurrency()
                        ]);
                } else {
                    DB::table('listing_investor')
                        ->where('listing_id', $listing->id)
                        ->where('investor_id', $user->id)
                        ->update([
                            'invested_shares' => DB::raw("invested_shares + {$toBeInvestedShares}"),
                            'invested_amount' => DB::raw("invested_amount + {$toBeInvestedAmountAndFees['amount']->getAmount()}"),
                        ]);
                }

                $token->update([
                    'expired_at' => now()
                ]);

//                $listing->update([
//                    'invest_percent' => $listing->percentage
//                ]);

                $subscriptionFormData = GetSubscriptionFormData::run(
                    $token,
                    $user,
                    $listing,
                    $toBeInvestedAmountAndFees['amount'],
                    $currentInvest->where('investor_id', $user->id)
                );

                ExportSubFormAndAttachToInvestingRecord::dispatch(
                    $investingRecord,
                    $token,
                    $user,
                    $listing,
                    $subscriptionFormData
                );

//                GenerateEInvoiceAfterInvesting::dispatch(
//                    $user,
//                    new Order(
//                        $adminFeesTransactionRef,
//                        [
//                            new PurchaseLine(
//                                [
//                                    'en' => "({$feePercentage}%) of investing in {$listing->title_en}",
//                                    'ar' => "(%{$feePercentage}) من مبلغ الاستثمار في {$listing->title_ar}",
//                                ],
//                                1,
//                                $toBeInvestedAmountAndFees['fee'],
//                                $vatPercentage,
//                                null
//                            )
//                        ],
//                        now('Asia/Riyadh'),
//                        $investingRecord
//                    ),
//                    app(ZatcaSettings::class)->seller,
//                    'zatca-e-invoices',
//                    $adminFeeTransaction
//                );

                dump('done  successfully');

//                $notificationData = $this->notificationData($listing, $toBeInvestedAmountAndFees['total']->formatByDecimal(), $toBeInvestedShares);
//
//                $this->send(NotificationTypes::Investment, $notificationData, $user->id);
//
//                $this->cacheServices->updateFundCache($listing->id);
//
//                Mail::to($user)->queue(new NewInvestmentTransaction($user, $listing, $toBeInvestedShares, $toBeInvestedAmountAndFees['total']));
            });

        } catch (ValidationException $e) {

            $this->logServices->log('INVESTMENT-MOBILE', $e);
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        } catch (AuthorizationException $e) {
            $this->logServices->log('INVESTMENT-MOBILE', $e);
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        } catch (VerificationSmsException $e) {
            $this->logServices->log('INVESTMENT-MOBILE', $e);
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        } catch (NoInvestorWalletFound $e) {
            $this->logServices->log('INVESTMENT-MOBILE', $e);
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        } catch (NoBalanceException $e) {
            $this->logServices->log('INVESTMENT-MOBILE', $e);
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        } catch (\Exception $e) {
            $this->logServices->log('INVESTMENT-MOBILE', $e);
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }

    }

    public function notificationData($listing, $amount, $shares)
    {

        $content_ar_1 = ' تم استلام مبلغ قدره: ' . $amount;
        $content_ar_2 = ' للاستثمار في فرصة: ' . $listing->title_ar;
        $content_ar_3 = ' عدد الوحدات ' . $shares;

        return [
            'action_id' => $listing->id,
            'channel' => 'mobile',
            'title_en' => 'Received Investment Amount',
            'title_ar' => 'تأكيد استلام مبلغ الاستثمار',
            'content_en' => 'An investment amount of has been received : ' . $amount . ' to invest in opportunity ' . $listing->title_en . ' and number of units: ' . $shares,
            'content_ar' => $content_ar_1 . $content_ar_2 . $content_ar_3,
        ];
    }
}
