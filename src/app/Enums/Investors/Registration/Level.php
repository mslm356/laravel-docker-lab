<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\DBColumInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class Level extends Enum implements LocalizedEnum,DBColumInterface
{
    const Low =   1;
    const Medium =   2;
    const High =   3;

    function getDBColumnName()
    {
        return 'level';
    }

}
