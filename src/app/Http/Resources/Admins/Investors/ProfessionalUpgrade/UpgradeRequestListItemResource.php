<?php

namespace App\Http\Resources\Admins\Investors\ProfessionalUpgrade;

use App\Http\Resources\Common\Investors\ProfessionalUpgrade\UpgradeRequestListItemResource as CommonUpgradeRequestListItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UpgradeRequestListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonUpgradeRequestListItemResource($this->resource))->toArray($request),
            [
                'user' => [
                    'id' => $this->user_id,
                    'name' => optional($this->user)->full_name,
                ],
                'reviewer' => optional($this->reviewer)->full_name,
                'type' => $this->user ? $this->user->type : null
            ]
        );
    }
}
