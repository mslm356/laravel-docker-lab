<?php

namespace App\Services\Sdad\Repository;

use App\Models\EdaatInvoice;
use App\Models\SdadBillConfirmation;
use App\Services\LogService;
use Illuminate\Database\Eloquent\Model;

class BillConfirmationRepository
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    /**
     * @inheritDoc
     */
    public function create(array $request_data): ?Model
    {
        try {
            $data = $this->prepare_data($request_data);
            $bill_confirmation = SdadBillConfirmation::create($data);
            return $bill_confirmation;
        } catch (\Exception $e) {
            $this->logService->log('Sdad-bill_confirmation_repo', $e, ['issue', json_encode($request_data)]);
            throw new \Exception('BillConfirmationRepository Creation error');
        }

    }

    /**
     * @inheritDoc
     */
    public function show($id): ?Model
    {
        return SdadBillConfirmation::findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function update($id, array $request_data): ?Model
    {
        $invoice = $this->show($id);
        if ($invoice->update($request_data)) {
            return $this->show($id);
        }
        return null;
    }

    public function prepare_data($request_data): array
    {
        $invoice = EdaatInvoice::find($request_data['InternalCode']);
        $data = [
            'edaat_invoice_id' => $invoice->id,
            'body' => $request_data,
        ];
        return $data;
    }
}
