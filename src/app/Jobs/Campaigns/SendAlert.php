<?php

namespace App\Jobs\Campaigns;

use App\Enums\Campaigns\CampaignStatus;
use App\Models\Campaigns\Campaign;
use App\Services\Campaigns\InvestorTypes\ManageData;
use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class SendAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $campaign;
    public $data;
    public $alertClass;
    public $logService;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $campaign, $alertClass)
    {
        $this->alertClass = $alertClass;
        $this->campaign = $campaign;
        $this->data = $data;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $campaign = Campaign::find($this->campaign->id);

            if ($campaign->version != $this->campaign->version) {
                $this->logService->log('Campaigns:check-version', null, ['campaign version not valid']);
                return;
            }

            $campaign->status = CampaignStatus::Running;
            $campaign->save();

            $manageDataObj = new ManageData();

            $dataUsedToSend = $manageDataObj->dataUsedToSend($campaign);

            $className = "App\\Services\\Campaigns\\" . $this->alertClass . "Type";

            $generator = new $className($this->data, $campaign);

            $generator->sendCampaign($dataUsedToSend, $campaign);

        } catch (\Exception $e) {
            $this->logService->log('Campaigns:set-users-ex', $e);
        }
    }


}
