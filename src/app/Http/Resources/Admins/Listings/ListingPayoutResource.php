<?php

namespace App\Http\Resources\Admins\Listings;

use App\Http\Resources\Common\Listings\ListingPayoutResource as CommonListingPayoutResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingPayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingPayoutResource($this->resource))->toArray($request),
            []
        );
    }
}
