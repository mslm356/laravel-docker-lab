<?php

namespace App\Models\Enquiries\Traits;

use Propaganistas\LaravelPhone\PhoneNumber;

trait MutatesAttributes
{
    /**
     * Mutate the name
     *
     * @param string|null $value
     * @return void
     */
    public function getNameAttribute($value)
    {
        return $value ?: $this->user->full_name;
    }

    /**
     * Mutate the email
     *
     * @param string|null $value
     * @return void
     */
    public function getEmailAttribute($value)
    {
        return $value ?: $this->user->email;
    }

    /**
     * Mutate the phone number
     *
     * @param string|null $value
     * @return void
     */
    public function getFmtPhoneAttribute()
    {
        return !$this->user_id ?
            PhoneNumber::make(
                $this->phone_number,
                $this->phone_country_iso2
            )->formatE164()
            : $this->user->phone_number;
    }
}
