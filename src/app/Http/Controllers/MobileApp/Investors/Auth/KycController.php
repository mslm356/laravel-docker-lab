<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Profile\KycResource;
use App\Models\WebsiteContent;
use App\Services\Investors\RegisterServices;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class KycController extends Controller
{

    public $registerServices;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->registerServices = new RegisterServices();
    }

    public function index()
    {
        try {
            $options = $this->registerServices->getKyc();
            return apiResponse(200, __('mobile_app/message.success'), $options);

        } catch (\Exception $e) {
            $code = $this->logService->log('GET-KYC', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }

    }

    public function getTermsConditions()
    {
        try {
            $content = WebsiteContent::where('name', 'terms-and-conditions')->first();
            return apiResponse(200, __('mobile_app/message.success'), $content->content->get(App::getLocale()));

        } catch (\Exception $e) {
            $code = $this->logService->log('GET-TERMS-CONDITIONS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    public function getInvestorKyc(Request $request)
    {

        $user = getAuthUser('api');
        return new KycResource($user);
    }
}
