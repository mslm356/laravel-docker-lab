<?php

namespace App\Http\Resources\Investors;

use App\Http\Resources\Common\TransactionResource as CommonTransactionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonTransactionResource($this->resource))->toArray($request),
            [
                'receipt_link' => $this->file ?  $this->file->temp_full_url : null,
                'get_file_url' => $this->file ? route('investor.files.get.url', ['file'=> $this->receipt_file_id]) : null ,
            ]
        );
    }
}
