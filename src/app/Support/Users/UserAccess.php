<?php


namespace App\Support\Users;

use App\Services\LogService;

class UserAccess
{

    public $logServices;

    public function __construct()
    {
        $this->logServices = new  LogService();
    }

    public function handle($user, $module = null)
    {
        try {
            $response = [
                'status' => true,
            ];

            $userStatusValues = $this->userStatusValues($user);

            $responseOfCheckUserStatusValues = $this->checkUserStatusValues($userStatusValues, $module);

            if (isset($responseOfCheckUserStatusValues['status']) && !$responseOfCheckUserStatusValues['status']) {
                $response['status'] = false;
                $response['message'] = isset($responseOfCheckUserStatusValues['message']) ? $responseOfCheckUserStatusValues['message'] : 'you not have access';

                return $response;
            }

            return $response;

        } catch (\Exception $e) {
            $code = $this->logServices->log('User-Can-Access-Exception', $e);
            return [
                'status' => false,
                'message' => 'sorry user cant access that: ' . $code,
            ];
        }
    }

    public function userStatusValues($user)
    {
        return [

            'user_valid' => [
                'value' => $user ? true : false,
                'true_value' => true,
            ],

            'nationalIdentity_valid' => [
                'value' => $user && $user->nationalIdentity ? true : false,
                'true_value' => true,
            ],

            'complete_your_profile' => [
                'value' => $user && $user->investor && $user->investor->is_kyc_completed ? true : false,
                'true_value' => true,
            ],

            'identity_verified_valid' => [
                'value' => $user && $user->investor && $user->investor->is_identity_verified ? true : false,
                'true_value' => true,
            ],

            'status_approved' => [
                'value' => $user && $user->investor && in_array($user->investor->status, [1, 3]) ? true : false,
                'true_value' => true,
            ],

            'account_not_approved' => [
                'value' => $user && $user->investor && $user->investor->status == 3 ? true : false,
                'true_value' => true,
            ],
            'check_kyc_suitability' => [
                'value' => (getKycSuitability($user->investor, $user) || (!getKycSuitability($user->investor, $user) && $user->investor->unsuitability_confirmation)) ? true : false,
                'true_value' => true,
            ],

//            Arr::get($user->investor->data, 'investment_xp') != 3|| $user->investor->annual_income == 1 || Arr::get($user->investor->data, 'years_of_inv_in_securities') == 0

        ];
    }

    public function checkUserStatusValues($userStatusValues, $module)
    {

        $response = ['status' => true];

        if ($module) {
            $moduleData = $this->$module();
        }

        foreach ($userStatusValues as $key => $status) {

            if ($module && !in_array($key, $moduleData)) {
                continue;
            }


            if ($status['value'] != $status['true_value']) {
                $response['status'] = false;
                $response['message'] = __('messages.' . $key);
                break;
            }
        }

        return $response;
    }

    public function login()
    {
        return ['status_approved', 'user_valid'];
    }

    public function upgradeRequest()
    {
        return ['user_valid', 'nationalIdentity_valid', 'complete_your_profile', 'identity_verified_valid','account_not_approved'];
    }

    public function investment()
    {
        return ['user_valid', 'nationalIdentity_valid', 'complete_your_profile', 'identity_verified_valid', 'account_not_approved','check_kyc_suitability'];
    }

    public function transfer()
    {
        return ['user_valid', 'nationalIdentity_valid', 'complete_your_profile', 'identity_verified_valid', 'account_not_approved'];
    }

    public function bankAccount()
    {
        return ['user_valid', 'nationalIdentity_valid', 'complete_your_profile', 'identity_verified_valid', 'status_approved'];
    }

    public function admin()
    {
        return ['user_valid'];
    }
}
