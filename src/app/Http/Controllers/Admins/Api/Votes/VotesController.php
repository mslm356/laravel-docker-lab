<?php

namespace App\Http\Controllers\Admins\Api\Votes;

use App\Actions\Listings\Votes\GetVotes;
use App\Enums\Notifications\NotificationTypes;
use App\Exports\Votes\InvestorsVoteExport;
use App\Http\Resources\Admins\Listings\ShowVoteResource;
use App\Http\Resources\Admins\Listings\VotesResource;
use App\Jobs\Funds\SendAlertMailsOnCreate;
use App\Models\InvestorVote;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\Vote;
use App\Services\LogService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enums\Role as EnumsRole;
use App\Http\Resources\Admins\AuthResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;

class VotesController extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function index(Listing $listing)
    {
        try {
            return VotesResource::collection(GetVotes::run($listing));
        } catch (\Exception $e) {
            $code = $this->logService->log('store-votes', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function show(Vote $vote)
    {
        try {

            $voteInvestors = InvestorVote::query()
                ->where('vote_id', $vote->id)
                ->join('users', function ($join) {
                    $join->on('users.id', '=', 'investor_votes.investor_id');
                })
                ->join('listing_investor', function ($join) use($vote) {
                    $join->on('listing_investor.investor_id', '=', 'investor_votes.investor_id')
                        ->where('listing_investor.listing_id', $vote->fund_id)
                        ->where('listing_investor.invested_shares','>',0);
                })->select('investor_votes.id as investor_vote_id',
                    DB::raw('CONCAT(users.first_name, " ",  users.last_name) as full_name'),
                    'listing_investor.invested_shares', 'investor_votes.created_at', 'investor_votes.answer')
                ->paginate();

            return ShowVoteResource::collection($voteInvestors);

        } catch (\Exception $e) {
            $code = $this->logService->log('show-vote', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function store(Request $request, Listing $listing)
    {
        try {

            $data = $this->validator($request->all())->validate();

            $saveData['fund_id'] = $listing->id;
            $saveData['question'] = $data['vote'];

            Vote::create($saveData);

            $voteJob = (new \App\Jobs\Listings\Vote($listing));
            dispatch($voteJob);

            return apiResponse(201, __('mobile_app/message.success'));

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            $code = $this->logService->log('ValidationException-store-votes', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('store-votes', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function validator($data)
    {
        return Validator::make($data, [
            'vote' => ['required', 'string',textValidation(),'max:500'],
        ]);
    }

    public function endVote($id)
    {
        try {
            $vote = Vote::find($id);
            if (!$vote)
                return apiResponse(400, __('messages.vote_not_found'));

            $vote->update(['status' => 1]);

            return apiResponse(200, __('messages.end_voting'));

        } catch (\Exception $e) {
            $code = $this->logService->log('store-votes', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }

    }

    public function download(Vote $vote)
    {
        try {

            $data = [
                'vote_id'=> $vote->id,
                'fund_id' => $vote->fund_id,
            ];

            return Excel::download(new InvestorsVoteExport($data), 'Investors_vote' . ".xlsx");

        } catch (\Exception $e) {
            $code = $this->logService->log('download-votes', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }
}
