<?php

use App\Models\City;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en', 80);
            $table->string('name_ar', 80);
            $table->timestamps();
        });

        $cities = json_decode(Storage::disk('local')->get('cities.json'), true);

        foreach ($cities as $city) {
            City::create([
                'name_en' => $city['name_en'],
                'name_ar' => $city['name_ar'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
