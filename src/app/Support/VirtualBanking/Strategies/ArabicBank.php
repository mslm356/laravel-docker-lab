<?php

namespace App\Support\VirtualBanking\Strategies;

use App\Enums\Banking\BankCode;
use App\Enums\QueryLockType;
use App\Models\Anb\AnbTransfer;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\Ledger;
use App\Services\LogService;
use App\Support\VirtualBanking\BankServiceInterface;
use App\Models\VirtualBanking\VirtualAccount;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\AnbNarrative;
use App\Support\Anb\Enums\AnbPaymentStatus;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Support\VirtualBanking\Helpers\LocalSaudiTransferFee;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ArabicBank implements BankServiceInterface
{
    public $logServices;

    public function __construct () {
        $this->logServices = new LogService();
    }


    protected $modelAlias = [
        User::class => '1',
        Listing::class => '2',
    ];


    /**
     * Create wallet (Virtual Account)
     *
     * @return array
     */
    function openVirtualAccount($model, $id = null, $suffix = null): array
    {
        if ($id === null) {
            $id = $model->getKey();
        }

        if ($suffix === null) {
            $suffix = '01';
        }

        $accountNoPrefix = $this->modelAlias[get_class($model)] ?? '3';

        $remainingDigits = 11 - strlen($accountNoPrefix) - strlen($suffix);

        $paddedId = str_pad((string)$id, $remainingDigits, '0', STR_PAD_LEFT);

        $accountNo = $accountNoPrefix . $paddedId . $suffix;

        return AnbVA::generate($accountNo);
    }

    /**
     * Transfer money
     *
     * @return array|false (current amount)
     */
    function transfer(
        array $custInfo,
        string $toIban,
        $amount,
        $bankCode,
        $tranactionId,
        $valueDate,
        $isFeeIncluded = true,
        $transferPurpose = '39',
        $currency = 'SAR',
        $bankAccount = null,
        $transfer = null
    ) {

        $ref = config('zoho.ANB_TRANSFER') == 'TESTING_ACCOUNT' ? randomDigitsStr() . $transfer->id : $transfer->id;

        $response = AnbApiUtil::transfer(
            $toIban,
            $amount->formatByDecimal(),
            $ref,
            $valueDate,
            $custInfo,
            null,
            null,
            $bankCode,
            AnbNarrative::make($transfer->id),
            $isFeeIncluded,
            $transferPurpose,
            $currency,
            $transfer->channel
        );

        $transfer->update([
            'external_id' => $response['id'],
            'external_trans_reference' => $response['transactionReferenceNumber'],
            'processing_status' => $response['status'],
            'transaction_id'=> $tranactionId
        ]);

        $status = $response['status'];

        $this->logServices->log('TRANSFER-FROM-WALLET', null, ['check-transfer-response', $response]);

        return in_array($status, AnbPaymentStatus::inProcessOrPaidStatuses());
    }

    /**
     * Get wallets for tranfer
     *
     * @param string $fromIban
     * @param string $toIban
     * @return array
     */
    public function getWalletsForTransfer($fromIban, $toIban)
    {
        $fromWallet = false;
        $toWallet = false;

        $isFromVA = AnbVA::isVA($fromIban);
        $isToVA = AnbVA::isVA($toIban);

        if ($isFromVA) {
            $fromWallet = VirtualAccount::where('iban', $fromIban)->sharedLock()->first();
        }
        if ($isToVA) {
            $toWallet = VirtualAccount::where('iban', $toIban)->sharedLock()->first();
        }

        if ($fromWallet === null || $toWallet === null) {
            throw new \Exception('VA NOT FOUND');
        }

        return [$fromWallet, $toWallet];
    }

    /**
     * Check wallet current balance
     * @return float (current amount)
     */
    function checkBalance(string $iban, $lockType = QueryLockType::LockForUpdate): float
    {
        $query = Ledger::selectRaw('sum(credit) total_credit, sum(debit) total_debit')
            ->where('iban', $iban);

        if ($lockType === QueryLockType::LockForUpdate) {
            $query->lockForUpdate();
        } else if ($lockType === QueryLockType::SharedLock) {
            $query->sharedLock();
        }

        $debitCredit = $query->first();

        $credit = (!is_null($debitCredit->total_credit)) ? $debitCredit->total_credit : 0;

        $debit = (!is_null($debitCredit->total_debit)) ? $debitCredit->total_debit : 0;

        $total = $credit - $debit;

        return $total;
    }

    /**
     * get wallet transaction history
     */
    function getHistory(string $iban, array $options): object
    {
        $ledger = Ledger::where('iban', $iban)->with('subscriptionFormFile');

        if (isset($options['latest'])) {
            $ledger = $ledger->latest()->limit(10)->get();
        } else if (isset($options['from']) && isset($options['to'])) {
            $dates = [$options['from'], $options['to']];
            $ledger = $ledger->latest()->period($dates)->paginate();
        } else {
            $ledger = $ledger->latest()->paginate();
        }

        return $ledger;
    }

    public function getTransferFeeOptions($toBankCode, $amount): array
    {
        return LocalSaudiTransferFee::getTransferFeeOptions(BankCode::Anb, $toBankCode, $amount);
    }

    /**
     * Validate that the given tranfer option can be used for transferring according to the given IBAN and amount
     *
     * @return bool
     */
    function validateTransferFeeOption($toBankCode, $amount, $fee): bool
    {
        $options = $this->getTransferFeeOptions($toBankCode, $amount);

        foreach ($options as $feeOption) {
            if ($feeOption->getFee() === $fee) {
                return true;
            }
        }

        return false;
    }

    /**
     * Suspend/ Stop wallet
     *
     */
    function changeStatus()
    {
    }
}
