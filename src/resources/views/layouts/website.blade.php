<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ App::getLocale() === 'ar' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.3.5/dist/alpine.min.js"></script>
    <script src="{{ asset('js/website/website.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/website.css') }}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-77SS64V9R7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-77SS64V9R7');
    </script>
</head>
<body class="flex flex-col h-screen">
        @if (Route::currentRouteName() !== 'home_page')
            <x-website.navbar />
        @endif

        <div class="{{ Route::currentRouteName() !== 'home_page' ? 'flex-grow' : '' }}">
            @yield('content')
        </div>

        <x-website.footer />
</body>
</html>
