<?php

use Illuminate\Database\Migrations\Migration;
use App\Enums\Role as EnumsRole;
use Spatie\Permission\Models\Role;

class AddingBimUserRoleToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::create(['name' => EnumsRole::PayoutsCollector, 'guard_name' => 'web']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::where('name', EnumsRole::PayoutsCollector)->delete();
    }
}
