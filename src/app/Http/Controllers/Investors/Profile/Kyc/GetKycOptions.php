<?php

namespace App\Http\Controllers\Investors\Profile\Kyc;

use App\Http\Controllers\Controller;
use App\Services\Investors\RegisterServices;

class GetKycOptions extends Controller
{

    public $registerServices;

    public function __construct()
    {
        $this->registerServices = new RegisterServices();
    }

    public function __invoke()
    {
        $options = $this->registerServices->getKyc();
        return response()->json([
            'data' => $options
        ]);
    }

}
