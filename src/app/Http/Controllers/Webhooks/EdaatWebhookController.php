<?php

namespace App\Http\Controllers\Webhooks;

use App\Services\Sdad\Services\SingleInvoiceBillConfirmationService;
use App\Services\Sdad\Services\SingleInvoicePaymentNotificationService;
use App\Http\Controllers\Controller;
use App\Services\LogService;
use Illuminate\Http\Request;

class EdaatWebhookController extends Controller
{

    public $bill_confirmation_service;
    public $payment_notification_service;
    public $logService;

    public function __construct()
    {
        $this->bill_confirmation_service = new SingleInvoiceBillConfirmationService();
        $this->payment_notification_service = new SingleInvoicePaymentNotificationService();
        $this->logService = new LogService();
    }

    public function bill_confirmation(Request $request)
    {
        try {

            $confirmation = $this->bill_confirmation_service->create_bill_confirmation($request->all());
            if ($confirmation) {
                return apiResponse(200, __('messages.confirmation_done_successfully'));
            }
            $this->logService->log('bill_confirmation_done', null, ['success', json_encode($request->all())]);
            return apiResponse(409, __('messages.data_issues'));
        } catch (\Exception $e) {
            $this->logService->log('Sdad-bill_confirmation', $e, ['issue', json_encode($request->all())]);
            throw new \Exception('SingleInvoiceBillConfirmationService create_bill_confirmation Creation error');
        }

    }

    public function payment_notification(Request $request)
    {
        try {

            $this->logService->log('SDAD-WEBHOOK-PAYMENT-NOTIFICATION-REQUEST', null, ['INFO', json_encode($request->all())]);

            $this->payment_notification_service->make_transaction($request->all());

            return apiResponse(200, __('messages.payment_done_successfully'));

        } catch (\Exception $e) {

            $this->logService->log('Sdad-payment_notification_issue', $e, ['error', json_encode($request->all())]);
            return apiResponse(409, __('messages.data_issues'));
        }
    }
}
