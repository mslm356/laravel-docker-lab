<?php

namespace App\Http\Controllers\Admins\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Enums\Role as EnumsRole;
use App\Http\Resources\Admins\AuthResource;
use Illuminate\Auth\Access\AuthorizationException;

class AdminController extends Controller
{
    /**
     * Show Admin Profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole(EnumsRole::Admin)) {
            return new AuthResource($user);
        }

        throw new AuthorizationException();
    }
}
