<?php

namespace App\Http\Controllers\Investors\Investing;

use App\Http\Controllers\Controller;
use App\Models\Listings\Listing;
use App\Services\Investors\Investment\SubscriptionTokenServices;
use Illuminate\Http\Request;

class CreateSubscriptionToken extends Controller
{

    public $subscriptionTokenServices;
    public $logService;

    public function __construct()
    {
        $this->subscriptionTokenServices = new SubscriptionTokenServices();
    }

    /**
     * Invest in the given listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, Listing $listing)
    {
        $request->request->add(['device' => 'web']);

        try {

            $response = $this->subscriptionTokenServices->handle($request, $listing);

            $customData = ['reference' => isset($response['data']['token']) ? $response['data']['token'] : null];

            return $this->handelStatusResponce($response,$customData);


        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'INVESTMENT-CREATE-TOKEN');
        }
    }
}
