<?php

namespace App\Console\Commands\Anb;

use App\Enums\Banking\BankCode;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Models\Anb\AnbStatementEvent;
use App\Models\Anb\AnbStatementTransaction;
use App\Models\Anb\AnbTransfer;
use App\Models\BankAccount;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Models\VirtualBanking\Wallet;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Auth;
use App\Support\Anb\Http\AnbHttp;
use App\Support\Anb\Http\HttpException;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Support\Zoho\CustomerPayments;
use Bavix\Wallet\Models\Transaction;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Cknow\Money\Money;
use Dflydev\DotAccessData\Data;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SpecialPullStmt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'special:pull-stmt {--from=} {--to=} {offset} {--process_type=} {--deposit=}';
    protected $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Anb pull stmt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            $accountNumber = config('services.anb.account_number');

            $offset = $this->argument('offset') ? substr($this->argument('offset'),7) : null; // "04-09-2022SD4292659      2   04-09-202214:04:170000000111870305.23";

            do {

                $msg = AnbApiUtil::statement(
                    $accountNumber,
                    $this->option('from'),
                    $this->option('to'), 100, $offset
                );

                $offset = isset($msg['offset']) ? $msg['offset'] : null;

                $is_complete = isset($msg['completed']) ? $msg['completed'] : true;

                dump($offset, $is_complete);

                if ($this->showTransaction($msg)) {
                    break;
                }

            } while ($is_complete != true);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
//            Log::info('pull-Statement-Anb-test', []);
        }
    }

    public function showTransaction($msg)
    {

        $transactions = $msg['statement']['transactions'];

        foreach ($transactions as $index => $anbTransaction) {

            if ($anbTransaction['partTrnType'] != strtoupper($this->option('process_type'))) {
                continue;
            }

            if (strtolower($this->option('process_type')) == 'credit') {


                $narr2 = $anbTransaction['narrative']['narr2'];

                if (Str::substr($narr2, 0, 3) == 'AS:') {

                    $response = $this->creditRefundTransactions($anbTransaction);
                } else {

                    $response = $this->creditTransactions($anbTransaction, $index);
                }

                if (isset($response['exists']) && $response['exists']) {
                    continue;
                }

                if (!$response['status']) {
                    dump($response['message']);
                    continue;
                }

                dump($response['data']);

            } else {

                $response = $this->debitTransactions($anbTransaction);

                if (!$response['status']) {
                    dump($response['message']);
                    continue;
                }

                dump($response['data']);
            }
        }
    }

    public function debitTransactions($anbTransaction)
    {

        $response = ['status' => true];

        $anbTransactionAmount = $anbTransaction['amount']['amount'];

        $narr2 = $anbTransaction['narrative']['narr2'] ?? null;

        $transId = substr($narr2, '3');

        $anbTransfer = AnbTransfer::find($transId);

        if (!$anbTransfer) {
            return $this->handleResponse($response, false, 'anbTransfer not valid' . $anbTransfer . ' ' . $transId . ' ' . $anbTransactionAmount, []);
        }

        $transaction = \App\Models\VirtualBanking\Transaction::find($anbTransfer->transaction_id);

        if (!$transaction) {
            return $this->handleResponse($response, false, 'transaction not valid' . $anbTransfer->id, []);
        }

        $transactionAmount = ($transaction->amount / 100);

        if ($transactionAmount != $anbTransactionAmount) {

            return $this->handleResponse($response, false,
                'transfer amount not equal bank amount ' . $transactionAmount . ' ' . $anbTransactionAmount . ' ' . $transId, []);
        }

        return $this->handleResponse($response, true, 'anb transfer exists' . $anbTransfer->id, []);
    }

    public function toIban($accountNo)
    {
        try {

            if (preg_match('/^SA\d\d30100/', $accountNo) === 1) {
                return $accountNo;
            }

            $modulo = bcmod('30100' . $accountNo . '281000', '97');

            $checkDigit = 98 - (int)$modulo;

            if ($checkDigit < 10) {
                $checkDigit = '0' . $checkDigit;
            }

            return 'SA' . $checkDigit . '30100' . $accountNo;

        } catch (\Exception $e) {
            return false;
        }
    }

    public function creditTransactions($anbTransaction, $index)
    {

        dump($this->count++ . ' DEPOSIT');

        $response = ['status' => true];

        $postDate = Carbon::parse($anbTransaction['valueDate']);

        $transactionExists = $this->checkTransactionExists($anbTransaction);

        if ($transactionExists) {
            return $this->handleResponse($response, false,
                'transaction already exists ' . $transactionExists->created_at . ' ' . $postDate->format('Y-m-d'), [], true);
        }

        $narr2 = $anbTransaction['narrative']['narr2'] ?? null;

        $narr2 = $this->handleNarr2($narr2);

        $iban = $this->toIban($narr2);

        $collector = $this->collectorWallet();

        if (!$collector) {
            dd('collector not found');
        }

        if (!$iban) {
            $data = $this->deposit($collector, $anbTransaction, null);

//            dd('iban not found', $data);

            return $this->handleResponse($response, true, 'narr2 not valid to be iban ' . $narr2 .
                ' ' . $anbTransaction['amount']['amount'] . ' ' . $anbTransaction['partTrnType'], $data);
        }

        $virtualAccount = VirtualAccount::where('identifier', $iban)->first();

        if (!$virtualAccount) {

            $data = $this->deposit($collector, $anbTransaction, null);

//            dd('virtual Account not found', $data);

            return $this->handleResponse($response, true, 'virtualAccount not valid: iban => '
                . $iban . ' narr2 => ' . $anbTransaction['narrative']['narr2'], $data);
        }

        $userWallet = $virtualAccount->wallet;

        if (!$userWallet) {
            return $this->handleResponse($response, false, 'wallet not valid: iban => ' . $userWallet, []);
        }

        $data = $this->deposit($userWallet, $anbTransaction, $virtualAccount);

        return $this->handleResponse($response, true, 'transaction data => this not in our database  ', $data);
    }

    public function creditRefundTransactions($anbTransaction)
    {

        dump($this->count++ . ' REFUND');

        $response = ['status' => true];

        $anbTransactionAmount = $anbTransaction['amount']['amount'];

        $transactionExists = $this->checkTransactionExists($anbTransaction);

        if ($transactionExists) {
            return $this->handleResponse($response, false, 'transaction already exists ', []);
        }

        $collector = $this->collectorWallet();

        if (!$collector) {
            dd('collector not found');
        }

        $anbTransfer = $this->getTransferInTransactionsRefunded($anbTransaction);

        if (!$anbTransfer) {

            $data = $this->deposit($collector, $anbTransaction, null);
//            dd('anb transfer not found', $data);
            return $this->handleResponse($response, true, 'anbTransfer not valid ' . $anbTransfer . ' ' . $anbTransactionAmount, $data);
        }

        $transaction = \App\Models\VirtualBanking\Transaction::find($anbTransfer->transaction_id);

        if (!$transaction) {
            return $this->handleResponse($response, false, 'transaction not valid ' . $anbTransfer->id, []);
        }

        $userWallet = Wallet::find($transaction->wallet_id);

        if (!$userWallet) {
            return $this->handleResponse($response, false, 'wallet not valid: iban => ' . $userWallet, []);
        }

        $virtualAccount = VirtualAccount::where('wallet_id', $userWallet->id)->first();

        if (!$virtualAccount) {
            return $this->handleResponse($response, false, 'virtualAccount not valid: iban => ' . $userWallet->id, []);
        }

        $data = $this->deposit($userWallet, $anbTransaction, $virtualAccount);

        return $this->handleResponse($response, true, 'transaction data => this not in our database  ', $data);
    }

    public function deposit($userWallet, $anbTransaction, $virtualAccount)
    {

        $bankRef = $anbTransaction['refNum'] ?? null;

        $user = User::find($userWallet->holder_id);

        $currency = $anbTransaction['amount']['currencyCode'];

        $anbTransactionAmount = Money::parseByDecimal($anbTransaction['amount']['amount'], $currency);

        $meta = [
            'vaccount_identifier' => $virtualAccount ? $virtualAccount->identifier : null,
            'vaccount' => $virtualAccount ? optional($virtualAccount)->toArray() : [],
            'reference' => transaction_ref(),
            'reason' => TransactionReason::AnbStatement,
            'details' => [
                'version' => 1,
                'data' => $anbTransaction,
                'source' => 'cmd_5_statement'
            ],
            'external_reference' => $bankRef,
            'value_date' => $anbTransaction['valueDate'], //->toDateTimeString(),
        ];

        $created_at = Carbon::parse($anbTransaction['valueDate'])->format('Y-m-d');

        $data = [

            User::class,
            $user->id,
            $userWallet->id,
            'deposit',
            $anbTransactionAmount->absolute()->getAmount(),
            1,
            json_encode($meta),
            (string)Str::uuid(),
            $created_at,
            $created_at,
        ];

        $userTransactions = Transaction::where('external_reference', $bankRef)
            ->where('payable_id', $user->id)
            ->where('payable_type', User::class)
            ->first();

        if ($userTransactions) {
            dump('transaction exists in user transactions ----------- tra_amount => ' . ($userTransactions->amount / 100) . ' bnk_amount => ' . $anbTransaction['amount']['amount'] . '  => bnk_ref = ' . $bankRef );
            return [];
        }

        if ($this->option('deposit') == 'add' && $anbTransaction['amount']['amount'] > 0) {

            DB::table('transactions')->insertGetId([
                'payable_type' => User::class,
                'payable_id' => $user->id,
                'wallet_id' => $userWallet->id,
                'type' => 'deposit',
                'amount' => $anbTransactionAmount->absolute()->getAmount(),
                'confirmed' => 1,
                'meta' => json_encode($meta),
                'uuid' => (string)Str::uuid(),
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $userWallet->refreshBalance();

            dump('transaction saved done');
        }

        return $data;
    }

    public function handleResponse($response, $status, $msg, $data, $exists = null)
    {

        $response['status'] = $status;
        $response['message'] = $msg;
        $response['data'] = $data;
        $response['exists'] = $exists;

        return $response;
    }

    public function handleNarr2($narr2)
    {
        return substr($narr2, 0, 15);
    }

    public function collectorWallet()
    {
        $collector = User::role(Role::AnbStatementCollector)->first();
        return $collector->getWallet(WalletType::AnbStatementCollecter);
    }

    public function checkTransactionExists ($anbTransaction) {

        $bankRef = $anbTransaction['refNum'] ?? null;

        $postDate = Carbon::parse($anbTransaction['valueDate']);

        $postDatY = $postDate->format('Y');
        $postDatM = $postDate->format('m');
        $postDatD = $postDate->format('d');

        $transactionExists = Transaction::where('external_reference', $bankRef)
            ->whereDay('created_at', $postDatD)
            ->whereMonth('created_at', $postDatM)
            ->whereYear('created_at', $postDatY)
            ->whereIn('reason', [
                TransactionReason::AnbStatement,
                TransactionReason::RefundAfterAnbTransferFailure
            ])
            ->limit(1)
            ->first();

        return $transactionExists;
    }

    public function getTransferInTransactionsRefunded ($anbTransaction) {

        $channel = isset($anbTransaction['channel']) ? $anbTransaction['channel'] : null;

        if ($channel == 'SARIE') {

            $narr3Refrence = null;

            $narr3 = $anbTransaction['narrative']['narr3'] ?? null;

            $narr3Data = explode('/', $narr3);

            foreach ($narr3Data as $index => $item) {

                if ($item == 'RET') {
                    $narr3Refrence = isset($narr3Data[$index + 1])  ? $narr3Data[$index + 1] : null;
                }
            }

            $transfer = AnbTransfer::where('id', $narr3Refrence)->first();

        } elseif ($channel == 'IPS') {

            $narr1 = $anbTransaction['narrative']['narr1'] ?? null;

            if (strlen($narr1) > 10) {
                $narr1 = $anbTransaction['narrative']['narr2'] ?? null;
            }

            $transfer = AnbTransfer::where('external_trans_reference', $narr1)->first();
        }else {

            $narr3 = $anbTransaction['narrative']['narr3'] ?? null;
            $transfer = AnbTransfer::where('id', $narr3)->first();
        }

        if (!$transfer) {
            return null;
        }

        return $transfer;
    }

}
