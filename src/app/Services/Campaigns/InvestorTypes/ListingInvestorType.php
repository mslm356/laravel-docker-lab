<?php

namespace App\Services\Campaigns\InvestorTypes;

use Illuminate\Support\Facades\DB;

class ListingInvestorType
{

    public function getUsersData(array $selectedData, array $customData = [])
    {

        $listing_id = $customData['custom_ids'];

        $data =  DB::table('listing_investor')
            ->where('listing_id', $listing_id)
            ->join('users', 'users.id', '=', 'listing_investor.investor_id')
            ->select($selectedData)
            ->get();

        return json_decode($data, 1);
    }

}
