<?php

namespace App\Models\Anb;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AnbWebhook extends AppModel
{
    use HasFactory;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array'
    ];
}
