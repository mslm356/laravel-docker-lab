<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class File extends AppModel
{
    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'extra_info' => 'collection'
    ];

    /**
     * Get the fileable model
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function fileable()
    {
        return $this->morphTo();
    }

    /**
     * Store file using the driver disk
     *
     * @param UploadedFile $file
     * @param string $dirname
     * @return string
     */
    public static function store(UploadedFile $file, $dirname)
    {
        return $file->store($dirname, self::getPrivateDriver());
    }

    /**
     * Get private driver
     *
     * @return string
     */
    public static function getPrivateDriver()
    {
        return config('filesystems.private');
    }

    /**
     * Delete file and model
     *
     * @return bool
     */
    public function deleteWithFile()
    {
        $isDeleted = Storage::disk(self::getPrivateDriver())->delete($this->path);

        return $this->delete();
    }

    /**
     * CHECK file EXITS
     *
     * @return bool
     */
    public function checkFileExists()
    {
        return Storage::disk(self::getPrivateDriver())->exists($this->path);
    }

    /**
     * Upload the file to the storage
     *
     * @param \Illuminate\Http\UploadedFile $image
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return $this
     */
    public static function upload(UploadedFile $file, $dir, $data)
    {
        $filename = $file->store($dir, self::getPrivateDriver());
        $fileOriginalName = explode('.', $file->getClientOriginalName());
        $fileWithoutExtension = isset($fileOriginalName[0]) ?$fileOriginalName[0] : $file->getClientOriginalName() ;

        if ($filename === false) {
            abort(500, __('messages.failed_to_upload_image'));
        }

        $fileModel = self::create([
            'id' => (string)Str::uuid(),
            'fileable_type' => $data['fileable_type'],
            'fileable_id' => $data['fileable_id'],
            'extra_info' => $data['info'] ?? [],
            'name' => mb_substr($fileWithoutExtension, 0, 20, 'utf-8').'.'. $file->getClientOriginalExtension(),
            'type' => $data['type'],
            'path' => $filename,
        ]);
        return $fileModel;
    }

    /**
     * Download the file
     *
     * @return void
     */
    public function download()
    {
        $driver = self::getPrivateDriver();

        return Storage::disk($driver)->download(
            $this->path,
            $this->name,
            ['Content-Disposition' => "attachment;filename={$this->name}"]
        );
    }

    public function getFullPathAttribute()
    {
        $path = Storage::disk(config('filesystems.private'))->temporaryUrl($this->path, now()->addMinutes(15));

        if (config('filesystems.default') === 'public') {
            return asset($path);
        } else {
            return $path;
        }
    }

    public function getTempFullPathAttribute()
    {
        $path = Storage::disk(config('filesystems.private'))->temporaryUrl($this->path, now()->addMinutes(15));

        if (config('filesystems.default') === 'public') {
            return asset($path);
        } else {
            return $path;
        }
    }

    public function view()
    {
        return response()->file(storage_path('app/' . $this->path));
    }

    public function getFile()
    {
        $driver = self::getPrivateDriver();

        return Storage::disk($driver)->get(
            $this->full_path
        );
    }
}
