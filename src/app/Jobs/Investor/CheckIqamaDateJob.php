<?php

namespace App\Jobs\Investor;

use App\Services\KYC\UpdateKycServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckIqamaDateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $updateKyc ;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->updateKyc = new UpdateKycServices();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // TODO add job in queue in schedule

        $this->updateKyc->updateKYC();
    }
}
