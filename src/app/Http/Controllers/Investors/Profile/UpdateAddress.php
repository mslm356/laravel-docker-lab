<?php

namespace App\Http\Controllers\Investors\Profile;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UpdateAddress extends Controller
{
    public function __invoke(Request $request)
    {
        DB::transaction(function () use ($request) {
            $data = $this->validate($request, [
                'city_id' => ['required', 'integer', Rule::exists(City::class, 'id')],
                'street' => ['required', 'string', 'max:100',textValidation()],
                'district' => ['required', 'string', 'max:100',textValidation()],
                'postal_code' => ['required', 'string', 'max:100',textValidation()],
            ]);


            $investorProfile = $request->user()->investor()->lockForUpdate()->first();

            $moreInfo = $investorProfile->more_info;

            $investorProfile->update([
                'more_info' => array_merge($moreInfo ? $moreInfo->toArray() : [], ['location' => $data])
            ]);
        });

        return response()->json();
    }
}
