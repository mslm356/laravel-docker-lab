<?php

namespace App\Console\Commands\Anb;

use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Http\HttpException;
use Illuminate\Console\Command;

class ListWebhooks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:list-webhooks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List ANB webhooks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $hooks = [];

        $page = 1;

        do {
            try {
                $response = AnbApiUtil::listWebhooks();
                foreach ($response['data'] as $hook) {
                    $hooks[] = [
                        'id' => $hook['id'],
                        'url' => $hook['url'],
                        'events' => collect($hook['events'])->pluck('code')->join(', ')
                    ];
                }
                $page++;
            } catch (HttpException $th) {
                $this->error($th->getResponse()->body());
                return Command::FAILURE;
            } catch (\Throwable $th) {
                $this->error($th->getMessage());
                return Command::FAILURE;
            }
        } while ($response['meta']['hasNextPage'] ?? false);

        if (empty($hooks)) {
            $this->info('No hooks found');
        } else {
            $this->table(
                [
                    'ID', 'URL', 'Events'
                ],
                $hooks
            );
        }
    }
}
