<?php

namespace App\Support\Elm\AbsherOtp\Exceptions;

use App\Models\Elm\AbsherOtp;
use Exception;

class VerificationSmsException extends Exception
{
    protected $reason;
    protected $nabaaMsg;

    public function __construct(AbsherOtp $absherOtp, $reasonCode)
    {
        parent::__construct("Failed to verify SMS verification code [{$reasonCode}]");

        $this->reason = $reasonCode;
        $this->absherOtp = $absherOtp;
    }

    public function getReasonCode()
    {
        return $this->reason;
    }

    public function context()
    {
        return [
            'absher_otp_id' => $this->nabaaMsg->id,
            'reason' => $this->reason,
        ];
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'reason' => $this->reason
            ], 400);
        } else {
            abort(400);
        }
    }
}
