<?php

namespace App\Http\Controllers\Admins\Api\Anb\EodStatements;

use Illuminate\Http\Request;
use App\Exports\AnbEodStatements\AnbEodTransactionsExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ExportAnbEodTransactions extends Controller
{
    /**
     * Export ANB EOD statement transactions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $data  = $this->validate($request, [
            'start_date' => ['required', 'date_format:Y-m-d'],
            'end_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:start_date'],
            'extension' => ['required', 'in:xlsx,csv']
        ]);

        return Excel::download(
            new AnbEodTransactionsExport(
                ksaDateToUtcCarbon($data['start_date']),
                ksaDateToUtcCarbon($data['end_date'])
            ),
            "anb-eod-transactions-{$data['start_date']}-{$data['end_date']}.{$data['extension']}"
        );
    }
}
