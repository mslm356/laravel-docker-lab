<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Enums\Banking\BankCode;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorInterface;
use Illuminate\Support\Arr;

class HyperPayReasonType implements GeneratorInterface
{
    /**
     * Generate description
     *
     * @return string
     */
    public function generate(Transaction $transaction, $locale = null): string
    {
        return __('transaction-description.hyperpay_deposit', [], $locale);
    }
}
