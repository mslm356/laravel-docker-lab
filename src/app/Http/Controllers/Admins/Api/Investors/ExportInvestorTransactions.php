<?php

namespace App\Http\Controllers\Admins\Api\Investors;

use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Enums\Banking\WalletType;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Wallets\WalletTransactionsExport;

class ExportInvestorTransactions extends Controller
{
    /**
     * Get Investors list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, User $user)
    {
        $data  = $this->validate($request, [
            'start_date' => ['required', 'date_format:Y-m-d'],
            'end_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:start_date'],
            'extension' => ['required', 'in:xlsx,csv']
        ]);

        $wallet = $user->getWallet(WalletType::Investor);

        if ($wallet) {
            return Excel::download(
                new WalletTransactionsExport(
                    ksaDateToUtcCarbon($data['start_date']),
                    ksaDateToUtcCarbon($data['end_date']),
                    $wallet->id
                ),
                "investor-{$user->id}-transactions-{$data['start_date']}-{$data['end_date']}.{$data['extension']}"
            );
        }

        return response()->json([], 404);
    }
}
