<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\BankAccounts;

use App\Enums\Banking\BankCode;
use App\Enums\BankAccountStatus;
use App\Http\Controllers\Controller;

class GetApprovedBankAccounts extends Controller
{
    /**
     * Create entity
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        return response()->json([
            'data' => authorizedEntity()
                ->bankAccounts()
                ->where('status', BankAccountStatus::Approved)
                ->get()
                ->map(fn ($item) => [
                    'id' => $item->id,
                    'name' => BankCode::getDescription($item->bank) . ' - ' . $item->iban
                ])
                ->toArray()
        ]);
    }
}
