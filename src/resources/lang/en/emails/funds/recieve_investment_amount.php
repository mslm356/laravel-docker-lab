<?php

return [
    'subject' => 'Received Investment Amount',
    'welcome_message' => 'Our dear investor :name',
    'message_1' => 'An investment amount of  has been received :amount',
    'message_2' => 'to invest in opportunity :fund_title ',
    'invoice_link' => 'You can also view the invoice through the following link',
    'link_text' => 'To view your control panel, please go to the link',
    'end_message' => 'Thank you for trusting Aseel platform',
    'units' => 'Number :units Units',
    'view' => 'View Dashboard',
    'download' => 'Download Invoice',
];
