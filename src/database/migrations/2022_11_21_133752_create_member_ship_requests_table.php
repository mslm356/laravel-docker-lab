<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberShipRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_ship_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('member_ship');
            $table->tinyInteger('status')->default(1)->comment('default => pending');
            $table->tinyInteger('is_active')->default(0)->comment('default => in_active');
            $table->tinyInteger('initiator_type')->default(1)->comment('default => AutoAssessment');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->text('comment')->nullable();
            $table->dateTime('expire_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_ship_requests');
    }
}
