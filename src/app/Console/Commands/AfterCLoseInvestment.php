<?php

namespace App\Console\Commands;

use App\Actions\Listings\AfterCloseListingAction;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use Bavix\Wallet\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AfterCLoseInvestment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collect-listing-wallet {--listing_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listing_id = $this->option('listing_id');
        $listing = Listing::find($listing_id);

        if (!$listing)
            dd('not found listing');

        $status=AfterCloseListingAction::run($listing);
        dd($status);
    }
}
