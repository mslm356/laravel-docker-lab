<?php

namespace App\Http\Controllers\Admins\Api\BankAccounts;

use App\Enums\BankAccountStatus;
use App\Enums\Banking\BankCode;
use App\Enums\Notifications\NotificationTypes;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\BankAccounts\BankAccountListItemResource;
use App\Http\Resources\Admins\BankAccounts\BankAccountResource;
use App\Mail\Investors\approveBankAccount;
use App\Mail\Investors\newBankAccount;
use App\Mail\Investors\rejectBankAccount;
use App\Models\BankAccount;
use App\Rules\VerifyIban;
use App\Models\Users\User;
use App\Support\QueryScoper\Scopes\Admin\BankAccounts\AccountableScope;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class BankAccountController extends Controller
{

    use NotificationServices, SendMessage;

    /**
     * Get the bank accounts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $accounts = BankAccount::toScopes([
            AccountableScope::class,
        ])
            ->with('reviewer', 'attachments', 'accountable');

        if ($request->has('value') && $request['value'] != null) {
            $accounts->where('bank_accounts.iban', 'LIKE', '%' . $request['value'] . '%')
                ->orWhere('bank_accounts.name', 'LIKE', '%' . $request['value'] . '%');
        }

        if ($request->has('status') && $request['status'] != null) {
            $accounts->where('bank_accounts.status', $request['status']);
        }

        $accounts->join('wallets', function ($join){
        $join->on('wallets.holder_id', '=', 'bank_accounts.accountable_id')
            ->where('wallets.holder_type', User::class);
        })->select('bank_accounts.*','wallets.balance');

        $sort_by=(isset($request->sort_by))?$request->sort_by:'balance';
        $sort_type=(isset($request->sort_type))?$request->sort_type:'desc';

        $accounts = $accounts->orderBy($sort_by,$sort_type)->paginate();

        return BankAccountListItemResource::collection($accounts);
    }

    /**
     * Show the bank account details
     *
     * @param \App\Models\BankAccount $bankAccount
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(BankAccount $bankAccount)
    {
        return new BankAccountResource($bankAccount);
    }

    /**
     * Update bank account status
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BankAccount $bankAccount
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, BankAccount $bankAccount)
    {
        $data = $this->validate($request, [
            'status' => ['required', 'integer', new EnumValue(BankAccountStatus::class, false)],
            'comment' => ['exclude_unless:status,' . BankAccountStatus::Rejected, 'nullable', 'string', 'max:1000', `regex:-_!^=@&\/\\#,+()$~%|'":*?<>{}`]
        ]);

        return DB::transaction(function () use ($data, $bankAccount, $request) {
            $bankAccount->update([
                'status' => $data['status'],
                'reviewer_comment' => $data['status'] == BankAccountStatus::Rejected ? $data['comment'] : null,
                'reviewer_id' => $request->user('sanctum')->id,
            ]);
            $this->sendEmail($data, $bankAccount);
            return response()->json();
        });
    }


    /**
     * Update bank account update
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BankAccount $bankAccount
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $bankAccount = BankAccount::find($id);

        if (!$bankAccount)
            return response()->json(['data' => ['message' => __('messages.invalid_bank_account')]], 400);

        $data = $this->validate($request, $this->getRules($bankAccount));

        $repeatedIban = BankAccount::whereIn('status', [BankAccountStatus::Approved, BankAccountStatus::InReview, BankAccountStatus::PendingReview])
            ->where('iban', $data['iban'])->where("id", '!=', $bankAccount->id)->first();
        if ($repeatedIban)
            throw ValidationException::withMessages(['iban' => __('messages.repeated_iban'),]);

        return DB::transaction(function () use ($data, $bankAccount, $request) {
            $bankAccount->update([
                'name' => $data['name'],
                'bank' => $data['bank'],
                'iban' => $data['iban'],
                'reviewer_id' => $request->user('sanctum')->id,
            ]);

            return response()->json(['data' => [
                'message' => 'Bank account updated successfully'
            ]], 200);

        });
    }

    public function getRules($bankAccount)
    {
        return [
            'name' => ['required', 'string', 'max:100'],
            'iban' => ['required', 'string', /*'unique:bank_accounts,iban,' . $bankAccount->id,*/ new VerifyIban],
            'bank' => ['required', 'string'],
        ];
    }

    public function sendEmail($data, $bankAccount)
    {
        $user = User::where('id', $bankAccount->accountable_id)->first();

        if ($data['status'] == BankAccountStatus::Approved) {
            if ($user) {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new approveBankAccount($user));
                }
                $this->sendSmsData($bankAccount->reviewer_comment, BankAccountStatus::Approved, $user);

                $this->send(NotificationTypes::ApproveBankAccount, $this->notificationData($bankAccount, BankAccountStatus::Approved, $bankAccount->reviewer_comment), $user->id);
            }
        }

        if ($data['status'] == BankAccountStatus::Rejected) {
            if ($user) {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new rejectBankAccount($user, $bankAccount));
                }

                $this->sendSmsData($bankAccount->reviewer_comment, BankAccountStatus::Rejected, $user);

                $this->send(NotificationTypes::RejectBankAccount, $this->notificationData($bankAccount, BankAccountStatus::Rejected , $bankAccount->reviewer_comment ), $user->id);

            }
        }
    }

    public function notificationData($bankAccount, $status , $reason)
    {
        return [
            'action_id' => $bankAccount->id,
            'channel' => 'mobile',
            'title_en' => $status == BankAccountStatus::Approved ? 'Your IBAN adding request has been approved' : 'Your IBAN adding request has been denied',
            'title_ar' => $status == BankAccountStatus::Approved ? 'تمت الموافقة على طلب إضافة الآيبان' : 'عدم الموافقة على طلب إضافة الآيبان',
            'content_en' => $status == BankAccountStatus::Approved ? 'The bank account has been added to your personal wallet' : '  We apologize for not accepting your request to add the bank account  ' . $reason,
            'content_ar' => $status == BankAccountStatus::Approved ? 'تمت إضافة الحساب البنكي في المحفظة الشخصية الخاصة بكم' :  '  نعتذر عن عدم قبول طلبكم إضافة الحساب البنكي إلى المحفظة وذلك بسبب أن  ' . $reason,
        ];
    }

    public function sendSmsData($reason, $status, $user)
    {
        $name = $user ? $user->full_name : '---';
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_en = $status == BankAccountStatus::Approved ? 'The bank account has been added to your personal wallet, Aseel platform wishes you all the best, Thank you for dealing with Aseel Platform' : "We apologize for not accepting your request to add the bank account, please see the request for the rejection reason is " . $reason . " Thank you for dealing with Aseel platform";
        $content_ar = $status == BankAccountStatus::Approved ? ' تمت إضافة الحساب البنكي في المحفظة الشخصية الخاصة بكم منصة أصيل تتمنى لكم كل التوفيق شكراً لتعاملك مع منصة أصيل المالية ' : ' شكراً لتعاملك مع منصة أصيل المالية  ' . $reason . ' نعتذر عن عدم قبول طلبكم إضافة الحساب البنكي إلى المحفظة و ذلك بسبب ';

        $content_ar = $welcome_message_ar . $content_ar;
        $content_en = "Our dear investor " . $name . $content_en;
        if ($user->locale = "ar")
            $message = $content_ar;

        else
            $message = $content_en;

        $this->sendOtpMessage($user->phone_number, $message);

    }

}
