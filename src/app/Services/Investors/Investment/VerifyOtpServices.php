<?php


namespace App\Services\Investors\Investment;

use App\Models\Elm\AbsherOtp;
use App\Models\Users\InvestorSubscriptionToken;
use App\Services\LogService;
use App\Services\MemberShip\SpecialCanInvest;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class VerifyOtpServices
{

    public $logService;
    public $handleResponseService;
    public $specialCanInvest;

    public function __construct()
    {
        $this->handleResponseService = new HandleResponseService();
        $this->logService = new LogService();
        $this->specialCanInvest = new SpecialCanInvest();
    }

    public function handle($request, $user)
    {

        $response = ['status' => true];

        $data = $this->dataValidation($request);

        $token = InvestorSubscriptionToken::query()
            ->where('is_expire', false)
            ->whereNull('expired_at')
            ->where('reference', $data['token'])
            ->first();

        if (!$token) {
            return $this->handleResponseService->responseValues($response, false, 'token not valid', []);
        }

        if (!$this->specialCanInvest->handle($token->listing, $user)) {
            return $this->handleResponseService->responseValues($response, false, 'you not have access', []);
        }

        if (config('app.absher_middleware_mode') == 'external') {
            $response = $this->sendRequestToVerify($data, $response);
        } else {
            $response = $this->oldVerify($user, $data, $token, $response);
        }

        if (!$response['status']) {
            return $this->handleResponseService
                ->responseValues($response, false, $response['message'], []);
        }

        $token->update(['authorization_expiry' => $expiry = now()->addMinutes(3)]);

        $responseData = ['expires_in' => now()->diffInSeconds($expiry)];

        return $this->handleResponseService->responseValues($response, true, $response['message'], $responseData);
    }

    public function sendRequestToVerify($data, $response)
    {

        $url = config('app.absher_middleware_verify_url') . '?vId=' . $data['vid'] . '&otp=' . $data['code'];

        $promise = Http::get($url);

        $this->logService->log('ABSHER-MIDDLEWARE', null, ['test', $promise->body()]);

        if ($promise->ok()) {
            return $this->handleResponseService->responseValues($response, true, 'code verified successfully', []);
        }

        return $this->handleResponseService->responseValues($response, false, 'messages.invalid_sms_verification_code', []);
    }

    public function oldVerify($user, $data, $token, $response)
    {

        $absherOtpMsg = AbsherOtp::findOrFail($data['vid']);

        $absherOtpMsgSubscriptionTokenId = $absherOtpMsg->data['subscription_token_id'] ?? null;

        if ($absherOtpMsgSubscriptionTokenId && $absherOtpMsgSubscriptionTokenId !== $token->id) {
            return $this->handleResponseService->responseValues($response, false, 'messages.invalid_sms_verification_vid', []);
        }

        $isVerified = VerificationSms::instance()->verify($user->nationalIdentity->nin, $absherOtpMsg, $data['code']);

        if ($isVerified === false) {
            throw ValidationException::withMessages([
                'code' => __('messages.invalid_sms_verification_code'),
            ]);
        }

        return $this->handleResponseService->responseValues($response, true, 'mobile_app/message.code_verified_successfully', []);
    }

    public function dataValidation($request)
    {
        $rules = [
            'token' => 'required|string',
            'code' => ['required', 'string'],
            'vid' => ['required', 'string']
        ];

        $data = Validator::make($request->all(), $rules)->validate();

        return $data;
    }
}
