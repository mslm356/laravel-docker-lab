<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VerifyIban implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return verify_iban($value, true);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.iban');
    }
}
