<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.3.5/dist/alpine.min.js"></script>
    <script src="{{ asset('js/website/website.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/website.css') }}" rel="stylesheet">
</head>
<body>
    <div class="flex flex-col justify-center min-h-screen py-12 bg-gray-50 sm:px-6 lg:px-8">
        <div class="sm:mx-auto sm:w-full sm:max-w-md">
            <img class="w-auto h-20 mx-auto" src="{{ asset('images/logo_symbol.png') }}"
                alt="Workflow">
            <h2 class="mt-6 text-3xl font-extrabold leading-9 text-center text-gray-900">
                A better way to send money 😎
            </h2>
        </div>

        <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
            <div class="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">
                <form action="{{ route('anb.transfer') }}" method="POST" class="space-y-4">
                    @csrf
                    <p>Amount: SAR 1,000</p>
                    <div>
                        <label for="iban" class="block text-sm font-medium leading-5 text-gray-700">
                            IBAN
                        </label>
                        <div class="mt-1 rounded-md shadow-sm">
                            <input id="iban" type="text" name="iban" value="{{ old('iban') }}"
                                class="block w-full px-3 py-2 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-md appearance-none focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5">
                        </div>
                        @error('iban')
                            <div class="text-sm text-red-500">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="mt-6">
                        <span class="block w-full rounded-md shadow-sm">
                            <button type="submit"
                                class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white transition duration-150 ease-in-out border border-transparent rounded-md bg-primary-600 hover:bg-primary focus:outline-none focus:shadow-outline-indigo active:bg-primary">
                                Tranfer
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
