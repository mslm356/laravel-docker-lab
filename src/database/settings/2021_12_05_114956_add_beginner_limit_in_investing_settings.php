<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class AddBeginnerLimitInInvestingSettings extends SettingsMigration
{
    public function up(): void
    {
        /**
         * Beginner limit will be money amount (integer)
         */
        $this->migrator->add(
            'investing.beginner_limit_per_listing',
            "20000000"
        );
    }

    public function down(): void
    {
        $this->migrator->delete('investing.beginner_limit_per_listing');
    }
}
