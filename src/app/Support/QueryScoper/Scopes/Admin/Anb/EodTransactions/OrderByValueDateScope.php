<?php

namespace App\Support\QueryScoper\Scopes\Admin\Anb\EodTransactions;

use App\Support\QueryScoper\QueryScoper;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrderByValueDateScope extends QueryScoper
{
    /**
     * Apply rules
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply($builder)
    {
        try {
            $validated = $this->validator($this->prepareData())->validate();
        } catch (\Throwable $th) {
            return $builder->oldest('value_date');
        }

        return $this->prepareBuilder($builder, $validated);
    }

    public function prepareBuilder($builder, $data)
    {
        if ($data['sort_by'] === 'latest') {
            $builder->latest('value_date');
        } else {
            $builder->oldest('value_date');
        }

        return $builder;
    }

    public function prepareData()
    {
        return [
            'sort_by' => Request::query('sort_by', '')
        ];
    }

    public function validator($data)
    {
        return Validator::make($data, [
            'sort_by' => ['nullable', 'string', Rule::in(['latest', 'oldest'])]
        ]);
    }
}
