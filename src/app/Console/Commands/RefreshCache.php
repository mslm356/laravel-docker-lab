<?php

namespace App\Console\Commands;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Jobs\AbsherTest;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Services\CacheServices;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RefreshCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Refresh:Cache {--listingId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'RefreshCache';

    public $cacheServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->cacheServices = new CacheServices();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            if ($this->option('listingId') == 'all') {

                $listingIds = DB::table('listings')->select('id')->get();

                foreach ($listingIds as $listingId) {
                    $this->cacheServices->updateFundCache($listingId->id);
                }

                dd('done');
            }

            $this->cacheServices->updateFundCache($this->option('listingId'));
            dd('done');

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }


}
