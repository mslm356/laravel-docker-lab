<?php

namespace App\Models\Listings;

use App\Casts\Money\MoneyCast;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ListingInvestor extends Pivot
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'invested_amount' => MoneyCast::class . ':currency'
    ];

    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }

    public function investor()
    {
        return $this->belongsTo(User::class,'investor_id');
    }
}
