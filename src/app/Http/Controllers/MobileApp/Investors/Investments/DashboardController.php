<?php

namespace App\Http\Controllers\MobileApp\Investors\Investments;

use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Listings\ListingReportResource;
use App\Http\Resources\Investors\MyUnitResource;
use App\Models\Listings\Listing;
use App\Services\Investors\DashboardServices;
use App\Services\Investors\ListingReportServices;
use App\Services\LogService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public $dashboardServices;
    public $logServices;
    public $listingReportServices;

    public function __construct()
    {
        $this->dashboardServices = new DashboardServices();
        $this->logServices = new LogService();
        $this->listingReportServices = new ListingReportServices();
    }

    public function getMyUnits(Request $request, $id)
    {
        try {

            $listing = Listing::find($id);

            if (!$listing) {
                return apiResponse(400, __('mobile_app/message.not_found'));
            }

            $user = getAuthUser('api');

            $listingInvestedUnits = $this->dashboardServices->getShowMyUnits($listing, $user);

            $this->authorize('accessListingWhichInvestedIn', $listing);

            $listingReports = ListingReportResource::collection($listing->reports()->with('files')->latest()->simplePaginate());

            $result = [
                'listingInvestedUnits' => $listingInvestedUnits ? new MyUnitResource($listingInvestedUnits) : null,
                'reports' => $listingReports ? $listingReports : null
            ];

            return apiResponse(200, __('mobile_app/message.success'), $result);

        } catch (AuthorizationException $e) {
            $code = $this->logServices->log('getMyUnits-AuthorizationException', $e);
            return apiResponse(400, $e->getMessage(). ' : ' .  $code);
        } catch (\Exception $e) {
            $code = $this->logServices->log('GET-MY-UNITS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }

    }

    public function ListingReport(Listing $listing)
    {
        try {
            $this->authorize('accessListingWhichInvestedIn', $listing);

            return apiResponse(
                200,
                __('mobile_app/message.success'),
                ListingReportResource::collection($listing->reports()->with('files')->latest()->simplePaginate())
                    ->additional(['guard'=>'api'])
            );

        } catch (AuthorizationException $e) {
            $code = $this->logServices->log('ListingReport-AuthorizationException', $e);
            return apiResponse(400, $e->getMessage(). ' : ' .  $code);
        } catch (\Exception $e) {
            $code = $this->logServices->log('LISTING-REPORT', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }

    }
}
