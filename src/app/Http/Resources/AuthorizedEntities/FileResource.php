<?php

namespace App\Http\Resources\AuthorizedEntities;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'url' => route('investor.files.download', ['file' => $this->id]),
            'name' => $this->name,
        ];
    }
}
