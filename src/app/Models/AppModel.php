<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as IlluminateModel;

abstract class AppModel extends IlluminateModel
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
