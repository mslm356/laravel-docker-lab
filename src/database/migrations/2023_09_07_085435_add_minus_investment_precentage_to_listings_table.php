<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinusInvestmentPrecentageToListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->integer('changed_investment_precentage')->default(0)->after('investment_shares');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('listings','decresed_investment_precentage'))
        {
            Schema::table('listings', function (Blueprint $table) {
                $table->dropColumn('decresed_investment_precentage');
            });
        }

        if(Schema::hasColumn('listings','changed_investment_precentage'))
        {
            Schema::table('listings', function (Blueprint $table) {
                $table->dropColumn('changed_investment_precentage');
            });
        }
    }
}
