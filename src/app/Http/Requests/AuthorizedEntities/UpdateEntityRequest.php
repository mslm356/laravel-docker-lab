<?php

namespace App\Http\Requests\AuthorizedEntities;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEntityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            (new CreateEntityRequest())->rules(),
            [
                'cr' => ['nullable', 'array', 'min:1', 'max:5'],
                'cr.*' => ['nullable', 'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/heif,image/heic', 'max:4096'],
                'cma_license' => ['nullable', 'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/heif,image/heic', 'max:4096'],
            ]
        );
    }
}
