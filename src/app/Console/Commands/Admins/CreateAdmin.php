<?php

namespace App\Console\Commands\Admins;

use App\Enums\Role as EnumsRole;
use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admins:create {--phone_number=} {--email=} {--password=} {--first_name=} {--last_name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->option('email');
        $first_name= $this->option('first_name');
        $last_name = $this->option('last_name');
        $passwordOption = $password = $this->option('password');
        $phone_number = $this->option('phone_number');

        if ($email === null) {
            return $this->error('Please enter an email address');
        }

        if ($password === null) {
            $password = Str::random();
        }

        if ($phone_number === null) {
            return $this->error('Please enter an Phone number Ex. 966XXXXXXXXX');
        }

        \DB::table('users')->insert([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone_number' => $phone_number,
            'password' => Hash::make($password)
        ]);

        $user = User::where('email',$email)->first();

//        $user = User::create([
//            'first_name' => $first_name,
//            'last_name' => $last_name,
//            'email' => $email,
//            'password' => Hash::make($password)
//        ]);

        $user->assignRole(
            Role::where([
                'name' => EnumsRole::Admin,
                'guard_name' => 'web'
            ])->first()
        );

        if ($passwordOption === null) {
            $this->info("Password: {$password}");
        } else {
            $this->info('Admin created successfully');
        }
    }
}
