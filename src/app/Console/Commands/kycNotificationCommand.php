<?php

namespace App\Console\Commands;

use App\Models\Users\User;
use App\Mail\kyc\IdExpired;
use App\Traits\SendMessage;
use App\Services\LogService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Jobs\NotificationFirebaseBulk;
use App\Enums\Notifications\NotificationTypes;

class kycNotificationCommand extends Command
{
    use SendMessage;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-notification';
    public $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send notification to people doesnot complete KYC';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $usersCollection = collect();

            User::query()
                ->whereHas('nationalIdentity', function ($q) {
                    $q->where('created_at', '!=', DB::raw('updated_at'))
                        ->whereYear('id_expiry_date', '>', 1444);
                })->whereHas('investor', function ($m) {
                    $m->where('is_kyc_completed', 0);
                })->each(function ($user) use ($usersCollection) {

                    $this->count++;

                    if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                        Mail::to($user)->queue(new IdExpired($user));
                    }

                    $this->sendSms($user);
                    $usersCollection->push($user);
                });

            $notificationData = $this->notificationData();

            $notification = new NotificationFirebaseBulk(NotificationTypes::Info, $notificationData, $usersCollection);
            dispatch($notification)->onQueue('notify');

            dd($this->count);

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }


    public function notificationData()
    {

        return [
            'title_ar' => 'عضويات أصيل', [], 'ar',
            'title_en' => 'Aseel', [], 'ar',
            'content_ar' => ' نظراً لانتهاء تاريخ الهوية المسجلة لدينا يرجى تحديث بياناتكم لتفعيل حسابكم ', [], 'ar',
            'content_en' => ' Due to the expiration of your registered ID, please update the KYC form in your profile ', [], 'ar',
            'channel' => 'mobile'
        ];
    }

    public function sendSms($user)
    {
        $name = $user ? $user->full_name : '---';
        $content_ar_1 = "  نظراً لانتهاء تاريخ الهوية المسجلة لدينا يرجى تحديث بياناتكم لتفعيل حسابكم شكرا لتعاملكم مع منصة أصيل ";
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar_1;
        $content_en =  "Our dear investor ". $name ." Due to the expiration of your registered ID, please update the KYC form in your profile Thank you for trusting Aseel " ;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }

}
