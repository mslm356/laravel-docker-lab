<?php

namespace App\Common\ValidationRules\Listings;

use App\Rules\NumericString;

class ListingPayoutRules
{
    static public function get()
    {
        return [
            'total' => ['required', new NumericString, 'gt:0'],
            'type' => ['required', 'string', 'in:payout,distributed'],
        ];
    }

    static public function getForPay()
    {
        return [
            'investors' => ['required', 'array'],
            'investors.*.id' => ['required', 'exists:users,id'],
            'investors.*.amount' => ['required', new NumericString, 'gt:0']
        ];
    }
}
