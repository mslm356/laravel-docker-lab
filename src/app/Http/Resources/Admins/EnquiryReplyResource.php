<?php

namespace App\Http\Resources\Admins;

use Illuminate\Http\Resources\Json\JsonResource;

class EnquiryReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sender' => $this->sender_details,
            'message' => $this->message,
            'create_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at
        ];
    }
}
