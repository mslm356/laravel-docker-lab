<?php
namespace App\Console\Commands\Funds;
use App\Services\CacheServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
class UpdateListingInvestorRecord extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'funds:update-listing-investor-record {--user_id=}';
    public $cacheServices;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update fund percentage';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->cacheServices = new CacheServices();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $this->updateListingInvestorRecord();
            $this->updateListingInvestor();
        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }
    public function updateListingInvestorRecord ()
    {
        $listingInvestorRecordCount = 0;
        DB::table('listing_investor_records')
            ->join('listings', function ($join) {
                $join->on('listings.id', '=', 'listing_investor_records.listing_id');
            })
            ->where('listing_investor_records.investor_id', $this->option('user_id'))
            ->where('listing_investor_records.invested_shares', 0)
            ->select([
                'listing_investor_records.id',
                'listing_investor_records.investor_id',
                'listings.id as listing_id',
                'listing_investor_records.amount_without_fees',
                'listings.target',
                'listings.total_shares',
            ])
            ->orderBy('listing_investor_records.id')
            ->lazy(60000)
            ->each(function ($listingInvestorRecord) use (&$listingInvestorRecordCount) {
                $sharePrice = $listingInvestorRecord->target / $listingInvestorRecord->total_shares;
                $investedShares = $listingInvestorRecord->amount_without_fees / $sharePrice;
                DB::table('listing_investor_records')
                    ->where('id', $listingInvestorRecord->id)
                    ->update(['invested_shares' => $investedShares]);
                $listingInvestorRecordCount++;
            });
        dump('updated listing investor record => ' . $listingInvestorRecordCount);
    }
    public function updateListingInvestor () {

        number_format('', '', '', '');
        $listingInvestorCount = 0;
        DB::table('listing_investor')
            ->where('listing_investor.investor_id', $this->option('user_id'))
            ->where('listing_investor.invested_shares', 0)
            ->select([
                'listing_investor.id',
                'listing_investor.listing_id',
                'listing_investor.investor_id',
            ])
            ->orderBy('listing_investor.id')
            ->lazy(4000)
            ->each(function ($listingInvestor) use (&$listingInvestorCount) {
                DB::table('listing_investor_records')
                    ->where('listing_id', $listingInvestor->listing_id)
                    ->where('investor_id', $listingInvestor->investor_id)
                    ->update(['invested_shares' => 0]);
                $listingInvestorCount++;
            });
        dump('updated listing investor record by listing => ' . $listingInvestorCount);
    }
}
