<?php

namespace App\Mail\companyProfile;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Approve extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/app/overview';

        return $this->view('emails.company_profiles.approve', compact('profileUrl'))
            ->subject(__('emails/company_profiles/approve.subject'));
    }
}
