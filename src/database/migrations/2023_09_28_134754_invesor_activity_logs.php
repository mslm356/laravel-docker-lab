<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InvesorActivityLogs extends Migration
{

    public function up()
    {
        Schema::create('invesor_activity_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('method');
            $table->string('url');
            $table->json('request_headers')->nullable();
            $table->json('request_data')->nullable();
            $table->integer('response_status_code');
            $table->json('response_headers')->nullable();
            $table->text('response_content')->nullable();
            $table->json('session_data')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invesor_activity_logs');
    }

}
