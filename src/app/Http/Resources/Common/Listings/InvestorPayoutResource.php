<?php

namespace App\Http\Resources\Common\Listings;

use Illuminate\Http\Resources\Json\JsonResource;

class InvestorPayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->full_name,
            'status' => $this->status,
            'paid' => money($this->pivot->paid, defaultCurrency())->formatByDecimal(),
            'expected' => money($this->pivot->expected, defaultCurrency())->formatByDecimal(),
            'shares' => $this->pivot->shares

//                $this->whenLoaded('investments', function () {
//                $listingInvest = $this->investments->first();
//                return $this->pivot->expected;
//            })
        ];
    }
}
