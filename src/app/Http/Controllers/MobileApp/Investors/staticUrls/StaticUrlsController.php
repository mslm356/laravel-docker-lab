<?php

namespace App\Http\Controllers\MobileApp\Investors\staticUrls;

use App\Http\Controllers\Controller;
use App\Services\LogService;
use Illuminate\Http\Request;

class StaticUrlsController extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function index(Request $request)
    {
        try {
            $lang = $request->header('lang') ? $request->header('lang') : '';

            $result = $this->createData($lang);

            return apiResponse(200, __('mobile_app/message.success'), $result);

        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ');
        }
    }

    public function createData($lang)
    {

        $baseUrl = $lang ? 'https://investaseel.sa/'.$lang : 'https://investaseel.sa';

        $data = [
            'about_us' => $baseUrl . '/about-us?app',
            'privacy_policy' => $baseUrl . '/privacy-policy?app',
            'sharia_compliance' => $baseUrl . '/sharia-compliance?app',
            'terms_conditions' => $baseUrl . '/terms-and-conditions?app',
            'disclosure_policy' => $baseUrl . '/disclosure-policy?app',
            'membership_conditions' => $baseUrl . '/membership-terms-conditions?app',
            'directors' => $baseUrl . '/directors?app'
        ];
        return $data;
    }
}
