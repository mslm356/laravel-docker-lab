<?php

namespace App\Actions\Investors\ProfessionalUpgrade;

use Lorisleiva\Actions\Concerns\AsAction;
use App\Enums\Investors\UpgradeRequestStatus;
use App\Models\Investors\InvestorUpgradeRequest;

class CheckIfCanSubmitNewRequest
{
    use AsAction;

    /**
     * Check if the investor can submit new upgrade request
     *
     * @param \App\Models\User $user
     * @return boolean
     */
    public function handle($user)
    {
        $latestUpdgradeRequest = InvestorUpgradeRequest::where('user_id', $user->id)->where("type",$user->type)
            //            ->lockForUpdate()
            ->latest()
            ->first();

        return $latestUpdgradeRequest === null || false === in_array(
            $latestUpdgradeRequest->status,
            [UpgradeRequestStatus::PendingReview, UpgradeRequestStatus::InReview]
        );
    }
}
