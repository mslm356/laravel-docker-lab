<?php

namespace App\Support\Zatca\EInvoice;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class SellerAddress
{
    /**
     * Address line 1
     *
     * @var string
     */
    protected $line1;

    /**
     * Address line 2
     *
     * @var string
     */
    protected $line2;

    function __construct(array $line1, array $line2)
    {
        $this->line1 = $line1;
        $this->line2 = $line2;
    }

    /**
     * Get line 1
     *
     * @return string
     */
    public function getLine1(string $locale = null)
    {
        if ($locale === null) {
            $locale = App::getLocale();
        }

        return Arr::get($this->line1, $locale);
    }

    /**
     * Get line 2
     *
     * @return string
     */
    public function getLine2(string $locale = null)
    {
        if ($locale === null) {
            $locale = App::getLocale();
        }

        return Arr::get($this->line2, $locale);
    }

    /**
     * Get line 1
     *
     * @return $this
     */
    public function setLine1($value)
    {
        $this->line1 = $value;

        return $this;
    }

    /**
     * Get line 2
     *
     * @return $this
     */
    public function setLine2($value)
    {
        $this->line2 = $value;

        return $this;
    }
}
