<?php

namespace App\Http\Resources\Admins;

use App\Enums\EnquiryStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class EnquiryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reason' => $this->reason->name,
            'name' => $this->name,
            'email' => $this->email,
            'phone_number' => $this->fmt_phone,
            'subject' => $this->subject,
            'message' => $this->message,
            'status' => [
                'value' => $this->status,
                'description' => EnquiryStatus::getDescription($this->status)
            ],
            'replies' => EnquiryReplyResource::collection($this->whenLoaded('replies')),
            'create_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at
        ];
    }
}
