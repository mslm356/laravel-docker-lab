<?php

namespace App\Services\Campaigns\InvestorTypes;

use Illuminate\Support\Facades\DB;

class CustomType
{

    public function getUsersData(array $selectedData, array $customData = [])
    {
        $handleCustomIds = explode(',', $customData['custom_ids']);

        $usersIds = is_array($handleCustomIds) ? $handleCustomIds : [];

        $data = DB::table('users')
            ->whereIn('id', $usersIds)
            ->select($selectedData)
            ->get();

        return json_decode($data, 1);
    }

}
