<?php

namespace App\Enums\MemberShips;

use BenSampo\Enum\Enum;

final class MemberShipAssessment extends Enum
{
    const AutoAssessment = 1;
    const AdminAssessment = 2;
}
