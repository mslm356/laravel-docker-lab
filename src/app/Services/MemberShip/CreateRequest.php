<?php


namespace App\Services\MemberShip;

use App\Enums\MemberShips\MemberShipAssessment;
use App\Enums\MemberShips\MemberShipStatus;
use App\Enums\MemberShips\MemberShipType;
use App\Models\MemberShip\MemberShipRequests;
use App\Services\LogService;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CreateRequest
{
    use NotificationServices;

    public $alertUserService;
    public $count=0;

    public function __construct()
    {
        $this->alertUserService = new AlertUser();
    }

    public function autoRequest()
    {
        $duringTimeValue = config('memberShip.during_time');
        $duringDate = now()->subMonths($duringTimeValue);
        $goldValue = config('memberShip.gold');
        $diamondValue = config('memberShip.diamond');

        DB::table('users')
            ->join('listing_investor', function ($join) use ($duringDate) {
                $join->on('users.id', '=', 'listing_investor.investor_id')
                    ->whereDate('listing_investor.created_at', '>=', $duringDate);
            })
            ->select('users.id as user_id', DB::raw('SUM(invested_amount) as invested_amountss'), 'users.member_ship')
            ->whereNotIn('users.member_ship', [MemberShipType::Diamond,MemberShipType::Blocked])
            ->groupBy('users.id')
            ->having('invested_amountss', '>', $goldValue * 100)
            ->orderBy('users.id', 'asc')
            ->lazy(100)
            ->each(function ($user) use ($diamondValue, $goldValue) {

                $amount = money($user->invested_amountss, defaultCurrency())->formatByDecimal();

                if ($amount > $diamondValue) {
                    $memberShip = MemberShipType::Diamond;
                } elseif ($amount > $goldValue) {
                    $memberShip = MemberShipType::Gold;
                } else {
                    $memberShip = MemberShipType::Regular;
                }

                if ($this->canCreateMemberShipRequest($user->user_id, $memberShip)) {

                    $memberShipRequestData = [
                        'user_id' => $user->user_id,
                        'member_ship' => $memberShip,
                    ];

                    dump($this->count++);

                    $this->createRequest($memberShipRequestData, true, 'create');
                }

            });

        dump($this->count);
    }

    public function createRequest($requestData, $withAlert = true, $alertType = 'create')
    {
        $memberShipRequest = MemberShipRequests::create($requestData);

        if ($withAlert) {
            $this->alertUserService->handle($memberShipRequest, $alertType);
        }
    }

    public function canCreateMemberShipRequest($userId, $memberShip, $type = 'job')
    {

        if ($memberShip == MemberShipType::Regular && $type == 'job') {
            return false;
        }

        $lastMemberShip = DB::table('member_ship_requests')
            ->where('user_id', $userId)
            ->orderBy('id', 'desc')
            ->first();

        if (!$lastMemberShip) {
            return true;
        }

        if (in_array($lastMemberShip->status, [MemberShipStatus::Approved, MemberShipStatus::Pending, MemberShipStatus::Rejected])
            && $memberShip == $lastMemberShip->member_ship) {
            return false;
        }

        if ($type == 'job' && $lastMemberShip->initiator_type == MemberShipAssessment::AdminAssessment) {
            return false;
        }

        return true;
    }

    public function approveMemberShip($status, $memberShip)
    {
        $memberShip->update(['status' => $status]);
        return true;
    }

    public function getMemberShipName($value)
    {

        if ($value == MemberShipType::Diamond) {
            return __('Diamond');
        } elseif ($value == MemberShipType::Gold) {
            return __('Gold');
        } else {
            return __('Regular');
        }

    }

    public function investorDecision($user, $status = null)
    {

        $lastMemberShip = $user->memberShips()->orderBy('id', 'desc')->first();

        if (!$lastMemberShip) {
            throw ValidationException::withMessages(['member_ship' => __('mobile_app/message.not_found')]);
        }

        if ($lastMemberShip->status != MemberShipStatus::Pending) {
            throw ValidationException::withMessages(['member_ship' => __('Not have pending requests')]);
        }

        $lastMemberShip->status = MemberShipStatus::Approved;
        $lastMemberShip->show_popup = true;
        $lastMemberShip->save();

        $user->member_ship = $lastMemberShip->member_ship;
        $user->save();

        $this->alertUserService->handle($lastMemberShip, 'accept');

        return true;
    }

    public function newInvestorDecision($user, $status = null)
    {
        $lastMemberShip = $user->memberShips()->orderBy('id', 'desc')->first();

        if (!$lastMemberShip) {
            throw ValidationException::withMessages(['member_ship' => __('mobile_app/message.not_found')]);
        }

        $lastMemberShip->status = $status == true ? MemberShipStatus::Approved : $lastMemberShip->status ;
        $lastMemberShip->show_popup = false;
        $lastMemberShip->save();

        if($status == true) {
            $user->member_ship = $lastMemberShip->member_ship;
            $user->save();

            $this->alertUserService->handle($lastMemberShip, 'accept');
        }

        return true;
    }


    public function expireMemberships()
    {
        $duringTimeValue = config('memberShip.during_time');
        $duringDate = now()->subMonths($duringTimeValue);

        DB::table('users')
            ->where('member_ship', '!=', MemberShipType::Regular)
            ->join('listing_investor', function ($join) use ($duringDate) {
                $join->on('users.id', '=', 'listing_investor.investor_id')
                    ->whereDate('listing_investor.created_at', '>=', $duringDate);
            })
            ->select('users.id as user_id', DB::raw('SUM(invested_amount) as invested_amount'), 'users.member_ship')
            ->groupBy('users.id')
            ->lazyById(1000, 'user_id')
            ->each(function ($user) {

                $membershipRequest = DB::table('member_ship_requests')
                    ->where('user_id', $user->user_id)
                    ->where('member_ship', $user->member_ship)
                    ->where('status', MemberShipStatus::Approved)
                    ->orderBy('id', 'desc')
                    ->first();

                if ($membershipRequest) {
                    $this->updateMembershipToExpire($membershipRequest, $user);
                }

            });
    }

    public function updateMembershipToExpire($membershipRequest, $user)
    {

        try {

            $goldValue = config('memberShip.gold');
            $diamondValue = config('memberShip.diamond');
            $logService = new LogService();
            $updateStatus = false;
            $investedAmount = money($user->invested_amount, defaultCurrency())->formatByDecimal();
            $membershipValue = $user->member_ship == MemberShipType::Diamond ? $diamondValue : $goldValue;

            if ($membershipRequest->initiator_type == MemberShipAssessment::AdminAssessment) {
                $expire_at = Carbon::parse($membershipRequest->expire_at);
                $updateStatus = $expire_at->lessThanOrEqualTo(now()) ? true : false;
            }

            if (!$updateStatus && $investedAmount < $membershipValue) {
                $updateStatus = true;
            }

            if ($updateStatus) {

                DB::table('member_ship_requests')
                    ->where('id', $membershipRequest->id)
                    ->update(['status' => MemberShipStatus::Expired]);

                DB::table('users')
                    ->where('id', $user->user_id)
                    ->update(['member_ship' => MemberShipType::Regular]);

                $this->alertUserService->handle($membershipRequest, 'downGrad');
            }

        } catch (\Exception $e) {
            $logService->log('UPDATE-MEMBERSHIP-TO-EXPIRE', $e);
        }
    }

    public function userCompensationWithGoldMembership($user)
    {

        try {
            if ($user->member_ship && ($user->member_ship > MemberShipType::Regular)) {
                return var_dump('current membership is '.$user->member_ship);
            }


            $lastMemberShip = DB::table('member_ship_requests')
                ->where('user_id', $user->investor_id)
                ->orderBy('id', 'desc')
                ->first();

            if ($lastMemberShip && $lastMemberShip->member_ship > MemberShipType::Regular && in_array($lastMemberShip->status, [MemberShipStatus::Approved, MemberShipStatus::Pending])) {
                return var_dump('user_id '.$user->investor_id.' has '.$lastMemberShip->status.' membership with type' . $lastMemberShip->member_ship);
            }


            $membershipData = [
                'member_ship' => MemberShipType::Gold,
                'user_id' => $user->investor_id,
                'comment' => 'User Compensation for foa fund',
                'initiator_type' => MemberShipAssessment::AdminAssessment,
                //     'created_by' => auth()->id(), get admin id
                'expire_at' => now()->addMonths(6),
            ];


            $this->createRequest($membershipData, false, 'create');
            return true;

        } catch (\Exception $e) {
            $logService = new LogService();
            $logService->log('User-Compensation-With-Gold-Membership', $e);
        }

    }


}
