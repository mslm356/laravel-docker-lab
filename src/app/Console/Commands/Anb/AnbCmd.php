<?php

namespace App\Console\Commands\Anb;

use App\Services\Anb\PullStmtService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AnbCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:stmt-cmd';
    public $pullStmtService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Anb pull stmt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->pullStmtService = new PullStmtService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->pullStmtService->handlePullStmt();
        dump('done');
    }

}
