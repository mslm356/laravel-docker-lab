<?php

namespace App\Http\Controllers\MobileApp\Investors\SubscriptionForm;

use App\Services\LogService;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionForm\SendRequest;
use Illuminate\Http\Request;

class RequestsController extends Controller
{
    public $logService;
    public $sendRequest;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->sendRequest = new SendRequest();
    }

    public function store(Request $request)
    {
        try {
            $user = getAuthUser('api');
            $response = $this->sendRequest->handle($request, $user);

            if (isset($response['status']) && !$response['status']) {
                $msg = isset($response['message']) ? $response['message'] : 'sorry please try later';
                return apiResponse(400, __($msg));
            }

            $responseMessage = isset($response['message']) ? $response['message'] : [];

            return apiResponse(200, __($responseMessage), []);

        } catch (\Exception $e) {
            $code = $this->logService->log('SEND-SUBSCRIPTION-FORM-MOBILE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }
}
