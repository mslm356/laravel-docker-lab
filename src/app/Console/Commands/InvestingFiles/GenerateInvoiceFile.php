<?php

namespace App\Console\Commands\InvestingFiles;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Actions\Listings\GetCurrentInvestments;
use App\Enums\Role;
use App\Jobs\Zatca\GenerateEInvoiceAfterInvesting;
use App\Models\Elm\AbsherOtp;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\Zatca\ZatcaInvoice;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Zatca\EInvoice\Order;
use App\Support\Zatca\EInvoice\PurchaseLine;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Validation\ValidationException;

class GenerateInvoiceFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'z-invoice:file {--listing_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate z-invoice file';
    protected $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    public function handle()
    {
        try {

            return DB::transaction(function () {

                ListingInvestorRecord::query()->with(['listing', 'zatcaInvoice', 'investor'])
                    ->where('listing_id', $this->option('listing_id'))
                    ->lazyById()
                    ->each(function ($record) {

                        if ($record->zatcaInvoice && !$record->zatcaInvoice->pdfFile && $record->investor) {

                            $user = $record->investor;

                            $listingRecords = $record;

                            $feePercentage = $listingRecords->admin_fees_percentage;

                            $vatPercentage = $listingRecords->vat_percentage;

                            $fees = $listingRecords->admin_fees;

                            $listing = $listingRecords->listing;

                            GenerateEInvoiceAfterInvesting::dispatch(
                                $user,
                                new Order(
                                    transaction_ref(),
                                    [
                                        new PurchaseLine(
                                            [
                                                'en' => "({$feePercentage}%) of investing in {$listing->title_en}",
                                                'ar' => "(%{$feePercentage}) من مبلغ الاستثمار في {$listing->title_ar}",
                                            ],
                                            1,
                                            $fees,
                                            $vatPercentage,
                                            null
                                        )
                                    ],
                                    now('Asia/Riyadh'),
                                    $listingRecords
                                ),
                                app(ZatcaSettings::class)->seller,
                                'zatca-e-invoices',
                                null
                            );
                        }
                    });

            });


        } catch (\Exception $e) {
            $this->logService->log('GENERATE-E-INVOICE-FILE-EXCEPTION', $e);
        }

    }


}
