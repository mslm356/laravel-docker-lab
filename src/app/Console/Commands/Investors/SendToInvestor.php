<?php

namespace App\Console\Commands\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Console\Command;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\Mail;
use App\Jobs\NotificationFirebaseBulk;
use App\Enums\Notifications\NotificationTypes;

class SendToInvestor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:alerts {--listing=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            $listing = Listing::find($this->option('listing'));
            $usersCollection = collect();

            User::role(Role::Investor)
                ->lazyById()
                ->each(function ($user) use ($listing, $usersCollection) {
                    if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                        Mail::to($user)->send(new \App\Mail\Funds\CreateFund($user, $listing));
                    }

                    $usersCollection->push($user);
                });

            $notification = new NotificationFirebaseBulk(NotificationTypes::Fund, $this->notificationData($listing), $usersCollection);
            dispatch($notification)->onQueue('notify');

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }

    public function notificationData($listing)
    {
        return [
            'action_id'=> $listing->id,
            'channel'=>'mobile',
            'title_en'=>'New Fund',
            'title_ar'=>'فرصة جديده',
            'content_en'=>'An opportunity to invest in the platform has been presented',
            'content_ar'=>'تم طرح فرصة للاستثمار في المنصة',
        ];
    }
}
