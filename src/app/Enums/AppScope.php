<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class AppScope extends Enum
{
    const Admin = 'admin';
    const Investor = 'investor';
    const AuthorizedEntity = 'authorized_entity';
}
