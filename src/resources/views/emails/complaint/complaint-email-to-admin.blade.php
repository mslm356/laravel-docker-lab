@extends('emails.partials.main')
@section('content')
    <tr> {{ __('emails/complaint/admin-email.dear', ['name' => $admin_name]) }} </tr>
    <br>

    <tr> {{ __('emails/complaint/admin-email.message', ['ticketNumber' => $complaint->id]) }} </tr>
    <tr> At {{ \Carbon\Carbon::parse($complaint->created_at)->timezone('Asia/Riyadh')->format('y-m-d h:i:s')}} </tr>
    <br>
    <tr> Aseel </tr>
    <br>
@endsection


