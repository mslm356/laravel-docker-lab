<?php

namespace App\Support\Transactions\Descriptions;

use App\Models\VirtualBanking\Transaction;

abstract class GeneratorBase implements GeneratorInterface
{
    public function generate(Transaction $transaction, $locale = null): string
    {
        if ($transaction->credit > 0) {
            return $this->generateForCredit($transaction, $locale);
        } else {
            return $this->generateForDebit($transaction, $locale);
        }
    }

    abstract protected function generateForCredit(Transaction $transaction, $locale);

    abstract protected function generateForDebit(Transaction $transaction, $locale);
}
