<?php

return [
    'subject' => 'رد على التذكرة #id:',
    'body' => 'لديك رد جديد:',
    'original_ticket' => 'الاستفسار الذي أرسلته هو:',
    'reply_by_reply_to_this' => 'يمكنك متابعة استفسارك بالرد على هذه الرسالة.'
];
