const mix = require('laravel-mix');
require('laravel-mix-alias');

mix.alias('@admin', '/resources/js/admin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.extend('i18n', new class {
    webpackRules() {
        return [
            {
                resourceQuery: /blockType=i18n/,
                type: 'javascript/auto',
                loader: '@kazupon/vue-i18n-loader',
            },
        ];
    }
}(),
);

mix.i18n()
    .js('resources/js/website/website.js', 'public/js/website')
    .js('resources/js/admin/admin.js', 'public/js')
    .js('resources/js/app.js', 'public/js')
    // .postCss('resources/css/teaser.css', 'public/css', [
    //     require('tailwindcss'),
    // ])
    .postCss('resources/css/website.css', 'public/css', [
        require('tailwindcss')('tailwind-website.config.js'),
    ]);
    // .postCss('resources/css/admin.css', 'public/css', [
    //     require('tailwindcss')('tailwind-admin.config.js'),
    // ])
