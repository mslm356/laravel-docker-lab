<?php

namespace App\Console\Commands\Payout;

use App\Enums\Banking\TransactionReason;
use App\Enums\Notifications\NotificationTypes;
use App\Mail\Investors\NewTransaction;
use App\Models\Payouts\Payout;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Services\LogService;
use App\Traits\NotificationServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class PayoutAlert extends Command
{

    use NotificationServices;

    public $logService;
    public $count=0;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payout:alert {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'payout alert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            Transaction::query()
                ->where('reason', TransactionReason::PayoutDistribution)
                ->where('payable_type', User::class)
                ->where('created_at', 'like', '%'.$this->option('date').'%')
                ->lazyById()
                ->each(function ($transaction) {

                    $user = $transaction->payable;
                    Mail::to($transaction->payable)->queue(new NewTransaction($user, $transaction));

                    $fund_title = isset($transaction->meta["listing_title_" . $user->locale]) ? $transaction->meta["listing_title_" . $user->locale] : "---";

                    $notificationData = $this->payoutNotificationData($user, $transaction, $fund_title);
                    $this->send(NotificationTypes::Deposit, $notificationData, $user->id);

                    $this->count++;

                    dump('sent...');

                });

            dump($this->count);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }

    }

    public function payoutNotificationData($user, $transaction, $listingTitle)
    {

        $payout_id = isset($transaction->meta["payout_id"]) ? $transaction->meta["payout_id"] : null;
        $payout = Payout::where('id',$payout_id)->first();

        $amount = $transaction->amount_float;
        $content_ar_1 =   ' إيداع العوائد النقدية لصندوق ' . '(#'.$listingTitle.')' ;
        $content_ar_1_dis =   ' إيداع  التوزيعات النقدية لصندوق ' . '(#'.$listingTitle.')' ;
        $content_ar_2 = 'وتوزيع المستحقات في حسابك ' . $amount . ' ريال سعودى ';
        $content_ar_2_dis = 'وتوزيع الأرباح في حسابك ' . $amount . ' ريال سعودى ';

        return [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => $payout && $payout->type == "payout" ?  'Deposit cash returns' : 'Deposit cash dividends ',
            'title_ar' => $payout && $payout->type == "payout" ? 'إيداع عوائد نقدية' : 'إيداع توزيعات نقدية',
            'content_en' => $payout && $payout->type == "payout" ? 'Cash proceeds have been deposited to Fund (#' . $listingTitle . ') And distribute the returns in your account is ' . $amount . ' SAR' :
                'The cash dividends have been deposited for Fund (#' . $listingTitle . ') And the profit distributed in your account is ' . $amount . ' SAR',
            'content_ar' => $payout && $payout->type == "payout" ? $content_ar_1 . "\n" . $content_ar_2 : $content_ar_1_dis . "\n" . $content_ar_2_dis,
        ];
    }

}
