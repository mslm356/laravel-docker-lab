<?php

namespace App\Http\Resources\Admins\Listings;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Listings\DiscussionResource as CommonDiscussionResource;

class DiscussionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonDiscussionResource($this->resource))->toArray($request),
            [
                'replies' => DiscussionReplyResource::collection(
                    $this->whenLoaded('replies')
                ),
            ]
        );
    }
}
