<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Enums\Listings\ListingReviewStatus;
use App\Enums\Listings\Report;
use App\Enums\Listings\ReportEnum;
use App\Enums\Role;
use App\Jobs\Funds\SendAlertMailsOnCreate;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Listings\ListingReport;
use App\Http\Resources\Admins\FileResource;
use App\Actions\Listings\Reports\UpdateOrCreateReport;
use App\Common\ValidationRules\Listings\ListingReportRules;
use App\Http\Resources\Admins\Listings\ListingReportResource;

class ListingReportController extends Controller
{
    /**
     * Get all the reports of listing
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listing $listing)
    {
        $reports = $listing->reports()
            ->with([
                'files'
            ])
            ->latest()
            ->paginate();

        return ListingReportResource::collection($reports);
    }

    /**
     * Store the report in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($listing, ListingReport $report)
    {

        $report['type'] = $report->user && $report->user->hasRole(Role::AuthorizedEntityAdmin) ? "Authorized Entity" : "Admin";

        $report['status'] = [
            'value' => $report->status,
            'description' => ReportEnum::getDescription($report->status)
        ];

        return response()->json([

            'data' => $report->toArray() + [
                    'files' => FileResource::collection($report->files)
                ]
        ]);
    }

    /**
     * Store the report in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        $data = $this->validate($request, ListingReportRules::get());
        DB::transaction(function () use ($data, $listing,$request) {
            UpdateOrCreateReport::run($this->mapData($data), $listing,null,$request);
        });
        if (config('mail.MAIL_STATUS') == 'ACTIVE') {
            $sendAlertMailsOnCreateReportJob = (new SendAlertMailsOnCreateReport($listing));
            dispatch($sendAlertMailsOnCreateReportJob);
        }

        return response()->json([], 201);
    }

    /**
     * Update the report in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Listings\ListingReport $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $listing, ListingReport $report)
    {
        $data = $this->validate($request, ListingReportRules::get());

        DB::transaction(function () use ($data, $report,$request) {
            UpdateOrCreateReport::run($this->mapData($data), null, $report,$request);
        });

        return response()->json();
    }


    public function updateStatus(Request $request, ListingReport $report)
    {
        $data = $this->validate($request, [
            'status' => ['required', 'integer', new EnumValue(ReportEnum::class, false)],
            'comment' => ['exclude_unless:status,' . ReportEnum::Rejected, 'nullable', 'string', 'max:1000', textValidation()]
        ]);

        if ($request->status == $report->status)
            return response()->json();

        return DB::transaction(function () use ($data, $report, $request) {
            $report->update([
                'status' => $data['status'],
                'reviewer_comment' => $data['status'] == ReportEnum::Rejected ? $data['comment'] : null,
                'reviewer_id' => $request->user('sanctum')->id,
            ]);

            if($report->status == ReportEnum::Approved)
            {
                $listing = Listing::find($report->listing_id);
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    $sendAlertMailsOnCreateReportJob = (new SendAlertMailsOnCreateReport($listing));
                    dispatch($sendAlertMailsOnCreateReportJob);
                }
            }
            return response()->json();
        });
    }

    /**
     * map data for saving
     *
     * @param array $data
     * @return array
     */
    public function mapData($data)
    {
        return [
            'title_en' => $data['title_en'],
            'title_ar' => $data['title_ar'],
            'description_en' => $data['description_en'] ?? null,
            'description_ar' => $data['description_ar'] ?? null,
            'files' => Arr::get($data, 'files', [])
        ];
    }
}
