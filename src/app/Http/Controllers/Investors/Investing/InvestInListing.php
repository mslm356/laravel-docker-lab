<?php

namespace App\Http\Controllers\Investors\Investing;

use App\Http\Controllers\Controller;
use App\Services\Investors\Investment\InvestingServices;
use Illuminate\Http\Request;

class InvestInListing extends Controller
{
    public $investingServices;

    public function __construct()
    {
        $this->investingServices = new InvestingServices();
    }


    public function __invoke(Request $request, $token)
    {
        try {
            $user = getAuthUser('web');
            $request->request->add(['token'=> $token]);

            $response = $this->investingServices->handle($request, $user);
            $customData['status'] = trans('messages.invest_success');

            return $this->handelStatusResponce($response,$customData);

        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'INVESTMENT-MOBILE');
        }
    }
}
