<?php

namespace App\Console\Commands\Investors;

use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Validation\ValidationException;

class RemoveDuplicated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:remove-duplicated {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'verify account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        try {

            $user = User::find($this->option('user_is'));

            $userWallet = $user->getWallet(WalletType::Investor);

            dd($userWallet);

            $balance = $userWallet->balance;

            dd($balance);

            $answer = $this->ask('are you sure ? ' . $balance, 'y');

            dd($answer);

            if (!$user || !$user->investor) {
                dd('user not valid or profile');
            }


        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage(), ['errors' => $errors]);
        } catch (VerificationSmsException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

    }


}
