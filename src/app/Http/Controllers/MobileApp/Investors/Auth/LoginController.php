<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Enums\Investors\InvestorStatus;
use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Models\Users\User;
use App\Services\Investors\FirebaseTokenServices;
use App\Services\Investors\TokenServices;
use App\Services\LogService;
use App\Support\Elm\AbsherOtp\TwoFactorAuth;
use App\Support\Users\UserAccess;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public $twoFactorAuthenticate;
    public $tokenServices;
    public $firebaseTokenServices;
    public $logService;
    public $userAccess;


    protected $maxAttempts = 3;
    protected $decayMinutes = 5;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->tokenServices = new TokenServices();
        $this->twoFactorAuthenticate = new TwoFactorAuth();
        $this->firebaseTokenServices = new FirebaseTokenServices();
        $this->userAccess = new UserAccess();
    }

    public function username()
    {
        return 'phone';
    }

    public function sendLockoutResponse($request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return apiResponse(400, trans('auth.throttle_with_minute', [
            'seconds' => $seconds,
            'minutes' => ceil($seconds / 60),
        ]));
    }

    public function login(Request $request)
    {

        try {

            $data = $this->validator($request->all())->validate();

            if (config('auth.login_limit') == 'active' && method_exists($this, 'hasTooManyLoginAttempts')
                && $this->hasTooManyLoginAttempts($request)) {

                $this->fireLockoutEvent($request);
                return $this->sendLockoutResponse($request);
            }

            $this->incrementLoginAttempts($request);

            $user = $this->checkIsValidAuth($data);

            if ($user == null) {
                return apiResponse(400, __('mobile_app/message.auth_failed'));
            }

            $profile = DB::table('investor_profiles')
                ->where('user_id', $user->id)
                ->select('status')
                ->first();

            if ($profile && $profile->status == InvestorStatus::Deleted) {
                return apiResponse(400, __('messages.deleted_account'));
            }

            if (!in_array($profile->status, [InvestorStatus::PendingReview, InvestorStatus::Approved])) {
                return apiResponse(400, __('messages.status_approved'));
            }

            $hasInvestorRole = DB::table('model_has_roles')
                ->where('model_id', $user->id)
                ->where('model_type', User::class)
                ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->where('roles.name', Role::Investor)
                ->count();

            if (!$hasInvestorRole) {
                return apiResponse(400, __('mobile_app/message.not_permission'));
            }

            $firebaseToken = $request->has('device_token') ? $request->device_token : null;

            $msg = $this->twoFactorAuthenticate->twoFactorAuth($user, $firebaseToken);

            if (!$msg) {
                return apiResponse(400, __('mobile_app/message.profile_not_verified'));
            }

            $responseData = ['vid' => $user->vid];

            return apiResponse(200, __('mobile_app/message.otp_send'), $responseData);

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-LOGIN', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('LOGIN', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function checkIsValidAuth($date)
    {

        $phoneNumber = phone($date['phone'], "SA" /*$request['phone_country_iso2']*/);

        $user = User::query()
            ->where('phone_number', $phoneNumber)
            ->where('type', 'user')
            ->select('phone_number', 'id', 'type', 'password', 'firebase_token', 'code')
            ->first();

        if ($user && Hash::check($date['password'], $user->password)) {
            return $user;
        }

        return null;
    }

    public function validator($data)
    {
        return Validator::make($data, [
            'phone' => ['required', 'string', 'phone:phone_country_iso2'],
            'password' => ['required']
        ]);
    }

    public function verifyCode(Request $request)
    {

        try {

            $data = $this->validate($request, $this->twoFactorAuthenticate->getRulesForMobile());

            $data['type'] = 'mobile_app';

            return DB::transaction(function () use ($data) {
                return $this->twoFactorAuthenticate->verify($data);
            }, 1);

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-VERIFY-LOGIN', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('VERIFY-LOGIN', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function logout(Request $request)
    {

        $request->validate([
            'type' => 'nullable|in:0,1',
            'device_id' => 'nullable|string',
        ]);

        try {

            $user = getAuthUser('api');

            if (!$user) {
                return apiResponse(400, __('mobile_app/message.user_not_valid'));
            }

            DB::table('users')
                ->where('id', $user->id)
                ->update(['firebase_token' => null, 'jwt_token' => null]);

            //            auth('api')->logout();

            return apiResponse(200, __('mobile_app/message.success'));

        } catch (\Exception $e) {
            $code = $this->logService->log('REGISTER', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function updateUserFirebaseToken($data, $user)
    {
        if (isset($data['device_token']) && isset($data['device_id'])) {
            $this->firebaseTokenServices->createUserFirebaseToken($user, $data['device_token'], $data['device_id'], $data['lang']);
        }
    }

}
