<?php

namespace App\Enums\Investors;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class InvestorStatus extends Enum implements LocalizedEnum
{
    const PendingReview = 1;
    const InReview = 2;
    const Approved = 3;
    const Rejected = 4;
    const Suspended = 5;
    const KycNotCompleted = 6;
    const AutoAmlCheckFailed = 7;
    const Deleted = 8;
}
