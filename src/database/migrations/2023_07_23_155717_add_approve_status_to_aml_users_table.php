<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApproveStatusToAmlUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aml_users', function (Blueprint $table) {
            $table->boolean('approve_status')->after('response')->default(0);
            $table->json('approve_response')->after('response')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aml_users', function (Blueprint $table) {
            $table->dropColumn('approve_response');
            $table->dropColumn('approve_status');
        });
    }
}
