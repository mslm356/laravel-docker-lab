<?php

namespace App\Http\Controllers;

use App\Services\LogService;
use App\Support\Aml\AmlServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AmlStatusController extends Controller
{

    public $logServices;
    public $amlServices;

    public function __construct()
    {
        $this->logServices = new LogService();
        $this->amlServices = new AmlServices();
    }

    public function amlStatus(Request $request)
    {

        try {

            DB::beginTransaction();

            $data = $request->all();
            $this->amlServices->createAmlHistory($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            $this->logServices->log('AML-STATUS-EXCEPTION', $e);
            return apiResponse(400, 'sorry please try later');
        }

        return apiResponse(200, 'done');
    }

    public function testAmlStatus(Request $request)
    {
        try {

            $data = [
                'event'=> 'screen.decision.approve',
            ];

            $this->amlServices->testWebhook($data);

        } catch (\Exception $e) {
            $this->logServices->log('AML-EXCEPTION', $e);
            return null;
        }
    }
}
