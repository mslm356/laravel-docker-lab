<?php


namespace App\Traits;

use App\Jobs\Notification;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;

trait NotificationServices
{

    public function send(string $type, array $data, int $receiver_id = null)
    {
        try {
            $notificationJob = new Notification($type, $data, $receiver_id);
            dispatch($notificationJob)->onQueue('notify');

            //            $validationResponse = $this->validatedData($data, $receiver_id);
            //
            //            if (!$validationResponse) {
            //                return false;
            //            }
            //
            //            $this->sendToMobile($type, $data, $receiver_id);
            //
            //            return true;


        } catch (\Exception $e) {
            return false;
        }
    }

    private function validatedData($data, $receiver_id)
    {
        $receiver = DB::table('users')
            ->where('id', $receiver_id)
            ->count();

        if (!isset($data['channel']) || !in_array($data['channel'], ['web', 'mobile', 'admin'])) {
            return false;
        }

        if (in_array($data['channel'], ['web', 'mobile']) && (!$receiver_id || !$receiver)) {
            return false;
        }

        return true;
    }

    private function sendToMobile($type, $data, $receiver_id)
    {
        $notificationClass = 'App\Notifications\\' . $type;

        $user = User::query()
            ->where('id', $receiver_id)
            ->where('type', 'user')
            ->select('id', 'locale', 'firebase_token')
            ->first();

        if ($user) {
            $data['user_id'] = $user->id;
            $user->notify(new $notificationClass($data));
            $this->sendNotificationToFirebase($user, $data, $type);
        }
    }

    public function sendNotificationToFirebase($user, $data, $notificationType)
    {

        $lang = $user->locale;
        //        $tokens = array_values(array_unique($user->userFirebaseTokens->pluck('token')->toArray()));

        $tokens = [$user->firebase_token];

        $msg = [
//
            'title' => isset($data['title_' . $lang]) ? $data['title_' . $lang] : '',
            'body' => isset($data['content_' . $lang]) ? $data['content_' . $lang] : '',
            'action_id' => isset($data['action_id']) ? $data['action_id'] : '',
            'action_type' => $notificationType
        ];

        $notification = [

            'title' => isset($data['title_' . $lang]) ? $data['title_' . $lang] : '',
            'body' => isset($data['content_' . $lang]) ? $data['content_' . $lang] : '',
            'sound' => true,
        ];

        $fcmNotification = [
            'registration_ids' => $tokens,
            'notification' => $notification,
            'data' => $msg
        ];

        $server_key = config('notification.server_key');

        $headers = [
            'Authorization:key=' . $server_key,
            'Content-Type: application/json'
        ];

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        $res_json = curl_exec($ch);

        $result = json_decode($res_json) ? get_object_vars(json_decode($res_json)) : [];

        $code = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
        $merge1 = array_merge($code, $fcmNotification);
        $merge = array_merge($result, $merge1);
        $reposeDet = array('response' => $merge);

        curl_close($ch);

        return $reposeDet;
    }
}
