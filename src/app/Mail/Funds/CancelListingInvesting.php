<?php

namespace App\Mail\Funds;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CancelListingInvesting extends Mailable
{
    use Queueable, SerializesModels;

    public $listingInvestor;
    public $total;
    public $time;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($listingInvestor, $total, $time)
    {
        $this->listingInvestor = $listingInvestor;
        $this->total = $total;
        $this->time = $time;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->listingInvestor->full_name;
        $fund_title = $this->listingInvestor->locale == "ar" ? $this->listingInvestor->title_ar : $this->listingInvestor->title_en;

        $amount = $this->total;
        $listingInvestor = $this->listingInvestor;
        $time = $this->time;

        return $this->view('emails.funds.cancel_listing_investing', compact('name', 'fund_title', 'amount', 'time', 'listingInvestor'))
            ->subject(__('emails/funds/cancel_listing_investing.subject'));
    }
}
