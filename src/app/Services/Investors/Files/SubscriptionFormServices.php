<?php

namespace App\Services\Investors\Files;

use App\Actions\ExportSubscriptionForm;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Actions\Investors\RenderSubscriptionForm;
use App\Enums\FileType;
use App\Models\File;
use App\Models\Listings\ListingInvestorRecord;
use App\Services\LogService;
use Illuminate\Support\Str;

class SubscriptionFormServices
{

    protected $logServices;
    protected $listingRecord;

    public function __construct(ListingInvestorRecord $listingRecord)
    {
        $this->logServices = new LogService();
        $this->listingRecord = $listingRecord;
    }

    public function getFileUrl()
    {

        if ($this->listingRecord->subscriptionForm) {
            return route('investor.files.get.url', ['file'=> $this->listingRecord->subscriptionForm->id]);
        }

        $file = $this->generate($this->listingRecord);

        return route('investor.files.get.url', ['file'=> $file->id]);
    }

    public function generate(ListingInvestorRecord $record)
    {

        try {

            $user = $record->investor;
            $listing = $record->listing;
            $subscriptionToken = $record->subscriptionToken;

            $formData = $this->formData($record, $listing, $user, $subscriptionToken);

            $todayDate = now('Asia/Riyadh')->format('Y-m-d');

            $html = RenderSubscriptionForm::run($formData)->render();

            /** @var \Illuminate\Http\UploadedFile $file */
            $filePath = ExportSubscriptionForm::run(
                $originalFileName = "{$todayDate}-{$subscriptionToken->id}-{$user->id}-subscriptionForm.pdf",
                $html
            );

            $fileModel = File::create([
                'id' => (string)Str::uuid(),
                'name' => $originalFileName,
                'fileable_id' => $record->id,
                'fileable_type' => $record->getMorphClass(),
                'type' => FileType::SubscriptionForm,
                'extra_info' => collect([
                    'listing_id' => $listing->id,
                    'data' => $formData
                ]),
                'path' => $filePath
            ]);

        } catch (\Exception $e) {
            $this->logServices->log('SUBSCRIPTION-FORUM-EXCEPTION', $e);
        }

        return $fileModel;
    }

    public function formData($listingInvestorRecord, $listing, $user, $token)
    {
        $amountAndFees = $listingInvestorRecord->amount_without_fees->getAmount();

        return GetSubscriptionFormData::run(
            $token,
            $user,
            $listing,
            $amountAndFees,
            null
        );
    }

}
