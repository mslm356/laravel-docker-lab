<?php

namespace App\Console\Commands\Wallets;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;

use App\Models\VirtualBanking\Wallet;
use App\Services\LogService;

use Carbon\Carbon;
use Illuminate\Console\Command;


class WalletRecharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallet:recharge {--amount=} {--wallet_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    public $logService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logServices = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

            $wallet = Wallet::find($this->option('wallet_id'));


            if (!$wallet) {
                dd('wallet not valid');
            }

            $amount = money_parse_by_decimal($this->option('amount'), defaultCurrency());

            $wallet->deposit(
                $amount->getAmount(),
                [
                    'reference' => transaction_ref(),
                    'reason' => TransactionReason::AnbStatement,
                    'details' => [
                        'version' => 1,
                        'source' => 'console_a_wallet_id',
                    ],
                    'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
                ]
            );

            $wallet->refreshBalance();

            dump('done');

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }


}
