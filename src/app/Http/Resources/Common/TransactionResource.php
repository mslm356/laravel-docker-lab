<?php

namespace App\Http\Resources\Common;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\DescriptionManager;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $credit = 0;
        $debit = 0;
        $amount = money($this->amount)->formatByDecimal();

        if ($this->type === Transaction::TYPE_DEPOSIT) {
            $credit = $amount;
        } else {
            $debit = $amount;
        }

        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'credit' => $credit,
            'debit' => $debit,
            'amount_fmt' => $credit > 0 ? number_format($credit,2) :    number_format($debit,2),
            'description' => DescriptionManager::getDescription($this->resource),
            // 'subscription_form_file_link' => $this->whenLoaded(
            //     'subscriptionFormFile',
            //     fn () => route('investor.files.download', ['file' => $this->subscriptionFormFile->id])
            // ),
            // 'receipt_link' => $this->receipt_file_id ? route('investor.files.download', ['file' => $this->receipt_file_id]) : null,
           // 'created_at' => $this->created_at->format('Y-m-d h:i'),
            'created_at' => $this->created_at ? $this->created_at->timezone('Asia/Riyadh')->format('Y-m-d h:i:s A') : null, //$this->created_at->format('Y-m-d h:i'),  ->toDateTimeString()
        ];
    }
}
