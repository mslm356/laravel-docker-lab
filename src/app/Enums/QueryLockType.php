<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class QueryLockType extends Enum
{
    const None =   1;
    const LockForUpdate =   2;
    const SharedLock =   3;
}
