<?php

namespace App\Console\Commands\Notification;

use App\Services\LogService;
use Illuminate\Console\Command;
use App\Jobs\NotificationFirebaseBulk;
use App\Models\Investors\InvestorProfile;
use App\Enums\Notifications\NotificationTypes;

class UnsuitabilityNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify-unsuitability-users';
    public $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send notification to unsuitable users';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $usersCollection = collect();

            InvestorProfile::where('status', 3)
                 ->where('unsuitability_confirmation', false)
                 ->where('annual_income', '!=', null)
                 ->where(function ($query) {
                     $query->where('annual_income', 1)
                         ->orWhereJsonContains('data->investment_xp', 1)
                         ->orWhereJsonContains('data->investment_xp', 2)
                         ->orWhereJsonContains('data->years_of_inv_in_securities', 0);
                 })
             ->lazyById()
             ->each(function ($investor) use ($usersCollection) {
                 $this->count++;
                 dump('user_id ---> ' . $investor->user_id);

                 $usersCollection->push($investor->user);
             });

            $notificationData = $this->notificationData();

            $notification = new NotificationFirebaseBulk(NotificationTypes::ReminderProfile, $notificationData, $usersCollection);
            dispatch($notification)->onQueue('notify');

            dd($this->count); // 677

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine());
        }
    }


    public function notificationData()
    {

        return [
            'title_ar' => 'منصة أصيل', [], 'ar',
            'title_en' => 'منصة أصيل', [], 'ar',
            'content_ar' => 'عميلنا العزيز، ليتاح لك الاستثمار في الفرص القادمة يرجى الموافقة على تقرير الملاءمة الخاص بكم', [], 'ar',
            'content_en' => 'عميلنا العزيز، ليتاح لك الاستثمار في الفرص القادمة يرجى الموافقة على تقرير الملاءمة الخاص بكم', [], 'ar',
            'channel' => 'mobile'
        ];
    }

}
