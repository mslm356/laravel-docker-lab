<?php

return [
    'subject' => 'فريق الدعم',
    'welcome_message' => ':name' .' مستثمرنا الكريم  ' ,
    'message' => 'سعدنا باستلام استفسارك، سيقوم فريقنا بخدمتك في أقرب وقت',
    'end_message' => 'أهلاً بك.',
];
