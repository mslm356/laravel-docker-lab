<div class="flex items-center flex-1">
    <input disabled {{ $isChecked ? 'checked' : '' }} type="checkbox" class="w-5 h-5 transition duration-150 ease-in-out text-primary-600 form-checkbox">
    <label class="block text-base leading-5 text-gray-900 ltr:ml-2 rtl:mr-2">
        {{ $label }}
    </label>
</div>
