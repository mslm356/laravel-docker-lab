<?php

return [
    'auto_update_investor_status_enabled' => env('AML_AUTO_UPDATE_INVESTOR_STATUS_ENABLED', 'fake'),
    'aml_registration_mode' => env('AML_REGISTRATION_MODE', 'production'),
];
