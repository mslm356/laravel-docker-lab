<?php

namespace App\Support\Users\Invitations;

use App\Models\Users\User;
use App\Models\Users\UserInvitation;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class InvitationTokenRepository
{
    protected $hashKey;

    protected $hasher;

    protected $separator = '-';

    public function __construct($hashKey, Hasher $hasher)
    {
        $this->hashKey = $hashKey;
        $this->hasher = $hasher;
    }

    public function create(string $email, Role $role, User $inviter)
    {
        $token = $this->createToken();

        $this->deleteExisting($email, $role, $inviter);

        $invitation =  UserInvitation::create([
            'email' => $email,
            'role_id' => $role->id,
            'token' => $this->hasher->make($token),
            'inviter_id' => $inviter->id
        ]);

        return "{$invitation->id}{$this->separator}{$token}";
    }

    protected function createToken()
    {
        return hash_hmac('sha256', Str::random(40), $this->hashKey);
    }

    public function deleteExisting($email, Role $role, User $inviter = null)
    {
        $where = [
            'email' => $email,
            'role_id' => $role->id,
        ];

        if ($inviter) {
            $where['inviter_id'] = $inviter->id;
        }

        UserInvitation::where($where)->delete();
    }

    public function validateTokenFormat($token)
    {
        $tokenParts = explode($this->separator, $token);

        return count($tokenParts) === 2;
    }

    public function exists($email, $token)
    {
        [$invitationId, $plainToken] = explode($this->separator, $token);

        $invitation = UserInvitation::find($invitationId);

        if ($invitation === null || $invitation->email !== $email || !$this->hasher->check($plainToken, $invitation->token)) {
            return false;
        }

        return $invitation;
    }
}
