<?php

namespace App\Console\Commands\Anb;

use App\Enums\Banking\BankCode;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Models\Anb\AnbStatementEvent;
use App\Models\Anb\AnbStatementTransaction;
use App\Models\Anb\AnbTransfer;
use App\Models\BankAccount;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Models\VirtualBanking\Wallet;
use App\Services\LogService;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Auth;
use App\Support\Anb\Http\AnbHttp;
use App\Support\Anb\Http\HttpException;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Support\Zoho\CustomerPayments;
use Bavix\Wallet\Models\Transaction;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Cknow\Money\Money;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CheckPullStmt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:pull-stmt {--from=} {--to=} {--type=} {--value=}';
    protected $logService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Anb pull stmt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $accountNumber = config('services.anb.account_number');

            $offset = null;

            do {

                $msg = AnbApiUtil::statement(
                    $accountNumber,
                    $this->option('from'),
                    $this->option('to'), 20, $offset
                );

                $offset = $msg['offset'];

                if ($this->showTransaction($msg)) {
                    break;
                }

            } while ($msg['completed'] != true);

        } catch (\Exception $e) {
            Log::info('check-pull-Statement-Anb-test', [$e->getMessage(), $e->getFile(), $e->getLine()]);
        }
    }

    public function showTransaction($msg)
    {

        $transactions = $msg['statement']['transactions'];

        $types = [
            'narr2' => 'byNarr2',
            'narr1' => 'byNarr1',
            'sd' => 'bySd',
            'amount' => 'byAmount',
        ];

        foreach ($transactions as $index => $anbTransaction) {

           $methodName = $types[$this->option('type')];
           $this->$methodName($anbTransaction);
        }
    }

    public function bySd($anbTransaction)
    {

        $bankRef = $anbTransaction['refNum'] ?? null;

        if ($bankRef == $this->option('value')) {
            dump($anbTransaction);
            return true;
        }

        return false;
    }

    public function byNarr2($anbTransaction)
    {

        $narr2 = $anbTransaction['narrative']['narr2'] ?? null;

        if ($narr2 == $this->option('value')) {
            dump($anbTransaction);
           return true;
        }

        return false;

    }

    public function byNarr1($anbTransaction)
    {

        $narr1 = $anbTransaction['narrative']['narr1'] ?? null;

        if ($narr1 == $this->option('value')) {
            dump($anbTransaction);
            return true;
        }

        return false;
    }

    public function byAmount($anbTransaction)
    {

        $amount = $anbTransaction['amount']['amount'] ?? null;
        $bankRef = $anbTransaction['refNum'] ?? null;
        $narr1 = $anbTransaction['narrative']['narr1'] ?? null;
        $narr2 = $anbTransaction['narrative']['narr2'] ?? null;
        $narr3 = $anbTransaction['narrative']['narr3'] ?? null;

        if ($amount == $this->option('value')) {

            dump($anbTransaction);

            $transaction = DB::table('transactions')->where('external_reference', $bankRef)->first();

            $exist = $transaction ? 'yes':'no';
            $user_id = $transaction ? $transaction->payable_id  : '';

            dump($amount, $bankRef, $narr1, $narr2, $narr3, 'transaction exist=> ' . $exist .  'user id => ' . $user_id);

            $handleNarr2 = $this->handleNarr2($narr2);

            if ($handleNarr2 && AnbVA::isVA($handleNarr2) && ($virtualAccount = VirtualAccount::where('identifier', AnbVA::toIban($handleNarr2))->first()))
            {
                $wallet =  Wallet::find($virtualAccount->wallet_id);
                dump('wallet id =>' . $wallet->holder_id);
            }

            dump('--------------------------------------------------------------');

            return true;
        }
        return false;
    }

    public function handleNarr2 ($narr2)
    {
        return substr($narr2, 0, 15);
    }
}
