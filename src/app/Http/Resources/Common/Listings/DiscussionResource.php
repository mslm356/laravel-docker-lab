<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Listings\DiscussionStatus;
use App\Http\Resources\Common\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscussionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'status' => $this->status,
            'status_text' => DiscussionStatus::getDescription($this->status),
            'user' => new UserResource($this->user),
            'replies' => DiscussionReplyResource::collection($this->whenLoaded('replies'))
        ];
    }
}
