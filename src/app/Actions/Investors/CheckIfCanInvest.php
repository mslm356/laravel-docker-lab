<?php

namespace App\Actions\Investors;

use App\Enums\Banking\WalletType;
use App\Exceptions\NoBalanceException;
use App\Exceptions\NoInvestorWalletFound;
use App\Models\Listings\ListingInvestor;
use App\Services\MemberShip\SpecialCanInvest;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Investors\InvestorLevel;
use App\Settings\InvestingSettings;

class CheckIfCanInvest
{
    use AsAction;

    /**
     * Check if the investor whether the investor can invest
     *
     * @param \App\Models\Listings\Listing $listing
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $currentInvest
     * @param int $toBeInvested
     *
     * @return true|int
     * @throws \App\Exceptions\NoBalanceException
     * @throws \App\Exceptions\NoInvestorWalletFound
     */
    public function handle($listing, $user, $currentInvest, $toBeInvestedShares,$calculatedData=null)
    {
        $currentUserInvestments=ListingInvestor::where('listing_id', $listing->id)->where('investor_id', $user->id)->first();
        $totalInvestorInvShares = ($currentUserInvestments)?$currentUserInvestments->invested_shares:0;
//        $currentUserInvestments = $currentInvest->where('investor_id', $user->id);
//        $totalInvestorInvShares = $currentUserInvestments->sum('invested_shares');
        $totalInvestorInvShares += $toBeInvestedShares;

        // Investor try to invest with amount less than
        // the min invest amount for the property
        if ($toBeInvestedShares < $listing->min_inv_share) {
            return 2;
        }

        // Investor try to invest with amount greater than
        // the max invest amount for the property
        if ($totalInvestorInvShares > $listing->max_inv_share) {
            return 3;
        }

        $calculatedData = ($calculatedData)?$calculatedData:CalculateInvestingAmountAndFees::run($listing, $toBeInvestedShares);

        $toBeInvestedAmountWithFees = $calculatedData['amount'];

        $user_level=request()->user_can_access_data->upgrade_request_level;
//        $user_level= $user->investor->level;


        if (in_array($user_level, [InvestorLevel::Beginner, InvestorLevel::Company])) {
//            $currentInvestedAmount = $currentUserInvestments->reduce(fn ($acc, $investment) => $acc->add($investment->invested_amount), money(0));
            $currentInvestedAmount=($currentUserInvestments)?$currentUserInvestments->invested_amount:money(0);
            $beginnerLimit = money(config('accounting.beginner_limit_per_listing'));
            $toBeTotalInvestedAmount = $currentInvestedAmount->add($toBeInvestedAmountWithFees);

            if ($toBeTotalInvestedAmount->greaterThan($beginnerLimit)) {
                return 4;
            }
        }

//        $currentInvestedShares = $currentInvest->sum('invested_shares');
        $currentInvestedShares = $listing->total_invested_shares;

        $totalInvestedShares = $currentInvestedShares + $toBeInvestedShares;

        $specialCanInvest = new SpecialCanInvest();

        $responseOfCheckMembershipLimit = $specialCanInvest->checkMembershipLimit($listing, $totalInvestedShares, $currentInvestedShares);

        if (!$responseOfCheckMembershipLimit['status']) {
            return $responseOfCheckMembershipLimit['message_num'];
        }

        // Property total invest will exceed its target
        if ($totalInvestedShares > $listing->total_shares) {
            return 1;
        }

        $wallet = $user->getWallet(WalletType::Investor);

        if (!$wallet) {
            throw new NoInvestorWalletFound();
        }

        $balance = money($wallet->balance);

        if ($balance->lessThan($calculatedData['total'])) {
            return 5;
//            throw new NoBalanceException();
        }

        return true;
    }
}
