<?php

namespace App\Models\Listings;

use App\Casts\LocalizedAttribute;
use App\Casts\Money\MoneyCast;
use App\Enums\Listings\AttachmentType;
use App\Models\AppModel;
use App\Models\City;
use App\Models\PropertyType;
use App\Models\AuthorizedEntity;
use App\Models\BankAccount;
use App\Models\Listings\ListingDiscussion;
use App\Models\Payouts\Payout;
use App\Models\Revisions\Revisionable;
use App\Models\Traits\HasFiles;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Models\Vote;
use App\Support\QueryScoper\HasScopes;
use Bavix\Wallet\Traits\HasWalletFloat;
use Bavix\Wallet\Traits\HasWallets;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Listing extends AppModel
{
    use HasFiles, Revisionable, HasScopes, HasWalletFloat, HasWallets;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'deadline' => 'datetime',
        'start_at' => 'datetime',
        'is_visible' => 'boolean',
        'is_completed' => 'boolean',
        'is_closed' => 'boolean',
        'title' => LocalizedAttribute::class,
        'target' => MoneyCast::class . ':currency',
        'valuation_unit' => MoneyCast::class . ':currency',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'share_price'
    ];

    /**
     * Get all the listing details
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details()
    {
        return $this->hasMany(ListingDetail::class, 'listing_id', 'id');
    }

    /**
     * Get all the listing subscription tokens
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function investorSubscriptionTokens()
    {
        return $this->hasMany(InvestorSubscriptionToken::class, 'listing_id', 'id');
    }

    /**
     * Get listing discussions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discussions()
    {
        return $this->hasMany(ListingDiscussion::class, 'listing_id', 'id');
    }

    /**
     * Get listing reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany(ListingReport::class, 'listing_id', 'id');
    }

    /**
     * Get listing files
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->files();
    }

    /**
     * Get listing files
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function dueDiligenceFiles()
    {
        return $this->attachments()
            ->where('extra_info->type', AttachmentType::DueDiligence);
    }

    /**
     * Get listing's authorized entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function authoirzedEntity()
    {
        return $this->belongsTo(AuthorizedEntity::class, 'authorized_entity_id', 'id');
    }

    /**
     * Get root listing discussions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rootDiscussions()
    {
        return $this->discussions()->where('parent_id', null);
    }

    /**
     * Get the share price (SAR/price)
     *
     * @return float
     */
    public function getSharePriceAttribute()
    {
        return $this->target->divide($this->total_shares);
    }

    /**
     * Format the given number
     *
     * @param float $value
     * @return string
     */
    public function formatNumberWithZeroDecimal($value)
    {
        return number_format($value);
    }

    /**
     * Get the of the of listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(PropertyType::class, 'type_id', 'id');
    }

    /**
     * Get all the listing images
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ListingImage::class, 'listing_id', 'id');
    }

    /**
     * Get the cover image
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function coverImage()
    {
        return $this->hasOne(ListingImage::class, 'listing_id', 'id')
            ->orderBy('is_cover', 'desc');
    }

    /**
     * Get listing valuations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function valuations()
    {
        return $this->hasMany(ListingValuation::class, 'listing_id', 'id');
    }

    /**
     * Get listing latest valuation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function latestValuation()
    {
        return $this->hasOne(ListingValuation::class, 'listing_id', 'id')->latest('date');
    }

    /**
     * Get the city of the listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * Get bank account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id', 'id');
    }

    /**
     * Get authorized entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorizedEntity()
    {
        return $this->belongsTo(AuthorizedEntity::class, 'authorized_entity_id', 'id');
    }

    /**
     * Investors invest in listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function investors()
    {
        return $this->belongsToMany(
            User::class,
            'listing_investor',
            'listing_id',
            'investor_id'
        )
            ->withPivot(['invested_amount', 'invested_shares'])
            ->withTimestamps();
    }

    /**
     * Get all payouts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payouts()
    {
        return $this->hasMany(Payout::class, 'listing_id', 'id');
    }

    /**
     * Determines if this listing can be invested in
     *
     * @return boolean
     */
    public function getCanInvestAttribute()
    {
        $isOpen = (!$this->is_closed && ($this->deadline->diffInSeconds(null, false) < 0));

        $start_at = Carbon::parse($this->start_at)->timezone('Asia/Riyadh');

        $is_started = $this->start_at ? $start_at->isBefore(now('Asia/Riyadh')) : true;

        return $isOpen && $is_started;
    }

    /**
     * Determines if the investors started investing
     *
     * @return boolean
     */
    public function investingStarted()
    {
        // greater than 1 as PayoutsCollector account is always present by default
        return $this->investors()->count() > 1;
    }

    public function getModelStateForRevision()
    {
        $this->loadMissing([
            'details',
            'city',
            'type',
            'images',
            'attachments',
            'bankAccount',
            'authorizedEntity',
        ]);

        return [
            'originals' => $this->getOriginal(),
            'attributes' => $this->getAttributes(),
            'with_relations' => $this,
            'version' => '2'
        ];
    }

    public function getStartTimeAttribute()
    {
        return Carbon::parse($this->start_at)->timezone('Asia/Riyadh')->format("Y-m-d h:i:s A");
    }

    public function getEarlyStartTimeAttribute()
    {
        return Carbon::parse($this->early_investment_date)->timezone('Asia/Riyadh')->format("Y-m-d h:i:s A");
    }

    /**
     * Determines if this listing start soon
     *
     * @return boolean
     */
    public function getStartSoonAttribute()
    {
        if (is_null($this->start_at)) {
            return false;
        }

        $start_at = Carbon::parse($this->start_at)->timezone('Asia/Riyadh');

        if (!$start_at || $start_at->isBefore(now('Asia/Riyadh'))) {
            return false;

        } elseif ($start_at->greaterThanOrEqualTo(now('Asia/Riyadh'))) { //&& !$this->start_at->greaterThan(now()->addDays(2))
            return true;
        }

        return false;
    }

    public function getPercentageAttribute()
    {
        return $this->invest_percent;
    }

    public function getFundPercentAttribute()
    {
        if ($this->is_completed) {
            return 100;
        }

        $start_at = Carbon::parse($this->start_at);
        return $start_at->isAfter(now()) ? 0 : $this->invest_percent;
    }

    public function getListingTagsAttribute()
    {

        $start_at = Carbon::parse($this->start_at);

        if (!$this->is_completed) {
            return [
                'is_new' => $this->created_at->isBefore(now()->addDays(20)),
                'is_closing_soon' => ($daysRemaining = now()->diffInDays($this->deadline, false)) <= 3 && $daysRemaining > 0,
                'is_start_soon' => $this->start_soon,
                'is_trending' => false,
                'is_realtime' => $start_at->isBefore(now()),
            ];

        } else {
            return [
                'is_new' => false,
                'is_start_soon' => false,
                'is_closing_soon' => false,
                'is_trending' => false,
                'is_realtime' => false,
            ];
        }
    }

    public function votes()
    {
        return $this->hasMany(Vote::class, 'fund_id', 'id');
    }

    public function getTotalInvestedSharesAttribute()
    {
        return $this->total_shares - $this->investment_shares;
    }


    public function getInvestPercentAttribute()
    {
        $precent=($this->total_invested_shares / $this->total_shares) * 100;

        $precent =(int)$precent + $this->changed_investment_precentage;

        if ($precent > 100)
        return 100;

        if ($precent <=0)
            return 0;

        return $precent;
    }

    function getRealInvestPercentAttribute()
    {
        $real_precent=( $this->total_invested_shares / $this->total_shares ) * 100;
        return $real_precent;
    }


    public function getTotalInvestedAmountAttribute()
    {
        return DB::table('listing_investor')
            ->where('listing_id', $this->id)
            ->sum('invested_amount') / 100;
    }
}
