<?php

namespace App\Console\Commands\Anb;

use App\Enums\Banking\BankCode;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Models\Anb\AnbStatementEvent;
use App\Models\Anb\AnbStatementTransaction;
use App\Models\Anb\AnbTransfer;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Models\VirtualBanking\Wallet;
use App\Services\LogService;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Auth;
use App\Support\Anb\Enums\AnbPaymentStatus;
use App\Support\Anb\Http\HttpException;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Support\Zoho\CustomerPayments;
use Bavix\Wallet\Models\Transaction;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Cknow\Money\Money;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Mail\Investors\depositTransaction;



class RunPullStmt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run-pull:stmt';
    protected $zohoCustomerPayments;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Anb pull stmt';
    protected $cachedCollectorWallet = null;
    protected $cachedWallets = [];
    public $logServices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->zohoCustomerPayments = new CustomerPayments();
        $this->logServices = new LogService();
    }

    public function handle()
    {
        try {

            $accountNumber = config('services.anb.account_number');

            $event = AnbStatementEvent::latest('ended_at')
                ->latest('statement_date')
                ->where([
                    'bank_code' => BankCode::Anb,
                    'account_number' => $accountNumber
                ])
                ->first();

            if ($event === null) {
                $this->pullStatementForDate($accountNumber, now(), null);
            } else if ($event->statement_date->isBefore(now()->startOfDay()) || $event->statement_date->isSameDay(now()->startOfDay())) {
                $latestStatementDate = $event->statement_date->copy();
                $offset = $event->offset;
                do {
                    $this->pullStatementForDate($accountNumber, $latestStatementDate->copy(), $offset);
                    $latestStatementDate->addDay();
                    $offset = null;
                } while ($latestStatementDate->isBefore(now()->startOfDay()) || $latestStatementDate->isSameDay(now()->startOfDay()));
            }

        } catch (\Exception $e) {
            dd('pull-Statement-Anb-', $e->getMessage(), $e->getFile(), $e->getLine());
        }

    }

    /**
     * Pull statement for the given date
     *
     * @param string $accountNumber
     * @param \Carbon\Carbon $date
     * @param string|null $offset
     * @return void
     */
    public function pullStatementForDate($accountNumber, $date, $offset)
    {
        $event = new AnbStatementEvent([
            'id' => (string)Str::uuid(),
            'statement_date' => $date->copy()->startOfDay(),
            'account_number' => $accountNumber,
            'bank_code' => BankCode::Anb,
            'started_at' => now()
        ]);

        $msg = AnbApiUtil::statement(
            $accountNumber,
            $from = $date->copy()->timezone('Asia/Riyadh')->toDateString(),
            $from,
            null,
            $offset
        );

        if ($msg === null || $msg['statement'] == null) {
            $event->ended_at = now();
            $event->offset = $offset;
            $event->save();
            return;
        }

        $event->ended_at = now();
        $event->offset = $msg['offset'];

        $transactions = $msg['statement']['transactions'];
        $isCompleted = $msg['completed'];

        DB::transaction(function () use ($transactions, $event) {
            $toBeUpsertedTransactions = [];

            // We cache wallets during each DB::transaction so that if multiple transactions happen to
            // the same Virtual Account, the wallet don't get locked because of the "lockForUpdate"
            $this->resetWalletsCache();

            foreach ($transactions as $anbTransaction) {

                $uniqueId = isset($anbTransaction['uniqueId']) ? $anbTransaction['uniqueId'] : null;
                sleep(1);
                $narr2 = $anbTransaction['narrative']['narr2'] ?? null;
                $narr3 = $anbTransaction['narrative']['narr3'] ?? null;
                $narr3Reference = $this->getNarr3Reference($narr3);
                $bankRef = $anbTransaction['refNum'] ?? null;
                $amount = $anbTransaction['amount']['amount'];
                $currency = $anbTransaction['amount']['currencyCode'];
                $money = Money::parseByDecimal($amount, $currency);

                $transaction = $this->mapTransactionForLaterUpserting($event, $anbTransaction, $money);
                $toBeUpsertedTransactions[] = $this->prepareTransactionForUpserting($transaction);

                $valueDate = Carbon::parse($anbTransaction['valueDate']);

                $valueDateY = $valueDate->format('Y');
                $valueDateM = $valueDate->format('m');
                $valueDateD = $valueDate->format('d');

                $transactionExists = Transaction::where([
                    'external_reference' => $bankRef
                ])
                    ->whereDay('created_at', $valueDateD)
                    ->whereMonth('created_at', $valueDateM)
                    ->whereYear('created_at', $valueDateY)

                    ->whereIn('reason', [
                        TransactionReason::AnbStatement,
                        TransactionReason::RefundAfterAnbTransferFailure
                    ])
                    ->limit(1)
                    ->exists();

//                $responseOfCheckUrwayTransactions = $this->checkUrwayTransactions($narr2, $bankRef);

                if ($transactionExists) {
                    continue;
                }

                $virtualAccount = null;
                $wallet = null;
                $type = null;
                $transfer = null;

                $handleNarr2 = $this->handleNarr2($narr2);

                if (
                    $handleNarr2 && AnbVA::isVA($handleNarr2) &&
                    ($virtualAccount = VirtualAccount::where('identifier', AnbVA::toIban($handleNarr2))->first())
                ) {
                    $type = 'deposit';

                    $wallet = $this->findOrCacheWallet($virtualAccount->wallet_id);
                } else if (
//                    $narr3Reference &&
//                    $transfer = AnbTransfer::where('external_trans_reference', $narr3Reference)->first()
                $transfer = $this->getTransferInTransactionsRefunded($anbTransaction)
                ) {
                    if ($transfer->data['is_refunded'] ?? null) {
                        continue;
                    } else {
                        $type = 'reversal';
                        $wallet = $this->findOrCacheWallet($transfer->transaction->wallet_id);
                    }
                }

                if ($wallet === null) {
                    $wallet = $this->findOrCacheCollectorWallet();
                }

                $checkUserTransactions = $this->checkUserTransactions($wallet, $bankRef);

                if ($checkUserTransactions) {
                    continue;
                }

                if ($money->isPositive()) {
                    if ($type === null || $type === 'deposit') {
                        $depositTransaction = $wallet->deposit(
                            $money->absolute()->getAmount(),
                            [
                                'vaccount_identifier' => $virtualAccount->identifier ?? null,
                                'vaccount' => optional($virtualAccount)->toArray(),
                                'reference' => transaction_ref(),
                                'unique_id' => $uniqueId,
                                'reason' => TransactionReason::AnbStatement,
                                'details' => [
                                    'version' => 1,
                                    'data' => $anbTransaction,
                                    'source' => 'pull_statement_run_cmd'
                                ],
                                'external_reference' => $bankRef,
                                'value_date' => $transaction['value_date']->toDateTimeString(),
                            ]
                        );

                        $depositTransaction->unique_id = $uniqueId;
                        $depositTransaction->save();

                        $wallet->refreshBalance();

//                        $this->zohoDeposit($wallet, $amount);

                    } else {
                        $refundTransaction = $wallet->deposit(
                            $money->absolute()->getAmount(),
                            [
                                'reason' => TransactionReason::RefundAfterAnbTransferFailure,
//                                'value_date' => now()->toDateTimeString(),
                                'reference' => transaction_ref(),
                                'unique_id' => $uniqueId,
                                'transaction_id' => $transfer->transaction->id,
                                'anb_transfer_id' => $transfer->id,
                                'transaction_reference' => $transfer->transaction->reference,
                                'from_bank_code' => $transfer->to_bank,
                                'from_iban' => $transfer->to_account,
                                'external_reference' => $bankRef,
                                'value_date' => $transaction['value_date']->toDateTimeString(),
                                'details' => [
                                    'version' => 1,
                                    'data' => $anbTransaction,
                                    'source' => 'pull_statement'
                                ],
                            ]
                        );

                        $refundTransaction->unique_id = $uniqueId;
                        $refundTransaction->save();

                        $wallet->refreshBalance();

                        $transfer->data = array_merge($transfer->data, [
                            'is_refunded' => true,
                            'refund_transaction_id' => $refundTransaction->id
                        ]);

                        $transfer->save();
                    }
                }
            }

            $event->save();

            AnbStatementTransaction::upsert(
                $toBeUpsertedTransactions,
                ['reference'],
                [
                    'event_id',
                    'from_account',
                    'reference',
                    'amount',
                    'currency',
                    'value_date',
                    'post_date',
                    'narrative',
                    'extra_data'
                ]
            );
        });

        // Check if there are more recordings or the end date is not equal
        // to today's date so we want to fetch all the records up to date
        if (!$isCompleted) {
            $this->pullStatementForDate($accountNumber, $date->copy(), $event->offset);
        }
    }

    public function mapTransactionForLaterUpserting($event, $anbTransaction, $money)
    {
        $transactionDate = !isset($anbTransaction['valueDate']) ? null : CarbonImmutable::createFromFormat(
            'Y-m-d',
            $anbTransaction['valueDate'],
            'Asia/Riyadh'
        )
            ->setTime(0, 0)
            ->timezone('UTC');

        return [
            'event_id' => $event->id,
            'from_account' => $anbTransaction['srcAcctNum'] ?? null,
            'reference' => $anbTransaction['refNum'] ?? null,
            'amount' => $money->getAmount(),
            'currency' => $money->getCurrency()->getCode(),
            'value_date' => $transactionDate,
            'post_date' => !isset($anbTransaction['postDate']) || !isset($anbTransaction['postingTime']) ?
                null : CarbonImmutable::createFromFormat(
                    'Y-m-d H:i:s',
                    "{$anbTransaction['postDate']} {$anbTransaction['postingTime']}",
                    'Asia/Riyadh'
                )
                    ->setTime(0, 0)
                    ->timezone('UTC'),
            'narrative' => $anbTransaction['narrative'] ?? [],
            'extra_data' => Arr::except(
                $anbTransaction,
                [
                    'srcAcctNum', 'refNum', 'amount', 'valueDate', 'postDate', 'postingTime', 'narrative'
                ]
            )
        ];
    }

    public function prepareTransactionForUpserting($unpreparedTransaction)
    {
        $transaction = $unpreparedTransaction;

        $transaction['post_date'] = $transaction['post_date']->toDateTimeString();
        $transaction['narrative'] = json_encode($transaction['narrative']);
        $transaction['extra_data'] = json_encode($transaction['extra_data']);
        $transaction['value_date'] = $transaction['value_date']->toDateString();

        return $transaction;
    }

    public function resetWalletsCache()
    {
        $this->cachedCollectorWallet = null;
        $this->cachedWallets = [];
    }

    public function findOrCacheWallet($walletId)
    {
        if (($this->cachedWallets[$walletId] ?? null) === null) {
            $this->cachedWallets[$walletId] = Wallet::find($walletId);
        }

        return $this->cachedWallets[$walletId];
    }

    public function findOrCacheCollectorWallet()
    {
        if (!$this->cachedCollectorWallet) {
            $collector = User::role(Role::AnbStatementCollector)->first();

            $this->cachedCollectorWallet = $collector->getWallet(WalletType::AnbStatementCollecter);
        }

        return $this->cachedCollectorWallet;
    }

    public function getNarr3Reference($narr3)
    {

        if (!$narr3) {
            return null;
        }

        $narr3Data = explode('/', $narr3);

        return isset($narr3Data[0]) ? $narr3Data[0] : null;
    }

    public function zohoDeposit($wallet, $amount)
    {

        try {

            $user = User::find($wallet->holder_id);
            $this->zohoCustomerPayments->createCustomerPayment($user, $amount);

        } catch (\Exception $e) {
            Log::error('ZOHO-CUSTOMER-PAYMENT-PULL-STMT', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }

    public function checkUrwayTransactions($narr2, $bankRef)
    {

        $rrn = substr($narr2, 0, 12);

        $transaction = Transaction::where('rrn', $rrn)
            ->where('reason', TransactionReason::UrwayReason)
            ->first();

        if (!$transaction) {
            return null;
        }

        $transaction->external_reference = $bankRef;
        $transaction->save();

        return $transaction;
    }

    public function handleNarr2 ($narr2)
    {
        return substr($narr2, 0, 15);
    }

    public function checkUserTransactions ($wallet, $bankRef) {

        $user_id = $wallet->holder_id;

        $userTransactions = Transaction::where('external_reference', $bankRef)
            ->where('payable_id', $user_id)
            ->where('payable_type', User::class)
            ->first();

        if ($userTransactions) {
            $this->logServices
                ->log('transaction exists in user transactions please check --------------------------------------------------------------------', null, [$bankRef]);

            return true;
        }

        return false;
    }

    public function getTransferInTransactionsRefunded ($anbTransaction) {

        $channel = isset($anbTransaction['channel']) ? $anbTransaction['channel'] : null;

        if ($channel == 'SARIE') {

            $narr3Refrence = null;

            $narr3 = $anbTransaction['narrative']['narr3'] ?? null;

            $narr3Data = explode('/', $narr3);

            if (isset($narr3Data[0])) {
                $narr3Refrence = $narr3Data[0];
            }

            $transfer = AnbTransfer::where('id', $narr3Refrence)
                ->whereNotNull('processing_status')
                ->whereNotIn('processing_status', [ AnbPaymentStatus::FAILED, AnbPaymentStatus::CANCELED])
                ->first();

        } elseif ($channel == 'IPS') {

            $narr1 = $anbTransaction['narrative']['narr1'] ?? null;

            if (strlen($narr1) > 10) {
                $narr1 = $anbTransaction['narrative']['narr2'] ?? null;
            }

            $transfer = AnbTransfer::where('external_trans_reference', $narr1)
                ->whereNotNull('processing_status')
                ->whereNotIn('processing_status', [ AnbPaymentStatus::FAILED, AnbPaymentStatus::CANCELED])
                ->first();
        }else {

            $narr2 = $anbTransaction['narrative']['narr2'] ?? null;
            $transfer = AnbTransfer::where('external_trans_reference', $narr2)
                ->whereNotNull('processing_status')
                ->whereNotIn('processing_status', [ AnbPaymentStatus::FAILED, AnbPaymentStatus::CANCELED])
                ->first();
        }

        if (!$transfer) {
            return null;
        }

        $this->logServices->log('ANB-TRANSFER-REFUND', null, ['PULL-STMT-REFUND', $transfer]);

        return $transfer;
    }
}
