<?php

namespace App\Http\Controllers\Investors\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\MyUnitResource;
use App\Support\QueryScoper\Scopes\Investor\MyInvestments\OrderByDateScope;

class GetMyUnits extends Controller
{
    /**
     * Invest in the given listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = $request->user();

        $order=($request->has('sort_by') && $request->sort_by == 'oldest')?'ASC':'DESC';

        $listing = $user->investments()
            ->where('payout_done', 0)
            ->wherePivot('invested_shares', '>', 0)
            ->with([
                'latestValuation',
                'votes',
                'payouts.investors' => function ($query) use ($request, $user) {
                    $query->where('users.id', $user->id);
                }
            ])
            ->orderBy('pivot_id', $order)
            ->toScopes(OrderByDateScope::class)
            ->simplePaginate(10);

        return MyUnitResource::collection($listing);
    }
}
