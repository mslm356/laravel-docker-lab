<?php

use \App\Http\Controllers\MobileApp\Investors\Discussions\DiscussionsController;


Route::middleware(['auth:api', 'jwt_check:api', 'role:' .\App\Enums\Role::Investor])->group(function () {
    Route::post('/listings/{listing_id}/discussions/create', [DiscussionsController::class, 'store']);
    Route::get('/listings/{listing_id}/discussions', [DiscussionsController::class, 'index']);
});

