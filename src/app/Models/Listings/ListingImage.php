<?php

namespace App\Models\Listings;

use App\Models\AppModel;
use Illuminate\Support\Facades\Storage;

class ListingImage extends AppModel
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url'];

    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }

    public function getUrlAttribute()
    {

        $path = Storage::disk(config('filesystems.public'))->url($this->path);

        if (config('filesystems.default') === 'public') {
            return asset($path);
        } else {
            return $path;
        }
    }
}
