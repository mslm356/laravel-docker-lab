<?php

namespace App\Http\Controllers\Investors\Profile\ProfessionalUpgrade;

use App\Actions\Investors\ProfessionalUpgrade\CheckIfCanSubmitNewRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CanSubmitNewRequest extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');

        return response()->json([
            'data' => [
                'result' => CheckIfCanSubmitNewRequest::run($user)
            ]
        ]);
    }
}
