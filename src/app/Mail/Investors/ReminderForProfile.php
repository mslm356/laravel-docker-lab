<?php

namespace App\Mail\Investors;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReminderForProfile extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/sign-up/profile';
        $name = $this->user ? $this->user->full_name : '---';

        return $this->view('emails.investors.reminder_for_complete_profile', compact('profileUrl', 'name'))
            ->subject(__('emails/investors/welcome.subject'));
    }
}
