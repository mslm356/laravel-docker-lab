<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\DBColumInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class CashOutPeriod extends Enum implements LocalizedEnum
{
    const ShortTerm =   1;
    const MediumTerm =   2;
    const LongTerm =   3;



}
