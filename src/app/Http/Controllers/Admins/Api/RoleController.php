<?php

namespace App\Http\Controllers\Admins\Api;

use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Enums\Role as EnumsRole;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\AdminResource;
use Spatie\Permission\Models\Permission;
use App\Http\Resources\Admins\RoleResource;
use App\Http\Resources\Admins\PermissionsResource;

class RoleController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdmins(Request $request)
    {
        $admins = User::leftJoin('model_has_roles', function ($join) {
            $join->on("model_has_roles.model_id", "=", "users.id")
                ->where("model_has_roles.model_type", "=", "App\Models\Users\User");
        })->where('role_id', "<>", 1)
        ->whereHas('roles', function ($query) {
            $query->where('name', '<>', EnumsRole::AuthorizedEntityAdmin);
        })
        ->get();

        return AdminResource::collection($admins);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function assignRole(Request $request, $id)
    {
        $user = User::find($id);
        $user->syncRoles($request->role_id);

        return response()->json([
            'id' => $user->id
        ], 201);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::select('id', 'name')->whereNotIn('name', [EnumsRole::Admin, EnumsRole::Investor, EnumsRole::AnbStatementCollector, EnumsRole::PayoutsCollector, EnumsRole::AuthorizedEntityAdmin])->paginate(100);

        return $roles;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function getPermissions(Request $request)
    {
        $permissions = Permission::getPermissions();

        //return PermissionsResource::collection($permissions);
        $allPermissions = [];
        foreach($permissions as $permission) {
            $allPermissions[$permission->group]['name'] = __('permissions.group-'.$permission->group);
            $allPermissions[$permission->group]['roles'][] = [
                    'value' => $permission->name,
                    'label' => __('permissions.'.$permission->name),
                    'selected' => $permission->selected??0,
            ];
        }
        unset($allPermissions['other']);

        return response()->json([
            'data' => array_values($allPermissions)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $requestData['guard_name'] = 'web';
        $permissions = [];
        if(isset($requestData['permissions'])) {
            $permissions = $requestData['permissions'];
            unset($requestData['permissions']);
        }
        $permissions[] = 'access-admin-portal';
        $role = Role::create($requestData);
        $role->givePermissionTo(Permission::whereIn('name', array_values($permissions))->pluck('id', 'id')->toArray());
        return response()->json([
            'id' => $role->id
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($role)
    {
        $role = Role::with('permissions')->find($role);

        return new RoleResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role)
    {
        $requestData = $request->all();

        $role = Role::find($role);
        $permissions = [];
        if(isset($requestData['permissions'])) {
            $permissions = $requestData['permissions'];
            unset($requestData['permissions']);
        }
        $permissions[] = 'access-admin-portal';
        $role->update($requestData);
        // $role->givePermissionTo([]);
        $role->syncPermissions(array_values($permissions));

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($role)
    {
        $admins = User::join('model_has_roles', function ($join) {
            $join->on("model_has_roles.model_id", "=", "users.id")
                ->where("model_has_roles.model_type", "=", "App\Models\Users\User");
        })->where('role_id', $role)->get();

        if(count($admins)) {
            return response()->json(['message' => 'Can\'t delete the role that assigned to users'], 400);
        }

        Role::find($role)->delete();

        return response()->json();
    }
}
