<?php

namespace App\Console\Commands\Kyc;

use App\Models\Investors\InvestorProfile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use function Google\Auth\Cache\get;

class UpdateCountry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:country-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update country id ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $nationalities = DB::table('national_identities')
            ->select('nationality')
            ->groupBy('nationality')
            ->get();

        foreach ($nationalities as $nationality) {

            $nationality = $nationality->nationality;

            $country = DB::table('countries')->where(function ($q) use ($nationality) {

                $q->where('name_en', 'like', '%' . $nationality . '%')
                    ->orWhere('name_ar', 'like', '%' . $nationality . '%');

            })->select('id')
                ->first();

            if ($country) {
                DB::table('national_identities')->where('nationality', $nationality)
                    ->update(['country_id'=> $country->id]);
            }
        }

    }
}
