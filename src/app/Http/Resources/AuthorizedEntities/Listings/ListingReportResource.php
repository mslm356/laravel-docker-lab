<?php

namespace App\Http\Resources\AuthorizedEntities\Listings;

use App\Http\Resources\AuthorizedEntities\FileResource;
use App\Http\Resources\Common\Listings\ListingReportResource as CommonListingReportResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingReportResource($this->resource))->toArray($request),
            [
                'files' => FileResource::collection($this->files)
            ]
        );
    }
}
