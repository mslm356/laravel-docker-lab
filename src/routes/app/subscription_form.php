<?php

use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\SubscriptionForm\RequestsController;


Route::middleware(['auth:api', 'jwt_check:api', 'role:' .Role::Investor])->group(function () {

    //     Subscription Form Requests
    Route::post('/subscription-form-request-store', [ RequestsController::class, 'store']);

});
