<?php

namespace App\Jobs\Zoho;

use App\Support\Zoho\ManualJournal;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ManualJournalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $zohoManualJournal;
    public $data;
    public $transaction;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $transaction = null)
    {
        $this->zohoManualJournal = new ManualJournal();
        $this->data = $data;
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $journal_id = $this->zohoManualJournal->store($this->data);

        if ($journal_id && $this->transaction) {
            $this->transaction->zoho_id = $journal_id;
            $this->transaction->save();
        }
    }
}
