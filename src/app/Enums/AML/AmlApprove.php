<?php

namespace App\Enums\AML;

use BenSampo\Enum\Enum;

final class AmlApprove extends Enum
{
    const Reject = 0;
    const Approve = 1;
    const Pending = 3;
}
