<?php

namespace App\Services\Sdad\Contracts;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

interface SingleInvoicesRepositoryContract
{
    /**
     * @param User $user
     * @param array $request_data
     * @return mixed
     */
    public function create(User $user, array $request_data): ? Model;

    /**
     * @param $invoice_id
     * @return Model|null
     */
    public function show($invoice_id): ? Model;

    /**
     * @param $invoice_id
     * @param array $request_data
     * @return Model|null
     */
    public function update($invoice_id, array $request_data): ? Model;
}
