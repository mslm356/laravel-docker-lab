<?php

namespace App\Http\Controllers\Investors\SubscriptionForm;

use App\Services\LogService;
use App\Services\SubscriptionForm\SendRequest;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestsController extends Controller
{

    use SendMessage, NotificationServices;

    public $logService;
    public $sendRequest;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->sendRequest = new SendRequest();
    }


    public function store(Request $request)
    {

        try {

            $user = getAuthUser('web');

            $hasInvestments = DB::table("listing_investor")->where('investor_id',$user->id)->count();
            if (!$hasInvestments)
                return apiResponse(400, __("mobile_app/message.has_investment"));

            $response = $this->sendRequest->handle($request, $user);

            if (isset($response['status']) && !$response['status']) {
                $msg = isset($response['message']) ? $response['message'] : 'sorry please try later';
                return apiResponse(400, __($msg));
            }

            $responseMessage = isset($response['message']) ? $response['message'] : [];

            return apiResponse(200, __($responseMessage), []);


        } catch (\Exception $e) {
            $code = $this->logService->log('SEND-SUBSCRIPTION-FORM-WEB', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }

}
