<?php

namespace App\Models\Zatca;

use App\Models\AppModel;
use App\Models\File;

class ZatcaInvoice extends AppModel
{
    public function invoicable()
    {
        return $this->morphTo();
    }

    public function pdfFile()
    {
        return $this->morphOne(File::class, 'fileable');
    }
}
