<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAbsherOptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('absher_otps', function (Blueprint $table) {
            $table->string('transaction_id')->nullable()->change();
            $table->string('verification_code')->nullable()->change();
            $table->json('data')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('absher_otps', function (Blueprint $table) {
            $table->string('transaction_id')->change();
            $table->string('verification_code')->change();
            $table->json('data')->change();
        });
    }
}
