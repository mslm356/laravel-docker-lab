<?php

namespace App\Support\Elm\Yaqeen\Exceptions;

use Exception;

class MissingInfoRelatedToIdException extends Exception
{
    protected $requestBody;

    public function __construct($requestBody)
    {
        $this->requestBody = $requestBody;
        parent::__construct('Missing information related to this ID/Iqama');
    }

    public function context()
    {
        return [
            'request_body' => $this->requestBody,
        ];
    }
}
