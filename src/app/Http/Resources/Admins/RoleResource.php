<?php

namespace App\Http\Resources\Admins;

use Spatie\Permission\Models\Permission;
use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $permissions = Permission::get();
        $allPermissions = [];
        foreach($permissions as $permission) {
            $permission->selected = 0;
            if(!empty($this->permissions->where('name', $permission->name)->toArray())) {
                $permission->selected = 1;
            }

            $allPermissions[$permission->group]['name'] = __('permissions.group-'.$permission->group);
            $allPermissions[$permission->group]['roles'][] = [
                    'value' => $permission->name,
                    'label' => __('permissions.'.$permission->name),
                    'selected' => $permission->selected??0,
            ];
        }
        unset($allPermissions['other']);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'permissions' => array_values($allPermissions)
        ];
    }
}
