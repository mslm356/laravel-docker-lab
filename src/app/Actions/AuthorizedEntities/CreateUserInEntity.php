<?php

namespace App\Actions\AuthorizedEntities;

use App\Enums\Role as EnumsRole;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateUserInEntity
{
    use AsAction;

    /**
     * Create user under the entity
     *
     * @param \App\Models\AuthorizedEntity $entity
     * @param array $inputs
     * @return \App\Models\Users\User
     */
    public function handle($entity, $data)
    {
        $user = $entity->users()->create(
            Arr::only(
                $data,
                ['first_name', 'last_name', 'email']
            ) +
                [
                    'phone_number' => phone(Arr::get($data, 'phone'), Arr::get($data, 'phone_country_code'))->formatE164(),
                    'locale' => App::getLocale(),
                    'authorized_entity_id' => $entity->id,
                    'password' => isset($data['password']) ? Hash::make($data['password']) : null
                ]
        );

        DB::table('model_has_roles')->where('model_id', $user->id)->where('role_id', 1)->delete();

        $user->assignRole(
            Role::where([
                'name' => EnumsRole::AuthorizedEntityAdmin,
                'guard_name' => 'web'
            ])->first()
        );

        return $user;
    }
}
