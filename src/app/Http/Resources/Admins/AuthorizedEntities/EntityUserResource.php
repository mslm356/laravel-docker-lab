<?php

namespace App\Http\Resources\Admins\AuthorizedEntities;

use App\Http\Resources\Common\AuthorizedEntities\EntityUserResource as CommonEntityUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EntityUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonEntityUserResource($this->resource))->toArray($request),
            []
        );
    }
}
