<?php

namespace App\Actions\Investors;

use App\Models\File;
use App\Enums\FileType;
use App\Models\Users\User;
use App\Models\Listings\Listing;
use Lorisleiva\Actions\ActionRequest;
use App\Enums\Listings\AttachmentType;
use Lorisleiva\Actions\Concerns\AsAction;

class DownloadFile
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param array $data
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function handle(File $file)
    {
        return $file->download();
    }

    public function asController(File $file)
    {
        return $this->handle($file);
    }

    public function authorize(ActionRequest $request)
    {
        $file = $request->route('file');

        if ($file->type === FileType::SubscriptionForm) {
            return $file->fileable->investor_id === $request->user()->id;
        } else if ($file->type === FileType::ListingReport) {
            return $request->user()->can('accessListingWhichInvestedIn', $file->fileable->listing);
        } else if ($file->type === FileType::TransactionReceipt) {
            return $file->fileable->iban === $request->user()->wallet->iban;
        } else if ($file->type === FileType::ListingAttachment) {
            return $file->fileable->is_visible ||
                $request->user()->can('accessListingWhichInvestedIn', $file->fileable);
        } else if ($file->type === FileType::TransactionZatcaEInvoice) {
            return $file->fileable->invoicable->investor_id === $request->user()->id;
        }

        return false;
    }
}
