<?php

namespace App\Support\Elm\Yaqeen\Exceptions;

use Exception;

class IdException extends Exception
{
    public function __construct()
    {
        parent::__construct('ID/Iqama is not correct');
    }
}
