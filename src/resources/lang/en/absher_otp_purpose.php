<?php

use App\Enums\Elm\AbsherOtpReason;

return [
    AbsherOtpReason::InvestingInFund => 'investing in :fund',
    AbsherOtpReason::VerifyIdentity => 'verifying identity',
    AbsherOtpReason::VerifyLogin => 'verifying identity',
    AbsherOtpReason::ForgetPassword => 'Forget Password',
];
