<?php

namespace Database\Seeders;

use App\Enums\FileType;
use App\Models\Banner;
use App\Models\File;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0 ; $i < 3 ; $i++)
        {
            $banner= new Banner();
            $banner->start_date = '2022-02-27';
            $banner->end_date = '2022-10-27';
            $banner->save();
            $storagePath = "storage/banners/".$banner->id.".png";

            $file = File::create([
                'id' => (string)Str::uuid(),
                'name' => "{$banner->id}.png",
                'fileable_id' => $banner->id,
                'fileable_type' => Banner::class,
                'path' => $storagePath,
                'extra_info' => collect([]),
                'type' => FileType::BannerImage,
            ]);
        }


    }
}
