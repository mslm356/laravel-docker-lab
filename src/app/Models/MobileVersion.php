<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class MobileVersion extends AppModel
{
    use HasFactory;
}
