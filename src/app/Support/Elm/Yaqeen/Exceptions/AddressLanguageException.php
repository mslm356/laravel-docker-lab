<?php

namespace App\Support\Elm\Yaqeen\Exceptions;

use Exception;

class AddressLanguageException extends Exception
{
    public function __construct()
    {
        parent::__construct('Language is not correct');
    }
}
