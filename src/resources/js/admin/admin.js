window.Vue = require('vue');
import vSelect from 'vue-select';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import VueI18n from 'vue-i18n';
import { router } from './router';
import App from './components/App';
import './bootstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faGripVertical, faMapSigns } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import ResourceTable from '@admin/utils/Resources/ResourceTable';
import ResourcePage from '@admin/utils/Resources/ResourcePage';
import PageHeading from '@admin/utils/PageHeading';
import Button from '@admin/utils/Buttons/Button';
import Input from '@admin/utils/FormControls/Input';
import Card from '@admin/utils/Card';
import ResourceCU from '@admin/utils/Resources/ResourceCU';

library.add(faGripVertical, faMapSigns);

const i18n = new VueI18n({
    locale: document.documentElement.lang
});


Vue.component('scale-loader', require('vue-spinner/src/ScaleLoader.vue').default);


Vue.component('v-select', vSelect);
Vue.component('App', App);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('resource-table', ResourceTable);
Vue.component('page-heading', PageHeading);
Vue.component('resource-page', ResourcePage);
Vue.component('c-button', Button);
Vue.component('c-input', Input);
Vue.component('card', Card);
Vue.component('resource-cu', ResourceCU);

const app = new Vue({
    el: '#app',
    i18n,
    router,
});
