<?php

namespace App\Actions\Listings\Payouts;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Services\LogService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Exceptions\NoBalanceException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Lorisleiva\Actions\Concerns\AsAction;

class PayForInvestors
{
    use AsAction;

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function handle($investors, $payout)
    {

        $ownerIds = $payout->investors->pluck('id')->toArray();

        $chunks = array_chunk($investors, 5000);

        foreach ($chunks as $chunk) {
            foreach ($chunk as $investorData) {

                $investor = User::query()
                    ->where('id', $investorData['id'])
                    ->with('wallet')
                    ->first();

                $investorPayout = DB::table('investor_payout')
                    ->where('investor_id', $investorData['id'])
                    ->where('payout_id', $payout->id)
                    ->where('expected', '>', 0)
                    ->first();

                if (!$investorPayout || !$investor) {
                    $this->logService->log('PAYOUT-PAY-INVESTOR-IS-EXISTS', null, ['CHECK', 'investor not valid']);
                    continue;
                }

                $responseOfValidData = $this->checkValidData($investorPayout, $ownerIds);

                if (isset($responseOfValidData['status']) && $responseOfValidData['status']) {

                    if (!$this->saveTransactions($payout, $investorPayout, $investor)) {
                        continue;
                    }

                    DB::table('investor_payout')
                        ->where([
                            ['investor_id', $investorData['id']],
                            ['payout_id', $payout->id]
                        ])
                        ->increment('paid', $investorPayout->expected);

                } else {
                    $message = isset($responseOfValidData['message']) ? $responseOfValidData['message'] : 'offset not found';
                    $this->logService->log('PAYOUT-PAY-INVESTOR-VALIDATION', null, ['pay for investor', $message]);
                }
            }
        }

        $this->updateWalletBalance($payout);
    }

    public function checkValidData($investorPayout, $ownerIds)
    {

        $response = ['status' => true];

        if (!in_array($investorPayout->investor_id, $ownerIds)) {
            $response['status'] = false;
            $response['message'] = 'investor not valid  ' . $investorPayout->investor_id;
            return $response;
        }

        if (money($investorPayout->paid, defaultCurrency())->getAmount() != 0) {
            $response['status'] = false;
            $response['message'] = 'this user already paid before id ' . $investorPayout->investor_id;
            return $response;
        }

        return $response;
    }

    public function saveTransactions($payout, $investorPayout, $investor)
    {

        $wallet = $payout->listing->getWallet(WalletType::ListingSpv);
        $investorWallet = $investor->getWallet(WalletType::Investor);

        if (!$wallet || !$investorWallet) {
            return false;
        }

        $meta = [
            'reason' => TransactionReason::PayoutDistribution,
            'reference' => transaction_ref(),
            'value_date' => now('UTC')->toDateTimeString(),
            'listing_id' => $payout->listing_id,
            'listing_title_en' => $payout->listing->title_en,
            'listing_title_ar' => $payout->listing->title_ar,
            'payout_id' => $payout->id,
            'user_id' => $investorPayout->investor_id,
            'user_name' => $investor->full_name
        ];

        DB::table('transactions')->insert([
            'payable_type' => Listing::class,
            'payable_id' => $payout->listing_id,
            'wallet_id' => $wallet->id,
            'type' => 'withdraw',
            'amount' => ($investorPayout->expected * -1),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('transactions')->insert([
            'payable_type' => User::class,
            'payable_id' => $investorPayout->investor_id,
            'wallet_id' => $investorWallet->id,
            'type' => 'deposit',
            'amount' => ($investorPayout->expected),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $investorBalance = DB::table('transactions')
            ->where('payable_id', $investorPayout->investor_id)
            ->where('payable_type', User::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('id', $investorWallet->id)
            ->update(['balance' => $investorBalance,'is_overview_statistics_consistent'=>false]);

        return true;
    }

    public function updateWalletBalance($payout)
    {

        $wallet = $payout->listing->getWallet(WalletType::ListingSpv);

        $walletBalance = DB::table('transactions')
            ->where('payable_id', $payout->listing_id)
            ->where('wallet_id', $wallet->id)
            ->where('payable_type', Listing::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('id', $wallet->id)
            ->update(['balance' => $walletBalance,'is_overview_statistics_consistent'=>false]);

    }

}
