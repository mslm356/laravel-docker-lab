<?php

return [
    'welcome_message' => 'Our dear investor  :name',
    'subject' => 'New Transaction in your account',
    'message' => 'You have a new transaction in your account   ',
    'amount' => ':amount SAR',
    'type' => 'Transaction type',
    'link_text' => 'To view your control panel, please visit the link',
    'view' => 'View Dashboard',


];
