<?php

namespace App\Actions\Listings;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestor;
use App\Services\LogService;
use Bavix\Wallet\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Lorisleiva\Actions\Concerns\AsAction;

class AfterCloseListingAction
{
    use AsAction;

    public function handle(Listing $listing)
    {
        $log_service=new LogService();

        $totals=DB::table('listing_investor_records')
            ->select(DB::raw('sum(amount_without_fees) as total_amount_without_fees'),
                DB::raw('sum(admin_fees) as total_admin_fees_amount'),
                DB::raw('sum(vat_amount) as total_vat_amount')
            )->where('listing_id',$listing->id)->first();

        $listingWalletEscrowAdminFee = $listing->getWallet(WalletType::ListingEscrowAdminFee);
        $listingWalletEscrowVat = $listing->getWallet(WalletType::ListingEscrowVat);
        $listingWalletEscrow = $listing->getWallet(WalletType::ListingEscrow);

        $meta = [
            'reason' => TransactionReason::InvestingInListing,
            'reference' => transaction_ref(),
            'value_date' => now('UTC')->toDateTimeString(),
            'listing_id' => $listing->id,
            'shares'=>$listing->total_invested_shares,
            'listing_title_en' => $listing->title_en,
            'listing_title_ar' => $listing->title_ar,
        ];

        DB::beginTransaction();
        try {

            $wallets_count=DB::table('transactions')->select(DB::raw('count(id) as num_of_wallets'))
                ->where('wallet_id',$listingWalletEscrow->id)
                ->where('reason',TransactionReason::InvestingInListing)
                ->first();

            if ($wallets_count->num_of_wallets > 1)
            {
                $log_service->log('AFTER-Close-Listing-ERROR',null,['error -> this is old investing']);
                DB::rollBack();
                return 'error -> this is old investing';
            }

            Transaction::updateOrCreate(['wallet_id'=>$listingWalletEscrow->id,'reason'=>TransactionReason::InvestingInListing],[
                'payable_type' => Listing::class,
                'payable_id' => $listing->id,
                'wallet_id' => $listingWalletEscrow->id,
                'type' => 'deposit',
                'amount' => $totals->total_amount_without_fees,
                'confirmed' => 1,
                'meta' => $meta,
                'uuid' => Str::uuid(),
            ]);


            $feesMeta = array_merge($meta, [
                'reason' => TransactionReason::InvestingInListingAdminFee,
                'reference' => $adminFeesTransactionRef = transaction_ref(),
                'fee_percentage' => $listing->administrative_fees_percentage,
            ]);

            Transaction::updateOrCreate(['wallet_id'=>$listingWalletEscrowAdminFee->id,'reason'=>TransactionReason::InvestingInListingAdminFee],
                [
                    'payable_type' => Listing::class,
                    'payable_id' => $listing->id,
                    'wallet_id' => $listingWalletEscrowAdminFee->id,
                    'type' => 'deposit',
                    'amount' => $totals->total_admin_fees_amount,
                    'confirmed' => 1,
                    'meta' => $feesMeta,
                    'uuid' => Str::uuid(),
                ]);


            $vatMeta = array_merge($meta, [
                'reason' => TransactionReason::InvestingInListingVatOfAdminFee,
                'reference' => transaction_ref(),
                'vat_percentage' => $listing->vat_percentage,
            ]);

            Transaction::updateOrCreate(['wallet_id'=>$listingWalletEscrowVat->id,'reason'=>TransactionReason::InvestingInListingVatOfAdminFee],
                [
                    'payable_type' => Listing::class,
                    'payable_id' => $listing->id,
                    'wallet_id' => $listingWalletEscrowVat->id,
                    'type' => 'deposit',
                    'amount' => $totals->total_vat_amount,
                    'confirmed' => 1,
                    'meta' => $vatMeta,
                    'uuid' => Str::uuid(),
                ]);

            $listingWalletEscrowAdminFee->refreshBalance();
            $listingWalletEscrowVat->refreshBalance();
            $listingWalletEscrow->refreshBalance();

            DB::commit();
            return 'transaction done';

        }catch (\Exception $e)
        {
            DB::rollBack();
            $log_service->log('AFTER-Close-Listing-Exception',$e);
            return 'error -> '.$e->getMessage();
        }

    }


}
