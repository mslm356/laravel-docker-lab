<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\DBColumInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class InvestmentObjective extends Enum implements LocalizedEnum,DBColumInterface
{
    const ProtectionOfCapital =   1;
    const Income =   2;
    const Balanced =   3;
    const GrowthOfCapital =   4;


    function getDBColumnName()
    {
        return 'invest_objective';
    }

}
