<?php

namespace App\Actions\Listings\Wallets;

use App\Enums\Banking\WalletType;
use Illuminate\Support\Carbon;
use App\Models\Listings\Listing;
use Maatwebsite\Excel\Facades\Excel;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Exports\Wallets\WalletTransactionsExport;

class ExportListingTransactions
{
    use AsAction, MapsWalletType;

    /**
     * Handle export listing transactions
     *
     * @param \App\Models\Listings\Listing $listing
     * @param array $data ['start_date', 'end_date', 'wallet_type', 'extension']
     * @param string $inputDatesTz
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function handle(Listing $listing, $data, $inputDatesTz = 'Asia/Riyadh')
    {
        $walletType = $data['wallet_type'];

        if ($walletType == 'ESCROW') {
            $walletIds = $listing->wallets()
                ->whereIn('slug', [WalletType::ListingEscrow, WalletType::ListingEscrowAdminFee, WalletType::ListingEscrowVat])
                ->get()
                ->pluck('id');
        } else {
            $walletIds = [$listing->getWallet(WalletType::ListingSpv)->id];
        }

        return Excel::download(
            new WalletTransactionsExport(
                $this->toCarbonInstance($data['start_date'], $inputDatesTz)->setTime(0,0)->timezone('UTC'),
                $this->toCarbonInstance($data['end_date'], $inputDatesTz)->endOfDay()->timezone('UTC'),
                $walletIds
            ),
            "fund-{$listing->id}-transactions-{$data['start_date']}-{$data['end_date']}.{$data['extension']}"
        );
    }

    public function toCarbonInstance($date, $timezone)
    {
        return Carbon::createFromFormat('Y-m-d', $date, $timezone);
    }
}
