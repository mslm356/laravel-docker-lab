<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinMaxDelayOfSendSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->json('sms_delay')->nullable();
        });

        \Illuminate\Support\Facades\DB::statement('UPDATE `listings` SET sms_delay= "{\"min\":0,\"max\":0}"');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->dropColumn('sms_delay');
        });

    }
}
