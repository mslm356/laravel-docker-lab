<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_discussions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('listing_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->text('text');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('user_role_id');
            $table->string('replier_name')->nullable();
            $table->json('replier_role')->nullable();
            $table->unsignedTinyInteger('status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('listing_id')->references('id')->on('listings');
            $table->foreign('parent_id')->references('id')->on('listing_discussions');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_role_id', 'listing_discussions_user_role_id_foreign')
                ->references('id')
                ->on(config('permission.table_names.roles'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_discussions');
    }
}
