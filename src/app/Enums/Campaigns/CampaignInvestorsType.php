<?php

namespace App\Enums\Campaigns;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class CampaignInvestorsType extends Enum
{
    const Custom = 1;
    const MembershipLevel = 2;
    const InCompletedKycType = 3;
    const upgradedInvestors = 4;
    const listingInvestorType = 5;
    const SuitableInvestor = 6;
    const UnsuitableInvestor = 7;
}
