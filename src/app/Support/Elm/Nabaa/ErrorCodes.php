<?php

namespace App\Support\Elm\Nabaa;

class ErrorCodes
{
    const configurationCodes = [
        '20001',
        '20002',
        '20003',
        '10003',
        '20005',
        '20004',
        '20006',
        '20007',
        '20008',
        '20009',
        '30001',
        '50001',
        '10005',
    ];

    const systemCodes = [
        '10004',
        '10008',
    ];
}
