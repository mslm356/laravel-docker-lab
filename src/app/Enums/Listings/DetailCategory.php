<?php

namespace App\Enums\Listings;

use BenSampo\Enum\Enum;

final class DetailCategory extends Enum
{
    const PropertyDetails =   'PROPERTY_DETAILS';
    const FinancialDetails =   'FINANCIAL_DETAILS';
    const TermsAndConditions =   'TERMS_CONDITIONS';
    const Team =   'TEAM';
    const DueDiligence  = 'DUE_DILIGENCE';
    const Timeline  = 'TIMELINE';
    const Risks  = 'RISKS';
    const Location = 'LOCATION';
}
