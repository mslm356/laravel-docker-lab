<?php

namespace App\Actions\Aml;

use App\Enums\Investors\InvestorStatus;
use App\Enums\Investors\ProfileReviewType;
use App\Mail\Investors\ApproveProfile;
use App\Mail\Investors\RejectProfile;
use Illuminate\Support\Facades\Mail;
use Lorisleiva\Actions\Concerns\AsAction;

class InvestorUpdateStatus
{
    use AsAction;

    public function handle($user,$data,$admin_id)
    {
        $investor_profile = $user->investor()->lockForUpdate()->firstOrFail();

        $oldProfileStatus = $investor_profile->status;

        $investor_profile->update($data);

        if ($user->type == 'company' && $user->companyProfile)
        {
            $user->companyProfile->status = $data['status'];
            $user->companyProfile->save();
        }

        if (
            $data['status'] == InvestorStatus::Approved
            && $oldProfileStatus != InvestorStatus::Approved
        ) {
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($user)->queue(new ApproveProfile());
            }

        }

        if (
            $data['status'] == InvestorStatus::Rejected
            && $oldProfileStatus != InvestorStatus::Rejected
        ) {
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($user)->queue(new RejectProfile($investor_profile));
            }

        }

        return true;
    }
}
