<?php

namespace App\Actions\AuthorizedEntities;

use App\Models\File;
use App\Enums\FileType;
use Illuminate\Support\Arr;
use App\Models\AuthorizedEntity;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateEntity
{
    use AsAction;

    /**
     * Create new entity
     *
     * @param \App\Models\AuthorizedEntity $entity
     * @param array $inputs
     * @return void
     */
    public function handle($data)
    {
        $entity = AuthorizedEntity::create(
            Arr::only($data, ['name', 'cr_number', 'description', 'status', 'creator_id', 'creator_role_id'])
        );

        foreach (Arr::get($data, 'cr', []) as $file) {
            File::upload($file, "authorized-entities/crs/{$entity->id}", [
                'fileable_id' => $entity->id,
                'fileable_type' => $entity->getMorphClass(),
                'type' => FileType::AuthorizedEntityCR,
            ]);
        }

        File::upload(Arr::get($data, 'cma_license'), "authorized-entities/cma-license/{$entity->id}", [
            'fileable_id' => $entity->id,
            'fileable_type' => $entity->getMorphClass(),
            'type' => FileType::AuthorizedEntityCMALicense,
        ]);

        return $entity;
    }
}
