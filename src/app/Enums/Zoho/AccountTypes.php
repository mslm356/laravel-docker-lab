<?php

namespace App\Enums\Zoho;

use BenSampo\Enum\Enum;

final class AccountTypes extends Enum
{
    const ANBCurrent = 'ANB - Current';
    const ANBBank = 'Anb bank';
    const InvestorWallet = 'Investor wallet';
    const FundWallet = 'Fund wallet';
    const AccountsReceivable = 'Accounts Receivable';
    const OutputVAT  = 'Output VAT';
}
