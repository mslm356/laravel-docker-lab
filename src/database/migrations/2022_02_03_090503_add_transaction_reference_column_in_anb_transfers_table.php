<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransactionReferenceColumnInAnbTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('anb_transfers', function (Blueprint $table) {
            $table->string('external_trans_reference')->after('external_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('anb_transfers', function (Blueprint $table) {
            $table->dropColumn('external_trans_reference');
        });
    }
}
