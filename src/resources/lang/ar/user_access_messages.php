<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'NinNotValid' => 'الهوية غير صالحة',
    'ProfileNotValid' => 'الملف الشخصى غير صالح',
    'IdentityVerified' => 'الحساب غير مفعل ',
    'ApprovedStatus' => 'الحساب لم يتم الموافقة عليه',
    'KycCompleted' => 'الحساب الشخصى غير كامل',
    'SuitabilityStatus'=>'بناءً على الافصاحات المقدمة من قبلكم على (نموذج اعرف عميلك) وبعد تحليل البيانات اتضح عدم ملاءمتكم للفرصة الاستثمارية، في حال رفض التقييم المقدم لكم الرجاء المتابعة وتقديم طلبكم لنا.',
    'AmericanNationalty' => "نظراً لمتطلبات الإقرار الضريبي، نعتذر عن قبول الحسابات الاستثمارية لحاملي الجنسية الامريكية",

];
