<?php

namespace App\Observers;

use App\Actions\Investors\ExportTransactionReceipt;
use App\Enums\Banking\TransactionReason;
use App\Enums\Notifications\NotificationTypes;
use App\Enums\Role;
use App\Mail\Investors\NewTransaction;
use App\Models\Payouts\Payout;
use App\Models\Users\User;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Models\VirtualBanking\Transaction;

class TransactionObserver
{

    use NotificationServices, SendMessage;

    /**
     * Handle the Transaction "created" event.
     *
     * @param \App\Models\VirtualBanking\Transaction $transaction
     * @return void
     */
    public function created(Transaction $transaction)
    {
        if (
            $transaction->payable_type === User::class &&
            $transaction->payable->hasRole(Role::Investor) &&
            $transaction->confirmed
        ) {

            $user = $transaction->payable;
//            ExportTransactionReceipt::dispatch($transaction, $user);

            $investReasons = [TransactionReason::InvestingInListing, TransactionReason::InvestingInListingAdminFee, TransactionReason::InvestingInListingVatOfAdminFee];

            if (!in_array($transaction->meta['reason'], $investReasons)) {

                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($transaction->payable)->queue(new NewTransaction($user, $transaction));
                }

                if ($transaction->meta['reason'] == TransactionReason::PayoutDistribution) {

                    $fund_title = isset($transaction->meta["listing_title_" . $user->locale]) ? $transaction->meta["listing_title_" . $user->locale] : "---";

                    $this->sendSMSPayoutData($user, $transaction, $fund_title);

                    $notificationData = $this->payoutNotificationData($user, $transaction, $fund_title);
                    $this->send(NotificationTypes::Deposit, $notificationData, $user->id);

                } else {

                    $notificationData = $this->notificationData($user, $transaction);
                    $this->send(NotificationTypes::Deposit, $notificationData, $user->id);

                    // send Message
                    $this->messageData($user, $transaction);

                }
            }
        }
    }

    public function messageData($user, $transaction)
    {
        $amount = $transaction->amount_float;
        $name = $user ? $user->full_name : '---';
        $content_ar_1 = '  مستثمرنا الكريم:  ' . $name;
        $content_ar_2 = '  تم إجراء عملية إيداع لمحفظتك الاستثمارية  ';
        $content_ar_3 = ' بمبلغ قدره: ' . $amount;
        $content_ar_4 = ' تاريخ ووقت العملية  ' . Carbon::parse($transaction->updated_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i');
        $content_ar_5 = ' شكراً لتعاملك مع منصة أصيل المالية  ';

        $content_ar = $content_ar_1 . $content_ar_2 . $content_ar_3 . $content_ar_4 . $content_ar_5;
        $content_en = "A deposit has been made to your investment portfolio In the amount of : " . $amount . " Operation date and time " . Carbon::parse($transaction->updated_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i') . " Thank you for dealing with Aseel Financial Platform ";

        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }

    public function sendSMSPayoutData($user, $transaction, $listingTitle)
    {
        $payout_id = isset($transaction->meta["payout_id"]) ? $transaction->meta["payout_id"] : null;
        $payout = Payout::where('id', $payout_id)->first();

        $amount = $transaction->amount_float;
        $content_ar_1 = ' إيداع العوائد النقدية لصندوق ' . '(#' . $listingTitle . ')';
        $content_ar_1_dis = ' إيداع  التوزيعات النقدية لصندوق ' . '(#' . $listingTitle . ')';
        $content_ar_2 = 'وتوزيع المستحقات في حسابك ' . $amount . ' ريال سعودى ';
        $content_ar_2_dis = 'وتوزيع الأرباح في حسابك ' . $amount . ' ريال سعودى ';

        $content_en = $payout && $payout->type == "payout" ? 'Cash proceeds have been deposited to Fund (#' . $listingTitle . ') And distribute the returns in your account is ' . $amount . ' SAR' : 'The cash dividends have been deposited for Fund (#' . $listingTitle . ') And the profit distributed in your account is ' . $amount . ' SAR';
        $content_ar = $payout && $payout->type == "payout" ? $content_ar_1 . "\n" . $content_ar_2 : $content_ar_1_dis . "\n" . $content_ar_2_dis;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);
    }

    public function notificationData($user, $transaction)
    {

        $amount = $transaction->amount_float;
        $name = $user ? $user->full_name : '---';
        $iban = isset($transaction->meta["vaccount_identifier"]) ? $transaction->meta["vaccount_identifier"] : "---";
        $content_ar_1 = '  مستثمرنا الكريم:  ' . $name;

        $content_ar_3 = '  الآيبان الافتراضي: ' . $iban;
        $content_ar_4 = ' بمبلغ قدره: ' . $amount;
        $content_ar_5 = ' تاريخ ووقت العملية  ' . $transaction->updated_at;

        return [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => 'Deposit Transaction',
            'title_ar' => 'عملية إيداع',
            'content_en' => ' Our dear investor ' . $name . ' A transaction to your wallet has been made With a price of : ' . $amount
                . ' With Virtual IBAN: ' . $iban . 'The date and time of the operation ' . $transaction->updated_at,
            'content_ar' => $content_ar_1 . $content_ar_3 . $content_ar_4 . $content_ar_5,
        ];
    }

    public function payoutNotificationData($user, $transaction, $listingTitle)
    {

        $payout_id = isset($transaction->meta["payout_id"]) ? $transaction->meta["payout_id"] : null;
        $payout = Payout::where('id', $payout_id)->first();

        $amount = $transaction->amount_float;
        $content_ar_1 = ' إيداع العوائد النقدية لصندوق ' . '(#' . $listingTitle . ')';
        $content_ar_1_dis = ' إيداع  التوزيعات النقدية لصندوق ' . '(#' . $listingTitle . ')';
        $content_ar_2 = 'وتوزيع المستحقات في حسابك ' . $amount . ' ريال سعودى ';
        $content_ar_2_dis = 'وتوزيع الأرباح في حسابك ' . $amount . ' ريال سعودى ';

        return [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => $payout && $payout->type == "payout" ? 'Deposit cash returns' : 'Deposit cash dividends ',
            'title_ar' => $payout && $payout->type == "payout" ? 'إيداع عوائد نقدية' : 'إيداع توزيعات نقدية',
            'content_en' => $payout && $payout->type == "payout" ? 'Cash proceeds have been deposited to Fund (#' . $listingTitle . ') And distribute the returns in your account is ' . $amount . ' SAR' :
                'The cash dividends have been deposited for Fund (#' . $listingTitle . ') And the profit distributed in your account is ' . $amount . ' SAR',
            'content_ar' => $payout && $payout->type == "payout" ? $content_ar_1 . "\n" . $content_ar_2 : $content_ar_1_dis . "\n" . $content_ar_2_dis,
        ];
    }

    /**
     * Handle the Transaction "updated" event.
     *
     * @param \App\Models\VirtualBanking\Transaction $transaction
     * @return void
     */
    public function updated(Transaction $transaction)
    {
        if (
            $transaction->payable_type === User::class &&
            $transaction->payable->hasRole(Role::Investor) &&
            $transaction->wasChanged('confirmed') &&
            $transaction->confirmed
        ) {
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($transaction->payable)->queue(new NewTransaction($transaction->payable, $transaction));
            }
        }
    }

    /**
     * Handle the Transaction "deleted" event.
     *
     * @param \App\Models\VirtualBanking\Transaction $transaction
     * @return void
     */
    public function deleted(Transaction $transaction)
    {
        //
    }

    /**
     * Handle the Transaction "restored" event.
     *
     * @param \App\Models\VirtualBanking\Transaction $transaction
     * @return void
     */
    public function restored(Transaction $transaction)
    {
        //
    }

    /**
     * Handle the Transaction "force deleted" event.
     *
     * @param \App\Models\VirtualBanking\Transaction $transaction
     * @return void
     */
    public function forceDeleted(Transaction $transaction)
    {
        //
    }
}
