<?php


namespace App\Support\Zoho;


use App\Models\ZohoSettings;
use App\Settings\AccountingSettings;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Account
{

    public $base_url;
    public $organization_id;
    public $auth;
    public $settings;


    public function __construct()
    {
        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
        $this->settings = new Settings();
    }

    public function prepareAccountData ($accountData)
    {
        $data = [
            'account_name' => $accountData['account_name'],
            'account_code' => $accountData['account_code'],
            'account_type' => $accountData['account_type'],
        ];

        return $data;
    }

    public function store($accountData)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->prepareAccountData($accountData);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'chartofaccounts?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                $this->settings->saveSettings($accountData['account_name'], $responseData['chart_of_account']['account_id']);
                return true;
            }

            Log::error('ZOHO-NEW-Account', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-NEW-Account', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }

    public function list()
    {

        $token = $this->auth->getToken();

        try {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->get($this->base_url . 'chartofaccounts?organization_id=' . $this->organization_id);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                return $responseData['chartofaccounts'];
            }

            Log::error('ZOHO-LIST-ACCOUNT', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-LIST-ACCOUNT', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }
}
