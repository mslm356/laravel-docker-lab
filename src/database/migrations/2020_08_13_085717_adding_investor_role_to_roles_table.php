<?php

use App\Enums\Role as EnumsRole;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;

class AddingInvestorRoleToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::create(['name' => EnumsRole::Investor, 'guard_name' => 'web']);
        Role::create(['name' => EnumsRole::Admin, 'guard_name' => 'web']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::whereIn('name', [EnumsRole::Investor, EnumsRole::Admin])->delete();
    }
}
