<?php

namespace App\Http\Resources\Common\NationalIdentities;

use Illuminate\Http\Resources\Json\JsonResource;

class IdentityAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'post_code' => $this->convertNumbersToEnglish($this->post_code),
            'additional_number' => $this->convertNumbersToEnglish($this->additional_number),
            'building_number' => $this->convertNumbersToEnglish($this->building_number),
            'city' => $this->city,
            'district' => $this->district,
            'location' => $this->convertNumbersToEnglish($this->location),
            'street_name' => $this->street_name,
            'unit_number' => $this->convertNumbersToEnglish($this->unit_number),
        ];
    }

    function convertNumbersToEnglish($string) {
        $newNumbers = range(0, 9);
        // 1. Persian HTML decimal
        $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
        // 2. Arabic HTML decimal
        $arabicDecimal = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;');
        // 3. Arabic Numeric
        $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        // 4. Persian Numeric
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');

        $string =  str_replace($persianDecimal, $newNumbers, $string);
        $string =  str_replace($arabicDecimal, $newNumbers, $string);
        $string =  str_replace($arabic, $newNumbers, $string);
        return str_replace($persian, $newNumbers, $string);
    }
}
