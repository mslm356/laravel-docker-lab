<?php

namespace App\Enums\Investors;

use BenSampo\Enum\Enum;

final class ProfileReviewType extends Enum
{
    const Automatic = 1;
    const Manual = 2;
    const BatchApprove = 3;
    const DeleteAccount = 4;
}
