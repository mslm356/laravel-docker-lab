<?php

namespace App\Support\Elm\Nabaa\Exceptions;

use App\Models\Elm\NabaaMessage;
use Exception;

class VerificationSmsException extends Exception
{
    protected $reason;
    protected $nabaaMsg;

    public function __construct(NabaaMessage $nabaaMsg, $reasonCode)
    {
        parent::__construct("Failed to verify SMS verification code [{$reasonCode}]");

        $this->reason = $reasonCode;
        $this->nabaaMsg = $nabaaMsg;
    }

    public function getReasonCode()
    {
        return $this->reason;
    }

    public function context()
    {
        return [
            'nabaa_msg_id' => $this->nabaaMsg->id,
            'reason' => $this->reason,
        ];
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'reason' => $this->reason
            ], 400);
        } else {
            abort(400);
        }
    }
}
