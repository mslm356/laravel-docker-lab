<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Enums\Listings\ListingReviewStatus;
use App\Jobs\Funds\SendAlertMailsOnCreate;
use App\Models\Listings\Listing;
use App\Http\Controllers\Controller;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UpdateListingStatus extends Controller
{
    /**
     * Upodate listing status
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, $listing)
    {
        $data = $this->validate($request, [
            'comment' => ['exclude_if:status,' . ListingReviewStatus::Approved, 'required', 'string', 'max:1000',textValidation()],
            'status' => ['required', 'integer', new EnumValue(ListingReviewStatus::class, false)]
        ]);

        DB::transaction(function () use ($request, $listing, $data) {
            $listing = Listing::lockForUpdate()->findOrFail($listing);

            /*if ($listing->review_status != ListingReviewStatus::Approved && $request->status == ListingReviewStatus::Approved
                && $listing->is_visible == 1 && $listing->is_closed == 0) {
                $sendAlertMailsOnCreateJob = (new SendAlertMailsOnCreate($listing));
                dispatch($sendAlertMailsOnCreateJob);
            }*/

            $listing->update([
                'review_status' => $data['status']
            ]);

            $listing->createRevision([
                'auditor_id' => $request->user('sanctum')->id,
                'comment' => $data['comment'] ?? null,
            ]);
        });

        return response()->json([], 201);
    }
}
