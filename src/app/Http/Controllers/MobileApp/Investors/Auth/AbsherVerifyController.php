<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\Elm\Natheer\NatheerRegisterationJob;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\NationalIdentity;
use App\Services\Investors\RegisterServices;
use App\Services\Investors\TokenServices;
use App\Services\LogService;
use App\Support\Aml\AmlServices;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AbsherVerifyController extends Controller
{

    public $tokenServices;
    public $yaqeenInfoService;
    public $logService;
    public $amlServices;
    public $registerServices;


    public function __construct()
    {
        $this->logService = new LogService();
        $this->tokenServices = new TokenServices();
        $this->yaqeenInfoService = new YaqeenInfoService();
        $this->amlServices = new AmlServices();
        $this->registerServices = new RegisterServices();

    }

    public function verifyNin(Request $request)
    {
        $user = getAuthUser('api');

        try {

            $data = $this->validate($request, $this->registerServices->getRules());

            $absherOtp = AbsherOtp::where('customer_id', $data['nin'])->find($data['vid']);

            if (
                $absherOtp === null ||
                Arr::get($user->investor->data, 'identity_verification_tmp.vid') !== $absherOtp->id
            ) {
                throw ValidationException::withMessages([
                    'vid' => __('messages.invalid_sms_verification_vid'),
                ]);
            }

            if ($this->checkCodeIsExpired($absherOtp)) {
                return apiResponse(400, __('mobile_app/message.code_expired'));
            }

            $isVerified = VerificationSms::instance()->verify($data['nin'], $absherOtp, $data['code']);

            if (!$isVerified) {
                return apiResponse(400, __('messages.invalid_sms_verification_code'));
            }

            DB::transaction(function () use ($data, $user) {

                $this->registerServices->deleteOldIdentities($user);

                $identity = NationalIdentity::create($user->investor->data['identity_verification_tmp']['identity']);

                foreach ($user->investor->data['identity_verification_tmp']['addresses'] as $address) {
                    $identity->addresses()->create($address);
                }

                $user->investor->update([
                    'is_identity_verified' => true,
                    'data->identity_verification_tmp' => null,
                ]);

            }, 1);

            dispatch(new NatheerRegisterationJob($user));

            return apiResponse(200, __('mobile_app/message.success'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('RESEND-ABSHER-SMS', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (VerificationSmsException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (\Exception $e) {
            $code = $this->logService->log('RESEND-ABSHER-SMS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }


    public function deleteOldIdentities($user)
    {
        $oldIdentities = NationalIdentity::where('user_id', $user->id)->get();

        foreach ($oldIdentities as $oldIdentity) {
            $oldIdentity->addresses()->delete();
            $oldIdentity->delete();
        }
    }

    public function checkCodeIsExpired($absherOtp)
    {

        $minutes = 6;

        if ($absherOtp->created_at->lessThan(now()->subMinutes($minutes))) {
            $absherOtp->expired_at = Carbon::parse($absherOtp->created_at)->addMinutes($minutes);
            $absherOtp->save();

            return true;
        }

        return false;
    }

    public function resendAbsherSms(Request $request)
    {

        try {

            $user = getAuthUser('api');

            $profile = $user->investor;

            if (!$profile) {
                return apiResponse(400, __('mobile_app/message.please_try_later'));
            }

            $data = [
                'nin' => isset($profile->data['identity_verification_tmp']) && isset($profile->data['identity_verification_tmp']['identity'])
                && $profile->data['identity_verification_tmp']['identity']['nin'] ? $profile->data['identity_verification_tmp']['identity']['nin'] : null,
            ];

            $verificationMsg = $this->yaqeenInfoService->sendSms($data, $user);

            $identity_verification_tmp_data = $profile->data['identity_verification_tmp'];

            $identity_verification_tmp_data['vid'] = $verificationMsg->id;

            $profile->update(['data->identity_verification_tmp' => $identity_verification_tmp_data]);

            $responseData = ['vid' => $verificationMsg->id, 'nin' => $data['nin']];

            return apiResponse(200, __('mobile_app/message.code_sent_successfully'), $responseData);

        } catch (ValidationException $e) {
            $code = $this->logService->log('RESEND-ABSHER-SMS', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('RESEND-ABSHER-SMS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }

}
