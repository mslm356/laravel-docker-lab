<?php

namespace App\Http\Resources\Admins\Campaigns;

use App\Enums\Campaigns\CampaignInvestorsType;
use App\Enums\Campaigns\CampaignStatus;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CampaignsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'title_en' => $this->title_en,
            'title_ar' => $this->title_ar,
            'description_en' => $this->description_en,
            'description_ar' => $this->description_ar,
            'type' => $this->type,
            'start_at' => Carbon::parse($this->start_at)->format('Y-m-d H:i'),
            'investors_type' => $this->investors_type ? CampaignInvestorsType::getValue($this->investors_type) : null,
            'investor_ids' => $this->getUserIds($this->users) && count($this->getUserIds($this->users)) ? implode(",", $this->getUserIds($this->users)) : null,
            'status' => $this->getStatus($this->status),
            'create_at' => Carbon::parse($this->created_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i'),
            'updated_at' => Carbon::parse($this->updated_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i'),
        ];
    }

    public function getStatus($value)
    {
        return [
            'value' => $value,
            'description' => __(CampaignStatus::getKey($value)),
        ];
    }

    public function getUserIds($userIds)
    {
        $result = [];
        foreach ($userIds as $userId) {
            $result[] = $userId->pivot->user_id;
        }
        return $result;
    }
}
