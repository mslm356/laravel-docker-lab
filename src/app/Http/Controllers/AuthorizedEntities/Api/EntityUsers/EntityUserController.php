<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\EntityUsers;

use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Actions\AuthorizedEntities\CreateUserInEntity;
use App\Actions\AuthorizedEntities\SendUserInvitation;
use App\Actions\AuthorizedEntities\UpdateUserInEntity;
use App\Http\Requests\AuthorizedEntities\Users\UpdateEntityUserRequest;
use App\Http\Requests\AuthorizedEntities\Users\InviteUserToEntityRequest;
use App\Http\Resources\AuthorizedEntities\EntityUsers\EntityUserResource;
use App\Http\Resources\AuthorizedEntities\EntityUsers\EntityUserListItemResource;

class EntityUserController extends Controller
{
    /**
     * Get the users of the entity
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return EntityUserListItemResource::collection(
            authorizedEntity()
                ->users()
                ->where('id', '!=', $request->user('sanctum')->id)
                ->simplePaginate()
        );
    }

    /**
     * Show the user information
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($user)
    {
        $user = authorizedEntity()->users()->whereId($user)->firstOrFail();

        return new EntityUserResource($user);
    }

    /**
     * Invite user to the entity
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function store(InviteUserToEntityRequest $request)
    {
        DB::transaction(function () use ($request) {
            $user = CreateUserInEntity::run(
                authorizedEntity(),
                array_merge($request->validated(), ['password' => null])
            );

            $this->sendInvitationToUser($user);
        });

        return response()->json();
    }

    /**
     * Update user
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\AuthorizedEntity $authorizedEntity
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateEntityUserRequest $request, User $user)
    {
        $this->authorize('canAccessUserWithinAuthorizedEntity', $user);

        DB::transaction(function () use ($request, $user) {
            UpdateUserInEntity::run($user, $request->validated());

            if ($user->password === null) {
                $this->sendInvitationToUser($user);
            }
        });

        return response()->json();
    }

    public function sendInvitationToUser($user)
    {
        SendUserInvitation::run($user, request()->user('sanctum'));
    }
}
