<?php

namespace App\Http\Controllers\Auth;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\Investors\AbsherOtpService;
use App\Services\Investors\LastLoginService;
use App\Support\Elm\AbsherOtp\TwoFactorAuth;
use App\Support\Users\UserAccess;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Users\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use function Google\Auth\Cache\get;
use App\Enums\Investors\UserAccessMessages;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public $twoFactorAuthenticate;
    public $userAccess;
    public $absherOtpService;

    protected $maxAttempts = 3;
    protected $decayMinutes = 5;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'login']);
        $this->twoFactorAuthenticate = new TwoFactorAuth();
        $this->userAccess = new UserAccess();
        $this->absherOtpService = new AbsherOtpService();
    }

    /**
     * The user has been authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
    }

    /**
     * The type validator instance (Not used)
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
//    public function validator($data)
//    {
//        return Validator::make($data, [
//            'email' => 'email|nullable',
//            'c_number' => 'string|nullable',
//            'password' => 'required',
//        ]);
//    }

    public function validateLogin(Request $request)
    {
        $rules = [
            'password' => 'required|string',
        ];

        if ($request->has('c_number')) {
            $rules['c_number'] = 'string|required';
        } else {
            $rules['email'] = 'email|required';
        }

        $request->validate($rules);
    }

    protected function attemptLogin(Request $request)
    {

        $user = $this->getUserByData($request->all());

        if (is_null($user) || !Hash::check($request['password'], $user->password)) {
            return false;
        }

        if (!$user) {
            return apiResponse(422, __('messages.data_not_correct'));
        }

        if ($user->investor && $user->investor->status == 8) {
            return validationResponse(422, __('messages.deleted_account'));
        }

        $isUserInvestor = $user->hasRole(Role::Investor);

        if ($isUserInvestor) {

            $can_access=$user->canAccess;
            if ($can_access && $can_access->login !=200) {
                $message = 'user_access_messages.' . UserAccessMessages::getKey($can_access->login);
                return apiResponse(422, __($message));
            }

            $responseOfUserIsVerified = $this->checkUserIsVerified($user);

            if (!$responseOfUserIsVerified['status']) {

                if ($responseOfUserIsVerified['nin']) {

//                    Auth::login($user);
                    $email=($user->type == 'user')?$request['email']:$user->email;

                    return apiResponse(200, __('messages.identity_verified_valid'), [
                        'vid' => $responseOfUserIsVerified['vid'],
                        'is_verified' => false,
                        'sessionKey'=>encrypt($email)
                    ]);
                }

                return apiResponse(422, __('messages.nationalIdentity_valid'));
            }
        }

//        $module = $isUserInvestor ? 'login' : 'admin';
//
//        $responseOfUserAccess = $this->userAccess->handle($user, $module);
//
//        if (isset($responseOfUserAccess['status']) && !$responseOfUserAccess['status'] && $user->hasRole(Role::Investor)) {
//
//            $message = isset($responseOfUserAccess['message']) ? $responseOfUserAccess['message'] : 'you not have access';
//            return response()->json(['message' => __($message)], 400);
//        }

        if ($user->type == 'company' && $request->has('email')) {
            return validationResponse(422, __('The given data was invalid.'));
        }

        if ($user->phone_number) {

            $msg = $this->twoFactorAuthenticate->twoFactorAuth($user);

            if ($msg && $user->authorized_entity_id == null) {

                $email=($user->type == 'user')?$request['email']:$user->email;
                if ($isUserInvestor)
                {
                    return response()->json(['data' =>
                        ['vid' => $user->vid,'sessionKey'=>encrypt($email)]
                    ], 200);

                }
                else
                {
                    return response()->json(['data' =>
                        ['vid' => $user->vid]
                    ], 200);

                }
            }
        }

        new LastLoginService($user);

        return $this->guard()->attempt($this->credentials($request));
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (config('auth.login_limit') == 'active' && method_exists($this, 'hasTooManyLoginAttempts')
            && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $attempt = $this->attemptLogin($request);

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);


        if (is_object($attempt)) {
            return $attempt;
        }

        if ($attempt) {
            if ($request->hasSession()) {
                $request->session()->put('auth.password_confirmed_at', time());
            }

            $user = getAuthUser('web');

            if($user && $user->authorizedEntity == null)
            $this->deleteOtherSessionRecords($request);

            return $this->sendLoginResponse($request);
        }


        return $this->sendFailedLoginResponse($request);
    }

    protected function deleteOtherSessionRecords($request)
    {
        if (config('session.driver') !== 'database') {
            return;
        }

        DB::table(config('session.table', 'sessions'))
            ->where('user_id', $request->user()->getAuthIdentifier())
            ->where('id', '!=', $request->session()->getId())
            ->delete();
    }

    public function getUserByData($requestedData)
    {
        if (isset($requestedData['c_number'])) {
            return User::where('c_number', $requestedData['c_number'])->first();
        }

        if (isset($requestedData['email'])) {
            return User::where('email', $requestedData['email'])->first();
        }

        return null;
    }

    protected function credentials(Request $request)
    {
        $username = $request->has('c_number') ? 'c_number' : 'email';

        return $request->only($username, 'password');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $username = $request->has('c_number') ? 'c_number' : 'email';

        throw ValidationException::withMessages([
            $username => [trans('auth.failed')],
        ]);
    }

    public function checkUserIsVerified($user)
    {

        $response = [
            'status' => true,
            'nin' => null,
        ];

        $profile = $user->investor;

        if ($profile->is_identity_verified) {
            return $response;
        }

        if (isset($profile->data['identity_verification_tmp'])
            && isset($profile->data['identity_verification_tmp']['vid'])) {

            DB::table('absher_otps')->where('id', $profile->data['identity_verification_tmp']['vid'])->delete();
        }

        if (!isset($profile->data['identity_verification_tmp'])
            || !isset($profile->data['identity_verification_tmp']['identity'])
            || !isset($profile->data['identity_verification_tmp']['identity']['nin'])) {

            $response['status'] = false;
            $response['nin'] = null;
            return $response;
        }

        $nin = $profile->data['identity_verification_tmp']['identity']['nin'];

        $verificationMsg = $this->absherOtpService->createOtp($user, null, null, $nin);

        $profileData = $profile->data;

        if (isset($profileData['identity_verification_tmp']['vid'])) {
            $profileData['identity_verification_tmp']['vid'] = $verificationMsg->id;
            $profile->data = $profileData;
            $profile->save();
        }

        $response['status'] = false;
        $response['vid'] = $verificationMsg->id;
        $response['nin'] = $nin;

        return $response;
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return \response()->json( [ 'message' => trans('auth.throttle_with_minute', [
            'seconds' => $seconds,
            'minutes' => ceil($seconds / 60),
        ])], 429);


    }

    public function logout(Request $request)
    {

        \auth('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }

}
