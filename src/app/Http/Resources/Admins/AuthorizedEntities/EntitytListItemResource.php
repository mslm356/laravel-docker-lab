<?php

namespace App\Http\Resources\Admins\AuthorizedEntities;

use App\Enums\AuthorizedEntityStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class EntitytListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cr_number' => $this->cr_number,
            'status' => [
                'description' => AuthorizedEntityStatus::getDescription($this->status),
                'value' => $this->status,
            ],
            'reviewer_name' => !$this->reviewer ? null : [
                'name' => $this->reviewer->full_name
            ],
        ];
    }
}
