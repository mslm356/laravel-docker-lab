<?php

namespace App\Jobs\Hyperpay;

use App\Enums\Notifications\NotificationTypes;
use App\Mail\Investors\ReminderForProfile;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Observers\TransactionObserver;
use App\Services\LogService;
use App\Services\Sdad\Services\CreateSingleInvoiceService;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class TransactionAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationServices;

    public $transactionId;
    /**
     * @var LogService
     */
    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($transactionId)
    {
        $this->transactionId = $transactionId;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $transaction = Transaction::where('id', $this->transactionId)
                ->select('id', 'payable_type', 'confirmed', 'payable_id', 'meta', 'wallet_id', 'amount', 'updated_at')
                ->first();

            if (!$transaction) {
                $this->logService->log('HYPER-PAY-ALERT-TRANSACTION-NOT-VALID', null, ['ALERT', $transaction]);
                return ;
            }

            $transactionObserverObj = new TransactionObserver();

            $transactionObserverObj->created($transaction);

        } catch (\Exception $e) {
            $this->logService->log('HYPER-PAY-ALERT-EXC-JOB', $e);
        }
    }


}
