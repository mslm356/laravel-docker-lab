<?php

namespace App\Jobs\Zoho;

use App\Support\Zoho\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateItem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $zohoItem;
    public $listing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listing)
    {
        $this->zohoItem = new Item();
        $this->listing = $listing;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->zohoItem->update($this->listing);
    }
}
