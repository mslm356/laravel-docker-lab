<?php

namespace App\Jobs\Listings;

use App\Enums\Notifications\NotificationTypes;
use App\Models\Users\User;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Vote implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationServices;

    public $listing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listing)
    {
        $this->listing = $listing;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        User::query()
            ->join('listing_investor', function ($join) {
                $join->on('users.id', '=', 'listing_investor.investor_id')
                    ->where('listing_investor.listing_id', $this->listing->id)
                    ->where('listing_investor.invested_shares', '>', 0);
            })
            ->select('users.id')
            ->lazyById()
            ->each(function ($user) {
                $this->send(NotificationTypes::Fund, $this->notificationData($this->listing), $user->id);
            });
    }

    public function notificationData($listing)
    {
        return [
            'action_id' => $listing->id,
            'channel' => 'mobile',
            'title_en' => 'New Vote',
            'title_ar' => 'تصويت جديد',
            'content_en' => 'New vote created for '. $listing->title_en,
            'content_ar' => '  تم انشاء تصويت جديد للفرصة ' . $listing->title_ar,
        ];
    }
}
