<?php

use App\Http\Controllers\MobileApp\Investors\Auth\RegisterController;
use App\Http\Controllers\MobileApp\Investors\Auth\LoginController;
use App\Enums\Role;
use App\Http\Controllers\MobileApp\Investors\Auth\KycController;
use App\Http\Controllers\MobileApp\Investors\Auth\AbsherVerifyController;
use \App\Http\Controllers\MobileApp\Investors\Auth\InvestorProfileController;
use App\Http\Controllers\MobileApp\Investors\ContactUs\ContactUsController;


Route::post('/contact-us', [ContactUsController::class, 'store']);
Route::get('/contact-reasons', [ContactUsController::class, 'index']);

