<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class CheckIfInvestorHasCompletedKyc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $investor = $request->user()->investor;

        if (is_null($investor) || $investor->is_kyc_completed === false) {
            throw new AuthorizationException();
        }

        return $next($request);
    }
}
