<?php

namespace App\Models;

use App\Casts\LocalizedAttribute;

class PropertyType extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'name' => LocalizedAttribute::class,
        'is_active' => 'boolean'
    ];
}
