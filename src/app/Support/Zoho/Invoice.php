<?php


namespace App\Support\Zoho;


use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\ZohoPayment;
use App\Models\ZohoSettings;
use App\Settings\InvestingSettings;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Invoice
{
    public $base_url;
    public $organization_id;
    public $auth;
    public $customerPayments;

    public function __construct()
    {
        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
        $this->customerPayments = new CustomerPayments();
    }

    public function prepareInvoiceData($listing, $user, $quantity)
    {

        $tax = ZohoSettings::where('key', 'Standard Rate')->first();

        $data = [
            'customer_id' => $user->zoho_id,
            'line_items' => [
                [
                    'item_id' => $listing->zoho_item_id,
                    'quantity' => $quantity,
                    'tax_id' => $tax ? $tax->value : '',
                ]
            ],
        ];

        return $data;
    }

    public function store($listing, $user, $quantity)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->prepareInvoiceData($listing, $user, $quantity);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'invoices?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                $this->markAsSent($responseData['invoice']['invoice_id']);
                $this->payInvoice($user, $responseData['invoice']['invoice_id'], $responseData['invoice']['total']);
                return $responseData['invoice']['invoice_id'];
            }

            Log::error('ZOHO-INVOICE', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-INVOICE', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return null;
    }

    public function markAsSent($invoice_id)
    {

        $token = $this->auth->getToken();

        try {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'invoices/' . $invoice_id . '/status/sent?organization_id=' . $this->organization_id);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] != 0) {
                Log::error('ZOHO-INVOICE-MARK-SENT', [$responseData]);
            }

        } catch (\Exception $e) {
            Log::error('ZOHO-INVOICE-MARK-SENT', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }

    public function payInvoice($user, $invoice_id, $amount)
    {
        try {

            $customerPayments = ZohoPayment::where('user_id', $user->id)->where('amount', '!=', 0)->get();

            $customerPaymentsTotalAmount = $customerPayments->sum('amount');

            if ($customerPaymentsTotalAmount < $amount) {
                Log::error('ZOHO-INVOICE-PAY',
                    [
                        'user_id' => $user->id,
                        'message' => 'customer balance less than required',
                        'amount' => $amount,
                        'invoice_id' => $invoice_id
                    ]);

                return true;
            }

            $payments = $this->getUserPayments($customerPayments, $amount);

            foreach ($payments as $payment) {
                $this->applyCredit($user, $invoice_id, $payment['payment_id'], $payment['amount']);
            }

        } catch (\Exception $e) {
            Log::error('ZOHO-INVOICE-PAY', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }
    }

    public function getUserPayments($customerPayments, $amount)
    {

        $payments = [];

        foreach ($customerPayments as $index => $customerPayment) {

            if ($customerPayment->amount <= $amount) {

                $payments[$index]['payment_id'] = $customerPayment->payment_id;
                $payments[$index]['amount'] = round($customerPayment->amount, 3);

                $amount -= $customerPayment->amount;

            } else {

                $payments[$index]['payment_id'] = $customerPayment->payment_id;
                $payments[$index]['amount'] = $amount;

                $amount -= $amount;
            }

            if ($amount == 0) {
                break;
            }
        }

        return $payments;
    }

    public function preparePayInvoiceData($payment_id, $amount)
    {
        $data = [

            'invoice_payments' =>
                [
                    [
                        'payment_id' => $payment_id,
                        'amount_applied' => $amount,
                    ]
                ]
        ];

        return $data;
    }

    public function applyCredit($user, $invoice_id, $payment_id, $amount)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->preparePayInvoiceData($payment_id, $amount);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'invoices/' . $invoice_id . '/credits?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                $zohoPayment = ZohoPayment::where('user_id', $user->id)->where('payment_id', $payment_id)->first();

                $zohoPayment->amount -= $amount;
                $zohoPayment->save();
            }

            Log::error('ZOHO-INVOICE-PAY', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-INVOICE-PAY', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }
}
