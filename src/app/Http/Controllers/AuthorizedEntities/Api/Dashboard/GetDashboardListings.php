<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Dashboard;

use App\Actions\Listings\GetMyListingsInSummary;
use App\Http\Controllers\Controller;

class GetDashboardListings extends Controller
{
    /**
     * Store the discussion in the storage
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        $listings = GetMyListingsInSummary::run(
            function ($query) {
                return $query->where('authorized_entity_id', authorizedEntityId());
            }
        );

        return response()->json([
            'data' => $listings,
        ]);
    }
}
