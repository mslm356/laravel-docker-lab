@extends('emails.partials.main')
@section('content')
    <tr> {{ __('emails/complaint/close-email.dear', ['name' => $complaint->user->first_name], $complaint->user->locale) }} </tr>
    <br>
    <tr> <p> {{$complaint->description}} </p> </tr>
    <tr> <p> {{$complaint->reviewer_comment}} </p> </tr>
    <tr> {{ __('emails/complaint/close-email.message', ['ticketNumber' => $complaint->id], $complaint->user->locale) }} </tr>
    <br>
    <tr> {{ __('emails/common.thanks', [], $complaint->user->locale) }} </tr>
    <br>
@endsection


