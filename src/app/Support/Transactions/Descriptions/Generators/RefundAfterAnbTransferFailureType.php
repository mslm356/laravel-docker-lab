<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Enums\Banking\BankCode;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorInterface;
use Illuminate\Support\Arr;

class RefundAfterAnbTransferFailureType implements GeneratorInterface
{
    /**
     * Generate description
     *
     * @return string
     */
    public function generate(Transaction $transaction, $locale = null): string
    {
        $items = Arr::only($transaction->meta, ['from_iban', 'from_bank_code']);

        if (count($items) < 2) {
            return __('transaction-description.refund_after_ext_tranfer_failure_in_general');
        }

        $bankName = BankCode::getDescription($items['from_bank_code']);
        $fromIban = $items['from_iban'];

        return __('transaction-description.refund_after_ext_tranfer_failure', ['bank' => $bankName, 'iban' => $fromIban], $locale);
    }
}
