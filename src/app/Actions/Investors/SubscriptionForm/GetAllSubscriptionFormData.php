<?php

namespace App\Actions\Investors\SubscriptionForm;


use App\Models\Listings\ListingInvestor;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\SubscriptionFormRequest;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Http\Resources\Common\Listings\ListingInvestorResourceSubscriptionForm;

class GetAllSubscriptionFormData
{
    use AsAction;

//    /**
//     * Undocumented function
//     *
//     * @param \App\Models\Users\User $user
//     * @param \App\Models\Listings\Listing $listing
//     * @param \Cknow\Money\Money $amount
//     * @param \Illuminate\Support\Collection $currentInvestments
//     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
//     */
    public function handle(User $user, $sumOfInvestedAmount, $subscriptionFormRequest, $wantsJson = false)
    {
        $userWallet = DB::table('wallets')
            ->where('holder_id', $user->id)
            ->where('holder_type', User::class)
            ->select('balance')
            ->first();

        $subscriptionDate =  $subscriptionFormRequest ? $subscriptionFormRequest->created_at->timezone('Asia/Riyadh')->format('Y-m-d H:i:s') : now('Asia/Riyadh')->format('Y-m-d');

        $listingInvestorRecords = DB::table('listing_investor_records')
            ->join('listings', 'listings.id', '=', 'listing_investor_records.listing_id')
            ->join('users', 'users.id', '=', 'listing_investor_records.investor_id')
            ->where('listing_investor_records.investor_id', $user->id)
            ->where('listing_investor_records.invested_shares', '!=', 0)
            ->where('listing_investor_records.created_at','<=', $subscriptionDate)
            ->select('listings.id' , 'listings.title_en', 'listings.title_ar',
                DB::raw('SUM(listing_investor_records.invested_shares) As invested_shares') ,
                DB::raw('SUM(listing_investor_records.amount_without_fees)/100 As totalAmount') ,
                DB::raw('MIN(listing_investor_records.created_at) As created_at'))
            ->groupBy('listing_investor_records.listing_id'/*,'listing_investor_records.created_at'*/)
            ->get();


        return [
            'value_date' => now('Asia/Riyadh')->format('Y-m-d'),
            'value_time' => $subscriptionFormRequest ? $subscriptionFormRequest->created_at->timezone('Asia/Riyadh')->format('H:i:s') : now('Asia/Riyadh')->format('H:i:s'),
            'send_request_date' => $subscriptionFormRequest ? $subscriptionFormRequest->created_at->timezone('Asia/Riyadh')->format('Y-m-d H:i:s A') :  now('Asia/Riyadh')->format('H:i:s'),
            //'is_new_investor' => $currentInvestments === 0,
            'investor_id' => $user->id,
            'investor_type' => $user->type == 'company' ? 'company' : 'individual',
//<<<<<<< HEAD
            'investor_name_en' => $user->getAbsherName('ar'),
            'investor_name_ar' => $user->first_name . "  " . $user->last_name,
            'investor_national_id' => $user->type == 'company' ? $user->c_number : $user->nationalIdentity->nin ?? '',
            'nationality' => $user->nationalIdentity->nationality ?? '',
            'wallet' => $userWallet ? ($userWallet->balance / 100) : 0.00,
            'listingInvestors' => $wantsJson ? $listingInvestorRecords->toArray() : $listingInvestorRecords,
//=======
//            'investor_name_en' => $user->getAbsherName('en'),
//            'investor_name_ar' => $user->getAbsherName('ar'),
//            'investor_national_id' => $user->type == 'company' ? $user->c_number : $user->nationalIdentity->nin ?? '',
//            'nationality' => $user->nationalIdentity->nationality ?? '',
//            'wallet' => $userWallet ? $userWallet->balance : 0.00,
//            'listingInvestors' => $wantsJson ? ListingInvestorResourceSubscriptionForm::collection($listingInvestors) : $listingInvestors,
//>>>>>>> 1ba84d0670295a20dd1083b7e9ff4a568fb988d8
            'userInvestments' => $sumOfInvestedAmount
        ];

    }

    public function asController($user, $listingInvestors, $subscriptionFormRequest)
    {
        //        $user = $request->user('sanctum');
        return response()->json(['data' => $this->handle($user, $listingInvestors, $subscriptionFormRequest)]);
    }

    //    public function authorize(ActionRequest $request)
    //    {
    //        $token = $request->route('investor_subscription_token');
    //        return $request->user()->id === $token->user_id && !$token->expired_at;
    //    }
}
