<?php

namespace App\Models\Transfer;

use App\Models\AppModel;
use App\Models\BankAccount;
use App\Models\Users\User;

class TransferRequest extends AppModel
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function bank()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }

    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id');
    }

}
