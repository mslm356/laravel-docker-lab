<?php


namespace App\Support\Elm\Natheer;

use App\Services\LogService;
use Illuminate\Support\Facades\Http;

class NatheerWatchListService
{
    private $url;
    private $logServices;
    public function __construct()
    {
        $this->logServices = new LogService();
    }

    public function addPerson($nin, $date_of_birth)
    {
        $this->logServices->log('NATHEER-ADD-SINGLE-PERSON-INIT');

        try {
            $data=['person'=>['personId'=>$nin,'dateOfBirth'=>$date_of_birth]];

            $responce= Http::withHeaders([
                'username' => config('natheer.username'),
                'password' => config('natheer.password'),
            ])->post(config('natheer.url').'/add', $data);

            return $responce['result'];

        } catch (\Exception $e) {
            $this->logServices->log('NATHEER-ADD-SINGLE-PERSON-EXCEPTION', $e);
            throw new \Exception($e->getMessage());
        }
    }

    public function addBulkPerson($data)
    {
        $this->logServices->log('NATHEER-ADD-BULK-PERSON-INIT');

        try {
            $responce= Http::withHeaders([
                'username' => config('natheer.username'),
                'password' => config('natheer.password'),
            ])->post('https://natheer.elm.sa/natheer-services/watchlist/person/bulk/add', $data);

            return $responce->json();

        } catch (\Exception $e) {
            $this->logServices->log('NATHEER-ADD-BULK-PERSON-EXCEPTION', $e);
            echo $e->getMessage()."\n";
            throw new \Exception($e->getMessage());
        }

    }

    public function findPerson($nin)
    {
        try {
            $responce= Http::withHeaders([
                'username' => config('natheer.username'),
                'password' => config('natheer.password'),
            ])->post(config('natheer.url').'/find', ['person'=>['personId'=>$nin]]);

            return $responce['result'];
        } catch (\Exception $e) {
            $this->logServices->log('NATHEER-FIND-SINGLE-PERSON-EXCEPTION', $e);
        }
    }

    public function findBulkPerson($data)
    {
        try {
            $responce= Http::withHeaders([
                'username' => config('natheer.username'),
                'password' => config('natheer.password'),
            ])->post(config('natheer.url').'/bulk/find', $data);

            return $responce['result'];

        } catch (\Exception $e) {
            $this->logServices->log('NATHEER-FIND-BULK-PERSON-EXCEPTION', $e);
        }
    }
}
