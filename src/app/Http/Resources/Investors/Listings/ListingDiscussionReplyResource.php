<?php

namespace App\Http\Resources\Investors\Listings;

use Illuminate\Http\Resources\Json\JsonResource;

class ListingDiscussionReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->replier_name,
            'role' => $this->replier_role,
            'text' => $this->text,
        ];
    }
}
