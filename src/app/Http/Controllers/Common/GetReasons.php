<?php

namespace App\Http\Controllers\Common;

use App\Models\ContactReason;
use App\Http\Controllers\Controller;

class GetReasons extends Controller
{
    /**
     * Get contact reasons
     *
     * @param
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function __invoke()
    {
        return response()->json([
            'data' => ContactReason::all()->map(fn ($reason) => [
                'id' => $reason->id,
                'name' => $reason->name
            ])
        ]);
    }
}
