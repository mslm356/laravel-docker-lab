<?php

namespace App\Settings\Casters;

use Spatie\LaravelSettings\SettingsCasts\SettingsCast;

class EloquentCaster implements SettingsCast
{
    private string $modelClass;

    public function __construct(?string $modelClass)
    {
        $this->modelClass = $modelClass;
    }

    public function get($payload)
    {
        return $this->modelClass::find($payload);
    }

    public function set($payload)
    {
        return $payload->getKey();
    }
}
