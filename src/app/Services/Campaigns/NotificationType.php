<?php

namespace App\Services\Campaigns;

use App\Enums\Campaigns\CampaignStatus;
use App\Enums\Notifications\NotificationTypes;
use App\Jobs\Campaigns\SetUsers;
use App\Services\Campaigns\InvestorTypes\ManageData;
use App\Traits\NotificationServices;

class NotificationType
{
    use NotificationServices;

    public $data;
    public $campaign;

    public function __construct($data, $campaign)
    {
        $this->data = $data;
        $this->campaign = $campaign;
    }

    public function handle()
    {
        $this->saveCampaignUsers();
    }

    public function saveCampaignUsers()
    {
        $setUsersJob = (new SetUsers($this->data, $this->campaign, 'Notification'));
        dispatch($setUsersJob)->onQueue('campaigns');
    }

    public function sendCampaign($dataUsedToSend, $campaign)
    {

        $varsInText = $dataUsedToSend['varsInText'];

        foreach ($dataUsedToSend['user_data'] as $investor_data) {
            $this->send(NotificationTypes::Fund, $this->notificationData($investor_data, $campaign, $varsInText), $investor_data['id']);
        }

        $this->updateCampaign($campaign);
    }

    public function manageInvestorsData()
    {
        $selectedColumns = ['users.id'];
        $manageData = new ManageData();
        return $manageData->manageData($this->data, $selectedColumns);
    }

    public function handleData()
    {
        $data = $this->manageInvestorsData();

        return [
            'investor_data' => $data['investorData'],
            'variablesInText' => isset($data['extraSelectedColumn']) ? $data['extraSelectedColumn'] : [],
        ];
    }

    public function notificationData($investorData, $campaign, $varsInText)
    {
        return [
            'action_id'=> $investorData['id'],
            'channel'=>'mobile',
            'title_en'=> $campaign->title_en,
            'title_ar'=> $campaign->title_ar,
            'content_en'=> $this->replaceVarsInText($campaign->description_en, $varsInText, $investorData),
            'content_ar'=> $this->replaceVarsInText($campaign->description_ar, $varsInText, $investorData),
        ];
    }

    public function updateCampaign($campaign)
    {
        $campaign->status = CampaignStatus::Ended;
        $campaign->save();
    }

    public function replaceVarsInText($text, $variablesInText, $investorData)
    {
        foreach ($variablesInText as $variable) {

            if (strpos($text, '{{'.$variable.'}}') && isset($investorData[$variable])) {
                $text = str_replace('{{'.$variable.'}}', $investorData[$variable], $text);
            }
        }

        return $text;
    }

}
