<?php

namespace App\Services\Campaigns\InvestorTypes;

use Illuminate\Support\Facades\DB;

class InCompletedKycType
{

    public function getUsersData(array $selectedData, array $customData = [])
    {
        $data =  DB::table('investor_profiles')
            ->where('is_kyc_completed', 0)
            ->join('users', 'users.id', '=', 'investor_profiles.user_id')
            ->select($selectedData)
            ->orderBy('investor_profiles.user_id', 'asc')
            ->get();

        return json_decode($data, 1);
    }

}
