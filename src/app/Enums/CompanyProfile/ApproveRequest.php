<?php

namespace App\Enums\CompanyProfile;

use BenSampo\Enum\Enum;

final class ApproveRequest extends Enum
{
    const Pending = 1;
    const InReview = 2;
    const Approved = 3;
    const Rejected = 4;
    const Suspended = 5;
    const KycNotCompleted = 6;
    const AutoAmlCheckFailed = 7;
}
