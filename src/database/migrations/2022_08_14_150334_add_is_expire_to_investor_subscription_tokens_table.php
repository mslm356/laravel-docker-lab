<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsExpireToInvestorSubscriptionTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_subscription_tokens', function (Blueprint $table) {
            $table->boolean('is_expire')->default(true)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_subscription_tokens', function (Blueprint $table) {
            $table->dropColumn('is_expire');
        });
    }
}
