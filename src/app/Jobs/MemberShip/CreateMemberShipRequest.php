<?php

namespace App\Jobs\MemberShip;

use App\Services\LogService;
use App\Services\MemberShip\CreateRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class CreateMemberShipRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels ;

    public $logService;
    public $memberShipService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->logService = new LogService();
        $this->memberShipService = new CreateRequest();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->memberShipService->autoRequest();
        } catch (\Exception $e) {
            $this->logService->log('AUTO-MEMBER-SHIP-JOB-EXC', $e);
        }
    }
}
