<?php

namespace App\Support\Aml;

use App\Enums\AML\AmlList;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class USList implements AmlListInterface
{
    protected $SDNList = null;
    protected $NonSDNList = null;

    protected static $fileNames = [
        'sdn' => 'aml-lists/us-sdn-list.xml',
        'non_sdn' => 'aml-lists/us-non-sdn-list.xml',
    ];


    /**
     * Get the list
     *
     * @return \SimpleXMLElement
     */
    public function getSDNList()
    {
        if ($this->SDNList) {
            return $this->SDNList;
        }

        if (Storage::disk('local')->exists(static::$fileNames['sdn'])) {
            $list = Storage::disk('local')->get(static::$fileNames['sdn']);
        } elseif (!($list = Cache::get('us-list-sdn'))) {
            $res = Http::get('https://www.treasury.gov/ofac/downloads/sdn.xml');
            Cache::put('us-list-sdn', $list = $res->body(), now()->addDay());
        }

        $this->SDNList = new \SimpleXMLElement($list);

        return $this->SDNList;
    }

    /**
     * Get the list
     *
     * @return \SimpleXMLElement
     */
    public function getNonSDNList()
    {
        if ($this->NonSDNList) {
            return $this->NonSDNList;
        }

        if (Storage::disk('local')->exists(static::$fileNames['non_sdn'])) {
            $list = Storage::disk('local')->get(static::$fileNames['non_sdn']);
        } elseif (!($list = Cache::get('us-list-non-sdn'))) {
            $res = Http::get('https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml');
            Cache::put('us-list-non-sdn', $list = $res->body(), now()->addDay());
        }

        $this->NonSDNList =  new \SimpleXMLElement($list);

        return $this->NonSDNList;
    }

    /**
     * Check the infomation in the list
     *
     * @param string|null $firstName
     * @param string|null $secondName
     * @param string|null $thirdName
     * @param string|null $fourthName
     * @param string|null $nationality
     * @param string|null $dob
     * @param string|null $nationalId
     * @param string|null $passportId
     * @return array
     */
    public function check($firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId)
    {
        $suspected = [];

        foreach ($this->getNonSDNList()->sdnEntry as $i) {
            $this->checkIfSuspected($suspected, $i, $firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId);
        }

        foreach ($this->getSDNList()->sdnEntry as $i) {
            $this->checkIfSuspected($suspected, $i, $firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId);
        }

        return $suspected;
    }

    /**
     * Check if the user is suspected
     *
     * @param array $list
     * @param \SimpleXMLElement $i
     * @param string|null $firstName
     * @param string|null $secondName
     * @param string|null $thirdName
     * @param string|null $fourthName
     * @param string|null $nationality
     * @param string|null $dob
     * @param string|null $nationalId
     * @param string|null $passportId
     * @return void
     */
    public function checkIfSuspected(&$list, $i, $firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId)
    {
        $info = $this->transformIndividualInfo($i);

        $isSuspected = false;

        foreach ($info['docs'] ?? [] as $doc) {
            if ($nationalId && stripos($doc['number'], $nationalId) !== false) {
                $isSuspected = true;
                break;
            } elseif ($passportId && stripos($doc['number'], $passportId) !== false) {
                $isSuspected = true;
                break;
            }
        }

        if ($isSuspected) {
            $list[] = $this->standardize($info);
            return;
        }

        $nameChecks = $this->checkNameIfExists($info['fullName'], [
            $firstName, $secondName, $thirdName, $fourthName
        ]);

        if ($nameChecks > 1) {
            $list[] = $this->standardize($info);
            return;
        }

        foreach ($info['aliases'] ?? [] as $alias) {
            $nameChecks = $this->checkNameIfExists($alias['fullName'], [
                $firstName, $secondName, $thirdName, $fourthName
            ]);

            if ($nameChecks > 1) {
                $list[] = $this->standardize($info);
                continue;
            }
        }
    }

    /**
     * Transform the user information into a suitable format
     *
     * @param \SimpleXMLElement $i
     * @return array
     */
    public function transformIndividualInfo($i)
    {
        $info = [
            'id' => (string)$i->uid
        ];

        $FULL_NAME = '';

        $FIRST_NAME = (string)$i->firstName;
        $SEC_NAME = (string)$i->lastName;

        if ($FIRST_NAME) {
            $info['firstName'] = $FIRST_NAME;
            $FULL_NAME .=  (' ' . $FIRST_NAME);
        }
        if ($SEC_NAME) {
            $info['secondName'] = $SEC_NAME;
            $FULL_NAME .=  (' ' . $SEC_NAME);
        }

        $info['fullName'] = $FULL_NAME;

        $aliases = [];

        // aliases
        if (isset($i->akaList->aka)) {
            foreach ($i->akaList->aka as $alias) {
                $aliasInfo = [];
                $lastName = (string)$alias->lastName;
                $firstName = (string)$alias->firstName;

                $fullName = '';

                if ($firstName) {
                    $fullName .= $firstName;
                    $aliasInfo['firstName'] = $firstName;
                }

                if ($lastName) {
                    $fullName .= (' ' . $lastName);
                    $aliasInfo['lastName'] = $lastName;
                }

                $aliasInfo['fullName'] = $fullName;

                $aliases[] = $aliasInfo;
            }
        }

        if (!empty($aliases)) {
            $info['aliases'] = $aliases;
        }

        // date of births
        if (isset($i->dateOfBirthList)) {
            foreach ($i->dateOfBirthList as $dob) {
                $date = (string)$dob->dateOfBirthItem->dateOfBirth;
                $info['dob'][] = $date;
            }
        }

        // Passport/National IDs
        if (isset($i->idList)) {
            foreach ($i->idList as $doc) {
                $type = (string)$doc->id->idType;
                $number = (string)$doc->id->idNumber;

                if ($type && $number) {
                    $info['docs'][] = [
                        'type' => $type,
                        'number' => $number
                    ];
                }
            }
        }

        return $info;
    }

    /**
     * Check number of matching of names in the full/alias name
     *
     * @param string $fullName
     * @param array $names
     * @return int
     */
    public function checkNameIfExists($fullName, $names)
    {
        $checks = 0;

        foreach ($names as $name) {
            if ($name && stripos($fullName, $name) !== false) {
                $checks++;
            }
        }

        return $checks;
    }

    /**
     * get List index
     *
     * @return int
     */
    public function getListIndex(): int
    {
        return AmlList::US;
    }

    /**
     * Standrize the data between the lists
     *
     * @param array $info
     * @return array
     */
    public function standardize($info)
    {
        try {
            $sInfo = [
                'first_name' => '',
                'second_name' => '',
                'third_name' => '',
                'fourth_name' => '',
                'full_name' => $info['fullName'],
                'aliases' => [],
                'dob' => [],
                'docs' => $info['docs'] ?? [],
            ];

            if (isset($info['aliases'])) {
                $sInfo['aliases'] = array_map(function ($item) {
                    return $item['fullName'];
                }, $info['aliases']);
            }

            if (isset($info['dob'])) {
                $sInfo['dob'] = array_map(function ($item) {
                    return [
                        'type' => 'APPROX',
                        'date' => $item,
                        'year' => null
                    ];
                }, $info['dob']);
            }

            return $sInfo;
        } catch (\Throwable $th) {
            throw new \Exception('unable to standrize user: '.json_encode($info), 0, $th);
        }
    }
}
