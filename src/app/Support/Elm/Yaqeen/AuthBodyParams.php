<?php

namespace App\Support\Elm\Yaqeen;

class AuthBodyParams
{
    public static function get()
    {
        return [
            'userName' => config('services.yaqeen.user_name'),
            'password' => config('services.yaqeen.password'),
            'chargeCode' => config('services.yaqeen.charge_code'),
        ];
    }
}
