<?php

return [
    'subject' => 'Account statement issued',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'We have issued your account statement',
    'message_2' => 'Please see the attachment.',
    'link_text' => 'Check your profile',
    'message_3' => ' Thanks,',
    'message_4' => ' Aseel Financial ',
];
