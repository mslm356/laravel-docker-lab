<?php

namespace App\Actions\AuthorizedEntities;

use App\Models\File;
use App\Enums\FileType;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Support\Arr;

class UpdateEntity
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param \App\Models\AuthorizedEntity $entity
     * @param array $inputs
     * @return void
     */
    public function handle($entity, $data)
    {
        $entity->update(
            Arr::only($data, ['name', 'cr_number', 'description'])
        );

        foreach (Arr::get($data, 'cr', []) as $file) {
            File::upload($file, "authorized-entities/crs/{$entity->id}", [
                'fileable_id' => $entity->id,
                'fileable_type' => $entity->getMorphClass(),
                'type' => FileType::AuthorizedEntityCR,
            ]);
        }

        if ($cmaFile = Arr::get($data, 'cma_license')) {
            $cmaLicenseFile = File::upload($cmaFile, "authorized-entities/cma-license/{$entity->id}", [
                'fileable_id' => $entity->id,
                'fileable_type' => $entity->getMorphClass(),
                'type' => FileType::AuthorizedEntityCMALicense,
            ]);

            $entity->files()->where([
                ['type', FileType::AuthorizedEntityCMALicense],
                ['id', '!=', $cmaLicenseFile->id]
            ])->delete();
        }

        return $entity;
    }
}
