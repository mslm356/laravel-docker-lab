<?php

namespace App\Actions\Listings\Discussions;

use App\Enums\Listings\DiscussionStatus;
use Illuminate\Support\Arr;
use Lorisleiva\Actions\Concerns\AsAction;

class StoreReply
{
    use AsAction;

    public function handle($discussion, $data)
    {
        $reply = $discussion->replies()->create(array_merge(
            Arr::only($data, ['user_id', 'user_role_id', 'replier_role', 'replier_name', 'text']),
            [
                'listing_id' => $discussion->listing_id,
                'status' => DiscussionStatus::Published
            ]
        ));

        $reply->parent()->update([
            'status' => DiscussionStatus::Published
        ]);

        return $reply;
    }
}
