<?php

namespace App\Mail\Investors;

use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CancelInvestment extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $amount;
    public $iban;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $amount, $iban)
    {
        $this->user = $user;
        $this->amount = $amount;
        $this->iban = $iban;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/app/overview';

        $iban = $this->iban;

        $time = Carbon::parse('2022-08-26 08:00:00');

        $name = $this->user ? $this->user->full_name : '---';

        $amount = $this->amount;

        return $this->view('emails.investors.deposit', compact('name', 'iban', "time", 'profileUrl', 'amount'))
            ->subject(__('emails/investors/deposit.subject'));

    }
}
