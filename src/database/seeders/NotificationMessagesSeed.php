<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NotificationMessagesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'key' => 'Update_Fund',
                'title_ar' => 'تعديل فرصة',
                'title_en' => 'Update Fund',
                'content_ar' => 'تعديل فرصة',
                'content_en' => 'Update Fund',
            ],
        ];

        foreach ($rows as $row) {
            \App\Models\NotificationMessage::firstOrCreate($row);
        }
    }
}
