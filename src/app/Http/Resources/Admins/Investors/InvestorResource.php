<?php

namespace App\Http\Resources\Admins\Investors;

use App\Enums\CompanyProfile\ApproveRequest;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Investors\Registration\CurrentInvestment;
use App\Enums\Investors\Registration\Education;
use App\Enums\Investors\Registration\InvestmentObjective;
use App\Enums\Investors\Registration\InvestorEntity;
use App\Enums\Investors\Registration\Level;
use App\Enums\Investors\Registration\MoneyRange;
use App\Enums\Investors\Registration\NetWorth;
use App\Enums\Investors\Registration\Occupation;
use App\Http\Resources\Admins\Elm\AbsherInfoResource;
use App\Http\Resources\Investors\CompanyProfileResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class InvestorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'type' => $this->type,
            'id_expiry_date' => optional($this->nationalIdentity)->id_expiry_date,
            'status' => [
                'value' => $this->type == 'company' ? optional($this->companyProfile)->status : optional($this->investor)->status,
                'description' => $this->type == 'company' ? ApproveRequest::getDescription(optional($this->companyProfile)->status) : InvestorStatus::getDescription(optional($this->investor)->status),
            ],
            'profile' => $this->getInvestorProfile(),
            'company_profile' => $this->type == 'company' && $this->companyProfile ? new CompanyProfileResource($this->companyProfile) : null,
            'absher' => new AbsherInfoResource($this->resource),
            'is_kyc_completed' => $this->investor->is_kyc_completed
        ];
    }

    public function getInvestorProfile()
    {
        if (!$this->investor || $this->investor->is_kyc_completed === false) {
            return null;
        } else {
            $investor = $this->investor;
            return [
                'education' => Education::getDescription($investor->education_level),
                'occupation' => Occupation::getDescription($investor->occupation),
                'investment_objectives' => array_map(
                    fn ($objective) => InvestmentObjective::getDescription($objective),
                    $investor->invest_objective
                ),
                'current_investements' => array_map(
                    fn ($current) => CurrentInvestment::getDescription($current),
                    $investor->current_invest
                ),
                'net_worth' => NetWorth::getDescription($investor->net_worth),
                'annual_income' => MoneyRange::getDescription($investor->annual_income),
                'expected_invest_amount_per_opportunity' => MoneyRange::getDescription(
                    $investor->expected_invest_amount_per_opportunity
                ),
                'expected_annual_invest' => MoneyRange::getDescription(
                    $investor->expected_annual_invest
                ),
                'is_on_board' => $investor->is_on_board,
                'entity_type' => InvestorEntity::getDescription(
                    Arr::get($investor->data, 'is_individual')
                ),
                'any_xp_in_fin_sector' => (bool)Arr::get($investor->data, 'any_xp_in_fin_sector'),
                'any_xp_related_to_fin_sector' => (bool)Arr::get($investor->data, 'any_xp_related_to_fin_sector'),
                'have_you_invested_in_real_estates' => (bool)Arr::get($investor->data, 'have_you_invested_in_real_estates'),
                'investment_xp' => Level::getDescription(
                    Arr::get($investor->data, 'investment_xp')
                ),
                'years_of_inv_in_securities' =>  Arr::get($investor->data, 'years_of_inv_in_securities'),

                'have_you_invested_in_real_estates' => Arr::get($investor->data, 'have_you_invested_in_real_estates'),
                'is_entrusted_in_public_functions' => Arr::get($investor->data, 'is_entrusted_in_public_functions'),
                'have_relationship_with_person_in_public_functions' => Arr::get($investor->data, 'have_relationship_with_person_in_public_functions'),
                'is_beneficial_owner' => $isOwner = Arr::get($investor->data, 'is_beneficial_owner'),
                'identity_of_beneficial_owner' => $this->when(
                    $isOwner === false,
                    fn () => Arr::get($investor->data, 'identity_of_beneficial_owner')
                ),
                'extra_financial_information' => Arr::get($investor->data, 'extra_financial_information'),
            ];
        }
    }
}
