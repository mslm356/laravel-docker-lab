<?php

namespace App\Console\Commands\Transfer;

use App\Enums\Role;
use App\Enums\Transfer\TransferStatus;
use App\Exceptions\FailedProcessException;
use App\Exceptions\NoBalanceException;
use App\Models\Transfer\TransferRequest;
use App\Models\Users\User;
use App\Services\Investors\TransferServices;
use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Validation\ValidationException;

class ApproveWithdrawOperations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approve:withdraw-operations {--admin_email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    public $count=0;
    public $transfer_id = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public $transferServices;
    public $logService;

    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
        $this->transferServices = new TransferServices();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $auth = User::role(Role::Admin)
                ->where('email', $this->option('admin_email'))
                ->first();

            if (!$auth) {
                dd('This User Not Found');
            }

            if (!$auth->can('transfer_accept')) {
                dd(__('not of your permissions'));
            }

            $transferRequests = TransferRequest::query()
                ->where('created_at', '<=', Carbon::now()->subMinute()->toDateTimeString())
                ->where('status', TransferStatus::Pending)
                ->get();

            foreach ($transferRequests as $transferRequest) {

                sleep(4);

                dump($this->count++ . ' req_id: ' , $transferRequest->id);
                $this->transfer_id = $transferRequest->id;

                 $this->transfer_id = $transferRequest->id;

                $client = $transferRequest->user;

                $transferData = [
                    'bank_account_id' => $transferRequest->bank_account_id,
                    'fee' => $transferRequest->fee,
                    'amount' => \money($transferRequest->amount)->formatByDecimal(),
                ];

                try {

                    $transferServiceResponse = $this->transferServices->transferMoney($transferData, $client, 'admin', $transferRequest->id);

                    if (isset($transferServiceResponse['status']) && $transferServiceResponse['status'] && isset($transferServiceResponse['transfer_id'])) {

                        $transferRequest->update([
                            'status' => TransferStatus::Approved,
                            'reviewer_comment' => "accept all withdraw operations",
                            'reviewer_id' => $auth->id,
                        ]);

                    }else {
                        dump('transfer-failed and transfer request id is :'.' '.$transferRequest->id);
                    }

                } catch (NoBalanceException $e) {
                    dump('no_enough_balance');
                    continue;
                } catch (FailedProcessException $e) {
                    dump($e->getMessage());
                    continue;
                } catch (ValidationException $e) {
                    dump($e->getMessage());
                    continue;
                }
            }

            dump('Requests updated successfully');

        } catch (\Exception $e) {
            dump($e->getMessage(),$e->getFile(),$e->getLine());
        }
    }
}
