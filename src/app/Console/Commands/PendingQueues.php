<?php

namespace App\Console\Commands;

use App\Jobs\AbsherTest;
use App\Jobs\CheckUserAmlAfterRegistration;
use App\Mail\Investors\approveBankAccount;
use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\Mail;
use Redis;

class PendingQueues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'show:pending-queues {--ip=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $name = $this->option('ip');
            dd(\Queue::size($name));


//            for ($i = 0; $i<10;  $i++) {
//
//                AbsherTest::dispatch()->onQueue('absher-20.0.2.134');
//                dump(\Queue::size($this->option('ip')));
//            }
//
//            dd(1);

            //20.0.2.0134

            dd(Redis::keys('*'));

            Redis::llen('queues:' . $queueName);

            $queueName = 'default';

            dd(
                \Queue::getRedis()
                    ->connection($this->option('connection'))
                    ->zrange('queues:'.$queueName.':delayed', 0, -1)
            );



        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
