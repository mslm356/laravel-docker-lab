<?php

use App\Models\Accounting\Vat;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vats', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
            $table->string('percentage');
            $table->timestamps();
            $table->softDeletes();
        });

        Vat::create([
            'name' => 'VAT (15%)',
            'percentage' => "15.00"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vats');
    }
}
