<?php

namespace App\Jobs\Zoho;

use App\Support\Zoho\CustomerPayments;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CustomerPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $zohoCustomerPayments;
    public $user;
    public $amount;
    public $transaction;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $amount, $transaction = null)
    {
        $this->zohoCustomerPayments = new CustomerPayments();
        $this->user = $user;
        $this->amount = $amount;
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $payment_id = $this->zohoCustomerPayments->createCustomerPayment($this->user, $this->amount);

        if ($payment_id && $this->transaction) {
            $this->transaction->zoho_id = $payment_id;
            $this->transaction->save();
        }
    }
}
