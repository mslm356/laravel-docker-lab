<?php

return [
    'subject' => 'Request for account statement',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'We have received your request for account statement',
    'message_2' => ' and we will reply to you as soon as possible ',
    'message_3' => ' Thanks,',
    'message_4' => ' Aseel Financial ',
    'link_text' => 'Check your profile',
];
