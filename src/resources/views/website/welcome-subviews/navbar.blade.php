<div class="relative px-3 pb-10 pt-14 lg:px-0" x-data="{isMenuOpen: false}">
    <nav class="relative flex items-center justify-between mx-auto">
        <div class="flex items-center flex-1">
            <div class="flex items-center justify-between w-full lg:w-auto">
                <a href="#" aria-label="Home">
                    <img class="w-auto h-14 sm:h-16" src="{{ asset('images/aseel-white.png') }}" alt="Logo">
                </a>
                <div class="flex items-center -mr-2 lg:hidden">
                    <button @click="isMenuOpen = true" type="button"
                        class="inline-flex items-center justify-center p-2 text-gray-400 transition duration-150 ease-in-out rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500"
                        id="main-menu" aria-label="Main menu" aria-haspopup="true">
                        <svg class="w-6 h-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </button>
                </div>
                <div class="hidden ltr:ml-8 rtl:mr-8 lg:block">
                    @foreach ($websiteNavLinks as $navLink)
                        @if (in_array($navLink['id'], ['about_us', 'funds', 'how_it_works']))
                            <a href="{{ $navLink['link'] }}"
                                class="text-lg {{ $loop->first ? '' : 'ltr:ml-8 rtl:mr-8 ' }}font-medium text-gray-300 hover:text-white transition duration-150 ease-in-out">{{ $navLink['text'] }}</a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="hidden text-right lg:flex lg:items-center">
            <div class="hidden md:block md:ltr:mr-6 md:rtl:ml-6">
                @foreach ($websiteNavLinks as $navLink)
                    @if (in_array($navLink['id'], ['login', 'signup', 'locale', 'dashboard']))
                        @if (isset($navLink['type']) && $navLink['type'] === 'highlighted')
                            <span
                                class="inline-flex rounded-md ring-1 ring-black ring-opacity-5 {{ $loop->first ? '' : 'ltr:ml-8 rtl:mr-8' }}">
                                <a href="{{ $navLink['link'] }}" class="text-lg font-medium bg-primary-inv-500 px-3 py-1.5 rounded">
                                    {{ $navLink['text'] }}
                                </a>
                            </span>
                        @else
                            <a href="{{ $navLink['link'] }}"
                                class="{{ $loop->first ? '' : 'ltr:ml-8 rtl:mr-8 ' }}text-lg font-medium text-gray-300 hover:text-white transition duration-150 ease-in-out">{{ $navLink['text'] }}</a>
                        @endif
                    @endif
                @endforeach

            </div>
            @auth
                <form method="POST" action="{{ route('api.auth.logout') }}">
                    @csrf
                    <button type="submit"
                        class="font-medium text-gray-300 transition duration-150 ease-in-out hover:text-white">{{ __('website.navlinks.logout') }}</a>
                </form>
            @endauth
        </div>
    </nav>

    <div @click.away="isMenuOpen = false" x-show="isMenuOpen" x-transition:enter="duration-150 ease-out"
        x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100"
        x-transition:leave="duration-100 ease-in" x-transition:leave-start="opacity-100 scale-100"
        x-transition:leave-end="opacity-0 scale-95"
        class="absolute inset-x-0 top-0 p-2 transition origin-top-right transform lg:hidden">
        <div class="rounded-lg shadow-md">
            <div class="overflow-hidden bg-white rounded-lg ring-1 ring-black ring-opacity-5" role="menu" aria-orientation="vertical"
                aria-labelledby="main-menu">
                <div class="flex items-center justify-between px-5 pt-4">
                    <div>
                        <img class="w-auto h-16" src="{{ asset('images/aseel-blue.png') }}">
                    </div>
                    <div class="-mr-2">
                        <button @click="isMenuOpen = false" type="button"
                            class="inline-flex items-center justify-center p-2 text-gray-400 transition duration-150 ease-in-out rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500"
                            aria-label="Close menu">
                            <svg class="w-6 h-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="px-2 pt-2 pb-3">
                    @foreach ($websiteNavLinks as $navLink)
                        <a href="{{ $navLink['link'] }}"
                            class="{{ $loop->first ? '' : 'mt-1 ' }}block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out">{{ $navLink['text'] }}</a>
                    @endforeach
                    @auth
                        <form method="POST" action="{{ route('api.auth.logout') }}">
                            @csrf
                            <button type="submit"
                                class="block px-3 py-2 text-base font-medium text-gray-700 transition duration-150 ease-in-out rounded-md hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50">{{ __('website.navlinks.logout') }}</a>
                        </form>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
