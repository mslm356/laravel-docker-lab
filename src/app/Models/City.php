<?php

namespace App\Models;

use App\Casts\LocalizedAttribute;

class City extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'name' => LocalizedAttribute::class
    ];
}
