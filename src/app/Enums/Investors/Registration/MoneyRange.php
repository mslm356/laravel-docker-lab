<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\AlertMessgesInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class MoneyRange extends Enum implements LocalizedEnum
{
    const Between1000To50k =   1;
    const Between50kTo120k =   2;
    const Between120kTo250k =   3;
    const Between250kTo500k =   4;
    const Over500k =   5;


}
