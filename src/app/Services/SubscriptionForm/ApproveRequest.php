<?php


namespace App\Services\SubscriptionForm;

use App\Actions\Admin\SubscriptionForm\RequestUpdateStatus;
use App\Enums\SubscriptionForm\RequestStatus;

class ApproveRequest
{
    public function handle($subscriptionFormRequest)
    {
            $user = $subscriptionFormRequest->user;

            $prepared_data = [
                'status' => RequestStatus::Approved,
                'reviewer_comment' => "Approve Request Using Command",
                'reviewer_id' => 1,
            ];

            RequestUpdateStatus::run($user, $prepared_data, $subscriptionFormRequest);

        return true;
    }


}
