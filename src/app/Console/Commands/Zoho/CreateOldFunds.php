<?php

namespace App\Console\Commands\Zoho;

use App\Enums\Zoho\AccountTypes;
use App\Jobs\Zoho\NewCustomer;
use App\Jobs\Zoho\NewItem;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Support\Zoho\Account;
use App\Support\Zoho\Auth;
use App\Support\Zoho\CustomerPayments;
use App\Support\Zoho\Settings;
use App\Support\Zoho\Tax;
use Illuminate\Console\Command;

class CreateOldFunds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:create-old-funds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create old funds to zoho';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $funds = Listing::whereNull('zoho_id')->get();

        foreach ($funds as $fund) {

            $zohoNewCustomerJob = (new NewCustomer($fund, \App\Enums\Zoho\NewCustomer::Fund));
            dispatch($zohoNewCustomerJob);

            $zohoNewItemJob = (new NewItem($fund));
            dispatch($zohoNewItemJob);
        }
    }
}
