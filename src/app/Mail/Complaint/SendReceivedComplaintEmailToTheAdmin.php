<?php

namespace App\Mail\Complaint;

use App\Models\Complaint;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendReceivedComplaintEmailToTheAdmin extends Mailable
{
    use Queueable, SerializesModels;

    protected $complaint = null;
    protected $adminEmail = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Complaint $complaint, $adminEmail)
    {
        $this->complaint = $complaint;
        $this->adminEmail = $adminEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.complaint.complaint-email-to-admin')
            ->with('complaint', $this->complaint)
            ->with('admin_name', explode('@', $this->adminEmail)[0])
            ->subject(__('emails/complaint/admin-email.subject', ['ticketNumber' => $this->complaint->id]));
    }
}
