<?php

namespace App\Actions;

use App\Support\PdfGenerator\PdfGenerator;
use Illuminate\Http\UploadedFile;
use Lorisleiva\Actions\Concerns\AsAction;

class ExportSubscriptionForm
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param \App\Models\Users\InvestorSubscriptionToken $subscriptionToken
     * @param \App\Models\Users\User $user
     * @param \App\Models\Listings\Listing $listing
     * @param \Illuminate\Support\Collection $currentInvestments
     * @return \App\Models\File
     */
    public function handle($fileName, $html)
    {
        $targetPath = "subscription-forms/$fileName";

        PdfGenerator::outputFromHtml($html, $targetPath, [
            'gotoOptions' => [
                'waitUntil' => 'networkidle0'
            ]
        ]);

        return $targetPath;
    }
}
