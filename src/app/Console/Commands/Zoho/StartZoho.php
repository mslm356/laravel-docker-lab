<?php

namespace App\Console\Commands\Zoho;

use App\Enums\Zoho\AccountTypes;
use App\Jobs\Zoho\NewCustomer;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Support\Zoho\Account;
use App\Support\Zoho\Auth;
use App\Support\Zoho\Customer;
use App\Support\Zoho\CustomerPayments;
use App\Support\Zoho\JournalPrepareData\InvestorTransfer;
use App\Support\Zoho\ManualJournal;
use App\Support\Zoho\Settings;
use App\Support\Zoho\Tax;
use Illuminate\Console\Command;

class StartZoho extends Command
{

    public $zohoSettings;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:start {--code=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start zoho settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->zohoSettings = new Settings();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

//        $user = User::find(149);
//        $this->zohoCustomer = new Customer();
//        $this->zohoCustomer->saveCustomer($user, \App\Enums\Zoho\NewCustomer::Customer);

//        $zohoNewCustomerJob = (new NewCustomer($user, \App\Enums\Zoho\NewCustomer::Customer));
//        dispatch($zohoNewCustomerJob);
//
//        dd(1);



        $zoho = new Auth();
        $data = $zoho->getToken($this->option('code'));

        $this->saveTax('Standard Rate');

        $this->saveAccounts();

        dd($data);
    }

    public function accountsData()
    {

        return [
            [
                'account_name' => AccountTypes::ANBCurrent,
                'account_code' => '',
                'account_type' => 'bank',
            ],
            [
                'account_name' => AccountTypes::ANBBank,
                'account_code' => '001',
                'account_type' => 'bank',
            ],
            [
                'account_name' => AccountTypes::InvestorWallet,
                'account_code' => '002',
                'account_type' => 'other_current_liability',
            ],
            [
                'account_name' => AccountTypes::FundWallet,
                'account_code' => '0022',
                'account_type' => 'other_current_liability',
            ],
            [
                'account_name' => AccountTypes::AccountsReceivable,
                'account_code' => '0011',
                'account_type' => 'accounts_receivable',
            ],
            [
                'account_name' => AccountTypes::OutputVAT,
                'account_code' => '00222',
                'account_type' => 'other_current_liability',
            ],
        ];
    }

    public function saveAccounts () {

        $zohoAccount = new Account();

        $zohoAccounts = $zohoAccount->list();

        $zohoAccountsData = [];

        foreach ($zohoAccounts as $zohoAccountItem) {
            $zohoAccountsData [$zohoAccountItem['account_name']] = $zohoAccountItem['account_id'];
        }

        $accountsData = $this->accountsData();

        foreach ($accountsData as $account) {

            if (isset($zohoAccountsData[$account['account_name']])) {

                $this->zohoSettings->saveSettings($account['account_name'], $zohoAccountsData[$account['account_name']]);
                continue;
            }

            $zohoAccount->store($account);
        }
    }

    public function saveTax ($tax_name) {

        $zohoTax = new Tax();

        $taxes = $zohoTax->list();

        foreach ($taxes as $tax) {

            if ($tax['tax_percentage'] == 15 && $tax['country_code'] == 'SA') {
                $this->zohoSettings->saveSettings($tax_name, $tax['tax_id']);
                return true;
            }
        }

        $zohoTax->store('Standard Rate');
    }
}
