<?php

namespace App\Http\Resources\Admins\EodStatements;

use App\Enums\Banking\BankCode;
use Illuminate\Http\Resources\Json\JsonResource;

class EodStatementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date->timezone('Asia/Riyadh')->toDateString(),
            'bank_name' => BankCode::getDescription($this->bank_code),
            'account_number' => $this->account_number,
            'currency' => $this->currency,
            'sequence_number' => $this->sequence_number,
            'opening_balance' => $this->opening_balance ?
                money($this->opening_balance['amount'], $this->opening_balance['currency'])->format() : null,
            'closing_balance' => $this->closing_balance ?
                money($this->closing_balance['amount'], $this->closing_balance['currency'])->format() : null,
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
