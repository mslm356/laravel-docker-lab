<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use App\Actions\Listings\Images\DeleteListingImage;
use App\Actions\Listings\Images\StoreListingImages;
use App\Common\ValidationRules\Listings\ListingImageRules;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Listings\ListingImagesResource;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingImage;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ListingImageController extends Controller
{
    use CanAccessListing;

    /**
     * Store the images in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        return DB::transaction(function () use ($request, $listing) {
            $data = $this->validator($request->all())->validate();

           // StoreListingImages::run($listing, Arr::get($data, 'files', []));

            $result = StoreListingImages::run($listing, Arr::get($data, 'files', []));
            $newImages = ListingImage::whereIn("name", $result)->where('listing_id', $listing->id)->get();

            return response()->json([
                'images' => ListingImagesResource::collection($newImages)
            ], 201);

           // return response()->json([], 201);
        });
    }

    public function sortImages(Request $request)
    {
        $request->validate(['imagesToBeSorting' => "nullable"]);

        foreach ($request->imagesToBeSorting as $image) {
            if ($image) {
                ListingImage::where("id", $image['id'])->update(['index' => $image['index']]);
            }

        }
        return response()->json([], 200);
    }

    /**
     * Delete the image
     *
     * @param \App\Models\Listings\ListingImage $image
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, ListingImage $image)
    {
        $this->canUserAccessListing($image->listing);

        return DB::transaction(function () use ($image) {
            DeleteListingImage::run($image);

            return response()->json();
        });
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, ListingImageRules::get());
    }
}
