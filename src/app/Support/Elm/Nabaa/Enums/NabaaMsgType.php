<?php

namespace App\Support\Elm\Nabaa\Enums;

use BenSampo\Enum\Enum;

final class NabaaMsgType extends Enum
{
    const SmsVerification = 1;
}
