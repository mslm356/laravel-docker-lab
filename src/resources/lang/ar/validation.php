<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'يجب قبول :attribute.',
    'active_url'           => ':attribute لا يُمثّل رابطًا صحيحًا.',
    'after'                => 'يجب على :attribute أن يكون تاريخًا لاحقًا للتاريخ :date.',
    'after_or_equal'       => ':attribute يجب أن يكون تاريخاً لاحقاً أو مطابقاً للتاريخ :date.',
    'alpha'                => 'يجب أن لا يحتوي :attribute سوى على حروف.',
    'alpha_dash'           => 'يجب أن لا يحتوي :attribute سوى على حروف، أرقام ومطّات.',
    'alpha_num'            => 'يجب أن يحتوي :attribute على حروفٍ وأرقامٍ فقط.',
    'array'                => 'يجب أن يكون :attribute ًمصفوفة.',
    'before'               => 'يجب على :attribute أن يكون تاريخًا سابقًا للتاريخ :date.',
    'before_or_equal'      => ':attribute يجب أن يكون تاريخا سابقا أو مطابقا للتاريخ :date.',
    'between'              => [
        'numeric' => 'يجب أن تكون قيمة :attribute بين :min و :max.',
        'file'    => 'يجب أن يكون حجم الملف :attribute بين :min و :max كيلوبايت.',
        'string'  => 'يجب أن يكون عدد حروف النّص :attribute بين :min و :max.',
        'array'   => 'يجب أن يحتوي :attribute على عدد من العناصر بين :min و :max.',
    ],
    'boolean'              => 'يجب أن تكون قيمة :attribute إما true أو false .',
    'confirmed'            => 'حقل التأكيد غير مُطابق للحقل :attribute.',
    'date'                 => ':attribute ليس تاريخًا صحيحًا.',
    'date_equals'          => 'يجب أن يكون :attribute مطابقاً للتاريخ :date.',
    'date_format'          => 'لا يتوافق :attribute مع الشكل :format.',
    'different'            => 'يجب أن يكون الحقلان :attribute و :other مُختلفين.',
    'digits'               => 'يجب أن يحتوي :attribute على :digits رقمًا/أرقام.',
    'digits_between'       => 'يجب أن يحتوي :attribute بين :min و :max رقمًا/أرقام .',
    'dimensions'           => 'الـ :attribute يحتوي على أبعاد صورة غير صالحة.',
    'distinct'             => 'للحقل :attribute قيمة مُكرّرة.',
    'email'                => 'يجب أن يكون :attribute عنوان بريد إلكتروني صحيح البُنية.',
    'ends_with'            => 'يجب أن ينتهي :attribute بأحد القيم التالية: :values',
    'exists'               => 'قيمة :attribute غير موجودة.',
    'file'                 => 'الـ :attribute يجب أن يكون ملفا.',
    'filled'               => ':attribute إجباري.',
    'gt'                   => [
        'numeric' => 'يجب أن تكون قيمة :attribute أكبر من :value.',
        'file'    => 'يجب أن يكون حجم الملف :attribute أكبر من :value كيلوبايت.',
        'string'  => 'يجب أن يكون طول النّص :attribute أكثر من :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على أكثر من :value عناصر/عنصر.',
    ],
    'gte'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أكبر من :value.',
        'file'    => 'يجب أن يكون حجم الملف :attribute على الأقل :value كيلوبايت.',
        'string'  => 'يجب أن يكون طول النص :attribute على الأقل :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على الأقل على :value عُنصرًا/عناصر.',
    ],
    'image'                => 'يجب أن يكون :attribute صورةً.',
    'in'                   => ':attribute غير موجود.',
    'in_array'             => ':attribute غير موجود في :other.',
    'integer'              => 'يجب أن يكون :attribute عددًا صحيحًا.',
    'ip'                   => 'يجب أن يكون :attribute عنوان IP صحيحًا.',
    'ipv4'                 => 'يجب أن يكون :attribute عنوان IPv4 صحيحًا.',
    'ipv6'                 => 'يجب أن يكون :attribute عنوان IPv6 صحيحًا.',
    'json'                 => 'يجب أن يكون :attribute نصًا من نوع JSON.',
    'lt'                   => [
        'numeric' => 'يجب أن تكون قيمة :attribute أصغر من :value.',
        'file'    => 'يجب أن يكون حجم الملف :attribute أصغر من :value كيلوبايت.',
        'string'  => 'يجب أن يكون طول النّص :attribute أقل من :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على أقل من :value عناصر/عنصر.',
    ],
    'lte'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أصغر من :value.',
        'file'    => 'يجب أن لا يتجاوز حجم الملف :attribute :value كيلوبايت.',
        'string'  => 'يجب أن لا يتجاوز طول النّص :attribute :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن لا يحتوي :attribute على أكثر من :value عناصر/عنصر.',
    ],
    'max'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أصغر من :max.',
        'file'    => 'يجب أن لا يتجاوز حجم الملف :attribute :max كيلوبايت.',
        'string'  => 'يجب أن لا يتجاوز طول النّص :attribute :max حروفٍ/حرفًا.',
        'array'   => 'يجب أن لا يحتوي :attribute على أكثر من :max عناصر/عنصر.',
    ],
    'mimes'                => 'يجب أن يكون ملفًا من نوع : :values.',
    'mimetypes'            => 'يجب أن يكون ملفًا من نوع : :values.',
    'min'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أكبر من :min.',
        'file'    => 'يجب أن يكون حجم الملف :attribute على الأقل :min كيلوبايت.',
        'string'  => 'يجب أن يكون طول النص :attribute على الأقل :min حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على الأقل على :min عُنصرًا/عناصر.',
    ],
    'multiple_of'          => ':attribute يجب أن يكون من مضاعفات :value',
    'not_in'               => 'العنصر :attribute غير صحيح.',
    'not_regex'            => 'صيغة :attribute غير صحيحة.',
    'numeric'              => 'يجب على :attribute أن يكون رقمًا.',
    'password'             => 'كلمة المرور غير صحيحة.',
    'present'              => 'يجب تقديم :attribute.',
    'regex'                => 'صيغة :attribute .غير صحيحة.',
    'required'             => ':attribute مطلوب.',
    'clamav'             => 'الملفات المرفوعه بها فيروسات.',
    'required_if'          => ':attribute مطلوب في حال ما إذا كان :other يساوي :value.',
    'required_unless'      => ':attribute مطلوب في حال ما لم يكن :other يساوي :values.',
    'required_with'        => ':attribute مطلوب إذا توفّر :values.',
    'required_with_all'    => ':attribute مطلوب إذا توفّر :values.',
    'required_without'     => ':attribute مطلوب إذا لم يتوفّر :values.',
    'required_without_all' => ':attribute مطلوب إذا لم يتوفّر :values.',
    'same'                 => 'يجب أن يتطابق :attribute مع :other.',
    'size'                 => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية لـ :size.',
        'file'    => 'يجب أن يكون حجم الملف :attribute :size كيلوبايت.',
        'string'  => 'يجب أن يحتوي النص :attribute على :size حروفٍ/حرفًا بالضبط.',
        'array'   => 'يجب أن يحتوي :attribute على :size عنصرٍ/عناصر بالضبط.',
    ],
    'starts_with'          => 'يجب أن يبدأ :attribute بأحد القيم التالية: :values',
    'string'               => 'يجب أن يكون :attribute نصًا.',
    'timezone'             => 'يجب أن يكون :attribute نطاقًا زمنيًا صحيحًا.',
    'unique'               => 'قيمة :attribute مُستخدمة من قبل.',
    'uploaded'             => 'فشل في تحميل الـ :attribute.',
    'url'                  => 'صيغة الرابط :attribute غير صحيحة.',
    'uuid'                 => ':attribute يجب أن يكون بصيغة UUID سليمة.',

    'phone' => 'رقم الهاتف المدخل غير صحيح',
    'iban' => 'حقل :attribute غير صحيح',
    'recaptcha' => 'فشل التحقق من reCaptcha',
    'incorrect_password' => 'كلمة المرور غير صحيحة',
    'numeric_string' => ':attribute يجب أن يكون رقمًا نصيًا',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => 'يجب أن تكون كلمة المرور ٨ أحرف أو أكثر وأن تكون الأحرف إنجليزية وأن تحتوي على الأقل حرف صغير واحد، حرف واحد كبير، رقم، ورموز خاصة (!@#$%^&*)',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'items.*.files'            => 'المرفقات',
        'items.*'                  => 'هذا الاختيار',
        'items.*.id'                  => 'هذا الاختيار',
        'name'                  => 'الاسم',
        'username'              => 'اسم المُستخدم',
        'email'                 => 'البريد الالكتروني',
        'first_name'             => 'الاسم الأول',
        'last_name'             => 'اسم العائلة',
        'password'              => 'كلمة المرور',
        'password_confirmation'  => 'تأكيد كلمة المرور',
        'phone_country_iso2'    => 'رمز الدولة',
        'nin'                   => 'رقم الهوية',
        'date_of_birth'         => 'تاريخ الميلاد',
        'education_level'       => 'مستوى التعليم',
        'occupation'            => 'الوظيفة',
        'city'                  => 'المدينة',
        'country'               => 'الدولة',
        'address'               => 'العنوان',
        'phone'                 => 'الهاتف',
        'mobile'                => 'الجوال',
        'age'                   => 'العمر',
        'sex'                   => 'الجنس',
        'gender'                => 'النوع',
        'day'                   => 'اليوم',
        'month'                 => 'الشهر',
        'year'                  => 'السنة',
        'hour'                  => 'ساعة',
        'minute'                => 'دقيقة',
        'second'                => 'ثانية',
        'title'                 => 'العنوان',
        'content'               => 'المُحتوى',
        'description'           => 'الوصف',
        'excerpt'               => 'المُلخص',
        'date'                  => 'التاريخ',
        'time'                  => 'الوقت',
        'available'             => 'مُتاح',
        'size'                  => 'الحجم',
        'iban'                  => 'الآيبان',
        'current_invest'        => 'ما الذي تستثمر فيه حاليًا؟',
        'net_worth'        => 'صافي الثروة التقريبي',
        'annual_income'        => 'الدخل السنوي',
        'expected_annual_invest' => 'المبلغ المتوقع استثماره سنويًا',
        'expected_invest_amount_per_opportunity' => 'المبلغ المتوقع استثماره لكل فرصة',
        'annual_yield' => 'ما مقدار العائد السنوي الذي تبحث عنه؟',
        'source_of_income' => 'مصدر الدخل',
        'invest_objective.*' => 'الهدف الاستثماري',
        'citizen' => 'الجنسية',
        'more_general_info.any_xp_in_fin_sector' => 'هل سبق للعميل العمل في القطاع المالي خلال السنوات الخمس السابقة؟',
        'more_general_info.any_xp_related_to_fin_sector' => 'هل للعميل أي خبرات عملية أخرى ذات صلة بالقطاع المالي؟',
        'more_general_info.xp_related_to_fin_sector' => 'الخبرات المتعلقة بالقطاع المالي',
        'more_general_info.any_other_fin_situation' => 'أي معلومات مالية أخرى عن الوضع المالي للعميل',
        'more_general_info.has_prominent_functions' => 'هل العميل مكلّف بمهمات عليا في المملكة أو في دولة أجنبية أو مناصب إدارة عليا أو وظيفة في إحدى المنظمات الدولية؟',
        'more_general_info.is_close_to_prominent_functions' => 'هل للعميل صلة قرابة برابطة الدم أو الزواج وصولًا إلى الدرجة الثانية أو يُعد مقرّبًا من شخص مكلّف بمبهمات عليا في المملكة أو في دولة أجنبية أو مناصب إدارة عليا أو وظيفة في إحدى المنظمات الدولية؟',
        'is_on_board' => 'هل العميل عضو مجلس إدارة أو لجنة مراجعة أو أحد كبار التنفيذيين في شركة مدرجة؟',
        'more_general_info.is_close_to_on_board' => 'هل العميل ذو علاقة بعضو مجلس إدارة أو لجنة مراجعة أو أحد كبار التنفيذيين في شركة مدرجة؟',
        'more_general_info.is_beneficial_owner' => 'هل العميل هو المستفيد الحقيقي من الحساب أو علاقة العمل؟',
        'more_general_info.identity_of_account_owner' => 'هوية المستفيد الحقيقي من الحساب أو علاقة العمل',
        'more_financial_info.expected_cash_out_period' => 'المدة التي يتوقع العميل خلالها استرداد الأموال المستثمرة',
        'more_financial_info.securities_trans_outside_ksa_over_past_5_years' => 'صفقات الأوراق المالية خارج المملكة خلال السنوات الخمس السابقة',
        'more_financial_info.in_which_countries_trans_executed' => 'إذا كان قد تم تنفيذ صفقات أوراق مالية خارج المملكة خلال السنوات الخمس السابقة ما الدول التي تم تنفيذ تلك الصفقات فيها؟',
        'more_financial_info.investment_xp' => 'المعرفة والخبرات الاستثمارية',
        'more_financial_info.loan_to_inv_money_ratio' => 'نسبة القروض من الأموال المستثمرة',
        'more_financial_info.margin_trans_over_past_5_years' => 'صفقات التمويل بالهامش خلال السنوات الخمس السابقة',
        'more_financial_info.preferred_inv_assets.in_foreign_currencies' => 'ما الأصول الاستثمارية المفضلة للعميل؟',
        'more_financial_info.preferred_inv_assets.in_sar' => 'ما الأصول الاستثمارية المفضلة للعميل؟',
        'more_financial_info.preferred_inv_assets.foreign_currencies' => 'العملات الأجنبية',
        'more_financial_info.products_prev_inv_in' => 'المنتجات الني سبق الاستثمار فيها',
        'more_financial_info.prof_certs' => 'الشهادات المهنية',
        'more_financial_info.risk_appetite' => 'قدرة العميل على تحمل المخاطرة',
        'more_financial_info.years_of_inv_in_securities' => 'عدد سنوات الاستثمار في أسواق الأوراق المالية',

        'listing.total_shares' => 'عدد الوحدات',

        'more_general_info.is_individual' => 'هل أنت فرد أو منشأة',
        'more_general_info.have_you_invested_in_real_estates' => 'هل استثمرت سابقًا في العقارات',
        'more_general_info.investment_xp' => 'المعرفة والخبرات الاستثمارية',
        'location.city_id' => 'المدية',
        'location.street' => 'الشارع',
        'location.district' => 'الحي',
        'location.postal_code' => 'الرمز البريدي',
        'bank_account_id' => 'الحساب البنكي',
        'bank' => 'البنك',
        'phone_number' => 'رقم الهاتف',
        'value' => 'القيمة',
        'number' => 'الرقم',
        'type' => 'النوع',
        'code' => 'الكود',
        'old_password' => 'كلمه السر القديمه',
        'attachments' => 'الملحقات',
        'token' => 'التوكن',
        'question' => 'السؤال',
        'answer' => 'الاجابه',
        'device_token' => 'التوكن',
        'platform' => 'البلاتفورم',
        'mobile_version' => 'اصدار الموبايل',
        'expires_in' => 'تنتهي صلاحيته في',
        'birth_date' => 'تاريخ الميلاد',
        'vid' => 'رمز التفعيل',
        'status' => 'الحاله',
        'subscription_form' => 'كشف حساب',
        'expired_at' => 'تنتهي صلاحيته في',
        'account_number' => 'رقم الحساب',
        'commercial_number' => 'رقم السجل التجاري',
        'absher_otp_purpose' => ' سبب كود التفعيل الخاص بابشر ',
    ],

    'values' => [
        'more_general_info.any_xp_related_to_fin_sector' => [
            '1' => 'نعم'
        ],
        'more_general_info' => [
            'is_beneficial_owner' => [
                '0' => 'لا'
            ]
        ],
        'more_financial_info.preferred_inv_assets.in_foreign_currencies' => [
            '1' => 'نعم'
        ],
    ],
];
