<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNabaaMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nabaa_messages', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedTinyInteger('type');
            $table->string('batch_number')->nullable();
            $table->string('batch_id')->nullable();
            $table->unsignedTinyInteger('status')->nullable();
            $table->string('status_description')->nullable();
            $table->string('template_id')->nullable();
            $table->json('recipients')->nullable();
            $table->json('data')->nullable();
            $table->json('detailed_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nabaa_messages');
    }
}
