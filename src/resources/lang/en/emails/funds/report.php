<?php

return [
    'subject' => 'A report has been issued about  :fund_title investment opportunity',
    'welcome_message' => 'Our dear investor',
    'name' => ':name',
    'message_1' => 'Attached is a report on the performance of the investment opportunity',
    'message_1_2' => 'You can view the report in your control panel',
    'message_2' => 'To view the opportunity, please visit the link',
    'end_message' => 'Thank you for trusting Aseel platform',
    'view' => 'View',

];
