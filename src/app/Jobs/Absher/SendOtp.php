<?php

namespace App\Jobs\Absher;

use App\Enums\Elm\AbsherOtpReason;
use App\Services\LogService;
use App\Support\Elm\AbsherOtp\ErrorCodes;
use App\Support\Elm\AbsherOtp\Exceptions\ConfigurationException;
use App\Support\Elm\AbsherOtp\Exceptions\CustomerException;
use App\Support\Elm\AbsherOtp\Exceptions\InternalConnectionException;
use App\Support\Elm\AbsherOtp\Exceptions\SendingSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use App\Traits\SendMessage;

class SendOtp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationServices,SendMessage;

    public $token;
    public $user;
    public $absherOtpPurpose;
    public $otpRecord;
    public $logService;
    public $delay;
    public $nin;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $token, $otpRecord, $absherOtpPurpose, $delay, $nin)
    {
        $this->token = $token;
        $this->user = $user;
        $this->absherOtpPurpose = $absherOtpPurpose;
        $this->logService = new LogService();
        $this->otpRecord = $otpRecord;
        $this->delay = $delay;
        $this->nin = $nin;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if ($this->token == null) {
            return $this->sendWithAbsher();
        }

        if ($this->token->listing->sms_provider == 4)
        {
            $this->token->listing->sms_provider= ($this->token->id % 2 == 0)? 1 : 2;
        }

        if ($this->token->listing->sms_provider == 1)
            return $this->sendWithTqnyat();
        elseif ($this->token->listing->sms_provider == 2)
            return $this->sendWithAbsher();
        elseif ($this->token->listing->sms_provider == 3)
        {
            $this->sendWithTqnyat();
            return $this->sendWithAbsher();
        }
        else
        {
            return $this->sendWithAbsher();
        }

    }


//<<<<<<< HEAD
    private function sendWithTqnyat()
    {
        try {
            $user_locale = $this->user->locale ?? App::getLocale();

            if ($user_locale == 'ar')
              $teqnyat_msg='عميلنا العزيز، يرجى استخدام رمز التحقق '.$this->otpRecord->code.' للموافقة على الاستثمار في '.$this->token->listing->title_ar.' من أصيل المالية. في حال عدم طلبك الخدمة الرجاء التواصل مع أصيل المالية.';
            else
                $teqnyat_msg='Dear Customer, please use the OTP number '.$this->otpRecord->code.' , to complete your request for investing in '.$this->token->listing->title_en.' with aseel capital, in case you did not request this service please contact أصيل المالية';

            $this->sendOtpMessage($this->user->phone_number, $teqnyat_msg);

        }catch (\Exception $e){
            $this->logService->log('Tqnyat-FAILED-SOAP', $e);

        }
//=======
////        try {
////            $teqnyat_msg='عميلنا العزيز، يرجى استخدام رمز التحقق '.$this->otpRecord->code.' للموافقة على الاستثمار في صندوق سكك تلالة العقاري من أصيل المالية. في حال عدم طلبك الخدمة الرجاء التواصل مع أصيل المالية.';
////            $this->sendOtpMessage($this->user->phone_number, $teqnyat_msg);
////
////        }catch (\Exception $e){
////            $this->logService->log('Tqnyat-FAILED-SOAP', $e);
////
////        }
//>>>>>>> 5378f93da21b6f8c5f4bf72c2bf7d6cdc8508e7c

    }

    private function sendWithAbsher()
    {
        try {

            $meta = [
                'reason' => __('absher_otp_purpose.' . AbsherOtpReason::VerifyIdentity, [], $user->locale ?? App::getLocale())
            ];

            if ($this->token != null) {

                $meta = [
                    'subscription_token_id' => $this->token ? $this->token->id : null,
                    'reason' => __(
                        'absher_otp_purpose.' . $this->absherOtpPurpose,
                        [
                            'fund' => $this->token->listing->{locale_suff('title', $preferredLocale = $this->user->locale ?? App::getLocale())}
                        ],
                        $preferredLocale
                    )
                ];
            }

            $absherData = VerificationSms::instance($this->nin)->sendToAbsher($this->user, $meta, $this->delay, $this->nin, $this->otpRecord->code);

            $absherResponse = $absherData['response'];
            $requestBody = $absherData['request'];

            if ((int)$absherResponse->status != 0) {
                throw new SendingSmsException();
            }

            //            $this->updateAbsherOtp($absherResponse, $this->otpRecord, $meta);
        } catch (SoapFault $th) {
            if ($fault = $th->detail->SendOTPFault ?? null) {

                $this->logService->log('ABSHER-FAILED-SOAP-true', null, [$fault, $th, $th->detail]);

                $exceptionArgs = [$requestBody, $fault->ErrorCode, $fault->ErrorType, $fault->ErrorMessage, $th->faultstring, $th->faultactor];

                if (in_array($fault->ErrorCode, ErrorCodes::configurationCodes)) {
                    throw new ConfigurationException(...$exceptionArgs);
                } elseif (in_array($fault->ErrorCode, ErrorCodes::customerCodes)) {
                    throw new CustomerException(...$exceptionArgs);
                } else {
                    throw new \App\Support\Elm\AbsherOtp\Exceptions\ExternalSystemException(...$exceptionArgs);
                }

            } else {

                $this->logService->log('ABSHER-FAILED-SOAP', null, [$th]);
                throw new InternalConnectionException($th->getMessage());
            }

        } catch (SendingSmsException $e) {
            $this->logService->log('ABSHER-FAILED-SENDING-SMS', $e);
        } catch (\Exception $e) {
            $this->logService->log('ABSHER-FAILED-EXCEPTION', $e);
        }
    }


    public function updateAbsherOtp ($absherResponse, $otpRecord, $meta) {

        $otpRecord->transaction_id = $absherResponse->transactionId;

        $otpRecord->data = array_merge(
            [
                'response' => [
                    'status' => $absherResponse->status,
                    'client_id' => $absherResponse->clientId,
                    'customer_id' => $absherResponse->customerId,
                ],
            ],
            Arr::except($meta, 'reason')
        );

        $this->otpRecord->save();
    }
}
