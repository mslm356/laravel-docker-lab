<?php

namespace App\Support\Users\UserAccess\Modules;

use App\Models\Listings\ListingInvestor;

class ShowAppRate
{
    public function handle($user, $module, $userDefaultData)
    {
        $response = 0;

        $response = $this->defaultValidation($user);

        return $response;
    }

    public function defaultValidation($user)
    {
        $last_invest=ListingInvestor::where('listing_id', 46)->where('investor_id', $user->id)->first();

        return [
            'show_app_rate' => ($last_invest)?true:false,
        ];
    }
}
