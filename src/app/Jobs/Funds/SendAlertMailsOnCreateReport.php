<?php

namespace App\Jobs\Funds;

use App\Enums\Notifications\NotificationTypes;
use App\Enums\Role;
use App\Models\Users\User;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendAlertMailsOnCreateReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels , NotificationServices , SendMessage;

    public $listing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listing)
    {
        $this->listing = $listing;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $listing = $this->listing;
        $users = optional($listing->investors()->where('invested_shares', '>', 0))->pluck('investor_id')->toArray();

        User::role(Role::Investor)
            ->lazyById()
            ->whereIn('id', $users)
            ->each(function ($user) use ($listing) {

                Mail::to($user)->queue(new \App\Mail\Funds\report($user, $listing));
                $this->sendSmsData($listing,$user);

                $this->send(NotificationTypes::FundReport, $this->notificationData($listing, $user), $user->id);

            });
    }

    public function notificationData($listing, $user)
    {

        return [
            'action_id' => $listing->id,
            'channel' => 'mobile',
            'title_en' => 'Aseel',
            'title_ar' => 'منصة أصيل',
            'content_en' => 'A report has been issued about  ' . $listing->title . ' investment opportunity You can view the report in your Account' ,
            'content_ar' =>     ' تم صدور تقرير عن فرصة  '.$listing->title . ' الاستثمارية  نأمل الاطلاع عليه من خانة استثماراتي ' ,
        ];
    }

    public function sendSmsData($listing, $user)
    {
        $name = $user ? $user->full_name : '---';
        $content_en = 'A report has been issued about '. $listing->title .' investment opportunity ' . ' Attached is a report on the performance of the investment opportunity You can view the report in your Dashboard Thank you for trusting Aseel ';
        $content_ar =  ' تم صدور تقرير عن فرصة '. $listing->title  .'  الاستثمارية ' . ' مرفق لكم تقرير عن أداء الفرصة الاستثمارية نأمل الاطلاع عليه كما تجدونه في لوحة التحكم الخاص بكم شكراً لثقتك في منصة أصيل '   ;

        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar;

        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = "Our dear investor ". $name . $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }
}
