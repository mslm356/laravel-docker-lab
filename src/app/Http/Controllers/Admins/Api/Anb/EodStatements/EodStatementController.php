<?php

namespace App\Http\Controllers\Admins\Api\Anb\EodStatements;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Admins\EodStatements\EodStatementResource;
use App\Http\Resources\Admins\EodStatements\EodTransactionResource;
use App\Models\Anb\AnbEodStatement;

class EodStatementController extends Controller
{
    /**
     * Get statements
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return EodStatementResource::collection(
            AnbEodStatement::orderBy('id','desc')->paginate()
        );
    }

    /**
     * Get statement
     *
     * @param \App\Models\EodStatement $statement
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(AnbEodStatement $statement)
    {
        return new EodStatementResource($statement);
    }

    /**
     * Get the transactions of a statement
     *
     * @param \App\Models\EodStatement $statement
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTransactions(AnbEodStatement $statement)
    {
        return EodTransactionResource::collection(
            $statement->transactions()->orderBy('id','desc')->paginate()
        );
    }

    /**
     * Download statement
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function downloadStatement(AnbEodStatement $statement)
    {
        $filename = last(explode('/', $statement->file_path));

        return Storage::disk(config('filesystems.private'))->download(
            $statement->file_path,
            $filename,
            ['Content-Disposition' => "attachment;filename={$filename}"]
        );
    }
}
