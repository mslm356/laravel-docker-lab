<?php

namespace App\Services\Sdad\Contracts;

interface EdaatProviderClientRepositoryContract
{
    /**
     * @param string $service_name
     * @return EdaatProviderClientRepositoryContract|null
     * initiation and perpetration for required headers and base url etc
     * according to a service name predefined in a config file and enums
     */

    public function initiation(string $service_name): ?EdaatProviderClientRepositoryContract;

    /**
     * @return EdaatProviderClientRepositoryContract|null
     * send request to edaat and assign the response to a property
     */
    public function send(): ?EdaatProviderClientRepositoryContract;

    /**
     * return response array returned from edaat
     */
    public function getResponse(): ?object;

    /**
     * @return EdaatProviderClientRepositoryContract
     * reset repo object to re-usability or discard properties
     */
    public function reset(): EdaatProviderClientRepositoryContract;

}
