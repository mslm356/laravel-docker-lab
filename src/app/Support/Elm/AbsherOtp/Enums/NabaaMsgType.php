<?php

namespace App\Support\Elm\AbsherOtp\Enums;

use BenSampo\Enum\Enum;

final class NabaaMsgType extends Enum
{
    const SmsVerification = 1;
}
