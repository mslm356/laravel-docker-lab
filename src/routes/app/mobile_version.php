<?php

use \App\Http\Controllers\MobileApp\MobileVersionsController;

Route::post('/mobile-versions/check-update', [ MobileVersionsController::class, 'updateVersion']);
Route::get('/mobile-versions/check-last-update', [ MobileVersionsController::class, 'checkUpdate']);
