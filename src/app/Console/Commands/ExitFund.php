<?php

namespace App\Console\Commands;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\WalletType;
use App\Enums\MemberShips\MemberShipType;
use App\Enums\Notifications\NotificationTypes;
use App\Enums\Role;
use App\Jobs\AbsherTest;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use App\Models\Anb\AnbTransfer;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestor;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\VirtualBanking\Helpers\AnbVA;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExitFund extends Command
{

    use NotificationServices;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exitFund {--listing_id=}  {--investor_id=}';
    public $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'exit fund command';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $listing = Listing::find((int)$this->option("listing_id"));
            $investor = User::role(Role::Investor)->find((int)$this->option("investor_id"));

            if(!$listing)
                dd("listing id not found");

            if(!$investor)
                dd("investor id not found");

           ListingInvestor::where('investor_id',$this->option("investor_id"))
               ->where('listing_id',$this->option("listing_id"))
               ->update([
                   "invested_shares" => 0
               ]);


            dd('Congratulation Exit Fund Done Successfully ');

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }


}
