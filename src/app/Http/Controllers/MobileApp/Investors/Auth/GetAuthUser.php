<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Services\LogService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\AuthResource;

class GetAuthUser extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }
    /**
     * Show Investor Profile.
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $user = getAuthUser('api')->load(['investor']);
            return apiResponse(200, __('done.successfully'), new AuthResource($user));

        }  catch (\Exception $e) {
            $code = $this->logService->log('GET-AUTH-USER', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }
}
