<?php

namespace App\Console\Commands\Admins;

use App\Enums\Role as EnumsRole;
use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class ResetPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admins:reset-password {--email=} {--password=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset password admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->option('email');
        $passwordOption = $password = $this->option('password');

        if ($email === null) {
            return $this->error('Please enter an email address');
        }

        if ($password === null) {
            $password = Str::random();
        }

        $usersCount = User::where('email', $email)
            ->where('type', 'admin')
//            ->role(\App\Enums\Role::Admin)
            ->count();

        if ($usersCount != 1) {
            dd('users with this email count => ' . $usersCount);
        }

        \DB::table('users')->where('email', $email)
            ->update([
                'password' => Hash::make($password)
            ]);

        if ($passwordOption === null) {
            $this->info("Password: {$password}");
        } else {
            $this->info('Admin created successfully');
        }
    }
}
