<?php

namespace App\Mail\Enquiries;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewReply extends Mailable
{
    use Queueable, SerializesModels;

    public $reply;
    public $enquiry;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($enquiry, $reply)
    {
        $this->reply = $reply;
        $this->enquiry = $enquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->markdown('emails.enquiries.new-reply')
            ->subject(__('emails/enquiries/new-reply.subject', ['id' => $this->reply->enquiry_id]));

        $this->withSwiftMessage(function ($message) {
            $message->getHeaders()
                ->addTextHeader(
                    'Reply-To',
                    $this->enquiry->createToken()['tokenPlainText'] . '@' . config('mail.from.domain')
                );
        });
    }
}
