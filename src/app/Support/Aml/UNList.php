<?php

namespace App\Support\Aml;

use App\Enums\AML\AmlList;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class UNList implements AmlListInterface
{
    protected $list = null;

    protected static $filename = 'aml-lists/un-list.xml';

    /**
     * Get the list
     *
     * @return \SimpleXMLElement
     */
    public function getList()
    {
        if ($this->list) {
            return $this->list;
        }

        if (Storage::disk('local')->exists(static::$filename)) {
            $list = Storage::disk('local')->get(static::$filename);
        } elseif (!($list = Cache::get('un-list'))) {
            $res = Http::get('https://scsanctions.un.org/resources/xml/en/consolidated.xml');
            Cache::put('un-list', $list = $res->body(), now()->addDay());
        }

        $this->list = new \SimpleXMLElement($list);

        return $this->list;
    }

    /**
     * Check the infomation in the list
     *
     * @param string|null $firstName
     * @param string|null $secondName
     * @param string|null $thirdName
     * @param string|null $fourthName
     * @param string|null $nationality
     * @param string|null $dob
     * @param string|null $nationalId
     * @param string|null $passportId
     * @return array
     */
    public function check($firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId)
    {
        $suspected = [];

        foreach ($this->getList()->INDIVIDUALS->INDIVIDUAL as $i) {
            $info = $this->transformIndividualInfo($i);

            $isSuspected = false;

            foreach ($info['docs'] ?? [] as $doc) {
                if ($doc['type'] === 'Passport' && stripos($doc['number'], $passportId) !== false) {
                    $isSuspected = true;
                    break;
                } elseif ($doc['type'] === 'NationalId' && stripos($doc['number'], $nationalId) !== false) {
                    $isSuspected = true;
                    break;
                }
            }

            if ($isSuspected) {
                $suspected[] = $this->standardize($info);
                continue;
            }

            $nameChecks = $this->checkNameIfExists($info['fullName'], [
                $firstName, $secondName, $thirdName, $fourthName
            ]);

            if ($nameChecks > 1) {
                $suspected[] = $this->standardize($info);
                continue;
            }

            foreach ($info['aliases'] ?? [] as $alias) {
                $nameChecks = $this->checkNameIfExists($alias, [
                    $firstName, $secondName, $thirdName, $fourthName
                ]);

                if ($nameChecks > 1) {
                    $suspected[] = $this->standardize($info);
                    continue;
                }
            }
        }

        return $suspected;
    }

    /**
     * Transform the user information into a suitable format
     *
     * @param \SimpleXMLElement $i
     * @return array
     */
    public function transformIndividualInfo($i)
    {
        $info = [
            'id' => (string)$i->DATAID
        ];

        $FULL_NAME = '';

        $FIRST_NAME = (string)$i->FIRST_NAME;
        $SEC_NAME = (string)$i->SECOND_NAME;
        $THIRD_NAME = (string)$i->THIRD_NAME;
        $FOURTH_NAME = (string)$i->FOURTH_NAME;

        if ($FIRST_NAME) {
            $info['firstName'] = $FIRST_NAME;
            $FULL_NAME .=  (' ' . $FIRST_NAME);
        }
        if ($SEC_NAME) {
            $info['secondName'] = $SEC_NAME;
            $FULL_NAME .=  (' ' . $SEC_NAME);
        }
        if ($THIRD_NAME) {
            $info['thirdName'] = $THIRD_NAME;
            $FULL_NAME .=  (' ' . $THIRD_NAME);
        }
        if ($FOURTH_NAME) {
            $info['fourthName'] = $FOURTH_NAME;
            $FULL_NAME .=  (' ' . $FOURTH_NAME);
        }

        $info['fullName'] = $FULL_NAME;

        $aliases = [];

        // aliases
        foreach ($i->INDIVIDUAL_ALIAS as $alias) {
            $name = (string)$alias->ALIAS_NAME;

            if ($name) {
                $aliases[] = $name;
            }
        }

        if (!empty($aliases)) {
            $info['aliases'] = $aliases;
        }

        // date of births
        foreach ($i->INDIVIDUAL_DATE_OF_BIRTH as $dob) {
            $type = (string)$dob->TYPE_OF_DATE;

            if ($type === 'APPROXIMATELY') {
                $year = (string)$dob->YEAR;

                if ($year) {
                    $info['dob'][] = [
                        'type' => 'APPROX',
                        'year' => $year,
                    ];
                }
            } elseif ($type === 'BETWEEN') {
                $fromYear = (string)$dob->FROM_YEAR;
                $toYear = (string)$dob->TO_YEAR;

                if ($fromYear && $toYear) {
                    $info['dob'][] = [
                        'type' => 'BETWEEN',
                        'fromYear' => $fromYear,
                        'toYear' => $toYear
                    ];
                }
            } elseif ($type === 'EXACT') {
                $date = (string)$dob->DATE;

                $info['dob'][] = [
                    'type' => 'EXACT',
                    'date' => $date,
                ];
            }
        }

        // Passport/National IDs
        foreach ($i->INDIVIDUAL_DOCUMENT as $doc) {
            $docType = (string)$doc->TYPE_OF_DOCUMENT;
            $number = (string)$doc->NUMBER;
            $type = null;

            if ($docType === 'Passport') {
                $type = 'Passport';
            } elseif ($docType === 'National Identification Number') {
                $type = 'NationalID';
            }

            if ($type && $number) {
                $info['docs'][] = [
                    'type' => $type,
                    'number' => $number
                ];
            }
        }

        return $info;
    }

    /**
     * Check number of matching of names in the full/alias name
     *
     * @param string $fullName
     * @param array $names
     * @return int
     */
    public function checkNameIfExists($fullName, $names)
    {
        $checks = 0;

        foreach ($names as $name) {
            if ($name && stripos($fullName, $name) !== false) {
                $checks++;
            }
        }

        return $checks;
    }

    /**
     * get List index
     *
     * @return int
     */
    public function getListIndex(): int
    {
        return AmlList::UN;
    }

    /**
     * Standrize the data between the lists
     *
     * @param array $info
     * @return array
     */
    public function standardize($info)
    {
        try {
            $sInfo = [
                'first_name' => $info['firstName'] ?? '',
                'second_name' => $info['secondName'] ?? '',
                'third_name' => $info['thirdName'] ?? '',
                'fourth_name' => $info['fourthName'] ?? '',
                'full_name' => $info['fullName'],
                'aliases' => $info['aliases'] ?? [],
                'dob' => [],
                'docs' => $info['docs'] ?? [],
            ];

            if (isset($info['dob'])) {
                $sInfo['dob'] = array_map(function ($item) {
                    if ($item['type'] === 'APPROX') {
                        return [
                            'type' => 'APPROX',
                            'date' => $item['date'],
                            'year' => null
                        ];
                    }

                    return $item;
                }, $info['dob']);
            }
        } catch (\Throwable $th) {
            throw new \Exception('unable to standrize user: '.json_encode($info), 0, $th);
        }
    }
}
