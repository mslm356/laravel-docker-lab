<?php

namespace App\Models;

class Banner extends AppModel
{
    protected $fillable = ['start_date','end_date'];
    protected $table = 'banners';

    public function imageFile()
    {
        return $this->morphOne(File::class, 'fileable');
    }
}
