<?php

namespace App\Enums\Campaigns;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class CampaignsCode extends Enum
{

    const Email = 1;
    const Sms = 2;
    const Notification = 3;
    const All = 4;

}
