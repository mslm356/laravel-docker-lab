<?php

namespace App\Models\Users;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class NationalIdentity extends AppModel
{
    use HasFactory, HasTranslations;

    protected $translatable = [
        'first_name',
        'second_name',
        'third_name',
        'forth_name',

    ];

    public function addresses()
    {
        return $this->hasMany(IdentityAddress::class, 'identity_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getGenderIdentityAttribute()
    {
        return __("mobile_app/message." . $this->gender);
    }


    public function getYaqeenFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->second_name . ' ' . $this->third_name . ' ' . $this->forth_name;
    }

    /**
     * Get Hijri DOB of the user
     *
     * @return string
     */
    public function getHijriDateOfBirthAttribute()
    {
        if ($this->birth_date_type == 'gregorian') {
            return Hijri::Date('Y-m-d', $this->birth_date);
        } else {
            return $this->birth_date;
        }

    }

    /**
     * Get gregorian DOB of the user
     *
     * @return string
     */
    public function getGregorianDateOfBirthAttribute()
    {
        if ($this->birth_date_type == 'gregorian') {
            return $this->birth_date;
        } else {
            return Hijri::Date('Y-m-d', $this->birth_date);
        }

    }
}
