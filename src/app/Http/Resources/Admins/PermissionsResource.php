<?php

namespace App\Http\Resources\Admins;

use Illuminate\Http\Resources\Json\JsonResource;

class PermissionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            //'id' => $this->id,
            'value' => $this->name,
            'label' => $this->name,
            'selected' => $this->selected??0,
            //'pivot' => $this->pivot
        ];
    }
}
