<?php

namespace App\Models\Investors;

use App\Models\AppModel;
use App\Models\Users\User;

class InvestorUpgradeRequest extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get user who sent the request
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get reviewer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id', 'id');
    }

    public function items()
    {
        return $this->belongsToMany(
            InvestorUpgradeItem::class,
            'investor_upgrade_request_item',
            'request_id',
            'item_id',
            'id',
            'id'
        )
            ->withPivot(['id'])
            ->using(InvestorUpgradeRequestItem::class);
    }
}
