<?php

namespace App\Models\Elm;

use App\Models\AppModel;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AbsherOtp extends AppModel
{
    use HasFactory;

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
        'expired_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
