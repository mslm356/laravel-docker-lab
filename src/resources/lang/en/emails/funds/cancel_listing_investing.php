<?php

return [
    'subject' => 'Refund investment amount ',
    'welcome_message' => 'Dear Investor  :name',
    'message_1' => 'The investment amount has been refunded :amount',
    'message_2' => '( including administration and VAT fees )',
    'message_3' => 'Number of units   :shares',
    'message_4' => 'Due to FAO Hospitality Fund being terminated according to the fund manager Alkhair Capital Saudi Arabia',
    'link_text' => 'To view your control panel, please visit the link',
    'view' => 'View dashboard',
];
