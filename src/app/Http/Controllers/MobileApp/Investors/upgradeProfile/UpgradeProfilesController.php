<?php

namespace App\Http\Controllers\MobileApp\Investors\upgradeProfile;

use App\Actions\Investors\ProfessionalUpgrade\CheckIfCanSubmitNewRequest;
use App\Enums\Investors\UpgradeRequestStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\ProfessionalUpgrade\UpgradeRequestListItemResource;
use App\Http\Resources\Investors\ProfessionalUpgrade\UpgradeRequestResource;
use App\Models\Investors\InvestorUpgradeItem;
use App\Models\Investors\InvestorUpgradeRequest;
use App\Services\Investors\UpgradeProfileServices;
use App\Services\LogService;
use App\Support\Users\UserAccess;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UpgradeProfilesController extends Controller
{

    public $upgradeProfileServices;
    public $logService;
    public $userAccess;

    public function __construct()
    {
        $this->upgradeProfileServices = new UpgradeProfileServices();
        $this->logService = new LogService();
        $this->userAccess = new UserAccess();
    }

    public function store(Request $request)
    {
        try {

            $data = $request->validate($this->upgradeProfileServices->getRules());

            $user = getAuthUser('api');

            if (!CheckIfCanSubmitNewRequest::run($user)) {
                return apiResponse(400, __('messages.investor_upgrade_request_already_exists'));
            }

            $repeatedUpgradeRequest = InvestorUpgradeRequest::whereIn(
                'status',
                [
                    UpgradeRequestStatus::Approved,
                    UpgradeRequestStatus::InReview,
                    UpgradeRequestStatus::PendingReview
                ]
            )
                ->where('user_id', $user->id)->first();

            if ($repeatedUpgradeRequest) {
                return apiResponse(400, __('messages.repeated_upgrade_request'));
            }

            if (!$this->upgradeProfileServices->store($data, $user)) {
                return apiResponse(400, __('mobile_app/message.please_try_later'));
            }

            return apiResponse(200, __('mobile_app/message.success'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-STORE-UPGRADE-PROFILE', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\ErrorException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (\Exception $e) {
            $code = $this->logService->log('STORE-UPGRADE-PROFILE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }

    }

    public function getUpgradedItems(Request $request)
    {
        $user = getAuthUser('api');

        try {
            $result = [
                'data' => InvestorUpgradeItem::where("type", $user->type)->where('is_active', true)->get()->map(fn ($item) => [
                    'id' => $item->id,
                    'text' => $item->text,
                ])
            ];
            return apiResponse(200, __('mobile_app/message.success'), $result);

        } catch (\Exception $e) {
            $code = $this->logService->log('GET-UPGRADE-PROFILE-ITEMS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }

    }

    public function index(Request $request)
    {
        try {
            $user = getAuthUser('api');

            $requests = UpgradeRequestListItemResource::collection(InvestorUpgradeRequest::where("type", $user->type)->where('user_id', $user->id)->get());

            return apiResponse(200, __('mobile_app/message.success'), $requests);

        } catch (\Exception $e) {
            $code = $this->logService->log('UPGRADE-PROFILE-DATA', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function show(Request $request, $upgradeRequest)
    {
        try {

            $user = getAuthUser('api');

            $upgradeRequest = InvestorUpgradeRequest::where('type', $user->type)->where('id', $upgradeRequest)->first();

            if (!$upgradeRequest) {
                return response()->json(['data' => ['message' => trans("messages.data_not_fount")]], 404);
            }

            if ($upgradeRequest->user_id !== $user->id) {
                throw new AuthorizationException;
            }

            return apiResponse(200, __('mobile_app/message.created_successfully'), new UpgradeRequestResource($upgradeRequest));

        } catch (\Exception $e) {
            $code = $this->logService->log('SHOW-UPGRADE-PROFILE-DATA', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }
}
