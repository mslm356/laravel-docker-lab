<?php

namespace App\Support\Aml;

interface AmlListInterface
{
    /**
     * Check the infomation in the list
     *
     * @param string|null $firstName
     * @param string|null $secondName
     * @param string|null $thirdName
     * @param string|null $fourthName
     * @param string|null $nationality
     * @param string|null $dob
     * @param string|null $nationalId
     * @param string|null $passportId
     * @return array
     */
    public function check($firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId);

    /**
     * get List index
     *
     * @return int
     */
    public function getListIndex();
}
