<?php

namespace App\Support\Elm\Nabaa;

use SoapClient;
use Illuminate\Support\Facades\App;

class NabaaSoapClient
{
    public static function instantiate()
    {
        return new SoapClient(config('services.nabaa.url'), [
            'trace' => App::environment('local'),
            'local_cert' => storage_path('app/elm/nabaa_private.pem'),
            'cache_wsdl' => WSDL_CACHE_NONE,
            'soap_version' => SOAP_1_1,
            'location' => rtrim(config('services.nabaa.url'), '?wsdl')
        ]);
    }
}
