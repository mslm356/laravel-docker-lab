<?php

namespace App\Http\Controllers\Admins\Api\AuthorizedEntities;

use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Models\AuthorizedEntity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Actions\AuthorizedEntities\CreateUserInEntity;
use App\Actions\AuthorizedEntities\SendUserInvitation;
use App\Actions\AuthorizedEntities\UpdateUserInEntity;
use App\Http\Requests\Admins\AuthorizedEntities\Users\UpdateEntityUserRequest;
use App\Http\Requests\Admins\AuthorizedEntities\Users\InviteUserToEntityRequest;
use App\Http\Resources\Admins\AuthorizedEntities\EntityUserResource;
use App\Http\Resources\Admins\AuthorizedEntities\EntityUserListItemResource;

class EntityUserController extends Controller
{
    /**
     * Show all the users of the entity
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(AuthorizedEntity $authorizedEntity)
    {
        return EntityUserListItemResource::collection(
            $authorizedEntity->users()->paginate()
        );
    }

    /**
     * Show the user information
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show(Request $request, $authorizedEntity, User $user)
    {
        return new EntityUserResource($user);
    }

    /**
     * Invite user to the entity
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\AuthorizedEntity $authorizedEntity
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function store(InviteUserToEntityRequest $request, AuthorizedEntity $authorizedEntity)
    {
        DB::transaction(function () use ($request, $authorizedEntity) {
            $data = array_merge($request->validated());

            $user = CreateUserInEntity::run($authorizedEntity, $data + ['password' => null]);

            $this->sendInvitationToUser($user);
        });

        return response()->json();
    }

    /**
     * Update user
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\AuthorizedEntity $authorizedEntity
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateEntityUserRequest $request, $authorizedEntity, User $user)
    {
        DB::transaction(function () use ($request, $user) {
            $user = UpdateUserInEntity::run($user, $request->validated());

            if ($user->password === null) {
                $this->sendInvitationToUser($user);
            }
        });

        return response()->json();
    }

    public function sendInvitationToUser($user)
    {
        SendUserInvitation::run($user, request()->user('sanctum'));
    }
}
