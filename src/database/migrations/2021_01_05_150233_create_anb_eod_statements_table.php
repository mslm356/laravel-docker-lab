<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnbEodStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anb_eod_statements', function (Blueprint $table) {
            $table->id();
            $table->string('external_id')->nullable();
            $table->dateTime('date');
            $table->string('bank_code', 10);
            $table->string('account_number');
            $table->string('currency')->nullable();
            $table->string('sequence_number')->nullable();
            $table->json('opening_balance')->nullable();
            $table->json('closing_balance')->nullable();
            $table->json('bank_api_response')->nullable();
            $table->string('file_path')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anb_eod_statements');
    }
}
