<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>كشف حساب</title>
    <style>
        @include('components.print-components.watermark',
      [
      'website' => 'https://investaseel.sa' ,
      'user_name' => 'eslam mosa' ,
      'date'=>\Carbon\Carbon::now()->format('Y-m-d H:i:s')
      ])
      body {
            font-family: "HelveticaNeueLT" !important;
        }

        html {
            height: 100%;
        }
    </style>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="flex flex-col gap-3 w-full max-w-4xl m-auto p-10">
<div class="m-auto text-center w-full max-w-[400px]">
    <img
        class="w-[150px] m-auto mb-[20px]"
        src="{{config('mail.img_domain')}}/images/identity/logo-dark.png"
        width="120"
    />
    <div class="text-3xl text-[#011291] font-semibold">كشف حساب</div>
    <div class="text-xl text-[#0366ff] font-medium">Account statement</div>
    <div
        style="direction: ltr"
        class="flex items-center p-3 rounded-lg bg-[#eef5ff] mt-5 gap-5"
    >
        <span class="text-lg text-black font-medium"> Date: 2023-02-03 </span>
        <span class="text-lg text-black font-medium"> Time: 212121211 </span>
    </div>
    <div
        style="direction: ltr"
        class="flex justify-center items-center py-2 px-3 rounded-lg bg-[#011291] mt-5 gap-4"
    >
        <img class="w-[20px]" src="{{config('mail.img_domain')}}/images/identity/logo-dark.png" width="20"/>
        <span class="text-lg text-white font-semibold">
          Investor Details | تفاصيل المستثمر
        </span>
    </div>
</div>

<div class="flex flex-col w-full gap-3">
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] pr-3"
        >
            <h6 class="font-semibold text-md">الإسم</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[400px]"
        >
            <h6 class="font-semibold text-md">  </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">name</h6>
        </div>
    </div>
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] pr-3"
        >
            <h6 class="font-semibold text-md">رقم الهوية</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[400px]"
        >
            <h6 class="font-semibold text-md">  </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">ID Number</h6>
        </div>
    </div>
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] pr-3"
        >
            <h6 class="font-semibold text-md">رصيد المحفظة</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[400px]"
        >
            <h6 class="font-semibold text-md">  </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">Wallet Balance</h6>
        </div>
    </div>
    <div class="flex justify-between items-center">
        <div
            style="border-right: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] pr-3"
        >
            <h6 class="font-semibold text-md">مجموع الاستثمارات</h6>
        </div>
        <div
            class="flex justify-center items-center w-full py-2 px-3 rounded-lg border-2 border-[#0366ff] gap-5 max-w-[400px]"
        >
            <h6 class="font-semibold text-md">  </h6>
        </div>
        <div
            style="border-left: 3px solid #e2e2e2"
            class="flex flex-row w-1/4 max-w-[175px] justify-end pl-3"
        >
            <h6 class="font-semibold text-md">Total Investments</h6>
        </div>
    </div>

    <div
        style="direction: ltr"
        class="flex justify-center items-center py-2 px-3 rounded-lg bg-[#011291] mt-3 gap-3"
    >
        <img class="w-[20px]" src="{{config('mail.img_domain')}}/images/identity/logo-dark.png" width="20"/>
        <span class="text-lg text-white font-semibold">
          Investments | عمليات الاستثمار
        </span>
    </div>
    <div class="flex justify-between items-center gap-3 text-white">
        <div
            class="flex flex-col justify-center w-[40px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center">#</h6>
        </div>
        <div
            class="flex flex-col justify-center w-[125px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center">
                Date / التاريخ
            </h6>
        </div>
        <div
            class="flex flex-col justify-center w-[300px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center">
                Fund Name / اسم الصندوق
            </h6>
        </div>
        <div
            class="flex flex-col justify-center w-[135px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center h-4">
                عدد الوحدات
            </h6>
            <h6 class="w-full font-semibold text-md text-center">
                Number of Units
            </h6>
        </div>
        <div
            class="flex flex-col justify-center w-[125px] h-12 bg-[#0366ff] rounded-lg p-1"
        >
            <h6 class="w-full font-semibold text-md text-center h-4">
                مبلغ الوحدات
            </h6>
            <h6 class="w-full font-semibold text-md text-center">Units Costs</h6>
        </div>
    </div>

    <div class="flex justify-between items-center gap-3">


    </div>
</div>
</body>
</html>
