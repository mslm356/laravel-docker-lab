<?php

namespace App\Jobs\Campaigns;

use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateUserStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $campaign;
    public $data;
    public $alertClass;
    public $logService;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign, $alertClass)
    {
        $this->alertClass = $alertClass;
        $this->campaign = $campaign;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $className = "App\\Services\\Campaigns\\" . $this->alertClass . "Type";

            $generator = new $className(null, $this->campaign);

            $generator->updateCampaignUsersStatus($this->campaign);

        } catch (\Exception $e) {
            $this->logService->log('Campaigns:update-users-status-ex', $e);
        }
    }


}
