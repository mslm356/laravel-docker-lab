<?php

return [
    'subject' => 'New Fund',
    'welcome_message' => 'Our dear investor :name',
    'message_1' => 'An opportunity to invest in the platform has been presented',
    'message_2' => 'To view the opportunity, please visit the link',
    'link_text' => 'Invest Now',
    'end_message' => 'Thank you for trusting Aseel platform',
];
