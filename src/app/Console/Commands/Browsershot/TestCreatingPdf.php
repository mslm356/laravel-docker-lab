<?php

namespace App\Console\Commands\Browsershot;

use Illuminate\Console\Command;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\DescriptionManager;
use App\Support\PdfGenerator\PdfGenerator;

class TestCreatingPdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pdf:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test creating pdf file with browsershot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $transaction = Transaction::first();

        $html = view('investors.receipt', [
            'transaction' => $transaction,
            'description' => DescriptionManager::getDescription($transaction, 'ar')
        ])
            ->render();

        PdfGenerator::outputFromHtml($html, 'pdf-tests2/test.pdf', [
            'gotoOptions' => [
                'waitUntil' => 'networkidle0'
            ]
        ]);
    }
}
