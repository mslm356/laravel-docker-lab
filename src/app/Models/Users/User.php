<?php

namespace App\Models\Users;

use App\Models\AML\AmlUser;
use App\Models\BankAccount;
use App\Models\AML\AmlHistory;
use App\Models\AuthorizedEntity;
use App\Models\CompanyProfile;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestor;
use App\Models\MemberShip\MemberShipRequests;
use App\Models\SubscriptionFormRequest;
use App\Models\UserFirebaseToken;
use Laravel\Sanctum\HasApiTokens;
use Bavix\Wallet\Traits\CanConfirm;
use Bavix\Wallet\Traits\HasWallets;
use Illuminate\Support\Facades\App;
use App\Models\Users\NationalIdentity;
use Spatie\Permission\Traits\HasRoles;
use Bavix\Wallet\Traits\HasWalletFloat;
use Illuminate\Notifications\Notifiable;
use App\Models\Investors\InvestorProfile;
use App\Models\Listings\ListingInvestorRecord;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Casts\E164PhoneNumberCast;

class User extends Authenticatable implements HasLocalePreference, JWTSubject
{
    use Notifiable, HasRoles, HasWalletFloat, HasWallets, CanConfirm ,HasApiTokens;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'jwt_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'profile' => 'collection',
        'is_active' => 'boolean',
        'data' => 'array',
        'phone_number' => E164PhoneNumberCast::class
    ];


    /**
     * Get investor profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function investor()
    {
        return $this->hasOne(InvestorProfile::class);
    }

    /**
     * Get the user investoments
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function investments()
    {
        return $this->belongsToMany(
            Listing::class,
            'listing_investor',
            'investor_id',
            'listing_id'
        )
            ->withPivot(['invested_amount', 'invested_shares','id'])
            ->withTimestamps();
    }

    /**
     * Get the user investoments
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function investmentsListUsingListingInvestor()
    {
        return $this->investments()->using(ListingInvestor::class)->orderBy('pivot_created_at');
    }

    /**
     * Get my investing records
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function investingRecords()
    {
        return $this->hasMany(ListingInvestorRecord::class, 'investor_id', 'id')->orderByDesc('id');
    }

    public function subscriptionFormRequests()
    {
        return $this->hasMany(SubscriptionFormRequest::class, 'user_id', 'id')->orderByDesc('id');
    }

    /**
     * Get my bank accounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function bankAccounts()
    {
        return $this->morphMany(BankAccount::class, 'accountable');
    }

    /**
     * Get absher identity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nationalIdentity()
    {
        return $this->hasOne(NationalIdentity::class, 'user_id', 'id');
    }

    /**
     * Get authorized entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorizedEntity()
    {
        return $this->belongsTo(AuthorizedEntity::class, 'authorized_entity_id', 'id');
    }

    /**
     * Get full name of the user
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getPhoneWithoutCountryAttribute()
    {
        if ($this->phone_number) {
            return $this->phone_number->formatForMobileDialingInCountry($this->phone_number->getCountry());
        }
        return null;
    }

    public function getPhoneCountryAttribute()
    {
        return optional($this->phone_number)->getCountry();
    }

    /**
     * Get all aml history
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function amlHistory()
    {
        return $this->hasMany(AmlHistory::class, 'investor_id', 'id');
    }


    public function memberShips()
    {
        return $this->hasMany(MemberShipRequests::class, 'user_id', 'id');
    }

    /**
     * Get user preferred locale
     *
     * @return string
     */
    public function preferredLocale()
    {
        return $this->locale;
    }

    /**
     * Did the user join
     *
     * @return boolean
     */
    public function getDidJoinAttribute()
    {
        return $this->passowrd !== null;
    }

    public function getAbsherName($locale = null)
    {
        if ($this->is_identity_verified === false || $this->nationalIdentity === null) {
            return $this->full_name;
        }

        if ($locale === null) {
            $locale = App::getLocale();
        }

        $identity = $this->nationalIdentity->setLocale($locale);

        $names = [
            $identity->first_name,
            $identity->second_name,
            $identity->third_name,
            $identity->forth_name,
        ];

        return implode(' ', array_filter($names));
    }

    public function companyProfiles()
    {
        return $this->hasMany(CompanyProfile::class, 'user_id');
    }

    public function companyProfile()
    {
        return $this->hasOne(CompanyProfile::class, 'user_company_id');
    }

    public function userFirebaseTokens()
    {
        return $this->hasMany(UserFirebaseToken::class, 'user_id');
    }

    public function amlUser()
    {
        return $this->hasMany(AmlUser::class, 'user_id', 'id');
    }

    public function amlHasOneUser()
    {
        return $this->hasOne(AmlUser::class, 'user_id', 'id');
    }

    function canAccess()
    {
        return $this->hasOne(UserCanAccess::class);
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
