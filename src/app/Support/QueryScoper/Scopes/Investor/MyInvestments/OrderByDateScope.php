<?php

namespace App\Support\QueryScoper\Scopes\Investor\MyInvestments;

use App\Support\QueryScoper\QueryScoper;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrderByDateScope extends QueryScoper
{
    public function prepareBuilder($builder, $data)
    {
        if ($data['sort_by'] === 'latest') {
            $builder->latest('pivot_created_at');
        } else {
            $builder->orderBy('pivot_created_at', 'asc');
        }

        return $builder;
    }

    public function prepareData()
    {
        return [
            'sort_by' => Request::query('sort_by', '')
        ];
    }

    public function validator($data)
    {
        return Validator::make($data, [
            'sort_by' => ['required', 'string', Rule::in(['latest', 'oldest'])]
        ]);
    }
}
