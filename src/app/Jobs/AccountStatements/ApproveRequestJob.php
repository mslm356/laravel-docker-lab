<?php

namespace App\Jobs\AccountStatements;

use App\Enums\SubscriptionForm\RequestStatus;
use App\Models\SubscriptionFormRequest;
use App\Services\SubscriptionForm\ApproveRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ApproveRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $approveRequest;
    public $subscriptionForm;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SubscriptionFormRequest $subscriptionForm)
    {
        $this->approveRequest = new ApproveRequest();
        $this->subscriptionForm = $subscriptionForm;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (in_array($this->subscriptionForm->status, [RequestStatus::PendingReview, RequestStatus::InReview])) {
            $this->approveRequest->handle($this->subscriptionForm);
        }
    }
}
