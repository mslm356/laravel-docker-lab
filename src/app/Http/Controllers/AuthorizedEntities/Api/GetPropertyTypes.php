<?php

namespace App\Http\Controllers\AuthorizedEntities\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorizedEntities\PropertyTypeResource;
use App\Models\PropertyType;

class GetPropertyTypes extends Controller
{
    /**
     * Get all the property types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        return PropertyTypeResource::collection(
            PropertyType::latest()->where('is_active', true)->get()
        );
    }
}
