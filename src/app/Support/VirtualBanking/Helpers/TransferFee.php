<?php

namespace App\Support\VirtualBanking\Helpers;

use JsonSerializable;

class TransferFee implements JsonSerializable
{
    protected $amount;
    protected $fee;
    protected $deliveryDate;

    /**
     * Create instance
     *
     * @param \Cknow\Money\Money $amount
     * @param string $fee
     * @param \Carbon\CarbonImmutable $deliveryDate
     */
    public function __construct($amount, $fee, $deliveryDate)
    {
        $this->amount = $amount;
        $this->deliveryDate = $deliveryDate;
        $this->fee = $fee;
    }

    public function getFee()
    {
        return $this->fee;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    static public function make($amount, $fee, $deliveryDate)
    {
        return new static($amount, $fee, $deliveryDate);
    }

    public function jsonSerialize()
    {
        return [
            'fee' => $this->fee,
            'delivery_date' => $this->deliveryDate
        ];
    }
}
