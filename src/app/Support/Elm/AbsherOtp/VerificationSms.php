<?php

namespace App\Support\Elm\AbsherOtp;

use App\Support\Elm\AbsherOtp\SmsStrategies\Fake;
use App\Support\Elm\AbsherOtp\SmsStrategies\AbsherOtp;

class VerificationSms
{
    private static $instance;

    public static function instance($nin = null)
    {
        if (self::$instance) {
            return self::$instance;
        }

        $instance = null;

        if (config('services.absher_otp.mode') === 'fake') {
            if (in_array($nin,config('sms.SMS_RECIPIENTS_NIN')))
                $instance = new AbsherOtp;
            else
                $instance = new Fake;
        } else {
            $instance = new AbsherOtp;
        }



        self::$instance = $instance;

        return self::$instance;
    }
}
