<?php

namespace App\Support\VirtualBanking;

use App\Models\BankAccount;

interface BankServiceInterface
{
    /**
     * Create wallet (Virtual Account)
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string|int|null $id
     * @return array
     */
    function openVirtualAccount($model, $id = null, $suffix = null): array;

    /**
     * Transfer money
     *
     * @return array|false
     * @throws \App\Exceptions\NoBalanceException
     */
    function transfer(
        array $custInfo,
        string $toIban,
        $amount,
        $bankCode,
        $tranactionId,
        $valueDate,
        $isFeeIncluded,
        $transferPurpose,
        $currency,
        BankAccount $bankAccount = null,
        $transfer = null
    );

    /**
     * Get transfer fee options
     *
     * @return array
     */
    function getTransferFeeOptions($toBankCode, $amount): array;

    /**
     * Validate that the given tranfer option can be used for transferring according to the given IBAN and amount
     *
     * @return bool
     */
    function validateTransferFeeOption($toBankCode, $amount, $option): bool;

    /**
     * Check wallet current balance
     *
     * @return float
     */
    function checkBalance(string $iban): float;

    /**
     * get wallet transaction history
     */
    function getHistory(string $iban, array $options): object;

    /**
     * Suspend/ Stop wallet
     */
    function changeStatus();
}
