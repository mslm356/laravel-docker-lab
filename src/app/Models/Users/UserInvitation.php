<?php

namespace App\Models\Users;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserInvitation extends AppModel
{
    use HasFactory;

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = null;
}
