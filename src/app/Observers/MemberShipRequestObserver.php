<?php

namespace App\Observers;

use App\Models\MemberShip\MemberShipRequests;
use App\Models\Users\User;
use App\Support\Users\UserAccess\ManageUserAccess;

class MemberShipRequestObserver
{
    private function updateMembershipRequest($memberShipRequests)
    {
        $user=User::find($memberShipRequests->user_id);
        if (!$user) {
            return;
        }

        $manageUserAccess =  new ManageUserAccess();
        $manageUserAccess->handle($user, 'memberShipRequestModule');

    }

    public function created(MemberShipRequests $memberShipRequests)
    {
        $this->updateMembershipRequest($memberShipRequests);
    }

    /**
     * Handle the MemberShipRequests "updated" event.
     *
     * @param  \App\Models\MemberShip\MemberShipRequests  $memberShipRequests
     * @return void
     */
    public function updated(MemberShipRequests $memberShipRequests)
    {
        $this->updateMembershipRequest($memberShipRequests);
    }

    /**
     * Handle the MemberShipRequests "deleted" event.
     *
     * @param  \App\Models\MemberShip\MemberShipRequests  $memberShipRequests
     * @return void
     */
    public function deleted(MemberShipRequests $memberShipRequests)
    {
        //
    }

    /**
     * Handle the MemberShipRequests "restored" event.
     *
     * @param  \App\Models\MemberShip\MemberShipRequests  $memberShipRequests
     * @return void
     */
    public function restored(MemberShipRequests $memberShipRequests)
    {
        //
    }

    /**
     * Handle the MemberShipRequests "force deleted" event.
     *
     * @param  \App\Models\MemberShip\MemberShipRequests  $memberShipRequests
     * @return void
     */
    public function forceDeleted(MemberShipRequests $memberShipRequests)
    {
        //
    }
}
