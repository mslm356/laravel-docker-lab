<?php

namespace App\Http\Controllers\Admins\Api\Campaigns;

use App\Enums\Campaigns\CampaignInvestorsType;
use App\Enums\Campaigns\CampaignsCode;
use App\Enums\Campaigns\CampaignStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Campaigns\CampaignsResource;
use App\Http\Resources\Admins\Campaigns\CampaignUsersResource;
use App\Jobs\Campaigns\SendAlert;
use App\Models\Campaigns\Campaign;
use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class CampaignsController extends Controller
{
    public $logService;

    public function __construct()
    {
        $this->middleware('permission:campaign', ['only' => ['index']]);
        $this->middleware('permission:campaign-edit', ['only' => ['update']]);
        $this->middleware('permission:campaign-view', ['only' => ['show']]);
        $this->middleware('permission:campaign-stop', ['only' => ['stopCampaign']]);

        $this->logService = new LogService();
    }

    public function index(Request $request)
    {
        $campaigns = Campaign::orderBy('id', 'desc')->paginate();
        return CampaignsResource::collection($campaigns);
    }

    public function show(Campaign $campaign)
    {
        return new CampaignsResource($campaign);
    }

    public function stopCampaign(Request $request)
    {
        try {

            $campaign = Campaign::where('id', $request->campaign_id)->where('status', '!=', CampaignStatus::Closed)->first();
            if (!$campaign) {
                return apiResponse(400, __('mobile_app/message.not_found_status_not_closed'));
            }

            $campaign->status = CampaignStatus::Closed;
            $campaign->version = Str::random(25);
            $campaign->save();

            return response()->json(['data' => [
                'message' => 'Campaigns Stopped successfully'
            ]], 201);

        } catch (\Exception $e) {
            $code = $this->logService->log('Store:Campaigns:EX', $e);
            return response()->json(['data' => [
                'message' => 'sorry please try later ' . $code
            ]], 400);
        }
    }

    public function showCampaignUser(Campaign $campaign)
    {
        return CampaignUsersResource::collection($campaign->users()->paginate());
    }

    public function store(Request $request)
    {

        try {

            $data = $this->validate($request, $this->getRules());

            return DB::transaction(function () use ($data, $request) {

                $campaignData = $request->only(
                    'name',
                    'investors_type',
                    'start_at',
                    'type',
                    'title_ar',
                    'title_en',
                    'description_ar',
                    'description_en'
                );

                $campaignData['version'] = Str::random(25);

                $investorType = CampaignInvestorsType::getKey($campaignData['investors_type']);

                $data['investors_type'] = $investorType;
                $campaignData['investors_type'] = $investorType;

                $campaign = Campaign::create($campaignData);

                $this->handleSendText($data, $campaign);

                return response()->json(['data' => [
                    'message' => 'Campaigns Created successfully'
                ]], 201);

            }, 1);

        } catch (ValidationException $e) {

            $code = $this->logService->log('Store:Campaigns:EX', $e);
            $errors = handleValidationErrors($e->errors());
            return response()->json(['data' => [
                'message' => $e->getMessage() . ' ' . $code, 'errors' => $errors
            ]], 422);
        } catch (\Exception $e) {
            $code = $this->logService->log('Store:Campaigns:EX', $e);
            return response()->json(['data' => [
                'message' => 'sorry please try later ' . $code
            ]], 400);
        }
    }

    public function update(Request $request, Campaign $campaign)
    {
        try {
            $data = $this->validate($request, $this->getRules());

            $changedVariables = $this->getChangesInUpdate($campaign, $request->all());
            $newVersionStatus = $this->saveNewVersion($changedVariables);

            return DB::transaction(function () use ($data, $request, $campaign, $newVersionStatus) {

                $campaignData = $request->only(
                    'name',
                    'investors_type',
                    'start_at',
                    'type',
                    'title_ar',
                    'title_en',
                    'description_ar',
                    'description_en'
                );

                $investorType = CampaignInvestorsType::getKey($campaignData['investors_type']);

                $data['investors_type'] = $investorType;
                $campaignData['investors_type'] = $investorType;

                if ($newVersionStatus['detach_users']) {

                    $campaign->users()->detach();
                    $campaignData['version'] = Str::random(25);
                    $campaign->update($campaignData);
                    $this->handleSendText($data, $campaign);

                } elseif ($newVersionStatus['new_version']) {

                    $campaignData['version'] = Str::random(25);
                    $campaign->update($campaignData);

                    $start_at = Carbon::parse($data['start_at']);

                    $alertClass = CampaignsCode::getKey($data['type']);

                    $sendAlertJob = (new SendAlert($data, $campaign, $alertClass));
                    dispatch($sendAlertJob)
                        ->onQueue('campaigns')
                        ->delay($start_at);

                } else {
                    $campaign->update($campaignData);
                }

                return response()->json(['data' => [
                    'message' => 'Campaigns Created successfully'
                ]], 201);

            }, 1);

        } catch (ValidationException $e) {

            $code = $this->logService->log('Store:Campaigns:EX', $e);
            $errors = handleValidationErrors($e->errors());
            return response()->json(['data' => [
                'message' => $e->getMessage() . ' ' . $code, 'errors' => $errors
            ]], 422);

        } catch (\Exception $e) {

            $code = $this->logService->log('Store:Campaigns:EX', $e);
            return response()->json(['data' => [
                'message' => 'sorry please try later ' . $code
            ]], 400);
        }
    }

    public function handleOldCampaign($campaign)
    {

        DB::table('campaign_users')
            ->where('campaign_id', $campaign->id)
            ->delete();


        //redis

        $command = 'php artisan queue:clear  --queue=campaigns';

        //        shell_exec($command);
        //        exec($command)
    }

    public function getRules()
    {
        return [
            'name' => ['required', 'string', 'max:100',textValidation()],
            'investor_ids' => ['nullable', 'string'],
            'investors_type' => [
                'required',
                'in:1,2,3,4,5,6,7'
            ],
            'type' => ['required', 'integer', 'in:1,2,3,4'],
            'title_en' => ['nullable', 'string', 'max:100',textValidation()],
            'title_ar' => ['nullable', 'string', 'max:100',textValidation()],
            'description_en' => ['required', 'string', 'max:1000'],
            'description_ar' => ['required', 'string', 'max:1000'],
            'start_at' => ['required', 'date', 'after_or_equal:now'],
        ];
    }

    public function getOptions()
    {
        $campaignOptionsCode = [
            'Custom',
            'MembershipLevel',
            'InCompletedKycType',
            'upgradedInvestors',
            'listingInvestorType',
            'SuitableInvestor',
            'UnsuitableInvestor',
        ];

        foreach ($campaignOptionsCode as $key => $value) {

            $options[] = [
                'value' => CampaignInvestorsType::getValue($value),
                'label' => __("$value"),
            ];
        }

        return response()->json([
            'data' => $options,
        ]);
    }

    public function handleSendText($data, $campaign)
    {

        $className = "App\\Services\\Campaigns\\" . CampaignsCode::getKey($data['type']) . "Type";

        if (!class_exists($className)) {
            throw ValidationException::withMessages(['message' => 'class not valid ' . CampaignsCode::getKey($data['type'])]);
        }

        $generator = new $className($data, $campaign);

        $generator->handle();
    }

    public function getChangesInUpdate($campaign, $requestData)
    {
        $changes = [];

        $variables = [
            'name', 'investors_type', 'type', 'title_en',
            'title_ar', 'description_en', 'description_ar', 'start_at'
        ];

        foreach ($variables as $variable) {

            if ($campaign->$variable != $requestData[$variable]) {
                $changes[] = $variable;
            }
        }

        if (in_array('investor_ids', array_keys($requestData))) {

            $campaignUsersIds = DB::table('campaign_users')
                ->where('campaign_id', $campaign->id)
                ->select('user_id')
                ->pluck('user_id')
                ->toArray();


            $dif = array_diff($campaignUsersIds, explode(',', $requestData['investor_ids']));

            if (count($dif)) {
                $changes[] = 'investor_ids';
            }
        }

        return $changes;
    }

    public function saveNewVersion($changedVariables)
    {

        $response = [
            'new_version' => false,
            'detach_users' => false,
        ];

        $changedDataForNewVersion = ['start_at', 'investors_type', 'investor_ids',
            'type', 'description_en', 'description_ar'
        ];

        foreach ($changedDataForNewVersion as $variable) {

            if (in_array($variable, $changedVariables)) {
                $response['new_version'] = true;
                $response['detach_users'] = $variable != 'start_at' ? true : $response['detach_users'];
            }
        }

        return $response;
    }
}
