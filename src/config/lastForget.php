<?php

return [

    'email' => [
        'attempts' => env('FORGET_EMAIL_ATTEMPTS', 3),
        'wait_minutes' => env('FORGET_EMAIL_WAITING_MIN', 15),
        'another_attempt_minutes' => env('FORGET_EMAIL_ATTEMPT_TIME', 15),
    ],

    'password' => [
        'attempts' => env('FORGET_PASSWORD_ATTEMPTS', 3),
        'wait_minutes' => env('FORGET_PASSWORD_WAITING_MIN', 15),
        'another_attempt_minutes' => env('FORGET_PASSWORD_ATTEMPT_TIME', 15),
    ],

    'phone' => [
        'attempts' => env('FORGET_PHONE_ATTEMPTS', 3),
        'wait_minutes' => env('FORGET_PHONE_WAITING_MIN', 15),
        'another_attempt_minutes' => env('FORGET_PHONE_ATTEMPT_TIME', 15),
    ],

];
