<?php


namespace App\Support\Elm\Yaqeen;

use App\Enums\Elm\AbsherOtpReason;
use App\Enums\IdentitySourceType;
use App\Models\Country;
use App\Support\Elm\AbsherOtp\Exceptions\CustomerException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Elm\Yaqeen\Exceptions\BirthDateException;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use App\Support\Elm\Yaqeen\Exceptions\MissingInfoRelatedToIdException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Alkoumi\LaravelHijriDate\Hijri;

class YaqeenInfoService
{

    public function yaqeenInfo($data)
    {
        $yaqeen = Yaqeen::instance();

        try {
            if (Str::startsWith($data['nin'], '1')) {
                if (isset($data['type']) && $data['type'] == 'mobile_app') {
                    $hijriDate = Hijri::Date('m-Y', $data['birth_date']);
                    $info = $yaqeen->getCitizenInfo($data['nin'], $hijriDate);
                } else {
                    $info = $yaqeen->getCitizenInfo($data['nin'], $data['birthDateInstance']->format('m-Y'));
                }

            } else {
                $info = $yaqeen->getInfoByIqama($data['nin'], $data['birthDateInstance']->format('m-Y'));
            }
        } catch (IdException $th) {
            throw ValidationException::withMessages([
                'nin' => __('messages.invalid_nin'),
            ]);
        } catch (BirthDateException $th) {
            throw ValidationException::withMessages([
                'birth_date' => __('messages.invalid_nin_birth_date'),
            ]);
        }

        return $info;
    }

    public function yaqeenAddress($data)
    {

        $yaqeen = Yaqeen::instance();

        try {

            if (Str::startsWith($data['nin'], '1')) {
                if (isset($data['type']) && $data['type'] == 'mobile_app') {
                    $hijriDate = Hijri::Date('m-Y', $data['birth_date']);
                    $citizenAddresses = $yaqeen->getCitizenAddress($data['nin'], $hijriDate, 'A');
                } else {
                    $citizenAddresses = $yaqeen->getCitizenAddress($data['nin'], $data['birthDateInstance']->format('m-Y'), 'A');
                }
                $addresses = array_map(function ($address) {
                    return collect($address)
                        ->map(fn ($el) => ['ar' => $el])
                        ->toArray();
                }, $citizenAddresses);

            } else {
                $citizenAddresses = $yaqeen->getAddressInfoByIqama($data['nin'], $data['birthDateInstance']->format('m-Y'), 'A');

                $addresses = array_map(function ($address) {
                    return collect($address)
                        ->map(fn ($el) => ['ar' => $el])
                        ->toArray();
                }, $citizenAddresses);
            }

        } catch (IdException $th) {
            throw ValidationException::withMessages([
                'nin' => __('messages.invalid_nin'),
            ]);
        } catch (BirthDateException $th) {
            throw ValidationException::withMessages([
                'birth_date' => __('messages.invalid_nin_birth_date'),
            ]);
        } catch (MissingInfoRelatedToIdException $th) {
            throw ValidationException::withMessages([
                'nin' => __('messages.no_national_addresses_found_in_yaqeen'),
            ]);
        }

        return $addresses;
    }

    public function sendSms($data, $user)
    {
        try {

            $verificationMsg = VerificationSms::instance()->send(
                $data['nin'],
                $user,
                [
                    'reason' => __('absher_otp_purpose.' . AbsherOtpReason::VerifyIdentity, [], $user->locale ?? App::getLocale())
                ]
            );

        } catch (CustomerException $th) {
            throw ValidationException::withMessages([
                'nin' => __('messages.no_phone_found_in_absher_otp'),
            ]);
        }

        return $verificationMsg;
    }

    public function updateInvestorProfile($user, $info, $addresses, $verificationMsg)
    {

        $user->investor->update([
            'is_identity_verified' => false,
            'data->identity_verification_tmp' => [
                'vid' => $verificationMsg->id,
                'identity' => $info + [
                        'user_id' => $user->id,
                        'nin_country_id' => Country::where('iso2', 'SA')->first()->id,
                        'source' => IdentitySourceType::Absher,
                    ],
                'addresses' => $addresses

            ]
        ]);

        return true;
    }
}
