<?php

namespace App\Console\Commands\Funds;

use App\Enums\Role;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Jobs\UpdateFundPercentageFirebase;

class UpdateFundPercentage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'funds:update-percentage {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update fund percentage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listing = Listing::find($this->option('id'));

        if (!$listing) {
            dd('fund not valid');
        }

        $listing->update([
            'invest_percent' => $listing->percentage
        ]);

        UpdateFundPercentageFirebase::dispatch($listing->id, $listing->percentage)->onQueue('firebase');

        return true;
    }
}
