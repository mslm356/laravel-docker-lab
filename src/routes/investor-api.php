<?php

use App\Actions\CreateNewEnquiry;
use App\Actions\Investors\DownloadFile;
use App\Actions\Investors\GetFile;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Enums\Role;
use App\Http\Controllers\Common\BankAccounts\BankAccountController;
use App\Http\Controllers\Investors\Auth\AbsherResetPassword;
use App\Http\Controllers\Investors\Auth\RegisterController;
use App\Http\Controllers\Investors\Auth\SendSmsVerificationCode;
use App\Http\Controllers\Investors\Auth\ShowAbsherInfo;
use App\Http\Controllers\Investors\Auth\UpdateInvestorProfile;
use App\Http\Controllers\Investors\Auth\VerifyNin;
use App\Http\Controllers\Investors\BankAccounts\GetApprovedBankAccounts;
use App\Http\Controllers\Investors\Dashboard\GetMyInvestments;
use App\Http\Controllers\Investors\Dashboard\GetMyUnits;
use App\Http\Controllers\Investors\Dashboard\GetOverview;
use App\Http\Controllers\Investors\Dashboard\GetOverviewTimeline;
use App\Http\Controllers\Investors\Dashboard\GetTransactionsHistory;
use App\Http\Controllers\Investors\Dashboard\InvestorWalletController;
use App\Http\Controllers\Investors\General\GenerateFileUrlController;
use App\Http\Controllers\Investors\General\GetTermsAndConditions;
use App\Http\Controllers\Investors\Investing\ConvertSharesToAmount;
use App\Http\Controllers\Investors\Investing\CreateSubscriptionToken;
use App\Http\Controllers\Investors\Investing\GetMyInvestingRecords;
use App\Http\Controllers\Investors\Investing\InvestInListing;
use App\Http\Controllers\Investors\Investing\SendOtpToAuthorizeInvesting;
use App\Http\Controllers\Investors\Investing\VerifyOtpToAuthorizeInvesting;
use App\Http\Controllers\Investors\Listing\DiscussionController;
use App\Http\Controllers\Investors\Listing\GetListingTermsAndConditions;
use App\Http\Controllers\Investors\Listing\ListingController as InvestorListingController;
use App\Http\Controllers\Investors\Listing\ListingReportController;
use App\Http\Controllers\Investors\Listing\VoteController;
use App\Http\Controllers\Investors\MemberShips\MemberShipController;
use App\Http\Controllers\Investors\Profile\CompanyProfileController;
use App\Http\Controllers\Investors\Profile\GetAuthUser;
use App\Http\Controllers\Investors\Profile\Kyc\GetKycOptions;
use App\Http\Controllers\Investors\Profile\Kyc\InvestorKycController;
use App\Http\Controllers\Investors\Profile\ProfessionalUpgrade\CanSubmitNewRequest;
use App\Http\Controllers\Investors\Profile\ProfessionalUpgrade\CreateUpgradeRequest;
use App\Http\Controllers\Investors\Profile\ProfessionalUpgrade\GetUpgradeItems;
use App\Http\Controllers\Investors\Profile\ProfessionalUpgrade\UpgradeRequestController;
use App\Http\Controllers\Investors\Profile\ShowMyProfile;
use App\Http\Controllers\Investors\Profile\UpdateAddress;
use App\Http\Controllers\Investors\Profile\UpdateInfo;
use App\Http\Controllers\Investors\Profile\UpdatePassword;
use App\Http\Controllers\Investors\SubscriptionForm\RequestsController;
use App\Http\Controllers\Investors\SystemSettings\GetSystemSettings;
use App\Http\Controllers\MobileApp\Investors\Sdad\ChargeWalletController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Investors\ComplaintController;
use App\Http\Controllers\Investors\HyperPay\HyperPayController;

Route::get('register/options', GetKycOptions::class);
Route::get('register/content/terms-and-conditions', GetTermsAndConditions::class);
Route::post('register', [RegisterController::class, 'register']);
Route::post('/absher/verify', VerifyNin::class)->middleware('recaptcha.v3');
Route::post('/absher/forget-password', [AbsherResetPassword::class,'sendResetPasswordCode'])->middleware('recaptcha.v3');
Route::post('/absher/verify-reset-password', [AbsherResetPassword::class,'verfiyCode'])->middleware('recaptcha.v3');
Route::post('/absher/reset-password', [AbsherResetPassword::class,'resetPassword'])->middleware('recaptcha.v3');



Route::middleware(['auth:sanctum', 'role:' . Role::Investor])->group(function () {
    Route::get('complaints/types', [ComplaintController::class, 'types']);

    Route::post('complaints', [ComplaintController::class, 'store']);

    Route::post('/sdad/export/invoices', [ChargeWalletController::class, 'create_invoice'])->middleware('user-can-access:investment:web');
    Route::get('/auth', GetAuthUser::class);
    Route::get('/absher', ShowAbsherInfo::class);

    Route::post('/absher/send-sms-verification', SendSmsVerificationCode::class)->middleware('recaptcha.v3');

    Route::get('/profile', ShowMyProfile::class);
    Route::put('/profile/update-info', UpdateInfo::class);
    Route::put('/profile/update-address', UpdateAddress::class);
    Route::put('/profile/update-password', UpdatePassword::class);
    Route::put('/profile/investor', UpdateInvestorProfile::class)->middleware('recaptcha.v3');
    Route::get('register/options-answer', InvestorKycController::class); // get kyc for investor
    Route::post('/confirm-unsuitability', [UpdateInvestorProfile::class,'confirmUnsuitability']);

    Route::middleware('investor.profile')->group(function () {
        Route::get('/professional-upgrade/items', GetUpgradeItems::class);
        Route::get('/professional-upgrade/requests/can-submit', CanSubmitNewRequest::class);
        Route::get('/professional-upgrade/requests', [UpgradeRequestController::class, 'index']);
        Route::get('/professional-upgrade/requests/{upgrade_request}', [UpgradeRequestController::class, 'show']);
        Route::post('/professional-upgrade/requests', CreateUpgradeRequest::class)
            ->middleware('user-can-access:upgradeRequest:web');

    });

    Route::get('auth-fund-details/{listing}', [InvestorListingController::class, 'fundDetailsWithAuth']);
    //    Route::get('/files/{file}/get-url', GetFileUrl::class)->name('files.get.url');
    Route::get('/files/{file}/get-url', GenerateFileUrlController::class)->name('files.get.url');
    Route::get('/files/investor-record/{investor_record_id}/get-url', [GenerateFileUrlController::class,'getInvestorFileUrl'])->name('investor-files.get.url');
});

Route::middleware(['auth:sanctum', 'role:' . Role::Investor, 'investor.profile'])
    ->group(function () {
        Route::get('/system-settings', GetSystemSettings::class);
        Route::post('/enquiries', CreateNewEnquiry::class)->middleware('recaptcha.v3');
        Route::post('/transfer', [InvestorWalletController::class, 'transfer'])
            ->middleware(['user-can-access:transfer:web','recaptcha.v3']);
        Route::post('/transfer/fees', [InvestorWalletController::class, 'getTransferFeeOptions']);
        Route::get('/investments', GetMyInvestments::class);
        Route::get('/balance-inquiry', [InvestorWalletController::class, 'balanceInquiry']);
        Route::get('/transactions', GetTransactionsHistory::class);
        Route::get('/investing-records', GetMyInvestingRecords::class);
        Route::get('bank-accounts/approved', GetApprovedBankAccounts::class);
        Route::apiResource('bank-accounts', BankAccountController::class)->only(['index', 'store']);


        Route::get('/files/{file}/download', DownloadFile::class)->name('files.download');
        Route::get('/files/{file}/get', GetFile::class)->name('files.get');

        // TODO: need review
        Route::get('/dashboard/my-units', GetMyUnits::class);
        // TODO: need review
        Route::get('/dashboard/overview', GetOverview::class);
        // TODO: need review
        Route::get('/dashboard/overview-timeline', GetOverviewTimeline::class);

        Route::get('/listings/{listing}/reports', [ListingReportController::class, 'index']);
        Route::post('/listings/{listing}/discussions', [DiscussionController::class, 'store']);
        Route::get('/listings/{listing}/terms-conditions', GetListingTermsAndConditions::class);

        //     COMPANY PROFILE
        Route::post('/company-profile-create', [CompanyProfileController::class, 'store']);
        Route::get('/company-profile-show', [CompanyProfileController::class, 'show']);

        //     Subscription Form Requests
        Route::post('/subscription-form-request-store', [RequestsController::class, 'store']);

        // MEMBER SHIP
        Route::get('/membership-accept', [ MemberShipController::class , 'approveMemberShip']);
        Route::post('update-membership-status', [ MemberShipController::class , 'updateMembershipStatus']);


        // VOTE
        Route::post('/answer-vote', [VoteController::class, 'store']);
        Route::get('/listing/{listing}/votes', [VoteController::class, 'getListingVotes']);


        // hyperPay
        Route::post('payment-checkout',[ HyperPayController::class , 'checkout']);
        Route::post('get-payment-status',[ HyperPayController::class , 'getPaymentStatus']);
        Route::get('get-payment-methods',[ HyperPayController::class , 'getPaymentMethods']);
    });

Route::get('/listings/{listing}/discussions', [DiscussionController::class, 'index']);

Route::get('listings/{listing}', [InvestorListingController::class, 'show']);


Route::get('fund-details/{listing}', [InvestorListingController::class, 'fundDetailsWithoutAuth']);


Route::middleware(['auth:sanctum', 'role:' . Role::Investor])
    ->group(function () {
        Route::post('/listings/{listing}/shares-to-amount', ConvertSharesToAmount::class);
        Route::post('/listings/{listing}/subscription-token', CreateSubscriptionToken::class)->middleware(['recaptcha.v3', 'user-can-access:investment:web']);
        Route::get('/listings/{listing}/subscription-data/{investor_subscription_token:reference}', GetSubscriptionFormData::class);
        Route::post('/invest/{token:reference}/send-otp', SendOtpToAuthorizeInvesting::class)->middleware('recaptcha.v3');
        Route::post('/invest/{token:reference}/verify-otp', VerifyOtpToAuthorizeInvesting::class)->middleware('recaptcha.v3');
        Route::post('/invest/{token}', InvestInListing::class)->middleware(['recaptcha.v3', 'user-can-access:investment:web']);
    });
