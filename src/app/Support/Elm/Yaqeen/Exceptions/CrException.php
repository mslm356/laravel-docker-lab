<?php

namespace App\Support\Elm\Yaqeen\Exceptions;

use Exception;

class CrException extends Exception
{
    public function __construct()
    {
        parent::__construct('CR is not correct');
    }
}
