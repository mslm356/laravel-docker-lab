<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorBase;

class DefaultGenerator extends GeneratorBase
{
    public function generateForCredit(Transaction $transaction, $locale)
    {
        return '';
    }

    public function generateForDebit(Transaction $transaction, $locale)
    {
        return '';
    }
}
