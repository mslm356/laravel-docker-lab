<?php

namespace App\Http\Middleware;

use App\Enums\Investors\UserAccessMessages;
use App\Support\Users\UserAccess\ManageUserAccess;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserCanAccess
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $variables)
    {
        try {

            $variables = explode(':', $variables);

            $module = $variables[0];
            $guard = $variables[1];

            $auth = getAuthUser($guard);

            $userAccess = DB::table('user_can_accesses')
                ->where('user_id', $auth->id)
                ->first();

            if (!$userAccess) {
                $manageUserAccess = new ManageUserAccess();
                $manageUserAccess->handle($auth);

                $userAccess = DB::table('user_can_accesses')
                    ->where('user_id', $auth->id)
                    ->first();

            }

            if ($userAccess) {

                $request->request->add(['user_can_access_data'=>$userAccess]);
                if ($userAccess->$module == 200) {
                    return $next($request);
                }

                $message = 'user_access_messages.' . UserAccessMessages::getKey($userAccess->$module);

                return apiResponse(400, __($message));
            } else {
                return apiResponse(400, __('User cannot access this data'));
            }

        } catch (\Exception $e) {
            return apiResponse(400, __('User cannot access this data'));
        }

    }
}
