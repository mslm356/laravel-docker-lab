<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAuthorizedEntityIdColumnToListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->unsignedBigInteger('authorized_entity_id')->after('type_id')->nullable();

            $table->foreign('authorized_entity_id', 'listings_authorized_entity_id_foreign')
                ->references('id')
                ->on('authorized_entities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listings', function (Blueprint $table) {
            $table->dropForeign('listings_authorized_entity_id_foreign');
            $table->dropColumn('authorized_entity_id');
        });
    }
}
