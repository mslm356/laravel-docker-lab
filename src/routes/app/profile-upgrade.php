<?php

use \App\Http\Controllers\MobileApp\Investors\upgradeProfile\UpgradeProfilesController ;
use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\bankAccounts\BankAccountsController;


Route::middleware(['auth:api', 'jwt_check:api', 'role:' .Role::Investor])->group(function () {


    Route::get('professional-upgrade-items',[ UpgradeProfilesController::class , 'getUpgradedItems']);
    Route::get('professional-upgrade-requests',[ UpgradeProfilesController::class , 'index']);
    Route::get('professional-upgrade-show-request/{id}',[ UpgradeProfilesController::class , 'show']);
    Route::post('/professional-upgrade-create-request',[ UpgradeProfilesController::class , 'store'])
        ->middleware('user-can-access:upgradeRequest:api');

});
