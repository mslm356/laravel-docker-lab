<?php

namespace App\Jobs;

use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotificationFirebaseBulk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $usersCollection;
    private $data;
    private $type;
    private $logService;
    private $serverKey;
    private $chunkNumber;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $data, Collection $usersCollection)
    {
        $this->type = $type;
        $this->data = $data;
        $this->usersCollection = $usersCollection->whereNotNull('firebase_token');
        $this->logService = new LogService();
        $this->serverKey = config('notification.server_key');
        $this->chunkNumber = 1000;
    }

    /**
     * Execute the job.
     *
     *
     */
    public function handle()
    {
        try {
            $arUsers = $this->usersCollection->where('locale', 'ar');
            $enUsers = $this->usersCollection->where('locale', 'en');

            foreach ($arUsers->chunk($this->chunkNumber) as $arUsers) {
                $this->sendNotificationToFirebase($this->type, $this->data, $arUsers->pluck('firebase_token')->toArray(), 'ar');
            }

            foreach ($enUsers->chunk($this->chunkNumber) as $enUsers) {
                $this->sendNotificationToFirebase($this->type, $this->data, $enUsers->pluck('firebase_token')->toArray(), 'en');
            }

        } catch (\Exception $e) {
            $this->logService->log('JOB-NOTIFICATION-EXCEPTION', $e);
        }
    }

    public function sendNotificationToFirebase($notificationType, $data, array $userTokens, $locale)
    {
        $msg = [
            'title' => isset($data['title_' . $locale]) ? $data['title_' . $locale] : '',
            'body' => isset($data['content_' . $locale]) ? $data['content_' . $locale] : '',
            'action_id' => isset($data['action_id']) ? $data['action_id'] : '',
            'action_type' => $notificationType
        ];

        $notification = [

            'title' => isset($data['title_' . $locale]) ? $data['title_' . $locale] : '',
            'body' => isset($data['content_' . $locale]) ? $data['content_' . $locale] : '',
            'sound' => true,
        ];

        $fcmNotification = [
            'registration_ids' => $userTokens,
            'notification' => $notification,
            'data' => $msg
        ];

        $headers = [
            'Authorization:key=' . $this->serverKey,
            'Content-Type: application/json'
        ];

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

        $res_json = curl_exec($ch);

        $result = json_decode($res_json) ? get_object_vars(json_decode($res_json)) : [];

        $code = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
        $merge1 = array_merge($code, $fcmNotification);
        $merge = array_merge($result, $merge1);
        $reposeDet = array('response' => $merge);
        curl_close($ch);

        Log::info('Done');

        Log::info(json_encode($reposeDet, true));

        return $reposeDet;
    }
}
