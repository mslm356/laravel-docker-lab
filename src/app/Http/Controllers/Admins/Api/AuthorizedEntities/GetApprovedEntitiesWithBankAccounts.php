<?php

namespace App\Http\Controllers\Admins\Api\AuthorizedEntities;

use App\Enums\AuthorizedEntityStatus;
use App\Enums\BankAccountStatus;
use App\Enums\Banking\BankCode;
use App\Models\AuthorizedEntity;
use App\Http\Controllers\Controller;

class GetApprovedEntitiesWithBankAccounts extends Controller
{
    /**
     * Show all the approved entities with their approved accounts
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __invoke()
    {
        $entities = AuthorizedEntity::where('status', AuthorizedEntityStatus::Approved)
            ->with([
                'bankAccounts' => function ($query) {
                    $query->where('status', BankAccountStatus::Approved);
                }
            ])->get();

        return response()->json([
            'data' => $entities->map(fn ($entity) => [
                'id' => $entity->id,
                'name' => $entity->name,
                'bank_accounts' => $entity->bankAccounts->map(fn ($account) => [
                    'id' => $account->id,
                    'name' => BankCode::getDescription($account->bank) . ' - ' . $account->iban
                ])
            ])
        ]);
    }
}
