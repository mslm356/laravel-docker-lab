<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateInvestmentStoredProcdure extends Migration
{

    public function up()
    {

        DB::unprepared("DROP PROCEDURE IF EXISTS investment_procedure");

        $sql2="CREATE PROCEDURE investment_procedure(IN p_user_id INT,IN p_user_class VARCHAR(200),IN p_user_wallet_id INT,IN p_listing_id INT,IN p_total_shares INT,IN p_subscription_token_id INT,IN p_invested_shares INT,IN p_amount_without_fees float,IN p_fee float,
                IN p_vat float,IN p_invested_amount float,IN p_vatPercentage float,IN p_feePercentage float,IN p_currency VARCHAR(5),IN p_meta JSON,IN p_vat_meta JSON,IN p_fee_meta JSON,OUT success INT)


  sp: BEGIN
            DECLARE p_total_invested_shares INT;
            DECLARE p_wallet_balance INT;

            DECLARE EXIT HANDLER FOR SQLEXCEPTION
                BEGIN
                   SHOW ERRORS;
                    ROLLBACK;
                END;

START TRANSACTION;

            update listings set investment_shares = investment_shares - p_invested_shares  where id = p_listing_id;

             INSERT INTO `listing_investor_records`
            (`investor_id`,`listing_id`,`subscription_token_id`,`invested_shares`,`amount_without_fees`,`admin_fees`,`vat_amount`,`vat_percentage`,`admin_fees_percentage`,`currency`,`created_at`,`updated_at`)
            VALUES(p_user_id,p_listing_id,p_subscription_token_id,p_invested_shares,p_amount_without_fees,p_fee,p_vat,p_vatPercentage,p_feePercentage,p_currency,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());


            INSERT INTO `transactions` (`amount`,`confirmed`,`payable_id`,`type`,`uuid`,`wallet_id`,`meta`,`payable_type`,`created_at`,`updated_at`)
            VALUES
              (p_amount_without_fees * -1,1,p_user_id,'withdraw',UUID(),p_user_wallet_id,p_meta,JSON_UNQUOTE(JSON_EXTRACT(meta, '$.user_class')),CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP()),
              (p_fee * -1,1,p_user_id,'withdraw',UUID(),p_user_wallet_id,p_fee_meta,JSON_UNQUOTE(JSON_EXTRACT(meta, '$.user_class')),CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP()),
              (p_vat * -1,1,p_user_id,'withdraw',UUID(),p_user_wallet_id,p_vat_meta,JSON_UNQUOTE(JSON_EXTRACT(meta, '$.user_class')),CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());




            INSERT INTO listing_investor (`listing_id`,`investor_id`,`invested_shares`,`invested_amount`,`currency`,`created_at`,`updated_at`)
            VALUES(p_listing_id,p_user_id,p_invested_shares,p_amount_without_fees,p_currency,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())
            ON DUPLICATE KEY UPDATE
            invested_shares=invested_shares + p_invested_shares,invested_amount= invested_amount + p_amount_without_fees,updated_at=CURRENT_TIMESTAMP();

            select sum(amount) into p_wallet_balance from transactions where wallet_id=p_user_wallet_id;

              IF p_wallet_balance < 0 THEN
                SELECT CONCAT('wallet_balance is less than 0 -- the wallet_id_is', p_user_wallet_id , ' and wallet_balance_is ', p_wallet_balance);
                 ROLLBACK;
                 LEAVE sp;
              END IF;

            update wallets set is_overview_statistics_consistent = 0,balance = p_wallet_balance,updated_at=CURRENT_TIMESTAMP() where id = p_user_wallet_id;

            update investor_subscription_tokens set
            expired_at = CURRENT_TIMESTAMP(),
            is_expire = 1,
            updated_at = CURRENT_TIMESTAMP()
            where
              id = p_subscription_token_id;


            SELECT 'investment done';

COMMIT;



  END";

        DB::unprepared($sql2);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE IF EXISTS investment_procedure");

    }
}
