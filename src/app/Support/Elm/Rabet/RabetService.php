<?php


namespace App\Support\Elm\Rabet;

use App\Services\LogService;
use Illuminate\Support\Facades\Http;

class RabetService
{
    private $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }

    public function checkRabetPhone($nin, $phone)
    {
        try {
            $response= Http::withHeaders([
                'APP-ID' => config('elm.rabet.app_id'),
                'APP-KEY' => config('elm.rabet.app_key'),
                'Service_key' => config('elm.rabet.service_key'),
            ])->get(config('elm.rabet.host').'/api/v1/person/'.$nin.'/owns-mobile/'.$phone);


            $this->logServices->log('RABET-RESPONCE', null, ['data', $response->body()]);

            $data=$response->json();
            if ($response->successful()) {
                return $data['isOwner'];
            } else {
                return $data['message'];
            }

        } catch (\Exception $e) {
            $code=$this->logServices->log('RABET-CHECK-PHONE-EXCEPTION', $e);
            return __('mobile_app/message.please_try_later' . ' : ' . $code);
        }
    }
}
