<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Enums\Elm\AbsherOtpReason;
use App\Http\Controllers\Controller;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\User;
use App\Services\Investors\RegisterServices;
use App\Services\Investors\Users\LastForgetService;
use App\Services\LogService;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ForgetPasswordController extends Controller
{
    public $registerServices;
    public $logService;
    public $lastForgetService;

    public function __construct()
    {
        $this->registerServices = new RegisterServices();
        $this->logService = new LogService();
        $this->lastForgetService = new LastForgetService();
    }

    public function forgetPassword(Request $request)
    {
        try {


            $data = $request->only('phone', 'phone_country_iso2');

            Validator::make($data, [
                'phone' => ['required', 'string', 'phone:phone_country_iso2']
            ])->validate();

            $phoneNumber = phone($data['phone'], "SA" /*$request['phone_country_iso2']*/);

            $responseOfLastForget = $this->lastForgetService->handle($phoneNumber, 'password');

            if (isset($responseOfLastForget['status']) && !$responseOfLastForget['status']) {
                $msg = isset($responseOfLastForget['msg']) ? $responseOfLastForget['msg'] : 'please try later';
                return apiResponse(400, __($msg));
            }

            $user = User::where('phone_number', $phoneNumber)->where('type', 'user')->first();

            if (!$user) {
                return apiResponse(400, __('messages.data_not_correct'));
            }

            if (!$user->nationalIdentity) {
                return apiResponse(400, __('mobile_app/message.please_active_your_account'));
            }


            $verificationSms = VerificationSms::instance()->send(
                $user->nationalIdentity->nin,
                $user,
                [
                    'reason' => __('absher_otp_purpose.' . AbsherOtpReason::ForgetPassword, [], $user->locale ?? App::getLocale())
                ]
            );

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-APP-FORGET-PASSWORD', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage().' : '.$code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('APP-FORGET-PASSWORD', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later').' : '.$code);
        }

        return apiResponse(200, __('mobile_app/message.success'), ['vid' => $verificationSms->id]);
    }

    public function verifyForgetPasswordCode(Request $request)
    {

        try {

            $data = $request->only('vid', 'code');

            Validator::make($data, [
                'vid' => 'required|string|exists:absher_otps,id',
                'code' => 'required',
            ])->validate();

            $absherOtp = AbsherOtp::find($data['vid']);

            if (!$absherOtp) {
                return apiResponse(400, __('mobile_app/message.vid_not_valid'));
            }

            $user = $absherOtp->user;

            $nin = $user && $user->nationalIdentity ? $user->nationalIdentity->nin : null;

            $isVerifiedResponse = $this->verify($nin, $absherOtp, $data['code']);

            if (isset($isVerifiedResponse['status']) && !$isVerifiedResponse['status']) {

                $message = isset($isVerifiedResponse['message']) ? $isVerifiedResponse['message'] : __('mobile_app/message.please_try_later');
                return apiResponse(400, __($message));
            }

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-APP-VERIFY-FORGET-PASSWORD-CODE', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('APP-VERIFY-FORGET-PASSWORD-CODE', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }

        return apiResponse(200, __('mobile_app/message.code_valid'));
    }

    public function resetPassword(Request $request)
    {
        try {

            $data = $request->only('vid', 'code', 'password', 'password_confirmation');

            Validator::make($data, [
                'vid' => 'required|string|exists:absher_otps,id',
                'code' => 'required',
                'password' => ['required', 'string', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/', 'confirmed'],
            ])->validate();

            $absherOtp = AbsherOtp::find($data['vid']);

            if (!$absherOtp) {
                return apiResponse(400, __('mobile_app/message.vid_not_valid'));
            }

            $user = $absherOtp->user;

            $nin = $user && $user->nationalIdentity ? $user->nationalIdentity->nin : null;

            $isVerifiedResponse = $this->verify($nin, $absherOtp, $data['code']);

            if (isset($isVerifiedResponse['status']) && !$isVerifiedResponse['status']) {

                $message = isset($isVerifiedResponse['message']) ? $isVerifiedResponse['message'] : 'mobile_app/message.please_try_later';
                return apiResponse(400, __($message));
            }

            $this->updatePassword($user, $absherOtp, $request['password']);

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-APP-RESET-PASSWORD', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('APP-RESET-PASSWORD', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }

        return apiResponse(200, __('mobile_app/message.success'));
    }

    public function verify($nationalId, $msg, $enteredCode)
    {
        $response = ['status' => true];

        $now = strtotime(Carbon::now()->subHour()->format('Y-m-d H:i:s'));
        $absherCreatedAt = strtotime($msg->created_at);

        if ($absherCreatedAt < $now || ($msg->expired_at ?? null) !== null) {

            $response['status'] = false;
            $response['message'] = 'this code expired';
            return $response;
        }

        if ($msg->customer_id !== $nationalId) {

            $response['status'] = false;
            $response['message'] = 'national id not valid';
            return $response;
        }

        if (strlen($enteredCode) == 4 && $msg->code == $enteredCode) {
            return $response;
        }

        if (false === Hash::check($enteredCode, $msg->verification_code)) {

            $response['status'] = false;
            $response['message'] = 'code not valid';
            return $response;
        }

        return $response;
    }

    public function updatePassword($user, $absherOtp, $password)
    {
        $absherOtp->update([
            'expired_at' => now()
        ]);

        $user->password = Hash::make($password);
        $user->save();
    }
}
