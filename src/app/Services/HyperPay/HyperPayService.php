<?php


namespace App\Services\HyperPay;

use App\Enums\Banking\TransactionReason;
use App\Enums\HyperPay\HyperPayStatus;
use App\Jobs\Hyperpay\TransactionAlert;
use App\Models\HyperPay\HyperPayTransactions;
use App\Models\HyperPay\RegistrationTokens;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Services\LogService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;


class HyperPayService
{
    public $base_url;
    public $webhook_url;
    public $password;
    public $logServices;
    public $entityIds;

    public function __construct()
    {
        $this->entityIds = [
            'ApplePay' => config('hyperPay.apple_pay_entity'),
            'visa' => config('hyperPay.visa_entity'),
        ];
        $this->password = config('hyperPay.password');
        $this->base_url = config('hyperPay.base_url'); //'https://test.oppwa.com/v1/checkouts';
        $this->webhook_url = config('app.url') . '/api/app/check-transaction-webhook';
        $this->logServices = new  LogService();
    }

    public function HandleCheckout($user, $data)
    {
        $TransactionReference = Str::random('30');

        $checkoutData = $this->checkoutData($data, $user, $TransactionReference);

        $data['checkout_request'] = $checkoutData;
        $data['reference'] = $TransactionReference;

        $hyperPayTransaction = $this->createHyperPayTransaction($user, $data);

        $response = $this->callHyperPay($this->base_url . 'checkouts', 'POST', $checkoutData);

        $this->updateHyperPayTransaction($hyperPayTransaction, $response);

        return $hyperPayTransaction;
    }

    public function createHyperPayTransaction($user, $data)
    {
        $transactionData = [
            'user_id' => $user->id,
            'payment_method' => $data['payment_method'],
            'amount' => money_parse_by_decimal($data['amount'], defaultCurrency())->getAmount(),
            'checkout_request' => json_encode($data['checkout_request']),
            'reference' => $data['reference'],
        ];

        return HyperPayTransactions::create($transactionData);
    }

    public function callHyperPay($url, $method, $data = null)
    {

        $headers = array(
            'Authorization:Bearer ' . $this->password,
        );

        $curl = curl_init($url);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
        }
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        if (!empty($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    public function updateHyperPayTransaction($hyperPayTransaction, $response)
    {

        $responseData = json_decode($response, 1);

        $responseData = [
            'checkout_response' => $response,
            'checkout_id' => isset($responseData['id']) ? $responseData['id'] : $hyperPayTransaction->checkout_id,
        ];

        $hyperPayTransaction->update($responseData);
    }

    public function checkoutValidation()
    {
        return [
            'amount' => 'required|numeric|max:10000000|min:1',
            'payment_method' => 'required|string|in:VISA,MASTER,Mada,ApplePay',
        ];
    }

    public function getPaymentStatusValidation()
    {
        return [
            'checkout_id' => 'required|string',
//            'token' => 'nullable|string',
        ];
    }

    public function checkoutData($requestData, $user, $TransactionReference)
    {

        $address = $this->userAddress($user);

        $entityId = $requestData['payment_method'] == 'ApplePay' ? $this->entityIds['ApplePay'] : $this->entityIds['visa'];

        $data = "entityId=" . $entityId .
            "&amount=" . number_format($requestData['amount'], 2, '.', '') .
            "&currency=" . defaultCurrency() .
            "&paymentType=DB" .
//            "&testMode=EXTERNAL" .  // for test
//            "&customParameters[3DS2_enrolled]=true" .  // for test
            "&merchantTransactionId=" . $TransactionReference .
            "&customer.email=" . $user->email .
            "&billing.street1=" . $address['street_name'] .
            "&billing.city=" . $address['city'] .
            "&billing.state=state" .
            "&billing.country=SA" .
            "&billing.postcode=" . $address['post_code'] .
            "&customer.givenName=" . $user->full_name .
            "&customer.surname=" . $user->last_name;

        $tokens = DB::table('registration_tokens')
            ->where('user_id', $user->id)
            ->select('token')
            ->get();

        // insert saved card in checkout id
        foreach ($tokens as $index => $token) {
            $data .= "&registrations[$index].id=$token->token";
        }

        return $data;
    }

    public function HandleGetPaymentStatus($user, $data)
    {

        $hyperPayTransaction = DB::table('hyper_pay_transactions')
            ->where('user_id', $user->id)
            ->where('checkout_id', $data['checkout_id'])
            ->where('status', HyperPayStatus::Pending)
            ->select('id', 'checkout_id', 'user_id', 'payment_method', 'amount', 'status')
            ->first();

        if (!$hyperPayTransaction) {
            throw ValidationException::withMessages(['message' => 'transaction not valid']);
        }

        $response = $this->checkPaymentStatus($hyperPayTransaction->payment_method, $data['checkout_id']);

        return $this->updateHyperPayTransactionPaymentStatus($hyperPayTransaction, $response);
    }

    public function checkPaymentStatus($payment_method, $checkout_id)
    {

        $entityId = $payment_method == 'ApplePay' ? $this->entityIds['ApplePay'] : $this->entityIds['visa'];

        $url = $this->base_url . 'checkouts/' . $checkout_id . '/payment' . "?entityId=" . $entityId;

        return $this->callHyperPay($url, 'GET');
    }

    public function checkPaymentStatusResponse($response, $regex = null)
    {

        $regex = $regex ?? "/^(000\.000\.|000\.100\.1|000\.[36])/";

        if (isset($response['result']) &&
            isset($response['result']['code']) &&
            preg_match($regex, $response['result']['code'])) {
            return true;
        }

        return false;
    }

    public function updateHyperPayTransactionPaymentStatus($hyperPayTransaction, $response)
    {

        $data = [
            'payment_status_response' => $response
        ];

        $responseValues = json_decode($response, 1);

        if (isset($responseValues['registrationId'])) {
            $this->saveRegistrationToken($hyperPayTransaction->user_id, $responseValues);
        }

        $data['status'] = $this->checkPaymentStatusResponse($responseValues) ? HyperPayStatus::Paid : HyperPayStatus::Unpaid;

        DB::table('hyper_pay_transactions')
            ->where('id', $hyperPayTransaction->id)
            ->update($data);

        if ($data['status'] == HyperPayStatus::Paid) {
            $this->payForUser($hyperPayTransaction);
        }

        return [
            'status' => $data['status'],
            'statusCode' => isset($responseValues['result']) && isset($responseValues['result']['code']) ? $responseValues['result']['code'] : ""
        ];
    }

    public function payForUser($hyperPayTransaction)
    {
        $wallet = DB::table('wallets')
            ->where('holder_id', $hyperPayTransaction->user_id)
            ->where('holder_type', User::class)
            ->select('id', 'balance')
            ->first();

        $meta = [
            'reason' => TransactionReason::HyperPayReason,
            'reference' => transaction_ref(),
            'value_date' => now('UTC')->toDateTimeString(),
            'source' => 'hyperPay',
        ];

       $transactionId =  DB::table('transactions')->insertGetId([
            'payable_type' => User::class,
            'payable_id' => $hyperPayTransaction->user_id,
            'wallet_id' => $wallet->id,
            'type' => 'deposit',
            'amount' => $hyperPayTransaction->amount,
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $investorBalance = DB::table('transactions')
            ->where('wallet_id', $wallet->id)
            ->where('payable_type', User::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('id', $wallet->id)
            ->update([
                'balance' => $investorBalance,
                'is_overview_statistics_consistent' => false
            ]);

        $transactionAlertJob = (new TransactionAlert($transactionId));
        dispatch($transactionAlertJob);
    }


    public function paymentApiMethods()
    {
        return [
            [
//                'url' => 'https://raw.githubusercontent.com/UNICODE-Venture/unicode-moyasar/main/assets/images/cards.png',
                'url' => 'https://investaseel.sa/images/payments/cards.png',
                'key' => 'VISA',
                'name' => '',
                'channel' => ["MADA", "MASTER", "VISA"],
            ],

            [
                'url' => 'https://investaseel.sa/images/payments/apple_pay.png',
                'key' => 'ApplePay',
                'name' => '',
                'channel' => ['APPLEPAY'],
            ],
        ];
    }

    public function paymentMethods()
    {
        return [
            [
//                'url' => 'https://raw.githubusercontent.com/UNICODE-Venture/unicode-moyasar/main/assets/images/cards.png',
                'url' => 'https://investaseel.sa/images/payments/cards.png',
                'key' => 'VISA',
                'name' => '',
                'channel' => ["MADA", "MASTER", "VISA"],
            ]
        ];
    }

    public function userAddress($user)
    {

        $address = DB::table('national_identities')
            ->where('user_id', $user->id)
            ->join('identity_addresses', function ($join) {
                $join->on('national_identities.id', 'identity_addresses.identity_id');
            })
            ->select('city', 'street_name', 'post_code')
            ->first();

        $data = [
            'city' => '',
            'street_name' => '',
            'post_code' => ''
        ];

        if (!$address) {
            return $data;
        }

        foreach ($data as $key => $item) {

            $value = json_decode($address->$key, 1);

            if (is_array($value) && (isset($value['ar']) || isset($value['en']))) {
                $data[$key] = isset($value['ar']) ? $value['ar'] : $value['en'];
            }
        }

        return $data;
    }

    public function saveRegistrationToken($userId, $responseValues)
    {

        $subMask = isset($responseValues['card']) && isset($responseValues['last4Digits']) ? $responseValues['last4Digits'] : null;

        $tokenExists = DB::table('registration_tokens')
            ->where('user_id', $userId)
            ->where('sub_mask', $subMask)
            ->count();

        if ($tokenExists) {

            DB::table('registration_tokens')
                ->where('user_id', $userId)
                ->update(['token' => $responseValues['registrationId']]);

            return true;
        }

        RegistrationTokens::create([
            'user_id' => $userId,
            'token' => $responseValues['registrationId'],
            'sub_mask' => isset($responseValues['card']) && isset($responseValues['last4Digits']) ? $responseValues['last4Digits'] : null,
        ]);

        return true;
    }

    function hyperPayStatusArray()
    {
        return [
            '800.100.100' ,
            '800.100.150' ,
            '800.100.151' ,
            '800.100.152' ,
            '800.100.153' ,
            '800.100.154' ,
            '800.100.155' ,
            '800.100.156' ,
            '800.100.157' ,
            '800.100.158' ,
            '800.100.159' ,
            '800.100.160' ,
            '800.100.161' ,
            '800.100.162' ,
            '800.100.163' ,
            '800.100.164' ,
            '800.100.165' ,
            '800.100.166' ,
            '800.100.167' ,
            '800.100.168' ,
            '800.100.169' ,
            '800.100.170' ,
            '800.100.171' ,
            '800.100.172' ,
            '800.100.173' ,
            '800.100.174' ,
            '800.100.175' ,
            '800.100.176' ,
            '800.100.177' ,
            '800.100.178' ,
            '800.100.179' ,
            '800.100.190' ,
            '800.100.191' ,
            '800.100.192' ,
            '800.100.195' ,
            '800.100.196' ,
            '800.100.197' ,
            '800.100.198' ,
            '800.100.199' ,
            '900.100.300' ,
            '900.100.301' ,
            '800.400.200' ,
            '800.300.401' ,
            '800.120.101' ,
            '800.120.102' ,
            '800.120.103' ,
            '600.200.500' ,
            '100.150.203' ,
            '200.100.603' ,
            '100.550.312'
        ];

    }

}
