<?php

namespace App\Services\Campaigns\InvestorTypes;

use Illuminate\Support\Facades\DB;

class SuitableInvestorType
{

    public function getUsersData(array $selectedData, array $customData = [])
    {
        $data =  DB::table('investor_profiles')
            ->where('investor_profiles.annual_income', '!=', 1)
            ->whereNotNull('investor_profiles.annual_income')
            ->whereJsonContains('data->investment_xp', 3)
            ->whereJsonContains('data->years_of_inv_in_securities', '>', 0)
            ->join('users', 'users.id', '=', 'investor_profiles.user_id')
            ->select($selectedData)
            ->orderBy('investor_profiles.user_id', 'asc')
            ->get();

        return json_decode($data, 1);
    }

}
