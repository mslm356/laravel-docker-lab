<?php

use App\Enums\InvestmentType;
use App\Enums\Banking\BankCode;
use App\Enums\BankAccountStatus;
use App\Enums\Investors\InvestorStatus;
use App\Enums\Investors\Registration\Level;
use App\Enums\Investors\Registration\NetWorth;
use App\Enums\Investors\Registration\Education;
use App\Enums\Investors\Registration\MoneyRange;
use App\Enums\Investors\Registration\Occupation;
use App\Enums\Investors\Registration\CashOutPeriod;
use App\Enums\Investors\Registration\InvestorEntity;
use App\Enums\Investors\Registration\CurrentInvestment;
use App\Enums\Investors\Registration\InvestmentObjective;
use App\Enums\Elm\NatheerNotifiaction;

return [

    BankCode::class => [
        BankCode::Alawwal => 'Alawwal Bank',
        BankCode::Albilad => 'Bank AlBilad',
        BankCode::Anb => 'Arab National Bank',
        BankCode::Aljazira => 'Bank AlJazira',
        BankCode::MuscatBank => 'Muscat Bank',
        BankCode::Bnp => 'BNP Paribas',
        BankCode::Alfarnsi => 'Banque Saudi Fransi',
        BankCode::ChaseMorgan => 'J.P. Morgan Chase N.A',
        BankCode::Deutsche => 'Deutsche Bank',
        BankCode::Emirates => 'Emirates NBD',
        BankCode::GulfInt => 'Gulf International Bank Saudi Arabia (GIB-SA)',
        BankCode::IndCommChina => 'Industrial and Commercial Bank of China (ICBC)',
        BankCode::Alinma => 'Alinma Bank',
        BankCode::BahrainBank => 'National Bank of Bahrain (NBB)',
        BankCode::KuwaitBank => 'National Bank of Kuwait (NBK)',
        BankCode::PakistanBank => 'National Bank Of Pakistan (NBP)',
        BankCode::Ncb => 'Saudi National Bank',
        BankCode::RiyadhBank => 'Riyad Bank',
        BankCode::Rajhi => 'Al Rajhi Bank',
        BankCode::Sabb => 'The Saudi British Bank (SABB)',
        BankCode::Sama => 'Saudi Central Bank',
//        BankCode::Samba => 'Samba Financial Group (Samba)',
        BankCode::Sib => 'Saudi Investment Bank',
        BankCode::TurkeyZiraat => 'T.C. ZIRAAT BANKASI A.S.',
        BankCode::QatarBank => 'Qatar National Bank (QNB)',
        BankCode::MUFGBank => 'MUFG Bank, Ltd.',
        BankCode::AbuDhabiBank => 'First Abu Dhabi Bank',
    ],
    InvestmentType::class => [
        InvestmentType::Development => 'Development',
        InvestmentType::IncomeGenerating => 'Income generating',
        InvestmentType::InfrastructureDevelopment => 'Infrastructure Development',
        InvestmentType::PrivateEquity => 'Private Equity',
    ],
    CashOutPeriod::class => [
        CashOutPeriod::ShortTerm => 'Short term (less than 1 year)',
        CashOutPeriod::MediumTerm => 'Medium term (1 year - 5 years)',
        CashOutPeriod::LongTerm => 'Long term (more than 10 years)',
    ],
    InvestmentObjective::class => [
        InvestmentObjective::ProtectionOfCapital => 'Protection of capital',
        InvestmentObjective::Income => 'Income',
        InvestmentObjective::Balanced => 'Balanced',
        InvestmentObjective::GrowthOfCapital => 'Growth of capital',
    ],
    Level::class => [
        Level::Low => 'Low',
        Level::Medium => 'Medium',
        Level::High => 'High',
    ],
    Education::class => [
        Education::Postgraduate => 'Postgraduate',
        Education::Undergraduate => 'Undergraduate',
        Education::HighSchool => 'High school',
        Education::Other => 'Other',
    ],
    Occupation::class => [
        Occupation::Retired => 'Retired',
        Occupation::Employee => 'Employee',
        Occupation::Employer => 'Business owner',
        Occupation::Unemployed => 'Unemployed',
        Occupation::Student => 'Student',
        Occupation::Other => 'Other',
    ],
    MoneyRange::class => [
        MoneyRange::Between1000To50k => '1,000 - 50,000',
        MoneyRange::Between50kTo120k => '50,001 - 100,000',
        MoneyRange::Between120kTo250k => '100,001 - 200,000',
        MoneyRange::Between250kTo500k => '200,001 - 500,000',
        MoneyRange::Over500k => 'Over 500,000',
    ],
    CurrentInvestment::class => [
        CurrentInvestment::RealEstate => 'Real estate',
        CurrentInvestment::CaptialMarket => 'Capital market',
        CurrentInvestment::PrivateEquity => 'Private Equity',
        CurrentInvestment::Other => 'Other',
    ],
    NetWorth::class => [
        NetWorth::LessThan5MillionsSAR => 'Less than 5 millions SAR',
        NetWorth::MoreThan5MillionsSAR => 'More than 5 millions SAR',
    ],
    BankAccountStatus::class => [
        BankAccountStatus::PendingReview => 'Pending review',
        BankAccountStatus::InReview => 'In review',
        BankAccountStatus::Rejected => 'Rejected',
        BankAccountStatus::Approved => 'Approved',
    ],
    InvestorEntity::class => [
        InvestorEntity::Firm => 'Firm',
        InvestorEntity::Individual => 'Individual'
    ],
    InvestorStatus::class => [
        InvestorStatus::InReview => 'In review',
        InvestorStatus::PendingReview => 'Pending review',
        InvestorStatus::Rejected => 'Rejected',
        InvestorStatus::Approved => 'Approved',
        InvestorStatus::AutoAmlCheckFailed => 'Failed to check AML',
        InvestorStatus::Suspended => 'Suspended',
    ],
    BankAccountStatus::class => [
        BankAccountStatus::InReview => 'In review',
        BankAccountStatus::PendingReview => 'Pending review',
        BankAccountStatus::Rejected => 'Rejected',
        BankAccountStatus::Approved => 'Approved',
    ],
    NatheerNotifiaction::class => [
        NatheerNotifiaction::Death_Indicator_Changed => 'Death Indicator Changed',
        NatheerNotifiaction::Final_ExitVisa => 'Final Exit Visa',
        NatheerNotifiaction::Crossing_out_with_Final_ExitVisa => 'Crossing out with Final Exit Visa',
        NatheerNotifiaction::Final_ExitVisa_Canceled => 'Final ExitVisa Canceled',
        NatheerNotifiaction::DL_Issued_Renewed => 'DL Issued Renewed',
        NatheerNotifiaction::IQAMA_Renewed => 'IQAMA Renewed',
        NatheerNotifiaction::IQAMA_Issued_for_borderNumber => 'IQAMA Issued for border Number',
        NatheerNotifiaction::Exit_with_ReEntry_Visa_but_has_not_returned_after_Visa_expiry => 'Exit with ReEntry Visa but has not returned after Visa expiry',
    ],



];
