<?php

namespace App\Jobs\Listings;

use App\Models\Listings\ListingInvestor;
use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class AfterValuation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $listing;
    public $logService;
    public function __construct($listing)
    {
        $this->listing = $listing;
        $this->logService = new LogService();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            ListingInvestor::where('listing_id', $this->listing->id)->chunk(2000, function ($investors) {

                DB::table('wallets')
                    ->where('holder_type', User::class)
                    ->whereIn('holder_id', $investors->pluck('investor_id')->toArray())
                    ->update(['is_overview_statistics_consistent'=>false]);

            });

        } catch (\Exception $e) {
            $this->logService->log('After-Valuation-Job-Exc', $e);

        }
    }
}
