<?php

namespace App\Http\Middleware;

use App\Services\LogService;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;

class ValidateRecaptchaV3Token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (App::environment('local') && $request->input('g_token') === null) {
            return $next($request);
        }

        $data = $request->validate([
            'g_token' => ['required', 'string']
        ]);

        $response = app('recaptcha')->validate($data['g_token']);

        if ($response['success'] && $response['score'] >= 0.1) {
            return $next($request);
        }

        try {
            $log_service=new LogService();
            $log_service->log('recaptcha-responce', null, $response);

        } catch (\Exception $e) {

        }

        throw ValidationException::withMessages([
            'g_token' => __('validation.recaptcha')
        ])
            ->status(418);
    }
}
