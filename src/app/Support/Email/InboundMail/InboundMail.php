<?php

namespace App\Support\Email\InboundMail;

use App\Models\Enquiries\EnquiryToken;

class InboundMail
{
    /** @var \App\Support\Email\InboundMail\InboundMailInterface */
    private $driver = null;

    public function __construct($driver = null)
    {
        if (is_null($driver)) {
            $driver = config('inbound-mail.service');
        }

        $driverClassName = $this->resolveClassName($driver);

        $this->setDriver($driverClassName);
    }

    public function getEnquiry($data)
    {
        $to = $this->getToAddress($data);
        $from = $this->getFromAddress($data);

        [$username,] = explode('@', $to, 2);

        $token = EnquiryToken::findToken($username);

        if (is_null($token) || $from !== $token->enquiry->email) {
            abort($this->failureHttpCode());
        }

        return $token->enquiry;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function resolveClassName($driverName)
    {
        return 'App\Support\Email\InboundMail\Strategies\\' . ucfirst($driverName);
    }

    public function setDriver($driverClassName)
    {
        return $this->driver = new $driverClassName;
    }

    public function __call($name, $parameters)
    {
        return $this->getDriver()->$name(...$parameters);
    }
}
