<?php

namespace App\Mail\Investors;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewInvestmentTransaction extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $listing;
    public $toBeInvestedShares;
    public $amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Listing $listing, $toBeInvestedShares, $amount)
    {
        $this->user = $user;
        $this->listing = $listing;
        $this->toBeInvestedShares = $toBeInvestedShares;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';

        $profileUrl = config('app.investor_url') . '/app/overview';

        $amount = $this->amount;

        $fund_title = $this->listing["title_" . $this->user->locale ] ;

        $units = $this->toBeInvestedShares ;

        return $this->view(
            'emails.investments.investingList',
            compact('name', "fund_title", 'amount', 'units', 'profileUrl')
        )
            ->subject(__('emails/funds/recieve_investment_amount.subject'));

    }

}
