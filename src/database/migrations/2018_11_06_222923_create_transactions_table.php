<?php

declare(strict_types=1);

use Bavix\Wallet\Models\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    public function up(): void
    {
        Schema::create($this->table(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('payable');
            $table->unsignedBigInteger('wallet_id');
            $table->enum('type', ['deposit', 'withdraw'])->index();
            $table->decimal('amount', 64, 0);
            $table->boolean('confirmed');
            $table->json('meta')->nullable();
            $table->uuid('uuid')->unique();
            $table->timestamps();

            $table->index(['payable_type', 'payable_id'], 'payable_type_payable_id_ind');
            $table->index(['payable_type', 'payable_id', 'type'], 'payable_type_ind');
            $table->index(['payable_type', 'payable_id', 'confirmed'], 'payable_confirmed_ind');
            $table->index(['payable_type', 'payable_id', 'type', 'confirmed'], 'payable_type_confirmed_ind');
        });

        DB::statement("ALTER TABLE `{$this->table()}` ADD COLUMN `reference` varchar(255) GENERATED ALWAYS AS (`meta` ->> '$.reference') STORED NOT NULL AFTER `meta`;");
        DB::statement("ALTER TABLE `{$this->table()}` ADD COLUMN `reason` TINYINT(3) UNSIGNED GENERATED ALWAYS AS (`meta` ->> '$.reason') STORED NOT NULL AFTER `meta`;");
        DB::statement("ALTER TABLE `{$this->table()}` ADD COLUMN `external_reference` varchar(255) GENERATED ALWAYS AS (`meta` ->> '$.external_reference') STORED AFTER `meta`;");
        DB::statement("ALTER TABLE `{$this->table()}` ADD COLUMN `value_date` DATETIME GENERATED ALWAYS AS (`meta` ->> '$.value_date') STORED AFTER `meta`;");

        Schema::table($this->table(), function (Blueprint $table) {
            $table->index(['reason', 'external_reference'], 'transactions_reason_ext_ref_index');
            $table->index('reference');
        });
    }

    public function down(): void
    {
        Schema::drop($this->table());
    }

    private function table(): string
    {
        return (new Transaction())->getTable();
    }
}
