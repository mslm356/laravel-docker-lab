<?php

namespace App\Http\Resources\Common\NationalIdentities;

use Illuminate\Http\Resources\Json\JsonResource;

class NationalIdentityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nin' => $this->nin,
            'nationality' => $this->nationality === 'saudi' ? 'السعودية' : $this->nationality,
            'first_name' => $this->first_name,
            'second_name' => $this->second_name,
            'third_name' => $this->third_name,
            'forth_name' => $this->forth_name,
            'id_expiry_date' => $this->id_expiry_date,
            'birth_date_type' => $this->birth_date_type,
            'birth_date' => $this->birth_date,
            'gender' =>   $this->gender_identity,
            'addresses' => IdentityAddressResource::collection($this->addresses),

        ];
    }
}
