<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class RegistrationOption extends Enum
{
    const Education =   'EDUCATION';
    const Occupation =   'OCCUPATION';
    const CurrentInvest =   'CURRENT_INVEST';
}
