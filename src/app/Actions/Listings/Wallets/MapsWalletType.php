<?php

namespace App\Actions\Listings\Wallets;

use App\Enums\Banking\WalletType;
use App\Enums\Listings\ListingWallet;

trait MapsWalletType
{
    protected $walletTypeMap = [
        ListingWallet::Spv => WalletType::ListingSpv,
        ListingWallet::Escrow => WalletType::ListingEscrow,
        ListingWallet::EscrowVat => WalletType::ListingEscrowVat,
        ListingWallet::EscrowAdminFee => WalletType::ListingEscrowAdminFee,
    ];

    protected function getMappedWalletType($type)
    {
        return $this->walletTypeMap[$type];
    }

    protected function getWalletTypeKey($value)
    {
        return array_search($value, $this->walletTypeMap);
    }
}
