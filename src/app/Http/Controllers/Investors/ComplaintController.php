<?php

namespace App\Http\Controllers\Investors;

use App\Http\Resources\Admins\Complaints\ComplaintsListItemResource;
use App\Models\ComplaintType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Complaint\StoreRequest;
use App\Services\Complaints\StoreComplaintService;
use App\Http\Resources\Admins\Complaints\ComplaintTypesListItemResource;

class ComplaintController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $complaintService = new StoreComplaintService($request->validated());
        $complaint = $complaintService->handle();

        return apiResponse(200, __('app.complaintCreated'), ComplaintsListItemResource::make($complaint));

    }

    public function types()
    {
        return apiResponse(200, __('mobile_app/message.success'),  ComplaintTypesListItemResource::collection(ComplaintType::all()));

    }
}
