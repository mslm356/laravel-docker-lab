<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNationalIdentitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('national_identities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->unique();
            $table->unsignedSmallInteger('nin_country_id');
            $table->unsignedTinyInteger('source');
            $table->string('nin');
            $table->json('first_name')->nullable();
            $table->json('second_name')->nullable();
            $table->json('third_name')->nullable();
            $table->json('forth_name')->nullable();
            $table->date('id_expiry_date')->nullable();
            $table->string('birth_date_type')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('gender')->nullable();
            $table->string('nationality')->nullable();
            $table->timestamps();

            $table->foreign('nin_country_id')->references('id')->on('countries');
            $table->unique(['nin', 'nin_country_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('national_identities');
    }
}
