<?php

namespace App\Console\Commands;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;

use App\Mail\TestingSupport;

use App\Models\Users\User;
use App\Services\LogService;

use Carbon\Carbon;
use Illuminate\Console\Command;


class WalletRecharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallet:recharge {--amount=} {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    public $logService;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logServices = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        try {

//            if (config('app.wallet_recharge') != 'testing') {
//                dd('not valid');
//            }

            $users = User::query()
                ->where('id', $this->option('user_id'))
                ->get();

            foreach ($users as $user)
            {

                if (!$user) {
                    dump('user not valid');
                    continue;
                }

                $userWallet = $user->getWallet(WalletType::Investor);

                $userWallet->deposit(
                    $this->option('amount'),
                    [
                        'reference' => transaction_ref(),
                        'reason' => TransactionReason::AnbStatement,
                        'details' => [
                            'version' => 1,
                            'source' => 'console_a',
                        ],
                        'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
                    ]
                );

                $userWallet->refreshBalance();

                dump('done');

            }

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }


}
