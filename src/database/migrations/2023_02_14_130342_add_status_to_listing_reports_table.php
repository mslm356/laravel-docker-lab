<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToListingReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listing_reports', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('reviewer_id')->nullable();
            $table->text('reviewer_comment')->nullable();
            $table->tinyInteger('status')->default(3)->comment('default => approved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listing_reports', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('reviewer_id')->nullable();
            $table->text('reviewer_comment')->nullable();
            $table->tinyInteger('status')->default(3)->comment('default => approved');
        });
    }
}
