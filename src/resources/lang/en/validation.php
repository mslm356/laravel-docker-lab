<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    'phone' => 'The :attribute field contains an invalid number.',
    'iban' => 'The :attribute field is invalid IBAN',
    'recaptcha' => 'Failed to pass reCaptcha',
    'incorrect_password' => 'The password is incorrect',
    'numeric_string' => 'The :attribute must be a numeric string.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => 'The password must be 8 characters long or more, must be English characters and must have at least one lower case, one upper case, one number and special characters (!@#$%^&*).',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [

        'items.*.files'            => 'files',
        'items.*'                  => 'this option',
        'items.*.id'                  => 'this option',

        'iban' => 'IBAN',
        'bank_account_id' => 'bank account',
        'current_invest' => 'what are you currently invested in?',
        'net_worth' => 'approximate net worth',
        'annual_income' => 'annual income',
        'expected_annual_invest' => 'amount expected to be invested annualy',
        'expected_invest_amount_per_opportunity' => 'amount expected to be invested for each opportuniy',
        'annual_yield' => 'how much annual yield are you looking for?',
        'source_of_income' => 'source of Income',
        'invest_objective.*' => 'Investment objectives',
        'citizen' => 'nationality',
        'more_general_info.any_xp_in_fin_sector' => 'Has the client worked in the financial sector during the past five years?',
        'more_general_info.any_xp_related_to_fin_sector' => 'Does the client have any other practical experience related to the financial sector?',
        'more_general_info.xp_related_to_fin_sector' => 'Your practical experience related to the financial sector',
        'more_general_info.any_other_fin_situation' => 'Any other financial information on the client’s financial situation?',
        'more_general_info.has_prominent_functions' => 'Is the client entrusted with prominent public functions in the Kingdom or a foreign country, senior management positions, or a position in an international organization?',
        'more_general_info.is_close_to_prominent_functions' => 'Does the client have a relationship (by blood or marriage up to the second degree), or have an association with a person entrusted with a prominent public function in the Kingdom or a foreign country, senior management positions, or a position in an international organization?',
        'is_on_board' => 'Is the client a board of directors member, an audit committee member or a senior executive in a listed company?',
        'more_general_info.is_close_to_on_board' => 'Does the client have a close association with a board of directors member, an audit committee member or a senior executive in a listed company?',
        'more_general_info.is_beneficial_owner' => 'Is the client the beneficial owner of the account or business relationship?',
        'more_general_info.identity_of_account_owner' => 'identity of the beneficial owner of the account or business relationship',
        'more_financial_info.expected_cash_out_period' => 'period during which the client expects to cash out his invested money',
        'more_financial_info.securities_trans_outside_ksa_over_past_5_years' => 'Securities transactions outside the Kingdom over the past five years',
        'more_financial_info.in_which_countries_trans_executed' => 'If securities transactions were executed outside the Kingdom over the past five years, in which countries were these transactions executed?',
        'more_financial_info.investment_xp' => 'Investment knowledge and experience',
        'more_financial_info.loan_to_inv_money_ratio' => 'Loan to invested money ratio',
        'more_financial_info.margin_trans_over_past_5_years' => 'Margin transactions over the past five years',
        'more_financial_info.preferred_inv_assets.in_foreign_currencies' => 'What are the client’s preferred investment assets?',
        'more_financial_info.preferred_inv_assets.in_sar' => 'What are the client’s preferred investment assets?',
        'more_financial_info.preferred_inv_assets.foreign_currencies' => 'Forign currencies',
        'more_financial_info.products_prev_inv_in' => 'Products previously invested in',
        'more_financial_info.prof_certs' => 'Professional certificates',
        'more_financial_info.risk_appetite' => 'Appetite for risk',
        'more_financial_info.years_of_inv_in_securities' => 'Number of years of investment in securities',


        'general_info.any_xp_in_fin_sector' => 'Has the client worked in the financial sector during the past five years?',
        'general_info.any_xp_related_to_fin_sector' => 'Does the client have any other practical experience related to the financial sector?',
        'general_info.xp_related_to_fin_sector' => 'Your practical experience related to the financial sector',
        'general_info.any_other_fin_situation' => 'Any other financial information on the client’s financial situation?',
        'general_info.has_prominent_functions' => 'Is the client entrusted with prominent public functions in the Kingdom or a foreign country, senior management positions, or a position in an international organization?',
        'general_info.is_close_to_prominent_functions' => 'Does the client have a relationship (by blood or marriage up to the second degree), or have an association with a person entrusted with a prominent public function in the Kingdom or a foreign country, senior management positions, or a position in an international organization?',
        'general_info.is_close_to_on_board' => 'Does the client have a close association with a board of directors member, an audit committee member or a senior executive in a listed company?',
        'general_info.is_beneficial_owner' => 'Is the client the beneficial owner of the account or business relationship?',
        'general_info.identity_of_account_owner' => 'identity of the beneficial owner of the account or business relationship',
        'financial_info.expected_cash_out_period' => 'period during which the client expects to cash out his invested money',
        'financial_info.securities_trans_outside_ksa_over_past_5_years' => 'Securities transactions outside the Kingdom over the past five years',
        'financial_info.in_which_countries_trans_executed' => 'If securities transactions were executed outside the Kingdom over the past five years, in which countries were these transactions executed?',
        'financial_info.investment_xp' => 'Investment knowledge and experience',
        'financial_info.loan_to_inv_money_ratio' => 'Loan to invested money ratio',
        'financial_info.margin_trans_over_past_5_years' => 'Margin transactions over the past five years',
        'financial_info.preferred_inv_assets.in_foreign_currencies' => 'What are the client’s preferred investment assets?',
        'financial_info.preferred_inv_assets.in_sar' => 'What are the client’s preferred investment assets?',
        'financial_info.preferred_inv_assets.foreign_currencies' => 'Forign currencies',
        'financial_info.products_prev_inv_in' => 'Products previously invested in',
        'financial_info.prof_certs' => 'Professional certificates',
        'financial_info.risk_appetite' => 'Appetite for risk',
        'financial_info.years_of_inv_in_securities' => 'Number of years of investment in securities',

        'more_general_info.is_individual' => 'Are you individual or firm?',
        'more_general_info.have_you_invested_in_real_estates' => 'Have you ever invested in real estate?',
        'more_general_info.investment_xp' => 'Investment knowledge and experience',

        'general_info.is_individual' => 'Are you individual or firm?',
        'general_info.have_you_invested_in_real_estates' => 'Have you ever invested in real estate?',
        'general_info.investment_xp' => 'Investment knowledge and experience',

        'location.city_id' => 'City',
        'location.street' => 'Street',
        'location.district' => 'District',
        'location.postal_code' => 'Postal code',


        'subject' => 'subject',
        'message' => 'message',
        'reason_id' => 'reason',
        'name' => 'name',
        'email' => 'email',
        'phone_country_iso2' => 'country code',
        'phone_number' => 'phone number',
        'shares' => 'units',
//        'iban' => 'IBAN',
        'name_en' => 'name (En)',
        'name_ar' => 'name (Ar)',
        'is_active' => 'active',
        'question' => 'question (En)',
        'question_ar' => 'question (Ar)',
        'answer' => 'answer (En)',
        'answer_ar' => 'answer (Ar)',
        'comment' => 'comment',
        'attachments.*.file' => 'file',
        'attachments.*.type' => 'file type',
        'listing.title_en' => 'title (En)',
        'listing.title_ar' => 'title (Ar)',
        'listing.city_id' => 'city',
        'listing.type_id' => 'type',
        'listing.target' => 'target',
        'listing.gross_yield' => 'gross yield',
        'listing.dividend_yield' => 'dividend_yield',
        'listing.deadline' => 'deadline',
        'listing.total_shares' => 'total units',
        'listing.reserved_shares' => 'reserved units',
        'listing.min_inv_share' => 'minimum investment unit',
        'listing.max_inv_share' => 'maximum investment unit',
        'listing.rent_amount' => 'rent amount',
        'listing.is_closed' => 'closed',
        'listing.is_visible' => 'visible',
        'listing.investment_type' => 'investment type',
        'due_diligence.content_en' => 'due diligence (English description)',
        'due_diligence.content_ar' => 'due diligence (Arabic description)',
        'property_details.description_en' => 'property details (English description)',
        'property_details.description_ar' => 'property details (Arabic description)',
        'property_details.location.description_en' => 'location (English description)',
        'property_details.location.description_ar' => 'location (Arabic description)',
        'property_details.location.longitude' => 'location (longitude)',
        'property_details.location.latitude' => 'location (latitude)',
        'financial_details.content_en' => 'financial details (English description)',
        'financial_details.content_ar' => 'financial details (Arabic description)',
        'team.*.name_en' => 'team (English name)',
        'team.*.name_ar' => 'team (Arabic name)',
        'team.*.members.*.name' => 'team member (name)',
        'team.*.members.*.job_title_en' => 'team member (English job title)',
        'team.*.members.*.job_title_ar' => 'team member (Arabic job title)',
        'timeline.*.title_en' => 'timeline (English title)',
        'timeline.*.title_ar' => 'timeline (Arabic title)',
        'timeline.*.description_en' => 'timeline (English description)',
        'timeline.*.description_ar' => 'timeline (Arabic description)',
        'timeline.*.date' => 'timeline (date)',
        'to' => 'to',
        'amount' => 'amount',
        'type' => 'type',
        'files.*' => 'file',
        'title_en' => 'title (En)',
        'title_ar' => 'title (Ar)',
        'description_en' => 'description (En)',
        'description_ar' => 'description (Ar)',
        'total' => 'total',
        'investors' => 'investors',
        'investors.*.id' => 'investor (id)',
        'investors.*.amount' => 'amount',
        'content.en' => 'content (En)',
        'content.ar' => 'content (Ar)',
        'bank_code' => 'bank code',
//        'name' => 'name',
        'first_name' => 'first name',
        'last_name' => 'last name',
        'commercial_number' => 'commercial number',
        'password' => 'password',
        'current_password' => 'current password',
        'nin' => 'national ID',
        'birth_date' => 'birth date',
        'education_level' => 'education level',
        'occupation' => 'occupation',
        'subscription_form' => 'Subscription form',

        'phone' => 'phone number',
    ],


    'values' => [
        'more_general_info' => [
            'is_beneficial_owner' => [
                '0' => 'no'
            ],
            'any_xp_related_to_fin_sector' => [
                '1' => 'yes'
            ],
            'preferred_inv_assets' => [
                'in_foreign_currencies' => [
                    '1' => 'yes'
                ]
            ]
        ],
    ],
];
