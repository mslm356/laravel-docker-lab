<?php

namespace App\Support\Transactions\Descriptions;

use App\Enums\Banking\TransactionReason;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\Generators\DefaultGenerator;

class DescriptionManager
{
    private static $generators = [];

    /**
     * Undocumented function
     *
     * @return \App\Support\Transactions\Descriptions\GeneratorInterface
     */
    protected static function getGenerator(int $reason)
    {
        if (isset(self::$generators[$reason])) {
            return self::$generators[$reason];
        }

        $className = "App\\Support\\Transactions\\Descriptions\\Generators\\" . TransactionReason::getKey($reason) . "Type";

        if (!class_exists($className)) {
            $className = DefaultGenerator::class;
        }

        $generator = new $className;

        self::$generators[$reason] = $generator;

        return $generator;
    }

    /**
     * Get transaction description
     *
     * @return string
     */
    public static function getDescription(Transaction $transaction, $locale = null)
    {
        return self::getGenerator($transaction->reason)->generate($transaction, $locale);
    }
}
