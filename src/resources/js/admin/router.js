import Vue from 'vue';
import VueRouter from 'vue-router';
import PropertyTypes from '@admin/components/propertyTypes/TypesList';
import PropertyTypeCU from '@admin/components/propertyTypes/TypeCU';

Vue.use(VueRouter);

export const router = new VueRouter({
    mode: 'history',
    base: '/' + document.documentElement.lang + '/admin',
    routes: [
        {
            path: '/',
            component: { template: '<div>bar</div>' }
        },
        {
            path: '/property-types',
            component: PropertyTypes,
            name: 'property-types.index',
        },
        {
            path: '/property-types/create',
            component: PropertyTypeCU,
            name: 'property-types.create',
        },
        {
            path: '/property-types/:id',
            component: PropertyTypeCU,
            name: 'property-types.update',
        },
    ]
});