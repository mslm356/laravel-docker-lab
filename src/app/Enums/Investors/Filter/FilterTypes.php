<?php

namespace App\Enums\Investors\Filter;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class FilterTypes extends Enum implements LocalizedEnum
{

    const Id = 1;
    const Name = 2;
    const Email = 3;
    const Phone = 4;
    const All = 5;
    const type = 6;
}
