<?php

namespace App\Http\Controllers\Investors\Auth;

use App\Jobs\Elm\Natheer\NatheerRegisterationJob;
use App\Services\Investors\LastLoginService;
use App\Services\Investors\RegisterServices;
use App\Support\Aml\AmlServices;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Elm\AbsherOtp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Models\Users\NationalIdentity;
use App\Support\Elm\AbsherOtp\VerificationSms;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;

class VerifyNin extends Controller
{

    public $amlServices;
    public $registerServices;

    public function __construct()
    {
        $this->amlServices = new AmlServices();
        $this->registerServices = new RegisterServices();

    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Support\VirtualBanking\BankService
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $data = $this->validate($request, $this->registerServices->getRules());

        $absherOtp = AbsherOtp::where('customer_id', $data['nin'])->find($data['vid']);

        if ($absherOtp === null) {
            throw ValidationException::withMessages(['vid' => __('messages.invalid_sms_verification_vid')]);
        }

        $user = $absherOtp->user;

        if (!$user && $user->investor && $user->investor->is_kyc_completed) {
            throw new AuthorizationException;
        }

        if (Arr::get($user->investor->data, 'identity_verification_tmp.vid') !== $absherOtp->id) {
            throw ValidationException::withMessages(['vid' => __('messages.invalid_sms_verification_vid')]);
        }

        try {
            $isVerified = VerificationSms::instance()->verify($data['nin'], $absherOtp, $data['code']);
        } catch (VerificationSmsException $th) {
            throw ValidationException::withMessages([
                'vid' => __('messages.invalid_sms_verification_vid'),
            ]);
        }

        if (!$isVerified) {
            throw ValidationException::withMessages([
                'code' => __('messages.invalid_sms_verification_code'),
            ]);
        }

        return DB::transaction(function () use ($data, $user, $absherOtp) {

            $this->registerServices->deleteOldIdentities($user);

            $identity = NationalIdentity::create($user->investor->data['identity_verification_tmp']['identity']);


            foreach ($user->investor->data['identity_verification_tmp']['addresses'] as $address) {
                $identity->addresses()->create($address);
            }

            $user->investor->update([
                'is_identity_verified' => true,
                'data->identity_verification_tmp' => null,
            ]);

            Auth::login($user);

            new LastLoginService($user);

            // AMl Services
//            if (config('aml.aml_registration_mode') == 'production') {
//                $dataValues = [
//                    'type' => 'screen',
//                    'user' => $user,
//                ];
//                $this->amlServices->handle($dataValues);
//            }


            dispatch(new NatheerRegisterationJob($user));


            return response()->json();
        }, 1);
    }

}
