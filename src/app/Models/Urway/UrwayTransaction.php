<?php

namespace App\Models\Urway;

use App\Models\AppModel;
use App\Models\Users\InvestorSubscriptionToken;

class UrwayTransaction extends AppModel
{
    protected $table = "urway_transactions";

    public function investorSubscriptionToken()
    {
        return $this->belongsTo(InvestorSubscriptionToken::class, 'subscription_id');
    }
}
