<?php

namespace App\Support\VirtualBanking\Helpers;

use App\Settings\AccountingSettings;
use BenSampo\Enum\Enum;
use Carbon\CarbonImmutable;
use Dflydev\DotAccessData\Data;

class LocalSaudiTransferFee extends Enum
{
    /**
     * Trafer fee in halals
     *
     * @var string
     */
    const FEE_0 = '0';

    /**
     * Trafer fee in halals
     *
     * @var string
     */
    const FEE_0_5 = '0'; //'50';

    /**
     * Trafer fee in halals
     *
     * @var string
     */
    const FEE_1 = '0';//'100';

    /**
     * Trafer fee in halals
     *
     * @var string
     */
    const FEE_5 = '500';

    /**
     * Trafer fee in halals
     *
     * @var string
     */
    const FEE_7 = '700';

    /**
     * Get transfer options
     *
     * @param string $fromBank
     * @param string $toBank
     * @param \Cknow\Money\Money $amount
     * @return \App\Support\VirtualBanking\Helpers\TransferFee[]
     */
    static public function getTransferFeeOptions($fromBank, $toBank, $amount)
    {
        if ($fromBank === $toBank) {
            return [
                TransferFee::make($amount, static::FEE_0, static::getDeliveryDate(static::FEE_0))
            ];
        }

        if ($amount->lessThanOrEqual(money('50000'))) {
            return [
                TransferFee::make($amount, static::FEE_0_5, static::getDeliveryDate(static::FEE_0_5))
            ];
        }

        if ($amount->greaterThan(money('50000')) && $amount->lessThanOrEqual(money('2000000'))) {
            return [
                TransferFee::make($amount, static::FEE_1, static::getDeliveryDate(static::FEE_1))
            ];
        }

        return [

            TransferFee::make($amount, static::FEE_5, static::getDeliveryDate(static::FEE_5)),
//            TransferFee::make($amount, static::FEE_7, static::getDeliveryDate(static::FEE_7)),
        ];
    }

    /**
     * Calculate the VAT of the transfer fee
     *
     * @param string $fee
     * @return \Cknow\Money\Money
     */
    static public function feeVat(string $fee)
    {
        $vatPercentage = app(AccountingSettings::class)->vat->percentage;

        return money($fee)->multiply($vatPercentage)->divide(100);
    }

    /**
     * Get total transfer fee with VAT
     *
     * @param string $fee
     * @return \Cknow\Money\Money
     */
    static public function feeWithVat(string $fee)
    {
        return money($fee)->add(static::feeVat($fee));
    }

    /**
     * Get delivery date for the given transfer fee
     *
     * @param string $transferFee
     * @return \Carbon\CarbonImmutable
     */
    static public function getDeliveryDate($transferFee)
    {

        switch ($transferFee) {
            case static::FEE_0:
            case static::FEE_0_5:
            case static::FEE_1:
                return CarbonImmutable::now('Asia/Riyadh');
                break;

            case static::FEE_5:
                $now = CarbonImmutable::now('Asia/Riyadh');
                $nowPassedWorkingHours = $now->greaterThan($now->setTime(15, 58));

                $deliveryDate = null;

                if ($now->isFriday()) {
                    $deliveryDate = $now->addDays(2);
                } else if ($now->isSaturday()) {
                    $deliveryDate = $now->addDays(1);
                } else if (($now->isThursday() && $nowPassedWorkingHours)) {
                    $deliveryDate = $now->addDays(3);
                } else if (($now->isThursday() && !$nowPassedWorkingHours)) {
                    $deliveryDate = $now; // ->addDays(4);
                } else if ($now->isWednesday() && $nowPassedWorkingHours) {
                    $deliveryDate = $now->addDays(1);   // 4 days
                } else if ($nowPassedWorkingHours) {
                    $deliveryDate = $now->addDays(1);  // 2days
                } else {
                    $deliveryDate = $now->addDay();
                }

                return $deliveryDate->setTime(16, 0);
                break;

            case static::FEE_7:
                $now = CarbonImmutable::now('Asia/Riyadh');
                $nowPassedWorkingHours = $now->greaterThan($now->setTime(15, 58));

                $deliveryDate = null;

                if ($now->isFriday()) {
                    $deliveryDate = $now->addDays(2);
                } else if ($now->isSaturday()) {
                    $deliveryDate = $now->addDays(1);
                } else if (($now->isThursday() && $nowPassedWorkingHours)) {
                    $deliveryDate = $now->addDays(3);
                } else if (($now->isThursday() && !$nowPassedWorkingHours)) {
                    $deliveryDate = $now;
                } else if ($now->isWednesday() && $nowPassedWorkingHours) {
                    $deliveryDate = $now->addDays(1);
                } else if ($nowPassedWorkingHours) {
                    $deliveryDate = $now->addDays(1);
                } else {
                    $deliveryDate = $now;
                }

                if ($deliveryDate->isSameDay(now('Asia/Riyadh'))) {
                    return $deliveryDate;
                }

                return $deliveryDate->setTime(16, 0);
                break;
        }
    }
}
