<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnbStatementTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anb_statement_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('event_id');
            $table->string('from_account')->nullable();
            $table->string('reference')->unique()->nullable();
            $table->decimal('amount', 64, 0)->nullable();
            $table->string('currency')->nullable();
            $table->date('value_date')->nullable();
            $table->timestamp('post_date')->nullable();
            $table->json('narrative')->nullable();
            $table->json('extra_data');
            $table->timestamp('created_at')->nullable();

            $table->foreign('event_id')->references('id')->on('anb_statement_events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anb_statement_transactions');
    }
}
