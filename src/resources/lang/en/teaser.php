<?php

return [
    'coming_soon' => 'Coming Soon',
    'pre_register' => [
        'description' => 'Pre-register now and we will notify you once we are fully launched, stay tuned!',
        'form' => [
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'confirm_password' => 'Confirm password',
            'phone_number' => 'Phone number',
            'country_code' => 'Country code',
            'nationality' => 'Nationality',
            'register' => 'Register',
            'is_living_in_ksa' => 'Are you living in Saudi Arabia?',
            'registered_successfully' => 'You have registered successfully and we will contact you soon!'

        ]
    ],
    'heading_p1' => 'Invest in',
    'heading_p2' => 'Real Assets',
    'description' => 'Diversify your passive income streams by investing in genuine assets without breaking the bank!'
];
