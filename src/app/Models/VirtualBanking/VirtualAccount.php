<?php

namespace App\Models\VirtualBanking;

use App\Enums\FileType;
use App\Models\AppModel;
use App\Models\File;

class VirtualAccount extends AppModel
{
    /**
     * Get wallet of the virtual account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id', 'id');
    }

    public function file()
    {
        return $this->morphOne(File::class, 'fileable')->where('type', FileType::TransactionReceipt);
    }
}
