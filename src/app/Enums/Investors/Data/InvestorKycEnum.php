<?php

namespace App\Enums\Investors\Data;

class InvestorKycEnum
{
    const COMPLETE = 1;
    const INCOMPLETE = 0;

    public static function all(): array
    {
        return [
            self::COMPLETE,
            self::INCOMPLETE,
        ];
    }

    public static function match($key): ?string
    {
        if ($key == self::COMPLETE) {
            return 'Complete';
        } elseif ($key == self::COMPLETE) {
            return 'Incomplete';
        } else {
            return  'UnKnown';
        }

    }
}
