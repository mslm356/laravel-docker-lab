<?php

use App\Enums\Role;
use App\Http\Controllers\Investors\ComplaintController;

Route::middleware(['auth:api', 'jwt_check:api', 'role:' . Role::Investor])->group(function () {
    Route::get('complaints/types', [ComplaintController::class, 'types']);
    Route::post('complaints', [ComplaintController::class, 'store']);
});
