<?php

namespace App\Http\Resources\Common\Investors\ProfessionalUpgrade;

use App\Http\Resources\Admins\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UpgradeRequestItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'files' => FileResource::collection($this->pivot->files)
        ];
    }
}
