<?php

namespace App\Http\Controllers\Investors\Listing;

use App\Enums\Listings\DetailCategory;
use App\Http\Controllers\Controller;
use App\Models\Listings\Listing;

class GetListingTermsAndConditions extends Controller
{
    /**
     * Invest in the given listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Listing $listing)
    {
        $termsAndConditions = $listing->details()->where('category', DetailCategory::TermsAndConditions)->first();

        return response()->json([
            'data' => [
                'description' => $termsAndConditions ? $termsAndConditions->value->get(locale_suff('description')) : ''
            ]
        ]);
    }
}
