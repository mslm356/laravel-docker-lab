<?php

namespace App\Models\Investors;

use App\Models\AppModel;
use Spatie\Translatable\HasTranslations;

class InvestorUpgradeItem extends AppModel
{
    use HasTranslations;

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatable = ['text'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];
}
