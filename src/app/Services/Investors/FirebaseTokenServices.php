<?php


namespace App\Services\Investors;

use App\Models\UserFirebaseToken;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FirebaseTokenServices
{

    public function createUserFirebaseToken($user, $token, $device_id, $lang = null)
    {
        $data = [
            'user_id' => $user->id,
            'token' => $token,
            'device_id' => $device_id,
        ];

        if ($lang) {
            $data['lang'] = $lang;
        }

        $result = DB::table('user_firebase_tokens')->where('user_id', $user->id)->update(['token' => $token,'updated_at'=>Carbon::now()]);

        if (!$result) {
            $result=UserFirebaseToken::create($data);
        }

        return $result;
    }

    public function createUserFirebaseToken_old($user, $token, $device_id, $lang = null)
    {
        DB::table('user_firebase_tokens')
            ->where('user_id', $user->id)
            ->where('device_id', $device_id)
            ->delete();

        $data = [
            'user_id' => $user->id,
            'token' => $token,
            'device_id' => $device_id,
        ];

        if ($lang) {
            $data['lang'] = $lang;
        }

        $userFirebaseToken = UserFirebaseToken::create($data);

        return $userFirebaseToken;
    }

    public function deleteUserFirebaseToken($user, $device_id, $type)
    {
        DB::table('user_firebase_tokens')
            ->where('user_id', $user->id)
            ->where('device_id', $device_id)
            ->where('type', $type)->delete();
    }
}
