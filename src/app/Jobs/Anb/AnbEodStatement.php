<?php

namespace App\Jobs\Anb;

use App\Actions\Transactions\CreateRefundTransactionsFromAnbTransfer;
use Illuminate\Support\Facades\Log;
use Jejik\MT940\Reader;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use App\Enums\Banking\BankCode;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\AnbNarrative;
use Illuminate\Support\Facades\DB;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Support\Banking\MT940\AnbEodParser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\VirtualBanking\VirtualAccount;
use App\Support\Banking\MT940\AnbEodTransaction;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Models\Anb\AnbEodStatement as EodStatementModel;
use App\Models\Anb\AnbEodTransaction as AnbAnbEodTransaction;
use App\Models\Anb\AnbTransfer;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Support\Banking\MT940\AnbEodBalance;
use Carbon\Carbon;
use Cknow\Money\Money;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class AnbEodStatement implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $accountNumber;
    protected $collectorWallet;
    protected $startDate;

    public function __construct($startDate = null)
    {
        $this->startDate = ($startDate ?? now('Asia/Riyadh')->subDays(1));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page = 1;

        do {
            $response = AnbApiUtil::endOfDayStatements(
                $this->startDate->toDateString(),
                $page,
                'ASC'
            );

            $this->readStatements($response['data'] ?? []);

            $page++;
        } while ($response['meta']['hasNextPage'] ?? false);
    }

    public function readStatements($statements)
    {
        if (empty($statements)) {
            return;
        }
        if (!$this->accountNumber) {
            $this->accountNumber = config('services.anb.account_number');
        }

        $filteredStatements = array_filter($statements, fn ($item) => $item['accountNumber'] === $this->accountNumber);

        foreach ($filteredStatements as $statement) {
            $this->readStatementTransactions($statement);
        }
    }

    public function readStatementTransactions($statementResponse)
    {
        $doesEodStatementExist = EodStatementModel::where('external_id', $statementResponse['id'])
            ->limit(1)
            ->exists();

        if ($doesEodStatementExist) {
            return;
        }

        $eodStatement = AnbApiUtil::endOfDayStatement($statementResponse['id']);

        DB::transaction(function () use ($eodStatement) {
            $reader = new Reader();
            $reader->setTransactionClass(AnbEodTransaction::class);
            $reader->setOpeningBalanceClass(AnbEodBalance::class);
            $reader->setClosingBalanceClass(AnbEodBalance::class);
            $parser = new AnbEodParser($reader);
            $statements = $parser->parse($eodStatement['data']);

            foreach ($statements as $statement) {
                /** @var \App\Support\Banking\MT940\AnbEodBalance $openingBalance */
                $openingBalance = $statement->getOpeningBalance();

                /** @var \App\Support\Banking\MT940\AnbEodBalance $closingBalance */
                $closingBalance = $statement->getClosingBalance();

                $eodStatement = EodStatementModel::create([
                    'external_id' => $eodStatement['id'],
                    'date' => Carbon::createFromFormat('Y-m-d', $eodStatement['date'], 'Asia/Riyadh')
                        ->setTime(0, 0)
                        ->timezone('UTC'),
                    'bank_code' => BankCode::Anb,
                    'account_number' => $statement->getAccount()->getNumber(),
                    'currency' => $statement->getAccount()->getCurrency(),
                    'bank_api_response' => $eodStatement,
                    'sequence_number' => $statement->getNumber(),
                    'opening_balance' => [
                        'amount' => Money::parseByDecimal($openingBalance->getAmountAsString(), $openingBalance->getCurrency())->getAmount(),
                        'date' => $openingBalance->getDate()->format('Y-m-d H:i:s'),
                        'currency' => $openingBalance->getCurrency(),
                    ],
                    'closing_balance' => [
                        'amount' => Money::parseByDecimal($closingBalance->getAmountAsString(), $closingBalance->getCurrency())->getAmount(),
                        'date' => $closingBalance->getDate()->format('Y-m-d H:i:s'),
                        'currency' => $closingBalance->getCurrency(),
                    ],
                ]);

                Log::info('anb-eod-stmt-transaction', [$eodStatement, $statement->getTransactions()]);

                /** @var AnbEodTransaction $eodTransaction */
                foreach ($statement->getTransactions() as $eodTransaction) {

                    $external_reference = $this->getExternalReferenceValue($eodTransaction->getDescription());

                    $doesEodTransactionExist = DB::table('anb_eod_transactions')->where('description', $external_reference)
                        ->limit(1)
                        ->exists();

                    if ($doesEodTransactionExist) {
                        continue;
                    }

                    $transaction = $eodStatement->transactions()->create([
                        'mark' => $mark = $eodTransaction->getMark(),
                        'amount' => Money::parseByDecimal($eodTransaction->getAmountAsString(), $openingBalance->getCurrency()),
                        'code' => $eodTransaction->getCode(),
                        'customer_ref' => $customerRef = $eodTransaction->getAccountHolder(),
                        'bank_ref' => $eodTransaction->getBankRef(),
                        'value_date' => $eodTransaction->getValueDate()->format('Y-m-d H:i:s'),
                        'book_date' => $eodTransaction->getBookDate()->format('Y-m-d H:i:s'),
                        'description' => $this->getExternalReferenceValue($eodTransaction->getDescription()),
                    ]);

                    if ($mark === 'C' && AnbVA::isVA($customerRef)) {
                        $this->checkOrCreateTransactionForVAccount($transaction);
                    } elseif (($mark === 'C' || $mark === 'CR') && ($moneyTransferId = AnbNarrative::check($customerRef)) !== false) {
                        Log::info('anb-eod-stmt-C-CR', [$transaction]);
                        //                        $this->checkOrCreateTransactionForCreditReversal($transaction, $moneyTransferId);
                    }
                }
            }
        });
    }

    public function checkOrCreateTransactionForVAccount(AnbAnbEodTransaction $transaction)
    {
        $iban = AnbVA::toIban($transaction->customer_ref);

        $vaccount = VirtualAccount::where('identifier', $iban)->first();

        $wallet = null;

        if ($vaccount) {
            $wallet = $vaccount->wallet;
        } else {
            if (!$this->collectorWallet) {
                $collector = User::role(Role::PayoutsCollector)->first();
                $this->collectorWallet = $collector->getWallet(WalletType::AnbStatementCollecter);
            }

            $wallet = $this->collectorWallet;
        }

        $external_reference = $this->getExternalReferenceValue($transaction->description);

        $localeTransaction = Transaction::where([
            'reason' => TransactionReason::AnbStatement,
            'external_reference' => $external_reference,
        ])
            ->first();

        if ($localeTransaction || $external_reference == 'CBS_NOT_FOUND') {
            Log::info('anb-eod-stmt', [$transaction, $external_reference, $localeTransaction]);
            return;
        }

        Log::info('anb-eod-stmt-C', [$transaction, $external_reference]);
    }

    public function checkOrCreateTransactionForCreditReversal($transaction, $moneyTransferId)
    {
        $transfer = AnbTransfer::lockForUpdate()->find($moneyTransferId);

        if (!$transfer) {
            return;
        }

        if ($transfer->data['is_refunded'] ?? null) {
            return;
        }

        $transfer = CreateRefundTransactionsFromAnbTransfer::run(
            $transfer,
            TransactionReason::RefundAfterAnbTransferFailure,
            $transaction->amount->getAmount()
        );

        $transfer->save();
    }

    public function getExternalReferenceValue($transactionDescription)
    {

        if (Str::startsWith($transactionDescription, ['SD', 'XSD', 'XXSD'])) {
            return $transactionDescription;
        }

        $descriptionData = explode('/', $transactionDescription);

        if (is_array($descriptionData)) {

            foreach ($descriptionData as $index=>$item) {

                if ($item == 'CBS') {

                    return isset($descriptionData[$index+1]) ? $descriptionData[$index+1] : 'CBS_NOT_FOUND';
                }
            }
        }

        return 'CBS_NOT_FOUND';
    }
}
