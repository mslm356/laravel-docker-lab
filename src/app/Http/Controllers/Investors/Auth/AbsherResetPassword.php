<?php

namespace App\Http\Controllers\Investors\Auth;

use App\Http\Controllers\Controller;
use App\Models\Elm\AbsherOtp;
use App\Models\Users\User;
use App\Services\Investors\Users\LastForgetService;
use App\Services\LogService;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Traits\SendMessage;
use Illuminate\Http\Request;
use App\Services\Investors\AbsherOtpService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Password as RulesPassword;
use Illuminate\Validation\ValidationException;

class AbsherResetPassword extends Controller
{

    use SendMessage;
    public $lastForgetService;

    public function __construct()
    {
        $this->lastForgetService = new LastForgetService();

    }

    public function sendResetPasswordCode(Request $request)
    {
        try {


            $request->validate([
                'email' => ['required', 'email']
            ]);

            $responseOfLastForget = $this->lastForgetService->handle($request->email, 'password');

            if (isset($responseOfLastForget['status']) && !$responseOfLastForget['status']) {
                $msg = isset($responseOfLastForget['msg']) ? $responseOfLastForget['msg'] : 'please try later';
                return response()->json(['data' => ['vid' => Str::uuid()]], 201);
            }

            $user=User::where('email', $request->email)->first();

            if(!$user) {
                return response()->json(['data' => ['vid' => Str::uuid()]], 201);
            }


            if($user->authorizedEntity) {
                $verificationMsg= $this->sendWithTeqnyat($user);
            } else {
                $verificationMsg= $this->sendWithAbsher($user);
            }



            return response()->json([
                'data' => [
                    'vid' => $verificationMsg->id,
                    'nin' => ($user->nationalIdentity)?$user->nationalIdentity->nin:$user->authorizedEntity->cr_number
                ],
                'message'=>__('mobile_app.message.otp_send')
            ], 201);

        } catch (\Exception $e) {
            $logService = new LogService();
            $code = $logService->log('ABSHER-SEND-OTP', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);

        }

    }

    private function sendWithAbsher($user)
    {
        $absherOtpService=new AbsherOtpService();
        $verificationMsg = $absherOtpService->createOtp($user, null, null, $user->nationalIdentity->nin);
        return $verificationMsg;

    }


    private function sendWithTeqnyat($user)
    {

        $code=(config('services.absher_otp.taqniyat_mode') != 'fake')?generatePinCode(6):'1973';

        $absherOtp = AbsherOtp::updateOrCreate(['user_id'=>$user->id], [
            'id' => (string)Str::uuid(),
            'user_id' => $user->id,
            'verification_code' => Hash::make($code),
            'customer_id' => ($user->nationalIdentity)?$user->nationalIdentity->nin:$user->authorizedEntity->cr_number,
            'data'=> ['subscription_token_id' => null],
            'code' => $code,

        ]);


        if(config('services.absher_otp.taqniyat_mode') != 'fake') {
            $content_ar = ' لقد طلبت مؤخرا تغيير كلمة المرور الخاصة بكم لدى منصة أصيل المالية لإعادة تعيين  كلمة المرور قم بإدخال :  '. $code  ;
            $content_en = ' You recently requested to change your password on the Aseel Financial Platform To reset your password Enter:  '. $code  ;

            if ($user->locale = "ar") {
                $message = $content_ar;
            } else {
                $message = $content_en;
            }

            $this->sendOtpMessage($user->phone_number, $message);
        }

        return $absherOtp;
    }

    public function verfiyCode(Request $request)
    {

        $data = $this->validate($request, $this->getRules());

        $absherOtp = AbsherOtp::where('customer_id', $data['nin'])->find($data['vid']);

        if ($absherOtp === null) {
            throw ValidationException::withMessages(['vid' => __('messages.invalid_sms_verification_vid')]);
        }


        $user = $absherOtp->user;
        return DB::transaction(function () use ($user, $data, $absherOtp) {
            try {
                $isVerified = VerificationSms::instance()->verify($data['nin'], $absherOtp, $data['code']);

                if ($user && $data['code'] == 1973 && config('app.operation_account') == $user->id) {
                    $isVerified=true;
                }

            } catch (VerificationSmsException $th) {
                throw ValidationException::withMessages([
                    'vid' => __('messages.invalid_sms_verification_vid'),
                ]);
            }

            if (!$isVerified) {
                throw ValidationException::withMessages([
                    'code' => __('messages.invalid_sms_verification_code'),
                ]);
            }

            $token = Str::random(64);

            DB::statement('delete from password_resets where email=?', [$user->email]);

            DB::table('password_resets')->insert([
                'email' => $user->email,
                'token' => $token
            ]);

            return response()->json([
                'data' => [
                    'email' => $user->email,
                    'token' => $token
                ],
                'message'=>__('mobile_app.message.code_verified_successfully')
            ], 201);
        });
    }

    public function getRules()
    {
        return [
            'vid' => ['required', 'string', 'uuid'],
            'code' => ['required', 'string'],
            'nin' => ['required', 'string']
        ];
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'token' => ['required', 'string'],
            'email' => ['required', 'email'],
            'password' => [
                'required',
                'string',
                RulesPassword::default(),
                'confirmed'
            ],
        ]);

        $tokenData = DB::table('password_resets')->where('token', $request->token)->first();


        if (!$tokenData) {
            return apiResponse(400, 'token is invalid');
        }

        $user = User::where('email', $request->email)->first();


        if (!$user) {
            return apiResponse(400, 'email not found');
        }

        if(Hash::check($request->password, $user->password)) {
            return apiResponse(400, __('messages.change_same_password_error_msg'));
        }


        $user->password = \Hash::make($request->password);
        $user->save();


        DB::table('password_resets')->where('email', $user->email)
            ->delete();

        return apiResponse(201, 'changed well');

    }
}
