<?php

namespace App\Observers;

use App\Models\Investors\InvestorProfile;
use App\Models\Users\User;
use App\Support\Users\UserAccess\ManageUserAccess;

class InvestorProfileObserver
{

    private function updateMainData($investorProfile){
        $user=User::find($investorProfile->user_id);
        if (!$user)
            return;

        $manageUserAccess =  new ManageUserAccess();
        $manageUserAccess->handle($user);

    }

    public function created(InvestorProfile $investorProfile)
    {
        $this->updateMainData($investorProfile);
    }

    public function updated(InvestorProfile $investorProfile)
    {
        $this->updateMainData($investorProfile);
    }

    public function deleted(InvestorProfile $investorProfile)
    {
        $this->updateMainData($investorProfile);
    }


    /**
     * Handle the InvestorProfile "restored" event.
     *
     * @param  \App\Models\Investors\InvestorProfile  $investorProfile
     * @return void
     */
    public function restored(InvestorProfile $investorProfile)
    {
        $this->updateMainData($investorProfile);
    }

    /**
     * Handle the InvestorProfile "force deleted" event.
     *
     * @param  \App\Models\Investors\InvestorProfile  $investorProfile
     * @return void
     */
    public function forceDeleted(InvestorProfile $investorProfile)
    {
        //
    }
}
