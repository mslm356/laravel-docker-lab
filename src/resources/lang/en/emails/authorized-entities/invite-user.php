<?php

return [
    'subject' => 'Invitation to :appName',
    'heading' => 'You are invited',
    'body' => [
        'greeting' => 'Hello, :name!',
        'introduction' => 'You have been invited to join :authorizedEntityName in :appName',
    ],
    'actions' => [
        'join_now' => 'Join now',
    ]
];
