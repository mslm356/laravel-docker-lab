<?php

namespace App\Http\Resources\AuthorizedEntities\Listings;

use App\Http\Resources\Common\Listings\ListingRevisionResource as CommonListingRevisionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingRevisionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingRevisionResource($this->resource))->toArray($request),
            []
        );
    }
}
