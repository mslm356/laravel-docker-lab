<?php
namespace App\Http\Middleware;
use App\Jobs\Investor\InvestorActivityLogJob;
use App\Services\LogService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class InvestorActivityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if (!config('app.investor_activity_log_status'))
                return $next($request);

            // Capture request data
            $requestData = [
                'user_id' => (Auth::check())?Auth::id():null,
                'method' => $request->method(),
                'url' => $request->fullUrl(),
                'request_headers' => json_encode($request->headers->all()),
                'request_data' =>json_encode( $request->all()),
//                'session_data'=>json_encode($request->session()->all())
            ];
            // Perform the request and capture the response
            $response = $next($request);
            // Capture response data
            $responseData = [
                'response_status_code' => $response->status(),
                'response_headers' => json_encode($response->headers->all()),
                'response_content' => $response->getContent(),
            ];
            $data=array_merge($requestData, $responseData);
            $investor_log = (new InvestorActivityLogJob($data));
            dispatch($investor_log)
                ->onQueue('activity_log');
            return $response;
        }catch (\Exception $e){
            $log_service=new LogService();
            $log_service->log('ACTIVITY-LOG',$e);
            return $next($request);
        }
    }
}

