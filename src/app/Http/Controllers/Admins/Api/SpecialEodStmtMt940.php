<?php

namespace App\Http\Controllers\Admins\Api;

use App\Http\Controllers\Controller;
use App\Models\Anb\AnbEodStatement;
use App\Services\Anb\SpecialEodMt940Service;
use App\Services\LogService;
use App\Support\Anb\AnbApiUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SpecialEodStmtMt940 extends Controller
{

    public $logService;
    public $accountNumber;
    public $specialEodMt940Service;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->specialEodMt940Service = new SpecialEodMt940Service();
    }

    /**
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecialEodStmt(Request $request)
    {
        try {
            $data = $this->specialEodMt940Service->validator($request->all())->validate();

            if ($data['key'] !== "19992") {
                return apiResponse(400, __('sorry, key not valid'));
            }

            $requestedDate = Carbon::parse($request->date);

            /*if ($requestedDate->format('Y') != '2023' || $requestedDate->format('m') != '07') {
                return apiResponse(200, __('success'), [ [], 'C'=> 0, 'D'=> 0]);
            }*/

            $date = $requestedDate->addDays(1);

            $response = AnbApiUtil::endOfDayStatements($date->format('Y-m-d'), $request->page, $request->order, $request->take);

            if (!$this->accountNumber) {
                $this->accountNumber = config('services.anb.account_number');
            }

            $filteredStatements = array_filter($response['data'], fn($item) => $item['accountNumber'] === $this->accountNumber);

            if (!count($filteredStatements)) {
                $smt = $this->emptyBankStmt($date);
                return apiResponse(200, __('success'), ['smt' => $smt]);
            }

            $smt = "";

            foreach ($filteredStatements as $statementResponse) {
                $eodStatement = AnbApiUtil::endOfDayStatement($statementResponse['id']);
                $smt .= $eodStatement['data'];
            }

            $this->logService->log('REQUEST-EOD-STMT940-getSpecialEodStmt', null, ['data', 'values' => $smt]);

            return apiResponse(200, __('success'), ['smt' => $smt]);

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            $code = $this->logService->log('ValidationException-getSpecialEodStmt', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('getSpecialEodStmt-exc', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function transactionsToStmt(Request $request)
    {
        try {

            $data = $this->specialEodMt940Service->transactionsToStmtValidation($request->all())->validate();

            if ($data['key'] !== "19992") {
                return apiResponse(400, __('sorry, key not valid'));
            }

            $transactions = DB::table('transactions')
                ->whereDate('created_at', '>=', Carbon::parse($request->date)->subHours(3))
                ->whereDate('created_at', '<', Carbon::parse($request->date)->addHours(21))
                ->whereIn('reason', [1, 4, 5])
                ->select('amount', 'meta', 'reason', 'id')
                ->get()
                ->toArray();

            $date = Carbon::parse($request->date)->addDays(1);

            $dateFormat = $date->format('y') . $date->format('m') . $date->format('d');

            $stmt = $this->specialEodMt940Service->stmtDefaultData($dateFormat);

            foreach ($transactions as $transaction) {

                $meta = json_decode($transaction->meta, 1);

                $t61 = $this->specialEodMt940Service->getTransactionData61($transaction, $dateFormat, $date, $meta);
                $t86 = $this->specialEodMt940Service->getTransactionData86($transaction, $meta);

                $stmt .= $t61;
                $stmt .= $t86;
            }

            $bankBalance = $this->specialEodMt940Service->getBankBalance($request->date);

            $closingBalance = $bankBalance ? number_format($bankBalance->closing_balance / 100, 2, ',', '') : '0,00';
            $openingBalance = $bankBalance ? number_format($bankBalance->opening_balance / 100, 2, ',', '') : '0,00';

            $s62f = ':62F:C' . $dateFormat . 'SAR' . $closingBalance . '\\n';
            $s64 = ':64:C' . $dateFormat . 'SAR' . $openingBalance;

            $stmt .= $s62f . $s64;

            $this->logService->log('REQUEST-TRANSACTION-TO-STMT-transactionsToStmt', null, ['data', 'smt' => $stmt]);

            return apiResponse(200, __('success'), ['smt' => $stmt]);

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            $code = $this->logService->log('ValidationException-transactionsToStmt', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('transactionsToStmt-exc', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function emptyBankStmt($date)
    {

        $dateFormat = $date->format('Y-m-d');
        $startDate = Carbon::parse('2022-02-06');
        $openingBalance = '0,00';
        $closingBalance = '0,00';
        $eodStmt = null;

        if ($startDate->lessThanOrEqualTo($date)) {

            for ($i=0; $i<9; $i++) {

                $eodStmt = AnbEodStatement::query()
                    ->where('created_at','like', $dateFormat)
                    ->first();

                if (!$eodStmt) {
                    $date->subDays(1);
                    continue;
                }else
                    break;
            }
        }

        $openingBalance = $eodStmt ? json_decode($eodStmt->opening_balance,1)['amount'] : $openingBalance;
        $closingBalance = $eodStmt ? json_decode($eodStmt->closing_balance,1)['amount'] : $closingBalance;

        $s20 = ':20:RPM-' . $dateFormat . randomDigitsStr('6') . '\\n';
        $s25 = ':25:' . config('services.anb.account_number') . '\\n';
        $s28c = ':28c:176/001' . '\\n';
        $s60f = ':60f:C' . $dateFormat . 'SAR0,00' . '\\n';
        $s62f = ':62F:C' . $dateFormat . 'SAR' . $closingBalance . '\\n';
        $s64 = ':64:C' . $dateFormat . 'SAR' . $openingBalance;

        return $s20 . $s25 . $s28c . $s60f . $s62f . $s64;

    }
}
