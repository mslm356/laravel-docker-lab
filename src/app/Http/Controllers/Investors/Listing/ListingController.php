<?php

namespace App\Http\Controllers\Investors\Listing;

use App\Enums\Listings\AttachmentType;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Listings\ListingDetailsWithAuthResource;
use App\Http\Resources\Investors\Listings\ListingDetailsWithoutAuthResource;
use App\Http\Resources\Investors\Listings\ListingListItemResource;
use App\Http\Resources\Investors\Listings\ListingResource;
use App\Models\File;
use App\Models\Listings\Listing;
use App\Services\Investors\ListingsServices;
use App\Services\LogService;

class ListingController extends Controller
{

    public $listingsServices;
    public $logServices;

    public function __construct()
    {
        $this->listingsServices = new ListingsServices();
        $this->logServices = new LogService();
    }

    /**
     * Show Listing Deatils
     *
     * @param  $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function show($listing)
    {
        $user = getAuthUser('web');
        $listing = $this->listingsServices->show($listing, $user);

        return new ListingResource($listing);
    }

    /**
     * Listings list
     *
     * @param
     */
    public function index()
    {
        try {
            $listings = $this->listingsServices->Listings();
            return ListingListItemResource::collection($listings);

        } catch (\Exception $e) {
            $this->logServices->log('FUND-DETAILS-WITHOUT-AUTH-EX', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }
    }

    /**
     * Download file
     *
     * @param \App\Models\Listings\Listing $listing
     * @param \App\Models\File $file
     * @return \Illuminate\Http\File
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function downloadDueDiligence(Listing $listing, File $file)
    {
        if (
            $listing->is_visible &&
            $file->fileable_type === Listing::class &&
            $file->fileable_id === $listing->id &&
            $file->extra_info->get('type') === AttachmentType::DueDiligence
        ) {
            return $file->download();
        }

        abort(404);
    }

    ////////////////// used for cache  /////////////////////

    public function fundDetailsWithAuth($listing)
    {
        try {

            $listing = $this->listingsServices->show($listing);

            return apiResponse(200, __('mobile_app/message.success'), new ListingDetailsWithAuthResource($listing, null, 'web'));

        } catch (\Exception $e) {
            $this->logServices->log('FUND-DETAILS-WITH-AUTH-EX', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }
    }

    public function fundDetailsWithoutAuth($listing)
    {
        try {

            $listing = $this->listingsServices->show($listing, $user=null);
            return apiResponse(
                200,
                __('mobile_app/message.created_successfully'),
                new ListingDetailsWithoutAuthResource($listing, 'mobile')
            );

        } catch (\Exception $e) {
            $this->logServices->log('FUND-DETAILS-WITHOUT-AUTH-EX', $e);

            return apiResponse(400, $e->getMessage());
        }
    }

}
