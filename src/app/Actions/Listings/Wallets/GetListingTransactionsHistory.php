<?php

namespace App\Actions\Listings\Wallets;

use App\Models\Listings\Listing;
use Lorisleiva\Actions\Concerns\AsAction;

class GetListingTransactionsHistory
{
    use AsAction, MapsWalletType;

    /**
     * Handle creating listing wallet
     *
     * @param \App\Models\Listings\Listing $listing
     * @param string $type 'SPV' or 'ESCROW'
     *
     * @return \Bavix\Wallet\Models\Wallet
     */
    public function handle(Listing $listing, $type)
    {
        $mappedType = $this->getMappedWalletType($type);

        $wallet = $listing->getWallet($mappedType);

        if ($wallet) {
            return $wallet->transactions()
                ->where('confirmed', true)
                ->simplePaginate();
        }

        return null;
    }
}
