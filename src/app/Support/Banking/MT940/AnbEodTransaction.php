<?php

namespace App\Support\Banking\MT940;

use Jejik\MT940\Transaction;

class AnbEodTransaction extends Transaction
{
    /**
     * Debit/Credit/Reverse Credit/Reverse Debit
     *
     * @var string
     */
    protected $mark = null;

    /**
     * Debit/Credit/Reverse Credit/Reverse Debit
     *
     * @var string
     */
    protected $amountAsString = '0';

    /**
     * Parsed 86 tag description
     *
     * @var \Illuminate\Support\Collection|null
     */
    protected $parsedDescription = null;

    public function getParsedDescription()
    {
        if (is_null($this->parsedDescription)) {
            $this->parsedDescription = $this->parseDescription();
        }

        return $this->parsedDescription;
    }

    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    public function getMark()
    {
        return $this->mark;
    }

    public function setParsedDescription($description)
    {
        $this->parsedDescription = $description;
        return $this;
    }

    /**
     * Process the description
     *
     * @return \Illuminate\Support\Collection
     */
    protected function parseDescription()
    {
        $description = $this->getDescription();

        if (is_null($description)) {
            $this->parsedDescription = [];
            return $this->parsedDescription;
        }

        $lines = preg_split("/\r\n|\n|\r/", $description);

        $info = [
            'description' => $description,
            'ref' => $this->transCoreBankingRef($lines),
            'bankAcc' => $this->transBankAccount($lines),
            'NIN' => $this->transNIN($lines),
            'channel' => $this->transChannel($lines),
            'network' => $this->transNetwork($lines),
            'senderTitle' => $this->transSenderTitle($lines),
            'bic' => $this->transBIC($lines),
            'senderName' => $this->transSenderName($lines),
            'narrative' => $this->transNarrative($lines),
        ];

        return collect($info);
    }

    /**
     * Get transaction core banking reference
     *
     * @param array $lines
     * @return string
     */
    protected function transCoreBankingRef($lines)
    {
        return explode("!", $lines[0])[0];
    }

    /**
     * Get transaction Narrative
     *
     * @param array $lines
     * @return string
     */
    protected function transNarrative($lines)
    {
        return explode("!", $lines[0])[1] ?? null;
    }

    /**
     * Get transaction bank account who initiated the transfer
     *
     * @param array $lines
     * @return string
     */
    protected function transBankAccount($lines)
    {
        return explode("!", $lines[1])[0];
    }

    /**
     * Get transaction National identifier number
     *
     * @param array $lines
     * @return string
     */
    protected function transNIN($lines)
    {
        return explode("!", $lines[1])[1];
    }

    /**
     * Get transaction channel
     *
     * @param array $description
     * @return string
     */
    protected function transChannel($lines)
    {
        return explode("!", $lines[1])[2];
    }

    /**
     * Get transaction channel
     *
     * @param array $description
     * @return string
     */
    protected function transNetwork($lines)
    {
        return explode("!", $lines[2])[0];
    }

    /**
     * Get transaction network
     *
     * @param array $description
     * @return string
     */
    protected function transSenderTitle($lines)
    {
        return explode("!", $lines[2])[1];
    }

    /**
     * Get transaction BIC
     *
     * @param array $description
     * @return string
     */
    protected function transBIC($lines)
    {
        return explode("!", $lines[2])[2];
    }

    /**
     * Get transaction sender name
     *
     * @param array $description
     * @return string
     */
    protected function transSenderName($lines)
    {
        return explode("!", $lines[3])[0];
    }

    /**
     * Setter for amount
     */
    public function setAmountAsString($amount)
    {
        $this->amountAsString = $amount;
        return $this;
    }

    /**
     * Setter for amount
     */
    public function getAmountAsString()
    {
        return $this->amountAsString;
    }
}
