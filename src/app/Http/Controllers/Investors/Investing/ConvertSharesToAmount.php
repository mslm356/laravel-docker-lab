<?php

namespace App\Http\Controllers\Investors\Investing;

use App\Http\Controllers\Controller;
use App\Models\Listings\Listing;
use App\Services\Investors\InvestmentServices;
use App\Services\MemberShip\SpecialCanInvest;
use Illuminate\Http\Request;

class ConvertSharesToAmount extends Controller
{

    public $investmentServices;
    public $specialCanInvest;

    public function __construct()
    {
        $this->investmentServices = new InvestmentServices();
        $this->specialCanInvest = new SpecialCanInvest();
    }

    /**
     * Invest in the given listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, Listing $listing)
    {
        $user = $request->user('sanctum');

        $this->validate($request, ['shares' => ['required', 'integer']]);

        $responseData = $this->investmentServices->shareAmount($request, $listing, $user);

        return response()->json(['data' => $responseData]);
    }
}
