<?php

namespace App\Http\Resources\Investors\Listings;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $appLocal = App::getLocale();

        $teams = [];

        $this->values()->each(function ($team, $key) use ($appLocal, &$teams) {
            $teams[$key]['name'] = $team['name' . '_' . $appLocal];
            $teams[$key]['members'] = [];

            foreach ($team['members'] ?? [] as $index => $member) {

                $teams[$key]['members'][$index]['name'] = isset($member['name_'. $appLocal]) ? $member['name_'. $appLocal] : '';
                $teams[$key]['members'][$index]['job_title'] = isset($member['job_title_' . $appLocal]) ? $member['job_title_' . $appLocal] : '' ;
                $teams[$key]['members'][$index]['bio'] = isset($member['bio_' . $appLocal]) ? $member['bio_' . $appLocal] : '' ;
                $teams[$key]['members'][$index]['link1'] = isset($member['link1' ]) ? $member['link1' ] : '' ;
                $teams[$key]['members'][$index]['link2'] = isset($member['link2' ]) ? $member['link2' ] : '' ;
            }
        });

        return $teams;
    }
}
