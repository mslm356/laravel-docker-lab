<?php

namespace App\Support\Anb\Http;

use App\Services\LogService;
use App\Support\Anb\AnbMsgUtil;
use App\Support\Anb\Auth;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class AnbHttp
{
    private static $baseUrl = null;
    protected $http;
    protected $sign = true;
    protected $withTokens = true;
    protected $retries = 0;
    public $logServices;


    private function __construct($http)
    {
        $this->http = $http;
        $this->logServices = new LogService();
    }

    public function signed($sign = true)
    {
        $this->sign = $sign;
        return $this;
    }

    public function __call($method, $arguments)
    {
        if (in_array($method, ['get', 'post', 'put'])) {
            $arguments[0] = ltrim($arguments[0], '/');
        }

        if ($this->sign && in_array($method, ['get', 'post', 'put'])) {
            if ($method !== 'get' && isset($arguments[1]) && is_array($arguments[1])) {
                $rawBody = json_encode($arguments[1]);
            }

            $uri = $arguments[0];

            if ($method === 'get' && isset($arguments[1]) && is_array($arguments[1])) {
                $uri .= Arr::query($arguments[1]);
            }

            $this->http->withHeaders([
                'sig_signed' => AnbMsgUtil::signMsg(strtoupper($method), $uri, $rawBody ?? ''),
                'sig_v' => 'v1'
            ]);
        }

        sendRequest:
        $response = $this->http->{$method}(...$arguments);

        if (!$response instanceof Response) {
            return $this;
        }

        if ($response->ok()) {
            $this->logServices->log('ANB-CONNECTION-SUCCESS', null, ['test anb', $response->body()]);
            return $response;
        }

        $this->logServices->log('ANB-CONNECTION-FAILED', null, ['test anb', $response->body()]);

        if ($response->status() === 401 && $this->retries < 4) {
            $authInstance = Auth::instance(true);

            $this->http->withToken(
                $authInstance->getToken(),
                $authInstance->getTokenType()
            );

            $this->retries = $this->retries + 1;

            goto sendRequest;
        }

        throw new HttpException($method, $response);
    }

    public static function __callStatic($method, $arguments)
    {
        $additionalData = isset($arguments[2]) && is_array($arguments[2]) ? $arguments[2] : null ;

        if ($additionalData != null && isset($additionalData['version'])) {
            self::$baseUrl = config('services.anb.base_api_url') . '/' . $additionalData['version'];
        } else {
            self::$baseUrl = config('services.anb.api_url');
        }

        $http = Http::baseUrl(self::$baseUrl);

        return (new AnbHttp($http))->{$method}(...$arguments);
    }
}
