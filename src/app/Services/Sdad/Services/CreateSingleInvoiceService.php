<?php

namespace App\Services\Sdad\Services;

use App\Enums\Notifications\NotificationTypes;
use App\Jobs\Sdad\UpdateExpiredInvoice;
use App\Mail\Investors\SdadInvoiceExport;
use App\Models\Users\User;
use App\Services\Exeptions\NoResponseDataException;
use App\Services\LogService;
use App\Services\Sdad\Repository\SingleInvoiceInquiryRepository;
use App\Services\Sdad\Repository\SingleInvoicesRepository;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class CreateSingleInvoiceService
{
    use NotificationServices, SendMessage;

    public $invoice_create_repo;
    public $invoice_inquiry_repo;
    public $logService;

    public function __construct()
    {
        $this->invoice_create_repo = new SingleInvoicesRepository();
        $this->invoice_inquiry_repo = new SingleInvoiceInquiryRepository();
        $this->logService = new LogService();
    }

    public function create_invoice(User $user, array $data): ?bool
    {
        try {

            app()->setLocale($user->locale ?? App::getLocale());
            $invoice = $this->invoice_create_repo->create($user, $data);
            $inquiry_result = $this->invoice_inquiry_repo->initiate_provider($invoice->edaat_invoice_id)->inquiry($invoice->edaat_invoice_id);
            $inquiry_data = [
                'status' => $inquiry_result->Body->StatusCode,
                'invoice_brief_result' => $inquiry_result
            ];

            $invoice = $this->invoice_create_repo->update($invoice->id, $inquiry_data);

            $expireInvoice = (new UpdateExpiredInvoice($invoice->id));
            dispatch($expireInvoice)
                ->delay(Carbon::now()->addMinutes(config('edaat.job_expire_time')))
                ->onQueue('sdad');

            if ($invoice) {

                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user->email)->send(new SdadInvoiceExport($user, $invoice));
                }
                // send sms
                $this->sendMessageAfterPayment($user, $invoice);

                $this->send(NotificationTypes::ReminderProfile, $this->notificationData($user, $invoice), $user->id);
                return true;
            } else {
                return false;
            }
        } catch (NoResponseDataException $e) {
            throw new NoResponseDataException();
        } catch (\Exception $e) {
            $this->logService->log('Sdad-EXPORT-INVOICE-Service-ERROR', $e, ['issue', json_encode($data)]);
            throw new \Exception('EXPORT INVOICE Service ERROR');

        }

    }

    public function sendMessageAfterPayment($user, $invoice)
    {
        $text_1 = __('emails/investors/sdad_invoice_export.welcome_message', ['name'=> $user->full_name]);
        $text_2 = __('emails/investors/sdad_invoice_export.message_1');
        $text_3 = __('emails/investors/sdad_invoice_export.sdad_invoice');
        $text_4 = __('emails/investors/sdad_invoice_export.service_type');
        $text_5 = __('emails/investors/sdad_invoice_export.message_2', ['invoice_id'=> $invoice->edaat_invoice_id]);
        $text_6 = __('emails/investors/sdad_invoice_export.message_3', ['amount'=> $invoice->amount]);
        $text_7 = __('emails/investors/sdad_invoice_export.message_4', ['time'=>  $invoice->created_at->addHours(3)]);
        $text_expire = __('emails/investors/sdad_invoice_export.expire');
        $text_8 = __('emails/investors/sdad_invoice_export.message_5') ;
        $text_9 = __('emails/investors/sdad_invoice_export.link_text') ;

        $message = $text_1 ."\n". $text_2 ."\n". $text_3 ."\n". $text_4 ."\n". $text_5 ."\n". $text_6
            ."\n". $text_7 ."\n" . $text_expire. "\n" . $text_8 ."\n". $text_9 ;

        $this->sendOtpMessage($user->phone_number, $message);
    }

    public function notificationData($user, $invoice)
    {

        $expireTextAr = ' وقت انتهاء صلاحية رقم السداد ٣٠ دقيقة ';
        $expireTextEn = ' payment number expiration time is 30 minutes. ';

        return  [
            'action_id' => $invoice->id,
            'channel' => 'mobile',
            'title_en' => 'SADAD Invoice Exported Successfully',
            'title_ar' => ' تم تصدير فاتورة سداد بنجاح ',
            'content_en' => 'A SADAD Invoice has been Exported Successfully with amount :' . $invoice->amount . 'SAR' . 'with invoice number :' . $invoice->edaat_invoice_id . $expireTextEn,
            'content_ar' => ' تم تصدير فاتورة سداد لإضافة رصيد لمحفظتك بمبلغ : ' . $invoice->amount . '  ريال سعودي  ' . '  رقم الفاتورة : ' . $invoice->edaat_invoice_id . $expireTextAr,
        ];
    }
}
