<?php

namespace App\Http\Controllers\Admins\Api\Aml;

use App\Models\Users\User;
use App\Models\AML\AmlHistory;
use App\Http\Controllers\Controller;
use App\Http\Resources\Aml\AmlHistoryResource;
use App\Jobs\CheckUserAmlManually;
use Illuminate\Http\Request;

class AmlController extends Controller
{
    /**
     * Check the user in the AML lists.
     *
     * @param \App\Models\Users\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUser(Request $request, User $user)
    {
        if ($user->investor->is_kyc_completed === false) {
            return response()->json([
                'message' => trans('messages.aml_failed_because_user_didnt_completed_kyc'),
            ], 400);
        }

        CheckUserAmlManually::dispatch($user, $request->user('sanctum'));

        return response()->json([
            'message' => trans('messages.aml_checking')
        ]);
    }

    /**
     * List AMl checks for investor
     *
     * @param \App\Models\Users\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserList(User $user)
    {
        $aml = AmlHistory::with('checks')
            ->where('investor_id', $user->id)
            ->latest()
            ->paginate();

        return AmlHistoryResource::collection($aml);
    }
}
