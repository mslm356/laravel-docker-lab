<?php

namespace App\Enums\Listings;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class ReportEnum extends Enum implements LocalizedEnum
{
    const PendingReview = 1;
    const InReview = 2;
    const Approved = 3;
    const Rejected = 4;
}
