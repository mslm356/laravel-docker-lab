<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Enums\RegistrationOption as Options;
use Illuminate\Database\Migrations\Migration;

class RegistrationOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_options', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('category');
            $table->timestamps();
        });

        DB::table('registration_options')->insert([
            [
                'name_en' => "Postgraduate",
                'name_ar' => "دراسات عليا",
                'key' => "Postgraduate",
                'category' => Options::Education
            ],
            [
                'name_en' => "Undergraduate",
                'name_ar' => "خريج",
                'key' => "Undergraduate",
                'category' => Options::Education
            ],
            [
                'name_en' => "Graduate",
                'name_ar' => "خريج",
                'key' => "Graduate",
                'category' => Options::Education
            ],
            [
                'name_en' => "Retired",
                'name_ar' => "متقاعد",
                'key' => "Retired",
                'category' => Options::Occupation
            ],
            [
                'name_en' => "Business-owner",
                'name_ar' => "صاحب عمل",
                'key' => "Business-owner",
                'category' => Options::Occupation
            ],
            [
                'name_en' => "Employed",
                'name_ar' => "موظف",
                'key' => "Employed",
                'category' => Options::Occupation
            ],
            [
                'name_en' => "Unemployed",
                'name_ar' => "غير موظف",
                'key' => "Unemployed",
                'category' => Options::Occupation
            ],
            [
                'name_en' => "Student",
                'name_ar' => "طالب",
                'key' => "Student",
                'category' => Options::Occupation
            ],
            [
                'name_en' => "Real-estate",
                'name_ar' => "تسويق عقارى",
                'key' => "Real-estate",
                'category' => Options::CurrentInvest
            ],
            [
                'name_en' => "Capital-markets",
                'name_ar' => "دعاية",
                'key' => "Capital-markets",
                'category' => Options::CurrentInvest
            ],
            [
                'name_en' => "Private-equity",
                'name_ar' => "عمل خاص",
                'key' => "Private-equity",
                'category' => Options::CurrentInvest
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_options');
    }
}
