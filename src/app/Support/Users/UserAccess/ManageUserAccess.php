<?php

namespace App\Support\Users\UserAccess;

use App\Models\Investors\InvestorProfile;
use App\Models\Users\NationalIdentity;
use App\Models\Users\UserCanAccess;
use App\Services\LogService;

class ManageUserAccess
{
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function handle($user, $module = null)
    {
        try {
            $response = ['status' => true];

            $modules = ['upgradeRequest', 'investment', 'transfer', 'bankAccount', 'memberShipRequestModule', 'investorMainData','virtualAccountModule','showAppRate'];

            $userDefaultData = $this->defaultData($user, $module);

            $userAccessData = [];

            if ($module) {

                $userAccessData[$module] = $this->callModuleClass($user, $module, $userDefaultData);

            } else {

                foreach ($modules as $module) {
                    $userAccessData[$module] = $this->callModuleClass($user, $module, $userDefaultData);
                }
            }

            $this->updateUserAccessData($user, $userAccessData);

            return $response;

        } catch (\Exception $e) {
            $this->logService->log('USER-ACCESS-EX', $e);
            return ['status' => false];
        }
    }

    public function callModuleClass($user, $module, $userDefaultData)
    {
        $className = "app\\Support\\Users\\UserAccess\\Modules\\" . ucfirst($module);

        if (!class_exists($className)) {
            return false;
        }

        $generator = new $className();

        return $generator->handle($user, $module, $userDefaultData);
    }

    public function defaultData($user, $module)
    {

        $modulesUseDefaultData = ['upgradeRequest', 'investment', 'transfer', 'bankAccount', 'investorMainData'];

        if ($module && !in_array($module, $modulesUseDefaultData)) {
            return null;
        }


        $data['profile'] = InvestorProfile::where('user_id', $user->id)
            ->select([
                'level',
                'status',
                'is_identity_verified',
                'is_kyc_completed',
                'unsuitability_confirmation',
                'last_kyc_update_date',
                'data',
                'annual_income'
            ])
            ->first();

        $data['nationalIdentity'] =NationalIdentity::where('user_id', $user->id)
            ->select([
                'nin',
                'id_expiry_date',
                'birth_date',
                'nationality',
                'birth_date_type',
                'nationality'
            ])
            ->first();

        return $data;
    }

    public function updateUserAccessData($user, $userAccessData)
    {
        if (empty($userAccessData)) {
            return false;
        }

        $userAccessData = $this->prepareData($userAccessData);

        UserCanAccess::updateOrCreate(['user_id' => $user->id], $userAccessData);
    }

    public function prepareData($userAccessData)
    {

        $arr = [];

        foreach ($userAccessData as $key => $user_data) {

            if (!is_array($user_data)) {
                $arr[$key] = $user_data;
            } else {
                foreach ($user_data as $key => $new_arr) {
                    $arr[$key] = $new_arr;
                }
            }
        }

        return $arr;
    }

}
