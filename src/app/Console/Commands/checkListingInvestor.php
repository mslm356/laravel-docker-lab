<?php

namespace App\Console\Commands;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Jobs\AbsherTest;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Settings\ZatcaSettings;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class checkListingInvestor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:listing-investor {--listing_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check investments';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $listingInvestors = DB::table('listing_investor')
                ->where('listing_id', $this->option('listing_id'))
                ->get();

            $count = 0;
            $totalDiff = 0;

            foreach ($listingInvestors as $index => $listingInvestor) {

                $transactionAmount = DB::table('transactions')
                    ->where('payable_id', $listingInvestor->investor_id)
                    ->where('reason', 2)
                    ->whereJsonContains('meta->listing_id', intval($this->option('listing_id')))
                    ->sum('amount');

                $totalInvest = DB::table('listing_investor_records')
                    ->where('listing_id', $this->option('listing_id'))
                    ->where('investor_id', $listingInvestor->investor_id)
                    ->sum('amount_without_fees');

                if ($totalInvest != $listingInvestor->invested_amount) {

                    $diff = $totalInvest - $listingInvestor->invested_amount;

                    dump([
                        $count++,
                        'listing_investor_id' => $listingInvestor->id,
                        'investor_id' => $listingInvestor->investor_id,
                        'record_amount' => $totalInvest,
                        'listing_investor_amount' => $listingInvestor->invested_amount,
                        'diff' => $diff,
                        'transaction_amount' => $transactionAmount,
                    ]);

                    $totalDiff += $diff;
                }
            }

            dd('done', $totalDiff);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine());
        }

    }


}
