<?php

namespace App\Support\Users\UserAccess\Modules;

use App\Models\Users\User;

class InvestorMainData
{
    public function handle($user, $module, $userDefaultData)
    {
        $response = 0;

        if ($userDefaultData) {
            $response = $this->defaultValidation($user, $userDefaultData);
        }

        return $response;
    }

    public function defaultValidation($user, $userDefaultData)
    {
        $profile = $userDefaultData['profile'];
        $nationalIdentity = $userDefaultData['nationalIdentity'];

        $nationalIdentityData = $this->getNationalIdentyData($nationalIdentity);

        $status = in_array($profile->status, [1, 3]) ? 200 : 0;

        if ($status != 200) {
            User::where('id', $user->id)->update(['jwt_token' => null]);
        }

        return [
            'is_kyc_completed' => $profile->is_kyc_completed,
            'unsuitability_confirmation' => $profile->unsuitability_confirmation,
            'kyc_suitability' => getKycSuitability($profile, $user),
            'upgrade_request_level' => $profile->level,
            'login' => $status,
            'national_identity_data' => ($nationalIdentityData) ? json_encode($nationalIdentityData) : null
        ];

    }

    public function getNationalIdentyData($nationalIdentity)
    {
        $nationalIdentityData = [];
        if ($nationalIdentity) {
            $nationalIdentityData = [
                'nin' => $nationalIdentity->nin,
                'birth_date' => $nationalIdentity->birth_date,
                'birth_date_type' => $nationalIdentity->birth_date_type,
                'id_expiry_date' => $nationalIdentity->id_expiry_date];
        }

        return (count($nationalIdentityData) > 0) ? $nationalIdentityData : null;
    }

}
