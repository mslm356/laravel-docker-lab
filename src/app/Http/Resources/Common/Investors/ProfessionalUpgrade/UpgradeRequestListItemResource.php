<?php

namespace App\Http\Resources\Common\Investors\ProfessionalUpgrade;

use App\Enums\Investors\UpgradeRequestStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class UpgradeRequestListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = UpgradeRequestStatus::getDescription($this->status);
        return [
            'id' => $this->id,
            'status' => [
                'value' => $this->status,
                'description' => __("messages.$status") ,
            ],
            'created_at' => $this->created_at->timezone('Asia/Riyadh')->toDateTimeString(),
        ];
    }
}
