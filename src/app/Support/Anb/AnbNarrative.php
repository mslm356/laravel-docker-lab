<?php

namespace App\Support\Anb;

class AnbNarrative
{
    private static $prefix = 'AS:';

    public static function make($ref)
    {
        return self::$prefix . $ref;
    }

    public static function check($narrative)
    {
        $result = preg_match("/" . self::$prefix . "(.*)/", $narrative, $matches);

        if ($result !== 1) {
            return false;
        }

        return $matches[1];
    }
}
