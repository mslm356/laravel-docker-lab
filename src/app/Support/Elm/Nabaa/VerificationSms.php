<?php

namespace App\Support\Elm\Nabaa;

use App\Support\Elm\Nabaa\SmsStrategies\Fake;
use App\Support\Elm\Nabaa\SmsStrategies\Nabaa;

class VerificationSms
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        $instance = null;
        if (config('services.nabaa.mode') === 'fake') {
            $instance = new Fake;
        } else {
            $instance = new Nabaa;
        }

        self::$instance = $instance;

        return self::$instance;
    }
}
