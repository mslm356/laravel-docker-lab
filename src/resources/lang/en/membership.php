<?php

return [
    'sms_create' => "Congrats! You are eligible for (:membership) membership
        Due to the volume of your investments in Aseel platform, we are pleased to inform you that you are eligible to upgrade your membership
        Please visit the Dashboard on the website or More in the application",

    'sms_accept' => 'Congrats! Your account has been upgraded to membership (:membership)
     Your membership has been successfully upgraded',

    'sms_downGrad' => 'Our dear investor,
     Due to the volume of your investments in the Aseel platform in the last two years, your membership will be changed to regular membership',


    'notify_create' => 'Congrats! You are eligible for (:membership) membership
     Please visit the Dashboard on the website or More in the application',

    'notify_accept' => 'Congrats! Your account has been upgraded to membership (:membership)',

    'notify_downGrad' => 'Our dear investor Due to the volume of your investments in the Aseel platform in the
     last two years, your membership will be changed to regular membership',

    'default_notify_title' => 'Member Ship',
    'notify_title' => 'صندوق مكين الشرق',
    'notify_cmd' => '. الآن عضويتك تمكنك من الاستثمار المبكر في الفرصة'

];
