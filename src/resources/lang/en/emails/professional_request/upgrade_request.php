<?php

return [

    'subject' => 'Your account upgrade request has been received',
    'welcome_message' => 'Dear investor  :name',
    'message' => 'Your request to upgrade your account on Aseel\'s platform to (a professional investor) has been received, and the team will notify you once it is reviewed.',
    'thanks_message' => 'Thank you for your dealings with Aseel platform',
    'info_message' => 'To access your control panel, please click on the link',
    'control_panel_btn' => 'View the control panel',
];
