<?php

namespace App\Support\VirtualBanking\Helpers;

class AnbVA
{
    /**
     * Generate virtual account
     *
     * @param string $customerNo
     * @return array
     */
    static public function generate($customerNo)
    {
        $prefix = self::getAccountPrefix();

        $leadingNo = $prefix . $customerNo;

        $checksum = self::makeSubaccountChecksum($leadingNo);

        $accountNo = $leadingNo . $checksum;

        return [
            'number' => $accountNo,
            'iban' => self::toIban($accountNo)
        ];
    }

    /**
     * Create the subaccount checksum
     *
     * @param string $number
     * @return string
     */
    static protected function makeSubaccountChecksum($number)
    {
        $numberArr = array_map('intval', str_split($number));

        $oddSum = 0;
        $evenSum = 0;

        foreach ($numberArr as $key => $value) {
            if (($key + 1) % 2 === 0) {
                $evenSum += $value;
            } else {
                $oddSum += $value;
            }
        }

        $odd = $oddSum * 3;

        $sum = $odd + $evenSum;

        $rightMost = (int)substr($sum, -1);

        return 9 - $rightMost;
    }

    /**
     * Convert account number to IBAN
     *
     * @param string $accountNo
     * @return string
     */
    static public function toIban($accountNo)
    {
        if (preg_match('/^SA\d\d30100/', $accountNo) === 1) {
            return $accountNo;
        }

        $modulo = bcmod('30100' . $accountNo . '281000', '97');

        $checkDigit = 98 - (int)$modulo;

        if ($checkDigit < 10) {
            $checkDigit = '0' . $checkDigit;
        }

        return 'SA' . $checkDigit . '30100' . $accountNo;
    }

    /**
     * Get account number from the IBAN
     *
     * @param string $iban
     * @return string
     */
    static public function toAccountNumber($iban)
    {
        return substr($iban, -15, 15);
    }

    /**
     * Check if the given IBAN/account number is a subaccount
     *
     * @param string $accountNo
     * @return boolean
     */
    static public function isVA($accountNo)
    {
        $subaccount = $accountNo;

        if (strlen($accountNo) === 24) {
            preg_match('/^SA\d\d30100/', $accountNo, $matches);

            if (!is_array($matches) || count($matches) !== 1) {
                return false;
            }

            $subaccount = substr($accountNo, -15);
        }

        if (substr($subaccount, 0, 3) !== self::getAccountPrefix()) {
            return false;
        }

        $checksum = self::makeSubaccountChecksum(
            $leadingNo = substr($subaccount, 0, 14)
        );

        if (($leadingNo . $checksum) === $subaccount) {
            return true;
        }

        return false;
    }

    /**
     * Get the ANB main account prefix
     *
     * @return string
     */
    static protected function getAccountPrefix()
    {
        return config('services.anb.account_prefix');
    }
}
