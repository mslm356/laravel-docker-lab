<?php

namespace App\Http\Controllers\MobileApp\Investors\Dashboard;

use App\Enums\Investors\InvestorLevel;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\AuthResource;
use App\Http\Resources\Investors\mobileApp\banners\BannersResource;
use App\Models\Banner;
use App\Models\Users\UserCanAccess;
use App\Services\LogService;
use App\Support\Users\UserAccess\ManageUserAccess;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardsController extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function getDashboardData(Request $request)
    {

        try {
            $user = getAuthUser('api');
            $today = Carbon::now()->format('Y-m-d');
            $banners = Banner::where('start_date', '<=', $today)->where('end_date', '>=', $today)->get();

            $profileData = [
                'is_kyc_completed' => $user ? $user->investor->is_kyc_completed : null,
                'kyc_suitability' => $user ? getKycSuitability($user->investor, $user) : null,
                'unsuitability_confirmation' => $user ? $user->investor->unsuitability_confirmation : null,
                'is_log_in' => $user ? true : false,
                'user' => $user ? new AuthResource($user) : null,
                'ShowRate' => false
            ];

            $result = ['banners' => BannersResource::collection($banners), 'profile_data' => $profileData];

            return apiResponse(200, __('mobile_app/message.success'), $result);

        } catch (\Exception $e) {

            $code = $this->logService->log('GET-DASHBOARD-DATA', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function getMainData(Request $request)
    {
        $user = getAuthUser('api');

        if (!$user) {
            return apiResponse(
                401,
                __('mobile_app/message.user_not_valid'),
                [
                    'is_vaild' => false,
                    'membership' => [
                        'current' => 1,
                        'membership_show_popup' => 0,
                        'new_request' => ['id' => null, 'type' => null]
                    ]
                ]
            );
        }

        $user_can_access = UserCanAccess::where('user_id', $user->id)->first();

        if (!$user_can_access) {
            $this->generateAllModules($user);
            $user_can_access = UserCanAccess::where('user_id', $user->id)->first();
        }

        $data['is_vaild'] = ($user_can_access->login == 200) ? true : false;
        $data['is_kyc_completed'] = $user_can_access->is_kyc_completed;
        $data['unsuitability_confirmation'] = $user_can_access->unsuitability_confirmation;
        $data['investment'] = ($user_can_access->investment == 200) ? true : false;
        $data['bank_account'] = ($user_can_access->bankAccount == 200) ? true : false;
        $data['transfer'] = ($user_can_access->transfer == 200) ? true : false;
        $data['kyc_suitability'] = $user_can_access->kyc_suitability;

        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['email'] = $user->email;
        $data['showRate'] = ($user_can_access->show_app_rate)?true:false;

        $data['membership']['current'] = ($user->member_ship != 0)?$user->member_ship:1;
        $data['membership']['new_request']['id'] = ($user_can_access->last_membership_request) ? json_decode($user_can_access->last_membership_request)->id : null;
        $data['membership']['new_request']['type'] = ($user_can_access->last_membership_request) ? json_decode($user_can_access->last_membership_request)->member_ship : null;
        $data['membership']['membership_show_popup'] = ($user_can_access->last_membership_request) ? json_decode($user_can_access->last_membership_request)->show_popup : null;

        $data['upgrade_request_level']['value'] = $user_can_access->upgrade_request_level;
        $data['upgrade_request_level']['description'] = ($user_can_access->upgrade_request_level) ? InvestorLevel::getDescription($user_can_access->upgrade_request_level) : '';
        $data['nin'] = ($user_can_access->national_identity_data) ? json_decode($user_can_access->national_identity_data)->nin : null;
        $data['accounts']['iban'] = ($user_can_access->is_kyc_completed)?$user_can_access->iban:'************************';
        $data['accounts']['bank_account'] = ($user_can_access->is_kyc_completed && $user_can_access->iban) ? AnbVA::toAccountNumber($user_can_access->iban) : '****************';

        return apiResponse(200, __('mobile_app/message.success'), $data);


    }

    public function generateAllModules($user)
    {
        $manageUserAccess = new ManageUserAccess();
        $manageUserAccess->handle($user);

    }

    public function showRateStatus($user)
    {
        $result = false;

        //        if (config('rate.show_rate') != 'ACTIVE')
        //            return $result;

        $hasRate = DB::table('rates')
            ->where('user_id', $user->id)
            ->count();

        $hasPayout = DB::table('investor_payout')
            ->where('investor_id', $user->id)
            ->where('paid', '>', 0)
            ->count();

        if (!$hasRate && $hasPayout) {
            $result = true;
        }

        return $result;
    }
}
