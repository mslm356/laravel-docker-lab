<?php

namespace App\Http\Controllers\Investors\General;

use App\Http\Controllers\Controller;
use App\Models\WebsiteContent;
use Illuminate\Support\Facades\App;

class GetWebsiteContent extends Controller
{
    /**
     * Get the content of the required page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(WebsiteContent $page)
    {
        return response()->json([
            'data' => [
                'content' => $page->content->get(App::getLocale())
            ]
        ]);
    }
}
