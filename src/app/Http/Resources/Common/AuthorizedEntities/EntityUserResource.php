<?php

namespace App\Http\Resources\Common\AuthorizedEntities;

use Illuminate\Http\Resources\Json\JsonResource;

class EntityUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone_without_country,
            'phone_country_code' => $this->phone_country,
            'fmt_phone' => $this->phone_number,
        ];
    }
}
