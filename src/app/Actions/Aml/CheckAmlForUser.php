<?php

namespace App\Actions\Aml;

use App\Enums\AML\AmlType;
use App\Support\Aml\AmlManager;
use Lorisleiva\Actions\Concerns\AsAction;

class CheckAmlForUser
{
    use AsAction;

    /**
     * Check if the investor whether the investor can invest
     *
     * @param \App\Models\Listings\Listing $listing
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $currentInvest
     * @param int $toBeInvested
     *
     * @return true|int
     * @throws \App\Exceptions\NoBalanceException
     * @throws \App\Exceptions\NoInvestorWalletFound
     */
    public function handle($user, $checkType, $initiatorId = null)
    {
        $amlManager = new AmlManager;

        $lists = $amlManager->getAllLists();

        $investorAml = $user->amlHistory()->create([
            'check_date' => now()->format('Y-m-d'),
            'type' => $checkType,
            'initiator_id' => $checkType === AmlType::AutoAssessment ? null : $initiatorId
        ]);

        $checks = [];

        foreach ($lists as $list) {
            $checks[] = CheckIfUserInList::run($list, $user, $investorAml);
        }

        $investorAml->setRelation('checks', collect($checks));

        return $investorAml;
    }
}
