<?php

namespace App\Enums\Listings;

use BenSampo\Enum\Enum;

final class ListingReviewStatus extends Enum
{
    const PendingReview = 1;
    const InReview = 2;
    const Approved = 3;
    const Rejected = 4;
    const NeedMoreInformation = 5;
}
