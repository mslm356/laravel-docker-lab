<nav class="relative flex items-center justify-between max-w-screen-xl px-4 mx-auto sm:px-6">
    <div class="flex items-center flex-1">
        <div class="flex items-center justify-between w-full md:w-auto">
            <a href="#" aria-label="Home">
                <img
                    class="w-auto h-20 sm:h-24"
                    src="{{ asset('images/aseel-white.png') }}"
                    alt="Logo"
                >
            </a>
            {{-- <div class="flex items-center -mr-2 md:hidden">
                <button type="button"
                    class="inline-flex items-center justify-center p-2 text-gray-400 transition duration-150 ease-in-out rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500"
                    id="main-menu" aria-label="Main menu" aria-haspopup="true">
                    <svg class="w-6 h-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                </button>
            </div> --}}
        </div>
    </div>
    <div class="block text-right">
        <span class="inline-flex rounded-md shadow-md">
          <span class="inline-flex rounded-md ring-1 ring-black ring-opacity-5">
            <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale() === 'en' ? 'ar' : 'en') }}" class="inline-flex items-center px-4 py-2 text-base font-medium leading-6 transition duration-150 ease-in-out bg-white border border-transparent rounded-md text-primary hover:bg-gray-50 focus:outline-none">
              {{ App::getLocale() === 'en' ? 'العربية' : 'English' }}
            </a>
          </span>
        </span>
      </div>
</nav>
