<?php

namespace App\Console\Commands\Transfer;

use App\Enums\Role;
use App\Enums\Transfer\TransferStatus;
use App\Exceptions\FailedProcessException;
use App\Exceptions\NoBalanceException;
use App\Models\Transfer\TransferRequest;
use App\Models\Users\User;
use App\Models\VirtualBanking\Wallet;
use App\Services\Investors\TransferServices;
use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class RemoveRefundForSpecialCase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb-transfer:remove-special-refund';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    public $count = 0;
    public $transfer_id = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public $transferServices;
    public $logService;

    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
        $this->transferServices = new TransferServices();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $ids = [];

            foreach ($ids as $anbTransferId) {

                $anbTransfer = DB::table('anb_transfers')
                    ->where('id', $anbTransferId)
                    ->select('id', 'initiator_id', 'transaction_id', 'amount', 'data')
                    ->first();

                if (!$anbTransfer) {
                    dump('not valid anb transfer id ' . $anbTransferId);
                    continue;
                }

                $eodTransaction = DB::table('anb_eod_transactions')
                    ->where('customer_ref', 'AS:' . $anbTransferId)
                    ->count();

                if (!$eodTransaction) {
                    dump('eod not valid ' . $anbTransferId);
                    continue;
                }

                $anbTransfer = (array) $anbTransfer;

                $anbTransferData = json_decode($anbTransfer['data'],1);

                if (isset($anbTransferData["is_refunded"]) && $anbTransferData["is_refunded"]) {

                    if (isset($anbTransferData["refund_transaction_id"])) {

                        $deleteStatus = DB::table('transactions')
                            ->where('id', $anbTransferData["refund_transaction_id"])
                            ->update(['payable_id'=> 2, 'wallet_id' => 2]);

                        if ($deleteStatus) {
                            $this->updateWallet($anbTransfer['initiator_id']);

                        }else {
                            dump('transaction not valid or not deleted');
                        }

                    }else {
                        dump('refund_transaction_id not found');
                    }

                }else {
                    dump('is refund not found or not true');
                }
            }


        } catch (\Exception $e) {
            dump($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }

    public function updateWallet ($user_id) {

        $wallet = Wallet::where('holder_id', $user_id)
            ->where('holder_type', User::class)
            ->first();

        $oldBalance = $wallet->balance;

        if (!$wallet) {
            dd('wallet not valid');
        }

        $wallet->refreshBalance();

        dump('done successfully old balance => ' . $oldBalance . ' new => ' . $wallet->balance . ' wallet_id => ' . $wallet->id . ' user_id => ' . $user_id );
    }

}
