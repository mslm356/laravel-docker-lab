<?php

namespace App\Enums\AML;

use BenSampo\Enum\Enum;

final class AmlScreenResult extends Enum
{
    const False = 0;
    const True = 1;
    const Pending = 3;
}
