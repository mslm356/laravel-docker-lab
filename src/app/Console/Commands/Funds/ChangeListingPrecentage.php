<?php

namespace App\Console\Commands\Funds;

use App\Jobs\UpdateFundPercentageFirebase;
use App\Models\Listings\Listing;
use App\Services\CacheServices;
use Illuminate\Console\Command;

class ChangeListingPrecentage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change-percentage {--listing_id=} {--percentage=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listing_id = $this->option('listing_id');
        $percentage =$this->option('percentage');
        $listing = Listing::find($listing_id);

        if (!$listing) {
            dd('error! fund not valid');
        }

        if (!$percentage) {
            dd('error! enter percentage');
        }


        $current_precent=( $listing->total_invested_shares / $listing->total_shares ) * 100;

        $changed_precentage= $percentage - $current_precent;

        $listing->update(['changed_investment_precentage'=>$changed_precentage]);

        UpdateFundPercentageFirebase::dispatch($listing->id, $listing->percentage)
            ->onQueue('firebase');

        $cacheServices= new CacheServices();
        $cacheServices->updateFundCache($listing->id);

        dd('done');
    }
}
