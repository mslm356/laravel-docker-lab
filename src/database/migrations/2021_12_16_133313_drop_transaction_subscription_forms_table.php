<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTransactionSubscriptionFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('transaction_subscription_forms');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('transaction_subscription_forms', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_reference');
            $table->uuid('file_id')->nullable();
            $table->json('data');
            $table->timestamps();

            $table->unique(['transaction_reference', 'file_id'], 'trans_sub_forms_trans_ref_file_id');
        });
    }
}
