<?php

namespace App\Http\Resources\AuthorizedEntities\Listings;

use App\Http\Resources\Common\Listings\InvestorPayoutResource as CommonInvestorPayoutResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvestorPayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonInvestorPayoutResource($this->resource))->toArray($request),
            []
        );
    }
}
