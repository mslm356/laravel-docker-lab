<?php

namespace App\Models\AML;

use App\Models\AppModel;

class AmlListCheck extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'collection'
    ];
}
