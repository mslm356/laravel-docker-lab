<?php

namespace App\Http\Controllers\MobileApp;

use App\Http\Controllers\Controller;
use App\Http\Resources\mobileApp\NotificationResource;
use App\Services\LogService;
use App\Traits\NotificationServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class NotificationsController extends Controller
{
    use NotificationServices;

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function allNotifications(Request $request)
    {
        try {
            $user = getAuthUser('api');

            $notifications = DB::table('notifications')
                ->where('notifications.notifiable_id', $user->id)
                ->select(['notifications.created_at', 'notifications.updated_at', 'notifications.id','notifications.notification_id',
                    'notifications.type', 'notifications.data', 'notifications.read_at'])
                ->orderBy('notification_id', 'desc')
                ->simplePaginate(10);

            $items = NotificationResource::collection($notifications->items());

            $data['items'] = $items;

            $data = array_merge($data, simplePaginationMetaData($notifications));

        } catch (\Exception $e) {
            $code = $this->logService->log('GET-ALL-NOTIFICATIONS', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }

        return apiResponse(200, trans('mobile_app/message.success'), $data);
    }

    public function updateNotification(Request $request, $id)
    {
        try {

            $user = getAuthUser('api');

            $status= DB::table('notifications')
                 ->where('notification_id', $id)
                 ->where('notifiable_id', $user->id)
                ->update(['read_at' => now()->format('Y-m-d H:i:s')]);

            if ($status) {
                return apiResponse(200, __('mobile_app/message.success'));
            }

            return apiResponse(400, __('mobile_app/message.notification.not.valid'));


        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            $code = $this->logService->log('ValidationException-UPDATE-NOTIFICATION', $e);
            return apiResponse(400, $e->getMessage(). ' : ' .  $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('UPDATE-NOTIFICATION', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }
}
