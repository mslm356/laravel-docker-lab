<?php

namespace App\Mail\SubscriptionForm;

use App\Models\SubscriptionFormRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $subscriptionFormRequest;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SubscriptionFormRequest $subscriptionFormRequest, $user)
    {
        $this->subscriptionFormRequest = $subscriptionFormRequest;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';
        $profileUrl = config('app.investor_url') . '/app/overview';
        $reason = $this->subscriptionFormRequest ? $this->subscriptionFormRequest->reviewer_comment : '---';


        return $this->view('emails.subscription_forms.reject_request', compact('profileUrl', 'name', 'reason'))
            ->subject(__('emails/subscription_forms/reject_request.subject'));
    }
}
