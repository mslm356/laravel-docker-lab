<?php

namespace App\Http\Controllers;


use App\Actions\Aml\InvestorUpdateStatus;
use App\Actions\Investors\UpdateYaqeenDataAction;
use App\Enums\CompanyProfile\ApproveRequest;
use App\Enums\Elm\NatheerNotifiaction;
use App\Enums\Investors\ProfileReviewType;
use App\Models\Investors\InvestorProfileLog;
use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NatheerController extends Controller
{

    public $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }

    public function updateStatus(Request $request)
    {
        $this->logServices->log('NATHEER-NOTIFICATION', null, ['response', $request->all()]);


        try {
            foreach ($request->all() as $request_data) {
                DB::transaction(function () use ($request_data) {


                    // investors and it's companies
                    $users = User::whereHas('nationalIdentity', function ($query) use ($request_data) {
                        $query->where('nin', $request_data['primaryId']);
                    })->with('nationalIdentity')->get();


                    if ($users->count() == 0)
                        return "ERROR";

                    $nateer_id = optional(User::where('email', 'natheer@aseel.com')->first())->id;

                    foreach ($users as $user) {

                        $status=$this->getAccountStatus($user,$request_data['notificationCode'],$request_data['parameterList']);

                        $prepared_data = [
                            'status' => $status,
                            'reviewer_comment' => NatheerNotifiaction::getDescription($request_data['notificationCode']),
                            'reviewer_id' => $nateer_id,
                            'review_type' => ProfileReviewType::Automatic,
                        ];

                        InvestorUpdateStatus::run($user, $prepared_data, $nateer_id);

                        $prepared_data['natheer_request_data'] = $request_data;
                        InvestorProfileLog::create(['user_id' => $user->id, 'data' => json_encode($prepared_data), 'type' => 'natheer_log', 'message' => 'natheer notification']);

                        $prepared_data = [];
                    }


                });


            }
            return "SUCCESS";
        } catch (\Exception $e) {
            $this->logServices->log('NATHEER-STATUS-EXCEPTION', $e);
            return "ERROR";
        }

    }

    function getAccountStatus($user,$code,$param_list)
    {
        if($code == 3000 || $code == 30000 || ($code ==1000 && $param_list !=null && $param_list['Parameter-0']=='Y'))
            return ApproveRequest::Suspended;

        if ($code == 6000)
        {
            $yaqeen_action = new UpdateYaqeenDataAction();
            $yaqeen_action->handle($user);
        }

        return ApproveRequest::Approved;

    }
}
