<div class="flex px-3 py-1 text-white bg-primary">
    <div>{{ $field['ar'] }}</div>
    <div class="flex-grow"></div>
    <div>{{ $field['en'] }}</div>
</div>
