<?php

namespace App\Models;

use App\Enums\Listings\VoteAnswers;
use App\Models\Users\User;

class InvestorVote extends AppModel
{
    protected $table = 'investor_votes';

    public function investor()
    {
        return $this->belongsTo(User::class, 'investor_id');
    }


    public function getVoteAnswerAttribute()
    {
        if($this->answer == VoteAnswers::Approved) {
            $value = __('messages.approved');
        } elseif ($this->answer == VoteAnswers::Rejected) {
            $value = __('messages.rejected');
        } else {
            $value = __('messages.refrain');
        }

        return $value;
    }
}
