<?php

namespace App\Support\Elm\Yaqeen\Repositories;

use Exception;
use Illuminate\Support\Carbon;

class FakeRepository
{
    /**
     * Get the citizen information
     *
     * @param string $nin
     * @param string $dateOfBirth
     * @throws \Exception
     */
    public function getCitizenInfo($nin, $dateOfBirth)
    {
        $citizenInfo =  (object)[
            'englishFirstName' => 'JOE',
            'englishSecondName' => 'DOE',
            'englishThirdName' => 'FOO',
            'englishLastName' => 'BAR',
            'firstName' => 'جو',
            'fatherName' => 'دو',
            'grandFatherName' => 'فو',
            'familyName' => 'بر',
            'idExpiryDate' => '15-09-1449',
            'gender' => 'M',
            'dateOfBirthH' => "03-{$dateOfBirth}",
        ];

        return [
            'nin' => $nin,
            'birth_date_type' => 'hijri',
            'first_name' => ['en' => $citizenInfo->englishFirstName, 'ar' => $citizenInfo->firstName],
            'second_name' => ['en' => $citizenInfo->englishSecondName, 'ar' => $citizenInfo->fatherName],
            'third_name' => ['en' => $citizenInfo->englishThirdName, 'ar' => $citizenInfo->grandFatherName],
            'forth_name' => ['en' => $citizenInfo->englishLastName, 'ar' => $citizenInfo->familyName],
            'id_expiry_date' => Carbon::createFromFormat('d-m-Y', $citizenInfo->idExpiryDate)->format('Y-m-d'),
            'gender' => strtoupper($citizenInfo->gender) === 'M' ? 'male' : 'female',
            'nationality' => 'السعودية',
            'birth_date' => Carbon::createFromFormat('d-m-Y', $citizenInfo->dateOfBirthH)->toDateString(),
        ];
    }

    /**
     * Get the information by Iqama
     *
     * @param string $iqamaNumber
     * @param string $dateOfBirth
     * @throws \Exception
     */
    public function getInfoByIqama($iqamaNumber, $dateOfBirth)
    {
        $iqamaInfo =  (object)[
            'englishFirstName' => 'JOE',
            'englishSecondName' => 'DOE',
            'englishThirdName' => 'FOO',
            'englishLastName' => 'BAR',
            'firstName' => 'جو',
            'fatherName' => 'دو',
            'grandFatherName' => 'فو',
            'familyName' => 'بر',
            'nationalityDesc' => 'الولايات المتحدة',
            'idExpiryDate' => '01-08-1444',
            'gender' => 'M',
            'dateOfBirthG' => "01-{$dateOfBirth}",
        ];

        return [
            'nin' => $iqamaNumber,
            'birth_date_type' => 'gregorian',
            'first_name' => ['en' => $iqamaInfo->englishFirstName, 'ar' => $iqamaInfo->firstName],
            'second_name' => ['en' => $iqamaInfo->englishSecondName, 'ar' => $iqamaInfo->fatherName],
            'third_name' => ['en' => $iqamaInfo->englishThirdName, 'ar' => $iqamaInfo->grandFatherName],
            'forth_name' => ['en' => $iqamaInfo->englishLastName, 'ar' => $iqamaInfo->familyName],
            'id_expiry_date' => Carbon::createFromFormat('d-m-Y', $iqamaInfo->idExpiryDate)->format('Y-m-d'),
            'gender' => strtoupper($iqamaInfo->gender) === 'M' ? 'male' : 'female',
            'nationality' => $iqamaInfo->nationalityDesc,
            'birth_date' => Carbon::createFromFormat('d-m-Y', $iqamaInfo->dateOfBirthG)->toDateString(),
        ];
    }

    /**
     * Get the citizen address information
     *
     * @param string $nin
     * @param string $dateOfBirth
     * @param string $locale
     * @throws \Exception
     */
    public function getCitizenAddress($nin, $dateOfBirth, $locale)
    {
        return $this->getFakeAddresses($locale);
    }

    /**
     * Get the citizen address information
     *
     * @param string $iqama
     * @param string $dateOfBirth
     * @param string $locale
     * @throws \Exception
     */
    public function getAddressInfoByIqama($iqama, $dateOfBirth, $locale)
    {
        return $this->getFakeAddresses($locale);
    }

    /**
     * Get the citizen address information
     *
     * @param string $iqama
     * @param string $dateOfBirth
     * @param string $locale
     * @throws \Exception
     */
    public function getCompanyAddressInfoByCr($cr, $locale)
    {
        return $this->getFakeAddresses($locale);
    }

    protected function getFakeAddresses($locale)
    {
        if ($locale === 'en') {
            return $this->mapAddresses([
                (object)[
                    'postCode' => '11226',
                    'additionalNumber' => '1441',
                    'buildingNumber' => '9',
                    'city' => 'Dammam',
                    'district' => 'Alkalij',
                    'locationCoordinates' => '26.68762 45.91684',
                    'streetName' => 'Omran Ibn Al fadl Street',
                    'unitNumber' => '12',
                ],
                (object)[
                    'postCode' => '16872',
                    'additionalNumber' => '239',
                    'buildingNumber' => '1',
                    'city' => 'Riyadh',
                    'district' => 'An Nuzhah',
                    'locationCoordinates' => '26.68762 22.91684',
                    'streetName' => 'Ahmed Bin Essa Street',
                    'unitNumber' => '12',
                ],
            ]);
        }

        return $this->mapAddresses([
            (object)[
                'postCode' => '١١٣٢٦٥',
                'additionalNumber' => '١٤٤١',
                'buildingNumber' => '٩',
                'city' => 'الدمام',
                'district' => 'الخليج',
                'locationCoordinates' => '٢٦٫٦٦٥٩ ٩٥٫٦٦٩٨٨',
                'streetName' => 'شارع عمران بن الفضل',
                'unitNumber' => '١٢',
            ],
            (object)[
                'postCode' => '١٦٨٧',
                'additionalNumber' => '٢٣٢١',
                'buildingNumber' => '١',
                'city' => 'الرياض',
                'district' => 'النزهة',
                'locationCoordinates' => '٢٦٫٦٦٥٩ ٩٥٫٦٦٩٨٨',
                'streetName' => 'شارع احمد بن عيسى',
                'unitNumber' => '١٢',
            ],
        ]);
    }

    protected function mapAddresses(array $addresses)
    {
        return collect($addresses)
            ->map(fn ($address) => [
                'post_code' => $address->postCode,
                'additional_number' => $address->additionalNumber,
                'building_number' => $address->buildingNumber,
                'city' => $address->city,
                'district' => $address->district,
                'location' => $address->locationCoordinates,
                'street_name' => $address->streetName,
                'unit_number' => $address->unitNumber,
            ])
            ->toArray();
    }
}
