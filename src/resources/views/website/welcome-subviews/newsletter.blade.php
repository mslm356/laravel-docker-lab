<div class="relative bg-white bg-right bg-cover" style="background-image: url({{ asset('images/newsletter-bg.jpg') }})">
    <div
        class="relative z-40 px-6 py-10 md:py-16 md:px-12 lg:py-24 lg:px-16 sm:flex sm:items-center sm:flex-col">
        <div>
            <h2 class="text-2xl font-extrabold leading-8 tracking-tight text-white sm:text-5xl sm:leading-9 sm:text-center">
                {{ __('website.newsletter.title') }}
            </h2>
            <p class="max-w-3xl mt-3 text-lg leading-6 text-white sm:text-center sm:text-2xl" id="newsletter-headline">
                {{ __('website.newsletter.cta') }}
            </p>
        </div>
        <div class="mt-8 sm:w-full sm:max-w-md">
            <form class="sm:flex" aria-labelledby="newsletter-headline">
                <input aria-label="Email address" type="email" required
                    class="w-full px-5 py-3 text-base leading-6 text-gray-900 placeholder-gray-500 transition duration-150 ease-in-out bg-white border border-transparent rounded-md appearance-none focus:outline-none focus:placeholder-gray-400"
                    placeholder="{{ __('website.newsletter.email_placeholder') }}">
                <div class="mt-3 rounded-md shadow sm:mt-0 sm:ltr:ml-3 sm:rtl:mr-3 sm:flex-shrink-0">
                    <button
                        class="flex items-center justify-center w-full h-full px-5 py-3 text-base font-medium leading-6 text-white transition duration-150 ease-in-out border border-transparent rounded-md bg-secondary-500 hover:bg-secondary-400 focus:outline-none">
                        {{ __('website.newsletter.notify_me') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="absolute inset-0 z-10 w-full h-full opacity-75 bg-gradient-to-r from-gray-300 to-primary-900 sm:opacity-50"></div>
</div>
