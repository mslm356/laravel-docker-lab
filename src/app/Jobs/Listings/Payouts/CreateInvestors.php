<?php

namespace App\Jobs\Listings\Payouts;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateInvestors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $listing;
    public $payout;
    public $totalInstance;
    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listing, $payout, $totalInstance)
    {
        $this->listing = $listing;
        $this->payout = $payout;
        $this->totalInstance = $totalInstance;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            $listing = $this->listing;
            $payout = $this->payout;
            $totalInstance = $this->totalInstance;

            $investors = $listing->investors;

            $investedShares = $investors->sum('pivot.invested_shares');

            $payoutPerShare = $totalInstance->divide($investedShares);

            $investorPayouts = [];

            foreach ($investors as $index=>$investor) {

                $paid = ($payoutPerShare->multiply($investor->pivot->invested_shares))->getAmount();

                $investorPayouts[$investor->id] = [
                    'paid' => $payout->type == 'payout' ? $paid : 0,
                    'expected' => $paid,
                    'currency' => defaultCurrency(),
                    'shares' => $investor->pivot->invested_shares
                ];


//                if ($payout->type == 'payout') {
//                    $this->payForInvestor($payout, $paid, $investor);
//
//                    DB::table('listing_investor')
//                        ->where('investor_id', $investor->id)
//                        ->where('listing_id', $payout->listing_id)
//                        ->update(['paid_payout'=>$paid]);
//                }

//                if ($payout->type == 'payout')
//                {
//                    $this->payForInvestor($payout, $paid, $investor);
//
//                    DB::table('listing_investor')
//                        ->where('investor_id',$investor->id)
//                        ->where('listing_id',$payout->listing_id)
//                        ->update(['paid_payout'=>$paid]);
//                }

            }

            $payout->investors()->sync($investorPayouts);

            $listing->payout_done = $payout->type == 'payout' ? 1 : 0;
            $listing->save();

            $payout->invested_units = $investedShares;
            $payout->unit_price =  $totalInstance->divide($investedShares)->getAmount();
            $payout->save();

//            if ($payout->type == 'payout') {
//                $this->updateWalletBalance($payout);
//            }

        } catch (\Exception $e) {
            $this->logService->log('PAYOUT-CREATE-INVESTOR-EXC', $e);
        }
    }

    public function payForInvestor($payout, $paid, $investor)
    {

        if ($paid == 0) {
            return false;
        }

        $wallet = $payout->listing->getWallet(WalletType::ListingSpv);
        $investorWallet = $investor->getWallet(WalletType::Investor);

        if (!$wallet || !$investorWallet) {
            return false;
        }

        $meta = [
            'reason' => TransactionReason::PayoutDistribution,
            'reference' => transaction_ref(),
            'value_date' => now('UTC')->toDateTimeString(),
            'listing_id' => $payout->listing_id,
            'listing_title_en' => $payout->listing->title_en,
            'listing_title_ar' => $payout->listing->title_ar,
            'payout_id' => $payout->id,
            'user_id' => $investor->id,
            'user_name' => $investor->full_name
        ];

        DB::table('transactions')->insert([
            'payable_type' => Listing::class,
            'payable_id' => $payout->listing_id,
            'wallet_id' => $wallet->id,
            'type' => 'withdraw',
            'amount' => ($paid * -1),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('transactions')->insert([
            'payable_type' => User::class,
            'payable_id' => $investor->id,
            'wallet_id' => $investorWallet->id,
            'type' => 'deposit',
            'amount' => ($paid),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $investorBalance = DB::table('transactions')
            ->where('payable_id', $investor->id)
            ->where('payable_type', User::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('holder_id', $investor->id)
            ->where('holder_type', User::class)
            ->update(['balance' => $investorBalance,'is_overview_statistics_consistent'=>false]);

        return true;
    }

    public function updateWalletBalance($payout)
    {

        $wallet = $payout->listing->getWallet(WalletType::ListingSpv);

        $walletBalance = DB::table('transactions')
            ->where('payable_id', $payout->listing_id)
            ->where('wallet_id', $wallet->id)
            ->where('payable_type', Listing::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('id', $wallet->id)
            ->update(['balance' => $walletBalance,'is_overview_statistics_consistent'=>false]);

    }
}
