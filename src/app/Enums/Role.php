<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Role extends Enum
{
    const Investor =   'Investor';
    const Admin =   'Admin';
    const PayoutsCollector = 'PayoutsCollector';
    const AnbStatementCollector = 'AnbStatementCollector';
    const AuthorizedEntityAdmin = 'AuthorizedEntityAdmin';

    public static function getScopeOfRole($role)
    {
        if (in_array($role, [static::Investor])) {
            return AppScope::Investor;
        } else if (in_array($role, [static::Admin])) {
            return AppScope::Admin;
        } else if (in_array($role, [static::AuthorizedEntityAdmin])) {
            return AppScope::AuthorizedEntity;
        }
    }
}
