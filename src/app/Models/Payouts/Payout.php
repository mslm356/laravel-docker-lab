<?php

namespace App\Models\Payouts;

use App\Models\AppModel;
use App\Models\Users\User;
use App\Models\Listings\Listing;

class Payout extends AppModel
{
    /**
     * Get the of the of listings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }

    /**
     * Get list of investors
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function investors()
    {
        return $this->belongsToMany(
            User::class,
            'investor_payout',
            'payout_id',
            'investor_id'
        )
            ->withPivot('paid', 'expected', 'shares')->orderByDesc('expected')
            ->withTimestamps();
    }
}
