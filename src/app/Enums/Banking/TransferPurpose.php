<?php

namespace App\Enums\Banking;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class TransferPurpose extends Enum implements LocalizedEnum
{
    const BillsRent = '20';
    const ExpensesServices = '21';
    const PurchaseAssets = '22';
    const SavingInvestment = '23';
    const GovernmentDues = '24';
    const MoneyExchange = '25';
    const CreditCardLoan = '26';
    const GiftOrReward = '27';
    const PersonalLoanAssistance = '28';
    const InvestmentTransaction = '29';
    const FamilyAssistance = '30';
    const Donation = '31';
    const PayrollBenefits = '32';
    const OnlinePurchase = '33';
    const HajjAndUmra = '34';
    const DividendPayment = '35';
    const GovernmentPayment = '36';
    const InvestmentHouse = '37';
    const PaymentToMerchant = '38';
    const OwnAccountTransfer = '39';
}
