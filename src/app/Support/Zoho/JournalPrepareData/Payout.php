<?php


namespace App\Support\Zoho\JournalPrepareData;


use App\Models\Users\User;
use App\Models\ZohoSettings;
use Carbon\Carbon;

class Payout
{

    public function preparePayoutForInvestors ($investorsData, $firstAccount, $secondAccount, $total, $listing) {

        $firstAccountName = ZohoSettings::where('key', $firstAccount)->first();
        $secondAccountName = ZohoSettings::where('key', $secondAccount)->first();

        $firstAccountValue =  $firstAccountName ? $firstAccountName->value : '';
        $secondAccountValue =  $secondAccountName ? $secondAccountName->value : '';

        $data = [
            'journal_date' => Carbon::now()->format('Y-m-d'),
            'line_items' => [
                [
                    'account_id' => $firstAccountValue,
                    'customer_id' => $listing ? $listing->zoho_id : null,
                    'description' => 'get amount from ' . $firstAccount,
                    'debit_or_credit' => 'debit',
                    'amount' => $total,
                ],
            ],
        ];

        foreach ($investorsData as $index=>$investorData) {

            $user = User::find($investorData['id']);

            $data['line_items'][$index+1] = [

                'account_id' => $secondAccountValue,
                'customer_id' => $user ? $user->zoho_id : null,
                'description' => 'add amount to ' . $secondAccount,
                'debit_or_credit' => 'credit',
                'amount' => $investorData['amount'],
            ];
        }

        return $data;
    }

    public function preparePayout ($listing, $firstAccount, $secondAccount, $amount) {

        $firstAccountName = ZohoSettings::where('key', $firstAccount)->first();
        $secondAccountName = ZohoSettings::where('key', $secondAccount)->first();

        $firstAccountValue =  $firstAccountName ? $firstAccountName->value : '';
        $secondAccountValue =  $secondAccountName ? $secondAccountName->value : '';

        $data = [
            'journal_date' => Carbon::now()->format('Y-m-d'),
            'line_items' => [
                [
                    'account_id' => $firstAccountValue,
                    'description' => 'get amount from ' . $firstAccount,
                    'debit_or_credit' => 'debit',
                    'amount' => $amount,
                ],
                [
                    'account_id' => $secondAccountValue,
                    'customer_id' => $listing ? $listing->zoho_id : '',
                    'description' => 'add amount to ' . $secondAccount,
                    'debit_or_credit' => 'credit',
                    'amount' => $amount,
                ]
            ],
        ];

        return $data;
    }
}
