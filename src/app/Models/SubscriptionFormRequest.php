<?php

namespace App\Models;

use App\Models\Users\User;

class SubscriptionFormRequest extends AppModel
{

    protected $table = 'subscription_form_requests';

    protected $fillable = ['user_id','reviewer_id','status','reviewer_comment'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id');
    }

}
