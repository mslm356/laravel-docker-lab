<?php

return [
    'transfer_limit' => env('TRANSFER_LIMIT', 15000),
    'transfer_operation_limit' => env('TRANSFER_OPERATION_LIMIT', 2),
];
