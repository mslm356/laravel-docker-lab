<?php


namespace App\Services\Files;

use Illuminate\Validation\ValidationException;

class CheckFile
{

    public function handle($file, array $extensions = null)
    {

        $defaultExtensions = ['pdf', 'png', 'jpg', 'jpeg', 'heic', 'heif'];

        $extensions = $extensions ? $extensions : $defaultExtensions;
        if (is_array($file))
            $file = $file['file'];

        $extension = strtolower($file->getClientOriginalExtension());

        if (!$this->checkFileExtensions($extension, $extensions)) {
            throw ValidationException::withMessages([__('messages.file_not_valid')]);
        }
    }

    public function checkFileExtensions($extension, $extensions)
    {
        if (in_array($extension, $extensions)) {
            return true;
        }

        return false;
    }
}
