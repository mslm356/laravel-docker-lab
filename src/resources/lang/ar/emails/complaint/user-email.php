<?php

return [
    'dear' => 'عزيزي الكريم :name',
    'subject' => 'تاكيد استلام شكواكم :ticketId',
    'message' =>  ' تم استلام شكواكم :ticketNumber' . ' وسيتم التواصل معكم قريبًا بالتحديثات.',
    'thanks' => 'أطيب التحيات ،',
];
