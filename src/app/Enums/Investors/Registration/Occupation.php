<?php

namespace App\Enums\Investors\Registration;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;
use App\Enums\Investors\Registration\Interfaces\AlertMessgesInterface;
use App\Enums\Investors\Registration\Interfaces\DBColumInterface;

final class Occupation extends Enum implements LocalizedEnum,AlertMessgesInterface,DBColumInterface
{
    const Retired =   1;
    const Employer =   2;
    const Employee =   3;
    const Unemployed =   4;
    const Student =   5;
    const Other =   6;


    function getAlertMessages()
    {
    }



    function getDBColumnName()
    {
        return 'occupation';
    }


}



