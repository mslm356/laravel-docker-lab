<?php

namespace App\Support\Zatca\EInvoice;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class Seller
{
    /**
     * Seller name
     *
     * @var string
     */
    protected $name;

    /**
     * VAT ID
     *
     * @var string
     */
    protected $vatId;

    /**
     * CR Number
     *
     * @var string
     */
    protected $crNumber;

    /**
     * Addresses
     *
     * @var \App\Support\Zatca\EInvoice\SellerAddress
     */
    protected $address;

    function __construct(array $name, string $crNumber, string $vatId, SellerAddress $address)
    {
        $this->name = $name;
        $this->crNumber = $crNumber;
        $this->vatId = $vatId;
        $this->address = $address;
    }

    /**
     * Get seller name
     *
     * @return string
     */
    public function getName(string $locale = null)
    {
        if ($locale === null) {
            $locale = App::getLocale();
        }

        return Arr::get($this->name, $locale);
    }

    /**
     * Get CR number
     *
     * @return string
     */
    public function getCrNumber()
    {
        return $this->crNumber;
    }

    /**
     * Get VAT ID
     *
     * @return string
     */
    public function getVatId()
    {
        return $this->vatId;
    }

    /**
     * Get address
     *
     * @return \App\Support\Zatca\EInvoice\SellerAddress
     */
    public function getAddress()
    {
        return $this->address;
    }
}
