<?php

namespace App\Console\Commands\Anb;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Jobs\Anb\AnbEodStatement;
use App\Models\Anb\AnbTransfer;
use App\Models\BankAccount;
use App\Models\Users\User;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Enums\AnbPaymentStatus;
use App\Support\VirtualBanking\Helpers\LocalSaudiTransferFee;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LostAnbTransfer extends Command
{
//    -**
//     * The name and signature of the console command.
//     *
//     * @var string
//     *-
    protected $signature = 'anb:lost-anb-transfer {{--transaction=}}';

//    -**
//     * The console command description.
//     *
//     * @var string
//     *-
    protected $description = 'Get PAYMENT ';

//    -**
//     * Create a new command instance.
//     *
//     * @return void
//     *-
    public function __construct()
    {
        parent::__construct();
    }

//    -**
//     * Execute the console command.
//     *
//     * @return int
//     *-
    public function handle()
    {

        try {

            $dataNotFound = [];

            $data = [
//                6545,
                12214,
                12740,
                15675,
                2484,
                2485,
                7493,
                7497,
                7498,
                7524,
                7530,
                7531,
                7494,
                12743,
                14831,
                14832,
                15506,
                12455,
                13612,
                14788,
                15501,
                15502,
                15508,
                15509,
                15666,
                2229,
                13618,
                12770,
                12772,
                12972,
                13404,
                12974,
                13406,
                13589,
                12736,
                12737,
                13587,
                12951,
                13396,
                4457,
                12703,
                12839,
                13392,
                12775,
                12767,
                13394,
                13399,
                5036,
                6805,
                6806,
                6807,
                6808,
                12226,
                12679,
                13019,
                13061,
                12779,
                16009,
                6598,
                12249,
                12250,
                12676,
                12727,
                12728,
                13769,
                16010,
                12847,
                12696,
                6599,
                6600,
                15498,
                15499,
                12671,
                5049,
                5054,
                15455,
                15458,
                5050,
                5053,
                4453,
                4454,
                4455,
                5051,
                5052,
                11471,
                12685,
                2432,
                3311,
                3367,
                3368,
                4378,
                4379,
                4391,
                6093,
                6094,
                6095,
                4445,
                4446,
                4449,
                4450,
                4451,
                2357,
                13571,
                6854,
                14825,
                5208,
                2466,
                2467,
                2468,
                6091,
                6092,
                13410,
                13574,
                13412,
                15514,
                12252,
                12253,
                13576,
                3741,
                3742,
                12213,
                12904,
                12931,
                12689,
                2359,
                12699,
                12224,
                12225,
                15421,
                2232,
                8743,
                3304,
                12217,
                13384,
                12257,
                2235,
                2237,
                2238,
                13386,
                3181,
                3183,
                13388,
                4447,
                4448,
            ];

            foreach ($data as $anbTransferId) {

                $anbTransfer = AnbTransfer::query()
                    ->where('id', $anbTransferId)
                    ->select('initiator_id', 'amount', 'transaction_id',
                        'to_bank', 'to_account', 'bank_account_id', 'created_at')
                    ->first();

                $anbData = $anbTransfer ? $anbTransfer->toArray() : null;

                if (!$anbData) {

                    $anbData = $this->getAnbTransferData($anbTransferId);

                    if (!$anbData) {
                        $dataNotFound[] = $anbTransferId;
                        dump($anbTransferId . ' anb-transfer not found -------------------------');
                        continue;
                    }
                }

                $anbData['created_at'] = Carbon::parse($anbData['created_at']);

                try {

                    $anbResponse = AnbApiUtil::newPayment($anbTransferId, 'sequence_number');

                } catch (\Exception $e) {
                    dump($e->getMessage(), $e->getFile(), $e->getLine(), $anbTransferId, '-----------------------------------');
                    continue;
                }

                if ($this->option('transaction') &&
                    $this->option('transaction') == 'add' &&
                    in_array($anbResponse['status'], AnbPaymentStatus::inProcessOrPaidStatuses())
                ) {

                    $this->addTransaction($anbTransfer, $anbData);
                }

                $balance = DB::table('transactions')
                    ->where('payable_id', $anbData['initiator_id'])
                    ->where('payable_type', User::class)
                    ->sum('amount');

                $checkTransaction = DB::table('transactions')
                    ->where('id', $anbData['transaction_id'])
                    ->count();

                $listingInvestor = DB::table('listing_investor')
                    ->where('investor_id', $anbData['initiator_id'])
                    ->sum('invested_amount');

                dump('transaction exist => ' . $checkTransaction);
                dump('bank transfer status => ' . $anbResponse['status']);
                dump('amount => ' . $anbData['amount']);
                dump('user balance => ' . $balance);
                dump('investing amount => ' . $listingInvestor);
                dump('transfer date => ' . $anbData['created_at']);
                dump('---------------------------------------------------------');

                sleep(1);
            }

            dump('array of data not found => ');
            dd($dataNotFound);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine(), $e->getFile());
        }
    }

    public function getAnbTransferData($anbTransferId)
    {

        $lostAnbTransferIds = [
            '2229' => ['iban' => 'SA6780000365608010424082', 'amount' => '9998.85', 'created_at' => '27-07-2022'],
            '2232' => ['iban' => 'SA5005000068200766390000', 'amount' => '49.42', 'created_at' => '27-07-2022'],
            '2235' => ['iban' => 'SA0280000221608010251547', 'amount' => '9.42', 'created_at' => '27-07-2022'],
            '2237' => ['iban' => 'SA0280000221608010251547', 'amount' => '9.42', 'created_at' => '27-07-2022'],
            '2238' => ['iban' => 'SA0280000221608010251547', 'amount' => '9.42', 'created_at' => '27-07-2022'],
            '2357' => ['iban' => 'SA4080000498608010141371', 'amount' => '975.85', 'created_at' => '01-08-2022'],
            '2359' => ['iban' => 'SA7080000448608010766459', 'amount' => '76.42', 'created_at' => '01-08-2022'],
            '2432' => ['iban' => 'SA5005000068202461077000', 'amount' => '998.85', 'created_at' => '01-08-2022'],
            '2466' => ['iban' => 'SA7145000000003679693001', 'amount' => '707.85', 'created_at' => '01-08-2022'],
            '2467' => ['iban' => 'SA7145000000003679693001', 'amount' => '707.85', 'created_at' => '01-08-2022'],
            '2468' => ['iban' => 'SA7145000000003679693001', 'amount' => '698.85', 'created_at' => '01-08-2022'],
            '2484' => ['iban' => 'SA6510000022049377000501', 'amount' => '19998.85', 'created_at' => '01-08-2022'],
            '2485' => ['iban' => 'SA6510000022049377000501', 'amount' => '19998.85', 'created_at' => '01-08-2022'],
            '3183' => ['iban' => 'SA6480000333608010962437', 'amount' => '4.42', 'created_at' => '08-08-2022'],
            '3181' => ['iban' => 'SA6480000333608010962437', 'amount' => '4.42', 'created_at' => '08-08-2022'],
            '3311' => ['iban' => 'SA3380000167608016000357', 'amount' => '998.85', 'created_at' => '09-08-2022'],
            '3367' => ['iban' => 'SA4105000068202605011000', 'amount' => '998.85', 'created_at' => '10-08-2022'],
            '3368' => ['iban' => 'SA4105000068202605011000', 'amount' => '998.85', 'created_at' => '10-08-2022'],
            '3741' => ['iban' => 'SA0980000323608010099075', 'amount' => '199.42', 'created_at' => '19-08-2022'],
            '3742' => ['iban' => 'SA0980000323608010099075', 'amount' => '199.42', 'created_at' => '19-08-2022'],
            '4378' => ['iban' => 'SA6910000019149714000108', 'amount' => '998.85', 'created_at' => '01-09-2022'],
            '4379' => ['iban' => 'SA6910000019149714000108', 'amount' => '998.85', 'created_at' => '01-09-2022'],
            '4391' => ['iban' => 'SA3180000305608010257980', 'amount' => '998.85', 'created_at' => '02-09-2022'],
            '4453' => ['iban' => 'SA3080000146608010374200', 'amount' => '1020.85', 'created_at' => '04-09-2022'],
            '4445' => ['iban' => 'SA5105000068203591301000', 'amount' => '997.85', 'created_at' => '04-09-2022'],
            '4446' => ['iban' => 'SA5105000068203591301000', 'amount' => '997.85', 'created_at' => '04-09-2022'],
            '4447' => ['iban' => 'SA3180000428608010342040', 'amount' => '1.42', 'created_at' => '04-09-2022'],
            '4448' => ['iban' => 'SA3180000428608010342040', 'amount' => '1.42', 'created_at' => '04-09-2022'],
            '4449' => ['iban' => 'SA5105000068203591301000', 'amount' => '995.85', 'created_at' => '04-09-2022'],
            '4450' => ['iban' => 'SA5105000068203591301000', 'amount' => '995.85', 'created_at' => '04-09-2022'],
            '4451' => ['iban' => 'SA5105000068203591301000', 'amount' => '995.85', 'created_at' => '04-09-2022'],
            '4454' => ['iban' => 'SA3080000146608010374200', 'amount' => '1020.85', 'created_at' => '04-09-2022'],
            '4455' => ['iban' => 'SA3080000146608010374200', 'amount' => '1020.85', 'created_at' => '04-09-2022'],
            '4457' => ['iban' => 'SA0280000222608010179135', 'amount' => '5562.44', 'created_at' => '04-09-2022'],
            '5049' => ['iban' => 'SA8280000347608010411132', 'amount' => '1022.85', 'created_at' => '07-09-2022'],
            '5050' => ['iban' => 'SA8280000347608010411132', 'amount' => '1021.85', 'created_at' => '07-09-2022'],
            '5051' => ['iban' => 'SA8280000347608010411132', 'amount' => '1020.85', 'created_at' => '07-09-2022'],
            '5052' => ['iban' => 'SA8280000347608010411132', 'amount' => '1018.85', 'created_at' => '07-09-2022'],
            '5053' => ['iban' => 'SA8280000347608010411132', 'amount' => '1021.85', 'created_at' => '07-09-2022'],
            '5054' => ['iban' => 'SA8280000347608010411132', 'amount' => '1022.85', 'created_at' => '07-09-2022'],
            '5036' => ['iban' => 'SA0980000129608010049217', 'amount' => '3998.85', 'created_at' => '07-09-2022'],
            '5208' => ['iban' => 'SA6980000216608011430790', 'amount' => '767.85', 'created_at' => '11-09-2022'],
            '6091' => ['iban' => 'SA5805000068200397242000', 'amount' => '664.85', 'created_at' => '22-09-2022'],
            '6092' => ['iban' => 'SA5805000068200397242000', 'amount' => '664.85', 'created_at' => '22-09-2022'],
            '6093' => ['iban' => 'SA7280000300608010219274', 'amount' => '998.85', 'created_at' => '22-09-2022'],
            '6094' => ['iban' => 'SA7280000300608010219274', 'amount' => '998.85', 'created_at' => '22-09-2022'],
            '6095' => ['iban' => 'SA7280000300608010219274', 'amount' => '998.85', 'created_at' => '22-09-2022'],
            '6545' => ['iban' => 'SA7980000226608010129243', 'amount' => '40148.25', 'created_at' => '28-09-2022'],
            '6598' => ['iban' => 'SA6965000005555617878001', 'amount' => '2198.85', 'created_at' => '27-09-2022'],
            '6599' => ['iban' => 'SA6965000005555617878001', 'amount' => '1196.85', 'created_at' => '27-09-2022'],
            '6600' => ['iban' => 'SA6965000005555617878001', 'amount' => '1196.85', 'created_at' => '27-09-2022'],
            '6805' => ['iban' => 'SA1220000002560320269940', 'amount' => '3998.85', 'created_at' => '29-09-2022'],
            '6806' => ['iban' => 'SA1220000002560320269940', 'amount' => '3998.85', 'created_at' => '29-09-2022'],
            '6807' => ['iban' => 'SA1220000002560320269940', 'amount' => '3998.85', 'created_at' => '29-09-2022'],
            '6808' => ['iban' => 'SA1220000002560320269940', 'amount' => '3998.85', 'created_at' => '29-09-2022'],
            '6854' => ['iban' => 'SA4180000118608010524394', 'amount' => '798.85', 'created_at' => '30-09-2022'],
            '7493' => ['iban' => 'SA2080000282608010048741', 'amount' => '20000', 'created_at' => '14-10-2022'],
            '7494' => ['iban' => 'SA2080000282608010048741', 'amount' => '19985', 'created_at' => '14-10-2022'],
            '7498' => ['iban' => 'SA5320000006179777449940', 'amount' => '20000', 'created_at' => '14-10-2022'],
            '7497' => ['iban' => 'SA5320000006179777449940', 'amount' => '20000', 'created_at' => '14-10-2022'],
            '7524' => ['iban' => 'SA5320000006179777449940', 'amount' => '20000', 'created_at' => '16-10-2022'],
            '7530' => ['iban' => 'SA4410000006362688000100', 'amount' => '20000', 'created_at' => '16-10-2022'],
            '7531' => ['iban' => 'SA4410000006362688000100', 'amount' => '20000', 'created_at' => '16-10-2022'],
        ];

        if (!isset($lostAnbTransferIds[$anbTransferId])) {
            dump('getAnbTransferData: not in array ' . $anbTransferId);
            return false;
        }

        $lostAnbTransferData = $lostAnbTransferIds[$anbTransferId];

        $bankAccount = BankAccount::query()
            ->where('iban', $lostAnbTransferData['iban'])
            ->first();

        if (!$bankAccount) {
            dump('getAnbTransferData: iban invalid ' . $lostAnbTransferData['iban']);
            return false;
        }

        return [
            'initiator_id' => $bankAccount->accountable_id,
            'amount' => $lostAnbTransferData['amount'] * 100,
            'transaction_id' => null,
            'bank_account_id' => $bankAccount->id,
            'to_bank' => $bankAccount->bank,
            'to_account' => $bankAccount->iban,
            'currency' => defaultCurrency(),
            'data' => [],
            'created_at' => Carbon::parse($lostAnbTransferData['created_at']),
        ];
    }

    public function addTransaction($anbTransfer, $anbData)
    {

        $user = User::find($anbData['initiator_id']);

        if (!$user) {
            dump('addTransaction: user not valid ' . $anbData['initiator_id']);
            return false;
        }

        $wallet = $user->getWallet(WalletType::Investor);

        if (!$wallet) {
            dump('addTransaction: wallet not valid ' . $anbData['initiator_id']);
            return false;
        }

        if (!$anbTransfer) {
            $anbTransfer = AnbTransfer::create($anbData);
            dump('new anb transfer created ' . $anbTransfer->id);
        }

        $meta =
            [
                'reference' => transaction_ref(),
                'reason' => TransactionReason::AnbExternalTransfer,
                'value_date' => $anbData['created_at']->format('Y-m-d'),
                'iban' => $anbData['to_account'],
                'bank_code' => $anbData['to_bank'],
                'source' => 'lost_transfer_cmd',
                'anb_transfer_id' => $anbTransfer->id,
            ];

        $transactionId = DB::table('transactions')->insertGetId([
            'payable_type' => User::class,
            'payable_id' => $user->id,
            'wallet_id' => $wallet->id,
            'type' => 'withdraw',
            'amount' => ($anbData['amount'] * -1),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => $anbData['created_at']->format('Y-m-d'), // now(),
            'updated_at' => $anbData['created_at']->format('Y-m-d'), // now(),
        ]);

        $user_amount = DB::table('transactions')
            ->where('payable_id', $user->id)
            ->where('payable_type', User::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('holder_id', $user->id)
            ->where('holder_type', User::class)
            ->update(['balance' => $user_amount]);

        $anbTransfer->transaction_id = $transactionId;
        $anbTransfer->save();

        dump('new transaction created ' . $transactionId);
    }
}
