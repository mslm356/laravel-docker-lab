{{--@extends('layouts.pdf')--}}

{{--@section('content')--}}
{{--    <div dir="{{ App::getLocale() === 'en' ? 'ltr' : 'rtl' }}">--}}
{{--        <div class="flex items-center justify-between mb-6">--}}
{{--            <div>--}}
{{--                <img class="w-auto h-20 mx-auto" src="{{ asset('images/aseel-blue.png') }}" />--}}
{{--            </div>--}}
{{--            <div>--}}
{{--                <h2 class="text-2xl font-bold">{{ __('zatca/e-invoice.tax_invoice') }}</h2>--}}
{{--                <p class="text-lg">{{ __('zatca/e-invoice.invoice_number', ['number' => $order->getInvoice()->id]) }}</p>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="flex my-10 space-x-8 rtl:space-x-reverse">--}}
{{--            <div class="w-3/12">--}}
{{--                <div class="mb-1">--}}
{{--                    <h2 class="text-sm text-gray-600">{{ __('zatca/e-invoice.bill_from') }}</h2>--}}
{{--                </div>--}}
{{--                <h2 class="mb-1 font-semibold">{{ $seller->getName() }}</h2>--}}
{{--                <ul class="text-xs text-gray-500">--}}
{{--                    <li>{{ $seller->getAddress()->getLine1() }}</li>--}}
{{--                    <li>{{ $seller->getAddress()->getLine2() }}</li>--}}
{{--                    <li>{{ __('zatca/e-invoice.vat_number') }}: {{ $seller->getVatId() }}</li>--}}
{{--                    <li>{{ __('zatca/e-invoice.cr_number') }}: {{ $seller->getCrNumber() }}</li>--}}
{{--                </ul>--}}
{{--            </div>--}}

{{--            <div class="w-3/12">--}}
{{--                <div class="mb-1">--}}
{{--                    <h2 class="text-sm text-gray-600">{{ __('zatca/e-invoice.bill_to') }}</h2>--}}
{{--                </div>--}}
{{--                <h2 class="mb-1 font-semibold">{{ $buyer->getAbsherName() }}</h2>--}}
{{--                <ul class="text-xs text-gray-500">--}}
{{--                    <li>{{ __('zatca/e-invoice.contact_number') }}: {{ $buyer->phone_without_country }}</li>--}}
{{--                </ul>--}}
{{--            </div>--}}

{{--            <div class="flex-1">--}}
{{--                <div class="mb-1">--}}
{{--                    <h2 class="text-sm text-gray-600">{{ __('zatca/e-invoice.order_details') }}</h2>--}}
{{--                </div>--}}
{{--                <div class="grid grid-cols-12 gap-2 text-xs text-gray-500">--}}
{{--                    <div class="col-span-3">--}}
{{--                        {{ __('zatca/e-invoice.transaction_reference') }}--}}
{{--                    </div>--}}
{{--                    <div class="col-span-9">--}}
{{--                        {{ $order->getReference() }}--}}
{{--                    </div>--}}

{{--                    <div class="col-span-3">--}}
{{--                        {{ __('zatca/e-invoice.issue_date') }}--}}
{{--                    </div>--}}
{{--                    <div class="col-span-9">--}}
{{--                        {{ $order->getInvoiceDate() }}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="flex px-2 py-3 text-white bg-primary-700">--}}
{{--            <div class="w-3/12">--}}
{{--                <div>{{ __('zatca/e-invoice.item') }}</div>--}}
{{--            </div>--}}

{{--            <div class="w-1/12">--}}
{{--                <div>{{ __('zatca/e-invoice.qty') }}</div>--}}
{{--            </div>--}}

{{--            <div class="w-2/12">--}}
{{--                <div>{{ __('zatca/e-invoice.unit_price') }}</div>--}}
{{--            </div>--}}

{{--            <div class="w-2/12">--}}
{{--                <div>{{ __('zatca/e-invoice.discount') }}</div>--}}
{{--            </div>--}}

{{--            <div class="w-2/12">--}}
{{--                <div>{{ __('zatca/e-invoice.vat_symbol') }}</div>--}}
{{--            </div>--}}

{{--            <div class="w-2/12">--}}
{{--                <div>{{ __('zatca/e-invoice.total_price') }}</div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        @foreach ($order->getItems() as $item)--}}
{{--            <div class="flex px-2 py-3 text-gray-600">--}}
{{--                <div class="w-3/12">--}}
{{--                    <div>{{ $item->getName() }}</div>--}}
{{--                </div>--}}

{{--                <div class="w-1/12">--}}
{{--                    <div>{{ $item->getQty() }}</div>--}}
{{--                </div>--}}

{{--                <div class="w-2/12">--}}
{{--                    <div>{{ $item->getItemPrice()->formatByDecimal() }}</div>--}}
{{--                </div>--}}

{{--                <div class="w-2/12">--}}
{{--                    <div>{{ $item->getDiscountPercentage() ?? 0 }}%</div>--}}
{{--                </div>--}}

{{--                <div class="w-2/12">--}}
{{--                    <div>{{ $item->getVatPercentage() === null ? 'N' : 'V' }}</div>--}}
{{--                </div>--}}

{{--                <div class="w-2/12">--}}
{{--                    <div>{{ $item->getLineTotalWithoutVat()->formatByDecimal() }}</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}
{{--        <div class="w-full h-px bg-primary-700"></div>--}}

{{--        <div class="flex items-center justify-between">--}}
{{--            <div class="flex">--}}
{{--                <img class="w-40 h-40" src="{{ $qr_code }}" />--}}
{{--                <div class="px-4">--}}
{{--                    <h2 class="text-xl font-bold">{{ __('zatca/e-invoice.tax_rates') }}</h2>--}}
{{--                    <ul class="mt-2">--}}
{{--                        <li>--}}
{{--                            "V"--}}
{{--                            {{ __('zatca/e-invoice.vat_symbol_v', [--}}
{{--                                'percentage' => clean_decimal(--}}
{{--                                    collect($order->getItems())->filter(fn($item) => $item->getVatPercentage() !== null)->first()->getVatPercentage(),--}}
{{--                                ),--}}
{{--                            ]) }}--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            "N" {{ __('zatca/e-invoice.vat_symbol_n') }}--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="w-1/2">--}}
{{--                <div class="grid grid-cols-2 gap-4 py-3">--}}
{{--                    <div>--}}
{{--                        {{ __('zatca/e-invoice.subtotal') }}--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        {{ __('zatca/e-invoice.amount_with_currency', [--}}
{{--                            'amount' => $order->getSubtotal()->formatByDecimal(),--}}
{{--                        ]) }}--}}
{{--                    </div>--}}

{{--                    @if ($order->getTotalDiscount()->getAmount() > 0)--}}
{{--                        <div>--}}
{{--                            {{ __('zatca/e-invoice.total_discount') }}--}}
{{--                        </div>--}}
{{--                        <div>--}}
{{--                            {{ __('zatca/e-invoice.amount_with_currency', [--}}
{{--                                'amount' => $order->getTotalDiscount()->formatByDecimal(),--}}
{{--                            ]) }}--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    <div>--}}
{{--                        {{ __('zatca/e-invoice.total_before_vat') }}--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        {{ __('zatca/e-invoice.amount_with_currency', [--}}
{{--                            'amount' => $order->getTotalWithoutVat()->formatByDecimal(),--}}
{{--                        ]) }}--}}
{{--                    </div>--}}

{{--                    <div>--}}
{{--                        {{ __('zatca/e-invoice.vat_total') }}--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        {{ __('zatca/e-invoice.amount_with_currency', [--}}
{{--                            'amount' => $order->getTotalVat()->formatByDecimal(),--}}
{{--                        ]) }}--}}
{{--                    </div>--}}

{{--                    <div>--}}
{{--                        {{ __('zatca/e-invoice.total') }}--}}
{{--                    </div>--}}
{{--                    <div class="font-bold text-gray-800">--}}
{{--                        {{ __('zatca/e-invoice.amount_with_currency', [--}}
{{--                            'amount' => $order->getTotalAmount()->formatByDecimal(),--}}
{{--                        ]) }}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="w-full h-px bg-primary-800"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}


    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Styles -->
{{--    <link href="{{ asset('css/website.css') }}" rel="stylesheet">--}}
    <style>
        @font-face {
            font-family: 'HelveticaNeueLT';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/HelveticaNeueLTArabic-Roman.woff2'))) }}") format('woff2');
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'HelveticaNeueLT';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/HelveticaNeueLTArabic-Bold.woff2'))) }}") format('woff2');
            font-weight: bold;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Alata';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/Alata-Regular.woff2'))) }}") format('woff2');
            font-weight: bold;
            font-style: normal;
            font-display: swap;
        }

        body {
            font-family: 'HelveticaNeueLT';
        }

        @tailwind base;

        @tailwind components;

        @tailwind utilities;

        @layer base {
            @font-face {
                font-family: 'HelveticaNeueLT';
                src: url('/fonts/HelveticaNeueLTArabic-Roman.woff2') format('woff2'),
                url('/fonts/HelveticaNeueLTArabic-Roman.woff') format('woff');
                font-weight: normal;
                font-style: normal;
                font-display: swap;
            }

            @font-face {
                font-family: 'HelveticaNeueLT';
                src: url('/fonts/HelveticaNeueLTArabic-Bold.woff2') format('woff2'),
                url('/fonts/HelveticaNeueLTArabic-Bold.woff') format('woff');
                font-weight: bold;
                font-style: normal;
                font-display: swap;
            }

            @font-face {
                font-family: 'Alata';
                src: url('/fonts/Alata-Regular.woff2') format('woff2'),
                url('/fonts/Alata-Regular.woff') format('woff');
                font-weight: normal;
                font-style: normal;
                font-display: swap;
            }

            [dir='rtl'] body {
                font-family: 'HelveticaNeueLT';
            }

            [dir='ltr'] body {
                font-family: 'Alata';
            }

            .bv-hidden {
                -webkit-backface-visibility: hidden;
                backface-visibility: hidden;
            }
        }

        @layer utilities {
            @variants group-hover {
                .rotate-y-180 {
                    transform: rotateY(180deg);
                }
            }

            @variants direction {
                .\-scale-x-100 {
                    --transform-scale-x: -1;
                }
            }
        }

        @layer components {
            .text-huge {
                @apply text-2xl font-bold;
            }
            @media screen(md) {
                .text-huge {
                    @apply text-4xl;
                }
            }

            .text-big {
                @apply text-lg font-bold;
            }
            @media screen(md) {
                .text-big {
                    @apply text-2xl;
                }
            }
        }

        p[style*="text-align:center"] > img {
            margin-right: auto;
            margin-left: auto;
        }

    </style>

    <script src="https://cdn.tailwindcss.com"></script>

</head>

<body>

<div dir="{{ App::getLocale() === 'en' ? 'ltr' : 'rtl' }}">
    <div class="flex items-center justify-between mb-6">
        <div>
            <img class="w-auto h-20 mx-auto" src="{{ asset('images/aseel-blue.png') }}"/>
        </div>
        <div>
            <h2 class="text-2xl font-bold" style="">{{ __('zatca/e-invoice.tax_invoice') }}</h2>
            <p class="text-lg">{{ __('zatca/e-invoice.invoice_number', ['number' => $order->getInvoice()->id]) }}</p>
        </div>
    </div>

    <div class="flex my-10 space-x-8 rtl:space-x-reverse">
        <div class="w-3/12">
            <div class="mb-1">
                <h2 class="text-sm text-gray-600">{{ __('zatca/e-invoice.bill_from') }}</h2>
            </div>
            <h2 class="mb-1 font-semibold">{{ $seller->getName() }}</h2>
            <ul class="text-xs text-gray-500">
                <li>{{ $seller->getAddress()->getLine1() }}</li>
                <li>{{ $seller->getAddress()->getLine2() }}</li>
                <li>{{ __('zatca/e-invoice.vat_number') }}: {{ $seller->getVatId() }}</li>
                <li>{{ __('zatca/e-invoice.cr_number') }}: {{ $seller->getCrNumber() }}</li>
            </ul>
        </div>

        <div class="w-3/12">
            <div class="mb-1">
                <h2 class="text-sm text-gray-600">{{ __('zatca/e-invoice.bill_to') }}</h2>
            </div>
            <h2 class="mb-1 font-semibold">{{ $buyer->getAbsherName() }}</h2>
            <ul class="text-xs text-gray-500">
                <li>{{ __('zatca/e-invoice.contact_number') }}: {{ $buyer->phone_without_country }}</li>
            </ul>
        </div>

        <div class="flex-1">
            <div class="mb-1">
                <h2 class="text-sm text-gray-600">{{ __('zatca/e-invoice.order_details') }}</h2>
            </div>
            <div class="grid grid-cols-12 gap-2 text-xs text-gray-500">
                <div class="col-span-3">
                    {{ __('zatca/e-invoice.transaction_reference') }}
                </div>
                <div class="col-span-9">
                    {{ $order->getReference() }}
                </div>

                <div class="col-span-3">
                    {{ __('zatca/e-invoice.issue_date') }}
                </div>
                <div class="col-span-9">
                    {{ $order->getInvoiceDate() }}
                </div>
            </div>
        </div>
    </div>

    <div class="flex px-2 py-3 text-white bg-primary-700">
        <div class="w-3/12">
            <div>{{ __('zatca/e-invoice.item') }}</div>
        </div>

        <div class="w-1/12">
            <div>{{ __('zatca/e-invoice.qty') }}</div>
        </div>

        <div class="w-2/12">
            <div>{{ __('zatca/e-invoice.unit_price') }}</div>
        </div>

        <div class="w-2/12">
            <div>{{ __('zatca/e-invoice.discount') }}</div>
        </div>

        <div class="w-2/12">
            <div>{{ __('zatca/e-invoice.vat_symbol') }}</div>
        </div>

        <div class="w-2/12">
            <div>{{ __('zatca/e-invoice.total_price') }}</div>
        </div>
    </div>

    @foreach ($order->getItems() as $item)
        <div class="flex px-2 py-3 text-gray-600">
            <div class="w-3/12">
                <div>{{ $item->getName() }}</div>
            </div>

            <div class="w-1/12">
                <div>{{ $item->getQty() }}</div>
            </div>

            <div class="w-2/12">
                <div>{{ $item->getItemPrice()->formatByDecimal() }}</div>
            </div>

            <div class="w-2/12">
                <div>{{ $item->getDiscountPercentage() ?? 0 }}%</div>
            </div>

            <div class="w-2/12">
                <div>{{ $item->getVatPercentage() === null ? 'N' : 'V' }}</div>
            </div>

            <div class="w-2/12">
                <div>{{ $item->getLineTotalWithoutVat()->formatByDecimal() }}</div>
            </div>
        </div>
    @endforeach
    <div class="w-full h-px bg-primary-700"></div>

    <div class="flex items-center justify-between">
        <div class="flex">
            <img class="w-40 h-40" src="{{ $qr_code }}"/>
            <div class="px-4">
                <h2 class="text-xl font-bold">{{ __('zatca/e-invoice.tax_rates') }}</h2>
                <ul class="mt-2">
                    <li>
                        "V"
                        {{ __('zatca/e-invoice.vat_symbol_v', [
                            'percentage' => clean_decimal(
                                collect($order->getItems())->filter(fn($item) => $item->getVatPercentage() !== null)->first()->getVatPercentage(),
                            ),
                        ]) }}
                    </li>
                    <li>
                        "N" {{ __('zatca/e-invoice.vat_symbol_n') }}
                    </li>
                </ul>
            </div>
        </div>
        <div class="w-1/2">
            <div class="grid grid-cols-2 gap-4 py-3">
                <div>
                    {{ __('zatca/e-invoice.subtotal') }}
                </div>
                <div>
                    {{ __('zatca/e-invoice.amount_with_currency', [
                        'amount' => $order->getSubtotal()->formatByDecimal(),
                    ]) }}
                </div>

                @if ($order->getTotalDiscount()->getAmount() > 0)
                    <div>
                        {{ __('zatca/e-invoice.total_discount') }}
                    </div>
                    <div>
                        {{ __('zatca/e-invoice.amount_with_currency', [
                            'amount' => $order->getTotalDiscount()->formatByDecimal(),
                        ]) }}
                    </div>
                @endif

                <div>
                    {{ __('zatca/e-invoice.total_before_vat') }}
                </div>
                <div>
                    {{ __('zatca/e-invoice.amount_with_currency', [
                        'amount' => $order->getTotalWithoutVat()->formatByDecimal(),
                    ]) }}
                </div>

                <div>
                    {{ __('zatca/e-invoice.vat_total') }}
                </div>
                <div>
                    {{ __('zatca/e-invoice.amount_with_currency', [
                        'amount' => $order->getTotalVat()->formatByDecimal(),
                    ]) }}
                </div>

                <div>
                    {{ __('zatca/e-invoice.total') }}
                </div>
                <div class="font-bold text-gray-800">
                    {{ __('zatca/e-invoice.amount_with_currency', [
                        'amount' => $order->getTotalAmount()->formatByDecimal(),
                    ]) }}
                </div>
            </div>
            <div class="w-full h-px bg-primary-800"></div>
        </div>
    </div>
</div>
</body>

</html>

