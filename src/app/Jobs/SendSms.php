<?php

namespace App\Jobs;

use App\Services\CacheServices;
use App\Services\LogService;
use App\Traits\SendMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, SendMessage;

    public $number;
    public $message;

    public $tries = 2;
    public $timeout = 800;
    public $logService = 800;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($number, $message)
    {
        $this->onQueue('sms');
        $this->number = $number;
        $this->message = $message;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->sendOtpMessage($this->number, $this->message);
        } catch (\Exception $e) {
            $this->logService->log('JOB-SEND-OTP-SMS-EXC', $e);
        }

    }
}
