<?php

namespace App\Http\Controllers\Investors\Auth;

use App\Http\Controllers\Controller;
use App\Services\Investors\Users\LastForgetService;
use App\Support\Elm\AbsherOtp\forgetEmailService;
use Illuminate\Http\Request;

class forgetEmail extends Controller
{
    public $nin;
    public $lastForgetService;

    public function __construct()
    {
        $this->nin = new forgetEmailService();
        $this->lastForgetService = new LastForgetService();
    }

    public function forgetEmail(Request $request)
    {
        $data = $this->validate($request, $this->nin->getNinRules());

        $responseOfLastForget = $this->lastForgetService->handle($data['nin'], 'email');

        if (isset($responseOfLastForget['status']) && !$responseOfLastForget['status']) {
            $msg = isset($responseOfLastForget['msg']) ? $responseOfLastForget['msg'] : 'please try later';
            return apiResponse(400, __($msg));
        }

        $msg = $this->nin->sendOtp($data);

        if (!$msg) {
            return response()->json(['message' => 'data not valid'], 400);
        }

        return response()->json(['data' => ['vid' => $msg->id]], 200);
    }

    public function resetEmail(Request $request)
    {
        $data = $this->validate($request, $this->nin->getRules());

        $this->nin->verify($data);

        return response()->json(['data' => ['message' => 'code verifed  and email updated successfully']], 200);
    }
}
