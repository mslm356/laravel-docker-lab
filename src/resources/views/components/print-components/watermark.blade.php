
    body {
        min-height: 100vh;
        position: relative;
        margin: 0;
        background-image: url('data:image/svg+xml;utf8,<svg style="transform:rotate(-25deg)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 550 50"><text x="0" y="20" font-size="18px" style="opacity:.3" fill="%23000" font-family="HelveticaNeueLT">{{$user_name}} - {{$date}} - {{$website}}</text></svg>');
        background-repeat: repeat;
        background-size: 80% 33vh;
        background-position: 0 0;

    }

