<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'NinNotValid' => 'national identity not valid',
    'ProfileNotValid' => 'profile not valid',
    'IdentityVerified' => 'account not verified',
    'ApprovedStatus' => 'account not approved',
    'KycCompleted' => 'kyc not completed',
    'SuitabilityStatus'=>'Based on the disclosures submitted by you on (Know Your Customer), you are not suitable for the investment opportunity. If you refuse the results and wish to invest in the fund despite the evaluation submitted, please continue and submit your request to us.',
    'AmericanNationalty' => "Due to FATCA regulations, We apologize for opening investment accounts for US nationality.",

];
