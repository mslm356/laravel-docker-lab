<?php

use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\Rate\RatesController;




Route::middleware(['auth:api', 'jwt_check:api', 'role:' .Role::Investor])->group(function () {

    Route::post('new-rate',[ RatesController::class , 'store']);

});
