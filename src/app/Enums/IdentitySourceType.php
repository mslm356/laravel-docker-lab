<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class IdentitySourceType extends Enum
{
    const Absher = 1;
}
