<?php


namespace App\Support\Zoho;


use App\Enums\Zoho\AccountTypes;
use App\Models\ZohoPayment;
use App\Models\ZohoSettings;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CustomerPayments
{

    public $base_url;
    public $organization_id;
    public $auth;
    public $retainerInvoice;

    public function __construct()
    {
        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
        $this->retainerInvoice = new RetainerInvoice();
    }

    public function preparePaymentData($user, $amount, $payment_mode)
    {

        $accountSetting = ZohoSettings::where('key', AccountTypes::ANBBank)->first();
        $account_id = $accountSetting ? $accountSetting->value : null;

        $investorAccountSetting = ZohoSettings::where('key', AccountTypes::InvestorWallet)->first();
        $investor_account_id = $investorAccountSetting ? $investorAccountSetting->value : null;

        $data = [
            'customer_id' => $user->zoho_id,
            'payment_mode' => $payment_mode,
            'amount' => $amount,
            'date' => Carbon::now()->format('Y-m-d'),
            'account_id' => $account_id,
            'amount_applied' => $amount,
            'customer_advance_account_id' => $investor_account_id
        ];

        return $data;
    }

    public function store($user, $amount, $payment_mode)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->preparePaymentData($user, $amount, $payment_mode);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'customerpayments?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                return $responseData['payment']['payment_id'];
            }

            Log::error('ZOHO-CUSTOMER-PAYMENT', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-CUSTOMER-PAYMENT', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return null;
    }

    public function createCustomerPayment($user, $amount, $payment_mode = 'cash')
    {

        try {

            $payment_id = $this->store($user, $amount, $payment_mode);

            if ($payment_id) {

                ZohoPayment::create([
                    'user_id' => $user->id,
                    'payment_id' => $payment_id,
                    'amount' => $amount,
                ]);
            }

            return $payment_id;

        } catch (\Exception $e) {
            Log::error('ZOHO-CUSTOMER-PAYMENT-PAID', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }
    }
}
