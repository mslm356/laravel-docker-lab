<?php

namespace App\Jobs\Listings;

use App\Services\CacheServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FlushCache implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $listingId;
    public $cacheServices;

    public $tries = 2;
    public $timeout = 800;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listingId)
    {

        $this->onQueue('listing-cache');
        $this->listingId = $listingId;
        $this->cacheServices = new CacheServices();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->cacheServices->updateFundCache($this->listingId);
    }
}
