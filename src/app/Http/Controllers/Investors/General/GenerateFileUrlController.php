<?php

namespace App\Http\Controllers\Investors\General;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Listings\ListingInvestorRecord;
use App\Services\Files\GenerateFileUrl;
use App\Services\LogService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class GenerateFileUrlController extends Controller
{

    public $generateFileUrl;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->generateFileUrl = new GenerateFileUrl();
    }

    public function __invoke(Request $request, File $file)
    {
        try {

            $user = getAuthUser('web');
            return $this->generateFileUrl->getUrl($file, $user);

        } catch (AuthorizationException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }
    }


    public function getInvestorFileUrl($investor_record_id)
    {
        $investor_record_id=Crypt::decryptString($investor_record_id);

        $file=File::where('fileable_type', ListingInvestorRecord::class)->where('fileable_id', $investor_record_id)->first();

        if ($file) {
            return apiResponse(200, 'done', ['file_url' => $file->temp_full_path]);
        }

        $file=File::find(config('app.empty_file'));
        return apiResponse(200, 'done', ['file_url' => $file->temp_full_path]);
    }

}
