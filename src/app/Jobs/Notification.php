<?php

namespace App\Jobs;

use App\Services\LogService;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class Notification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels , NotificationServices;

    public $receiver_id;
    public $data;
    public $type;
    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $data, $receiver_id)
    {
        $this->type = $type;
        $this->data = $data;
        $this->receiver_id = $receiver_id;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     *
     */
    public function handle()
    {
        try {
            $validationResponse = $this->validatedData($this->data, $this->receiver_id);

            if (!$validationResponse) {
                return false;
            }

            $this->sendToMobile($this->type, $this->data, $this->receiver_id);

        } catch (\Exception $e) {
            $this->logService->log('JOB-NOTIFICATION-EXCEPTION', $e);
        }
    }

}
