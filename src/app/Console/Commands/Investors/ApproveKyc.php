<?php

namespace App\Console\Commands\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;

class ApproveKyc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approve:kyc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        User::role(Role::Investor)
            ->whereHas('investor', function ($investor) {
                $investor->where('status', InvestorStatus::PendingReview)
                    ->where('is_kyc_completed', true);
            })
            ->lazyById()
            ->each(function ($user) {
                InvestorProfile::where('user_id', $user->id)
                    ->update([
                        'status' => InvestorStatus::Approved,
                        'review_type' => ProfileReviewType::BatchApprove,
                    ]);
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new ApproveProfile());
                }
            });

        return true;
    }
}
