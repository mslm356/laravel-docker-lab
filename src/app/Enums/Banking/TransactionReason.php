<?php

namespace App\Enums\Banking;

use BenSampo\Enum\Enum;

final class TransactionReason extends Enum
{
    const AnbStatement = 1;   // deposit => payment received
    const InvestingInListing = 2;  // manual journal
    const PayoutDistribution = 3;  // ---
    const AnbExternalTransfer = 4; // manual journal
    const RefundAfterAnbTransferFailure = 5;  // deposit => payment received
    const InvestingInListingAdminFee = 6;     // invoice
    const InvestingInListingVatOfAdminFee = 7;  // invoice
    const UrwayReason = 8;   // deposit => payment received
    const SdadReason = 9;   // invoice
    const Compensation = 10;   // compensation
    const CommonReason = 11;   // common reason
    const HyperPayReason = 12;

}
