<?php

namespace App\Http\Controllers\Investors\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\User;
use Illuminate\Validation\Rule;

class UpdateInfo extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $this->validate($request, [
            'email' => [
                'required', 'string', 'max:200', 'email',
                Rule::unique(User::class, 'email')
                    ->ignore($request->user())
            ],
            'first_name' => ['required', 'string', 'max:100',textValidation()],
            'last_name' => ['required', 'string', 'max:100',textValidation()],
        ]);

        $request->user()->update([
            'email' => $data['email'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name']
        ]);

        return response()->json();
    }
}
