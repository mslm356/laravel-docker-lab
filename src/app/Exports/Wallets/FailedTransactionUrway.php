<?php

namespace App\Exports\Wallets;

use App\Models\Users\InvestorSubscriptionToken;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\DescriptionManager;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class FailedTransactionUrway implements FromCollection, WithHeadings, WithMapping
{

    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return InvestorSubscriptionToken::query()

            ->whereNotNull('authorization_expiry')
            ->whereNull('expired_at')
            ->where('payment_type', 'urway')
            ->where('listing_id', 24)

//            ->join('users', function ($join) {
//                $join->on('users.id', '=', 'investor_subscription_tokens.user_id');
//            })
//
//            ->join('urway_transactions', function ($join) {
//                $join->on('urway_transactions.subscription_id', '=', 'investor_subscription_tokens.id');
//            })

            ->get();
    }

    public function headings(): array
    {
        return [
//            '#',
            'id',
            'Amount',
            'reference',
        ];
    }

    public function map($token): array
    {
        return [
            $token->id,
            $token->amount,
            $token->reference,
        ];
    }
}
