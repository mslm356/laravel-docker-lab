<?php

namespace App\Support\Users\UserAccess\Modules;

use App\Enums\Banking\WalletType;
use App\Models\VirtualBanking\VirtualAccount;

class VirtualAccountModule
{
    public function handle($user, $module, $userDefaultData)
    {
        return $this->getAccountsData($user);
    }

    public function getAccountsData($user)
    {
        $wallet = $user->getWallet(WalletType::Investor);

        if ($wallet) {
            $anbVirtualAccount = VirtualAccount::where('wallet_id', $wallet->id)->first();
            return ['iban' => ($anbVirtualAccount)?$anbVirtualAccount->identifier:null, 'user_wallet_id' => $wallet->id];
        }

        return [
            'iban' => null,
            'user_wallet_id' => null,
        ];
    }
}
