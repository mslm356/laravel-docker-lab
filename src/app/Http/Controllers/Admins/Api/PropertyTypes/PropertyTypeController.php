<?php

namespace App\Http\Controllers\Admins\Api\PropertyTypes;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\PropertyTypeResource;
use App\Models\PropertyType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PropertyTypeController extends Controller
{
    /**
     * Get all the property types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return PropertyTypeResource::collection(PropertyType::latest()->get());
    }

    /**
     * Show the stored property
     *
     * @param \App\Models\PropertyType $propertyType
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(PropertyType $propertyType)
    {
        return new PropertyTypeResource($propertyType);
    }

    /**
     * Store the type in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $data = $this->validator($request->all())->validate();

            $propertyType = PropertyType::create($data);

            return new PropertyTypeResource($propertyType);
        });
    }

    /**
     * Update the given type in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PropertyType $propertyType
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, PropertyType $propertyType)
    {
        return DB::transaction(function () use ($request, $propertyType) {
            $data = $this->validator($request->all())->validate();

            $propertyType->update($data);

            return new PropertyTypeResource($propertyType);
        });
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, [
            'name_en' => ['required', 'string', 'max:50',textValidation()],
            'name_ar' => ['required', 'string', 'max:50',textValidation()],
            'is_active' => ['required', 'boolean']
        ]);
    }
}
