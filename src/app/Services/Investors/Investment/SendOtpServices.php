<?php


namespace App\Services\Investors\Investment;

use App\Models\Users\InvestorSubscriptionToken;
use App\Services\Investors\AbsherOtpService;
use App\Services\LogService;
use App\Services\MemberShip\SpecialCanInvest;
use Illuminate\Support\Facades\Validator;

class SendOtpServices
{

    public $logService;
    public $handleResponseService;
    public $absherOtpService;
    public $specialCanInvest;

    public function __construct()
    {
        $this->handleResponseService = new HandleResponseService();
        $this->logService = new LogService();
        $this->absherOtpService = new AbsherOtpService();
        $this->specialCanInvest = new SpecialCanInvest();
    }

    public function handle($request, $user)
    {

        $response = ['status' => true];

        $data = $this->dataValidation($request);

        $token = InvestorSubscriptionToken::query()
            ->where('is_expire', false)
            ->where('reference', $data['token'])
            ->where('user_id', $user->id)
            ->first();

        if (!$token) {
            return $this->handleResponseService->responseValues($response, false, 'sorry, token not valid', []);
        }

        if (!$this->specialCanInvest->handle($token->listing, $user)) {
            return $this->handleResponseService->responseValues($response, false, 'you not have access', []);
        }
        //
        if (!$user->nationalIdentity) {
            return $this->handleResponseService->responseValues($response, false, 'mobile_app/message.profile_not_verified', []);
        }

        $absherOtp = $this->absherOtpService->createOtp($user, $token, $token->listing->sms_delay, $user->nationalIdentity->nin);

        $responseData = ['vid' => $absherOtp->id];

        return $this->handleResponseService->responseValues($response, true, 'mobile_app/message.otp_sent_successfully', $responseData);
    }

    public function dataValidation($request)
    {

        $rules = [
            'token' => 'required|string',
            'delay' => 'nullable|integer|min:0',
        ];

        $data = Validator::make($request->all(), $rules)->validate();

        return $data;
    }

}
