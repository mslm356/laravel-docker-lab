<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmlFocalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aml_focal_histories', function (Blueprint $table) {
            $table->id();
//            $table->string('webhook_uuid')->nullable();
//            $table->string('decision_uuid')->nullable();
            $table->string('query_id')->nullable();
            $table->string('customer_reference_id')->nullable();
            $table->string('decision')->nullable();
            $table->json('meta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aml_focal_histories');
    }
}
