<?php

namespace App\Support\QueryScoper\Scopes\Admin\Anb\EodTransactions;

use App\Support\QueryScoper\QueryScoper;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class FilterByDateRangeScope extends QueryScoper
{
    public function prepareBuilder($builder, $data)
    {
        $start = $data['start'] ? $this->toUTCDateString($data['start']) : null;
        $end = $data['end'] ? $this->toUTCDateString($data['end']) : null;

        if ($start && $end) {
            $builder->whereBetween('value_date', [$start, $end]);
        } elseif ($start) {
            $builder->where('value_date', '>=', $start);
        } elseif ($end) {
            $builder->where('value_date', '<=', $end);
        }

        return $builder;
    }

    public function prepareData()
    {
        $segments = explode(',', Request::query('date_range', ','));

        return [
            'start' => $segments[0] ?? null,
            'end' => $segments[1] ?? null
        ];
    }

    public function toUTCDateString($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date, 'Asia/Riyadh')->timezone('UTC')->toDateString();
    }

    public function validator($data)
    {
        return Validator::make($data, [
            'start' => ['nullable', 'date'],
            'end' => array_merge(
                [
                    'nullable', 'date'
                ],
                $data['start'] ? ['after_or_equal:start'] : []
            ),
        ]);
    }
}
