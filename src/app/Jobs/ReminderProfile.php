<?php

namespace App\Jobs;

use App\Enums\Notifications\NotificationTypes;
use App\Mail\Investors\ReminderForProfile;
use App\Traits\NotificationServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ReminderProfile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels , NotificationServices;

    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;

        if ($user->investor && !$user->investor->is_kyc_completed) {
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($user->email)->send(new ReminderForProfile($user));
            }
            $this->send(NotificationTypes::ReminderProfile, $this->notificationData($user), $user->id);

        }
    }

    public function notificationData($user)
    {
        return [
            'action_id'=> $user->id,
            'channel'=>'mobile',
            'title_en'=>'Complete your profile',
            'title_ar'=>'اكمل ملفك الشخصى',
            'content_en'=>'One step away from entering the investment world',
            'content_ar'=>'خطوة واحدة تفصلك عن الدخول إلى عالم الاستثمار',
        ];
    }
}
