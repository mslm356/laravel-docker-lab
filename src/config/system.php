<?php

return [
    'listings' => [
        'min_share_price' => env('LISTING_MIN_SHARE_PRICE', 1000)
    ],
    'browsershot' => [
        'include_path' => env('BROWSERSHOT_INCLUDE_PATH')
    ]
];
