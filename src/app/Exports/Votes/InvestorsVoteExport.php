<?php

namespace App\Exports\Votes;

use App\Enums\Role;
use App\Models\InvestorVote;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class InvestorsVoteExport  implements FromCollection, WithHeadings, WithMapping
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        $data = $this->data;

        return InvestorVote::query()
            ->where('vote_id', $data['vote_id'])
            ->join('users', function ($join) {
                $join->on('users.id', '=', 'investor_votes.investor_id');
            })
            ->join('listing_investor', function ($join) use($data) {
                $join->on('listing_investor.investor_id', '=', 'investor_votes.investor_id')
                    ->where('listing_investor.invested_shares','>',0)
                    ->where('listing_investor.listing_id', $data['fund_id']);
            })->select('investor_votes.id as investor_vote_id',
                DB::raw('CONCAT(users.first_name, " ",  users.last_name) as full_name'),
                'listing_investor.invested_shares', 'investor_votes.created_at', 'investor_votes.answer')
            ->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'User',
            'Answer',
            'Invested Shares',
            'Created_at',
        ];
    }

    public function map($investorVote): array
    {
        return [
            $investorVote->investor_vote_id,
            $investorVote->full_name,
            $investorVote->vote_answer,
            $investorVote->invested_shares,
            $investorVote->created_at ? Carbon::parse($investorVote->created_at)->timezone('Asia/Riyadh')->format('y-m-d h:i:s') : null,
        ];
    }
}
