<!DOCTYPE html>
<html lang="{{ App::getLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}

    <!-- Styles -->
    <link href="{{ asset('css/teaser.css') }}" rel="stylesheet">
</head>

<body>
    <div class="relative bg-white overflow-hidden teaser-bg-position bg-center bg-cover" style="background-image: url({{ asset('images/teaser-bg.jpg') }})">
        {{-- <div class="hidden lg:block lg:absolute lg:inset-0">
          <svg class="absolute top-0 transform -translate-y-8 ltr:translate-x-64 ltr:left-1/2 rtl:-translate-x-64 rtl:right-1/2" width="640" height="784" fill="none" viewBox="0 0 640 784">
            <defs>
              <pattern id="9ebea6f4-a1f5-4d96-8c4e-4c2abf658047" x="118" y="0" width="20" height="20" patternUnits="userSpaceOnUse">
                <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor" />
              </pattern>
            </defs>
            <rect y="72" width="640" height="640" class="text-gray-50" fill="currentColor" />
            <rect x="118" width="404" height="784" fill="url(#9ebea6f4-a1f5-4d96-8c4e-4c2abf658047)" />
          </svg>
          <img src="/images/bg-teaser.jpg" width="700" class="absolute top-0 transform translate-y-16 ltr:translate-x-64 ltr:left-1/2 rtl:-translate-x-40 rtl:right-1/2 opacity-25">

        </div> --}}
        <div class="fixed top-0 right-0 h-screen w-screen bg-primary-600 bg-opacity-25"></div>

        <div class="relative pt-6 pb-16 md:pb-20 lg:pb-24 xl:pb-32">
            @include('teaser.navbar')
            @include('teaser.hero-content')
        </div>
    </div>
</body>

</html>