<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOverviewStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wallets', function (Blueprint $table) {
            $table->boolean('is_overview_statistics_consistent')->default(0)->after('status');
        });

            Schema::create('overview_statistics', function (Blueprint $table)
            {
                $table->id();
                $table->unsignedBigInteger('user_id');
                $table->decimal('total_investments',64, 0);
                $table->decimal('dividends',64, 0);
                $table->decimal('unrealized_gain',64, 0);
                $table->timestamps();
                $table->foreign('user_id')->references('id')->on('users');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overview_statistics');

        Schema::table('wallets', function (Blueprint $table) {
            $table->dropColumn('is_overview_statistics_consistent');
        });

    }
}
