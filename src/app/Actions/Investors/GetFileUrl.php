<?php

namespace App\Actions\Investors;

use App\Models\File;
use App\Enums\FileType;
use App\Models\Users\User;
use App\Models\Listings\Listing;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Lorisleiva\Actions\ActionRequest;
use App\Enums\Listings\AttachmentType;
use Lorisleiva\Actions\Concerns\AsAction;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetFileUrl
{
    use AsAction;

    public function handle(File $file)
    {
        return apiResponse(200, 'done', ['file_url' => $file->temp_full_path]);
    }

    public function asController(ActionRequest $request, File $file)
    {
        try {

            $checkAuthorizeResponse = $this->checkAuthorize($request);

            if (!$checkAuthorizeResponse) {
                throw new AuthorizationException();
            }

            return $this->handle($file);
        } catch (AuthorizationException $e) {
            return apiResponse(400, $e->getMessage());
        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }
    }

    public function checkAuthorize(ActionRequest $request)
    {
        $file = $request->route('file');
        $user = getAuthUser($request->guard == 'api' ? 'api' : 'web');

        if ($file->type === FileType::SubscriptionForm) {
            return $file->fileable->investor_id === $user->id;
        } else if ($file->type === FileType::ListingReport) {
            return $user->can('accessListingWhichInvestedIn', $file->fileable->listing);
        } else if ($file->type === FileType::TransactionReceipt) {
            return $file->fileable->iban === $user->wallet->iban;
        } else if ($file->type === FileType::ListingAttachment) {
            return $file->fileable->is_visible ||
                $user->can('accessListingWhichInvestedIn', $file->fileable);
        } else if ($file->type === FileType::TransactionZatcaEInvoice) {
            return $file->fileable->invoicable->investor_id === $user->id;
        }

        return false;
    }
}
