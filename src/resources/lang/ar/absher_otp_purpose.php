<?php

use App\Enums\Elm\AbsherOtpReason;

return [
    AbsherOtpReason::InvestingInFund => 'الاستثمار في :fund',
    AbsherOtpReason::VerifyIdentity => 'التحقق من هويتك',
    AbsherOtpReason::VerifyLogin => 'التحقق من هويتك',
    AbsherOtpReason::ForgetPassword => 'نسيان كلمة المرور',
];
