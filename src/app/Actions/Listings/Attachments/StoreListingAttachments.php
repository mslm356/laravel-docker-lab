<?php

namespace App\Actions\Listings\Attachments;

use App\Models\File;
use App\Enums\FileType;
use App\Services\Files\CheckFile;
use Illuminate\Support\Str;
use Lorisleiva\Actions\Concerns\AsAction;

class StoreListingAttachments
{
    use AsAction;

    public $checkFile;

    public function __construct()
    {
        $this->checkFile = new CheckFile();
    }

    public function handle($listing, $attachments)
    {
        foreach ($attachments as $attachment) {

            $extentions = ['pdf','png','jpeg','jpg','heic','xlsx','xls','doc','docx'];
            $this->checkFile->handle($attachment, $extentions);

            $path = File::store($attachment['file'], "listings/{$listing->id}/attachments");

            File::create([
                'id' => (string)Str::uuid(),
                'fileable_id' => $listing->id,
                'fileable_type' => $listing->getMorphClass(),
                'type' => FileType::ListingAttachment,
                'name' => $attachment['file']->getClientOriginalName(),
                'path' => $path,
                'extra_info' => [
                    'type' => $attachment['type']
                ],
            ]);
        }
    }
}
