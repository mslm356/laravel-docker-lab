<?php

namespace App\Models\AML;

use App\Models\AppModel;
use App\Models\Users\User;

class AmlUser extends AppModel
{

    /**
     * Get the user of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
