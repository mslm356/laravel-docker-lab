<div class="{{ $color ?? 'text-primary-500' }} {{ $fontSize ?? 'text-lg' }} {{ $isBold ? 'font-bold' : 'font-normal' }}">{{ $ar }}</div>
<div class="{{ $color ?? 'text-primary-500' }} {{ $fontSize ?? 'text-lg' }} {{ $isBold ? 'font-bold' : 'font-normal' }} text-right" dir="ltr">{{ $en }}</div>
