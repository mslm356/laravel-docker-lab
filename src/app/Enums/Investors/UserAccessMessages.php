<?php

namespace App\Enums\Investors;

use BenSampo\Enum\Enum;

final class UserAccessMessages extends Enum
{
    const NinNotValid = 1;
    const ProfileNotValid = 2;
    const IdentityVerified = 3;
    const ApprovedStatus = 4;
    const KycCompleted = 5;
    const SuitabilityStatus = 6;
    const AmericanNationalty = 7;
}
