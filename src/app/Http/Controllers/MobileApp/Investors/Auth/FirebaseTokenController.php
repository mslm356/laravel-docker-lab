<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Models\UserFirebaseToken;
use App\Services\Investors\FirebaseTokenServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\AuthResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class FirebaseTokenController extends Controller
{
//    public $firebaseTokenServices;

    public function __construct () {

//        $this->firebaseTokenServices = new FirebaseTokenServices();
    }

    public function updateFirebase(Request $request)
    {
        return apiResponse(200, __('mobile_app/message.success'));

        $rules = [
            'device_token' => 'required:string',
            'device_id' => 'required|string',
        ];

        try {

            $data = Validator::make($request->all(), $rules)->validate();

            $user = getAuthUser('api');
           // $user = $request->user();

            $lang = $request->header('lang') ?? 'ar';

            $this->firebaseTokenServices->createUserFirebaseToken($user, $data['device_token'], $data['device_id'], $lang);

        } catch (ValidationException $e) {
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage(), ['errors' => $errors]);
        } catch (\Exception $e) {
            return apiResponse(400, __('mobile_app/message.please_try_later'));
        }

        return apiResponse(200, __('mobile_app/message.success'));
    }


}
