<?php

namespace App\Models\Campaigns;

use App\Enums\Campaigns\CampaignsCode;
use App\Models\AppModel;
use App\Models\Users\User;

class Campaign extends AppModel
{

    public function getCampaignsTypeAttribute()
    {
        if ($this->type == CampaignsCode::Email) {
            return __("email");
        } elseif ($this->type == CampaignsCode::Sms) {
            return __("sms");
        } elseif ($this->type == CampaignsCode::Notification) {
            return __("notification");
        } else {
            return __("all");
        }
    }

    public function users ()
    {
        return $this->belongsToMany(User::class, 'campaign_users')
            ->withPivot('user_id','status', 'response', 'user_data')
            ->withTimestamps();
    }

}
