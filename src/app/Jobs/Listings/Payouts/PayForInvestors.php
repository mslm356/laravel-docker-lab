<?php

namespace App\Jobs\Listings\Payouts;

use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PayForInvestors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $payout;
    public $data;
    public $logService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payout, $data)
    {
        $this->payout = $payout;
        $this->data = $data;
        $this->logService = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \App\Actions\Listings\Payouts\PayForInvestors::run($this->data, $this->payout);

        } catch (\Exception $e) {
            $this->logService->log('PAYOUT-PAY-FOR-INVESTOR-EXC', $e);
        }

    }
}
