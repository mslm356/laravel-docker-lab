<?php

use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\Transfer\TransferController;


Route::middleware(['auth:api', 'jwt_check:api', 'role:' .Role::Investor])->group(function () {

    Route::post('/transfer', [TransferController::class, 'transfer'])->middleware('user-can-access:transfer:api');
    Route::post('/transfer/fees', [TransferController::class, 'getTransferFeeOptions']);

});
