<?php

return [

    'subject' => 'Your upgrade request has been approved',
    'welcome_message' => 'Dear investor  :name',
    'message' => 'We congratulate you on accepting your application for (professional investor), Aseel wishes you all the best',
    'thanks_message' => 'Thank you for your dealings with Aseel platform',
    'info_message' => 'To access your control panel, please click on the link',
    'control_panel_btn' => 'View the control panel',
];
