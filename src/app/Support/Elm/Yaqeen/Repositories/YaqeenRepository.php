<?php

namespace App\Support\Elm\Yaqeen\Repositories;

use App\Services\LogService;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Support\Elm\Yaqeen\AuthBodyParams;
use App\Support\Elm\Yaqeen\YaqeenSoapClient;
use App\Support\Elm\Yaqeen\Exceptions\CrException;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use App\Support\Elm\Yaqeen\Exceptions\BirthDateException;
use App\Support\Elm\Yaqeen\Exceptions\ConfigurationException;
use App\Support\Elm\Yaqeen\Exceptions\ExternalSystemException;
use App\Support\Elm\Yaqeen\Exceptions\AddressLanguageException;
use App\Support\Elm\Yaqeen\Exceptions\MissingInfoRelatedToIdException;

class YaqeenRepository
{
    public $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }

    /**
     * Get the citizen information
     *
     * @param string $nin
     * @param string $dateOfBirth
     * @return array
     * @throws \Exception
     */
    public function getCitizenInfo($nin, $dateOfBirth)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'CitizenInfoRequest' => array_merge(
                AuthBodyParams::get(),
                [
                    'nin' => $nin,
                    'dateOfBirth' => $dateOfBirth,
                    'referenceNumber' => (string) Str::uuid(),
                ]
            )
        ];

        try {

            $response = $client->__soapCall('getCitizenInfo', [$requestBody]);

            $this->logServices->log('YAQEEN-REQUEST-RESPONSE-CITIZEN', null, [$response, $requestBody]);

        } catch (\Throwable $th) {

            $this->logServices->log('YAQEEN-EXCEPTION-CITIZEN', $th, ['Request_Body'=> $requestBody]);
            $this->handleError($th, $requestBody);
        }

        $citizenInfo = $response->CitizenInfoResult;

        $explodedDate = explode('-', $citizenInfo->dateOfBirthH);

        if(in_array($explodedDate[2], ["30","29"]) &&  $explodedDate[1] == "02") {
            $explodedDate[2] = "28" ;
            $explodedDateFormat = Carbon::createFromFormat('Y-m-d', implode('-', $explodedDate))->toDateString();
        }

        return [
            'nin' => $nin,
            'birth_date_type' => 'hijri',
            'first_name' => ['en' => $citizenInfo->englishFirstName, 'ar' => $citizenInfo->firstName],
            'second_name' => ['en' => $citizenInfo->englishSecondName, 'ar' => $citizenInfo->fatherName],
            'third_name' => ['en' => $citizenInfo->englishThirdName, 'ar' => $citizenInfo->grandFatherName],
            'forth_name' => ['en' => $citizenInfo->englishLastName, 'ar' => $citizenInfo->familyName],
            'id_expiry_date' => Carbon::createFromFormat('d-m-Y', $citizenInfo->idExpiryDate)->format('Y-m-d'),
            'gender' => strtoupper($citizenInfo->gender) === 'M' ? 'male' : 'female',
            'nationality' => 'saudi',
            'birth_date' => isset($explodedDateFormat) && $explodedDateFormat ? $explodedDateFormat : Carbon::createFromFormat('d-m-Y', $citizenInfo->dateOfBirthH)->toDateString(),
        ];
    }

    /**
     * Get the information by Iqama
     *
     * @param string $iqamaNumber
     * @param string $dateOfBirth
     * @return array
     * @throws \Exception
     */
    public function getInfoByIqama($iqamaNumber, $dateOfBirth)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'AlienInfoByIqamaRequest' => array_merge(
                AuthBodyParams::get(),
                [
                    'dateOfBirth' => $dateOfBirth,
                    'iqamaNumber' => $iqamaNumber,
                    'referenceNumber' => (string) Str::uuid(),
                ]
            )
        ];

        try {
            $response = $client->__soapCall('getAlienInfoByIqama', [$requestBody]);
            $this->logServices->log('YAQEEN-REQUEST-RESPONSE-IQAMA-CITIZEN', null, [$response, $requestBody]);

        } catch (\Throwable $th) {

            $this->logServices->log('YAQEEN-EXCEPTION-IQAMA-CITIZEN', $th);
            $this->handleError($th, $requestBody);
        }

        $iqamaInfo = $response->AlienInfoByIqamaResult;

        return [
            'nin' => $iqamaNumber,
            'birth_date_type' => 'gregorian',
            'first_name' => ['en' => $iqamaInfo->englishFirstName, 'ar' => $iqamaInfo->firstName],
            'second_name' => ['en' => $iqamaInfo->englishSecondName, 'ar' => $iqamaInfo->secondName],
            'third_name' => ['en' => $iqamaInfo->englishThirdName, 'ar' => $iqamaInfo->thirdName],
            'forth_name' => ['en' => $iqamaInfo->englishLastName, 'ar' => $iqamaInfo->lastName],
            'gender' => strtoupper($iqamaInfo->gender) === 'M' ? 'male' : 'female',
            'nationality' => $iqamaInfo->nationalityDesc,
            'birth_date' => Carbon::createFromFormat('d-m-Y', $iqamaInfo->dateOfBirthG)->format('Y-m-d')
        ];
    }

    /**
     * Get the citizen address information
     *
     * @param string $nin
     * @param string $dateOfBirth
     * @param string $locale
     * @throws \Exception
     */
    public function getCitizenAddress($nin, $dateOfBirth, $locale)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'CitizenInfoAddressRequest' => array_merge(
                AuthBodyParams::get(),
                [
                    'nin' => $nin,
                    'dateOfBirth' => $dateOfBirth,
                    'referenceNumber' => (string) Str::uuid(),
                    'addressLanguage' => $locale,
                ],
            )
        ];

        try {
            $response = $client->__soapCall('getCitizenInfoAddress', [$requestBody]);
            $this->logServices->log('YAQEEN-Citizen-ADDRESS-REQUESR-RESPONSE', null, [$response, $requestBody]);
        } catch (\Throwable $th) {
            $this->logServices->log('YAQEEN-EXCEPTION-Citizen-Address', $th);
            $this->handleError($th, $requestBody);
        }

        $addresses = $response->CitizenInfoAddressResult->addressListList;

        if (!is_array($addresses)) {
            $addresses = [$addresses];
        }

        return $this->mapAddresses($addresses);
    }

    /**
     * Get the citizen address information
     *
     * @param string $iqama
     * @param string $dateOfBirth
     * @param string $locale
     * @throws \Exception
     */
    public function getAddressInfoByIqama($iqama, $dateOfBirth, $locale)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'AlienInfoByIqamaAddressRequest' => array_merge(
                [

                    'iqamaNumber' => $iqama,
                    'dateOfBirth' => $dateOfBirth,
                    'referenceNumber' => (string) Str::uuid(),
                    'addressLanguage' => $locale,

                ],
                AuthBodyParams::get(),
            )
        ];

        try {
            $response = $client->__soapCall('getAlienInfoByIqamaAddress', [$requestBody]);
            $this->logServices->log('YAQEEN-ADDRESS-INFO-IQAMA', null, [$response, $requestBody]);
        } catch (\Throwable $th) {
            $this->logServices->log('YAQEEN-EXCEPTION-ADDRESS-INFO-IQAMA', $th);
            $this->handleError($th, $requestBody);
        }

        $addresses = $response->AlienInfoByIqamaAddressResult->addressListList;

        if (!is_array($addresses)) {
            $addresses = [$addresses];
        }

        return $this->mapAddresses($addresses);
    }

    /**
     * Get the citizen address information
     *
     * @param string $iqama
     * @param string $dateOfBirth
     * @param string $locale
     * @throws \Exception
     */
    public function getCompanyAddressInfoByCr($cr, $locale)
    {
        $client = YaqeenSoapClient::instantiate();

        $requestBody = [
            'CompanyAddressInfoByCrRequest' => array_merge(
                AuthBodyParams::get(),
                [

                    'crNumber' => $cr,
                    'addressLanguage' => $locale,
                ],
            )
        ];

        try {
            $response = $client->__soapCall('getCompanyAddressInfoByCr', [$requestBody]);
            $this->logServices->log('YAQEEN-COMPANY-ADDRESS', null, [$response, $requestBody]);
        } catch (\Throwable $th) {
            $this->logServices->log('YAQEEN-EXCEPTION-COMPANY-ADDRESS', $th);
            $this->handleError($th, $requestBody);
        }

        return $response;
    }

    protected function handleError(\Throwable $th, array $requestBody)
    {
        if ($fault = $th->detail->Yakeen4Aseel4ROWADFault->commonErrorObject->enc_value ?? null) {
            $exceptionArgs = [$fault->ErrorCode, $fault->Type, $fault->ErrorMessage];

            $this->logServices->log('ABSHER-RESPONSE-FAULT', null, [$fault, $th]);

            switch ($fault->ErrorCode) {
                case 1:
                case 2:
                case 6:
                case 8:
                    throw new IdException();
                    break;

                case 4:
                    throw new MissingInfoRelatedToIdException($requestBody);

                case 5:
                case 15:
                    throw new CrException();
                    break;

                case 3:
                    throw new ConfigurationException($requestBody, ...$exceptionArgs);
                    break;

                case 7:
                case 9:
                case 11:
                case 12:
                case 13:
                case 14:
                    throw new BirthDateException();
                    break;

                case 10:
                    throw new AddressLanguageException();
                    break;

                default:
                    throw new ExternalSystemException(...$exceptionArgs);
                    break;
            }
        } else {
            throw $th;
        }
    }

    protected function mapAddresses(array $addresses)
    {
        return collect($addresses)
            ->map(fn ($address) => [
                'post_code' => $address->postCode,
                'additional_number' => $address->additionalNumber,
                'building_number' => $address->buildingNumber,
                'city' => $address->city,
                'district' => $address->district,
                'location' => $address->locationCoordinates,
                'street_name' => $address->streetName,
                'unit_number' => $address->unitNumber,
            ])
            ->toArray();
    }
}
