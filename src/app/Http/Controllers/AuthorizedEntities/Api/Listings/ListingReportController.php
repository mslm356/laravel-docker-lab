<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use App\Actions\Listings\Reports\UpdateOrCreateReport;
use App\Common\ValidationRules\Listings\ListingReportRules;
use App\Enums\Listings\Report;
use App\Enums\Listings\ReportEnum;
use App\Enums\Role;
use App\Http\Controllers\AuthorizedEntities\Api\Listings\CanAccessListing;
use App\Mail\AuthorizedEntities\Listings\CreateReport;
use App\Mail\Investors\WelcomeToAseel;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorizedEntities\FileResource;
use App\Http\Resources\AuthorizedEntities\Listings\ListingReportResource;
use App\Models\Listings\ListingReport;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ListingReportController extends Controller
{
    use CanAccessListing;

    /**
     * Get all the reports of listing
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $reports = $listing->reports()
            ->with([
                'files'
            ])
            ->latest()
            ->simplePaginate();

        return ListingReportResource::collection($reports);
    }

    /**
     * Store the report in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $listing, ListingReport $report)
    {
        $report['type'] = $report->user && $report->user->hasRole(Role::AuthorizedEntityAdmin) ? "Authorized Entity" : "Admin";
        $this->canUserAccessListing($report->listing);

        $report['status'] = [
            'value' => $report->status,
            'description' => ReportEnum::getDescription($report->status)
        ];

        return response()->json([
            'data' => $report->toArray() + [
                    'files' => FileResource::collection($report->files)
                ]
        ]);
    }

    /**
     * Store the report in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $listing)
    {
        $listing = Listing::find($listing);
        if (!$listing) {
            return response()->json(['data' => ['message' => 'Listing Not Found']], 400);
        }

        $user = request()->user('sanctum');

        $this->canUserAccessListing($listing);

        $data = $this->validate($request, ListingReportRules::get());

        DB::transaction(function () use ($data, $listing, $request) {
            UpdateOrCreateReport::run($this->mapData($data), $listing, null, $request);
        });

        if (config('mail.MAIL_STATUS') == 'ACTIVE') {
            Mail::to('ap_update@investAseel.sa')->queue(new CreateReport($user, $listing));
        }

        return response()->json([], 201);
    }

    /**
     * Update the report in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Listings\ListingReport $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $listing, ListingReport $report)
    {
        $this->canUserAccessListing($report->listing);

        $data = $this->validate($request, ListingReportRules::get());

        DB::transaction(function () use ($data, $report, $request) {
            UpdateOrCreateReport::run($this->mapData($data), null, $report, $request);
        });

        return response()->json();
    }

    /**
     * map data for saving
     *
     * @param array $data
     * @return array
     */
    public function mapData($data)
    {
        return [
            'title_en' => $data['title_en'],
            'title_ar' => $data['title_ar'],
            'description_en' => $data['description_en'] ?? null,
            'description_ar' => $data['description_ar'] ?? null,
            'status' => ReportEnum::PendingReview,
            'files' => Arr::get($data, 'files', [])
        ];
    }
}
