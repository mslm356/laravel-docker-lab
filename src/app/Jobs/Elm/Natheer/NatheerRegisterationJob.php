<?php

namespace App\Jobs\Elm\Natheer;

use App\Support\Elm\Natheer\NatheerWatchListService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NatheerRegisterationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public function __construct($user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        $natheerWatchListService = new NatheerWatchListService();

        $date_of_birth=str_replace('-', '', $this->user->nationalIdentity->birth_date);

        $responce= $natheerWatchListService->addPerson($this->user->nationalIdentity->nin, $date_of_birth);

        if($responce=="true") {
            $this->user->update(['added_to_natheer'=>true]);
        }
    }
}
