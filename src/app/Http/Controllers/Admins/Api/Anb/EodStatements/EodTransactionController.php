<?php

namespace App\Http\Controllers\Admins\Api\Anb\EodStatements;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\EodStatements\EodTransactionResource;
use App\Models\Anb\AnbEodStatement;
use App\Models\Anb\AnbEodTransaction;
use App\Support\QueryScoper\Scopes\Admin\Anb\EodTransactions\FilterByDateRangeScope;
use App\Support\QueryScoper\Scopes\Admin\Anb\EodTransactions\OrderByValueDateScope;

class EodTransactionController extends Controller
{
    /**
     * Get FAQ list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return EodTransactionResource::collection(
            AnbEodTransaction::toScopes([
                OrderByValueDateScope::class,
                FilterByDateRangeScope::class,
            ])->orderBy('id','desc')->paginate()
        );
    }
}
