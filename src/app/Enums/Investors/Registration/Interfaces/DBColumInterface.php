<?php
namespace App\Enums\Investors\Registration\Interfaces;

interface DBColumInterface
{
    function getDBColumnName();

}
