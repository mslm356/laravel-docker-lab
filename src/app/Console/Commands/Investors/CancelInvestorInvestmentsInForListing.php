<?php

namespace App\Console\Commands\Investors;

use App\Enums\Banking\WalletType;
use App\Models\Elm\AbsherOtp;
use App\Models\File;
use App\Models\Users\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\VirtualBanking\Transaction;
use Bavix\Wallet\Models\Transfer;

class CancelInvestorInvestmentsInForListing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investments:cancel-for-listing {--userId=} {--listingId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel investments for a listing for a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listingId = $this->option('listingId');
        $userId = $this->option('userId');

        DB::transaction(function () use ($listingId, $userId) {
            $listing = Listing::findOrFail($listingId);
            $user = User::findOrFail($userId);
            $investorWallet = $user->getWallet(WalletType::Investor);

            $escrowWallets = $listing->wallets()
                ->whereIn('slug', [
                    WalletType::ListingEscrowVat,
                    WalletType::ListingEscrow,
                    WalletType::ListingEscrowAdminFee,
                ])
                ->get();

            $transfers = Transfer::where([
                'from_type' => $investorWallet->getMorphClass(),
                'from_id' => $investorWallet->id,
            ])
                ->whereIn('to_type', $escrowWallets->map(fn ($wallet) => $wallet->getMorphClass())->values())
                ->whereIn('to_id', $escrowWallets->pluck('id')->values())
                ->get();

            foreach ($transfers as $transfer) {
                $deposit = $transfer->deposit;
                $withdraw = $transfer->withdraw;

                File::whereIn('fileable_id', [$deposit->id, $withdraw->id])
                    ->where('fileable_type', (new Transaction())->getMorphClass())
                    ->delete();

                $deposit->delete();
                $withdraw->delete();
                $transfer->delete();
            }

            DB::table('listing_investor')
                ->where('listing_id', $listing->id)
                ->where('investor_id', $user->id)
                ->delete();

            $investingRecords = ListingInvestorRecord::where('listing_id', $listing->id)
                ->where('investor_id', $user->id)
                ->get();

            foreach ($investingRecords as $record) {
                $record->subscriptionForm()->delete();
                $record->zatcaInvoice->pdfFile()->delete();
                $record->zatcaInvoice->delete();
                AbsherOtp::where('data->subscription_token_id', $record->subscription_token_id)->delete();
                $record->delete();
                InvestorSubscriptionToken::where('id', $record->subscription_token_id)->delete();
            }

            foreach ($escrowWallets as $escrowWallet) {
                $escrowWallet->refreshBalance();
            }

            $investorWallet->refreshBalance();
        });

        return true;
    }
}
