<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class AddingZatcaInvoiceInfoInAccountingSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add(
            'zatca.seller',
            [
                'cr_number' => '1010728236',
                'vat_id' => '311091023800003',
                'name' => [
                    'en' => 'شركة أصيل المالية',
                    'ar' => 'شركة أصيل المالية',
                ],
                'address' => [
                    'line1' => [
                        'en' => '3504 Al Imam Saud Bin Faisal Rd - Al Malqa',
                        'ar' => '3504 طريق الامام سعود بن فيصل - حي الملقا',
                    ],
                    'line2' => [
                        'en' => 'Unit Number: 3509 RIYADH 13522 - 6418',
                        'ar' => 'رقم الوحدة: 3509 الرياض 13522 - 6418',
                    ],
                ]
            ]
        );
    }

    public function down(): void
    {
        $this->migrator->delete('zatca.seller');
    }
}
