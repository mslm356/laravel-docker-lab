<?php

namespace App\Support\Elm\Yaqeen\Exceptions;

use Exception;

class BirthDateException extends Exception
{
    public function __construct()
    {
        parent::__construct('Birth date is not correct');
    }
}
