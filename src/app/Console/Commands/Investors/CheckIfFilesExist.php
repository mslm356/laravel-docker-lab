<?php

namespace App\Console\Commands\Investors;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\GetSubscriptionFormData;
use App\Actions\Listings\GetCurrentInvestments;
use App\Enums\Role;
use App\Jobs\Zatca\GenerateEInvoiceAfterInvesting;
use App\Models\Elm\AbsherOtp;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\NationalIdentity;
use App\Models\Users\User;
use App\Models\Zatca\ZatcaInvoice;
use App\Settings\ZatcaSettings;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Zatca\EInvoice\Order;
use App\Support\Zatca\EInvoice\PurchaseLine;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;
use Illuminate\Validation\ValidationException;

class CheckIfFilesExist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-if-files-exist {--listing_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check-if-files-exist ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        try {

            return DB::transaction(function () {

                ListingInvestorRecord::query()->with(['listing', 'investor'])
                    ->where('listing_id', $this->option('listing_id'))
                    ->lazyById()
                    ->each(function ($record) {

                        $user = $record->investor;
                        $listing = $record->listing;
                        $token = $record->subscriptionToken;

                        $subscriptionFormFile = $record->subscriptionForm;

                        if (!$subscriptionFormFile) {
                            dump("Subscription Form Not  Exists In DB  ");
                        } else {

                            if ($subscriptionFormFile->checkFileExists() == false)
                                dump("Subscription Form File Not Exists => Id :  " . $subscriptionFormFile->id);

                        }

                        $e_invoice = ZatcaInvoice::where('invoicable_type', ListingInvestorRecord::class)
                            ->where('invoicable_id', $record->id)
                            ->first();

                        if (!$e_invoice) {
                            dump("ZatcaInvoice Not  Exists In DB  ");
                        } else {

                            if (!$e_invoice->pdfFile) {

                                dump("zatca pdf file not found => Id :  " . $e_invoice->id);

                            } else {

                                if ($e_invoice->pdfFile->checkFileExists() == false)
                                    dump("Pdf File Not  Exists => Id :  " . $e_invoice->id);
                            }
                        }


                        /*$toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing, $token->shares);

//                        $currentInvest = GetCurrentInvestments::run($listing)->where('investor_id', $user->id)->first();

                        $subscriptionFormData = GetSubscriptionFormData::run(
                            $token,
                            $user,
                            $listing,
                            $toBeInvestedAmountAndFees['total'],
                            null
                        );

                        ExportSubFormAndAttachToInvestingRecord::dispatch(
                            $record,
                            $token,
                            $user,
                            $listing,
                            $subscriptionFormData
                        );

                        $feePercentage = $toBeInvestedAmountAndFees['feePercentage'];
                        $vatPercentage = $toBeInvestedAmountAndFees['vatPercentage'];

                        GenerateEInvoiceAfterInvesting::dispatch(
                            $user,
                            new Order(
                                transaction_ref(),
                                [
                                    new PurchaseLine(
                                        [
                                            'en' => "({$feePercentage}%) of investing in {$listing->title_en}",
                                            'ar' => "(%{$feePercentage}) من مبلغ الاستثمار في {$listing->title_ar}",
                                        ],
                                        1,
                                        $toBeInvestedAmountAndFees['fee'],
                                        $vatPercentage,
                                        null
                                    )
                                ],
                                now('Asia/Riyadh'),
                                $record
                            ),
                            app(ZatcaSettings::class)->seller,
                            'zatca-e-invoices',
                            null
                        );

                        dump('done success');*/

                    });
            });

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }

    }


}
