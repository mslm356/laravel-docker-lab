<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Http\Controllers\Controller;
use App\Services\Investors\TokenServices;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class RefreshTokenController extends Controller
{

    public $logService;
    public $tokenServices;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->tokenServices = new TokenServices();
    }

    public function refreshToken(Request $request)
    {

        try {

//            if (!$request->hasheader('authorization')) {
//                $request->headers->set('authorization', $request->token);
//            }

            $token = \JWTAuth::getToken();
            $refreshToken = \JWTAuth::refresh($token);
            $tokenPayload = $this->checkValidToken($token, $refreshToken);

            DB::table('users')
                ->where('id', $tokenPayload['sub'])
                ->update(['jwt_token' => $refreshToken]);

            return apiResponse('200', __('mobile_app/message.login_successfully'), ['token' => $refreshToken]);

        } catch (ValidationException $e) {
            return apiResponse(401, 'Unauthenticated.');

        } catch (\Exception $e) {
            $this->logService->log('REFRESH-TOKEN', $e, ['data', 'token' => $token]);
            return apiResponse(401,'Unauthenticated.');

        }
    }

    public function checkValidToken ($token, $refreshToken)
    {

        $refreshTokenPayload = \JWTAuth::payload($refreshToken);
        $refreshTokenPayload = $refreshTokenPayload->toArray();

        if (!isset($refreshTokenPayload['sub'])) {
            throw ValidationException::withMessages([]);
        }

        $userJwt = DB::table('users')
            ->where('id', $refreshTokenPayload['sub'])
            ->where('jwt_token', $token)
            ->count('jwt_token');

        if (!$userJwt) {
            throw ValidationException::withMessages([]);
        }

        return $refreshTokenPayload;
    }
}
