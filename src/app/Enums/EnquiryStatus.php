<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class EnquiryStatus extends Enum
{
    const Open = 1;
    const Closed = 2;
    const Waiting = 3;
    const Answered = 4;
}
