<?php

namespace App\Models\Enquiries;

use App\Enums\Role;
use App\Models\AppModel;
use App\Models\Users\User;

class EnquiryReply extends AppModel
{
    /**
     * Get the enquiry information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enquiry()
    {
        return $this->belongsTo(Enquiry::class, 'enquiry_id', 'id');
    }

    /**
     * Get the sender information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    /**
     * Get the sender details
     *
     * @return \Illuminate\Support\Collection
     */
    public function getSenderDetailsAttribute()
    {
        if ($this->sender_id) {
            return collect([
                'name' => $this->sender->full_name,
                'email' => $this->sender->email,
                'fmt_phone_number' => $this->sender->fmt_phone,
                'role' => $this->sender->getRoleNames()->map(function ($name) {
                    return Role::getDescription($name);
                })->implode(', ')
            ]);
        } else {
            return collect([
                'name' => $this->enquiry->name,
                'email' => $this->enquiry->email,
                'fmt_phone_number' => $this->enquiry->fmt_phone,
                'role' => __('Visitor')
            ]);
        }
    }
}
