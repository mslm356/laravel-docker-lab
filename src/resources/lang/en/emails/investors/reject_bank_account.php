<?php

return [
    'subject' => 'Your IBAN adding request has been denied',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'We apologize for not accepting your request to add the bank account',
    'message_2' => 'please see the request for the rejection reason :reason',
    'message_3' => 'Thank you for dealing with Aseel platform',
    'link_text' => 'To view your control panel, please go to the link',
    'view' => 'View control panel',
];
