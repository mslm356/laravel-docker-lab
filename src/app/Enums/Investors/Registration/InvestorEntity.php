<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\DBColumInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class InvestorEntity extends Enum implements LocalizedEnum
{
    const Individual =   1;
    const Firm =   2;



}
