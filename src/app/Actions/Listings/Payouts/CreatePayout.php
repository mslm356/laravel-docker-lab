<?php

namespace App\Actions\Listings\Payouts;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Jobs\Listings\Payouts\CreateInvestors;
use App\Models\Payouts\Payout;
use App\Models\Users\User;
use Illuminate\Support\Carbon;
use App\Models\Listings\Listing;
use Lorisleiva\Actions\Concerns\AsAction;

class CreatePayout
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param \App\Models\Listings\Listing $listing
     * @param array $data
     * @return object
     */
    public function handle(Listing $listing, array $data)
    {
        $total = $data['total'];

        $totalInstance = (money_parse_by_decimal($total, $defaultCurrency = defaultCurrency()));

//        $investors = $listing->investors;
//        $investedShares = $investors->sum('pivot.invested_shares');

        $payout = $listing->payouts()->create([
            'total' => $totalInstance->getAmount(),
            'currency' => $defaultCurrency,
            'payout_date' => Carbon::today(),
//            'invested_units' => $investedShares,
//            'unit_price' =>  $totalInstance->divide($investedShares)->getAmount(),
            'type' => $data['type'],
        ]);

        $createInvestorsJob = (new CreateInvestors($listing, $payout, $totalInstance));
        dispatch($createInvestorsJob)->onQueue('payouts');

        return $payout;
    }

    public function payForInvestor ($payout, $paid, $investor) {

        $wallet = $payout->listing->getWallet(WalletType::ListingSpv);

        $wallet->transfer(
            $investor->getWallet(WalletType::Investor),
            $paid,
            [
                'reason' => TransactionReason::PayoutDistribution,
                'reference' => transaction_ref(),
                'value_date' => now('UTC')->toDateTimeString(),
                'listing_id' => $payout->listing_id,
                'listing_title_en' => $payout->listing->title_en,
                'listing_title_ar' => $payout->listing->title_ar,
                'payout_id' => $payout->id,
                'user_id' => $investor->id,
                'user_name' => $investor->full_name
            ]
        );

    }
}
