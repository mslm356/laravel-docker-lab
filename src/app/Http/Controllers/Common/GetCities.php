<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Http\Resources\Common\CityResource;

class GetCities extends Controller
{
    /**
     * Listings list
     *
     * @param
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function __invoke()
    {
        $cities = City::where('country', 'SA')->get();

        return CityResource::collection($cities);
    }
}
