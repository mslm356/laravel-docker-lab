<?php

namespace App\Models\Enquiries\Traits;

use Illuminate\Support\Str;
use App\Models\Enquiries\EnquiryToken;

trait HasTokens
{
    /**
     * Get the tokens of the enquiry
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tokens()
    {
        return $this->hasMany(EnquiryToken::class, 'enquiry_id', 'id');
    }

    /**
     * Create token for the enquiry reply
     *
     * @return array
     */
    public function createToken()
    {
        $tokenModel = $this->tokens()->create([
            'token' => EnquiryToken::hash($plainText = Str::random(40)),
        ]);

        return ['tokenInstance' => $tokenModel, 'tokenPlainText' => $tokenModel->id . '-' . $plainText];
    }
}
