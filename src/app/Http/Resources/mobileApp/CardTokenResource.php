<?php

namespace App\Http\Resources\mobileApp;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CardTokenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'card_token' => $this->card_token,
            'sub_mask' => $this->sub_mask,
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d H:i'),
            'updated_at' => Carbon::parse($this->updated_at)->format('Y-m-d H:i')
        ];
    }
}
