<?php

namespace App\Actions\Listings\Reports;

use App\Enums\Listings\ReportEnum;
use App\Enums\Role;
use App\Models\File;
use App\Enums\FileType;
use App\Services\Files\CheckFile;
use Illuminate\Support\Arr;
use App\Models\Listings\ListingReport;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdateOrCreateReport
{
    use AsAction;

    public $checkFile;

    public function __construct()
    {
        $this->checkFile = new CheckFile();
    }

    public function handle($data, $listing, $report = null, $request)
    {
        $user = $request->user('sanctum');
        $modelData = Arr::only(
            $data,
            ['title_en', 'title_ar', 'description_en', 'description_ar']
        );

        if ($report) {
            if (!in_array($report->status, [ReportEnum::Approved, ReportEnum::Rejected]) || !$user->hasRole(Role::AuthorizedEntityAdmin)) {
                $report->update($modelData);
            }
        } else {
            $report = ListingReport::create(
                $modelData + [
                    'listing_id' => $listing->id,
                    'user_id' => $user->id,
                    'status' => $user->hasRole(Role::AuthorizedEntityAdmin) ? ReportEnum::PendingReview : ReportEnum::Approved
                ]
            );

        }

        if ($report) {
            if (!in_array($report->status, [ReportEnum::Approved, ReportEnum::Rejected]) || !$user->hasRole(Role::AuthorizedEntityAdmin)) {
                foreach ($data['files'] as $file) {

                    $this->checkFile->handle($file);

                    File::upload($file, "listings/{$report->listing_id}/reports/{$report->id}", [
                        'fileable_id' => $report->id,
                        'fileable_type' => $report->getMorphClass(),
                        'type' => FileType::ListingReport,
                    ]);
                }
            }
        }

        return $report;
    }
}
