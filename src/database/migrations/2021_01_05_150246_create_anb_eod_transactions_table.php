<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnbEodTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anb_eod_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('statement_id');
            $table->string('mark', 3);
            $table->decimal('amount', 64, 0);
            $table->string('code', 4);
            $table->string('customer_ref');
            $table->string('bank_ref');
            $table->dateTime('value_date')->nullable();
            $table->dateTime('book_date')->nullable();
            $table->string('description');
            $table->timestamps();

            $table->foreign('statement_id')->references('id')->on('anb_eod_statements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anb_eod_transactions');
    }
}
