<?php

namespace App\Enums\Listings;

use BenSampo\Enum\Enum;

final class VoteAnswers extends Enum
{
    const Approved = 1;
    const Rejected = 2;
    const Refrain = 3;
}
