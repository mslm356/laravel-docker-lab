<?php


namespace App\Services\SubscriptionForm;


use App\Jobs\AccountStatements\ApproveRequestJob;
use App\Traits\NotificationServices;
use App\Traits\SendMessage;
use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Enums\Notifications\NotificationTypes;
use App\Enums\SubscriptionForm\RequestStatus;
use App\Mail\SubscriptionForm\NewRequest;
use App\Models\SubscriptionFormRequest;
use App\Services\Investors\Investment\HandleResponseService;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubscriptionForm\SubscriptionFormRequestReceived;

class SendRequest
{
    use NotificationServices, SendMessage;

    public $logService;
    public $handleResponseService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->handleResponseService = new HandleResponseService();
    }

    public function handle($request, $user)
    {
        $response = ['status' => true];

        if ($user->investor && $user->investor->status != 3) {
            return $this->handleResponseService->responseValues($response, false, trans("messages.identity_verified_valid"), []);
        }

        $subscriptionFormRequest = SubscriptionFormRequest::query()
            ->where('user_id', $user->id)
            ->whereIn('status', [RequestStatus::InReview, RequestStatus::PendingReview])
            ->count();

        if ($subscriptionFormRequest) {
            return $this->handleResponseService->responseValues($response, false, __('messages.you_have_pending_subscription_form'), []);
        }

        $data = [
            'user_id' => $user->id,
            'status' => RequestStatus::PendingReview,
        ];

        $subscriptionFormRequest = DB::transaction(function () use ($request, $data, $user) {

            return SubscriptionFormRequest::create($data);
        });

        // send mail
        if (config('mail.MAIL_STATUS') == 'ACTIVE') {
            Mail::to($user)->queue(new NewRequest($user));
        }

        $this->sendSms($user);

        $adminEmails = config('app.env') === 'production' ? ['husam@investaseel.sa', 'm.alshehri@investaseel.sa'] : ['a.elsaeed@investaseel.sa'];

        Mail::to($adminEmails)->queue(new SubscriptionFormRequestReceived($subscriptionFormRequest));

        $this->send(NotificationTypes::SubscriptionForm, $this->notificationData($subscriptionFormRequest), $user->id);

        ApproveRequestJob::dispatch($subscriptionFormRequest)->delay(Carbon::now()->addMinutes(1));

        return $this->handleResponseService->responseValues($response, true, __('Done successfully'), []);
    }

    public function sendSms($user)
    {
        $name = $user ? $user->full_name : '---';
        $content_ar_1 = "  تم استلام طلب كشف عمليات الاستثمار في منصة أصيل وسيتم الرد عليكم قريبا من قبل الفريق ";
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar_1;
        $content_en = "Our dear investor " . $name . " We have received your request for account statement and we will reply to you as soon as possible";
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }

    public function notificationData($subscriptionFormRequest)
    {
        return [
            'action_id' => $subscriptionFormRequest->id,
            'channel' => 'mobile',
            'title_en' => 'Aseel',
            'title_ar' => 'منصة أصيل',
            'content_en' => ' We have received your request for account statement and we will reply to you as soon as possible ',
            'content_ar' => ' تم استلام طلب كشف عمليات الاستثمار في منصة أصيل وسيتم الرد عليكم قريبا من قبل الفريق ',
        ];
    }
}
