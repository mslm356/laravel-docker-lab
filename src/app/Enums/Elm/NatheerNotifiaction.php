<?php

namespace App\Enums\Elm;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class NatheerNotifiaction extends Enum implements LocalizedEnum
{
    const Death_Indicator_Changed = 1000;
    const Final_ExitVisa = 2000;
    const Crossing_out_with_Final_ExitVisa = 3000;
    const Final_ExitVisa_Canceled = 4000;
    const DL_Issued_Renewed  = 5000;
    const IQAMA_Renewed = 6000;
    const IQAMA_Issued_for_borderNumber = 9000;
    const Exit_with_ReEntry_Visa_but_has_not_returned_after_Visa_expiry = 30000;
}
