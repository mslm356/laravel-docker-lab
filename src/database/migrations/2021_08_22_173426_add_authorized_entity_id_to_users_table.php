<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAuthorizedEntityIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('authorized_entity_id')->after('email')->nullable();

            $table->foreign('authorized_entity_id', 'users_authorized_entity_id_foreign')->references('id')->on('authorized_entities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_authorized_entity_id_foreign');
            $table->dropColumn('authorized_entity_id');
        });
    }
}
