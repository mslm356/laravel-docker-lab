<?php

namespace App\Http\Resources\Investors\mobileApp\banners;

use Illuminate\Http\Resources\Json\JsonResource;

class BannersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'image' => $this->imageFile ? url($this->imageFile->path) : null ,
        ];
    }
}
