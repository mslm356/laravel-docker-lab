<?php

namespace App\Http\Controllers\Admins\Api\Logs;

use App\Http\Controllers\Controller;

class WorkerLogController extends Controller
{
    /**
     * Get Laravel logs
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLogs()
    {
        $file = file_get_contents(storage_path(config('logging.worker_filename')));

        if ($file === false) {
            return response()->json([
                'message' => __('messages.logs_file_not_found')
            ], 400);
        }

        return response()->json([
            'data' => [
                'logs' => $file
            ]
        ]);
    }

    /**
     * Clear Laravel logs
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function clearLogs()
    {
        $file = file_put_contents(storage_path(config('logging.worker_filename')), '');

        if ($file === false) {
            return response()->json([
                'message' => __('messages.failed_to_clear_logs')
            ], 400);
        }

        return response()->json();
    }
}
