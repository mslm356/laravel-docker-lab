<?php

return [
    'subject' => 'Enquiry #:id',
    'body' => 'We have received your enquiry and we will reply to you as soon as possible.',
];
