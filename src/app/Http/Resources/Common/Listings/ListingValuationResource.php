<?php

namespace App\Http\Resources\Common\Listings;

use Illuminate\Http\Resources\Json\JsonResource;

class ListingValuationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'value' => $this->value->formatByDecimal(),
            'date' => $this->date->toDateString(),
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
