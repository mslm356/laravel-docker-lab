/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import vSelect from 'vue-select';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import Input from './FormControls/Input';
import Textarea from './FormControls/Textarea';
import ContactUs from './components/ContactUs';
import VueI18n from 'vue-i18n'
require('./bootstrap');
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light-border.css';

window.tippy = tippy;

window.Vue = require('vue');

const i18n = new VueI18n({
    locale: document.documentElement.lang
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('contact-us', ContactUs);
Vue.component('v-input', Input);
Vue.component('v-textarea', Textarea);
Vue.component('scale-loader', require('vue-spinner/src/ScaleLoader.vue').default);


Vue.component('v-select', vSelect);

// Register it globally
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    i18n
});
