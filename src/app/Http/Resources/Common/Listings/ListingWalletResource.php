<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Banking\VirtualAccountType;
use App\Actions\Listings\Wallets\MapsWalletType;
use App\Enums\Banking\WalletType;
use App\Models\Listings\ListingInvestorRecord;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingWalletResource extends JsonResource
{
    use MapsWalletType;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

       $this->getWalletBalance($this);

        return [
            'id' => $this->id,
            'name' => WalletType::getDescription($this->slug),
            'type' => $this->getWalletTypeKey($this->slug),
//            'balance' => money($this->balance)->format(),
            'balance' => $this->getWalletBalance($this),
            'virtual_accounts' => $this->virtualAccounts->map(fn ($account) => [
                'identifier' => $account->identifier,
                'type' => [
                    'value' => $account->type,
                    'description' => VirtualAccountType::getDescription($account->type)
                ]
            ])
        ];
    }

    function getWalletBalance ($wallet) {

        if ($wallet->holder->is_closed) {
            return money($wallet->balance)->format();
        }

        $listingRecord = ListingInvestorRecord::where('listing_id', $wallet->holder_id);

        if ($wallet->slug == WalletType::ListingEscrow) {
            $investEscrow = $listingRecord->sum('amount_without_fees');
            return  money($investEscrow)->format();
        }

        if ($wallet->slug == WalletType::ListingEscrowAdminFee) {
            $investFees = $listingRecord->sum('admin_fees');
            return money($investFees)->format(); ;
        }

        if ($wallet->slug == WalletType::ListingEscrowVat) {
            $investVat = $listingRecord->sum('vat_amount');
            return money($investVat)->format(); ;
        }

        return  money($wallet->balance)->format();
    }
}
