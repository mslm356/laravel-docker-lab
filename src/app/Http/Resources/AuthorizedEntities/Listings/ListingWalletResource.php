<?php

namespace App\Http\Resources\AuthorizedEntities\Listings;

use App\Http\Resources\Common\Listings\ListingWalletResource as CommonListingWalletResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingWalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingWalletResource($this->resource))->toArray($request),
            []
        );
    }
}
