<?php

namespace App\Console\Commands\Membership;

use Carbon\Carbon;
use App\Services\LogService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Jobs\NotificationFirebaseBulk;
use App\Enums\Notifications\NotificationTypes;

class SendAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:send-alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create member ship send alert';

    public $logService;
    public $count = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            //            $this->sendAutoRequestNotify();
            $this->sendAlertForAdmin();

            dump($this->count);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }

    public function notificationData()
    {

        return [

            'title_ar' => __('membership.notify_title', [], 'ar'),
            'title_en' => __('membership.notify_title', [], 'ar'),
            'content_ar' => __('membership.notify_cmd', [], 'ar'),
            'content_en' => __('membership.notify_cmd', [], 'ar'),
            'channel' => 'mobile'

        ];

    }

    public function sendAlertForAdmin()
    {
        $usersCollection = collect();

        $result = DB::table('member_ship_requests as t1')
            ->whereIn('status', [1, 3])
            ->select('t1.*')
            ->whereRaw('t1.id = (SELECT MAX(id) FROM member_ship_requests WHERE user_id = t1.user_id AND status IN (1, 3))')
             ->orderBy('t1.id', 'desc')
            ->lazy(100)
            ->each(function ($member_ship_requests) use ($usersCollection) {

                if($member_ship_requests->member_ship != 1) {
                    $this->count++;

                    $usersCollection->push($member_ship_requests);
                }

            });

        $notification = new NotificationFirebaseBulk(NotificationTypes::Membership, $this->notificationData(), $usersCollection);
        dispatch($notification)->onQueue('notify');

        //        DB::table('member_ship_requests')
        //            ->where('initiator_type', 2)
        //            ->whereIn('status', [1, 3])
        //            ->select('member_ship_requests.user_id', DB::raw('COUNT(member_ship_requests.user_id)'))
        //            ->groupBy('member_ship_requests.user_id')
        //            ->orderBy('member_ship_requests.user_id', 'asc')
        //            ->lazy(100)
        //            ->each(function ($member_ship_requests) {
        //
        //                $this->count++;
        //
        //                $notificationData = $this->notificationData();
        //                $this->send(NotificationTypes::Membership, $notificationData, $member_ship_requests->user_id);
        //
        //            });

    }


    public function sendAutoRequestNotify()
    {
        //        $duringTimeValue = config('memberShip.during_time');
        //        $duringDate = now()->subMonths($duringTimeValue);
        //        $goldValue = config('memberShip.gold');
        //        $diamondValue = config('memberShip.diamond');

        //            DB::table('users')
        //                ->join('listing_investor', function ($join) use ($duringDate) {
        //                    $join->on('users.id', '=', 'listing_investor.investor_id')
        //                        ->whereDate('listing_investor.created_at', '>=', $duringDate);
        //                })
        //                ->select('users.id as user_id', DB::raw('SUM(invested_amount) as invested_amountss'), 'users.member_ship')
        ////                ->where('users.member_ship', '!=', MemberShipType::Diamond)
        //                ->groupBy('users.id')
        //                ->having('invested_amountss', '>', $goldValue * 100)
        //                ->orderBy('users.id', 'asc')
        //                ->lazy(100)
        //                ->each(function ($user) use ($diamondValue, $goldValue) {
        //                    $amount = money($user->invested_amountss, defaultCurrency())->formatByDecimal();
        //
        //                    if ($amount > $diamondValue) {
        //                        $memberShip = MemberShipType::Diamond;
        //                    } elseif ($amount > $goldValue) {
        //                        $memberShip = MemberShipType::Gold;
        //                    } else {
        //                        $memberShip = MemberShipType::Regular;
        //                    }
        //
        //                    if ($memberShip != MemberShipType::Regular) {
        //                        $this->count++;
        //
        //                        $notificationData = $this->notificationData();
        //                        $this->send(NotificationTypes::Membership, $notificationData, $user->user_id);
        //                    }
        //                });

    }

}
