<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use Illuminate\Auth\Access\AuthorizationException;

trait CanAccessListing
{
    /**
     * Can authorized entity access the listing
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Illuminate\Auth\Access\Response
     */
    public function canUserAccessListing($listing)
    {
        if (request()->user('sanctum')->cannot('accessListingByAuthorizedEntity', $listing)) {
            throw new AuthorizationException;
        }
    }
}
