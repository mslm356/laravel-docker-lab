<?php

namespace App\Http\Resources\Admins;

use App\Enums\Role;
use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'role_id' => $this->role_id,
            'role' => $this->roles[0]? $this->roles[0]->name : '',
            'type' => $this->type,
        ];
    }
}
