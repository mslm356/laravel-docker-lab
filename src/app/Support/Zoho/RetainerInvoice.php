<?php


namespace App\Support\Zoho;


use App\Models\ZohoSettings;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class RetainerInvoice
{

    public $base_url;
    public $organization_id;
    public $auth;

    public function __construct()
    {
        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
    }

    public function prepareRetainerInvoiceData($user, $amount)
    {

        $data = [
            'customer_id' => $user->zoho_id,
            'line_items' => [
                [
                    'description' => 'customer deposit',
                    'rate' => $amount,
                ]
            ],
        ];

        return $data;
    }

    public function store($user, $amount)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->prepareRetainerInvoiceData($user, $amount);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'retainerinvoices?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                $this->markRetainerInvoiceAsSent($responseData['retainerinvoice']['retainerinvoice_id']);

                return $responseData['retainerinvoice']['retainerinvoice_id'];
            }

            Log::error('ZOHO-RETAINER-INVOICE', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-RETAINER-INVOICE', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return null;
    }

    public function markRetainerInvoiceAsSent($invoice_id)
    {

        $token = $this->auth->getToken();

        try {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'retainerinvoices/' . $invoice_id . '/status/sent?organization_id=' . $this->organization_id);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] != 0) {
                Log::error('ZOHO-RETAINER-INVOICE-MARK-SENT', [$responseData]);
            }

        } catch (\Exception $e) {
            Log::error('ZOHO-RETAINER-INVOICE-MARK-SENT', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }
}
