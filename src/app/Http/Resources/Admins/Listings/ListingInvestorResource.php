<?php

namespace App\Http\Resources\Admins\Listings;

use App\Http\Resources\Common\Listings\ListingInvestorResource as CommonListingInvestorResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingInvestorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonListingInvestorResource($this->resource))->toArray($request),
            []
        );
    }
}
