<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Investors\Data\InvestorKycEnum;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingInvestorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_name' => $this->type == "user" ? $this->full_name : optional($this->companyProfile)->name ,
            'type' => $this->type,
            'kyc_status_key' =>($this->investor) ? $this->investor->is_kyc_completed : -1,
            'kyc_status' => InvestorKycEnum::match(($this->investor) ? $this->investor->is_kyc_completed : -1),
            'invested_shares' => $this->pivot->invested_shares,
            'invested_amount' => money($this->pivot->invested_amount, $this->pivot->currency)->formatByDecimal(),
            'nin' => $this->type == "user"  ? optional($this->nationalIdentity)->nin : optional($this->companyProfile)->commercial_number,
            'yaqeen_full_name' => $this->type == "user" ? optional($this->nationalIdentity)->yaqeen_full_name : optional($this->companyProfile)->name
        ];
    }
}
