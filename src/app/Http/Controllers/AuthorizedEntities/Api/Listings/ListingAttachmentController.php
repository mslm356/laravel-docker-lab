<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use App\Actions\Listings\Attachments\StoreListingAttachments;
use App\Actions\Listings\Attachments\DeleteListingAttachment;
use App\Common\ValidationRules\Listings\ListingAttachmentRules;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ListingAttachmentController extends Controller
{
    use CanAccessListing;

    /**
     * Store attachments
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Listings\Listing $file
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $data = $this->validate($request, ListingAttachmentRules::get());

        DB::transaction(function () use ($listing, $data) {
            StoreListingAttachments::run(
                $listing,
                Arr::get($data, 'attachments', [])
            );
        });

        return response()->json([], 201);
    }

    /**
     * Delete attachment
     *
     * @param \App\Models\File $file
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, File $file)
    {
        if (!($file->fileable instanceof Listing)) {
            throw new AuthorizationException;
        }

        $this->canUserAccessListing($file->fileable);

        DB::transaction(function () use ($file) {
            DeleteListingAttachment::run($file);
        });

        return response()->json([], 201);
    }
}
