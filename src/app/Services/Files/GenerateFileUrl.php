<?php


namespace App\Services\Files;

use App\Enums\FileType;
use App\Models\File;
use Illuminate\Auth\Access\AuthorizationException;

class GenerateFileUrl
{

    public function handle(File $file)
    {
        return apiResponse(200, 'done', ['file_url' => $file->temp_full_path]);
    }

    public function getUrl(File $file, $user)
    {
        $checkAuthorizeResponse = $this->checkAuthorize($file, $user);

        if (!$checkAuthorizeResponse) {
            throw new AuthorizationException();
        }

        return $this->handle($file);
    }

    public function checkAuthorize($file, $user)
    {
        if ($file->type === FileType::SubscriptionForm) {
            return $file->fileable->investor_id === $user->id;
        } elseif ($file->type === FileType::ListingReport) {
            return $user->can('accessListingWhichInvestedIn', $file->fileable->listing);
        } elseif ($file->type === FileType::TransactionReceipt) {
            return $file->fileable->iban === $user->wallet->iban;
        } elseif ($file->type === FileType::ListingAttachment) {
            return $file->fileable->is_visible ||
                $user->can('accessListingWhichInvestedIn', $file->fileable);
        } elseif ($file->type === FileType::TransactionZatcaEInvoice) {
            return $file->fileable->invoicable->investor_id === $user->id;
        }

        return false;
    }
}
