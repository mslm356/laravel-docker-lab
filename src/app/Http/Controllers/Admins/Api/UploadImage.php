<?php

namespace App\Http\Controllers\Admins\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadImage extends Controller
{
    /**
     * Download file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $file = $this->validate($request, [
            'upload' => ['file', 'image']
        ])['upload'];

        $path = $file->store('images', 'public');

        return response()->json([
            'url' => asset(Storage::disk('public')->url($path))
        ]);
    }
}
