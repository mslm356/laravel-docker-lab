<?php

namespace App\Http\Controllers\Admins\Api\Reports;

use App\Actions\Reports\ExportInvestorsDeposit;
use App\Exports\Investors\DepositReportExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Concerns\AsAction;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    public function __invoke(Request $request)
    {
        if ($request['password'] != '1912') {
           return redirect(config('app.admin_url') . '/app/reports');
        }

        $data = $this->validate($request, $this->rules());

        $types = [
            'Investors' => 'DepositReportExport',
            'Transactions' => 'TransactionReportExport',
            'Wallets' => 'WalletReportExport',
            'AnbTransfer' => 'AnbTransferReportExport',
            'Transaction Balance' => 'TransactionBalanceReportExport',
            'Investors profile with kyc status' => 'ProfileWithKyc',
        ];

        $className = 'App\Exports\Investors\\'. $types[$data['type']];

        return Excel::download(new $className($data), $data['type'] . ".xlsx");
    }

    public function rules()
    {

        return [
            'type' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ];
    }
}
