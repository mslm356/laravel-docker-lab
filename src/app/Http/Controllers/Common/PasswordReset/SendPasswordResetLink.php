<?php

namespace App\Http\Controllers\Common\PasswordReset;

use App\Http\Controllers\Controller;
use App\Services\Investors\Users\LastForgetService;
use App\Services\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class SendPasswordResetLink extends Controller
{
    public $logService;
    public $lastForgetService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->lastForgetService = new LastForgetService();
    }

    public function __invoke(Request $request)
    {
        try {

            $request->validate([
                'email' => ['required', 'email']
            ]);

            $responseOfLastForget = $this->lastForgetService->handle($request['email'],'password');

            if (isset($responseOfLastForget['status']) && !$responseOfLastForget['status']) {
                $msg = isset($responseOfLastForget['msg']) ? $responseOfLastForget['msg'] : 'please try later';
                return apiResponse(400, __($msg));
            }

            $status = Password::sendResetLink(
                $request->only('email')
            );


            if ($status === Password::RESET_LINK_SENT) {
                return response()->json();
            }

            throw ValidationException::withMessages([
                'email' => __('mobile_app/message.forget_password_validation')
            ]);

        } catch (ValidationException $e) {
           // $code = $this->logService->log('WEB-VALIDATION-FORGET-PASSWORD', $e);
            return apiResponse(200, __('mobile_app/message.forget_password_validation'));
        } catch (\Exception $e) {
            $code = $this->logService->log('WEB-FORGET-PASSWORD', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }

    }
}
