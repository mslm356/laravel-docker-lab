<?php

namespace App\Http\Controllers\Investors\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UpdatePassword extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $this->validate($request, [
            'current_password' => ['required', 'string'],
            'password' => ['required', 'string', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/', 'confirmed'],
        ],);

        if (!Hash::check($data['current_password'], $request->user()->password)) {
            throw ValidationException::withMessages([
                'current_password' => __('validation.incorrect_password')
            ]);
        }

        if(Hash::check($request->password, $request->user()->password))
            return apiResponse(400,__('messages.change_same_password_error_msg'));


        $request->user()->update([
            'password' => Hash::make($data['password'])
        ]);

        return response()->json();
    }
}
