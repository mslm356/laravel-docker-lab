<?php

namespace App\Http\Controllers\Admins\Api\Transfer;

use App\Enums\Investors\UpgradeRequestStatus;
use App\Enums\Transfer\TransferStatus;
use App\Exceptions\FailedProcessException;
use App\Exceptions\NoBalanceException;
use App\Http\Resources\Admins\Transfer\TransferRequestsResource;
use App\Models\Transfer\TransferRequest;
use App\Services\Investors\TransferServices;
use App\Services\LogService;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TransferRequestsController extends Controller
{

    public $transferServices;
    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->transferServices = new TransferServices();
    }

    public function index(Request $request)
    {
        try {

            $auth = $request->user();

            if (!$auth->can('transfer_review')) {
                return \response()->json(['message' => 'not of your permissions'], 400);
            }

            $data = TransferRequest::query()->with('reviewer')
                ->leftJoin('users', function ($join) use ($request) {
                    $join->on('users.id', '=', 'transfer_requests.user_id');
                })
                ->leftJoin('bank_accounts', function ($join) use ($request) {
                    $join->on('transfer_requests.bank_account_id', '=', 'bank_accounts.id');
                })
//                ->leftJoin('wallets', function ($join) use ($request) {
//                    $join->on('transfer_requests.user_id', '=', 'wallets.holder_id');
//                })
                ->leftJoin('national_identities', function ($join) use ($request) {
                    $join->on('transfer_requests.user_id', '=', 'national_identities.user_id');
                });

            if ($request->has('investor_withdrawal_id') && $request['investor_withdrawal_id'] != null) {
                $data->where('national_identities.nin', $request->investor_withdrawal_id);
            }

            if ($request->has('from') && $request['from'] != null) {
                $data->whereDate('transfer_requests.created_at', '>=', $request['from']);
            }

            if ($request->has('to') && $request['to'] != null) {
                $data->whereDate('transfer_requests.created_at', '<=', $request['to']);
            }

            if ($request->has('status') && $request['status'] != null) {
                $data->where('transfer_requests.status', $request['status']);
            }

            $data = $data->select('transfer_requests.id',
                DB::raw('CONCAT(users.first_name, " ", users.last_name) as full_name'),
                'transfer_requests.user_id', 'transfer_requests.status', 'transfer_requests.amount',
                'transfer_requests.reviewer_comment', 'transfer_requests.reviewer_id',
                'transfer_requests.created_at', 'transfer_requests.updated_at',
//                'wallets.balance',
                'bank_accounts.bank',
                'national_identities.nin'
            )
                ->orderBy('transfer_requests.id', 'desc')
                ->paginate(10);

            return TransferRequestsResource::collection($data);

        } catch (\Exception $e) {
            $code = $this->logService->log('TRANSFER-REQUEST-Admin', $e);
            return response()->json('sorry, please try later : ' . $code, 400);
        }
    }

    public function show(TransferRequest $transferRequest)
    {

        return new TransferRequestsResource(
            $transferRequest
        );
    }

    /**
     * Update status of the request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, TransferRequest $transferRequest)
    {
        try {

            $data = $this->validate($request, [
                'status' => ['required', 'integer', new EnumValue(TransferStatus::class, false)],
                'reviewer_comment' => ['nullable', 'string',textValidation(),'max:500']
            ]);

            $auth = $request->user();

            if (!$auth->can('transfer_accept')) {
                throw ValidationException::withMessages(['status' => __('not of your permissions')]);
            }

            if (!in_array($transferRequest->status, [TransferStatus::Pending, TransferStatus::InReview])) {
                throw ValidationException::withMessages(['status' => __('Sorry, this request already updated')]);
            }

            if ($data['status'] == TransferStatus::Approved) {

                $client = $transferRequest->user;

                $transferData = [
                    'bank_account_id' => $transferRequest->bank_account_id,
                    'fee' => $transferRequest->fee,
                    'amount' => \money($transferRequest->amount)->formatByDecimal(),
                ];

                $transferServiceResponse = $this->transferServices->transferMoney($transferData, $client, 'admin', $transferRequest->id);

                if (isset($transferServiceResponse['status']) && $transferServiceResponse['status'] && isset($transferServiceResponse['transfer_id'])) {

                    $transferRequest->update([
                        'anb_transfer_id' => $transferServiceResponse['transfer_id'] ?? null,
                        'status' => $data['status'],
                        'reviewer_comment' => $data['reviewer_comment'],
                        'reviewer_id' => $auth->id,
                    ]);

                    return response()->json(['data' => [
                        'message' => 'Transfer success'
                    ]], 200);
                }

            } else {

                $transferRequest->update([
                    'status' => $data['status'],
                    'reviewer_comment' => $data['reviewer_comment'],
                    'reviewer_id' => $auth->id,
                ]);
            }

            return response()->json(['data' => [
                'message' => 'Request updated successfully'
            ]], 200);

        } catch (NoBalanceException $e) {
            $code = $this->logService->log('NoBalanceException-TRANSFER-Admin', $e);
            return apiResponse(400, __('messages.no_enough_balance') . ' : ' . $code);
        } catch (FailedProcessException $e) {
            $code = $this->logService->log('FailedProcessException-TRANSFER-Admin', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-TRANSFER-WEB', $e);
            return response()->json(['message' => $e->getMessage() . ' : ' . $code, 'errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            $code = $this->logService->log('TRANSFER-REQUEST-Admin', $e);
            return response()->json('sorry, please try later : ' . $code, 400);
        }

    }

    public function allowedStatus($user)
    {

        $allowedStatus = [
//            TransferStatus::Pending,
            'in_review' => TransferStatus::InReview,
            'rejected' => TransferStatus::Rejected,
            'approved' => TransferStatus::Approved,
        ];

        if ($user->can(['transfer_review', 'transfer_accept'])) {
            return $allowedStatus;
        } elseif ($user->can('transfer_review')) {
            unset($allowedStatus['approved']);
            return $allowedStatus;
        } elseif ($user->can('transfer_accept')) {
            unset($allowedStatus['in_review']);
        } else {
            return [];
        }
    }


    public function accept(Request $request)
    {

        try {
            $auth = $request->user();

            if (!$auth->can('transfer_accept')) {
                throw ValidationException::withMessages(['canTransfer' => __('not of your permissions')]);
            }

            $transferRequests = TransferRequest::where('created_at', '<=', Carbon::now()->subHours(24)->toDateTimeString())
                ->where('status', TransferStatus::Pending)->get();

            foreach ($transferRequests as $transferRequest) {

                $client = $transferRequest->user;

                $transferRequest->update([
                    'status' => TransferStatus::Approved,
                    'reviewer_comment' => "accept all withdraw operations",
                    'reviewer_id' => $auth->id,
                ]);

                $transferData = [
                    'bank_account_id' => $transferRequest->bank_account_id,
                    'fee' => $transferRequest->fee,
                    'amount' => \money($transferRequest->amount)->formatByDecimal(),
                ];

                $transferServiceResponse = $this->transferServices->transferMoney($transferData, $client, 'admin', $transferRequest->id);

                if (isset($transferServiceResponse['status']) && $transferServiceResponse['status'] && isset($transferServiceResponse['transfer_id'])) {

                    $transferRequest->update([
                        'anb_transfer_id' => $transferServiceResponse['transfer_id'] ?? null,
                    ]);

                    $this->logService->log('TRANSFER-SUCCESS', null, [
                        "transfer_request" => $transferRequest]);

                } else {

                    $this->logService->log('TRANSFER-FAILED', null, [
                        "transfer_request" => $transferRequest]);

                    $transferRequest->update([
                        'status' => TransferStatus::Pending,
                        'reviewer_comment' => null,
                        'reviewer_id' => null,
                    ]);

                }
            }

            return response()->json(['data' => [
                'message' => 'Requests updated successfully'
            ]], 200);


        } catch (\Exception $e) {
            $code = $this->logService->log('TRANSFER-REQUEST-Admin', $e);
            return response()->json('sorry, please try later : ' . $code, 400);
        }

    }
}
