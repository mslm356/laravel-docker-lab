<?php

namespace App\Http\Resources\Admins\Investors;

use App\Enums\Investors\InvestorStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class InvestorListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->full_name,
            'phone_number' => $request->has('type_user') == 'company' ? '' : $this->phone_number,
            'status' => [
                'value' => $this->investor ? $this->investor->status : null,
                'description' => $this->investor ?  InvestorStatus::getDescription($this->investor->status) : null,
            ],
            'is_kyc_completed' => $this->investor ? $this->investor->is_kyc_completed : null
        ];
    }
}
