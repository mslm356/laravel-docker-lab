<?php


namespace App\Services\Investors;

use App\Enums\Banking\BankCode;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\TransferPurpose;
use App\Enums\Banking\WalletType;
use App\Enums\Transfer\TransferStatus;
use App\Exceptions\FailedProcessException;
use App\Exceptions\NoBalanceException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Common\TransferFeeResource;
use App\Jobs\Transfer\AcceptWithdrawOperationJob;
use App\Jobs\transfer\CheckTransfer;
use App\Models\Anb\AnbTransfer;
use App\Models\BankAccount;
use App\Models\Transfer\TransferRequest;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Anb\AnbApiUtil;
use App\Support\Anb\Enums\AnbPaymentStatus;
use App\Support\Anb\Http\HttpException;
use App\Support\VirtualBanking\BankService;
use App\Support\VirtualBanking\Helpers\LocalSaudiTransferFee;
use Carbon\Carbon;
use Cknow\Money\Money;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class TransferServices extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function feesOptions($data)
    {

        $bankAccount = BankAccount::find($data['bank_account_id']);

        $feeOptions = (new BankService())->getTransferFeeOptions($bankAccount->bank, money_parse_by_decimal($data['amount'], defaultCurrency()));

        return TransferFeeResource::collection(collect($feeOptions));
    }

    public function transferMoney($data, $user, $action_source = 'customer', $transfer_request_id = null, $defaultChannel = null, $defaultDate = null)
    {

        $response = ['status' => true];

        $wallet = $user->getWallet(WalletType::Investor);

        $canTransfer = $this->canTransfer($user->id, $wallet);

        if (isset($canTransfer['status']) && !$canTransfer['status'] && $action_source != "admin") {
            throw ValidationException::withMessages(['canTransfer' => $canTransfer['msg']]);
        }

        $bankService = new BankService();


        $bankAccount = BankAccount::find($data['bank_account_id']);

        $info = [
            'id' => $user->id,
            'name' => $bankAccount->name,
            'city' => 'Riyadh',
        ];

        $amount = money_parse_by_decimal($data['amount'], defaultCurrency());

        if (!$bankService->validateTransferFeeOption($bankAccount->bank, $amount, $data['fee'])) {
            throw new FailedProcessException(__('messages.invalid_transfer_fee_option'));
        }

        $amountWithFee = $this->getTransferFinalAmountData($amount, $data['fee']);

        $wallet->refreshBalance();

        if ($amountWithFee->greaterThan(money($wallet->balance))) {
            $this->updateTransferRequestStatus($transfer_request_id, TransferStatus::Rejected);
            throw new NoBalanceException();
        }

        if ($action_source != 'admin') {

            $responseOfCheckLimit = $this->checkTransferLimit($data, $user->id);

            if ($responseOfCheckLimit['status']) {
                $response['status'] = false;
                $response['msg'] = isset($responseOfCheckLimit['msg']) ? $responseOfCheckLimit['msg'] : '';
                return $response;
            }
        }

        $channel = $defaultChannel ?? $this->getChannel($amount, $bankAccount->bank);

        if (!in_array($channel, ['ANB', 'SARIE', 'IPS', 'SWIFT'])) {
            throw new FailedProcessException(__('messages.channel_not_valid'));
        }

        $responseData = DB::transaction(function () use ($data, $user, $wallet, $amount, $bankAccount, $channel) {

            $transaction = $wallet->withdraw(
                $amount->getAmount(),
                [
                    'reference' => transaction_ref(),
                    'reason' => TransactionReason::AnbExternalTransfer,
                    'value_date' => ($valueDate = LocalSaudiTransferFee::getDeliveryDate($data['fee']))
                        ->timezone('UTC')
                        ->toDateTimeString(),
                    'iban' => $bankAccount->iban,
                    'bank_code' => $bankAccount->bank,
                ],
                false
            );
            $wallet->confirm($transaction);

            $wallet->refreshBalance();

            $transfer = AnbTransfer::create([
                'initiator_id' => $user->id, // Auth::guard('sanctum')->user()->id,
                'to_bank' => $bankAccount->bank,
                'transaction_id' => $transaction->id,
                'to_account' => $bankAccount->iban,
                'amount' => $amount->getAmount(),
                'currency' => $amount->getCurrency()->getCode(),
                'data' => [],
                'channel' => $channel,
                'bank_account_id' => optional($bankAccount)->id
            ]);

            return [
                'transfer' => $transfer,
                'transaction' => $transaction,
                'value_date' => $valueDate,
            ];

        }, 1);

        $checkTransferJob = (new CheckTransfer($responseData['transfer']->id, $transfer_request_id));
        dispatch($checkTransferJob)
            ->delay(\Carbon\Carbon::now()->addMinutes(20))
            ->onQueue('transfer');

        $date = $defaultDate ?? $responseData['value_date']->toMutable();

        $bankService->transfer(
            $info,
            $bankAccount->iban,
            $amount,
            $bankAccount->bank,
            $responseData['transaction']->id,
            $date,
            true,
            TransferPurpose::OwnAccountTransfer,
            'SAR',
            $bankAccount,
            $responseData['transfer']
        );

        $response['transfer_id'] = $responseData['transfer']->id;

        return $response;

    }

    /**
     * Get transfer with fee
     *
     * @param \Cknow\Money\Money $amount
     * @param string $fee
     * @return \Cknow\Money\Money
     */
    public function getTransferFinalAmountData(Money $amount, string $fee)
    {
        $feeWithVat = LocalSaudiTransferFee::feeWithVat($fee);

        return $amount; //->subtract($feeWithVat);
    }

    public function getChannel($amount, $toBankCode)
    {
        $fromBankCode = BankCode::Anb;

        if ($fromBankCode === $toBankCode) {
            return 'ANB';
        } elseif ($amount->lessThanOrEqual(money('2000000'))) {
            return 'IPS';
        } else {
            return 'SARIE';
        }
    }

    public function checkTransferLimit($data, $user_id)
    {

        $response = ['status' => false];

        if ($data['amount'] < config('transfer.transfer_limit')) {
            return $response;
        }

        $data["user_id"] = $user_id;
        $data["amount"] = Money::parseByDecimal($data['amount'], defaultCurrency())->getAmount();

        $transferRequest = TransferRequest::create($data);

        $approveTransferOperationsJob = (new AcceptWithdrawOperationJob($transferRequest->id));
        dispatch($approveTransferOperationsJob)
//            ->delay(Carbon::now()->addMinutes(10))
            ->delay(Carbon::now()->addHours(24))
            ->onQueue('transfer');

        $this->alertAdmin($data["amount"]);

        $response['status'] = true;
        return $response;
    }

    public function alertAdmin($amount)
    {

        try {

            $emails = DB::table('users')
                ->join('model_has_permissions', function ($join) {
                    $join->on('users.id', '=', 'model_has_permissions.model_id')
                        ->where('model_has_permissions.model_type', User::class);
                })
                ->join('permissions', function ($join) {
                    $join->on('permissions.id', '=', 'model_has_permissions.permission_id')
                        ->whereIn('permissions.name', [/*'transfer_review',*/ 'transfer_accept']);
                })->select('users.email')
                ->get();

            foreach ($emails as $email) {
                Mail::to($email->email)->queue(new \App\Mail\Transfer\TransferRequest($amount));
            }

            return true;

        } catch (\Exception $e) {
            $this->logService->log('TRANSFER-REQUEST-ALERT', $e);
            return false;
        }
    }

    public function checkTransferProcess($transferId, $transfer_request_id = null)
    {

        try {

            $response = ['status' => true];

            $anbTransfer = AnbTransfer::find($transferId);

            if (!$anbTransfer) {
                $response ['msg'] = 'anb transfer not found transfer id ';
                $this->logService->log('CHECK-TRANSFER-PROCESS', null, [$response ['msg'] . $transferId, $transferId]);
                $response ['status'] = false;
                return $response;
            }

            $user = $anbTransfer->user;
            $bankAccount = $anbTransfer->bankAccount;

            if (!$user) {
                $response ['msg'] = 'user not valid ';
                $this->logService->log('CHECK-TRANSFER-PROCESS', null, [$response['msg'], ['transfer id' => $transferId]]);
                $response ['status'] = false;
                return $response;
            }

            $wallet = $user->getWallet(WalletType::Investor);

            if (!$wallet) {
                $response ['msg'] = 'wallet not valid ';
                $this->logService->log('CHECK-TRANSFER-PROCESS', null, [$response['msg'], ['transfer id' => $transferId]]);
                $response ['status'] = false;
                return $response;
            }

            $responseOfPayment = $this->getPayment($transferId);

            if ($this->isRefunded($responseOfPayment, $transferId, $transfer_request_id)) {
                $refundTransaction = $wallet->deposit(
                    $anbTransfer->amount,
                    [
                        'reference' => transaction_ref(),
                        'reason' => TransactionReason::RefundAfterAnbTransferFailure,
                        'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
                        'from_iban' => $bankAccount ? $bankAccount->iban : null,
                        'from_bank_code' => $bankAccount ? $bankAccount->bank : null,
                        'source' => 'transfer-job'
                    ],
                    true
                );

                $wallet->refreshBalance();

                $anbTransfer->data = array_merge($anbTransfer->data, [
                    'status' => isset($responseOfPayment['status']) ? $responseOfPayment['status'] : null,
                    'is_refunded' => true,
                    'refund_transaction_id' => $refundTransaction->id,
                    'failure_reason' => isset($responseOfPayment['reasonOfFailure']) ? $responseOfPayment['reasonOfFailure'] : null,
                ]);

            }

            $anbTransfer->processing_status = isset($responseOfPayment['status']) ? $responseOfPayment['status'] : $anbTransfer->processing_status;
            $anbTransfer->external_trans_reference = !$anbTransfer->external_trans_reference && isset($responseOfPayment['transactionReferenceNumber']) ?
                $responseOfPayment['transactionReferenceNumber'] : $anbTransfer->external_trans_reference;
            $anbTransfer->external_id = !$anbTransfer->external_id && isset($responseOfPayment['id']) ? $responseOfPayment['id'] : $anbTransfer->external_id;
            $anbTransfer->save();

            return $response;

        } catch (\Exception $e) {
            $response['msg'] = $e->getMessage();
            $response['status'] = false;
            return $response;
        }
    }

    public function getPayment($transferId)
    {

        try {

            $response = AnbApiUtil::newPayment($transferId, 'sequence_number');

            $this->logService->log('TRANSFER-JOB-GET-PAYMENT-RESPONSE', null, ['transfer id => ' . $transferId, $response, $transferId]);

            return $response;

        } catch (HttpException $e) {
            return $e->getResponse()->json();
        } catch (\Exception $e) {
            $this->logService->log('TRANSFER-JOB-GET-PAYMENT-RESPONSE-Exc', $e);
            return true;
        }
    }

    public function isRefunded($responseOfPayment, $transferId, $transfer_request_id = null)
    {

        if (!is_array($responseOfPayment)) {
            return false;
        }

        if (isset($responseOfPayment['status']) && in_array($responseOfPayment['status'], AnbPaymentStatus::pendingStatuses())) {

            $checkTransferJob = (new CheckTransfer($transferId, $transfer_request_id));
            dispatch($checkTransferJob)
                ->delay(\Carbon\Carbon::now()->addMinutes(120))
                ->onQueue('transfer');

            return false;
        }

        if (isset($responseOfPayment['status']) && in_array($responseOfPayment['status'], AnbPaymentStatus::canceledOrFailedStatuses())) {
            $this->updateTransferRequestStatus($transfer_request_id, TransferStatus::Refund);
            return true;
        }

        if (isset($responseOfPayment['statusCode']) && $responseOfPayment['statusCode'] === "E650002") {
            $this->updateTransferRequestStatus($transfer_request_id, TransferStatus::Refund);
            return true;
        }

        return false;
    }

    public function canTransfer($user_id, $wallet)
    {

        $response = ['status' => true];

        if (!$wallet->status) {
            $response['status'] = false;
            $response['msg'] = __('Inactive Wallet');
            return $response;
        }

        $repeatedRequest = TransferRequest::query()
            ->where('user_id', $user_id)
            ->whereIn('status', [TransferStatus::Pending, TransferStatus::InReview])
            ->count();

        if ($repeatedRequest) {
            $response['status'] = false;
            $response['msg'] = __('You already have a request, please wait for it to be processed');
            return $response;
        }

        $anbTransfer = AnbTransfer::where('initiator_id', $user_id)
            ->whereIn('processing_status', AnbPaymentStatus::inProcessOrPaidStatuses())
            ->whereDate('created_at', now()->format('Y-m-d'))
            ->count();

        if ($anbTransfer >= config("transfer.transfer_operation_limit")) {
            $response['status'] = false;
            $response['msg'] = __('You have reached the daily limit for the withdraw transaction (2 transactions)');
            return $response;
        }

        return $response;
    }

    public function updateTransferRequestStatus($transfer_request_id, $status)
    {
        $transferRequest = TransferRequest::find($transfer_request_id);

        if ($transferRequest) {
            $transferRequest->status = $status; //TransferStatus::Refund;
            $transferRequest->save();
        }
    }
}
