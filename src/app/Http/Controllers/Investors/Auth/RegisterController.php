<?php

namespace App\Http\Controllers\Investors\Auth;

use App\Services\Investors\AbsherOtpService;
use App\Services\Investors\RegisterServices;
use App\Services\LogService;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    public $registerServices;
    public $yaqeenInfoService;
    public $absherOtpService;


    public function __construct()
    {
        $this->registerServices = new RegisterServices();
        $this->yaqeenInfoService = new YaqeenInfoService();
        $this->absherOtpService = new AbsherOtpService();
    }

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function register(Request $request)
    {
        $rules = $this->registerServices->rules();

        $rules['password'] = ['required', 'string', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/'/*, 'confirmed'*/];
        $ninRules = $this->registerServices->getRulesOfNin('web_app');
        $rules = array_merge($rules, $ninRules);
        $data = Validator::make($request->all(), $rules)->validate();

        try {

            $data['birthDateInstance'] = Carbon::createFromFormat('Y-m-d', $data['birth_date']);

            if ($this->registerServices->checkAgeOfUser($data['birth_date'], Str::startsWith($data['nin'], '1') ? "hijri" : "gregorian") == 0) {
                return response()->json(['errors' => ['birth_date' => [0=>__('mobile_app/message.invalid_registration_age')]]], 422);
            }

            $info = $this->yaqeenInfoService->yaqeenInfo($data);

            if ($info['nationality'] == 'الولايات المتحدة' || $info['nationality'] == 'الولايات  المتحدة') {
                return response()->json(['errors' => ['nin' => [0=>__('mobile_app/message.invalid_registration_for_american')]]], 422);
            }


            $addresses = $this->yaqeenInfoService->yaqeenAddress($data);

            $values = DB::transaction(function () use ($data, $info, $addresses) {

                $user = $this->registerServices->create($data, null);

                event(new Registered($user));

                //                Auth::login($user);

                $this->registerServices->updateUserInfo($user, $info);

                $verificationMsg = $this->absherOtpService->createOtp($user, null, null, $data['nin']);

                $this->yaqeenInfoService->updateInvestorProfile($user, $info, $addresses, $verificationMsg);




                $response = ['user' => $user, 'verificationMsg' => $verificationMsg];

                return $response;

            }, 1);
            $user = $values['user'];

            $verificationMsg = $values['verificationMsg'];

            $this->registerServices->alertUser($user);

            $user->setRelation('investor', null);

            return response()->json([
                'data' => [
                    'vid' => $verificationMsg->id,
                    'nin' => $data['nin'],
                    'sessionKey'=>encrypt($data['email'])
                ]
            ], 201);

        } catch (\Exception $e) {
            $logService = new LogService();
            $code = $logService->log('REGISTERATION-Exception', $e);
            if ($e instanceof IdException) {
                return response()->json(['data' => ['message' => __('mobile_app/message.invalid_nin').' code '.$code]], 400);
            }

            return response()->json(['data' => ['message' => __('mobile_app/message.please_try_later').' code '.$code]], 400);

        }
    }

}
