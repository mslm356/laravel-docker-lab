<?php


namespace App\Services\Investors;

use App\Enums\Listings\DiscussionStatus;

class DiscussionServices
{

    public function index($listing)
    {
        $discussions = $listing->rootDiscussions()
            ->where('status', DiscussionStatus::Published)
            ->with(['replies' => function ($query) {
                $query->where('status', DiscussionStatus::Published);
            }])
            ->latest()
            ->simplePaginate();

        return $discussions;
    }

}
