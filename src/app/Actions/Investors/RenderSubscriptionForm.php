<?php

namespace App\Actions\Investors;

use App\Models\Listings\ListingInvestor;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Listings\GetCurrentInvestments;
use App\Models\Users\InvestorSubscriptionToken;

class RenderSubscriptionForm
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param array $data
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function handle($data)
    {
        return view('investors.subscription-form', compact('data'));
    }

    public function asController(Request $request, Listing $listing, InvestorSubscriptionToken $token)
    {
        return $this->handle(
            GetSubscriptionFormData::run(
                $token,
                $user = $request->user('sanctum'),
                $listing,
                $listing->share_price->multiply($token->shares),
                GetCurrentInvestments::run($listing)->where('investor_id', $user->id),
            )
        );
    }

    public function authorize(ActionRequest $request)
    {
        return $request->user()->id === $request->route('token')->user_id &&
            $request->route('token')->fund_id === $request->route('listing')->id;
    }
}
