<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class InvestorNetWorth extends Enum
{
    const LessThanFiveMillion =   1;
    const GreaterThanFiveMillion =   2;
}
