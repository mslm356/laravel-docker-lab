<?php

namespace App\Http\Resources\Investors\Listings;

use App\Enums\InvestmentType;
use App\Enums\Listings\AttachmentType;
use App\Http\Resources\Admins\Listings\ListingEditImagesResource;
use App\Services\MemberShip\SpecialCanInvest;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use App\Enums\Listings\DetailCategory;
use App\Http\Resources\Common\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Admins\PropertyTypeResource;
use App\Http\Resources\Investors\Listings\ListingTimelineResource;

class ListingDetailsWithAuthResource extends JsonResource
{

    public $type;
    public $guard;
    public $specialCanInvest;

    public function __construct($resource, $type = null, $guard = null)
    {
        $this->type = $type;
        $this->guard = $guard;
        $this->specialCanInvest = new SpecialCanInvest();
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $locale = App::getLocale();
        $images = $this->images->sortBy("index");

        $listingTags = $this->Listing_tags;

        $listingTags['is_start_soon'] =  $this->start_soon;

        $opened_member_ships=($this->opened_member_ships)?json_decode($this->opened_member_ships):[];


        $feeAmount = $this->share_price->multiply($this->administrative_fees_percentage)->divide('100');
        $vatAmount = $feeAmount->multiply($this->vat_percentage)->divide('100');
        $beginnerLimit = money(config('accounting.beginner_limit_per_listing'));
        $max_non_professional=$beginnerLimit->add($beginnerLimit->multiply($this->administrative_fees_percentage / 100)->multiply(1 + $this->vat_percentage / 100));


        $data = [
            'id' => $this->id,
            'title' => $this->title,
            'target' => $this->target->formatByDecimal(),
            'target_fmt' => cleanDecimal(number_format($this->target->formatByDecimal())),
            'gross_yield' => $this->gross_yield,
            'dividend_yield' => $this->dividend_yield,
            'start_date' => $this->start_time,
            'administrative_fees_percentage' => $this->administrative_fees_percentage,
            'vat_percentage' => $this->vat_percentage,
            'final_listing_price' => money_sum($this->share_price, $feeAmount, $vatAmount)->formatByDecimal(),
            'total_fee_and_vat' => money_sum($feeAmount, $vatAmount)->formatByDecimal(),
            'max_non_professional'=>$max_non_professional->formatByDecimal(),
            'expected_net_return' => number_format($this->expected_net_return,2) ." ". " %",
            'deadline' => $this->deadline,
            'fund_duration' => $this->fund_duration ." ". trans("messages.month"),
            'min_inv_share' => $this->min_inv_share,
            'min_inv_amount' => $minInvestmentAmount = $this->share_price->multiply($this->min_inv_share)->formatByDecimal(),
            'min_inv_amount_fmt' => number_format($minInvestmentAmount, 2),
            'max_inv_share' => $this->max_inv_share,
            'total_shares' => $this->total_shares,
            'rent_amount' => $this->rent_amount,
            'created_at' => $this->created_at->format('Y-m-d h:i'),
            'updated_at' => $this->updated_at->format('Y-m-d h:i'),
            'city' => new CityResource($this->whenLoaded('city')),
            'type' => new PropertyTypeResource($this->whenLoaded('type')),
            'investment_type' => InvestmentType::getDescription($this->investment_type),
            'team' => $this->whenLoaded('details', function () {
                $item = $this->details->firstWhere('category', DetailCategory::Team);
                return $item ? new TeamResource($item['value']) : null;
            })
           ,
            'complete_percent' => $this->fund_percent,
            'property_details' => $this->whenLoaded('details', function () use ($locale) {
                $item = $this->details->firstWhere('category', DetailCategory::PropertyDetails);
                return $item ? [
                    'description' => $item->value->get('description_' . $locale),
                    'files' => $this->filterAttachments(AttachmentType::Details),
                ] : null;
            }),
            'location' =>$this->whenLoaded('details', function () use ($locale) {
                $item = $this->details->firstWhere('category', DetailCategory::Location);
                return $item ? [
                    'description' => $item->value->get('description_' . $locale),
                    'gmap_url' => $item->value->get('gmap_url'),
                    'files' => $this->filterAttachments(AttachmentType::Location),
                ] : null;
            }),
            'risks' =>$this->whenLoaded('details', function () use ($locale) {
                $item = $this->details->firstWhere('category', DetailCategory::Risks);
                return $item ? [
                    'content' => $item->value->get('content_' . $locale),
                    'files' => $this->filterAttachments(AttachmentType::Risks)
                ] : null;
            }),
            'timeline' => $this->whenLoaded('details', function () {
                $item = $this->details->firstWhere('category', DetailCategory::Timeline);
                return $item ? new ListingTimelineResource($item['value']) : null;
            }),
            'days_remaining' => $daysRemaining = now('Asia/Riyadh')->diffInDays($this->deadline, false),
            'hours_remaining' => now('Asia/Riyadh')->diffInHours($this->deadline, false),
            'financial_details' =>  $this->whenLoaded('details', function () use ($locale) {
                $item = $this->details->firstWhere('category', DetailCategory::FinancialDetails);
                return $item ? [
                    'content' => $item->value->get('content_' . $locale),
                    'files' => $this->filterAttachments(AttachmentType::FinancialDetails),
                ] : null;
            }),
            'due_diligence' =>  $this->whenLoaded('details', function () use ($locale) {
                $item = $this->details->firstWhere('category', DetailCategory::DueDiligence);
                return $item ? [
                    'content' => $item->value->get('content_' . $locale),
                    'files' => $this->filterAttachments(AttachmentType::DueDiligence),
                ] : null;
            }),
            'invested_shares' => $this->when(isset($this->pivot->invested_shares), function () {
                return $this->total_invested_shares;
            }),

            'invested_amount' => $this->when(isset($this->pivot->invested_shares), function () {
                return $this->total_invested_amount;
            }),
            'invested_amount_fmt' => $this->when(isset($this->pivot->invested_shares), function () {
                return clean_decimal(number_format($this->total_invested_amount, 2));
            }),

            'images' => ListingEditImagesResource::collection($images)
            ,
            'can_invest' => false,
            'investment_valid'=>$this->can_invest,
            'opened_member_ships'=>(!$this->is_closed && Carbon::parse($this->early_investment_date)->isBefore(now()))?$opened_member_ships:[],
            'unit_price' => number_format($this->share_price->formatByDecimal(), 2),
            'authorized_entity' => $this->whenLoaded('authorizedEntity', fn () => optional($this->authorizedEntity)->name),
            'is_completed' => $this->is_completed,
            'is_closed' => $this->is_closed,
        ];

        return array_merge($data, $listingTags);
    }

    public function AdjustProprty($category)
    {
        return $this->whenLoaded('details', function () use ($category) {
            $item = $this->details->firstWhere('category', $category);
            return $item ? $item['value']->toArray() : null;
        });
    }

    public function filterAttachments($type)
    {
        $appType = $this->type;

        return ListingFileResource::collection(
            $this->attachments->filter(fn ($item) => $item->extra_info->get('type') === $type)->values(),
            $appType
        );
    }

    public function getMinAndMaxInvestableShares($user)
    {
        if (!$user) {
            return [$this->min_inv_share, $this->max_inv_share];
        }

        $investor = $this->investors->firstWhere('id', $user->id);

        if (!$investor) {
            return [$this->min_inv_share, $this->max_inv_share];
        }

        $investedShares = $investor->pivot->invested_shares;
        $remainingShares = $this->max_inv_share - $investedShares;

        if ($investedShares < $this->min_share) {
            return [$this->min_inv_share, $remainingShares];
        }

        if ($investedShares < $this->max_inv_share) {
            return [1, $remainingShares];
        }

        return [0, 0];
    }
}
