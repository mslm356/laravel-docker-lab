<?php

namespace App\Services\Campaigns;

use App\Enums\Campaigns\CampaignStatus;
use App\Enums\Campaigns\CampaignUserStatus;
use App\Jobs\Campaigns\SetUsers;
use App\Jobs\Campaigns\UpdateUserStatus;
use App\Services\Campaigns\InvestorTypes\ManageData;
use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Mailgun\Mailgun;

class EmailType
{
    public $data;
    public $campaign;
    public $logService;

    public function __construct($data = null, $campaign = null)
    {
        $this->data = $data;
        $this->campaign = $campaign;
        $this->logService = new LogService();
    }

    public function handle()
    {
        $this->saveCampaignUsers();
    }

    public function saveCampaignUsers()
    {
        $setUsersJob = (new SetUsers($this->data, $this->campaign, 'Email'));
        dispatch($setUsersJob)->onQueue('campaigns');
    }

    public function sendCampaign($dataUsedToSend, $campaign)
    {

        // First, instantiate the SDK with your API credentials
        $mg = Mailgun::create(config('mailgun.apikey'));

        # Next, instantiate a Message Builder object from the SDK, pass in your sending domain.
        $batchMessage = $mg->messages()->getBatchMessage(config('mailgun.domain'));

        # Define the from address.
        $batchMessage->setFromAddress("me@example.com");


        $batchMessage->setSubject($campaign->title_ar);

        $text = $this->replaceVariablesWithText($campaign->description_en, $dataUsedToSend['varsInText']);

        $batchMessage->setTextBody($text);

        # Next, let's add a few recipients to the batch job.
        foreach ($dataUsedToSend['user_data'] as $data) {
            $batchMessage->addToRecipient($data['email'], $data);
        }

        // After 1,000 recipients, Batch Message will automatically post your message to the messages endpoint.

        // Call finalize() to send any remaining recipients still in the buffer.
        $batchMessage->finalize();

        $messageIds = $batchMessage->getMessageIds();

        $this->updateCampaign($campaign, $messageIds);
    }

    public function manageInvestorsData()
    {
        $selectedColumns = ['users.id', 'email'];
        $manageData = new ManageData();
        return $manageData->manageData($this->data, $selectedColumns);
    }

    public function handleData()
    {

        $data = $this->manageInvestorsData();

        $investorData = $data['investorData'];

        return [
            'investor_data' => $investorData,
            'variablesInText' => isset($data['extraSelectedColumn']) ? $data['extraSelectedColumn'] : [],
        ];
    }

    public function updateCampaign($campaign, $messageIds)
    {

        $campaign->status = CampaignStatus::Ended;
        $campaign->response = $messageIds;
        $campaign->save();

        $updateUsersStatusJob = (new UpdateUserStatus($this->campaign, 'Email'));
        dispatch($updateUsersStatusJob)
            ->onQueue('campaigns')
            ->delay(Carbon::now()->addMinutes(5));
    }

    public function updateCampaignUsersStatus($campaign)
    {

        $message_id = Arr::first(json_decode($campaign->response, true));

        $message_id = rtrim($message_id, '>');
        $message_id = ltrim($message_id, '<');

        if ($message_id == '') {
            return false;
        }

        $mg = Mailgun::create(config('mailgun.apikey'));

        $queryString = array(
//            'recipient'    => 'islam.mosa@beetleware.com',
            'message-id'=> $message_id,
        );

        $event = $mg->events()->get(config('mailgun.domain'), $queryString);

        $allowedStatus = ['rejected', 'delivered', 'failed'];

        foreach ($event->getItems() as $item) {

            if (!in_array($item->getEvent(), $allowedStatus)) {
                $this->logService->log(
                    'Campaigns-Email:update-users-status',
                    null,
                    ['event status not allowed', 'event' => $item->getEvent()]
                );
                continue;
            }

            $user = $campaign->users()->where('email', $item->getRecipient())->first();

            if (!$user) {
                $this->logService->log(
                    'Campaigns-Email:update-users-status',
                    null,
                    ['user not found', $user, 'Recipient' => $item->getRecipient()]
                );
                continue;
            }

            $user->pivot->status = $item->getEvent() == 'delivered' ? CampaignUserStatus::Sent : CampaignUserStatus::Failed  ;
            $user->pivot->response = json_encode($item->getDeliveryStatus());
            $user->pivot->save();
        }

    }

    public function replaceVariablesWithText($text, $variablesInText)
    {
        foreach ($variablesInText as $variable) {

            if (strpos($text, '{{'.$variable.'}}')) {
                $text = str_replace('{{'.$variable.'}}', '%recipient.' . $variable . '%', $text);
            }
        }

        return $text;
    }
}
