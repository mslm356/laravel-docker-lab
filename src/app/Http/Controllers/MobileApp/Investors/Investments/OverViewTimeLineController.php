<?php

namespace App\Http\Controllers\MobileApp\Investors\Investments;

use App\Http\Controllers\Controller;
use App\Services\Investors\OverViewTimeLineServices;
use App\Services\LogService;
use Illuminate\Http\Request;

class OverViewTimeLineController extends Controller
{

    public $overViewTimeLineServices;
    public $logServices;

    public function __construct()
    {
        $this->overViewTimeLineServices = new OverViewTimeLineServices();
        $this->logServices = new LogService();
    }

    public function index(Request $request)
    {
        try {
            $user = getAuthUser('api');
            $responseData = $this->overViewTimeLineServices->index($user);

            return apiResponse(200, __('mobile_app/message.success'), $responseData);

        } catch (\Exception $e) {
            $code = $this->logServices->log('OverViewTimeLine', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

    public function overViewStatistics(Request $request)
    {
        try {

            $user = getAuthUser('api');
            $responseData = $this->overViewTimeLineServices->getStatistics($user);

            return apiResponse(200, __('mobile_app/message.success'), $responseData);

        } catch (\Exception $e) {
            $code = $this->logServices->log('overViewStatistics', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }


    public function newGetStatistic(Request $request)
    {
        try {

            $user = getAuthUser('api');
            $responseData = $this->overViewTimeLineServices->newGetStatistic($user);

            return apiResponse(200, __('mobile_app/message.success'), $responseData);

        } catch (\Exception $e) {
            $code = $this->logServices->log('overViewStatistics', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' .  $code);
        }
    }

}
