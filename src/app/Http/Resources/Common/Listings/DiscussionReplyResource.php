<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Listings\DiscussionStatus;
use App\Enums\Role;
use App\Http\Resources\Common\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscussionReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'text' => $this->text,
            'status' => $this->status,
            'status_text' => DiscussionStatus::getDescription($this->status),
            'user' => new UserResource($this->user),
            'user_role' => $this->whenLoaded('userRole', function () {
                return Role::getDescription($this->userRole->name);
            }),
            'replier_name' => $this->replier_name,
            'replier_role' => $this->replier_role,
            'replier_role_locales' => $this->getTranslations('replier_role'),
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
