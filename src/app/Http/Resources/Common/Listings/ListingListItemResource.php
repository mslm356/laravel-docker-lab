<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Listings\ListingReviewStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'expected_net_return' => $this->expected_net_return,
            'fund_duration' => $this->fund_duration ,
            'target' => $this->target->formatByDecimal(),
            'gross_yield' => $this->gross_yield,
            'dividend_yield' => $this->dividend_yield,
            'authorized_entity_id' => $this->auth,
            'review_status' => [
                'value' => $this->review_status,
                'description' => ListingReviewStatus::getDescription($this->review_status)
            ]
        ];
    }
}
