<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorizedEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorized_entities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('cr_number');
            $table->text('description');
            $table->unsignedTinyInteger('status');
            $table->unsignedBigInteger('reviewer_id')->nullable();
            $table->text('reviewer_comment')->nullable();
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('creator_role_id')->nullable();
            $table->timestamps();

            $table->foreign('reviewer_id')->references('id')->on('users');

            $table->foreign('creator_id', 'authorized_entities_creator_id_foreign')
                ->references('id')->on('users');

            $table->foreign('creator_role_id', 'authorized_entities_creator_role_id_foreign')
                ->references('id')->on(config('permission.table_names.roles'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authorized_entities');
    }
}
