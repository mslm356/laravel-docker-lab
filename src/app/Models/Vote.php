<?php

namespace App\Models;

use App\Models\Listings\Listing;

class Vote extends AppModel
{
    protected $table = 'votes';

    public function listing()
    {
        return $this->belongsTo(Listing::class, 'fund_id');
    }

    public function InvestorVotes()
    {
        return $this->hasMany(InvestorVote::class, 'vote_id');
    }

}
