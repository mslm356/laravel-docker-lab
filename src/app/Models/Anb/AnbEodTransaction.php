<?php

namespace App\Models\Anb;

use App\Casts\Money\MoneyCast;
use App\Models\AppModel;
use App\Support\QueryScoper\HasScopes;
use Illuminate\Database\Eloquent\Builder;

class AnbEodTransaction extends AppModel
{
    use HasScopes;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('leftJoinCurrencyFromStatement', function (Builder $builder) {
            $transactionTable = (new AnbEodTransaction())->getTable();
            $statementTable = (new AnbEodStatement())->getTable();

            $builder->select("{$transactionTable}.*", "{$statementTable}.opening_balance->currency as currency")
                ->leftJoin($statementTable, "{$statementTable}.id", '=', "{$transactionTable}.statement_id");
        });
    }

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'value_date' => 'datetime',
        'amount' => MoneyCast::class . ':currency',
    ];
}
