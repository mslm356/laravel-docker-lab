<?php

namespace App\Console\Commands\Anb;

use App\Jobs\Anb\AnbEodStatement;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class GetEodStatements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:eod {--start-date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get EOD Statemnets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $nextDate = CarbonImmutable::createFromFormat(
            'Y-m-d',
            $this->option('start-date'),
            'Asia/Riyadh'
        )
            ->startOfDay();

        $today = now('Asia/Riyadh')->startOfDay();

        while ($nextDate->isBefore($today) || $nextDate->isSameDay($today)) {
            AnbEodStatement::dispatchSync($nextDate);
            $nextDate = $nextDate->addDay();
        }
    }
}
