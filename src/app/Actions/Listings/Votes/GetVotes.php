<?php

namespace App\Actions\Listings\Votes;

use App\Models\Payouts\Payout;
use App\Models\Listings\Listing;
use App\Models\Vote;
use Lorisleiva\Actions\Concerns\AsAction;

class GetVotes
{
    use AsAction;

    public function handle(Listing $listing)
    {
        return Vote::where('fund_id', $listing->id)->simplePaginate(10);
    }
}
