<?php

namespace App\Http\Resources\Admins\SubscriptionForms;

use App\Enums\SubscriptionForm\RequestStatus;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class RequestsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => [
                'description' => RequestStatus::getDescription($this->status),
                'value' => $this->status,
            ],
            'name' => optional($this->user)->full_name ,
            'user_id' => $this->user_id ,
            'nin' => optional($this->user)->nationalIdentity ?  optional($this->user->nationalIdentity)->nin : null,
            'reviewer_name' => optional($this->reviewer)->full_name,
            'reviewer_comment' => $this->reviewer_comment,
            'created_at' => Carbon::parse($this->created_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i'),
            'updated_at' => Carbon::parse($this->updated_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i'),
        ];
    }


}
