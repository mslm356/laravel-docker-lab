<?php

namespace App\Support\Elm\Nabaa\SmsStrategies;

use App\Models\Users\User;
use Illuminate\Support\Str;
use App\Models\Elm\NabaaMessage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use App\Support\Elm\Nabaa\Enums\NabaaMsgType;
use App\Support\Elm\Nabaa\Exceptions\VerificationSmsException;

class Fake
{
    const EXPIRED = 1000;
    const INVALID_MSG_TYPE = 1001;
    const INVALID_MSG = 1002;

    /**
     * Sign the given message
     *
     * @param string $nationalId
     * @param \App\Models\Users\User $user
     * @return \App\Models\Elm\NabaaMessage
     * @throws \App\Support\Elm\Nabaa\Exceptions\SendingSmsException
     */
    public function send($nationalId, User $user = null, $meta = [])
    {
        $verificationCode = '1973';

        $msg = NabaaMessage::create([
            'id' => (string) Str::uuid(),
            'type' => NabaaMsgType::SmsVerification,
            'batch_number' => (string) Str::uuid(),
            'status' => '1',
            'recipients' => $this->constructRecipients($nationalId, $user, $hashedCode = Hash::make($verificationCode)),
            'data' => array_merge(
                [
                    'nin' => $nationalId,
                    'code' => $hashedCode,
                    'expired_at' => null
                ],
                $meta
            )
        ]);

        return $msg;
    }

    protected function constructRecipients($nationalId, $user, $verificationCode)
    {
        return [
            [
                'NationalOrIqamaId' => $nationalId,
                'Language' => strtoupper($user ? $user->preferredLocale() : App::getLocale()),
                'Params' => [
                    'Param' => [
                        [
                            'Name' => 'VAR01',
                            'Value' => $verificationCode
                        ]
                    ]
                ],
            ]
        ];
    }

    public function verify($nationalId, NabaaMessage $msg, $enteredCode)
    {
        if (($msg->data['expired_at'] ?? null) !== null) {
            throw new VerificationSmsException($msg, static::EXPIRED);
        }

        if ($msg->type !== NabaaMsgType::SmsVerification) {
            throw new VerificationSmsException($msg, static::INVALID_MSG_TYPE);
        }

        if (($msg->data['nin'] ?? null) !== $nationalId) {
            throw new VerificationSmsException($msg, static::INVALID_MSG);
        }

        if (false === Hash::check($enteredCode, $msg->data['code'])) {
            return false;
        }

        $msg->update([
            'data->expired_at' => now()
        ]);

        return true;
    }
}
