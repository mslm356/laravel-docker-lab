<?php


namespace App\Services\Investors;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Users\User;
use Bavix\Wallet\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TransactionServices
{

    public function myTransactions($request, $user, $withPaginate = false)
    {
        $start_date = $request->has('start_date') ? Carbon::parse($request->start_date)->timezone('Asia/Riyadh')->format('Y-m-d') : null;
        $end_date = $request->has('end_date') ? Carbon::parse($request->end_date)->timezone('Asia/Riyadh') : null;

        $endDateHour = $end_date ? $end_date->format('H') : null;

        if ($endDateHour) {
            $end_date->addHour(23 - $endDateHour);
        }

        $wallet = $user->getWallet(WalletType::Investor);

        $transactions = $wallet->transactions()->where('confirmed', true);

        if ($request->has('start_date') && $request->start_date != null) {
            $transactions->whereDate('created_at', '>=', $start_date);
        }

        if ($request->has('end_date') && $request->end_date != null) {
            $transactions->whereDate('created_at', '<=', $end_date);
        }

        if ($request->has('type') && $request->type == 'deposit') {
            $transactions->whereIn('reason', [TransactionReason::SdadReason, TransactionReason::AnbStatement]);
        }

        if ($request->has('type') && $request->type == 'withdraw') {
            $transactions->where('reason', TransactionReason::AnbExternalTransfer);
        }

        if ($request->has('type') && $request->type == 'invest') {
            $transactions->whereIn('reason', [TransactionReason::InvestingInListing, TransactionReason::InvestingInListingAdminFee, TransactionReason::InvestingInListingVatOfAdminFee]);
        }

        if ($request->has('type') && $request->type == 'distribution') {
            $transactions->where('reason', TransactionReason::PayoutDistribution);
        }

        $paginate = $withPaginate ? 'paginate' : 'simplePaginate';

        $transactions = tap($transactions->orderBy('id', 'desc')->$paginate())->map(function ($transaction) use ($wallet) {
            return $transaction->setRelation('wallet', $wallet);
        });

        return $transactions;
    }

    public function getMyInvestingRecords($request, $user)
    {
        if (!$user) {
            throw ValidationException::withMessages(['vid' => __('messages.invalid_user'),]);
        }

        $investingRecords = $user->investingRecords();
        if ($request->has('start_date') && $request->start_date != null) {
            $investingRecords = $investingRecords->where('created_at', '>=', $request->start_date);
        }

        if ($request->has('end_date') && $request->end_date != null) {
            $investingRecords = $investingRecords->where('created_at', '<=', $request->end_date);
        }

        if ($request->has('amount') && $request->amount != null) {
            $investingRecords = $investingRecords->where('amount_without_fees', $request->amount * 100);
        }

        return $investingRecords;
    }

    public function getBalanceInquiry($user)
    {
        $data = [];
        $data['wallet'] = $user->getWallet(WalletType::Investor);

        $data['total'] = money($data['wallet']->balance)->formatByDecimal();
        $data['wallet_status'] =$data['wallet']->status;

        $data['total_withdrawal']=0;

        return $data;
    }

    public function myInvestingRecordsResource($record, $guard)
    {

        return [
            'id' => $record->id,
            'listing' => optional($record->listing)->title,
            'amount_without_fees' => $record->amount_without_fees->formatByDecimal(),
            'vat_amount' => $record->vat_amount->formatByDecimal(),
            'admin_fees' => $record->admin_fees->formatByDecimal(),
            'zatca_invoice' => $this->getZatcaFilePath($record),
            'subscription_form' => $this->getSubscriptionFilePath($record),
            'zatca_invoice_link' => $this->zatcaInvoiceLink($record->zatcaInvoice, $guard),
            'subscription_form_link' => $this->subscriptionFormLink($record->subscriptionForm, $guard),
            'created_at' => $record->created_at ? $record->created_at->timezone('Asia/Riyadh')->format('Y-m-d h:i:s A') : null,
        ];
    }

    public function getZatcaFilePath($record)
    {

        if ($record->zatcaInvoice && $record->zatcaInvoice->pdfFile) {
            return $record->zatcaInvoice->pdfFile->temp_full_path;
        }

        return null;
    }

    public function getSubscriptionFilePath($record)
    {

        if ($record->subscriptionForm) {
            return $record->subscriptionForm->temp_full_path;
        }

        return null;
    }

    public function subscriptionFormLink($subscriptionForm, $guard)
    {

        if (!$subscriptionForm) {
            return null;
        }

        return $guard == 'api' ? url('api/app/files/' . $subscriptionForm->id . '/get-url') : route('investor.files.get.url', ['file'=> $subscriptionForm->id]);
    }

    public function zatcaInvoiceLink($zatcaInvoice, $guard)
    {

        if (!$zatcaInvoice || !$zatcaInvoice->pdfFile) {
            return null;
        }

        $pdfFile = $zatcaInvoice->pdfFile;

        return $guard == 'api' ? url('api/app/files/' . $pdfFile->id . '/get-url') : route('investor.files.get.url', ['file' => $pdfFile->id]) ;
    }

}
