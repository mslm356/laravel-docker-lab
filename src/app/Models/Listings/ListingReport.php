<?php

namespace App\Models\Listings;

use App\Models\AppModel;
use App\Models\Traits\HasFiles;
use App\Casts\LocalizedAttribute;
use App\Models\Users\User;

class ListingReport extends AppModel
{
    use HasFiles;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'title' => LocalizedAttribute::class,
        'description' => LocalizedAttribute::class,
    ];

    /**
     * Get listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
