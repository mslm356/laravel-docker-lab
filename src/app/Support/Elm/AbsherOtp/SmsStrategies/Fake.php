<?php

namespace App\Support\Elm\AbsherOtp\SmsStrategies;

use App\Models\Users\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\Elm\AbsherOtp;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;

class Fake
{
    const EXPIRED = 1000;
    const INVALID_MSG_TYPE = 1001;
    const INVALID_MSG = 1002;

    /**
     * Sign the given message
     *
     * @param string $nationalId
     * @param \App\Models\Users\User $user
     * @return \App\Models\Elm\AbsherOtp
     * @throws \App\Support\Elm\AbsherOtp\Exceptions\SendingSmsException
     */
    public function send($nationalId, User $user, $meta = [])
    {
        $verificationCode = '1973';

        $msg = AbsherOtp::create([
            'id' => (string) Str::uuid(),
            'user_id' => $user->id,
            'customer_id' => $nationalId,
            'transaction_id' => (string) Str::uuid(),
            'verification_code' => Hash::make($verificationCode),
            'code' => substr($verificationCode, 0, 4) ,
            'data' => array_merge(
                [
                    'response' => [
                        'status' => 0,
                        'client_id' => Str::random(),
                        'customer_id' => $nationalId,
                    ],
                ],
                Arr::except($meta, 'reason')
            )
        ]);

        return $msg;
    }

    public function verify($nationalId, AbsherOtp $msg, $enteredCode)
    {
        if (($msg->expired_at ?? null) !== null) {
            throw new VerificationSmsException($msg, static::EXPIRED);
        }

        if ($msg->customer_id !== $nationalId) {
            throw new VerificationSmsException($msg, static::INVALID_MSG);
        }
        if (false === Hash::check($enteredCode, $msg->verification_code)) {
            return false;
        }
        $msg->delete();

        //        $msg->update([
        //            'expired_at' => now()
        //        ]);

        return true;
    }

    public function sendToAbsher(User $user, $meta = [], $delay, $nin=null, $code = null)
    {

        $verificationCode = '1973';

        $nationalId = $nin ?? $user->nationalIdentity->nin;

        $response = new \stdClass();

        $response->status = 0;
        $response->verificationCode = $verificationCode;
        $response->transactionId = (string) Str::uuid();
        $response->clientId = Str::random();
        $response->customerId = $nationalId;

        $data = [
            'request'=> [],
            'response'=> $response,
        ];

        if ($delay >= 1) {
            sleep(3);
        }

        return $data;
    }
}
