
<style>


    body {
        padding: 0;
        margin: 0;
    }

    html {
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: none;
    }

    @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
        *[class="table_width_100"] {
            width: 96% !important;
        }

        *[class="border-right_mob"] {
            border-right: 1px solid #dddddd;
        }

        *[class="mob_100"] {
            width: 100% !important;
        }

        *[class="mob_center"] {
            text-align: center !important;
        }

        *[class="mob_center_bl"] {
            float: none !important;
            display: block !important;
            margin: 0px auto;
        }

        .iage_footer a {
            text-decoration: none;
            color: #929ca8;
        }

        img.mob_display_none {
            width: 0px !important;
            height: 0px !important;
            display: none !important;
        }

        img.mob_width_50 {
            width: 40% !important;
            height: auto !important;
        }
    }

    .table_width_100 {
        width: 680px;
    }

    .btn-red {
        background: #e41420;
        color: #fff;
        text-transform: capitalize;
        min-width: 100px;
        font-weight: bold;
    }

    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
</style>


<div id="mailsub" class="notification" align="center">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;min-height:635px">
        <tr>
            <td align="center" bgcolor="#eff3f8">

                <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%"
                       style="max-width: 680px; min-width: 300px;">
                    <tr>
                        <td>
                            <!-- padding -->
                        </td>
                    </tr>
                    <!--header -->
                    <tr>
                        <td align="center" bgcolor="#ffffff">
                            <!-- padding -->
                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <div style="height: 30px; line-height: 30px; font-size: 10px;"></div>

                            @include('emails.partials.header')
                                <!--header END-->

                                <!--content 1 -->
                                <tr>
                                    <td align="center" bgcolor="#fbfcfd" style="{{app()->getLocale() == 'ar' ? 'direction: rtl;':'direction: ltr;'}}">
                                        <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e"
                                              style="font-size: 34px;">
                                <span style="font-family: Arial, Helvetica, sans-serif; font-size: 19px; color: #57697e; font-weight: 600;">

                                    {{__('emails/investors/new_transaction.welcome_message', ['name'=> $name])}} <br>


                                    {{__('emails/investors/new_transaction.message')}}
                                    ( {{$description}} ) <br>

                                   {{__('emails/investors/new_transaction.amount', ['amount'=> $amount])}}
                                     <br>

                                    {{__('emails/investors/new_transaction.link_text')}}<br>

<br>
                                      <a href="{{$profileUrl }}" target="_blank" style=" color: #f9f9f9;background: #1a2e44; padding: 11px;border-radius: 13px;
                                      text-decoration: none;">{{__('emails/investors/new_transaction.view')}}</a>

                                    <br>
<br>
                                     @include('emails.partials.social_links')

                                   </span>
                                        </font>
                                    </td>
                                </tr>
                                <!--content 1 END-->


                                <!--footer -->
                            @include('emails.partials.footer')

                                <!--footer END-->
                                <tr>
                                    <td>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>











{{--@component('mail::message')
{{ __('emails/common.greetings', ['name' => $name]) }}<br>

{{ __('emails/investors/new-transaction.message') }}

@component('mail::panel')
{{ $description }}<br>
{{ __('emails/investors/new-transaction.amount', ['amount' => $amount]) }}
@endcomponent

{{ __('emails/common.thanks') }}<br>
{{ config('app.name') }}
@endcomponent--}}
