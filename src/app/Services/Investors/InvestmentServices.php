<?php


namespace App\Services\Investors;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\CheckIfCanInvest;
use App\Actions\Listings\GetCurrentInvestments;
use App\Enums\Banking\WalletType;
use App\Models\Listings\Listing;
use App\Models\Urway\UrwayTransaction;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InvestmentServices
{

    public function shareAmount(Request $request, Listing $listing, $user)
    {
        $amountAndFees = CalculateInvestingAmountAndFees::run($listing, $request->input('shares'));

        if ($request->has('guard') && $request->guard == 'api') {
            return [
                'amount_fmt' => number_format($amountAndFees['total']->formatByDecimal(), 2),
                'amount' => $amountAndFees['total']->formatByDecimal(),
                'remaining_amount_to_invest' => 0
            ];

        }

        $wallet = $user->getWallet(WalletType::Investor);
        $diffOfBalanceAndTotal = money($wallet->balance)->subtract($amountAndFees['total']);
        $remainingToInvest = $diffOfBalanceAndTotal->greaterThanOrEqual(money('0')) ? money(0) : $diffOfBalanceAndTotal->absolute();

        $result = [
            'amount_fmt' =>
                number_format(
                    $amountAndFees['total']->formatByDecimal(),
                    2
                ),
            'amount' => $amountAndFees['total']->formatByDecimal(),
            'remaining_amount_to_invest' => cleanDecimal(
                number_format(
                    $remainingToInvest->formatByDecimal(),
                    2
                )
            ),
        ];

        return $result;
    }

    public function createSubscriptionToken(Request $request, Listing $listing)
    {
        $data = $request->validate(['shares' => ['required', 'integer', 'min:1']]);

        $case = CheckIfCanInvest::run(
            $listing,
            $request->user('sanctum'),
            GetCurrentInvestments::run($listing)->get(),
            $data['shares']
        );

        return $case;

    }

    public function handle(User $user, Listing $listing, $shares, $payment_type = null)
    {
        return InvestorSubscriptionToken::create([
            'user_id' => $user->id,
            'listing_id' => $listing->id,
            'shares' => $shares,
            'reference' => Str::random(25),
            'payment_type' => $payment_type ? $payment_type : 'wallet',
            'is_expire' => false,
        ]);
    }

    public function createUrwayTransaction($listing, $subscriptionToken)
    {
        $toBeInvestedAmountWithFees = CalculateInvestingAmountAndFees::run($listing, $subscriptionToken->shares)['total'];

        UrwayTransaction::create([
            'subscription_id'=> $subscriptionToken->id,
            'amount'=> $toBeInvestedAmountWithFees->getAmount()
        ]);
    }
}
