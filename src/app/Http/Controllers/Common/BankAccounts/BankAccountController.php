<?php

namespace App\Http\Controllers\Common\BankAccounts;

use App\Mail\Investors\newBankAccount;

use App\Models\BankAccount;
use App\Models\Users\User;
use App\Services\Files\CheckFile;
use App\Services\LogService;
use App\Traits\SendMessage;
use Illuminate\Http\Request;
use App\Enums\BankAccountStatus;
use App\Enums\Banking\BankCode;
use App\Enums\FileType;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Common\BankAccounts\BankAccountListItemResource;
use App\Models\File;
use App\Rules\VerifyIban;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class BankAccountController extends Controller
{
    use SendMessage;

    public $logService;
    public $checkFile;

    public function __construct()
    {
        $this->middleware(['user-can-access:bankAccount:web'], ['only' => ['store']]);
        $this->logService = new LogService();
        $this->checkFile = new CheckFile();
    }

    /**
     * Get the bank accounts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $user = $request->user('sanctum');

        if ($request->is('api/investor/*')) {
            $bankAccounts = $user->bankAccounts()->simplePaginate();
        } elseif ($request->is('api/authorized-entity/*')) {
            $bankAccounts = authorizedEntity()->bankAccounts()->simplePaginate();
        } else {
            abort(404);
        }
        return BankAccountListItemResource::collection($bankAccounts);
    }

    /**
     * Create new bank account
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $user = $request->user('sanctum');

            if ($user->investor && $user->investor->status != 3) {
                return response()->json(['data' => ['message' => trans("messages.identity_verified_valid")]], 400);
            }

            if (is_array($request['attachments']) && count($request['attachments']) != 1) {
                return apiResponse(400, __('files count not valid'));
            }

            $inputs = $this->validate($request, [
                'name' => ['required', 'string', 'max:100',textValidation()],
                'iban' => ['required', 'string'/*,'unique:bank_accounts,iban'*/, new VerifyIban],
                'bank' => ['required', 'string', new EnumValue(BankCode::class)],
                'attachments' => ['required'/*, 'clamav'*/, 'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/hief,image/heic', 'max:4096']
            ]);

            $this->checkFile->handle($request->file('attachments'), null);

            $pendingBankAccount = BankAccount::query()
                ->where('accountable_id', $user->id)
                ->where('accountable_type', User::class)
                ->whereIn('status', [ BankAccountStatus::InReview, BankAccountStatus::PendingReview])
                ->count();

            if ($pendingBankAccount) {
                throw ValidationException::withMessages(['iban' => __('messages.you_have_pending_bank_account'),]);
            }

            $repeatedIban = BankAccount::query()
                ->where('accountable_id', $user->id)
                ->where('accountable_type', User::class)
                ->whereIn('status', [BankAccountStatus::Approved, BankAccountStatus::InReview, BankAccountStatus::PendingReview])
                ->where('iban', $inputs['iban'])
                ->count();

            if ($repeatedIban) {
                throw ValidationException::withMessages(['iban' => __('messages.repeated_iban'),]);
            }

            $data = [
                'name' => $inputs['name'],
                'iban' => $inputs['iban'],
                'bank' => $inputs['bank'],
                'status' => BankAccountStatus::PendingReview,
            ];

            return DB::transaction(function () use ($request, $inputs, $data, $user) {

                if ($request->is('api/investor/*')) {
                    $bankAccount = $user->bankAccounts()->create($data);
                    // send mail
                    if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                        Mail::to($user)->queue(new newBankAccount($user));
                    }

                    $this->sendSms($user);

                } elseif ($request->is('api/authorized-entity/*')) {
                    $bankAccount = $user->authorizedEntity->bankAccounts()->create($data);
                } else {
                    abort(404);
                }

                if (isset($inputs['attachments'])) {
                    File::upload($inputs['attachments'], 'bank-accounts/' . $bankAccount->id, [
                        'fileable_id' => $bankAccount->id,
                        'fileable_type' => get_class($bankAccount),
                        'type' => FileType::BankAccountAttachment
                    ]);
                }

                return response()->json();
            });

        } catch (ValidationException $e) {
            $code = $this->logService->log('CREATE-BANK-ACCOUNT-VALIDATION', $e);
            return response()->json(['message' => $e->getMessage() . ' : ' . $code, 'errors' => $e->errors()], 400);
        } catch (\Exception $e) {
            $code = $this->logService->log('CREATE-BANK-ACCOUNT-WEB', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later' . ' : ' . $code));
        }
    }

    public function sendSms($user)
    {
        $name = $user ? $user->full_name : '---';
        $content_ar_1 = "  تم تلقي طلبكم لإضافة حسابكم البنكي في منصة أصيل إلى المحفظة الشخصية سوف يتم تبليغكم في حال الرد من قبل الفريق المختص شكراً لتعاملك مع منصة أصيل المالية ";
        $welcome_message_ar = '  مستثمرنا الكريم:  ' . $name;
        $content_ar = $welcome_message_ar . $content_ar_1;
        $content_en =  "Our dear investor ". $name ." Your request to add your bank account has been received, and the team will notify you once it is reviewed Thank you for dealing with Aseel platform" ;
        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);

    }
}
