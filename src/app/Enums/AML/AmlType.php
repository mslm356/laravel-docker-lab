<?php

namespace App\Enums\AML;

use BenSampo\Enum\Enum;

final class AmlType extends Enum
{
    const AutoAssessment =   1;
    const ReAssessment =   2;
}
