<?php

namespace App\Services\Sdad\Enums;

class InvoiceCodesStatusesEnums
{

    const CREATED = 16;
    const PAID = 19;
    const EXPORT= 32;
    const EXPIRED= 37;
    const EXPIRED_VALIDITY= 526;

    public static function all(): array
    {
        return [
            self::CREATED,
            self::PAID,
            self::EXPORT,
            self::EXPIRED,
            self::EXPIRED_VALIDITY,

        ];
    }

    public static function status_name(int $status_enum): ?string
    {
        switch ($status_enum) {
            case self::CREATED :
                $status_name = 'Created';
                break;
            case self::PAID :
                $status_name = 'Paid';
                break;
            case self::EXPORT :
                $status_name = 'Export';
                break;
            case self::EXPIRED :
                $status_name = 'Expired';
                break;
            case self::EXPIRED_VALIDITY :
                $status_name = 'Expired Validity';
                break;
            default:
                $status_name = null;

        }
        return $status_name;
    }

    public static function status_id(int $status_enum): ?string
    {
        switch ($status_enum) {
            case self::CREATED :
                $status_name = 17;
                break;
            case self::PAID :
                $status_name = 20;
                break;
            case self::EXPORT :
                $status_name = 34;
                break;
            case self::EXPIRED :
                $status_name = 39;
                break;
            case self::EXPIRED_VALIDITY :
                $status_name = 5441;
                break;
            default:
                $status_name = null;

        }
        return $status_name;
    }
}
