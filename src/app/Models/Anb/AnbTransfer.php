<?php

namespace App\Models\Anb;

use App\Models\AppModel;
use App\Models\BankAccount;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;

class AnbTransfer extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'initiator_id', 'id');
    }

    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id', 'id');
    }
}
