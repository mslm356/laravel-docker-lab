<?php

namespace App\Enums\MemberShips;

use BenSampo\Enum\Enum;

final class MemberShipShowPopup extends Enum
{
    const Hide = 0;
    const Show = 1;
}
