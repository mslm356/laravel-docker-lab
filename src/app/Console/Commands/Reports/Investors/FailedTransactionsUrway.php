<?php

namespace App\Console\Commands\Reports\Investors;

use App\Exports\Investors\DepositReportExport;

use App\Exports\Investors\OlderThan18;
use App\Exports\Wallets\FailedTransactionUrway;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class FailedTransactionsUrway extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:failed-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get investors investor-older-18';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Excel::store(
            new FailedTransactionUrway(),
            "mgmnt-reports/investor.xlsx",
        );
    }
}
