<?php

namespace App\Http\Resources\Investors\Listings;

use App\Enums\InvestmentType;
use App\Services\MemberShip\SpecialCanInvest;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ListingListItemResource extends JsonResource
{

    public $specialCanInvest;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $this->specialCanInvest = new SpecialCanInvest();

        $user = Auth::guard('sanctum')->check() ? Auth::guard('sanctum')->user() : null;

        $listingTags = $this->Listing_tags;

        $listingTags['is_start_soon'] = $this->specialCanInvest->handle($this, $user) ? false : $this->start_soon;

        $images=['id'=>$this->id,'name'=>$this->title,'url'=>Storage::disk(config('filesystems.public'))->url($this->default_image)];
        $data = [

            'id' => $this->id,
            'title' => $this->title,
            'target' => $this->target->formatByDecimal(),
            'target_fmt' => cleanDecimal(number_format($this->target->formatByDecimal())),
            'min_inv_amount' => $minInvestmentAmount = number_format($this->share_price->multiply($this->min_inv_share)->formatByDecimal()),
            'min_inv_amount_fmt' => $minInvestmentAmount,
            'city' =>(App::currentLocale() == 'ar')?$this->city_name_ar:$this->city_name_en,
            'type' =>(App::currentLocale() == 'ar')?$this->property_type_ar:$this->property_type_en,
            'start_date' => $this->start_time,
            'administrative_fees_percentage' => $this->administrative_fees_percentage,
            'expected_net_return' => number_format($this->expected_net_return, 2) . " " . " %",
            'deadline' => $this->deadline,
            'fund_duration' => $this->fund_duration . " " . trans("messages.month"),
            'investment_type' => InvestmentType::getDescription($this->investment_type),
            'complete_percent' => $this->fund_percent,
            'images'=>($this->default_image)?[$images]:[],
            'can_invest' => $this->specialCanInvest->handle($this, $user),
            'authorized_entity' => $this->whenLoaded('authorizedEntity', fn () => optional($this->authorizedEntity)->name),
            'is_completed' => $this->is_completed,
            'is_closed' => $this->specialCanInvest->handle($this, $user) ? false : true,
        ];

        return array_merge($data, $listingTags);
    }
}
