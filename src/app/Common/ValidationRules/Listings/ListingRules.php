<?php

namespace App\Common\ValidationRules\Listings;

use App\Models\BankAccount;
use App\Enums\InvestmentType;
use Illuminate\Validation\Rule;
use App\Enums\BankAccountStatus;
use App\Models\AuthorizedEntity;
use App\Rules\Listings\MaxTotalShares;
use App\Rules\NumericString;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;

class ListingRules
{
    static public function getBankAccountIdRuleForAdmin(Request $request)
    {
        return [
            'listing.bank_account_id' => [
                'nullable',
                'integer',
                Rule::exists(BankAccount::class, 'id')
                    ->where('status', BankAccountStatus::Approved)
                    ->where('accountable_id', $request->input('listing.authorized_entity_id'))
                    ->where('accountable_type', (new AuthorizedEntity())->getMorphClass())
            ],
        ];
    }

    static public function getBankAccountIdRuleForAuthorizedEntity()
    {
        return [
            'listing.bank_account_id' => [
                'nullable',
                'integer',
                Rule::exists(BankAccount::class, 'id')
                    ->where('status', BankAccountStatus::Approved)
                    ->where('accountable_id', authorizedEntityId())
                    ->where('accountable_type', (new AuthorizedEntity())->getMorphClass())
            ],
        ];
    }

    static public function getAdminListingRule($listing = null)
    {
        return [
            'listing.work_date' => ['nullable' , 'string',/* 'date_format:Y-m-d'*/],
            'listing.early_investment_date' => ['nullable', 'date', 'before:listing.start_at'],
            'listing.opened_member_ships' => ['nullable'],
        ];

    }

    static public function get($listing = null)
    {
        $regex = '/^(https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i';
        return [
            'listing.title_en' => ['required', 'string', 'max:200',textValidation(), $listing ? 'unique:listings,title_en,' . $listing->id . 'id' : 'unique:listings,title_en'],
            'listing.title_ar' => ['required', 'string', 'max:200',textValidation(), $listing ? 'unique:listings,title_ar,' . $listing->id . 'id' : 'unique:listings,title_ar'],
            'listing.city_id' => ['required', 'integer', 'exists:cities,id'],
            'listing.type_id' => ['required', 'integer', 'exists:property_types,id'],
            'listing.target' => ['required', new NumericString, 'min:0'],
            'listing.gross_yield' => ['required', 'numeric', 'min:0'],
            'listing.dividend_yield' => ['required', 'numeric', 'min:0'],
            'listing.deadline' => ['required', 'string', 'date_format:Y-m-d', 'after:listing.start_at'],
            'listing.start_at' => ['nullable', 'date'],
            'listing.administrative_fees_percentage' => ['numeric','min:0'],
            'listing.fund_duration' => ['nullable', 'numeric'],
            'listing.expected_net_return' => ['nullable', 'numeric' ],
            'listing.is_completed' => ['nullable', 'boolean'],
            'listing.total_shares' => ['required', 'integer', 'min:0', new MaxTotalShares('listing.target')],
            'listing.reserved_shares' => ['required', 'integer', 'min:0'],
            'listing.min_inv_share' => ['required', 'integer', 'min:0','lte:listing.max_inv_share','lt:listing.total_shares'],
            'listing.max_inv_share' => ['required', 'integer', 'gte:listing.min_inv_share','lte:listing.total_shares',],
            'listing.rent_amount' => ['required', 'numeric', 'min:0'],
            'listing.is_closed' => ['required', 'boolean'],
            'listing.is_visible' => ['required', 'boolean'],
            'listing.investment_type' => ['required', new EnumValue(InvestmentType::class, false)],

            'due_diligence.content_en' => [
                'nullable', 'required_with:due_diligence.*.content_ar', 'string'
            ],
            'due_diligence.content_ar' => [
                'nullable', 'required_with:due_diligence.*.content_en', 'string'
            ],

            'property_details.description_en' => ['required', 'string'],
            'property_details.description_ar' => ['required', 'string'],
            'location.description_en' => ['required', 'string'],
            'location.description_ar' => ['required', 'string'],
            'location.gmap_url' => ['required', 'string', 'url'],

            'terms_conditions.description_en' => ['required', 'string'],
            'terms_conditions.description_ar' => ['required', 'string'],

            'financial_details.content_en' => [
                'nullable', 'required_with:financial_details.content_ar', 'string'
            ],
            'financial_details.content_ar' => [
                'nullable', 'required_with:financial_details.content_en', 'string'
            ],

            'risks.content_en' => [
                'nullable', 'required_with:risks.content_ar', 'string'
            ],
            'risks.content_ar' => [
                'nullable', 'required_with:risks.content_en', 'string'
            ],

            'team.*.name_en' => ['required', 'string','max:500'],
            'team.*.name_ar' => ['required', 'string','max:500'],
            'team.*.members.*.name_en' => ['required', 'string'],
            'team.*.members.*.name_ar' => ['required', 'string'],
            'team.*.members.*.bio_en' => ['nullable','max:5000', 'string'],
            'team.*.members.*.bio_ar' => ['nullable','max:5000', 'string'],
            'team.*.members.*.link1' => ['nullable','regex:' . $regex, 'string'],
            'team.*.members.*.link2' => ['nullable','regex:' . $regex, 'string'],
            'team.*.members.*.job_title_en' => ['nullable','required', 'string'],
            'team.*.members.*.job_title_ar' => ['nullable','required', 'string'],

            'timeline.*.title_en' => ['required', 'string'],
            'timeline.*.title_ar' => ['required', 'string'],
            'timeline.*.description_en' => ['nullable', 'string'],
            'timeline.*.description_ar' => ['nullable', 'string'],
            'timeline.*.date' => ['required', 'date'],
        ];
    }
}
