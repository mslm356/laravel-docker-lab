<?php

return [
    'subject' => 'Your IBAN adding request has been approved',
    'welcome_message' => 'Our dear investor  :name',
    'message_1' => 'The bank account has been added to your personal wallet',
    'message_2' => 'Aseel platform wishes you all the best',
    'message_3' => 'Thank you for dealing with Aseel Platform',
    'link_text' => 'To view your control panel, please go to the link',
    'view' => 'View control panel',
];
