<?php

namespace App\Mail\Investors;

use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeToAseel extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/sign-up/profile';

        return $this->view('emails.investors.welcome', compact('profileUrl'))
            ->subject(__('emails/investors/welcome.subject'));
    }
}
