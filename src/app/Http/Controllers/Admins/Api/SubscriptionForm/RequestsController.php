<?php

namespace App\Http\Controllers\Admins\Api\SubscriptionForm;

use App\Actions\Admin\SubscriptionForm\RequestUpdateStatus;
use App\Actions\Investors\SubscriptionForm\ExportInvestmentSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\SubscriptionForm\GetAllSubscriptionFormData;
use App\Enums\SubscriptionForm\RequestStatus;
use App\Http\Resources\Admins\SubscriptionForms\RequestsResource;
use App\Mail\SubscriptionForm\ApproveRequest;
use App\Models\SubscriptionFormRequest;
use App\Models\Users\User;
use App\Services\LogService;
use App\Support\Users\FilterServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Mail;

class RequestsController extends Controller
{
    public $logService;
    public $filterService;

    public function __construct()
    {
        $this->logService = new LogService();
        $this->filterService = new FilterServices();
    }

    public function index(Request $request)
    {
        try {

            $subscriptionRequests = $this->filterService->filterWithSubscriptionForm($request);
            $subscriptionRequests = $subscriptionRequests->orderBy('id', 'desc')->paginate();
            return RequestsResource::collection($subscriptionRequests);

        } catch (\Exception $e) {
            $this->logService->log('ADMIN-SUBSCRIPTION-FORM', $e);
            return response()->json(['message' => 'sorry please try later'], 400);
        }
    }

    /**
     * Get Investors profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($subscriptionFormRequest)
    {
        $request = SubscriptionFormRequest::find($subscriptionFormRequest);
        if (!$request) {
            return response()->json(['message' => __('messages.resource_cannot_be_found'),], 400);
        }

        return new RequestsResource($request);
    }

    /**
     * Update investor status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, $subscriptionFormRequest)
    {

        $this->validate($request, [
            'status' => ['required', 'integer', new EnumValue(RequestStatus::class, false)],
            'comment' => ['nullable', 'string', 'max:1000',textValidation()]
        ]);

        $subscriptionFormRequest = SubscriptionFormRequest::where('id', $subscriptionFormRequest)
            ->whereIn('status', [RequestStatus::PendingReview, RequestStatus::InReview])
            ->first();

        if (!$subscriptionFormRequest) {
            return response()->json(['message' => __('messages.resource_cannot_be_found'),], 400);
        }

        $user = $subscriptionFormRequest->user;

        $prepared_data = [
            'status' => $request->status,
            'reviewer_comment' => $request->reviewer_comment,
            'reviewer_id' => $request->user('sanctum')->id,
            'file_password' => $request->file_password,
        ];

        RequestUpdateStatus::run($user, $prepared_data, $subscriptionFormRequest);

        return apiResponse(200, __('Done successfully'));

    }

}
