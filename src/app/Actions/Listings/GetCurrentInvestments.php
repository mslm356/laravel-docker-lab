<?php

namespace App\Actions\Listings;

use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestor;
use Lorisleiva\Actions\Concerns\AsAction;

class GetCurrentInvestments
{
    use AsAction;

    public function handle(Listing $listing)
    {
        return ListingInvestor::where('listing_id', $listing->id);
    }
}
