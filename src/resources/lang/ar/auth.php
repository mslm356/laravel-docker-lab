<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'البيانات غير صحيحة',
    'throttle' => 'لقد تجاوزت الحد المسموح للمحاولة. من فضلك قم بالمحاولة بعد :seconds ثانية',
    'throttle_with_minute' => 'لقد تجاوزت الحد المسموح للمحاولة. من فضلك قم بالمحاولة بعد :minutes دقيقة',

];
