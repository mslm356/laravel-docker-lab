<?php

namespace App\Http\Controllers\Investors\Auth;

use App\Services\Investors\InvestorProfileServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

class UpdateInvestorProfile extends Controller
{

    public $investorProfileServices;

    public function __construct()
    {
        $this->investorProfileServices = new InvestorProfileServices();
    }

    protected $redirectTo = RouteServiceProvider::HOME;


    public function __invoke(Request $request)
    {
        $data = $this->investorProfileServices->validator($request->all())->validate();
        $user=$request->user();

        if ($this->investorProfileServices->checkIqamaExpire($user)) {
            return apiResponse(400, __('mobile_app/message.iqama_date_expire'));
        }

        $this->investorProfileServices->create($data, $user);

        return response()->json(['kyc_suitability'=>getKycSuitability($user->investor, $user)], 201);
    }

    public function confirmUnsuitability(Request $request)
    {
        $this->investorProfileServices->confirmUnsuitability($request->user());
        return apiResponse(200, '');
    }
}
