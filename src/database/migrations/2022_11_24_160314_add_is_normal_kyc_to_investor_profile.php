<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

 use Illuminate\Support\Facades\DB;
class AddIsNormalKycToInvestorProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->boolean('is_normal_kyc_data')->default(0)->after('is_kyc_completed');
        });

        // this for old investor make data is visible else if will apply restriction on it this,then must change
        DB::statement('update investor_profiles set is_normal_kyc_data = 1');
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->dropColumn('is_normal_kyc_data');

        });
    }
}
