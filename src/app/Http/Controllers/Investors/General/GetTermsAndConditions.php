<?php

namespace App\Http\Controllers\Investors\General;

use App\Http\Controllers\Controller;
use App\Models\WebsiteContent;
use Illuminate\Support\Facades\App;

class GetTermsAndConditions extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $content = WebsiteContent::where('name', 'terms-and-conditions')->first();

        return response()->json([
            'data' => $content->content->get(App::getLocale())
        ]);
    }
}
