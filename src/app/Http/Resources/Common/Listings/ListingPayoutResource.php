<?php

namespace App\Http\Resources\Common\Listings;

use Illuminate\Http\Resources\Json\JsonResource;

class ListingPayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        try {

            $total = (money($this->total, defaultCurrency())->formatByDecimal());
            $unit_price = (money($this->unit_price, defaultCurrency())->formatByDecimal());

            return [
                'id' => $this->id,
                'total' => $total,
                'payout_date' => $this->payout_date,
                'owners' => $this->when(!is_null($this->owners), $this->owners),
                'paid_owners' => $this->when(!is_null($this->paid_owners), $this->paid_owners),
                'invested_units' => $this->invested_units,
                'unit_profit' => $unit_price,
                'investors' => InvestorPayoutResource::collection($this->investors()->wherePivot('shares', '>', 0)->get()),
                'listing' => $this->whenLoaded('listing', function () {
                    return [
                        'title' => $this->listing->title,
                        'share_price' => ($this->unit_price / 100), //$this->listing->share_price->formatByDecimal(),
                        'total_shares' => $this->invested_units //$this->listing->total_shares
                    ];
                }),
            ];

        } catch (\Exception $e) {
            $this->logService->log('payout-ListingPayoutResource-resource', $e);
        }
    }
}
