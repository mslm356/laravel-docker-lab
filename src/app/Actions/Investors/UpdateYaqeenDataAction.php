<?php

namespace App\Actions\Investors;

use App\Services\LogService;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdateYaqeenDataAction
{
    use AsAction;

    protected $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }


    public function handle($user)
    {
        try {
            $yaqeenInfoService = new YaqeenInfoService();

            $birth_date= Carbon::createFromFormat('Y-m-d', $user->nationalIdentity->birth_date);
            $yaqeen_request_data=['nin'=>$user->nationalIdentity->nin,'birthDateInstance'=>$birth_date];

            $info = $yaqeenInfoService->yaqeenInfo($yaqeen_request_data);
            $user->nationalIdentity->update($info);

            $addresses = $yaqeenInfoService->yaqeenAddress($yaqeen_request_data);

            DB::transaction(function () use ($addresses,$user){
                $user->nationalIdentity->addresses()->delete();
                $user->nationalIdentity->addresses()->createMany($addresses);
            });

            if(isset($info['id_expiry_date']) && $info['id_expiry_date'] < now())
                return false;

            return true;

        }
        catch (\Exception $e)
        {
            $this->logServices->log('UPDATE-YAQEEN-DATA-EXCEPTION', $e);
        }

    }

}
