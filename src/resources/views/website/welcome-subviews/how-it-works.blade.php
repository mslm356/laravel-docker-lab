<div class="py-16 overflow-hidden bg-primary-500 lg:py-24" id="howItWorks">
    <div class="relative px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div class="relative">
            <h1 class="text-3xl font-extrabold tracking-tight text-center text-primary-inv-500 sm:text-6xl">
                {{ __('website.how_it_works') }}
            </h1>
        </div>

        <ul class="md:flex md:justify-center md:flex-wrap">
            @foreach ($howItWorks as $item)
                <li class="px-4 my-6 md:w-1/2 lg:w-1/3 xl:w-1/5">
                    <div class="relative">
                        <img src="{{ $item['image'] }}" class="{{ $item['class'] }}">
                        <h4 class="absolute bottom-0 text-2xl font-extrabold leading-8 tracking-tight text-primary-inv-500 sm:text-3xl sm:leading-9">
                            {{ $item['title'] }}
                        </h4>
                    </div>
                    <p class="mt-3 text-lg leading-7 text-gray-300">
                        {{ $item['description'] }}
                    </p>
                </li>
            @endforeach
        </ul>
    </div>
</div>
