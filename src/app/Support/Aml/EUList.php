<?php

namespace App\Support\Aml;

use App\Enums\AML\AmlList;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class EUList implements AmlListInterface
{
    protected $list = null;

    protected static $filename = 'aml-lists/eu-list.xml';

    /**
     * Get the list
     *
     * @return \SimpleXMLElement
     */
    public function getList()
    {
        if ($this->list) {
            return $this->list;
        }

        if (Storage::disk('local')->exists(static::$filename)) {
            $list = Storage::disk('local')->get(static::$filename);
        } elseif (!($list = Cache::get('eu-list'))) {
            $res = Http::get('https://webgate.ec.europa.eu/europeaid/fsd/fsf/public/files/xmlFullSanctionsList/content?token=dG9rZW4tMjAxNw');
            Cache::put('eu-list', $list = $res->body(), now()->addDay());
        }

        $this->list = new \SimpleXMLElement($list);

        return $this->list;
    }

    /**
     * Check the infomation in the list
     *
     * @param string|null $firstName
     * @param string|null $secondName
     * @param string|null $thirdName
     * @param string|null $fourthName
     * @param string|null $nationality
     * @param string|null $dob
     * @param string|null $nationalId
     * @param string|null $passportId
     * @return array
     */
    public function check($firstName, $secondName, $thirdName, $fourthName, $nationality, $dob, $nationalId, $passportId)
    {
        $suspected = [];

        foreach ($this->getList()->sanctionEntity as $i) {
            $info = $this->transformIndividualInfo($i);

            $isSuspected = false;

            foreach ($info['docs'] ?? [] as $doc) {
                if (!in_array($doc['type'], ['passport', 'id'])) {
                    $passport = stripos($doc['number'], $passportId);
                    $id = stripos($doc['number'], $nationalId);

                    if ($passport !== false || $id !== false) {
                        $isSuspected = true;
                        break;
                    }
                } elseif ($doc['type'] === 'passport' && stripos($doc['number'], $passportId) !== false) {
                    $isSuspected = true;
                    break;
                } elseif ($doc['type'] === 'id' && stripos($doc['number'], $nationalId) !== false) {
                    $isSuspected = true;
                    break;
                }
            }

            if ($isSuspected) {
                $suspected[] = $this->standardize($info);
                continue;
            }

            foreach ($info['aliases'] ?? [] as $alias) {
                $nameChecks = $this->checkNameIfExists($alias, [
                    $firstName, $secondName, $thirdName, $fourthName
                ]);

                if ($nameChecks > 1) {
                    $suspected[] = $this->standardize($info);
                    continue;
                }
            }
        }

        return $suspected;
    }

    /**
     * Transform the user information into a suitable format
     *
     * @param \SimpleXMLElement $i
     * @return array
     */
    public function transformIndividualInfo($i)
    {
        $info = [];

        $aliases = [];

        // aliases
        foreach ($i->nameAlias as $alias) {
            $name = (string)$alias['wholeName'];

            if ($name) {
                $aliases[] = $name;
            }
        }

        if (!empty($aliases)) {
            $info['aliases'] = $aliases;
        }

        // date of births
        if (isset($i->birthdate)) {
            foreach ($i->birthdate as $birthdate) {
                $date = (string)$birthdate['birthdate'];
                $dob = [];
                if ($date) {
                    $dob['date'] = $date;
                }

                $year = (string)$birthdate['year'];

                if ($year) {
                    $dob['year'] = $year;
                }

                if (count($dob)) {
                    $info['dob'][] = $dob;
                }
            }
        }

        // Passport/National IDs
        if (isset($i->identification)) {
            foreach ($i->identification as $identity) {
                $type = (string)$identity['identificationTypeCode'];
                $number = (string)$identity['number'];

                if ($type && $number) {
                    $info['docs'][] = [
                        'type' => $type,
                        'number' => $number
                    ];
                }
            }
        }


        return $info;
    }

    /**
     * Check number of matching of names in the full/alias name
     *
     * @param string $fullName
     * @param array $names
     * @return int
     */
    public function checkNameIfExists($fullName, $names)
    {
        $checks = 0;

        foreach ($names as $name) {
            if ($name && stripos($fullName, $name) !== false) {
                $checks++;
            }
        }

        return $checks;
    }

    /**
     * get List index
     *
     * @return int
     */
    public function getListIndex(): int
    {
        return AmlList::EU;
    }

    /**
     * Standrize the data between the lists
     *
     * @param array $info
     * @return array
     */
    public function standardize($info)
    {
        try {
            $sInfo = [
                'first_name' => '',
                'second_name' => '',
                'third_name' => '',
                'fourth_name' => '',
                'full_name' => '',
                'aliases' => $info['aliases'] ?? [],
                'dob' => [],
                'docs' => $info['docs'] ?? [],
            ];

            if (isset($info['dob'])) {
                $sInfo['dob'] = array_map(function ($item) {
                    return [
                        'type' => 'APPROX',
                        'date' => $item['date'] ?? null,
                        'year' => $item['year'] ?? null
                    ];
                }, $info['dob']);
            }

            return $sInfo;
        } catch (\Throwable $th) {
            throw new \Exception('unable to standrize user: '.json_encode($info), 0, $th);
        }
    }
}
