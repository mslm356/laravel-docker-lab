<?php

namespace App\Services\Sdad\Repository;

use App\Models\EdaatInvoice;
use App\Models\Users\User;
use App\Services\Exeptions\NoResponseDataException;
use App\Services\LogService;
use App\Services\Sdad\Contracts\SingleInvoicesRepositoryContract;
use App\Services\Sdad\Enums\ServiceEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SingleInvoicesRepository implements SingleInvoicesRepositoryContract
{
    public $provider_repo;
    public $logService;

    public function __construct()
    {
        $this->provider_repo = new EdaatProviderClientRepository();
        $this->logService = new LogService();

    }

    /**
     * @inheritDoc
     */
    public function create(User $user, array $request_data): ?Model
    {
        try {
            $request_data['internal_invoice_uuid'] = (string)Str::uuid();
            $service_name = ServiceEnums::service_name(ServiceEnums::SINGLE_INVOICE_CREATE);
            $data = $this->prepare_invoice_data($user, $request_data);
            $this->provider_repo->initiation($service_name)->body($data)->send();

            if ($this->provider_repo->response->successful()) {
                $internal_data = $this->prepare_internal_invoice_data($user, $request_data);
                $invoice = EdaatInvoice::create($internal_data);
                return $invoice;

            } else {
                throw new NoResponseDataException();
            }
        } catch (NoResponseDataException $e) {
            throw new NoResponseDataException();
        } catch (\Exception $e) {
            $this->logService->log('Sdad-create_single_invoice_repo', $e, ['issue', json_encode($request_data)]);

            throw new \Exception('provider error');
        }
    }

    /**
     * @inheritDoc
     */
    public function show($invoice_id): ?Model
    {
        return EdaatInvoice::findOrFail($invoice_id);
    }

    /**
     * @inheritDoc
     */
    public function update($invoice_id, array $request_data): ?Model
    {
        $invoice = $this->show($invoice_id);
        if ($invoice->update($request_data)) {
            return $this->show($invoice_id);
        }
        return null;
    }

    public function prepare_invoice_data(User $user, $request_data): array
    {
        $identity = $user->nationalIdentity->setLocale('ar');
        $data = [
            'IsClientEnterpise' => false,
            'InternalCode' => $request_data['internal_invoice_uuid'],
            'NationalId' => $identity->nin,
            'IssueDate' => now()->addHours(3)->format('Y-m-d H:i:s'),
            'DueDate' =>  now()->addHours(3)->format('Y-m-d H:i:s'),
            'TotalAmount' => $request_data['amount'],
            'ExportToSadad' => true,
            'HasValidityPeriod' => true,
            'FromDurationTime' => now()->addHours(3)->format('H:i'),
            'ToDurationTime' => now()->addHours(3)->addMinutes(config('edaat.expire_time'))->format('H:i'),
            'ExpiryDate' => now()->addHours(3)->addMinutes(config('edaat.expire_time'))->format('Y-m-d H:i:s'),
        ];
        $data['Products'] = $this->prepare_invoice_products_data($user, $request_data);
        $data['Customer'] = $this->prepare_invoice_customer_data($user, $request_data);

        return $data;
    }


    public function prepare_invoice_products_data(User $user, $request_data): array
    {
        return [
            [
                'ProductCode' => config('edaat.wallet_charge_code'),
                'Price' => $request_data['amount'],
                'Qty' => 1,
            ]
        ];
    }

    public function prepare_invoice_customer_data(User $user, $request_data): array
    {
        $identity = $user->nationalIdentity->setLocale('ar');

        $data = [
            'NationalID' => $identity->nin,
            'FirstNameAr' => $identity->first_name,
            'FatherNameAr' => $identity->second_name,
            'GrandFatherNameAr' => $identity->third_name,
            'LastNameAr' => $identity->forth_name,
            'Email' => $user->email,
            'MobileNo' => $user->phone_number->getRawNumber(),
            'CustomerRefNumber' => $user->id,
            'PreferedLanguage' => 'ar',
        ];

        if ($identity->birth_date_type == 'gregorian') {
            $data['DateOfBirth'] = $identity->birth_date;
        } else {
            $data['DateOfBirthHijri'] = $identity->birth_date;
        }

        return $data;
    }

    public function prepare_internal_invoice_data(User $user, $request_data): array
    {
        $data = [
            'id' => $request_data['internal_invoice_uuid'],
            'user_id' => $user->id,
            'expire_at' => now()->addMinutes(config('edaat.expire_time'))->format('Y-m-d H:i:s'),
            'amount' => $request_data['amount'],
            'edaat_invoice_id' => $this->provider_repo->getResponse()->Body->InvoiceNo ?? ''
        ];
        return $data;
    }
}
