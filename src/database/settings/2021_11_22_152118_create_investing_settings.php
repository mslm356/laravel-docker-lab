<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateInvestingSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add(
            'investing.administrative_fees_percentage',
            "2"
        );
    }

    public function down(): void
    {
        $this->migrator->delete('investing.administrative_fees_percentage');
    }
}
