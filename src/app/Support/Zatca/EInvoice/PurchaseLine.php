<?php

namespace App\Support\Zatca\EInvoice;

use Cknow\Money\Money;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class PurchaseLine
{
    /**
     * Item name
     *
     * @var string
     */
    protected $name;

    /**
     * Quantity
     *
     * @var int
     */
    protected $qty;

    /**
     * Item price without VAT
     *
     * @var \Cknow\Money\Money
     */
    protected $itemPrice;

    /**
     * VAT Percentage
     *
     * @var string|null
     */
    protected $vatPercentage;

    /**
     * Discount percentage
     *
     * @var string|null
     */
    protected $discount;

    function __construct(array $name, int $qty, Money $itemPrice, ?string $vatPercentage, ?string $discount)
    {
        $this->name = $name;
        $this->qty = $qty;
        $this->itemPrice = $itemPrice;
        $this->vatPercentage = $vatPercentage;
        $this->discount = $discount;
    }

    /**
     * Get name
     *
     * @return \Cknow\Money\Money
     */
    public function getName(string $locale = null)
    {
        if ($locale === null) {
            $locale = App::getLocale();
        }

        return Arr::get($this->name, $locale, '');
    }

    /**
     * Get quantity
     *
     * @return \Cknow\Money\Money
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Get item price
     *
     * @return \Cknow\Money\Money
     */
    public function getItemPrice()
    {
        return $this->itemPrice;
    }

    /**
     * Get discount percentage
     *
     * @return string|null
     */
    public function getDiscountPercentage()
    {
        return $this->discount;
    }

    /**
     * Get VAT percentage
     *
     * @return string|null
     */
    public function getVatPercentage()
    {
        return $this->vatPercentage;
    }

    /**
     * Get total VAT amount
     *
     * @return \Cknow\Money\Money
     */
    public function getTotalVatAmount()
    {
        if (!$this->vatPercentage) {
            return money(0, $this->itemPrice->getCurrency()->getCode());
        }

        return $this->getLineSubtotal()
            ->subtract($this->getTotalDiscountAmount())
            ->multiply($this->vatPercentage)
            ->divide(100);
    }

    /**
     * Get total discount amount
     *
     * @return \Cknow\Money\Money
     */
    public function getTotalDiscountAmount()
    {
        if (!$this->discount) {
            return money(0, $this->itemPrice->getCurrency()->getCode());
        }

        return $this->getLineSubtotal()
            ->multiply($this->discount)
            ->divide(100);
    }

    /**
     * Get total line amount (without VAT & without discount)
     *
     * @return \Cknow\Money\Money
     */
    public function getLineSubtotal()
    {
        return $this->itemPrice->multiply($this->qty);
    }

    /**
     * Get total line amount (without VAT)
     *
     * @return \Cknow\Money\Money
     */
    public function getLineTotalWithoutVat()
    {
        return $this->getLineSubtotal()
            ->subtract($this->getTotalDiscountAmount());
    }

    /**
     * Get total line amount (with VAT)
     *
     * @return \Cknow\Money\Money
     */
    public function getLineTotal()
    {
        return $this->getLineSubtotal()
            ->subtract($this->getTotalDiscountAmount())
            ->add($this->getTotalVatAmount());
    }
}
