<?php

namespace App\Support\Transactions\Descriptions\Generators;

use App\Models\Payouts\Payout;
use Illuminate\Support\Arr;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\GeneratorBase;

class PayoutDistributionType extends GeneratorBase
{
    public function generateForCredit(Transaction $transaction, $locale)
    {
        $items = Arr::only($transaction->meta, ['listing_id', 'listing_title']);

        if (count($items) < 2) {
            return __('transaction-description.credit.payout_distribution_in_general');
        }

        $id = $items['listing_id'];
        $title = $items[locale_suff('listing_title')];

        return __('transaction-description.credit.payout_distribution', ['title' => $title, 'id' => $id], $locale);
    }

    public function generateForDebit(Transaction $transaction, $locale)
    {

        $items = Arr::only($transaction->meta, ['user_id', 'user_name', 'payout_id']);

        if (count($items) < 3) {
            return __('transaction-description.debit.payout_distribution_general');
        }

        $locale = $locale ?? app()->getLocale();

        $fund_title = isset($transaction->meta["listing_title_" . $locale]) ? $transaction->meta["listing_title_" . $locale] : "---";

        $userId = $items['user_id'];
        $userName = $items['user_name'];
        $payoutId = $items['payout_id'];

        $payout_id = isset($transaction->meta["payout_id"]) ? $transaction->meta["payout_id"] : null;
        $payout = Payout::where('id', $payout_id)->first();
        if ($payout && $payout->type == "payout") {
            return __('transaction-description.debit.payout_distribution', [
                'user_id' => $userId,
                'user_name' => $userName,
                'payout_id' => $payoutId,
                'fund_title' => $fund_title,
                'amount' => $transaction->amount_float,
            ], $locale);
        } else {
            return __('transaction-description.debit.payout_distribution_dis', [
                'user_id' => $userId,
                'user_name' => $userName,
                'payout_id' => $payoutId,
                'fund_title' => $fund_title,
                'amount' => $transaction->amount_float,
            ], $locale);
        }

    }
}
