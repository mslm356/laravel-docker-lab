<?php

namespace App\Actions\Listings;

use App\Enums\Role;
use App\Jobs\Funds\SendAlertMailsOnCreate;
use App\Jobs\Zoho\NewCustomer;
use App\Jobs\zoho\NewItem;
use App\Jobs\Zoho\UpdateItem;
use App\Models\Users\User;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Enums\Listings\DetailCategory;
use Illuminate\Support\Arr;
use Lorisleiva\Actions\Concerns\AsAction;
use Cknow\Money\Money;

class UpdateOrCreateListing
{
    use AsAction;

    /**
     * Create or update listing
     *
     * @param array $data
     * @param \App\Models\Listings\Listing $listing
     * @return \App\Models\Listings\Listing
     */
    public function handle($data, Listing $listing = null)
    {


        $data['listing']['investment_shares']=$data['listing']['total_shares'];

        if ($listing === null) {
            $listing = $this->create($data);
        } else {
            $listing = $this->update($data, $listing);
        }

        return $listing;
    }

    /**
     * Create listing
     *
     * @param array $data
     * @return \App\Models\Listings\Listing
     */
    public function create($data)
    {
        $listing = Listing::create($this->transformListingValues($data));

        foreach (DetailCategory::asArray() as $value) {
            $key = strtolower($value);

            if ($content = ($data[$key] ?? null)) {
                $listing->details()->create([
                    'category' => $value,
                    'value' => $content
                ]);
            }
        }

        $this->updateOrCreateBimShares($listing);

//        $zohoNewCustomerJob = (new NewCustomer($listing, \App\Enums\Zoho\NewCustomer::Fund));
//        dispatch($zohoNewCustomerJob);
//
//        $zohoNewItemJob = (new NewItem($listing));
//        dispatch($zohoNewItemJob);

//        $sendAlertMailsOnCreateJob = (new SendAlertMailsOnCreate($listing));
//        dispatch($sendAlertMailsOnCreateJob);

        return $listing;
    }

    /**
     * Update listing
     *
     * @param array $data
     * @param \App\Models\Listings\Listing $listing
     * @return \App\Models\Listings\Listing
     */
    public function update($data, Listing $listing)
    {
        if ($listing->investingStarted() ) {
            $this->cleanDataIfInvestorsStartedInvesting($data);
            $data['listing']['target'] = optional($listing->target)->formatByDecimal();
        }

        $oldSharePrice = $listing->share_price;

        $listing->update($this->transformListingValues($data));

        foreach (DetailCategory::asArray() as $value) {
            $key = strtolower($value);

            if ($content = ($data[$key] ?? null)) {
                $listing->details()
                    ->updateOrCreate(
                        ['category' => $value],
                        ['value' => $content]
                    );
            }
        }

        if ($listing->zoho_item_id && $oldSharePrice != $listing->share_price) {
            $zohoUpdateItemJob = (new UpdateItem($listing));
            dispatch($zohoUpdateItemJob);
        }

        $this->updateOrCreateBimShares($listing);

        return $listing;
    }

    public function transformListingValues($data)
    {
        return array_merge(
            $data['listing'],
            [
                'target' => Money::parseByDecimal(
                    $data['listing']['target'],
                    $defaultCurrency = defaultCurrency()
                )
                    ->getAmount(),
                'currency' => $defaultCurrency
            ]
        );
    }

    public function cleanDataIfInvestorsStartedInvesting(&$data)
    {
        Arr::forget($data, 'listing.total_shares');
        Arr::forget($data, 'listing.investment_shares');
        Arr::forget($data, 'listing.administrative_fees_percentage');
    }

    /**
     * add reserved shares for BIM to listing investors
     *
     * @param \App\Models\Listings\Listing $listing
     * @return void
     */
    public function updateOrCreateBimShares($listing)
    {
        $user = User::role(Role::PayoutsCollector)->with('investor')->first();

        $investments = DB::table('listing_investor')
            ->where('listing_id', $listing->id)
            ->lockForUpdate()
            ->get();

        $investmentsCount = $investments->where('investor_id', $user->id)->count();

        if ($investmentsCount === 0) {
            $method = 'attach';
        } else {
            $method = 'updateExistingPivot';
        }

        $user->investments()
            ->{$method}($listing->id, [
                'invested_amount' => $listing->share_price->multiply($listing->reserved_shares)->getAmount(),
                'invested_shares' => $listing->reserved_shares,
                'currency' => defaultCurrency()
            ]);
    }
}
