<?php

namespace App\Services\Exeptions;

use App\Http\Helper\APIResponse;
use Exception;

class NoResponseDataException extends Exception
{

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        $message = trans('messages.no_returned_response_data');

        if ($request->expectsJson()) {
            return response()->json([
                'message' => $message
            ], 400);
        }

        return response($message, 400);
    }
}
