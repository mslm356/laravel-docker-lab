<?php

namespace App\Mail\Investors;

use App\Enums\Banking\TransactionReason;
use App\Models\Payouts\Payout;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Support\Transactions\Descriptions\DescriptionManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class NewTransaction extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $transaction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Transaction $transaction)
    {
        $this->user = $user;
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $amount =  $this->transaction->amount_float;
        $name = $this->user ? $this->user->full_name : '---';

        if (in_array(
            $this->transaction->reason,
            [
                TransactionReason::InvestingInListing,
                TransactionReason::InvestingInListingAdminFee,
                TransactionReason::InvestingInListingVatOfAdminFee
            ]
        )
        ) {
            $file = DB::table("files")->where("fileable_id", $this->transaction->id)
                ->where("fileable_type", "App\Models\VirtualBanking\Transaction")
                ->first();

            $profileUrl = config('app.investor_url') . '/app/overview';

            $fund_title = isset($this->transaction->meta["listing_title_" . $this->user->locale]) ? $this->transaction->meta["listing_title_" . $this->user->locale] : "---";
            $units = isset($this->transaction->meta["shares"]) ? $this->transaction->meta["shares"] : "---";

            $downloadInvoiceLink = $file ? route('investor.files.download', ['file' => $file->id]) : null;

            return $this->view(
                'emails.funds.recieve_investment_amount',
                compact('name', "fund_title", 'downloadInvoiceLink', 'profileUrl', 'amount', 'units')
            )
                ->subject(__('emails/funds/recieve_investment_amount.subject'));

        } elseif ($this->transaction->reason === TransactionReason::AnbStatement) {

            $profileUrl = config('app.investor_url') . '/app/overview';
            $iban = isset($this->transaction->meta["vaccount_identifier"]) ? $this->transaction->meta["vaccount_identifier"] : "---";
            $time = $this->transaction->updated_at;

            return $this->view('emails.investors.deposit', compact('name', 'iban', "time", 'profileUrl', 'amount'))
                ->subject(__('emails/investors/deposit.subject'));

        } elseif ($this->transaction->reason === TransactionReason::PayoutDistribution) {

            $fund_title = isset($this->transaction->meta["listing_title_" . $this->user->locale]) ? $this->transaction->meta["listing_title_" . $this->user->locale] : "---";
            $payout_id = isset($this->transaction->meta["payout_id"]) ? $this->transaction->meta["payout_id"] : null;
            $payout = Payout::where('id', $payout_id)->first();

            $profileUrl = config('app.investor_url') . '/app/overview';
            if($payout && $payout->type == "payout") {
                return $this->view('emails.payouts.create', compact('name', 'profileUrl', 'amount', 'payout', 'fund_title'))
                    ->subject(__('emails/payouts/create.subject'));
            } else {
                return $this->view('emails.payouts.create', compact('name', 'profileUrl', 'amount', 'payout', 'fund_title'))
                    ->subject(__('emails/payouts/create.subject_dis'));
            }
        } else {
            $profileUrl = config('app.investor_url') . '/app/overview';
            $name = $this->user->first_name;
            $description = DescriptionManager::getDescription($this->transaction);

            return $this->view(
                'emails.investors.new-transaction',
                compact('amount', 'name', "description", 'profileUrl')
            )
                ->subject(__('emails/investors/new_transaction.subject'));
        }
    }
}
