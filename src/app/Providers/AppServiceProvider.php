<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }

        Password::defaults(function () {
            return Password::min(8)->mixedCase()->numbers()->symbols();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::excludeUnvalidatedArrayKeys();

        if(config('app.force_https', false)) {
            URL::forceScheme('https');
        }

        View::composer(['website.welcome-subviews.navbar', 'components.website.navbar'], function ($view) {
            $websiteNavLinks = [
                [
                    'id' => 'about_us',
                    'text' => __('website.navlinks.about_us'),
                    'link' => route('website_section', ['section' => 'about-us']),
                ],
                [
                    'id' => 'funds',
                    'text' => __('website.navlinks.funds'),
                    'link' => route('home_page').'#funds',
                ],
                [
                    'id' => 'login',
                    'text' => __('website.navlinks.login'),
                    'link' => investor_url('login'),
                    'visible' => !auth()->check()
                ],
                [
                    'id' => 'signup',
                    'text' => __('website.navlinks.signup'),
                    'link' => investor_url('sign-up'),
                    'visible' => !auth()->check(),
                    'type' => 'highlighted'
                ],
                [
                    'id' => 'how_it_works',
                    'text' => __('website.navlinks.how_it_works'),
                    'link' => route('home_page').'#howItWorks',
                ],
                [
                    'id' => 'dashboard',
                    'text' => __('website.navlinks.dashboard'),
                    'link' => investor_url('app/overview'),
                    'visible' => auth()->check()
                ],
                [
                    'id' => 'locale',
                    'text' => (($locale = App::getLocale()) === 'ar') ? 'English' : 'العربية',
                    'link' => LaravelLocalization::getLocalizedURL($locale === 'en' ? 'ar' : 'en'),
                ],
            ];

            $websiteNavLinks = array_filter($websiteNavLinks, function ($link) {
                return $link['visible'] ?? true;
            });

            $view->with('websiteNavLinks', $websiteNavLinks);
        });
    }
}
