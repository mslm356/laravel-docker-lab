<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Actions\Listings\Payouts\CreatePayout;
use App\Actions\Listings\Payouts\GetPayouts;
use App\Actions\Listings\Payouts\PayForInvestors;
use App\Common\ValidationRules\Listings\ListingPayoutRules;
use App\Enums\Zoho\AccountTypes;
use App\Http\Resources\Common\Listings\ListingPayoutV2Resource;
use App\Jobs\Zoho\ManualJournalJob;
use App\Jobs\Zoho\ManualJournalWithCustomDataJob;
use App\Models\Users\User;
use App\Models\ZohoSettings;
use App\Services\LogService;
use App\Support\Zoho\ManualJournal;
use Carbon\Carbon;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Exceptions\NoBalanceException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\Payouts\Payout;
use App\Models\Listings\Listing;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Listings\ListingPayoutResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ListingPayoutController extends Controller
{

    public $zohoJournal;
    public $zohoJournalData;
    public $logService;

    public function __construct()
    {
        $this->middleware('permission:funds-id-payouts', ['only' => ['index', 'show']]);
        $this->middleware('permission:listing-payouts-create', ['only' => ['store']]);
        $this->logService = new LogService();
        $this->zohoJournal = new ManualJournal();
        $this->zohoJournalData = new \App\Support\Zoho\JournalPrepareData\Payout();
    }

    /**
     * List Payouts
     *
     * @param \App\Models\Listings\Listing $listing
     * //     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Listing $listing, Request $request)
    {

        $data = Validator::make($request->all(), [
            'type' => 'required|in:distributed,payout'
        ])->validate();

        try {

            $payouts = GetPayouts::run($listing, $data['type']);

            if ($data['type'] == 'payout') {

                if (!$payouts) {
                    return response()->json(['data' => null], 200);
                }
                return new ListingPayoutV2Resource(GetPayouts::run($listing, $data['type']));
            }

            return ListingPayoutV2Resource::collection($payouts);

        } catch (\Exception $e) {
            $this->logService->log('payout-index', $e);
        }
    }

    /**
     * List Payout details
     *
     * @param \App\Models\Payouts\Payout $payout
     * @return \App\Http\Resources\AuthorizedEntities\Listings\ListingPayoutResource
     */
    public function show($listing, Payout $payout)
    {
        try {

            $payout->load(['listing']);
            return new ListingPayoutResource($payout);

        } catch (\Exception $e) {
            $this->logService->log('payout-show', $e);
        }
    }

    /**
     * Store payout
     *
     * @param \Illuminate\Http\Request $request
     * @param App\Models\Listings\Listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {

        if (!request()->user('sanctum')->can('access-admin-portal')) {
            throw new AuthorizationException;
        }

        $isListingHasPayout = Payout::query()
            ->where('listing_id', $listing->id)
            ->where('type', 'payout')
            ->count();

        if ($isListingHasPayout) {
            return response()->json(['message' => __('listing already has final payout')], 400);
        }

        $totalCheck = explode('.', $request['total']);

        if (isset($totalCheck[1]) && strlen($totalCheck[1]) > 2) {
            return response()->json([
                'message' => 'total must be two digits ex: 1.22'
            ], 400);
        }

        $data = $this->validator($request->all())->validate();

        $investors = $listing->investors;

        $investedShares = $investors->sum('pivot.invested_shares');

        if ($investors->count() === 0 || $investedShares == 0) {
            return response()->json([
                'message' => trans('messages.no_investors_in_listing')
            ], 400);
        }

        $data['total'] *= $investedShares;

        $wallet = $listing->getWallet(WalletType::ListingSpv);

        $amount = money_parse_by_decimal($data['total'], defaultCurrency());

        if ($amount->greaterThan(money($wallet->balance))) {

            return response()->json([
                'message' => trans('messages.no_enough_balance')
            ], 400);
        }

        return DB::transaction(function () use ($request, $listing, $data, $investedShares, $investors, $amount) {

            $payout = CreatePayout::run($listing, $data);

//            $zohoData =  $this->zohoJournalData->preparePayout($listing,AccountTypes::ANBBank,AccountTypes::FundWallet,$data['total']);
//
//            $zohoManualJournalJob = (new ManualJournalJob($zohoData));
//            dispatch($zohoManualJournalJob);

            return response()->json(['id' => $payout->id], 201);

        }, 1);
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, ListingPayoutRules::get());
    }

    /**
     * pay for investors
     *
     * @param \Illuminate\Http\Request $request
     * @param App\Models\Payouts\Payout
     * @param \App\Support\VirtualBanking\BankService $wallet
     * @return \Illuminate\Http\JsonResponse
     */
    public function pay(Request $request, Listing $listing, Payout $payout)
    {
        try {

            if (!$payout->approval_one || !$payout->approval_two) {
                return response()->json(['message' => __('payout not approved .')], 422);
            }

            if ($payout->type == 'payout') {
                return response()->json(['message' => __('this payout already paid before')], 422);
            }

//             array_values(Arr::where($requestedInvestors, function ($value, $key) use ($requestedInvestors) {
//                if ($value['id'] == 1 && $value['amount'] == '0') {
//                    unset($requestedInvestors[$key]);
//                }
//             }));

            $validatedData['investors'] = $request['investors'];

            $validator = Validator::make($validatedData, ListingPayoutRules::getForPay());

            if ($validator->fails()) {
                throw ValidationException::withMessages([$validator->errors()->first()]);
            }

            $totalPaid = DB::table('investor_payout')
                ->whereIn('investor_id', array_column($request['investors'], 'id'))
                ->where('paid', 0)
                ->where('payout_id', $payout->id)
                ->sum('expected');

            $wallet = $payout->listing->getWallet(WalletType::ListingSpv);

            if (!$wallet || money($wallet->balance)->lessThan(money($totalPaid, defaultCurrency()))) {
                $this->logService->log('PAYOUT-PAY-INVESTOR-WALLETs', null, ['CHECK', $wallet->balance, $totalPaid]);
                throw new NoBalanceException();
            }

            $payForInvestorsJob = (new \App\Jobs\Listings\Payouts\PayForInvestors($payout, $request['investors']));
            dispatch($payForInvestorsJob)->onQueue('payouts');

//            PayForInvestors::run($request['investors'], $payout);

            return response()->json();

        } catch (NoBalanceException $e) {
            $this->logService->log('PAYOUT-PAY-NO-BALANCE', $e);
            return response()->json(['message' => __('messages.no_enough_balance')], 422);
        } catch (ValidationException $e) {
            $this->logService->log('PAYOUT-PAY-VALIDATION', $e);
            return response()->json(['message' => $e->getMessage(), 'errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            $this->logService->log('PAYOUT-PAY-EXC', $e);
            return response()->json(['message' => [$e->getMessage(), $e->getLine(), $e->getFile()]], 422);
        }
    }

    public function prepareZohoData($investorsData, $firstAccount, $secondAccount, $total, $listing)
    {

        $firstAccountName = ZohoSettings::where('key', $firstAccount)->first();
        $secondAccountName = ZohoSettings::where('key', $secondAccount)->first();

        $firstAccountKey = $firstAccountName ? $firstAccountName->key : '';
        $secondAccountKey = $secondAccountName ? $secondAccountName->key : '';

        $data = [
            'journal_date' => Carbon::now()->format('Y-m-d'),
            'line_items' => [
                [
                    'account_id' => $firstAccountName ? $firstAccountName->value : '',
                    'customer_id' => $listing ? $listing->zoho_id : null,
                    'description' => 'get amount from ' . $firstAccountKey,
                    'debit_or_credit' => 'debit',
                    'amount' => $total,
                ],
            ],
        ];


        foreach ($investorsData as $index => $investorData) {

            $user = User::find($investorData['id']);

            $data['line_items'][$index + 1] = [

                'account_id' => $secondAccountName ? $secondAccountName->value : '',
                'customer_id' => $user ? $user->zoho_id : null,
                'description' => 'add amount to ' . $secondAccountKey,
                'debit_or_credit' => 'credit',
                'amount' => $investorData['amount'],
            ];
        }

        return $data;
    }

    public function payoutApproval(Request $request, Listing $listing, Payout $payout)
    {
        try {

            $data = $this->payoutApprovalValidator($request->all())->validate();

            $user = getAuthUser('web');

            $userPermissions = DB::table('users')
                ->where('users.id', $user->id)
                ->join('model_has_roles', function ($join) {
                    $join->on('users.id', '=', 'model_has_roles.model_id');
                })
                ->join('role_has_permissions', function ($join) {
                    $join->on('model_has_roles.role_id', '=', 'role_has_permissions.role_id');
                })
                ->join('permissions', function ($join) {
                    $join->on('role_has_permissions.permission_id', '=', 'permissions.id');
                })
                ->select(['permissions.name'])
                ->get()
                ->toArray();

            if (empty($userPermissions)) {
                return response()->json(['message' => __('not of your permissions')], 422);
            }

            $userPermissions = array_column($userPermissions, 'name');

            $updatedColumn = null;

            if (in_array('listing-payouts-approval', $userPermissions)) {
                $updatedColumn = 'approval_one';
            }elseIf (in_array('listing-payouts-approval-two', $userPermissions)) {
                $updatedColumn = 'approval_two';
            }

            $this->logService->log('PERMISSIONS', NULL, ['update column'=> $updatedColumn, $userPermissions]);

            if (!$updatedColumn) {
                return response()->json(['message' => __('not of your roles, Finance, Investment Team')], 422);
            }

            if ($payout->$updatedColumn) {
                return response()->json(['message' => __('this payout already approved before from this department')], 422);
            }

            $payout->update([
                $updatedColumn => $data['status'] ? $user->id : null
            ]);

            return response()->json(['message' => 'payout approved successfully'], 200);

        } catch (ValidationException $e) {
            $this->logService->log('PAYOUT-APPROVAL-VALIDATION', $e);
            return response()->json(['message' => $e->getMessage(), 'errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            $this->logService->log('PAYOUT-APPROVAL-EXC', $e);
            return response()->json(['message' => 'sorry, please try later .'], 422);
        }
    }

    public function payoutApprovalValidator($data)
    {
        return Validator::make($data, [
            'status' => 'required|boolean|in:1,0',
        ]);
    }
}
