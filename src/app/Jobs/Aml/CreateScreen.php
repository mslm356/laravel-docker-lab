<?php

namespace App\Jobs\Aml;

use App\Enums\AML\AmlApprove;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Services\LogService;
use App\Support\Aml\AmlServices;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CreateScreen implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $amlServices;
    public $logServices;
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->amlServices = new AmlServices();
        $this->logServices = new LogService();
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            $user = $this->user;

            if (config('aml.aml_registration_mode') != 'production') {
                return;
            }

            $userAmlAlreadyExist = DB::table('aml_users')
                ->where('user_id', $user->id)
                ->select('risk_result', 'screen_result', 'customer_reference_id', 'query_id', 'user_id', 'approve_status')
                ->first();

            if ($userAmlAlreadyExist && $userAmlAlreadyExist->approve_status != AmlApprove::Approve) {
                $this->approveAmlAfterKyc($userAmlAlreadyExist);
                return;
            }

            $data = [
                'type' => 'screen',
                'user' => $user,
            ];

            $this->amlServices->handle($data);

            $amlUser = DB::table('aml_users')
                ->where('user_id', $user->id)
                ->select('customer_reference_id', 'query_id', 'risk_result', 'screen_result')
                ->first();

            if (!$amlUser) {
                $this->logServices->log('AML-USER-NOT-VALID', null, ['aml user', 'aml user' => $amlUser, 'user' => $user]);
                return;
            }

            // Suspend account if rejected form aml focal
            $this->approveAmlAfterKyc($amlUser);

        } catch (\Exception $e) {
            $this->logServices->log('AML-JOB-EXCEPTION', $e);
        }
    }

    public function approveAmlAfterKyc($amlUser)

    {

        if ($amlUser->risk_result == "High") {

            InvestorProfile::query()
                ->where('user_id', $amlUser->user_id)
                ->update(['status' => InvestorStatus::Suspended]);

            DB::table('users')
                ->where('id', $amlUser->user_id)
                ->update(['jwt_token' => null]);
        }

        if ($amlUser->screen_result == 1 && $amlUser->risk_result) {
            $this->amlServices->approveAml($amlUser);
        }

        return true;
    }
}
