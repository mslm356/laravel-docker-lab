<?php

namespace App\Console\Commands\Investors;

use App\Enums\Role;
use App\Models\Users\User;
use App\Support\Users\UserAccess\ManageUserAccess;
use Facade\Ignition\Http\Controllers\HealthCheckController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Investors\ApproveProfile;
use App\Enums\Investors\InvestorStatus;
use App\Models\Investors\InvestorProfile;
use App\Enums\Investors\ProfileReviewType;

//use Spatie\Health\Facades\Health;
//use Spatie\Health\Checks\Checks\DatabaseConnectionCountCheck;

class UserAccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:user-access {--userId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update user access';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

//    /**
//     * Execute the console command.
//     *
//     * @return int
//     */
    public function handle()
    {

        $manageUserAccess = new ManageUserAccess();

//<<<<<<< HEAD
////        $user = User::find(10);
////
////        $manageUserAccess->handle($user);
////        dd(1);
//
//=======
        $userId = $this->option('userId');
        if ($userId)
        {
            $user = User::find($userId);
            return $manageUserAccess->handle($user);

        }
//>>>>>>> a4c9bba5a8bca498c3fb331a1a8b615f9dc13684

        User::whereHas('nationalIdentity')->role(Role::Investor)
            ->lazyById()
            ->each(function ($user) use ($manageUserAccess) {

                $manageUserAccess->handle($user);

            });
        dd('done');
    }
}
