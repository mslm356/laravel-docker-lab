<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use App\Jobs\Funds\SendAlertMailsOnCreate;
use App\Models\BankAccount;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Enums\BankAccountStatus;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Enums\Listings\ListingReviewStatus;
use App\Actions\Listings\UpdateOrCreateListing;
use App\Common\ValidationRules\Listings\ListingRules;
use App\Actions\Listings\Wallets\CreateListingWallets;
use App\Http\Resources\AuthorizedEntities\Listings\ListingResource;
use App\Http\Resources\AuthorizedEntities\Listings\EditListingResource;
use App\Http\Resources\AuthorizedEntities\Listings\ListingInvestorResource;
use App\Http\Resources\AuthorizedEntities\Listings\ListingListItemResource;

class ListingController extends Controller
{
    use CanAccessListing;

    /**
     * Listings list
     *
     * @param
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {
        $listing = Listing::latest()
            ->where('authorized_entity_id', authorizedEntityId())
            ->with('images')
            ->simplePaginate();

        return ListingListItemResource::collection($listing);
    }

    /**
     * Show Listing Deatils
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function show(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $listing->loadMissing([
            'details',
            'city',
            'type',
            'images',
            'attachments',
            'bankAccount',
            'authorizedEntity'
        ]);

        if ($request->input('edit')) {
            return new EditListingResource($listing);
        }

        return new ListingResource($listing);
    }

    /**
     * Show the listing's investors
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInvestors(Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $listing->load('investors');

        return ListingInvestorResource::collection($listing->investors);
    }

    /**
     * Store the type in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $data = $this->validate(
                $request,
                array_merge(
                    ListingRules::get(),
                    ListingRules::getBankAccountIdRuleForAuthorizedEntity()
                )
            );

            $data['listing']['authorized_entity_id'] = authorizedEntityId();
            $data['listing']['review_status'] = ListingReviewStatus::PendingReview;

            $listing = UpdateOrCreateListing::run($data);

            if ($listing->review_status === ListingReviewStatus::Approved && $listing->is_visible == 1 && $listing->is_closed == 0) {
                $sendAlertMailsOnCreateJob = (new SendAlertMailsOnCreate($listing));
                dispatch($sendAlertMailsOnCreateJob);
            }

            CreateListingWallets::run($listing);

            return response()->json([
                'id' => $listing->id
            ], 201);
        });
    }

    /**
     * update listing and its details
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        return DB::transaction(function () use ($request, $listing) {
            $data = $this->validate(
                $request,
                array_merge(
                    ListingRules::get($listing),
                    ListingRules::getBankAccountIdRuleForAuthorizedEntity()
                )
            );

            $data['listing']['review_status'] = ListingReviewStatus::PendingReview;

            $listing = UpdateOrCreateListing::run($data, $listing);

            return response()->json([
                'id' => $listing->id
            ], 200);
        });
    }
}
