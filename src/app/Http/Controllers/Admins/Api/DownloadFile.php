<?php

namespace App\Http\Controllers\Admins\Api;

use App\Http\Controllers\Controller;
use App\Models\File;

class DownloadFile extends Controller
{
    /**
     * Download file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(File $file)
    {
        return $file->download();
    }
}
