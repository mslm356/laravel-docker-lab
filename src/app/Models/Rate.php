<?php

namespace App\Models;

use App\Models\Users\User;

class Rate extends AppModel
{
    /**
     * Get the user (approved/rejected) of the account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
