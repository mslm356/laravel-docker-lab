<?php


namespace App\Services\Investors\Investment;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Actions\Investors\CheckIfCanInvest;
use App\Actions\Investors\ExportSubFormAndAttachToInvestingRecord;
use App\Actions\Investors\IsFirstInvestmentInListing;
use App\Actions\Listings\AfterCloseListingAction;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Enums\Notifications\NotificationTypes;
use App\Mail\Investors\NewInvestmentTransaction;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Urway\CardToken;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Services\CacheServices;
use App\Services\LogService;
use App\Services\MemberShip\SpecialCanInvest;
use App\Traits\NotificationServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Jobs\UpdateFundPercentageFirebase;

class InvestingServices
{

    use NotificationServices;

    public $handleResponseService;
    public $cacheServices;
    public $logService;
    public $specialCanInvest;

    public function __construct()
    {
        $this->handleResponseService = new HandleResponseService();
        $this->cacheServices = new CacheServices();
        $this->logService = new LogService();
        $this->specialCanInvest = new SpecialCanInvest();
    }

    public function handle($request, $user)
    {
        $response = ['status' => true];

        $data = $this->dataValidation($request);


        if ($this->checkAgeOfUser() == 0) {
            return $this->handleResponseService->responseValues($response, false, 'mobile_app/message.invalid_age', []);
        }

        $tokenResponse = $this->getToken($user->id, $data['token']);

        if (isset($tokenResponse['status']) && !$tokenResponse['status']) {
            $tokenResponseMessage = isset($tokenResponse['message']) ? $tokenResponse['message'] : 'token not valid';
            return $this->handleResponseService->responseValues($response, false, $tokenResponseMessage, []);
        }

        $token = $tokenResponse['data'];

        $listing = $this->getListing($token->listing_id, $user);

        if (!$listing) {
            return $this->handleResponseService->responseValues($response, false, 'messages.listing_not_found', []);
        }

        $toBeInvestedShares = $token->shares;

        $toBeInvestedAmountAndFees = CalculateInvestingAmountAndFees::run($listing, $toBeInvestedShares);


        try {
            $currentInvest = null;

            $case = CheckIfCanInvest::run($listing, $user, $currentInvest, $toBeInvestedShares,$toBeInvestedAmountAndFees);

            if ((is_int($case) && $token->payment_type != 'urway') || is_int($case) && $case != 5 && $token->payment_type == 'urway') {

                return $this->handleResponseService->responseValues($response, false, 'messages.invest_policy_' . $case, []);
            }


            $wallet = $user->getWallet(WalletType::Investor);


            $meta = [
                'reason' => TransactionReason::InvestingInListing,
                'reference' => transaction_ref(),
                'value_date' => now('UTC')->toDateTimeString(),
                'listing_id' => $listing->id,
                'listing_title_en' => $listing->title_en,
                'listing_title_ar' => $listing->title_ar,
                'user_id' => $user->id,
                'user_name' => "{$user->first_name} {$user->last_name}",
                'shares' => $toBeInvestedShares,
                'user_class'=>User::class,
                'wallat_balance_before_transaction'=>$wallet->balance,
                'subscription_token_id'=>$token->id
            ];

            $vatMeta = array_merge($meta, [
                'reason' => TransactionReason::InvestingInListingVatOfAdminFee,
                'reference' => transaction_ref(),
                'vat_percentage' => $toBeInvestedAmountAndFees['vatPercentage'],
            ]);

            $feesMeta = array_merge($meta, [
                'reason' => TransactionReason::InvestingInListingAdminFee,
                'reference' => $adminFeesTransactionRef = transaction_ref(),
                'fee_percentage' => $toBeInvestedAmountAndFees['feePercentage'],
            ]);

            $sp_responce=DB::select(
                'call investment_procedure(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                array(
                    $user->id,
                    User::class,
                    $wallet->id,
                    $listing->id,
                    $listing->total_shares,
                    $token->id,
                    $toBeInvestedShares,
                    $toBeInvestedAmountAndFees['amount']->getAmount(),
                    $toBeInvestedAmountAndFees['fee']->getAmount(),
                    $toBeInvestedAmountAndFees['vat']->getAmount(),
                    $toBeInvestedAmountAndFees['total']->getAmount(),
                    $toBeInvestedAmountAndFees['vatPercentage'],
                    $toBeInvestedAmountAndFees['feePercentage'],
                    defaultCurrency(),
                    json_encode($meta),
                    json_encode($vatMeta),
                    json_encode($feesMeta),
                    0,
                    Carbon::now()
                )
            );



            $investingRecord=ListingInvestorRecord::where('subscription_token_id',$token->id)->first();

            if(!$investingRecord) {
                $this->logService->log('INVESTMENT-EXCEPTION-SP', null, $sp_responce);
                throw new \Exception();

            }

            $values = ['investingRecord' => $investingRecord, 'adminFeesTransactionRef' => $adminFeesTransactionRef];

        } catch (\Exception $e) {
            $this->logService->log('INVESTMENT-EXCEPTION', $e);
            return $this->handleResponseService->responseValues($response, false, 'messages.invest_policy_' . '9', []);
        }

        $investingRecord = $values['investingRecord'];
        $adminFeesTransactionRef = $values['adminFeesTransactionRef'];
        $old_precentage=$listing->invest_percent;
        $listing->refresh();
        $this->updatesAfterInvesting($token, $listing, $old_precentage);

        $this->generateInvestFiles($listing, $user, $token, $toBeInvestedAmountAndFees, $investingRecord, $adminFeesTransactionRef);

        $this->userAlert($user, $listing, $toBeInvestedAmountAndFees, $toBeInvestedShares);

        $responseData = ['invested_shares' => $token->shares];

        return $this->handleResponseService->responseValues($response, true, 'messages.invest_success', $responseData);
    }

    public function dataValidation($request)
    {

        $rules = [
            'token' => 'required|string',
            'sub_mask' => 'nullable|string|max:191',
            'card_token' => 'nullable|string|max:191',
            'p_id' => 'nullable|string|max:191',
        ];

        $data = Validator::make($request->all(), $rules)->validate();

        return $data;
    }

    public function checkAgeOfUser()
    {

        $nationalIdentity=json_decode(request()->user_can_access_data->national_identity_data);

        $today = Carbon::now();

        $todayInHijri = Hijri::Date('Y-m-d', $today->format('Y-m-d'));

        $birth_date = $nationalIdentity ? $nationalIdentity->birth_date : null;

        $start_date = new \DateTime($birth_date);

        $birth_date_type = $nationalIdentity ? $nationalIdentity->birth_date_type : null;

        if ($birth_date_type == "hijri") {
            $end = new \DateTime($todayInHijri);
            $result = optional($start_date->diff($end))->y < 18 ? 0 : 1;
        } else {
            $end = new \DateTime($today);
            $result = optional($start_date->diff($end))->y < 18 ? 0 : 1;
        }

        return $result;
    }

    public function getToken($userId, $reference)
    {

        $response = ['status' => true];

        $token = InvestorSubscriptionToken::query() // lockForUpdate()
        ->where('user_id', $userId)
            ->where('is_expire', false)
            ->where('reference', $reference)
            ->whereNull('expired_at')
            ->first();

        if (!$token) {
            return $this->handleResponseService->responseValues($response, false, 'token not valid', []);
        }

        if ($token->authorization_expiry === null || now()->isAfter($token->authorization_expiry)) {
            return $this->handleResponseService->responseValues($response, false, 'messages.investor_subscription_token_auth_expired', []);
        }

        return $this->handleResponseService->responseValues($response, true, 'token valid', $token);
    }

    public function getListing($listingId, $user)
    {

        $listingConditions = [
            ['id', $listingId],
            ['is_visible', true],
        ];

        $listing = Listing::where($listingConditions)->first();

        if ($this->specialCanInvest->handle($listing, $user)) {
            return $listing;
        }

        return false;
    }

    public function transactions($user, $listing, $toBeInvestedAmountAndFees, $meta)
    {

        $userWallet = $user->getWallet(WalletType::Investor);

        $listingWalletEscrow = $listing->getWallet(WalletType::ListingEscrow);

        $amountWithoutFees = $toBeInvestedAmountAndFees['amount']->getAmount();

        DB::table('transactions')->insertGetId([
            'payable_type' => User::class,
            'payable_id' => $user->id,
            'wallet_id' => $userWallet->id,
            'type' => 'withdraw',
            'amount' => ($amountWithoutFees * -1),
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('transactions')->insertGetId([
            'payable_type' => Listing::class,
            'payable_id' => $listing->id,
            'wallet_id' => $listingWalletEscrow->id,
            'type' => 'deposit',
            'amount' => $amountWithoutFees,
            'confirmed' => 1,
            'meta' => json_encode($meta),
            'uuid' => Str::uuid(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        $vatAmount = $toBeInvestedAmountAndFees['vat']->getAmount();
        $vatPercentage = $toBeInvestedAmountAndFees['vatPercentage'];
        $adminFees = $toBeInvestedAmountAndFees['fee']->getAmount();
        $feePercentage = $toBeInvestedAmountAndFees['feePercentage'];

        $feesMeta = array_merge($meta, [
            'reason' => TransactionReason::InvestingInListingAdminFee,
            'reference' => $adminFeesTransactionRef = transaction_ref(),
            'fee_percentage' => $feePercentage,
        ]);

        if ($adminFees > 0) {

            DB::table('transactions')->insertGetId([
                'payable_type' => User::class,
                'payable_id' => $user->id,
                'wallet_id' => $userWallet->id,
                'type' => 'withdraw',
                'amount' => ($adminFees * -1),
                'confirmed' => 1,
                'meta' => json_encode($feesMeta),
                'uuid' => Str::uuid(),
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            $listingWalletEscrowAdminFee = $listing->getWallet(WalletType::ListingEscrowAdminFee);

            DB::table('transactions')->insertGetId([
                'payable_type' => Listing::class,
                'payable_id' => $listing->id,
                'wallet_id' => $listingWalletEscrowAdminFee->id,
                'type' => 'deposit',
                'amount' => $adminFees,
                'confirmed' => 1,
                'meta' => json_encode($feesMeta),
                'uuid' => Str::uuid(),
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            $vatMeta = array_merge($meta, [
                'reason' => TransactionReason::InvestingInListingVatOfAdminFee,
                'reference' => transaction_ref(),
                'vat_percentage' => $vatPercentage,
            ]);

            $feesMeta = array_merge($meta, [
                'reason' => TransactionReason::InvestingInListingAdminFee,
                'reference' => $adminFeesTransactionRef = transaction_ref(),
                'fee_percentage' => $feePercentage,
            ]);


            DB::table('transactions')->insertGetId([
                'payable_type' => User::class,
                'payable_id' => $user->id,
                'wallet_id' => $userWallet->id,
                'type' => 'withdraw',
                'amount' => ($vatAmount * -1),
                'confirmed' => 1,
                'meta' => json_encode($vatMeta),
                'uuid' => Str::uuid(),
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            $listingWalletEscrowVat = $listing->getWallet(WalletType::ListingEscrowVat);

            DB::table('transactions')->insertGetId([
                'payable_type' => Listing::class,
                'payable_id' => $listing->id,
                'wallet_id' => $listingWalletEscrowVat->id,
                'type' => 'deposit',
                'amount' => $vatAmount,
                'confirmed' => 1,
                'meta' => json_encode($vatMeta),
                'uuid' => Str::uuid(),
                'created_at' => now(),
                'updated_at' => now(),
            ]);

        }

        $user_amount = DB::table('transactions')
            ->where('payable_id', $user->id)
            ->where('payable_type', User::class)
            ->sum('amount');

        DB::table('wallets')
            ->where('holder_id', $user->id)
            ->where('holder_type', User::class)
            ->update(['balance' => $user_amount]);

        return $adminFeesTransactionRef;

        //        $listing_amount = DB::table('transactions')
        //            ->where('payable_id', $listing->id)
        //            ->where('payable_type', Listing::class)
        //            ->sum('amount');

        //        DB::table('wallets')
        //            ->where('holder_id', $listing->id)
        //            ->where('holder_type', Listing::class)
        //            ->update(['balance'=> $listing_amount]);


        //        $userWallet->refreshBalance();
        //        $listingWallet->refreshBalance();
    }

    public function listingInvestorShares($listingId, $user, $currentInvest, $toBeInvestedShares, $toBeInvestedAmountAndFees)
    {

        if (IsFirstInvestmentInListing::run($currentInvest, $user->id)) {
            $user->investments()
                ->attach($listingId, [
                    'invested_amount' => $toBeInvestedAmountAndFees['amount']->getAmount(),
                    'invested_shares' => $toBeInvestedShares,
                    'currency' => defaultCurrency()
                ]);
        } else {
            DB::table('listing_investor')
                ->where('listing_id', $listingId)
                ->where('investor_id', $user->id)
                ->update([
                    'invested_shares' => DB::raw("invested_shares + {$toBeInvestedShares}"),
                    'invested_amount' => DB::raw("invested_amount + {$toBeInvestedAmountAndFees['amount']->getAmount()}"),
                ]);
        }

    }

    public function userAlert($user, $listing, $toBeInvestedAmountAndFees, $toBeInvestedShares)
    {

        try {

            $notificationData = $this->notificationData($listing, $toBeInvestedAmountAndFees['total']->formatByDecimal(), $toBeInvestedShares);

            $this->send(NotificationTypes::Investment, $notificationData, $user->id);
            if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                Mail::to($user) //  TODO abdelaty@investaseel.sa CHANGE MAIL
                ->queue(new NewInvestmentTransaction($user, $listing, $toBeInvestedShares, $toBeInvestedAmountAndFees['total']));
            }
        } catch (\Exception $e) {
            $this->logService->log('INVESTMENT-USER-ALERT-EXCEPTION', $e);
            return false;
        }

        return true;
    }

    public function notificationData($listing, $amount, $shares)
    {

        $content_ar_1 = ' تم إستلام مبلغ قدره: ' . $amount;
        $content_ar_2 = ' للاستثمار في فرصة: ' . $listing->title_ar;
        $content_ar_3 = ' عدد الوحدات ' . $shares;

        return [
            'action_id' => $listing->id,
            'channel' => 'mobile',
            'title_en' => 'Received Investment Amount',
            'title_ar' => 'تأكيد إستلام مبلغ الاستثمار',
            'content_en' => 'An investment amount of has been received : ' . $amount . ' to invest in opportunity ' . $listing->title_en . ' and number of units: ' . $shares,
            'content_ar' => $content_ar_1 . $content_ar_2 . $content_ar_3,
        ];
    }

    public function updatesAfterInvesting($token, $listing, $oldPercentage)
    {

        //        $token->update([
        //            'expired_at' => now(),
        //            'is_expire' => true,
        //        ]);

        //        $oldPercentage = $listing->invest_percent;
        $newPercentage = $listing->percentage;
        $start_at = Carbon::parse($listing->start_at);

        if (($newPercentage - $oldPercentage) >= 1 || $newPercentage == 100) {
            //            $listing->invest_percent = $newPercentage >= 100 ? 100 : $newPercentage;
            //        $this->logService->log('INVEST-PRECENTAGE',null,['oldPercentage'=>$oldPercentage,'newPercentage'=>$newPercentage,'diff'=>($newPercentage - $oldPercentage)]);

            if ($start_at->isBefore(now())) {
                UpdateFundPercentageFirebase::dispatch($listing->id, $newPercentage)
                    ->onQueue('firebase');
            }

            $this->cacheServices->updateFundCache($listing->id);
        }

//        if ($listing->total_invested_shares >= $listing->total_shares) {

        if ($listing->real_invest_percent >= 100 || $listing->investment_shares == 0)
        {
            $listing->is_closed = 1;
            $listing->is_completed = 1;
            $listing->save();
        }


        if ($listing->is_closed) {
            AfterCloseListingAction::run($listing);
        }

    }

    public function generateInvestFiles($listing, $user, $token, $toBeInvestedAmountAndFees, $investingRecord, $adminFeesTransactionRef)
    {

        try {

            $feePercentage = $toBeInvestedAmountAndFees['feePercentage'];
            $vatPercentage = $toBeInvestedAmountAndFees['vatPercentage'];



            ExportSubFormAndAttachToInvestingRecord::dispatch(
                $investingRecord,
                $token,
                $user,
                $listing
            );

        } catch (\Exception $e) {
            $this->logService->log('INVESTMENT-GENERATE-FILES-EXCEPTION', $e);
            return false;
        }

        return true;
    }

    public function createCardToken($user, $request)
    {
        if (!$request->sub_mask && !$request->card_token) {
            return false;
        }

        $cardToken = null;

        $repeatedSubMusk = CardToken::where("sub_mask", $request->sub_mask)->where("user_id", $user->id)->first();

        if ($repeatedSubMusk) {
            $cardToken = $repeatedSubMusk->update([
                "sub_mask" => $request->sub_mask,
                "card_token" => $request->card_token,
                "user_id" => $user->id,
            ]);
        } else {
            $cardToken = CardToken::create([
                "sub_mask" => $request->sub_mask,
                "card_token" => $request->card_token,
                "user_id" => $user->id,
            ]);
        }
        return $cardToken;
    }

}
