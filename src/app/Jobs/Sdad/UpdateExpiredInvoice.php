<?php

namespace App\Jobs\Sdad;

use App\Models\EdaatInvoice;
use App\Services\LogService;
use App\Services\Sdad\Enums\InvoiceCodesStatusesEnums;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Support\Facades\DB;

class UpdateExpiredInvoice implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $logServices;
    public $invoiceId;

    public function __construct($invoiceId)
    {
        $this->logServices = new LogService();
        $this->invoiceId = $invoiceId;
    }

    public function handle()
    {
        try {

            $invoice = EdaatInvoice::find($this->invoiceId);

            if (!$invoice) {
                $this->logServices->log('SDAD-UPDATE-EXPIRED-INVOICE-SPECIAL', null, ['invoice not valid', $this->invoiceId]);
            }

            if ($invoice->status != InvoiceCodesStatusesEnums::PAID) {
                $invoice->status = InvoiceCodesStatusesEnums::EXPIRED;
                $invoice->save();
            }

        } catch (\Exception $e) {
            $this->logServices->log('SDAD-UPDATE-EXPIRED-INVOICE-SPECIAL', $e);
        }

    }

}
