<?php

namespace App\Enums\Listings;

use BenSampo\Enum\Enum;

final class AttachmentType extends Enum
{
    const DueDiligence = 'DUE_DILIGENCE';
    const Details = 'DETAILS';
    const Location = 'LOCATION';
    const Risks = 'RISKS';
    const FinancialDetails = 'FINANCIAL_DETAILS';
    const ProofDocs = 'PROOF_DOCS';
}
