<?php

namespace App\Support\Elm\AbsherOtp\Exceptions;

use Exception;

class InternalConnectionException extends Exception
{
    protected $reason;

    public function __construct($reason)
    {
        $this->reason = $reason;
        parent::__construct('Failed to connect to Absher OTP');
    }

    public function context()
    {
        return [
            'reason' => $this->reason
        ];
    }
}
