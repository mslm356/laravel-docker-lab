<?php

namespace App\Enums\Zoho;

use BenSampo\Enum\Enum;

final class NewCustomer extends Enum
{
    const Fund = 'fund';
    const Customer = 'customer';
}
