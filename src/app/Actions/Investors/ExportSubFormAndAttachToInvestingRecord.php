<?php

namespace App\Actions\Investors;

use App\Models\File;
use App\Enums\FileType;
use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Support\Str;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Actions\ExportSubscriptionForm;
use App\Models\Listings\ListingInvestorRecord;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Models\Users\InvestorSubscriptionToken;

class ExportSubFormAndAttachToInvestingRecord
{
    use AsAction;

    protected $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }

    /**
     * Handle exporting subscription form and attach it to a transaction
     *
     * @param \App\Models\Users\InvestorSubscriptionToken $subscriptionToken
     * @param \App\Models\Users\User $user
     * @param \App\Models\Listings\Listing $listing
     * @param \Illuminate\Support\Collection $currentInvestments
     * @return \App\Models\File
     */
    public function handle(ListingInvestorRecord $record, InvestorSubscriptionToken $subscriptionToken, User $user, Listing $listing, $formData=null)
    {

        try {

            if ($formData==null)
                $formData = GetSubscriptionFormData::run(
                    $subscriptionToken,
                    $user,
                    $listing,
                    $record->amount_without_fees, null
                //$currentInvest->where('investor_id', $user->id)
                );

            $todayDate = now('Asia/Riyadh')->format('Y-m-d');

            $html = RenderSubscriptionForm::run($formData)->render();

            /** @var \Illuminate\Http\UploadedFile $file */
            $filePath = ExportSubscriptionForm::run(
                $originalFileName = "{$todayDate}-{$subscriptionToken->id}-{$user->id}-subscriptionForm.pdf",
                $html
            );

            $fileModel = File::create([
                'id' => (string)Str::uuid(),
                'name' => $originalFileName,
                'fileable_id' => $record->id,
                'fileable_type' => $record->getMorphClass(),
                'type' => FileType::SubscriptionForm,
                'extra_info' => collect([
                    'listing_id' => $listing->id,
                    'data' => $formData
                ]),
                'path' => $filePath
            ]);

            return $fileModel;

        } catch (\Exception $e) {
            $this->logServices->log('SUBSCRIPTION-FORUM-EXCEPTION', $e);
        }
    }

    public function asJob(...$args)
    {
        DB::transaction(function () use ($args) {
            $this->handle(...$args);
        });
    }
}
