<?php

return [
    'subject' => 'Approval of your corporate account',
    'message_1' => 'Welcome to Aseel platform',
    'message_2' => 'We would like to inform you that your registration with us is confirmed',
    'message_3' => 'We are glad to have you in Aseel platform',
    'link_text' => 'Check your profile',
    'message_4' => 'You can now recharge your balance, invest in the opportunities, and manage your investment portfolio through your control panel via the following link'
];
