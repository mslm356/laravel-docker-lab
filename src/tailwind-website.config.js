const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: {
        content: [
            './resources/views/**/*.blade.php',
            './resources/js/website/**/*.vue',
            './app/Http/Controllers/Welcome.php',
        ],
        safelist: [
            'text-huge',
            'text-big',
            'md:text-huge',
            'md:text-big',
            'p[style*="text-align:center"] > img',
        ],
    },
    theme: {
        extend: {
            typography: {
                DEFAULT: {
                    css: {
                        a: {
                            color: '#343549',
                            '&:hover': {
                                color: '#717280',
                            },
                        },
                    },
                },
            },
            colors: {
                primary: {
                    '50': '#BBD5FF',
                    '100': '#A6C9FF',
                    '200': '#7DB0FF',
                    '300': '#5598FF',
                    '400': '#2C7FFF',
                    '500': '#0366FF',
                    '600': '#004FCA',
                    '700': '#003992',
                    '800': '#00235A',
                    '900': '#000D22',
                },
                'primary-inv': {
                    '50': '#050a17',
                    '100': '#050a17',
                    '200': '#050a17',
                    '300': '#050a17',
                    '400': '#fff',
                    '500': '#fff',
                    '600': '#fff',
                    '700': '#fff',
                    '800': '#fff',
                    '900': '#fff',
                },
                secondary: {
                    '50': '#A3EED0',
                    '100': '#92EBC7',
                    '200': '#6FE4B6',
                    '300': '#4DDEA4',
                    '400': '#2AD793',
                    '500': '#22B77C',
                    '600': '#19885C',
                    '700': '#10583C',
                    '800': '#08291C',
                    '900': '#000000',
                },
                'secondary-inv': {
                    '50': '#050a17',
                    '100': '#050a17',
                    '200': '#050a17',
                    '300': '#050a17',
                    '400': '#050a17',
                    '500': '#050a17',
                    '600': '#fff',
                    '700': '#fff',
                    '800': '#fff',
                    '900': '#fff',
                },
            },
            fontFamily: {
                sans: [
                    "'HelveticaNeueLT Arabic 55 Roman'",
                    'Alata',
                    ...defaultTheme.fontFamily.sans,
                ],
                heading: ["'HelveticaNeueLT Arabic 75'", 'Alata'],
            },
            fontSize: {
                '7xl': '5rem',
                '8xl': '6rem',
            },
            boxShadow: {
                'outline-primary': '0 0 0 3px rgba(52,95,148,.45)',
            },
            spacing: {
                '-13': '-3.25rem',
                '-14': '-3.5rem',
                '13': '3.25rem',
                '14': '3.5rem',
            },
        },
    },
    variants: {
        padding: ['responsive', 'hover', 'focus', 'direction'],
        margin: ['responsive', 'hover', 'focus', 'direction'],
        inset: ['responsive', 'hover', 'focus', 'direction'],
        translate: ['responsive', 'hover', 'focus', 'direction'],
        textAlign: ['responsive', 'hover', 'focus', 'direction'],
        cursor: ['responsive', 'hover', 'focus', 'disabled'],
        backgroundColor: ['responsive', 'hover', 'focus', 'disabled'],
        flexDirection: ['responsive', 'hover', 'focus', 'direction'],
        space: ['responsive', 'direction'],
        scale: ['responsive', 'hover', 'focus', 'group-hover'],
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/forms'),
        require('tailwindcss-dir')(),
    ],
};
