<?php

namespace App\Jobs\Investor;

use App\Services\LogService;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class DeleteKycPdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $logService;
    public $path;

    public function __construct($path)
    {
        $this->logService = new LogService();
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $driver = config('filesystems.private');
            Storage::disk($driver)->delete($this->path);

        } catch (ValidationException $e) {
            $this->logService->log('VERIFY-ACCOUNT-VALIDATION ', $e);
        } catch (VerificationSmsException $e) {
            $this->logService->log('VERIFY-ACCOUNT-Verification ', $e);
        } catch (\Exception $e) {
            $this->logService->log('VERIFY-ACCOUNT-EXC', $e);
        }
    }
}
