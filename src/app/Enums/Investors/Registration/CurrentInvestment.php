<?php

namespace App\Enums\Investors\Registration;

use App\Enums\Investors\Registration\Interfaces\DBColumInterface;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class CurrentInvestment extends Enum implements LocalizedEnum,DBColumInterface
{
    const RealEstate =   1;
    const CaptialMarket =   2;
    const PrivateEquity =   3;
    const Other = 4;


    function getDBColumnName()
    {
        return 'current_invest';
    }

}
