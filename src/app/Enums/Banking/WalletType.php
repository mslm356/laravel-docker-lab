<?php

namespace App\Enums\Banking;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class WalletType extends Enum implements LocalizedEnum
{
    const Investor = 'investor-wallet';
    const AnbStatementCollecter = 'anb-statement-collecter-wallet';
    const ListingSpv = 'listing-spv-wallet';
    const ListingEscrow = 'listing-escrow-wallet';
    const ListingEscrowVat = 'listing-escrow-vat-wallet';
    const ListingEscrowAdminFee = 'listing-escrow-admin-fee-wallet';
}
