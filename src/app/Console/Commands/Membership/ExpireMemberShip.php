<?php

namespace App\Console\Commands\Membership;

use Alkoumi\LaravelHijriDate\Hijri;
use App\Enums\Banking\WalletType;
use App\Enums\Role;
use App\Jobs\AbsherTest;
use App\Jobs\Funds\SendAlertMailsOnCreateReport;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\VirtualAccount;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use App\Services\MemberShip\CreateRequest;
use App\Services\MemberShip\SpecialCanInvest;
use App\Settings\ZatcaSettings;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireMemberShip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:member-ship';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire member ship';

    public $memberShipService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->memberShipService = new CreateRequest();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $this->memberShipService->expireMemberships();

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }


}
