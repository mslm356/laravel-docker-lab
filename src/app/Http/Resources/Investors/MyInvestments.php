<?php

namespace App\Http\Resources\Investors;

use App\Enums\InvestmentType;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class MyInvestments extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $investedAmount = $this->pivot->invested_amount;
        $investedShares = $this->pivot->invested_shares;

        $images=['id'=>$this->id,'name'=>$this->title,'url'=>Storage::disk(config('filesystems.public'))->url($this->default_image)];

        return [
            'id' => $this->id,
            'title' => $this->title,
            'has_vote' => $this->votes->first() ? true : false,
            'invested_shares' => $investedShares,
            'invested_amount_fmt' => number_format(money($investedAmount)->formatByDecimal(), 2),
            'images'=>[$images],
            'investment_type' => InvestmentType::getDescription($this->investment_type),
            'created_at' => $this->created_at->format('Y-m-d h:i'),
            'updated_at' => $this->updated_at->format('Y-m-d h:i'),
        ];
    }
}
