<?php

namespace App\Support\Users\Invitations;

use App\Enums\Role as EnumsRole;
use App\Models\Users\User;
use App\Models\Users\UserInvitation;
use Spatie\Permission\Models\Role;

class InvitationManager
{
    /**
     * Invitation repository token
     *
     * @var \App\Support\Users\Invitations\InvitationTokenRepository
     */
    protected $tokens;

    public function __construct(InvitationTokenRepository $tokens)
    {
        $this->tokens = $tokens;
    }

    public function createInvitationLink($email, Role $role, User $inviter)
    {
        $token = $this->tokens->create($email, $role, $inviter);

        $url = config('app.front_urls.' . EnumsRole::getScopeOfRole($role->name));

        return "{$url}/sign-up/invitations?token={$token}&email={$email}";
    }

    public function accept($email, $token, $action)
    {
        $invitation = $this->validate($email, $token);

        if (!$invitation instanceof UserInvitation) {
            return $invitation;
        }

        $user = User::role($invitation->role_id)
            ->where('email', $email)
            ->first();

        if ($user->password !== null) {
            return ValidationResponse::USER_EXISTS;
        }

        $action($user, $invitation);

        $this->tokens->deleteExisting($email, Role::findById($invitation->role_id));

        return ValidationResponse::INVITATION_ACCEPTED;
    }

    protected function validate($email, $token)
    {
        if (!$this->tokens->validateTokenFormat($token)) {
            return ValidationResponse::INVALID_TOKEN;
        }

        if (!($invitation = $this->tokens->exists($email, $token))) {
            return ValidationResponse::INVALID_INVITATION;
        }

        return $invitation;
    }
}
