<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaint_types', function (Blueprint $table) {
            $table->id();
            $table->string('ar_name');
            $table->string('en_name');
            $table->timestamps();
        });

        DB::table('complaint_types')->insert(
            [
            ['ar_name' => 'تقنية', 'en_name' => 'Technical'],
            ['ar_name' => 'مالية', 'en_name' => 'Finance'],
            ['ar_name' => 'قانونية', 'en_name' => 'Legal'],
            ['ar_name' => 'استثمار', 'en_name' => 'Investment'],
            ['ar_name' => 'اخرى', 'en_name' => 'Other'],]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaint_types');
    }
}
