<?php

namespace App\Support\Elm\AbsherOtp;

use SoapClient;
use Illuminate\Support\Facades\App;

class AbsherOtpSoapClient
{
    public static function instantiate()
    {
        return new SoapClient(config('services.absher_otp.url'), [
            'trace' => App::environment('local'),
            'local_cert' => storage_path('app/elm/aseel_capital_absher_otp.pem'),
            'cache_wsdl' => WSDL_CACHE_NONE,
            'soap_version' => SOAP_1_1,
            'location' => rtrim(config('services.absher_otp.url'), '?wsdl')
        ]);
    }
}
