<?php

namespace App\Mail\companyProfile;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Create extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profileUrl = config('app.investor_url') . '/app/overview';

        return $this->view('emails.company_profiles.create', compact('profileUrl'))
            ->subject(__('emails/company_profiles/create.subject'));
    }
}
