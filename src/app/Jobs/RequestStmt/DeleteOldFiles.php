<?php

namespace App\Jobs\RequestStmt;

use App\Services\LogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class DeleteOldFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $logServices;
    public $filePath;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filePath)
    {
        $this->logServices = new LogService();
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            if (Storage::disk(config('filesystems.private'))->exists($this->filePath)) {
                Storage::disk(config('filesystems.private'))->delete($this->filePath);
            }

        } catch (\Exception $e) {
            $this->logServices->log('Request-STMT-DELETE-FILE', $e);
        }
    }
}
