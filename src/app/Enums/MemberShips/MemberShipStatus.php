<?php

namespace App\Enums\MemberShips;

use BenSampo\Enum\Enum;

final class MemberShipStatus extends Enum
{
    const Pending = 1;
    const Rejected = 2;
    const Approved = 3;
    const Expired = 4;
}
