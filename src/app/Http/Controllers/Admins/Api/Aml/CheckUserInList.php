<?php

namespace App\Http\Controllers\Admins\Api\Aml;

use App\Enums\AML\AmlList;
use App\Enums\AML\AmlStatus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CheckUserInList extends Controller
{
    /**
     * Parse AML list.
     *
     * @param \App\Support\Aml\AmlListInterface $list
     * @param \App\Models\Users\User $user
     * @param \App\Models\AML\AmlHistory $amlHistory
     * @return \Illuminate\Http\JsonResponse
     */
    public static function do($list, $user, $amlHistory)
    {
        $failed = false;
        $exception = null;

        try {
            $suspected = $list->check(
                $user->first_name,
                '',
                '',
                $user->last_name,
                '',
                $user->date_of_birth,
                $user->investor->nin,
                ''
            );
        } catch (\Throwable $th) {
            $failed = true;
            $exception = $th->getMessage();
            Log::critical('UNABLE_TO_CHECK_USER', [
                'list' => AmlList::getDescription($list->getListIndex()),
                'exception' => $th,
                'user_id' => $user->id
            ]);
        }

        if ($failed) {
            $status = AmlStatus::NotChecked;
            $details = ['reason' => $exception];
        } else {
            $status = (count($suspected) === 0 ? AmlStatus::Passed : AmlStatus::Found);
            $details = ['found' => $suspected];
        }

        $amlHistory->checks()->create([
            'list' => $list->getListIndex(),
            'status' => $status,
            'details' => $details
        ]);
    }
}
