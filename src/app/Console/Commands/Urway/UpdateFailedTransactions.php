<?php

namespace App\Console\Commands\Urway;

use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Exports\Wallets\FailedTransactionUrway;
use App\Models\Listings\Listing;
use App\Models\Urway\UrwayTransaction;
use App\Models\Users\InvestorSubscriptionToken;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Services\Investors\URwayServices;
use App\Services\LogService;
use Carbon\Carbon;
use Cknow\Money\Money;
use Dflydev\DotAccessData\Data;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class UpdateFailedTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'urway:update-failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create customers on zoho';

    public $urwayServices;
    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->urwayServices = new URwayServices();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        return DB::transaction(function () {

            try {

                InvestorSubscriptionToken::query()
                    ->whereNotNull('authorization_expiry')
                    ->whereNull('expired_at')
                    ->where('payment_type', 'urway')
                    ->where('listing_id', 24)
                    ->lazyById()
                    ->each(function ($token) {

                        $urwayTransaction = $token->urwayTransaction;

                        $amount = \money( $urwayTransaction->amount, defaultCurrency());

                        $responseOfUrway = $this->urwayServices->callUrway($token->reference, $amount, '');

                        if (isset($responseOfUrway['status']) && $responseOfUrway['status']) {

                            $result = $responseOfUrway['data'];

                            if (isset($result['responseCode']) && $result['responseCode'] === '000' && $result['result'] === 'Successful') {

                                $user = $token->user;

                                $userWallet = $user->getWallet(WalletType::Investor);

                                $userWallet->deposit(
                                    $urwayTransaction->amount,
                                    [
                                        'reference' => transaction_ref(),
                                        'reason' => TransactionReason::UrwayReason,
                                        'rrn' => $result['rrn'],
                                        'details' => [
                                            'version' => 1,
                                            'source' => 'urway',
                                            'tranid' => $result['tranid'],
                                            'trackid' => $result['trackid'],
                                            'rrn' => $result['rrn'],
                                            'urway_transactions' => 'failed',
                                        ],
                                        'value_date' => Carbon::now()->timezone('UTC')->toDateTimeString(),
                                    ]
                                );

                                $userWallet->refreshBalance();

                                $urwayTransaction->status = 'updated';
                                $urwayTransaction->response = 'updated';
                                $urwayTransaction->save();

                            } else {

                                $this->logService->log('UPDATE-TRANSACTIONS-URWAY-error-code', null, ['urway code result', $responseOfUrway]);
                            }

                        } else {

                            $this->logService->log('UPDATE-TRANSACTIONS-URWAY-RESPONSE', null, ['status false', $responseOfUrway]);
                        }

                    });

            } catch (\Exception $e) {
                $this->logService->log('UPDATE-FAILED-TRANSACTIONS-URWAY-EXCEPTION', $e);
            }

        });

    }


    public function test()
    {

        $items = ['CmYMrWRszDDwoSpSeUyLhmG7e',
            'QWMv3jRQK63aZ19k0sd8M1grs',
            'kxOJQMwT5x2GKrCspYgi8iYAf',
            'htXATLb70TZW9T9PDf3PKRaSN',
            'VB6x6G2iwSLc4tRhiSIEU9kQu',
            'ULWGsYcKmcXFtgmguIZm4l1YR',
            'LQc7zdqmfQWkuvZszqX8brEAX',
            'MLNo7KYHqV71DEswXu7Lyvfjk',
            '1Ud9RgpQC4ODPwXIrwWIXt5bv',
            'AiPN9mMSnFWzUc6jTXpNoe8w9',
            'TInbarkzCVoOOAQNQoa9ojoiF',
            'ilZExyfN96BJn5V0u4pc2MYnt',
            'RdleQTCwtHLuarnGKuted99ey',
            'bEMi9hlYUCqIG41XAoBXawekT',
            'cWdZ4R1cWQV25RZ79N2FtZW1K',
            '5B4IQ8W5Yu55KGnAOlwLFQg1O',
            'c6iogHaDQWwH2a4UJpHcQRZgm',
            'he0MgBEGAtNBOTLhT2bfMeo24',
            'cQa0uZRiqI64gPTfiohNmGpcO',
            '7Suxqn03FKU7fLjB9sbGWZdyK',
            'e02w24glr1TTOzFUu0VyezKGx',
            'oCIglwdOLIDTMPKATGxvjJqfg',
            'x9XW5POCAR1dNTwKuIZi794ZP',
            'o5lwz8DHrulOculsLxezsmqAs',
            'wv2Qyu0qt9R1Qpz3GdC4u6qsm',
            '15Vdk0RiX93A7Frk7EsTCaTKg',
            'OiYj5vaagiXd31ai15EjRRXVT',
            'bv9EIeL1sDD547FXhWcR3QDc1',
            '4eEHBOLHJdT8CFKTJPXsDyO6a',
            'QpPn8xIfWasfhAauIcd2whHQV',
            'z6JSLRYv6Atl8pqOSIeGvqQ6z',
            'kgEeAd65W7aY2pHm2TLojzbRu',
            'c71USkobJO3fF6UDKgkOEAWzV',
            '2C6qOiFHjNmJQuy5wyhJSYLNA',
            'GvCJh3DPKuRWH7mR97anhmEPF',
            'uIy3XtksWJoGlxBsfoZZV3MkW',
            'SyUn1r689gbeYFkJWBDCtUR9U',
            'Az3tRQIzGsXzVYAAmiY3HbgOO',
            'vSJBIYY6w6Li3jzEdRZoiZHHM',
            '7NGX0dga15sqSuUBbAIsk9hVp',
            'f90IzeeYzyPcFIxR70W9Xltd1',
            'm6N1MvrrXoWWD1cfll5AzcZCA',
            'QtuSp8iWGU8xBfZk7NIhKyu7I',
            'JR8TDRoFHCshI8AuTVXXbOsAd',
            '4fiPam1QTkNdEvJZHcTjjpVev',
            'BH34jCnxwnrVmYSg7uYLKEigW',
            'N8RWXbAdGweGpArTK7LM465VF',
            'kkwwotMz1PGnv9tPttO44qMpI',
            'kgunNVzRz5I0Yszyjqtzy7bDJ',
            '9U1SmYKBYMjRI0BGjmnHDrLEk',
            'KKvH2QE6JCtqeM2dpswmoqsHH',
            'yTWRf3lpZM5HEKTvIdBxYHCa8',
            'wAtaopMMzZ8RXT9u0s0tOBTIV',
            'EmkWoZLtPCKFUe2PkCYCfVo94',
            'a6zUimMtvmFE6VD6dngW4adaN',
            'XfJa1G1d5UPfEvf17ndKGu8PN',
            '23O8XbOuPpaOXPlKcYMuQVf79',
            'fWtu9yGL2mZDfJXBeKzbzb5MC',
            'qOo3G3IthSc8SyYG1whFmgSMn',
            'Dj71KyqhLN5Re0H77V6GNfJNH'];

        foreach ($items as $item) {

            $transactions = Transaction::where('reason', 8)->whereJsonContains('meta->details->trackid', $item)->get()->pluck('id')->toArray();

            for ($i = 1; $i < count($transactions); $i++) {

                DB::table('transactions')->where('id', $transactions[$i])->delete();
                dump($transactions[$i]);
            }

            dump($item);
        }

        dd(1);

    }
}
