<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Styles -->
    <link href="{{ asset('css/website.css') }}" rel="stylesheet">
    <style>
        @yield('style')

@font-face {
            font-family: 'HelveticaNeueLT';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/HelveticaNeueLTArabic-Roman.woff2'))) }}") format('woff2');
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'HelveticaNeueLT';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/HelveticaNeueLTArabic-Bold.woff2'))) }}") format('woff2');
            font-weight: bold;
            font-style: normal;
            font-display: swap;
        }

        @font-face {
            font-family: 'Alata';
            src: url("data:application/font-woff;charset=utf-8;base64,{{ base64_encode(file_get_contents(public_path('/fonts/Alata-Regular.woff2'))) }}") format('woff2');
            font-weight: bold;
            font-style: normal;
            font-display: swap;
        }

        body {
            font-family: 'HelveticaNeueLT';
        }

    </style>
</head>

<body>
@yield('content')
</body>

</html>
