<?php

namespace App\Enums\SubscriptionForm;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class RequestStatus extends Enum implements LocalizedEnum
{
    const PendingReview = 1;
    const InReview = 2;
    const Approved = 3;
    //const Rejected = 4;
}
