<?php

namespace App\Http\Resources\Admins\Listings;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ShowVoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->investor_vote_id,
            'user' => $this->full_name,
            'answer' =>  $this->vote_answer,
            'invested_shares' =>  $this->invested_shares,
            'created_at' => $this->created_at ? Carbon::parse($this->created_at)->timezone('Asia/Riyadh')->format('y-m-d h:i:s') : null,
        ];
    }
}
