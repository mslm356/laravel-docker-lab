<?php


namespace App\Support\Zoho;


use App\Enums\Zoho\NewCustomer;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Customer
{
    public $base_url;
    public $organization_id;
    public $auth;

    public function __construct()
    {

        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
    }

    public function prepareCustomerData($customer)
    {

        $nationalIdentity = $customer->nationalIdentity ? $customer->nationalIdentity->nin : '';

        $data = [
            'contact_name' => $nationalIdentity . '-' . $customer->full_name,
        ];

        return $data;
    }

    public function prepareFundData($fund)
    {
        $title = 'title_' . app()->getLocale();

        $data = [
            'contact_name' => $fund->id . '-' . $fund->$title,
        ];

        return $data;
    }

    public function saveCustomer($item, $type = NewCustomer::Customer)
    {

        $token = $this->auth->getToken();

        try {

            $data = $type == NewCustomer::Fund ? $this->prepareFundData($item) : $this->prepareCustomerData($item);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'contacts?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {
                $item->zoho_id = $responseData['contact']['contact_id'];
                $item->save();

                return true;
            }

            Log::error('ZOHO-NEW-CUSTOMER', [$responseData]);

        } catch (\Exception $e) {
//            dd($e->getMessage(), $e->getFile(), $e->getLine());
            Log::error('ZOHO-NEW-CUSTOMER', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }

}
