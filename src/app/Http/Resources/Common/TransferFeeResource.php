<?php

namespace App\Http\Resources\Common;

use App\Settings\AccountingSettings;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Support\VirtualBanking\Helpers\LocalSaudiTransferFee;

class TransferFeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'value' => $fee = $this->getFee(),
            'delivery_date' => $this->getDeliveryDate()->toDateString(),
            'breakdown' => [
                'amount' => cleanDecimal(number_format($this->getAmount()->formatByDecimal(), 2)),
                'fee' => "0",
                'fee_vat' => "0",
                'vat_percentage' => app(AccountingSettings::class)->vat->percentage,
                'total' => cleanDecimal(
                    number_format($this->getAmount()->formatByDecimal(), 2)
                ),
            ]
        ];
    }
}
