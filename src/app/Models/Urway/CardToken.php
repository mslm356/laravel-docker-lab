<?php

namespace App\Models\Urway;

use App\Models\AppModel;

class CardToken extends AppModel
{
    protected $table = "card_tokens";
    protected $fillable = ["sub_mask","card_token","user_id"];
}
