<?php


return [
    'during_time' => env('MEMBER_SHIP_DURING_TIME', 24) ,  // by month
    'gold' => env('MEMBER_SHIP_GOLD', 99999),
    'diamond' => env('MEMBER_SHIP_DIAMOND', 999999),
    'membership_limit' => env('MEMBER_SHIP_LIMIT', 60),
];
