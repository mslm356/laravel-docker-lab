<?php

namespace App\Http\Controllers\Investors\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Http\Resources\Investors\Elm\AbsherInfoResource;

class ShowAbsherInfo extends Controller
{
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Support\VirtualBanking\BankService
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = $request->user('sanctum');

        return new AbsherInfoResource($user);
    }
}
