<div class="flex items-center p-3 {{ $dark ? 'bg-gray-50' : '' }}">
    <div class="w-1/3 divide-y divide-gray-200">
        <div class="py-2">{{ $field['ar'] }}</div>
        <div class="py-2 text-right" dir="ltr">{{ $field['en'] }}</div>
    </div>

    <div class="w-2/3 text-center">
        {{ $slot }}
    </div>
</div>
