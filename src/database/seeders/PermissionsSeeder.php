<?php

namespace Database\Seeders;

use App\Models\Users\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Artisan::call('config:cache');
        \Artisan::call('cache:clear');

        $permissions = [
//            'roles' => [
//                 'manage-roles',
//                 'manage-roles-id',
//                 'list-admins',
//                 'admin-assign-role',
//            ],
//            'property-types' => [
//                'fund-types', // manage-property-types
//            ],
//            'bank-account' => [
//                'bankEditor', // 'update-bank-account'
//                'update-bank-account-status',
//                'bank-accounts', // manage-bank-accounts
//            ],
//            'authorized-entities' => [
//                'authorized-entities-approved_with_bank_accounts',
//                'authorized-entities-id', // update-authorized-entities-status
//                'authorized-entities', // manage-authorized-entities
//                'authorized-entities-users', // manage-authorized-entities-users
//            ],
//            'contact-reasons' => [
//                'contact-reasons', // manage-contact-reasons
//            ],
//            'enquiries' => [
//                'enquiries', // manage-enquiries
//                'create-enquiries-replies',
//            ],
//            'website-contents' => [
//                //'manage-website-contents',
//                'pages',
//                'pages-one',
//            ],
//            'listing-discussion' => [
//                'create-listing-discussion-replies',
//                'update-listing-discussion-reply',
//                'detele-listing-discussion-replies',
//                'funds-id-discussions', // list-listing-discussions
//                'export-listing-discussions',
//                'show-listing-discussion',
//                'delete-listing-discussion',
//            ],
            'listing' => [
//                'funds', // manage-listings
//                'funds-id', // manage-listings
//                'funds-id-show',
//                'funds-id-payouts', // manage-listings-payouts
//                'funds-id-distribution-show_payout', // manage-listings-payouts
//                'funds-id-distribution',
//                'funds-id-reports', //manage-listings-reports
//                'funds-id-reports-show_report',
//                'funds-id-revisions', //list-listing-revisions
//                'funds-id-valuations', //manage-listings-valuations
//                'listing-payouts-pay',
//                'listing-payouts-create',
                'listing-payouts-approval',
                'listing-payouts-approval-two',
//                'list-listing-wallets',
//                'show-listing-balance',
//                'list-listing-investors',
//                'funds-id-accounts',
//                'export-listing-investors',
//                'list-listing-transactions',
//                'update-listing-status',
//                'manage-listings-files',
//
            ]
//            'professional-requests' => [
//                'professional-requests', // list-upgrade-requests
//                'professional-requests-id', // list-investor-upgrade-requests
//            ],
//            'anb-accounts' => [
//                'anb', // list-anb-accounts
//            ],
//            'eod' => [
//                'anb-transactions', // list-eod-transactions
//                'export-eod-statements',
//                'list-eod-statements',
//                'show-eod-statement-transactions',
//                'export-eod-statements-transactions',
//                'show-eod-statement',
//            ],
//            'logs' => [
//                'logs', // list-logs
//                'delete-logs',
//                'worker-logs', // list-worker-logs
//                'delete-worker-logs',
//            ],
//            'investors' => [
//                'show-investor-upgrade-request',
//                'update-investor-upgrade-request-status',
//                'investors', //list-investors
//                'investors-id',
//                'investors-editor', // update-investor
//                'companies', // investors?
//                'companies-id',
//                'companies-editor',
//                'list-investor-investments',
//                'list-investor-transactions',
//                'export-investor-transactions',
//                'list-investor-balance-inquiry',
//                'update-investor-status',
//                'show-investor-profile',
//                'delete-investor',
//
//            'change-wallet-status'
//
//            ],
//
//            'faqs' => [
//                'faqs', // manage-faqs
//            ],
//            'vats' => [
//                'manage-vats',
//            ],
//            'aml' => [
//                'aml-check-user',
//                'aml-list-user',
//            ],
//            'dashboard' => [
//                'dashboard', // dashboard-overview, dashboard-listings
//                //'dashboard-listings',
//                //'upload-editor-images',
//                //'upload-images',
//            ],
//            'reports' => [
//                // 'reports', // list-report-types
//                'export-reports',
//            ],
//            'company-profile' => [
//                'approve-company-profile',
//            ],
//            'listing-votes' => [
//                'end-listing-vote',
//                'votes', //list-listing-votes, show-listing-vote
//                'votes-id', // create-listing-votes
//            ],
//            'transfer-requests' => [
//                'transfer_review', // list-transfer-requests
//                'show-transfer-request',
//                'transfer_accept', // update-transfer-request-status, accept-transfer-requests
//                //'accept-transfer-requests',
//            ],
//            'files' => [
//                'download-files',
//                'delete-files',
//            ],
//            'memberships' => [
//                'investors-membership',
//                'create-investor-membership',
//            ],
//            'other' => [
//                'access-admin-portal',
//                'dashboard', // ?
//                'investors',
//                'enquiries',
//                'investors-id', //view
//                'investors-editor', // edit
//                'bankEditor', //edit
//                'fund-types',
//                'pages',
//                'pages-one',
//                'anb',
//                'anb-transactions',
//                'bank-accounts',
//                'authorized-entities',
//                'authorized-entities-id',
//                'authorized-entities-users',
//                'companies', // ?
//                'companies-id',
//                'companies-editor',
//                'votes',
//                'votes-id',
//                'reports',
//                'funds',
//                'funds-id',
//                'funds-id-accounts', // ?
//                'funds-id-show',
//                'funds-id-payouts',
//                'funds-id-discussions',
//                'funds-id-reports',
//                'funds-id-reports-show_report',
//                'funds-id-revisions',
//                'funds-id-valuations',
//                'funds-id-distribution-show_payout', // ?
//                'faqs',
//                'contact-reasons',
//                'worker-logs',
//                'professional-requests',
//                'professional-requests-id',
//                'funds-id-distribution', // ?
//                'transfer_review',
//                'transfer_accept',
//                'investors-membership'
//            ],
//
//
//            'account-statement' => [
//                'account-statement',
//                'update-account-statement-status',
//                'account-statement-view',
//            ],


//            'complaints' => [
//                'view-all-complaints',
//                'show-complaints',
//                'update-complaints',
//                'get-investor-complaints',
//            ],

//            'campaign' => [
//                'campaign',
//                'campaign-create',
//                'campaign-view',
//                'campaign-status',
//                'campaign-edit',
//            ],

        ];


        try {
            Permission::findByName('reports')->delete();
        } catch (\Exception $e) {
        }

        try {
            Permission::where('name', 'open-listing-wallet')->delete();
            Permission::where('name', 'list-upgrade-requests')->delete();
            Permission::where('name', 'list-investor-upgrade-requests')->delete();
            Permission::where('name', 'dashboard-listings')->delete();
            Permission::where('name', 'upload-editor-images')->delete();
            Permission::where('name', 'upload-images')->delete();
            Permission::where('name', 'list-transfer-requests')->delete();
            Permission::where('name', 'update-transfer-request-status')->delete();
            Permission::where('name', 'accept-transfer-requests')->delete();
            Permission::where('name', 'manage-faqs')->delete();
            Permission::where('name', 'list-report-types')->delete();
            Permission::where('name', 'list-listing-votes')->delete();
            Permission::where('name', 'show-listing-vote')->delete();
            Permission::where('name', 'create-listing-votes')->delete();
            Permission::where('name', 'list-investors')->delete();
            Permission::where('name', 'update-investor')->delete();
            Permission::where('name', 'list-anb-accounts')->delete();
            Permission::where('name', 'list-eod-transactions')->delete();
            Permission::where('name', 'manage-website-contents')->delete();
            Permission::where('name', 'list-listing-discussions')->delete();
            Permission::where('name', 'manage-enquiries')->delete();
        } catch (\Exception $e) {
        }

        foreach ($permissions as $group => $groupPermissions) {
            foreach ($groupPermissions as $permission) {
                try {
                    $_permission = Permission::findOrCreate($permission);
                    $_permission->guard_name = 'web';
                    $_permission->group = $group;
                    $_permission->save();
                    //Permission::create(['name' => $permission, 'group' => $group, 'guard_name' => 'web']);
                } catch (\Exeption $e) {

                }
            }
        }

        $adminRole = Role::where('name', 'Admin')->first();
        $adminRole->givePermissionTo(Permission::pluck('id', 'id')->toArray());
    }
}
