const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  purge: {
    mode: 'all',
    content: [
      './resources/views/teaser/**/*.blade.php',
      './resources/js/components/**/*.vue',
    ]
  },
  theme: {
    extend: {
      colors: {
        'primary': {
          50: '#F5F7FA',
          100: '#EBEFF4',
          200: '#CCD7E4',
          300: '#AEBFD4',
          400: '#718FB4',
          500: '#345F94',
          600: '#2F5685',
          700: '#1F3959',
          800: '#172B43',
          900: '#101D2C',
        },
        'primary-inv': {
          50: '#333333',
          100: '#333333',
          200: '#333333',
          300: '#333333',
          400: '#333333',
          500: '#fff',
          600: '#fff',
          700: '#fff',
          800: '#fff',
          900: '#fff',
        },
        'secondary': {
          50: '#FFFEF6',
          100: '#FFFCED',
          200: '#FFF8D1',
          300: '#FFF4B5',
          400: '#FFEC7E',
          500: '#FFE446',
          600: '#E6CD3F',
          700: '#99892A',
          800: '#736720',
          900: '#4D4415',
        },
        'secondary-inv': {
          50: '#333333',
          100: '#333333',
          200: '#333333',
          300: '#333333',
          400: '#333333',
          500: '#333333',
          600: '#333333',
          700: '#333333',
          800: '#fff',
          900: '#fff',
        }
      },
      fontFamily: {
        sans: [ "'TheSansArabic SemiLight'", ...defaultTheme.fontFamily.sans ],
        heading: [ 'TheSansArabic' ]
      },
      boxShadow: {
        'outline-primary': '0 0 0 3px rgba(52,95,148,.45)'
      }
    },
  },
  variants: {
    padding: [ 'responsive', 'hover', 'focus', 'direction' ],
    margin: [ 'responsive', 'hover', 'focus', 'direction' ],
    inset: [ 'responsive', 'hover', 'focus', 'direction' ],
    translate: [ 'responsive', 'hover', 'focus', 'direction' ],
    textAlign: [ 'responsive', 'hover', 'focus', 'direction' ],
    cursor: [ 'responsive', 'hover', 'focus', 'disabled' ],
    backgroundColor: [ 'responsive', 'hover', 'focus', 'disabled' ],
    flexDirection: [ 'responsive', 'hover', 'focus', 'direction' ],
  },
  plugins: [
    require('@tailwindcss/ui'),
    require('tailwindcss-dir')(),
  ],
}
