<?php

namespace App\Actions\Listings\Wallets;

use App\Models\Listings\Listing;
use App\Enums\Listings\ListingWallet;
use Lorisleiva\Actions\Concerns\AsAction;

class CreateListingWallets
{
    use AsAction;

    /**
     * Handle creating listing wallets
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Bavix\Wallet\Models\Wallet
     */
    public function handle(Listing $listing)
    {
        $wallets = collect();

        foreach (ListingWallet::asArray() as $value) {
            $wallets->push(CreateListingWallet::run($listing, $value));
        }

        return $wallets;
    }
}
