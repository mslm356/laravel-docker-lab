<div>
    <div class="flex items-center text-gray-500">
        <h5 class="text-sm">{{ $name }}</h5>
        @if ($tooltipId)
            <span id="{{ $tooltipId }}"><svg class="w-5 h-5 ml-1 text-gray-400 rtl:mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
            </svg></span>
        @endif
    </div>
    <p class="text-xl text-gray-700">
        {{ $value }}
    </p>
</div>
