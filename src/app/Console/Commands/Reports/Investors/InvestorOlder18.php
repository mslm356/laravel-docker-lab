<?php

namespace App\Console\Commands\Reports\Investors;

use App\Exports\Investors\DepositReportExport;

use App\Exports\Investors\OlderThan18;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class InvestorOlder18 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:investor-older-18';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get investors investor-older-18';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Excel::store(
            new OlderThan18(),
            "mgmnt-reports/investor.xlsx",
        );
    }
}
