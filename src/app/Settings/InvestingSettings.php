<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class InvestingSettings extends Settings
{
    public $administrative_fees_percentage;
    public $beginner_limit_per_listing;

    public static function group(): string
    {
        return 'investing';
    }
}
