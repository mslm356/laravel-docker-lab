<?php

namespace App\Http\Controllers\MobileApp\Investors\Auth;

use App\Enums\Notifications\NotificationTypes;
use App\Http\Controllers\Controller;
use App\Services\Investors\AbsherOtpService;
use App\Services\Investors\RegisterServices;
use App\Services\Investors\TokenServices;
use App\Services\LogService;
use App\Support\Elm\Yaqeen\Exceptions\AddressLanguageException;
use App\Support\Elm\Yaqeen\Exceptions\BirthDateException;
use App\Support\Elm\Yaqeen\Exceptions\CrException;
use App\Support\Elm\Yaqeen\Exceptions\ExternalSystemException;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use App\Support\Elm\Yaqeen\Exceptions\MissingInfoRelatedToIdException;
use App\Support\Elm\Yaqeen\YaqeenInfoService;
use App\Traits\NotificationServices;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{

    use NotificationServices;

    public $registerServices;
    public $tokenServices;
    public $yaqeenInfoService;
    public $logService;
    public $auth;
    public $absherOtpService;

    public function __construct()
    {
        $this->registerServices = new RegisterServices();
        $this->tokenServices = new TokenServices();
        $this->yaqeenInfoService = new YaqeenInfoService();
        $this->logService = new LogService();
        $this->absherOtpService = new AbsherOtpService();
    }

    public function register(Request $request)
    {

        $rules = $this->registerServices->rules("mobile_app");

        $rules['password'] = ['nullable', 'string', textValidation()];
        $rules['first_name'] = ['nullable', 'string', 'max:100',textValidation()];
        $rules['last_name'] = ['nullable', 'string', 'max:100',textValidation()];
        $ninRules = $this->registerServices->getRulesOfNin("mobile_app");

        $rules = array_merge($rules, $ninRules);

        try {

            $data = Validator::make($request->all(), $rules)->validate();

            $data['password'] = uniqid();
            $data['type'] = 'mobile_app';
            $data['birthDateInstance'] = Carbon::createFromFormat('Y-m-d', $data['birth_date']);

            if ($this->registerServices->checkAgeOfUser($data['birth_date'], Str::startsWith($data['nin'], '1') ? "hijri" : "gregorian") == 0) {
                return apiResponse(400, __('mobile_app/message.invalid_registration_age'));
            }

            $info = $this->yaqeenInfoService->yaqeenInfo($data);

            if ($info['nationality'] == 'الولايات المتحدة' || $info['nationality'] == 'الولايات  المتحدة') {
                return apiResponse(400, __('mobile_app/message.invalid_registration_for_american'));
            }

            $addresses = $this->yaqeenInfoService->yaqeenAddress($data);

            $values = DB::transaction(function () use ($data, $info, $addresses) {

                $user = $this->registerServices->create($data, "mobile_app");

                event(new Registered($user));

                $token = auth('api')->login($user);

                $this->registerServices->updateUserInfo($user, $info, $token);

                $verificationMsg = $this->absherOtpService->createOtp($user, null, null, $data['nin']);

                $this->yaqeenInfoService->updateInvestorProfile($user, $info, $addresses, $verificationMsg);

                $response = ['user' => $user, 'verificationMsg' => $verificationMsg, 'token' => $token];

                return $response;

            }, 1);

            $user = $values['user'];

            $verificationMsg = $values['verificationMsg'];

            $this->registerServices->alertUser($user);

            $this->send(NotificationTypes::Registration, $this->notificationData($user), $user->id);

            $user->setRelation('investor', null);
            $responseData = ['token' => $values['token'], 'vid' => $verificationMsg->id, 'nin' => $data['nin']];

            return apiResponse(200, __('mobile_app/message.login_successfully'), $responseData);


        } catch (IdException | MissingInfoRelatedToIdException | CrException |
        BirthDateException | AddressLanguageException | ExternalSystemException  $e) {

            $code = $this->logService->log('IdException-REGISTER', $e);
            return apiResponse(400, $e->getMessage() . ' : ' . $code);
        } catch (ValidationException $e) {

            $code = $this->logService->log('ValidationException-REGISTER', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {

            $code = $this->logService->log('REGISTER', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }

    public function notificationData($user)
    {
        return [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => 'Welcome to Aseel',
            'title_ar' => 'مرحبا بك في أصيل',
            'content_en' => 'May God bless you, our honorable investor in the Aseel Financial Platform, which will provide you with unique opportunities for real estate investment',
            'content_ar' => 'حياك الله مستثمرنا الكريم في منصة أصيل المالية التي ستتيح لك فرصاً مميزة للاستثمار العقاري',
        ];
    }

    public function updateInvestorInfoByYaqeen($data, $user)
    {

        $info = $this->yaqeenInfoService->yaqeenInfo($data);

        $addresses = $this->yaqeenInfoService->yaqeenAddress($data);

        $this->updateUserInfo($user, $info);

        $verificationMsg = $this->yaqeenInfoService->sendSms($data, $user);

        $this->yaqeenInfoService->updateInvestorProfile($user, $info, $addresses, $verificationMsg);

        return $verificationMsg;
    }


    public function updatePassword(Request $request)
    {

        try {

            $rules = [
                'password' => ['required', 'string', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/']
            ];

            $data = Validator::make($request->all(), $rules)->validate();

            $user = getAuthUser('api');

            $profile = $user->investor;

            if (!$profile) {
                return apiResponse(400, __('mobile_app/message.profile_not_valid'));
            }

            if (!$profile->is_identity_verified) {
                return apiResponse(400, __('mobile_app/message.profile_not_verified'));
            }

            $user->password = Hash::make($data['password']);
            $user->save();

            return apiResponse(200, __('mobile_app/message.password_saved_successfully'));

        } catch (ValidationException $e) {
            $code = $this->logService->log('ValidationException-UPDATE-PASSWORD', $e);
            $errors = handleValidationErrors($e->errors());
            return apiResponse(400, $e->getMessage() . ' : ' . $code, ['errors' => $errors]);
        } catch (\Exception $e) {
            $code = $this->logService->log('UPDATE-PASSWORD', $e);
            return apiResponse(400, __('mobile_app/message.please_try_later') . ' : ' . $code);
        }
    }
}
