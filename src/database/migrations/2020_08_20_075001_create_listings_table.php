<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Enums\Listings\ListingReviewStatus;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->id();
            $table->string('title_en');
            $table->string('title_ar');
            $table->unsignedInteger('city_id');
            $table->unsignedSmallInteger('type_id');
            $table->unsignedTinyInteger('investment_type')->nullable();
            $table->decimal('gross_yield', 64, 0);
            $table->decimal('dividend_yield', 64, 0);
            $table->timestamp('deadline');
            $table->decimal('min_inv_share', 64, 0);
            $table->decimal('max_inv_share', 64, 0);
            $table->decimal('target', 64, 0);
            $table->string('currency');
            $table->decimal('total_shares', 64, 0);
            $table->decimal('reserved_shares', 64, 0);
            $table->decimal('rent_amount', 64, 0);
            $table->boolean('is_closed');
            $table->boolean('is_visible');
            $table->unsignedTinyInteger('review_status')
                ->default(ListingReviewStatus::PendingReview);

            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('type_id')->references('id')->on('property_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
