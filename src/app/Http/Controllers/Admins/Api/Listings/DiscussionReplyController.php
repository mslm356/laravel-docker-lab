<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Actions\Listings\Discussions\StoreReply;
use App\Common\ValidationRules\Listings\DiscussionReplyRules;
use App\Enums\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Listings\ListingDiscussion;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role as ModelsRole;

class DiscussionReplyController extends Controller
{
    /**
     * Store the discussion in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $listing, $discussion)
    {
        $discussion = ListingDiscussion::whereNull('parent_id')
            ->where('listing_id', $listing)
            ->findOrFail($discussion);

        $data = $this->validate($request, DiscussionReplyRules::get());

        return DB::transaction(function () use ($discussion, $data, $request) {
            StoreReply::run($discussion, [
                'user_id' => $request->user('sanctum')->id,
                'user_role_id' => ModelsRole::findByName(Role::Admin, 'web')->id,
                'text' => $data['comment'],
                'replier_name' => $data['replier_name'],
                'replier_role' => $data['replier_role']
            ]);

            return response()->json();
        });
    }

    /**
     * Store the discussion in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $listing, $discussion, $reply)
    {
        $reply = ListingDiscussion::where('parent_id', $discussion)
            ->where('listing_id', $listing)
            ->findOrFail($reply);

        $data = $this->validate($request, DiscussionReplyRules::get());

        $reply->update([
            'text' => $data['comment'],
            'replier_name' => $data['replier_name'],
            'replier_role' => $data['replier_role'],
        ]);

        return response()->json();
    }

    /**
     * Delete the discussion from the storage
     *
     * @param int $listing
     * @param int $discussion
     * @param int $reply
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, $discussion, $reply)
    {
        $reply = ListingDiscussion::where('parent_id', $discussion)
            ->where('listing_id', $listing)
            ->findOrFail($reply);


        $reply->delete();

        return response()->json();
    }
}
