<?php

namespace App\Jobs\Funds;

use App\Enums\Role;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use App\Jobs\NotificationFirebaseBulk;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Enums\Notifications\NotificationTypes;

class SendAlertMailsOnCreate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $listing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listing)
    {
        $this->listing = $listing;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $listing = $this->listing;
        $usersCollection = collect();

        User::role(Role::Investor)
            ->lazyById()
            ->each(function ($user) use ($listing, $usersCollection) {
                if (config('mail.MAIL_STATUS') == 'ACTIVE') {
                    Mail::to($user)->queue(new \App\Mail\Funds\CreateFund($user, $listing));
                }

                $usersCollection->push($user);
            });

        $notification = new NotificationFirebaseBulk(NotificationTypes::Info, $this->notificationData($listing), $usersCollection);
        dispatch($notification)->onQueue('notify');
    }

    public function notificationData($listing)
    {
        return [
            'action_id'=> $listing->id,
            'channel'=>'mobile',
            'title_en'=>'New Fund',
            'title_ar'=>'فرصة جديده',
            'content_en'=>'An opportunity to invest in the platform has been presented',
            'content_ar'=>'تم طرح فرصة للاستثمار في المنصة',
        ];
    }
}
