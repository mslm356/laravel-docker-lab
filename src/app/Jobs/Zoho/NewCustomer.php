<?php

namespace App\Jobs\Zoho;

use App\Support\Zoho\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class NewCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $zohoCustomer;
    public $item;
    public $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($item, $type)
    {
        $this->zohoCustomer = new Customer();
        $this->item = $item;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->zohoCustomer->saveCustomer($this->item, $this->type);
    }
}
