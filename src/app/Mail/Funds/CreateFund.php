<?php

namespace App\Mail\Funds;

use App\Models\Listings\Listing;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreateFund extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $listing;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Listing $listing)
    {
        $this->user = $user;
        $this->listing = $listing;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user ? $this->user->full_name : '---';

        $listingUrl = investor_url('funds/' . $this->listing->id);

        return $this->view('emails.funds.create_fund', compact('name', 'listingUrl'))
            ->subject(__('emails/funds/create.subject'));
    }
}
