<?php

namespace App\Http\Resources\Admins\BankAccounts;

use App\Enums\Banking\BankCode;
use App\Enums\BankAccountStatus;
use App\Models\AuthorizedEntity;
use Illuminate\Http\Resources\Json\JsonResource;

class BankAccountListItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'iban' => $this->iban,
            'user_id' => $this->accountable_id ,
            'nin' => $this->accountable && $this->accountable->nationalIdentity ? $this->accountable->nationalIdentity->nin : null ,
            'status' => [
                'description' => BankAccountStatus::getDescription($this->status),
                'value' => $this->status,
            ],
            'bank' => BankCode::getDescription($this->bank),
            'authorized_entity' => $this->accountable_type === (new AuthorizedEntity())->getMorphClass() ? $this->accountable->name : null,
            'reviewer_name' => $this->reviewer ? $this->reviewer->full_name : null,
            'reviewer_comment' => $this->reviewer_comment,
            'balance' => money($this->balance)->formatByDecimal(),
            'created_at' => $this->created_at ? $this->created_at->timezone('Asia/Riyadh')->format('Y-m-d h:i:s A') : null, //$this->created_at->format('Y-m-d h:i'),  ->toDateTimeString()
            'updated_at' => $this->updated_at,
        ];
    }
}
