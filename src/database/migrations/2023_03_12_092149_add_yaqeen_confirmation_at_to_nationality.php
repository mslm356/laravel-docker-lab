<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddYaqeenConfirmationAtToNationality extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('national_identities', function (Blueprint $table) {
            $table->timestamp('yaqeen_confirmation_at')->after('nationality');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('national_identities', function (Blueprint $table) {
            $table->dropColumn('yaqeen_confirmation_at');
        });
    }
}
