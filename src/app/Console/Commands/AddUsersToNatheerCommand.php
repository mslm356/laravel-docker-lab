<?php

namespace App\Console\Commands;

use App\Models\Users\User;
use App\Support\Elm\Natheer\NatheerWatchListService;
use Illuminate\Console\Command;

class AddUsersToNatheerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'natheer:add-users {--num=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $num=$this->option('num')?(int)$this->option('num'):50;

        try {
            $natheerWatchListService = new NatheerWatchListService();

            $i = 0;
            User::where('added_to_natheer',false)->whereHas('nationalIdentity',function ($query){
                $query->where('nin', 'like', '1%')->orWhere('nin', 'like', '2%');
            })->with('nationalIdentity')
                ->chunk($num, function ($users) use ($natheerWatchListService, $i) {
                    $i++;
                    $persons_array=[];
                    $this->prepareData($users,$persons_array);
                    $responce= $natheerWatchListService->addBulkPerson($persons_array);
                    $this->updateNatherStatus($users,$responce);
                    print_r($responce);
                    echo "done_" . $i . "\n";
                });

        }
        catch (\Exception $e)
        {
            echo $e->getMessage()."\n";
        }
    }

    function prepareData($users,&$persons_array)
    {
        foreach ($users as $user) {
            $date_of_birth=str_replace('-','',$user->nationalIdentity->birth_date);
            $data=['person'=>['personId'=>$user->nationalIdentity->nin,'dateOfBirth'=>$date_of_birth]];
            array_push($persons_array,$data);
        }

    }

    function updateNatherStatus($users,$responce_data)
    {
        $responce_data=collect($responce_data);
        $added_presons=$responce_data->where('resultDetail.resultCode',103)->pluck('person.personId')->toArray();

//        $added_presons=$responce_data->where('result',"=",'true')->pluck('person.personId')->toArray();

        User::whereHas('nationalIdentity',function ($query) use ($added_presons){
            $query->whereIn('nin',$added_presons);
        })->update(['added_to_natheer'=>true]);

    }

}
