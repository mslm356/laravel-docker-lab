<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Models\Listings\Listing;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Listings\ListingRevisionResource;

class GetListingRevisions extends Controller
{
    /**
     * Get all listings revisions
     *
     * @param \App\Models\Listings\Listing $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Listing $listing)
    {
        return ListingRevisionResource::collection(
            $listing->revisions->load('auditor')
        );
    }
}
