<?php

namespace App\Http\Resources\Admins\Campaigns;

use App\Enums\Campaigns\CampaignUserStatus;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CampaignUsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->full_name,
            'status' => $this->getTypeCode($this->pivot->status),
            'create_at' => Carbon::parse($this->created_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i'),
            'updated_at' => Carbon::parse($this->updated_at)->timezone('Asia/Riyadh')->format('Y-m-d H:i'),
        ];
    }

    public function getTypeCode($value)
    {
        return [
            'value' => $value,
            'description' => __(CampaignUserStatus::getKey($value)),
        ];
    }
}
