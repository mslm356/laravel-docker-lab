<?php

namespace App\Console\Commands\Membership;


use App\Enums\MemberShips\MemberShipType;
use App\Enums\Notifications\NotificationTypes;
use App\Models\MemberShip\MemberShipRequests;
use App\Services\LogService;
use App\Services\MemberShip\CreateRequest;
use App\Traits\NotificationServices;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendNotificationToGoldMemberShip extends Command
{
    use NotificationServices;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-notification:gold-member-ship';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create member ship request';

    public $logService;
    public $memberShipService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
        $this->memberShipService = new CreateRequest();

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

               $result =  DB::table('member_ship_requests as t1')
                ->where('status', 3)
                ->where('member_ship', 3)
                ->select('t1.*')
                ->whereRaw('t1.id = (SELECT MAX(id) FROM member_ship_requests WHERE user_id = t1.user_id AND status = 3)')
                ->orderBy('t1.id', 'desc')
                ->lazy(100)
                ->each(function ($member_ship_request)  {

                    if($member_ship_request->member_ship == 2)
                    {
                        $notificationData = $this->notificationData($member_ship_request);
                        $this->send(NotificationTypes::Membership, $notificationData, $member_ship_request->user_id);

                    }

                });
            dd( "Gold MemberShip Had Been Sent To => " . $result->count());

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }

    public function notificationData($member_ship_request)
    {
        return [
            'action_id'=> $member_ship_request->id,
            'channel'=>'mobile',
            'title_en'=>' العضــــوية المــاسيــة    ',
            'title_ar'=>' العضــــوية المــاسيــة    ',
            'content_en'=> '  " الآن عضويتك تمكنك من الاستثمار المبكر في " صندوق سكك تلالة العقاري  ',
            'content_ar'=> '  " الآن عضويتك تمكنك من الاستثمار المبكر في " صندوق سكك تلالة العقاري  ',
        ];
    }


}
