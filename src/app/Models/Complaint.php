<?php

namespace App\Models;

use App\Models\Users\User;
use App\Models\ComplaintStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Complaint extends Model
{
    use HasFactory;

    protected $fillable = [
        'subject',
        'description',
        'complaint_type_id',
        'user_id',
        'status_id',
        'reviewer_id',
        'reviewer_comment'
    ];

    protected $appends = [
        'type_name',
        'user_name',
    ];

    /**
    * Get the type of uploaded by the entity
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function type()
    {
        return $this->belongsTo(ComplaintType::class, 'complaint_type_id');
    }

    /**
    * Get the user relationship.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
    * Get the user relationship.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id');
    }

    /**
     * Get the files of uploaded by the entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    /**
     * Get the status relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(ComplaintStatus::class);
    }

    public function getUserNameAttribute()
    {
        return $this->user->full_name;
    }

    public function getTypeNameAttribute()
    {
        return $this->type->{app()->getLocale(). "_name"} ?? '';
    }


}
