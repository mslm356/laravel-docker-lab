<?php

namespace App\Enums\Campaigns;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class CampaignStatus extends Enum
{
    const Ended = 0;
    const Running = 1;
    const Scheduled = 2;
    const Closed = 3;
}
