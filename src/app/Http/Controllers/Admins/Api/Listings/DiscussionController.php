<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Actions\Listings\Discussions\GetDiscussions;
use App\Models\Listings\Listing;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\Listings\DiscussionResource;
use App\Models\Listings\ListingDiscussion;
use Illuminate\Support\Facades\DB;

class DiscussionController extends Controller
{
    /**
     * Store the discussion in the storage
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listing $listing)
    {
        return DiscussionResource::collection(
            GetDiscussions::run($listing, ['user.investor', 'replies.userRole'])
        );
    }

    /**
     * Show the discussion in the storage
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($listing, $discussion)
    {
        $discussion = ListingDiscussion::where('listing_id', $listing)
            ->findOrFail($discussion);

        return new DiscussionResource(
            $discussion->load('user.investor', 'replies.userRole')
        );
    }

    /**
     * Delete the discussion from the storage
     *
     * @param int $reply
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, $discussion)
    {
        $discussion = ListingDiscussion::whereNull('parent_id')
            ->findOrFail($discussion);

        DB::transaction(function () use ($discussion) {
            $discussion->delete();

            $discussion->replies()->delete();
        });

        return response()->json();
    }
}
