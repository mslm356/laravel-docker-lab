<?php


namespace App\Support\Zoho;


use App\Models\ZohoSettings;
use App\Settings\AccountingSettings;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Tax
{

    public $base_url;
    public $organization_id;
    public $auth;
    public $settings;

    public function __construct()
    {
        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
        $this->settings = new Settings();
    }

    public function prepareTaxData($tax_name)
    {
        $vatPercentage = optional(app(AccountingSettings::class)->vat)->percentage ?? '0';

        $data = [
            'tax_name' => $tax_name,
            'tax_percentage' => $vatPercentage,
        ];

        return $data;
    }

    public function store($tax_name)
    {

        $token = $this->auth->getToken();

        try {

            $data = $this->prepareTaxData($tax_name);

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'settings/taxes?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                $this->settings->saveSettings($tax_name, $responseData['tax']['tax_id']);
                return true;
            }

            Log::error('ZOHO-NEW-Tax', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-NEW-Tax', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }

    public function list()
    {

        $token = $this->auth->getToken();

        try {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->get($this->base_url . 'settings/taxes?organization_id=' . $this->organization_id);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {

                return $responseData['taxes'];
            }

            Log::error('ZOHO-NEW-Tax', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-NEW-Tax', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return true;
    }
}
