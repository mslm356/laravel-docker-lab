<?php

namespace App\Actions\Listings\Discussions;

use App\Models\Listings\Listing;
use Lorisleiva\Actions\Concerns\AsAction;

class GetDiscussions
{
    use AsAction;

    public function handle(Listing $listing, $withRelations)
    {
        $disucssions = $listing->rootDiscussions()
            ->with($withRelations)
            ->latest()
            ->simplePaginate();

        return $disucssions;
    }
}
