<?php
namespace App\Traits;
use App\Services\LogService;
use Illuminate\Auth\Access\AuthorizationException;
use App\Support\Elm\AbsherOtp\Exceptions\VerificationSmsException;
use Illuminate\Validation\ValidationException;
use App\Exceptions\NoBalanceException;
use App\Exceptions\NoInvestorWalletFound;
trait CustomExceptionHandle
{
    /**
     * Handle common exceptions for mobile investment requests
     *
     * @param \Exception $e
     * @param string $logPrefix
     * @return \Illuminate\Http\JsonResponse
     */
    function handleMobileInvestmentException(\Exception $e, string $logPrefix): \Illuminate\Http\JsonResponse
    {
        $logService = new LogService();
        $message = $e->getMessage() . ' : ' . $code = $logService->log($logPrefix, $e);
        $statusCode = 400;
        if ($e instanceof ValidationException) {
            return response()->json(['message' => $message, 'errors' => $e->errors()], $statusCode);
        }
        if ($e instanceof AuthorizationException) {
            return apiResponse($statusCode, $message);
        }
        if ($e instanceof VerificationSmsException) {
            return apiResponse($statusCode, $message);
        }
        if ($e instanceof NoInvestorWalletFound) {
            return apiResponse($statusCode, __('messages.no_active_wallet_found_for_investor') . ' : ' . $message);
        }
        if ($e instanceof NoBalanceException) {
            return apiResponse($statusCode, __('messages.no_enough_balance') . ' : ' . $message);
        }
        return apiResponse($statusCode, __('mobile_app/message.please_try_later') . ' : ' . $message);
    }
    function handelStatusResponce($response,$cutomData=[])
    {
        if (isset($response['status']) && !$response['status']) {
            $msg = isset($response['message']) ? $response['message'] : 'sorry please try later';
            return apiResponse(400, __($msg));
        }
        $responseData = isset($response['data']) ? $response['data'] : [];
        $responseData = array_merge($responseData,$cutomData);
        if (isset($response['message']))
            return apiResponse(200, __($response['message']), $responseData);
        return response()->json($responseData);
    }
}

