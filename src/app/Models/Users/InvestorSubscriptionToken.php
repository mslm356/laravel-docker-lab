<?php

namespace App\Models\Users;

use App\Models\AppModel;
use App\Models\Listings\Listing;
use App\Models\Listings\ListingInvestorRecord;
use App\Models\Urway\UrwayTransaction;

class InvestorSubscriptionToken extends AppModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'expired_at' => 'datetime',
        'authorization_expiry' => 'datetime'
    ];

    /**
     * Get fund
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }

    public function listingRecord()
    {
        return $this->hasOne(ListingInvestorRecord::class, 'subscription_token_id');
    }

    public function urwayTransaction()
    {
        return $this->hasOne(UrwayTransaction::class, 'subscription_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
