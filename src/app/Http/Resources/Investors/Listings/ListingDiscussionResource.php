<?php

namespace App\Http\Resources\Investors\Listings;

use Illuminate\Http\Resources\Json\JsonResource;

class ListingDiscussionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->user->full_name,
            'text' => $this->text,
            'replies' => $this->whenLoaded('replies', fn () => ListingDiscussionReplyResource::collection($this->replies)),
            'created_at' => $this->created_at->toDateTimeString()
        ];
    }
}
