<?php

namespace App\Http\Controllers\Investors\SystemSettings;

use App\Http\Controllers\Controller;
use App\Settings\InvestingSettings;

class GetSystemSettings extends Controller
{
    /**
     * Invest in the given listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        $investingSettings = app(InvestingSettings::class);

        return response()->json([
            'data' => [
                'beginner_limit_per_listing' => cleanDecimal(
                    number_format(
                        money($investingSettings->beginner_limit_per_listing)->formatByDecimal(),
                        2
                    )
                ),
            ]
        ]);
    }
}
