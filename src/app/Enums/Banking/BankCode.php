<?php

namespace App\Enums\Banking;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class BankCode extends Enum implements LocalizedEnum
{
    const Alawwal = 'AAALSARI';
    const Albilad = 'ALBISARI';
    const Anb = 'ARNBSARI';
    const Aljazira = 'BJAZSAJE';
    const MuscatBank = 'BMUSSARI';
    const Bnp = 'BNPASARI';
    const Alfarnsi = 'BSFRSARI';
    const ChaseMorgan = 'CHASSARI';
    const Deutsche = 'DEUTSARI';
    const Emirates = 'EBILSARI';
    const GulfInt = 'GULFSARI';
    const IndCommChina = 'ICBKSARI';
    const Alinma = 'INMASARI';
    const BahrainBank = 'NBOBSARI';
    const KuwaitBank = 'NBOKSAJE';
    const PakistanBank = 'NBPASARI';
    const Ncb = 'NCBKSAJE';
    const RiyadhBank = 'RIBLSARI';
    const Rajhi = 'RJHISARI';
    const Sabb = 'SABBSARI';
    const Sama = 'SAMASARI';
//    const Samba = 'SAMBSARI';
    const Sib = 'SIBCSARI';
    const TurkeyZiraat = 'TCZBSAJE';
    const QatarBank = 'QNBASARI';
    const MUFGBank = 'BOTKSARI';
    const AbuDhabiBank = 'FABMSARI';
}
