<?php

use \App\Http\Controllers\MobileApp\Investors\Listings\FundsController;
use \App\Http\Controllers\MobileApp\Investors\VoteController;
use App\Http\Controllers\MobileApp\Investors\Investments\TestInvestInListingController;

Route::get('/all-listings', [FundsController::class, 'allListings']);
Route::get('/listing/{listing}', [FundsController::class, 'showListing']);

//  New for caching
Route::get('/all-funds', [FundsController::class, 'allFunds']);


Route::middleware(['auth:api', 'jwt_check:api', 'role:' . \App\Enums\Role::Investor])->group(function () {

    Route::get('/auth-fund-details/{listing}', [FundsController::class, 'fundDetailsWithAuth']);

    // VOTE
    Route::post('/answer-vote', [VoteController::class, 'store']);
    Route::get('/listing/{listing}/votes', [VoteController::class, 'getListingVotes']);
});

Route::get('/fund-details/{listing}', [FundsController::class, 'fundDetailsWithoutAuth']);




Route::post('/test-subscription-token', [TestInvestInListingController::class, 'subscriptionToken']);
Route::post('/test-send-otp', [TestInvestInListingController::class, 'sentOtp']);

Route::post('/test-investing', [TestInvestInListingController::class, 'investment']);



