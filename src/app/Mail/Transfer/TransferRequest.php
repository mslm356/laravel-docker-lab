<?php

namespace App\Mail\Transfer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransferRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $amount = $this->amount;
        return $this->view('emails.transfer.review_request', compact('amount'))
            ->subject(__('emails/transfer/review_request.subject'));
    }
}
