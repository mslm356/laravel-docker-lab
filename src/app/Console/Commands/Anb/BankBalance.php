<?php

namespace App\Console\Commands\Anb;

use App\Services\Anb\BankBalanceService;
use App\Services\Anb\PullStmtService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BankBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anb:update-Bank-balance {--date=}';
    public $bankBalanceService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update-Bank-balance table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->bankBalanceService = new BankBalanceService();
    }

    public function handle()
    {

        try {

            $this->bankBalanceService->handle($this->option('date'));
            dd('done success');

        } catch (\Exception $e) {
            dd($e->getLine(), $e->getFile(), $e->getMessage());
        }
    }
}
