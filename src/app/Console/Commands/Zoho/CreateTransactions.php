<?php

namespace App\Console\Commands\Zoho;

use App\Enums\Banking\TransactionReason;
use App\Enums\Zoho\AccountTypes;
use App\Jobs\Zoho\NewCustomer;
use App\Jobs\Zoho\NewItem;
use App\Models\Listings\Listing;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Services\LogService;
use App\Support\Zoho\Account;
use App\Support\Zoho\Auth;
use App\Support\Zoho\CustomerPayments;
use App\Support\Zoho\Invoice;
use App\Support\Zoho\Settings;
use App\Support\Zoho\Tax;
use App\Support\Zoho\Transactions;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateTransactions extends Command
{

    public $zohoSettings;
    public $logServices;
    public $transactionHandle;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:create-transactions {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create transactions without zoho id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logServices = new LogService();
        $this->transactionHandle = new Transactions();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $date = Carbon::parse($this->option('date'));

            Transaction::query()
                ->whereDate('created_at','>=' , $date)
                ->whereNotIn('reason', [TransactionReason::PayoutDistribution, TransactionReason::InvestingInListingVatOfAdminFee])
                ->where('payable_type',User::class)
                ->whereNull('zoho_id')
                ->lazyById()
                ->each(function ($transaction) {
                    $this->transactionHandle->handle($transaction);
                });

        } catch (\Exception $e) {
            $this->logServices->log('ZOHO-TRANSACTIONS',$e);
            dd($e->getMessage(), $e->getFile(), $e->getLine());
        }
    }




}
