<?php

namespace App\Actions\Listings\Attachments;

use App\Models\File;
use Lorisleiva\Actions\Concerns\AsAction;

class DeleteListingAttachment
{
    use AsAction;

    public function handle(File $file)
    {
        return $file->deleteWithFile();
    }
}
