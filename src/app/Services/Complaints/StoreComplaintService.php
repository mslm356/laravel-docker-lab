<?php

namespace App\Services\Complaints;

use App\Models\File;
use App\Enums\FileType;
use App\Models\Complaint;
use App\Models\Users\User;
use App\Traits\SendMessage;
use App\Models\ComplaintType;
use App\Models\ComplaintStatus;
use App\Traits\NotificationServices;
use App\Jobs\SendComplaintEmailToAdminJob;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Enums\Notifications\NotificationTypes;
use App\Jobs\SendReceivedComplaintEmailToTheUserJob;

class StoreComplaintService
{
    use NotificationServices, SendMessage, DispatchesJobs;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {

        $data = $this->data;
        $gaurd = (request()->has('guard') && request()->guard == 'api') ? 'api' : 'web';
//        $user = User::first();
        $user = getAuthUser($gaurd);

        $data['user_id'] = $user->id;
        $data['status_id'] = ComplaintStatus::where('name', 'new')->first()->id;

        $complaintType = ComplaintType::find($data['complaint_type_id']);
        $complaint = Complaint::create($data);

        if (isset($data['file'])) {
            File::upload($data['file'], "complaint/{$complaintType->en_name}/{$complaint->id}", [
                'fileable_id' => $complaint->id,
                'fileable_type' => $complaint->getMorphClass(),
                'type' => FileType::InvestorsComplaint,
            ]);
        }

        $complaint->files;

        $this->dispatch(new SendReceivedComplaintEmailToTheUserJob($complaint));
        $this->dispatch(new SendComplaintEmailToAdminJob($complaint));
        $this->sendSmsData($user, $complaint);
        $this->send(NotificationTypes::Info, $this->notificationData($user), $user->id);

        return $complaint;
    }

    public function sendSmsData($user, $complaint)
    {
        $content_en = "We have received your complaint #$complaint->id and we will reply to you as soon as possible";
        $content_ar = "تم استلام شكواكم #$complaint->id وسيتم التواصل معكم قريبًا بالتحديثات.";

        if ($user->locale = "ar") {
            $message = $content_ar;
        } else {
            $message = $content_en;
        }

        $this->sendOtpMessage($user->phone_number, $message);
    }

    public function notificationData($user)
    {
        return [
            'action_id' => $user->id,
            'channel' => 'mobile',
            'title_en' => 'Aseel complaint',
            'title_ar' => 'شكوى اصيل',
            'content_en' => 'We have received your complaint and we will reply to you as soon as possible.',
            'content_ar' => 'تم استلام شكواكم وسيتم التواصل معكم قريبًا بالتحديثات.',
        ];
    }
}
