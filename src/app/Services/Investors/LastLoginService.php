<?php


namespace App\Services\Investors;

class LastLoginService
{

    public $user;
    public $token;

    public function __construct($user, $token=null)
    {
        $this->user = $user;
        $this->token = $token;
        $this->updateLastLogin();
    }

    public function updateLastLogin()
    {
        $this->user->last_login = now();
        $this->user->vid = null;
        $this->user->code = null;
        if ($this->token) {
            $this->user->jwt_token = $this->token;
        }
        $this->user->save();
    }

}
