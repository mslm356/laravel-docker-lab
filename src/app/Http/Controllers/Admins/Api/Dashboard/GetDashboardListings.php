<?php

namespace App\Http\Controllers\Admins\Api\Dashboard;

use App\Actions\Listings\GetMyListingsInSummary;
use App\Http\Controllers\Controller;

class GetDashboardListings extends Controller
{
    /**
     * Store the discussion in the storage
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        $listings = GetMyListingsInSummary::run(
            fn ($query) => $query->with('authorizedEntity'),
            function ($listing, $mappedData) {
                $mappedData['authorized_entity'] = optional($listing->authorizedEntity)->name;
                return $mappedData;
            }
        );

        return response()->json([
            'data' => $listings,
        ]);
    }
}
