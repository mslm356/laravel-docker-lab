<?php

use App\Http\Controllers\MobileApp\Investors\Investments\MyInvestmentsController;
use \App\Enums\Role;
use \App\Http\Controllers\MobileApp\Investors\bankAccounts\BankAccountsController;


Route::get('bank-names',[ BankAccountsController::class , 'getBankNames']);
Route::get('bank-names-test',[ BankAccountsController::class , 'getBankNamesTest']);

//Role::Investor

Route::middleware(['auth:api', 'jwt_check:api', 'role:' . Role::Investor ])->group(function () {

    Route::get('bank-accounts',[ BankAccountsController::class , 'index']);
    Route::post('new-bank-account',[ BankAccountsController::class , 'store'])->middleware('user-can-access:bankAccount:api');
    Route::get('approved-bank-accounts',[ BankAccountsController::class , 'getApprovedBankAccounts']);

});
