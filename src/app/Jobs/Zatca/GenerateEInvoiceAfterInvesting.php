<?php

namespace App\Jobs\Zatca;

use App\Models\File;
use App\Enums\FileType;
use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Salla\ZATCA\GenerateQrCode;
use Salla\ZATCA\Tags\TaxNumber;
use Salla\ZATCA\Tags\InvoiceDate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Support\Zatca\EInvoice\Order;
use App\Support\Zatca\EInvoice\Seller;
use Illuminate\Queue\SerializesModels;
use Salla\ZATCA\Tags\InvoiceTaxAmount;
use Illuminate\Queue\InteractsWithQueue;
use Salla\ZATCA\Tags\InvoiceTotalAmount;
use App\Support\PdfGenerator\PdfGenerator;
use Salla\ZATCA\Tags\Seller as TagsSeller;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateEInvoiceAfterInvesting implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $filePath;
    protected $order;
    protected $buyer;
    protected $seller;
    protected $transfer;
    protected $logServices;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        User $buyer,
        Order $order,
        Seller $seller,
        string $filePath,
        $transfer

        //Transfer
    ) {
        $this->buyer = $buyer;
        $this->order = $order;
        $this->seller = $seller;
        $this->filePath = $filePath;
        $this->transfer = $transfer;
        $this->logServices = new LogService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            DB::transaction(function () {
                $displayQRCodeAsBase64 = GenerateQrCode::fromArray([
                    new TagsSeller($this->seller->getName()),
                    new TaxNumber($this->seller->getVatId()),
                    new InvoiceDate($this->order->getInvoiceDate()->toDateTimeString()),
                    new InvoiceTotalAmount($this->order->getTotalAmount()->formatByDecimal()),
                    new InvoiceTaxAmount($this->order->getTotalVat()->formatByDecimal())
                ])
                    ->render();

                $currentLocale = App::getLocale();

                App::setLocale('ar');

                $html = view('zatca.e-invoice', [
                    'seller' => $this->seller,
                    'order' => $this->order,
                    'qr_code' => $displayQRCodeAsBase64,
                    'buyer' => $this->buyer,
                ])
                    ->render();

                App::setLocale($currentLocale);

                $filename = "zatca-{$this->order->getReference()}.pdf";

                $fullPath = trim($this->filePath, '/') . '/' . $filename;

                PdfGenerator::outputFromHtml(
                    $html,
                    $fullPath,
                    [
                        'gotoOptions' => [
                            'waitUntil' => 'networkidle0'
                        ]
                    ]
                );

                File::create([
                    'id' => (string)Str::uuid(),
                    'name' => $filename,
                    'fileable_id' => $this->order->getInvoice()->id,
                    'fileable_type' => $this->order->getInvoice()->getMorphClass(),
                    'path' => $fullPath,
                    'extra_info' => collect([]),
                    'type' => FileType::TransactionZatcaEInvoice,
                ]);
            });

        } catch (\Exception $e) {
            $this->logServices->log('E-Z-INVOICE-EXCEPTION', $e);
        }
    }
}
