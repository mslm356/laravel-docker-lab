<?php

namespace App\Models\Listings;

use App\Models\AppModel;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Role;
use Spatie\Translatable\HasTranslations;

class ListingDiscussion extends AppModel
{
    use HasTranslations, SoftDeletes;

    public $translatable = ['replier_role'];

    /**
     * Get the parent (main question)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parent()
    {
        return $this->belongsTo(ListingDiscussion::class, 'parent_id', 'id');
    }

    /**
     * Get replies
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(ListingDiscussion::class, 'parent_id', 'id');
    }

    /**
     * Get replies
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get user role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userRole()
    {
        return $this->belongsTo(Role::class, 'user_role_id', 'id');
    }

    /**
     * Get listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id', 'id');
    }
}
