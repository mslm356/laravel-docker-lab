<?php

namespace App\Support\Elm\AbsherOtp\Exceptions;

use Exception;

class ConfigurationException extends Exception
{
    protected $requestBody;
    protected $errorCode;
    protected $errorType;
    protected $errorMsg;
    protected $faultActor;
    protected $faultString;

    public function __construct(array $requestBody, $errorCode, $errorType, $errorMsg, $faultString, $faultActor)
    {
        $this->requestBody = $requestBody;
        $this->errorCode = $errorCode;
        $this->errorType = $errorType;
        $this->errorMsg = $errorMsg;
        $this->faultString = $faultString;
        $this->faultActor = $faultActor;

        parent::__construct("Failed to send SMS");
    }

    /**
     * Get the exception's context information.
     *
     * @return array
     */
    public function context()
    {
        return [
            'request_body' => json_encode($this->requestBody),
            'error_code' => $this->errorCode,
            'error_type' => $this->errorType,
            'error_msg' => $this->errorMsg,
            'fault_string' => $this->faultString,
            'fault_actor' => $this->faultActor,
        ];
    }
}
