<?php

namespace App\Common\ValidationRules\Listings;

use App\Models\Users\User;
use Illuminate\Validation\Rule;
use App\Rules\UniquePhoneNumber;

class EntityUserRules
{
    static public function getForCreate()
    {
        return [
            'first_name' => ['required', 'string', 'max:100', textValidation() ],
            'last_name' => ['required', 'string', 'max:100',textValidation() ],
            'email' => ['required', 'email', 'max:200', 'unique:users,email'],
            'phone_country_code' => ['required_with:phone', 'string', 'size:2'],
            'phone' => ['required', 'string', 'phone:phone_country_code', new UniquePhoneNumber('phone_country_code')],
        ];
    }

    static public function getForUpdate(User $user)
    {
        $rules = static::getForCreate();

        return array_merge(
            $rules,
            [
                'email' => [
                    'required',
                    'email',
                    'max:200',
                    Rule::unique(User::class, 'email')
                        ->ignore($user, 'email')
                ],
                'phone' => [
                    'required',
                    'string',
                    'phone:phone_country_code',
                    (new UniquePhoneNumber('phone_country_code'))
                        ->ignore($user)
                ],
            ]
        );
    }
}
