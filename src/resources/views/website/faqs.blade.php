@extends('layouts.website')

@section('content')
<div class="bg-white">
    <div class="max-w-screen-xl px-4 py-16 mx-auto sm:px-6 lg:py-20 lg:px-8">
        <div class="lg:grid lg:grid-cols-3 lg:gap-8">
            <div class="space-y-4">
                <h2 class="text-3xl font-extrabold leading-9 text-gray-900">
                    {{ __('glossary.faqs') }}
                </h2>
                <p class="text-lg leading-7 text-gray-500">{{ __('website.cannot_find_an_answer') }}<a
                        href="{{ route('contact_us.show_form') }}"
                        class="font-medium transition duration-150 ease-in-out text-primary-600 hover:text-primary-400"
                        >{{ __('website.cannot_find_an_answer_customer_support') }}</a>.</p>
            </div>
            <div class="mt-12 lg:mt-0 lg:col-span-2">
                <dl class="space-y-12">
                    @foreach ($faqs as $faq)
                    <div class="space-y-2">
                        <dt class="text-lg font-medium leading-6 text-gray-900">
                            {{ $faq->question }}
                        </dt>
                        <dd class="text-base leading-6 text-gray-500">
                            {{ $faq->answer }}
                        </dd>
                    </div>
                    @endforeach
                </dl>
            </div>
        </div>
    </div>
</div>
@endsection
