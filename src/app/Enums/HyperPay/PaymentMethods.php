<?php

namespace App\Enums\HyperPay;

use BenSampo\Enum\Enum;

final class PaymentMethods extends Enum
{
    const Pending = 1;
    const Unpaid = 2;
    const Paid = 3;
    const Refund = 4;
}
