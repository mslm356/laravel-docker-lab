<?php

namespace App\Enums\Listings;

use BenSampo\Enum\Enum;

final class DiscussionStatus extends Enum
{
    const Published =   1;
    const PendingReply =   2;
}
