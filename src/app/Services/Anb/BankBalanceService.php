<?php


namespace App\Services\Anb;

use App\Services\LogService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BankBalanceService
{
    public $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }

    public function handle($startDate)
    {

        try {

            $date = Carbon::parse($startDate);

            do {

                $dateFormat = $date->format('Y-m-d');

                $closingBalance = $this->calculateClosingBalance($dateFormat);

                $openingBalance = $this->calculateOpeningBalance($dateFormat);

                $data = [
                    'opening_balance' => $openingBalance,
                    'closing_balance' => ($closingBalance + $openingBalance),
                    'date' => Carbon::parse($dateFormat)->subDays(1),
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                $checkDateExists = DB::table('bank_balances')
                    ->where('date', Carbon::parse($dateFormat)->subDays(1))
                    ->count();

                $bankBalance = DB::table('bank_balances');

                $checkDateExists ?
                    $bankBalance
                        ->where('date', $dateFormat)->update($data)
                    : $bankBalance->insert($data);

                $date->addDays(1);

            } while ($date->lessThan(now()));

        } catch (\Exception $e) {
            $this->logServices->log('BANK-BALANCE-SERVICE', $e) ;
        }
    }

    public function calculateClosingBalance($date)
    {

        return DB::table('transactions')
            ->whereDate('created_at', '>=', Carbon::parse($date)->subHours(3))
            ->whereDate('created_at', '<', Carbon::parse($date)->addHours(21))
            ->whereIn('reason', [1,4,5])
            ->select('amount')
            ->sum('amount');
    }

    public function calculateOpeningBalance($date)
    {

        $lastDay = Carbon::parse($date)->subDays(2);

        $lastBankBalance = DB::table('bank_balances')
            ->where('date', $lastDay->format('Y-m-d'))
            ->select('closing_balance')
            ->first();

        return $lastBankBalance ? $lastBankBalance->closing_balance : 0;
    }

}
