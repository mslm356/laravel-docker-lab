<?php

namespace App\Http\Resources\Investors\Listings;

use App\Enums\InvestmentType;
use App\Enums\Listings\AttachmentType;
use App\Http\Resources\Admins\Listings\ListingEditImagesResource;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Enums\Listings\DetailCategory;
use App\Http\Resources\Common\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Admins\PropertyTypeResource;
use App\Http\Resources\Investors\Listings\ListingTimelineResource;

class ListingResource extends JsonResource
{

    public $type;

    public function __construct($resource, $type = null)
    {
        $this->type = $type;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $images = $this->images->sortBy("index");

        $locale = App::getLocale();
        $memberShips = json_decode($this->opened_member_ships);

        $userHasInvestorProfile = ($user = Auth::guard('sanctum')->user()) && Auth::guard('sanctum')->user()->investor;

        $user_id = Auth::guard('sanctum')->check() ? Auth::guard('sanctum')->id() : null;

        [$minInvestableShares, $maxInvestableShares] = $this->getMinAndMaxInvestableShares($user);

        $listingTags = $this->listing_tags;

        $data = [
            'id' => $this->id,  //
            'title' => $this->title, //
            'target' => $this->target->formatByDecimal(),
            'target_fmt' => cleanDecimal(number_format($this->target->formatByDecimal())),
            'gross_yield' => $this->gross_yield,
            'dividend_yield' => $this->dividend_yield,
            'start_date' => $this->start_time,
            'administrative_fees_percentage' => $this->administrative_fees_percentage,
            'deadline' => $this->deadline,
            'expected_net_return' => number_format($this->expected_net_return, 2) ." ". " %",
            'fund_duration' => $this->fund_duration ." ". trans("messages.month"),
            'min_investable_shares' => $minInvestableShares,
            'max_investable_shares' => $maxInvestableShares,
            'min_inv_share' => $this->min_inv_share,
            'min_inv_amount' => $minInvestmentAmount = $this->share_price->multiply($this->min_inv_share)->formatByDecimal(),
            'min_inv_amount_fmt' => number_format($minInvestmentAmount, 2),
            'max_inv_share' => $this->max_inv_share,
            'total_shares' => $this->total_shares,
            'rent_amount' => $this->rent_amount,
            'has_vote' => $this->votes->count() ? true : false, //
            'created_at' => $this->created_at->format('Y-m-d h:i'),
            'updated_at' => $this->updated_at->format('Y-m-d h:i'),
            'city' => new CityResource($this->whenLoaded('city')),
            'type' => new PropertyTypeResource($this->whenLoaded('type')),
            'investment_type' => InvestmentType::getDescription($this->investment_type),
            'team' => $this->when($userHasInvestorProfile, function () {
                return $this->whenLoaded('details', function () {
                    $item = $this->details->firstWhere('category', DetailCategory::Team);
                    return $item ? new TeamResource($item['value']) : null;
                });
            }),

            'complete_percent' => $this->fund_percent,

            'property_details' => $this->whenLoaded('details', function () use ($locale) {
                $item = $this->details->firstWhere('category', DetailCategory::PropertyDetails);
                return $item ? [
                    'description' => $item->value->get('description_' . $locale),
                    'files' => $this->filterAttachments(AttachmentType::Details),
                ] : null;
            }),
            'location' => $this->when($userHasInvestorProfile, function () use ($locale) {
                return $this->whenLoaded('details', function () use ($locale) {
                    $item = $this->details->firstWhere('category', DetailCategory::Location);
                    return $item ? [
                        'description' => $item->value->get('description_' . $locale),
                        'gmap_url' => $item->value->get('gmap_url'),
                        'files' => $this->filterAttachments(AttachmentType::Location),
                    ] : null;
                });
            }),
            'risks' => $this->when($userHasInvestorProfile, function () use ($locale) {
                return $this->whenLoaded('details', function () use ($locale) {
                    $item = $this->details->firstWhere('category', DetailCategory::Risks);
                    return $item ? [
                        'content' => $item->value->get('content_' . $locale),
                        'files' => $this->filterAttachments(AttachmentType::Risks)
                    ] : null;
                });
            }),
            'timeline' => $this->when($userHasInvestorProfile, function () {
                return $this->whenLoaded('details', function () {
                    $item = $this->details->firstWhere('category', DetailCategory::Timeline);
                    return $item ? new ListingTimelineResource($item['value']) : null;
                });
            }),
            'days_remaining' => $daysRemaining = now('Asia/Riyadh')->diffInDays($this->deadline, false),
            'hours_remaining' => now('Asia/Riyadh')->diffInHours($this->deadline, false),
            'financial_details' => $this->when($userHasInvestorProfile, function () use ($locale) {
                return $this->whenLoaded('details', function () use ($locale) {
                    $item = $this->details->firstWhere('category', DetailCategory::FinancialDetails);
                    return $item ? [
                        'content' => $item->value->get('content_' . $locale),
                        'files' => $this->filterAttachments(AttachmentType::FinancialDetails),
                    ] : null;
                });
            }),
            'due_diligence' => $this->when($userHasInvestorProfile, function () use ($locale) {
                return $this->whenLoaded('details', function () use ($locale) {
                    $item = $this->details->firstWhere('category', DetailCategory::DueDiligence);
                    return $item ? [
                        'content' => $item->value->get('content_' . $locale),
                        'files' => $this->filterAttachments(AttachmentType::DueDiligence),
                    ] : null;
                });
            }),
            'invested_shares' => $this->when($userHasInvestorProfile && isset($this->pivot->invested_shares), function () { //
                return $this->pivot->invested_shares;
            }),
            'invested_amount' => $this->when($userHasInvestorProfile && isset($this->pivot->invested_shares), function () {
                return $this->share_price->multiply($this->pivot->invested_shares)->formatByDecimal();
            }),
            'invested_amount_fmt' => $this->when($userHasInvestorProfile && isset($this->pivot->invested_shares), function () { //
                return
                    number_format(
                        $this->share_price->multiply($this->pivot->invested_shares)->formatByDecimal(),
                        2
                    );
            }),
           // 'images' => $this->whenLoaded('images', function () {return collect($this->images)->sortBy('index')->toArray() /*$this->images*/;})
            'images' => ListingEditImagesResource::collection($images) //
            ,
            'can_invest' => in_array($this->id, [21]) && in_array($user_id, []) ? true : $this->can_invest,
            'unit_price' => number_format($this->share_price->formatByDecimal(), 2),
            'authorized_entity' => $this->whenLoaded('authorizedEntity', fn () => optional($this->authorizedEntity)->name),
            'is_completed' => $this->is_completed,
            'is_closed' => $this->is_closed,
        ];

        return array_merge($data, $listingTags);
    }

    public function AdjustProprty($category)
    {
        return $this->whenLoaded('details', function () use ($category) {
            $item = $this->details->firstWhere('category', $category);
            return $item ? $item['value']->toArray() : null;
        });
    }

    public function filterAttachments($type)
    {
        $appType = $this->type;

        return ListingFileResource::collection(
            $this->attachments->filter(fn ($item) => $item->extra_info->get('type') === $type)->values(),
            $appType
        );
    }

    public function getMinAndMaxInvestableShares($user)
    {
        if (!$user) {
            return [$this->min_inv_share, $this->max_inv_share];
        }

        $investor = $this->investors->firstWhere('id', $user->id);

        if (!$investor) {
            return [$this->min_inv_share, $this->max_inv_share];
        }

        $investedShares = $investor->pivot->invested_shares;
        $remainingShares = $this->max_inv_share - $investedShares;

        if ($investedShares < $this->min_share) {
            return [$this->min_inv_share, $remainingShares];
        }

        if ($investedShares < $this->max_inv_share) {
            return [1, $remainingShares];
        }

        return [0, 0];
    }
}
