<?php

namespace App\Services\Campaigns\InvestorTypes;

use Illuminate\Support\Facades\DB;

class MembershipType
{

    public function getUsersData(array $selectedData, array $customData = [])
    {
        $data =  DB::table('member_ship_requests')
            ->whereIn('member_ship_requests.status', [1, 3])
            ->whereIn('member_ship_requests.member_ship', [2, 3])
            ->join('users', 'users.id', '=', 'member_ship_requests.user_id')
            ->select($selectedData, DB::raw('COUNT(member_ship_requests.user_id)'))
            ->groupBy('member_ship_requests.user_id')
            ->orderBy('member_ship_requests.user_id', 'asc')
            ->get();

        return json_decode($data, 1);
    }

}
