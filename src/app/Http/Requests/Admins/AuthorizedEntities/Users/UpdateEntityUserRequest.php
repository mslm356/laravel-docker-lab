<?php

namespace App\Http\Requests\Admins\AuthorizedEntities\Users;

use App\Common\ValidationRules\Listings\EntityUserRules;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEntityUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return EntityUserRules::getForUpdate($this->route('user'));
    }
}
