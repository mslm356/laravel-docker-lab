<div id="funds">
    <div class="px-4 py-10 mx-auto max-w-7xl sm:px-6">
        <div class="mb-4 ltr:ml-16 rtl:mr-16">
            <h1 class="text-3xl font-bold text-primary md:text-5xl">{{ __('website.listings') }}</h1>
            <p class="text-xl text-gray-500 md:text-2xl">{{ __('website.listings_description') }}</p>
        </div>
        <div class="md:flex md:flex-wrap">
            @foreach ($listings as $listing)
                <div class="my-8 md:w-1/2 lg:w-1/3 lg:my-4">
                    <div class="max-w-xs mx-auto shadow-lg group" style="perspective: 1000px;">
                        <div class="relative w-full h-full transition-transform duration-300 ease-in-out group-hover:rotate-y-180" style="transform-style:preserve-3d">
                            <div class="relative w-full h-full bg-white bv-hidden">
                                @if (!$listing->can_invest)
                                    <div class="absolute z-0 mt-2 ml-2 group-hover:hidden bv-hidden">
                                        <div class="px-1.5 py-1 bg-gray-500">
                                            <p class="text-white">{{ __('glossary.is_closed') }}</p>
                                        </div>
                                    </div>
                                @endif

                                @if ($listing->can_invest && $listing->days_remaining < 10)
                                    <div class="absolute z-0 mt-2 ltr:ml-2 rtl:mr-2 group-hover:hidden bv-hidden">
                                        <div class="px-1.5 py-1 bg-red-500">
                                            <p class="flex items-center text-white">
                                                <svg class="w-6 h-6 text-white" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M12.395 2.553a1 1 0 00-1.45-.385c-.345.23-.614.558-.822.88-.214.33-.403.713-.57 1.116-.334.804-.614 1.768-.84 2.734a31.365 31.365 0 00-.613 3.58 2.64 2.64 0 01-.945-1.067c-.328-.68-.398-1.534-.398-2.654A1 1 0 005.05 6.05 6.981 6.981 0 003 11a7 7 0 1011.95-4.95c-.592-.591-.98-.985-1.348-1.467-.363-.476-.724-1.063-1.207-2.03zM12.12 15.12A3 3 0 017 13s.879.5 2.5.5c0-1 .5-4 1.25-4.5.5 1 .786 1.293 1.371 1.879A2.99 2.99 0 0113 13a2.99 2.99 0 01-.879 2.121z" clip-rule="evenodd"></path>
                                                </svg>
                                                {{ __('glossary.closing_soon') }}
                                            </p>
                                        </div>
                                    </div>
                                @endif
                                <div class="bg-cover" style="background-image: url({{ $listing->image_url }}); height: 27rem">
                                </div>
                                <div class="p-3">
                                    <h1 class="text-lg text-gray-800 sm:text-2xl">{{ $listing->title }}</h1>
                                    <p class="text-base text-gray-500 sm:text-lg">{{ $listing->city_name }}</p>
                                </div>
                            </div>

                            <div class="absolute top-0 right-0 z-30 flex flex-col w-full h-full bg-white divide-y divide-gray-200 bv-hidden rotate-y-180" style="transform: rotateY(180deg);">
                                <a href="{{ investor_url(['funds', $listing->id]) }}" class="flex items-center p-3">
                                    <div class="w-16 h-16 overflow-hidden rounded-lg ltr:mr-3 rtl:ml-3">
                                        <div class="w-full h-full bg-cover" style="background-image: url({{ $listing->image_url }})">
                                        </div>
                                    </div>
                                    <div>
                                        <h1 class="text-lg text-gray-800 sm:text-xl">{{ $listing->title }}</h1>
                                        <p class="text-base text-gray-500 sm:text-lg">{{ $listing->city_name }}</p>
                                    </div>
                                </a>
                                <div class="flex flex-col justify-between flex-1 p-3">
                                    <div class="space-y-3">
                                        <x-website.listing-list-item
                                            :name="__('glossary.investment_type')" :value="$listing->investment_type"
                                        />
                                        <x-website.listing-list-item
                                            :name="__('glossary.property_type')" :value="$listing->type_name"
                                        />
                                        <x-website.listing-list-item
                                            :name="__('glossary.funding_target')" :value="'SAR '. $listing->target"
                                        />
                                        <x-website.listing-list-item
                                            :name="__('glossary.gross_yield')"
                                            :value="$listing->gross_yield . '%'"
                                            tooltipId="grossYield"
                                        />
                                        <x-website.listing-list-item
                                            :name="__('glossary.dividend_yield')"
                                            :value="$listing->dividend_yield . '%'"
                                            tooltipId="dividendYield"
                                        />
                                        @if(!$listing->can_invest)
                                            <x-website.listing-list-item
                                                :name="__('glossary.listing_status')" :value="__('glossary.is_closed')"
                                            />
                                        @else
                                            @if ($listing->days_remaining > 0)
                                                <x-website.listing-list-item
                                                    :name="__('glossary.days_remaining')"
                                                    :value="trans_choice('glossary.days', $listing->days_remaining)"
                                                />
                                            @endif
                                            @if ($listing->hours_remaining <= 24 && $listing->hours_remaining > 0)
                                                <x-website.listing-list-item
                                                    :name="__('glossary.hours_remaining')"
                                                    :value="trans_choice('glossary.hours', $listing->hours_remaining)"
                                                />
                                            @endif
                                        @endif
                                    </div>
                                    <div class="flex justify-end">
                                        <div>
                                            <a href="{{ investor_url(['funds', $listing->id]) }}" class="relative inline-block">
                                                <span class="text-primary-700 text-base py-0.5">{{ __('glossary.more_detail') }}</span>
                                                <div class="absolute inset-x-0 bottom-0 h-1.5 bg-primary-400" style="opacity: 0.2"></div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script>
    // With the above scripts loaded, you can call `tippy()` with a CSS
    // selector and a `content` prop:
    window.addEventListener('DOMContentLoaded', (event) => {
        window.tippy('#grossYield', {
            content: '{{ __('website.defs.gross_yield') }}',
            theme: 'light-border'
        });

        window.tippy('#dividendYield', {
            content: '{{ __('website.defs.dividend_yield') }}',
            theme: 'light-border'
        });
    });

</script>
