<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Listings\ListingDiscussion;
use App\Actions\Listings\Discussions\GetDiscussions;
use App\Http\Resources\AuthorizedEntities\Listings\DiscussionResource;

class DiscussionController extends Controller
{
    use CanAccessListing;

    /**
     * Store the discussion in the storage
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listing $listing)
    {
        $this->canUserAccessListing($listing);

        return DiscussionResource::collection(
            GetDiscussions::run($listing, ['user.investor', 'replies.userRole'])
        );
    }

    /**
     * Store the discussion in the storage
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($listing, $discussion)
    {
        $discussion = ListingDiscussion::where('listing_id', $listing)->findOrFail($discussion);

        $this->canUserAccessListing($discussion->listing);

        return new DiscussionResource(
            $discussion->load('user.investor', 'replies.userRole')
        );
    }

    /**
     * Delete the discussion from the storage
     *
     * @param int $listing
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, $discussion)
    {
        $discussion = ListingDiscussion::whereNull('parent_id')
            ->where('listing_id', $listing)
            ->findOrFail($discussion);

        $this->canUserAccessListing($discussion->listing);

        DB::transaction(function () use ($discussion) {
            $discussion->delete();

            $discussion->replies()->delete();
        });

        return response()->json();
    }
}
