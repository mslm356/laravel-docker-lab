<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeColums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('listing_investor_records','invested_shares'))
            Schema::table('listing_investor_records', function (Blueprint $table) {
                $table->dropColumn('invested_shares');
            });

        if (Schema::hasColumn('listings','investment_shares'))
            Schema::table('listings', function (Blueprint $table) {
                $table->dropColumn('investment_shares');
            });


        Schema::table('listing_investor_records', function (Blueprint $table) {
            $table->integer('invested_shares')->default(0)->after('investor_id');
        });

        Schema::table('listings', function (Blueprint $table) {
            $table->unsignedInteger('investment_shares')->default(0)->after('total_shares');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listing_investor_records', function (Blueprint $table) {
            $table->dropColumn('invested_shares');
        });

        Schema::table('listings', function (Blueprint $table) {
            $table->dropColumn('investment_shares');
        });

    }
}
