<?php

namespace App\Http\Resources\Common\Listings;

use Illuminate\Support\Arr;
use App\Enums\Banking\BankCode;
use App\Enums\Listings\DetailCategory;
use App\Enums\Listings\ListingReviewStatus;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class ListingRevisionResource extends JsonResource
{
    protected $detailsArray = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $model = Arr::get($this->model_state, 'with_relations', []);

        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'created_at' => $this->created_at->toDateString(),
            'listing_info' => [
                'id' => Arr::get($model, 'id'),
                'title_en' => Arr::get($model, 'title_en'),
                'title_ar' => Arr::get($model, 'title_ar'),
                'target' => Arr::get($model, 'target'),
                'gross_yield' => Arr::get($model, 'gross_yield'),
                'dividend_yield' => Arr::get($model, 'dividend_yield'),
                'deadline' => Carbon::parse(Arr::get($model, 'deadline'))->toDateString(),
                'min_inv_share' => Arr::get($model, 'min_inv_share'),
                'max_inv_share' => Arr::get($model, 'max_inv_share'),
                'reserved_shares' => Arr::get($model, 'reserved_shares'),
                'total_shares' => Arr::get($model, 'total_shares'),
                'rent_amount' => Arr::get($model, 'rent_amount'),
                'is_closed' => Arr::get($model, 'is_closed'),
                'investment_type' => Arr::get($model, 'investment_type'),
                'is_visible' => Arr::get($model, 'is_visible'),
                'city' => [
                    'id' => Arr::get($model, 'city.id'),
                    'name' => Arr::get($model, locale_suff('city.name')),
                ],
                'type' => [
                    'id' => Arr::get($model, 'type.id'),
                    'name' => Arr::get($model, locale_suff('type.name')),
                ],
                'team' => array_values($this->findDetailsByCategory($model, DetailCategory::Team, [])),
                'timeline' => $this->findDetailsByCategory($model, DetailCategory::Timeline, []),
                'property_details' => $this->findDetailsByCategory($model, DetailCategory::PropertyDetails),
                'financial_details' => $this->findDetailsByCategory($model, DetailCategory::FinancialDetails),
                'risks' => $this->findDetailsByCategory($model, DetailCategory::Risks),
                'bank_account' => !Arr::get($model, 'bank_account') ?
                    null :
                    BankCode::getDescription(Arr::get($model, 'bank_account.bank')) . ' - ' . Arr::get($model, 'bank_account.iban'),
                'authorized_entity' => Arr::get($model, 'authorized_entity') ?
                    Arr::get($model, 'authorized_entity.name') :
                    null,
                'due_diligence' => array_merge(
                    $this->findDetailsByCategory($model, DetailCategory::DueDiligence, []),
                    [
                        'files' => collect(Arr::get($model, 'due_diligence_files', []))
                            ->map(fn ($file) => [
                                'id' => $file['id'],
                                'url' => route('admin.files.download', ['file' => $file['id']]),
                                'name' => $file['name']
                            ])
                    ]
                ),
                'images' => collect(Arr::get($model, 'images', []))->map(fn ($file) => [
                    'id' => $file['id'],
                    'url' => $file['url'],
                    'name' => $file['name']
                ]),
                'review_status' => [
                    'value' => Arr::get($model, 'review_status'),
                    'description' => ListingReviewStatus::getDescription((int)Arr::get($model, 'review_status')),
                ]
            ]
        ];
    }

    public function mapTeams($teams)
    {
        return collect($teams)->map(function ($team) {
            if (!isset($team['members']) || !is_array($team['members'])) {
                $team['members'] = [];
            }

            return $team;
        })
            ->values()
            ->toArray();
    }

    public function findDetailsByCategory($data, $category, $default = null)
    {
        if ($this->detailsArray === null) {
            $this->detailsArray = Arr::get($data, 'details', []);
        }

        return Arr::get(
            Arr::first(
                $this->detailsArray,
                fn ($value) => $value['category'] === $category,
                []
            ),
            'value',
            $default
        );
    }
}
