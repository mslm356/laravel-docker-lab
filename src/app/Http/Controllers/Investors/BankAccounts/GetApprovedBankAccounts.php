<?php

namespace App\Http\Controllers\Investors\BankAccounts;

use Illuminate\Http\Request;
use App\Enums\BankAccountStatus;
use App\Enums\Banking\BankCode;
use App\Http\Controllers\Controller;

class GetApprovedBankAccounts extends Controller
{
    /**
     * Get the bank accounts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $accounts = $request->user('sanctum')->bankAccounts()->where('status', BankAccountStatus::Approved)->get()
            ->map(fn ($item) => [
                'id' => $item->id,
                'name' => BankCode::getDescription($item->bank) . ' - ' . $item->iban
            ]);

        return response()->json([
            'data' => $accounts
        ]);
    }
}
