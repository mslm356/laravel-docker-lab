<?php


namespace App\Services\Investors;

use App\Http\Controllers\Controller;
use App\Models\Users\NationalIdentity;
use App\Services\LogService;
use Illuminate\Support\Facades\DB;

class VerifyAccount extends Controller
{

    public $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function verify($user)
    {

        if (!$user || !$user->investor) {
            $this->logService->log('VERIFY-ACCOUNT user not valid or profile');
            return false;
        }

        return DB::transaction(function () use ($user) {

            if (!$user->investor->data) {
                $this->logService->log('VERIFY-ACCOUNT identity data not valid');
                return false;
            }

            if (!$user->investor->data['identity_verification_tmp']) {
                $this->logService->log('VERIFY-ACCOUNT identity tmp data not valid');
                return false;
            }

            if ($user->investor->is_identity_verified) {
                $this->logService->log('VERIFY-ACCOUNT account is verified');
                return false;
            }

            $nationality = NationalIdentity::query()
                ->where('nin', $user->investor->data['identity_verification_tmp']['identity']['nin'])
                ->count();

            if ($nationality) {
                $this->logService->log('VERIFY-ACCOUNT nin already exists');
                return false;
            }

            $identity = NationalIdentity::create($user->investor->data['identity_verification_tmp']['identity']);

            foreach ($user->investor->data['identity_verification_tmp']['addresses'] as $address) {
                $identity->addresses()->create($address);
            }

            $user->investor->update([
                'is_identity_verified' => true,
                'data->identity_verification_tmp' => null,
            ]);
        });

    }

    public function deleteOldIdentities($user)
    {
        $oldIdentities = NationalIdentity::where('user_id', $user->id)->get();

        foreach ($oldIdentities as $oldIdentity) {
            $oldIdentity->addresses()->delete();
            $oldIdentity->delete();
        }
    }

}
