<?php

namespace App\Jobs\Absher;

use App\Enums\Elm\AbsherOtpReason;

use App\Support\Elm\AbsherOtp\VerificationSms;
use App\Support\Elm\Yaqeen\Exceptions\AddressLanguageException;
use App\Support\Elm\Yaqeen\Exceptions\BirthDateException;
use App\Support\Elm\Yaqeen\Exceptions\CrException;
use App\Support\Elm\Yaqeen\Exceptions\ExternalSystemException;
use App\Support\Elm\Yaqeen\Exceptions\IdException;
use App\Support\Elm\Yaqeen\Exceptions\MissingInfoRelatedToIdException;
use App\Traits\NotificationServices;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;

class SendAbsherOtp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationServices;

    public $token;
    public $user;
    public $delay;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $token, $delay)
    {
        $this->token = $token;
        $this->user = $user;
        $this->delay = $delay;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            VerificationSms::instance()->send(
                $this->user->nationalIdentity->nin,
                $this->user,
                [
                    'subscription_token_id' => $this->token->id,
                    'reason' => __(
                        'absher_otp_purpose.' . AbsherOtpReason::InvestingInFund,
                        [
                            'fund' => $this->token->listing->{locale_suff('title', $preferredLocale = $user->locale ?? App::getLocale())}
                        ],
                        $preferredLocale
                    )
                ]
            );

            sleep($this->delay);

        } catch (IdException | MissingInfoRelatedToIdException | CrException |BirthDateException | AddressLanguageException | ExternalSystemException  $e) {
            $code = $this->logServices->log('INVESTMENTS-SEND-OTP', $e);
        } catch (ValidationException $e) {
            $code = $this->logServices->log('ValidationException-INVESTMENTS-SEND-OTP', $e);
        } catch (AuthorizationException $e) {
            $code = $this->logServices->log('INVESTMENTS-SEND-OTP-INVESTMENTS-AUTHORIZATION-EXCEPTION', $e);
        } catch (\Exception $e) {
            $code = $this->logServices->log('INVESTMENTS-SEND-OTP', $e);
        }
    }
}
