<?php


namespace App\Support\Zoho;


use App\Models\ZohoSettings;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ManualJournal
{

    public $base_url;
    public $organization_id;
    public $auth;

    public function __construct()
    {
        $this->base_url = config('zoho.base_url');
        $this->organization_id = config('zoho.organization_id');
        $this->auth = new Auth();
    }

    public function store($data)
    {

        $token = $this->auth->getToken();

        try {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'journals?organization_id=' . $this->organization_id, $data);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {
                return $responseData['journal']['journal_id'];
            }

            Log::error('ZOHO-JOURNAL', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-JOURNAL', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return null;
    }

//    FOR PAYOUT IN ADMIN
    public function storeJournalForWithCustomData ($journalData) {

        $token = $this->auth->getToken();

        try {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json;charset=UTF-8',
                'Authorization' => 'Zoho-oauthtoken ' . $token,
            ])
                ->post($this->base_url . 'journals?organization_id=' . $this->organization_id, $journalData);

            $responseData = json_decode($response->body(), true);

            if (isset($responseData['code']) && $responseData['code'] == 0) {
                return true;
            }

            Log::error('ZOHO-JOURNAL', [$responseData]);

        } catch (\Exception $e) {
            Log::error('ZOHO-JOURNAL', [$e->getMessage(), $e->getLine(), $e->getFile(), $e->getCode()]);
        }

        return false;
    }
}
