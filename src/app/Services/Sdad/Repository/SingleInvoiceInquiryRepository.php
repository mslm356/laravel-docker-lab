<?php

namespace App\Services\Sdad\Repository;

use App\Services\Exeptions\NoResponseDataException;
use App\Services\LogService;
use App\Services\Sdad\Enums\ServiceEnums;
use Illuminate\Support\Facades\Http;

class SingleInvoiceInquiryRepository
{
    public $provider_repo;
    public $logService;

    public function __construct()
    {
        $this->provider_repo = new EdaatProviderClientRepository();
        $this->logService = new LogService();
    }

    /**
     * @return void
     * @throws \App\Services\Exeptions\NoResponseDataException
     * because it HAS A URL PREMATERS
     * TODO:make it dynamic from edaat config
     */
    public function initiate_provider($invoice_id): ?self
    {
        $service_name = ServiceEnums::service_name(ServiceEnums::SINGLE_INVOICE_BRIEF);
        $this->provider_repo->auth_repo = new EdaatAuthRepository();
        $this->provider_repo->service_name = $service_name;
        $this->provider_repo->content_type = $this->provider_repo->get_content_type($service_name);
        $this->provider_repo->url = $this->get_url($invoice_id);
        $this->provider_repo->method = $this->provider_repo->get_method($service_name);
        $this->provider_repo->auth_required = $this->provider_repo->check_auth($service_name);
        $this->provider_repo->client = Http::withHeaders($this->provider_repo->headers_preparation());
        if ($this->provider_repo->auth_required) {
            $token = $this->provider_repo->auth_repo->getValidToken()->token;
            $this->provider_repo->client = $this->provider_repo->client->withToken($token);
        }
        return $this;
    }

    public function inquiry($invoice_id): ?object
    {
        try {
            $this->provider_repo->send();
            if ($this->provider_repo->response->successful()) {
                return $this->provider_repo->getResponse();

            } else {
                throw new NoResponseDataException();
            }
        } catch (NoResponseDataException $e) {
            throw new NoResponseDataException();
        } catch (\Exception $e) {
            $this->logService->log('Sdad-inquiry_repo', $e, ['issue', $invoice_id]);

            throw new \Exception('inquiry repo error');
        }
    }

    public function get_url($invoice_id): string
    {
        if (config('edaat.mode') == 'test') {
            return config('edaat.test.base') . "/api/v2/Invoices/$invoice_id/Brief";
        } else {
            return config('edaat.live.base') . "/api/v2/Invoices/$invoice_id/Brief";
        }
    }
}
