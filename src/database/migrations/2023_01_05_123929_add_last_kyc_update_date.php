<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class AddLastKycUpdateDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->timestamp('last_kyc_update_date')->default(DB::raw('CURRENT_TIMESTAMP'))->after('is_kyc_completed');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_profiles', function (Blueprint $table) {
            $table->dropColumn('last_kyc_update_date');
        });

    }
}
