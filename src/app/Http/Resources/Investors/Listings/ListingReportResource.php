<?php

namespace App\Http\Resources\Investors\Listings;

use Illuminate\Http\Resources\Json\JsonResource;

class ListingReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $guard = $request->guard;

        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'files' => $this->files->map(
                fn ($file) => [
                    'name' => $file->name,
                    'url' =>  $guard == 'api' ? url('api/app/files/' . $file->id . '/get-url') : route('investor.files.get.url', ['file'=> $file->id]),
                    'get_url' =>  $guard == 'api' ? url('api/app/files/' . $file->id . '/get-url') : route('investor.files.get.url', ['file'=> $file->id]),
                ]
            ),
            'created_at' => $this->created_at->format('Y-m-d'),
        ];
    }
}
