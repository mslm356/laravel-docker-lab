<?php

namespace App\Common\ValidationRules\Listings;

use App\Rules\NumericString;

class ListingValuationRules
{
    static public function get()
    {
        return [
            'value' => ['required', new NumericString],
            'date' => ['required', 'date'],
        ];
    }
}
