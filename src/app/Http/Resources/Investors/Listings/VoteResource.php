<?php

namespace App\Http\Resources\Investors\Listings;

use App\Enums\Listings\VoteAnswers;
use Illuminate\Http\Resources\Json\JsonResource;

class VoteResource extends JsonResource
{

    protected $foo;


    public function __construct($resource, $user)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $auth = $request->user('api');

        return [
            'id' => $this->id,
            'vote' => $this->question,
            'answer' => $this->getAnswerValue($this, $auth),
            'answerOptions' => $this->getAnswerOptionValue($this, $auth),
        ];
    }

    public function getAnswerValue($vote, $auth)
    {
        $userVote = $this->InvestorVotes()->where('investor_id', $auth->id)->first();

        if ($vote && $this->status == 1 && !$userVote) {
            return  __('messages.voting_ended');
        }

        if ($vote && $this->status == 1) {
            return $userVote ? $userVote->vote_answer : __('messages.refrain');
        }

        if ($vote && $this->status == 0) {
            return $userVote ? $userVote->vote_answer : null;
        }
    }

    public function getAnswerOptionValue($vote, $auth)
    {
        $userVote = $this->InvestorVotes()->where('investor_id', $auth->id)->first();
        if ($this->status == 1 || $userVote || ($this->status == 0 && $userVote)) {
            return null;
        } else {
            return $this->getAnswerOptions();
        }

    }

    public function getAnswerOptions()
    {
        return [
            [
                'label' => __('messages.approved'),
                'value' => VoteAnswers::Approved
            ],
            [
                'label' => __('messages.rejected'),
                'value' => VoteAnswers::Rejected
            ]
        ];

    }
}
