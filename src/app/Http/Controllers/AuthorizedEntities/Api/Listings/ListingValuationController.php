<?php

namespace App\Http\Controllers\AuthorizedEntities\Api\Listings;

use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Actions\Listings\Valuations\UpdateOrCreateValuation;
use App\Common\ValidationRules\Listings\ListingValuationRules;
use App\Http\Resources\AuthorizedEntities\Listings\ListingValuationResource;
use App\Http\Resources\AuthorizedEntities\Listings\ValuationListItemResource;
use App\Models\Listings\ListingValuation;

class ListingValuationController extends Controller
{
    use CanAccessListing;

    /**
     * Get all the valuations of listing
     *
     * @param int $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $valuations = $listing->valuations()
            ->latest()
            ->simplePaginate();

        return ValuationListItemResource::collection($valuations);
    }

    /**
     * Store the valuation in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Listing $listing, ListingValuation $valuation)
    {
        $this->canUserAccessListing($listing);

        return new ListingValuationResource($valuation);
    }

    /**
     * Store the valuation in the storage
     *
     * @param int $discussion
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        $this->canUserAccessListing($listing);

        $data = $this->validate($request, ListingValuationRules::get());

        DB::transaction(function () use ($data, $listing) {
            UpdateOrCreateValuation::run($data, $listing);
        });

        return response()->json([], 201);
    }

    /**
     * Update the valuation in the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Listings\ListingReport $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $listing, ListingValuation $valuation)
    {
        $this->canUserAccessListing($valuation->listing);

        $data = $this->validate($request, ListingValuationRules::get());

        DB::transaction(function () use ($data, $valuation) {
            UpdateOrCreateValuation::run($data, null, $valuation);
        });

        return response()->json();
    }

    /**
     * Delete the valuation from the storage
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Listings\ListingReport $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($listing, ListingValuation $valuation)
    {
        $this->canUserAccessListing($valuation->listing);

        $valuation->delete();

        return response()->json();
    }
}
