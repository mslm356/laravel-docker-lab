<?php

namespace App\Exceptions;

use Exception;

class NoInvestorWalletFound extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        $message = trans('messages.no_active_wallet_found_for_investor');

        if ($request->expectsJson()) {
            return response()->json([
                'message' => $message
            ], 400);
        }

        return response($message, 400);
    }
}
