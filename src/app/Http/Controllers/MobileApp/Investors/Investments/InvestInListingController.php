<?php

namespace App\Http\Controllers\MobileApp\Investors\Investments;

use App\Http\Controllers\Controller;
use App\Services\Investors\Investment\InvestingServices;
use App\Traits\NotificationServices;
use Illuminate\Http\Request;

class InvestInListingController extends Controller
{
    use NotificationServices;

    public $logServices;
    public $investingServices;

    public function __construct()
    {
        $this->investingServices = new InvestingServices();
    }

    public function __invoke(Request $request)
    {
        try {

            $user = getAuthUser('api');

            $response = $this->investingServices->handle($request, $user);

            return $this->handelStatusResponce($response);

        } catch (\Exception $e) {
            return $this->handleMobileInvestmentException($e, 'INVESTMENT-MOBILE-Mobile');

        }
    }
}
