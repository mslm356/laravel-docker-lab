<?php

namespace App\Models\Traits;

use App\Models\File;

trait HasFiles
{
    /**
     * Get listing files
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    abstract function morphMany($related, $name, $type = null, $id = null, $localKey = null);
}
