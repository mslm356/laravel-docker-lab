<?php

namespace App\Http\Controllers\Investors\Profile;

use App\Http\Resources\Investors\CompanyProfileResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowMyProfile extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user();

        return response()->json([
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'phone' => $user->phone_number,
            'company_profile' => $user->companyProfile ?  new CompanyProfileResource($user->companyProfile ) : null,
        ]);
    }
}
