<?php

namespace App\Http\Resources\Investors\Listings;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class PropertyDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $appLocal = App::getLocale();

        $detail = $this->except([
            'description_en', 'description_ar',
            'location'
        ])->all();

        $propertyDetails = $this->all();

        $detail['description'] = $propertyDetails['description_' . $appLocal];

        //location object
        $detail['location'] = [];
        $detail['location']['description'] = $propertyDetails['location']['description_' . $appLocal];
        $detail['location']['longitude'] = $propertyDetails['location']['longitude'];
        $detail['location']['latitude'] = $propertyDetails['location']['longitude'];

        return $detail;
    }
}
