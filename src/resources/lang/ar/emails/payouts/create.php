<?php

return [
    'subject' => 'عملية جديدة في حسابك إيداع عوائد',
    'subject_dis' => 'عملية جديدة في حسابك إيداع توزيعات',
    'welcome_message' => ' مستثمرنا الكريم ' ,
    'username' => ':name',
    'message_1' => 'لديك عملية جديدة في حسابك',
    'message_2' => ' إيداع عوائد استثمار للفرصة ' ,
    'message_2_dis' => ' توزيع أرباح لفرصة ' ,
    'listingName' => ':listing_name' ,
    'listing_num' => ':listing_num' . ' رقم ' ,
    'amount' => ':amount' . ' ريال سعودى ' ,
    'link_description' => 'وللاطلاع على لوحه التحكم الخاصة بك ، يرجى الدخول إلى الرابط',
    'dashboard_link' => 'عرض لوحه التحكم الخاصة بك',
];
