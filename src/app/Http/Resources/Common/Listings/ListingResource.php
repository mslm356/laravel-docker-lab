<?php

namespace App\Http\Resources\Common\Listings;

use App\Enums\Investors\InvestorStatus;
use App\Enums\Listings\AttachmentType;
use App\Enums\Listings\DetailCategory;
use App\Enums\Listings\ListingReviewStatus;
use App\Enums\MemberShips\MemberShipType;
use App\Http\Resources\Common\CityResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Admins\PropertyTypeResource;

class ListingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dueDiligence = $this->AdjustProprty(DetailCategory::DueDiligence) ?? [];
        $memberShips = json_decode($this->opened_member_ships);
        return [
            'id' => $this->id,
            'title_en' => $this->title_en,
            'title_ar' => $this->title_ar,
            'target' => $this->target->formatByDecimal(),
            'gross_yield' => $this->gross_yield,
            'dividend_yield' => $this->dividend_yield,
            'deadline' => $this->deadline->toDateString(),
            'work_date' => optional(\DateTime::createFromFormat('Y-m-d h:i:s', $this->work_date))->format("Y-m-d") ,
            'min_inv_share' => $this->min_inv_share,
            'max_inv_share' => $this->max_inv_share,
            'total_shares' => $this->total_shares,
            'reserved_shares' => $this->reserved_shares,
            'rent_amount' => $this->rent_amount,
            'is_closed' => $this->is_closed,
            'investment_type' => $this->investment_type,
            'is_visible' => $this->is_visible,
            'is_completed' => $this->is_completed,
            'early_investment_date' => $this->early_start_time,
            'investor_membership' =>  is_array($memberShips) && in_array(MemberShipType::Regular , $memberShips) ? true : false,
            'gold_membership' =>  is_array($memberShips) && in_array(MemberShipType::Gold , $memberShips) ? true : false,
            'diamond_membership' =>  is_array($memberShips) && in_array(MemberShipType::Diamond , $memberShips) ? true : false,
            'upgraded_membership' =>  is_array($memberShips) && in_array(MemberShipType::Upgraded , $memberShips) ? true : false,
            'start_at' => $this->start_time,
            'administrative_fees_percentage' => $this->administrative_fees_percentage,
            'expected_net_return' => $this->expected_net_return,
            'fund_duration' => $this->fund_duration ,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'city' => new CityResource($this->whenLoaded('city')),
            'type' => new PropertyTypeResource($this->whenLoaded('type')),
            'team' => $this->AdjustProprty(DetailCategory::Team) ?? [],
            'timeline' => $this->AdjustProprty(DetailCategory::Timeline) ?? [],
            'property_details' => array_merge(
                $this->AdjustProprty(DetailCategory::PropertyDetails) ?? [],
                [
                    'files' => $this->filterAttachments(AttachmentType::Details)
                ],
            ),
            'financial_details' => array_merge(
                $this->AdjustProprty(DetailCategory::FinancialDetails) ?? [],
                [
                    'files' => $this->filterAttachments(AttachmentType::FinancialDetails)
                ]
            ),
            'risks' => array_merge(
                $this->AdjustProprty(DetailCategory::Risks) ?? [],
                [
                    'files' => $this->filterAttachments(AttachmentType::Risks)
                ]
            ),
            'bank_account_id' => $this->bank_account_id,
            'authorized_entity_id' => $this->authorized_entity_id,
            'due_diligence' => array_merge(
                $dueDiligence,
                [
                    'files' => $this->filterAttachments(AttachmentType::DueDiligence)
                ]
            ),
            'proof_docs' => [
                'files' => $this->filterAttachments(AttachmentType::ProofDocs)
            ],
            'location' => array_merge(
                $this->AdjustProprty(DetailCategory::Location) ?? [],
                [
                    'files' => $this->filterAttachments(AttachmentType::Location)
                ]
            ),
            'terms_conditions' => $this->AdjustProprty(DetailCategory::TermsAndConditions) ?? [],
            'images' => $this->whenLoaded('images', function () {
                return collect($this->images)->sortBy('index')->toArray()   /* $this->images */;
            }),
            'review_status' => [
                'value' => $this->review_status,
                'description' => ListingReviewStatus::getDescription($this->review_status),
            ],
            'investing_started' => $this->investingStarted(),
        ];
    }

    /**
     * Helper function to return listing details
     * category on the object root
     */
    public function AdjustProprty($category)
    {
        return $this->whenLoaded('details', function () use ($category) {
            $item = $this->details->firstWhere('category', $category);
            return $item ? $item['value']->toArray() : null;
        });
    }

    public function filterAttachments($type)
    {
        return $this->attachments->filter(fn ($item) => $item->extra_info->get('type') === $type)->values();
    }
}
