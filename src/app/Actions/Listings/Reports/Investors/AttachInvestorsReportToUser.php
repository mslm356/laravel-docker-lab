<?php

namespace App\Actions\Listings\Reports\Investors;

use App\Actions\Listings\Reports\Investors\ExportInvestorsReport;
use App\Http\Resources\Admins\Listings\ListingInvestorResource;
use App\Models\File;
use App\Enums\FileType;
use App\Models\Users\User;
use App\Services\LogService;
use Illuminate\Support\Str;
use App\Models\Listings\Listing;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Concerns\AsAction;

class AttachInvestorsReportToUser
{
    use AsAction;

    protected $logServices;

    public function __construct()
    {
        $this->logServices = new LogService();
    }


    /**
     *Handle exporting fund investors report and attach it to a user with watermark
     * @param User $user
     * @param Listing $listing
     * @return void
     */
    public function handle(User $user, Listing $listing)
    {

        try {

            $time = now('Asia/Riyadh')->format('Y-m-d-H:i:s');

            $listing->load('investors');
            $data = ListingInvestorResource::collection($listing->investors);
            $data = json_decode($data->toJson(), true);
            $html = view('listings.investors.report', [
                'investors' => $data,
                'listing' => $listing,
                'user_name_en' => $user->getAbsherName('en')
            ])
                ->render();

            /** @var \Illuminate\Http\UploadedFile $file */
            $filePath = ExportInvestorsReport::run(
                $originalFileName = "{$time}-{$user->id}-investors-report.pdf",
                $html
            );

            $fileModel = File::create([
                'id' => (string)Str::uuid(),
                'name' => $originalFileName,
                'fileable_id' => $user->id,
                'fileable_type' => $user->getMorphClass(),
                'type' => FileType::FundInvestorsReport,
                'extra_info' => collect([
                    'listing_id' => $listing->id,
                ]),
                'path' => $filePath
            ]);

            return $fileModel;

        } catch (\Exception $e) {
            $this->logServices->log('SUBSCRIPTION-FORUM-EXCEPTION', $e);
        }
    }

    public function asJob(...$args)
    {
        DB::transaction(function () use ($args) {
            $this->handle(...$args);
        });
    }
}
