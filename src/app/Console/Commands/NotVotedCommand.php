<?php

namespace App\Console\Commands;

use App\Models\Vote;
use App\Services\LogService;
use Illuminate\Console\Command;
use App\Jobs\NotificationFirebaseBulk;
use App\Models\Listings\ListingInvestor;
use App\Enums\Notifications\NotificationTypes;

class NotVotedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vote-notification {{--vote_id=}} {{--listing_id=}}';
    public $count = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send notification to people does not vote in listing';

    public $logService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logService = new LogService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $vote = Vote::find($this->option('vote_id'));

            if (!$vote) {
                dd('vote not valid');
            }

            $votedUsers = $vote->InvestorVotes
                ->pluck('investor_id')
                ->toArray();
            $usersCollection = collect();

            ListingInvestor::query()
                ->where('listing_id', $this->option('listing_id'))
                ->where('invested_shares', '>', 0)
                ->whereNotIn('investor_id', $votedUsers)
                ->lazyById()
                ->each(function ($listingInvestor) use ($usersCollection) {
                    $this->count++;
                    dump('user_id ---> ' . $listingInvestor->investor_id);
                    $usersCollection->push($listingInvestor->investor);
                });

            $notificationData = $this->notificationData($this->option('listing_id'));

            $notification = new NotificationFirebaseBulk(NotificationTypes::Fund, $notificationData, $usersCollection);
            dispatch($notification)->onQueue('notify');

            dd($this->count); // 677

            //            User::query()
            //                ->leftJoin('listing_investor', 'users.id', '=', 'listing_investor.investor_id')
            //                ->leftJoin('listings', 'listings.id', '=', 'listing_investor.listing_id')
            //                ->leftJoin('votes', 'votes.fund_id', '=', 'listings.id')
            //                ->leftJoin('investor_votes', 'investor_votes.vote_id', '=', 'votes.id')
            //
            //               ->where('investor_votes.investor_id', null)
            //                ->where('listings.payout_done', 0)
            //                ->where('listing_investor.invested_shares', '>', 0)
            //                ->where('listings.id', 20)
            //                ->select('users.*')
            //               ->lazyById()
            //                ->each(function ($user) {
            //                    $this->count++;
            //                    echo 'user_id ---> '.$user->id."\n";
            //                    $notificationData = $this->notificationData();
            //                    $this->send(NotificationTypes::Info, $notificationData, $user->id);
            //                });


        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getLine());
        }
    }


    public function notificationData($listing_id)
    {

        return [
            'action_id' => $listing_id,
            'title_ar' => 'منصة أصيل', [], 'ar',
            'title_en' => 'منصة أصيل', [], 'ar',
            'content_ar' => 'عزيزنا مستثمر صندوق نمو الأحساء العقاري، يوجد تصويت نأمل الاطلاع عليه', [], 'ar',
            'content_en' => 'عزيزنا مستثمر صندوق نمو الأحساء العقاري، يوجد تصويت نأمل الاطلاع عليه', [], 'ar',
            'channel' => 'mobile'
        ];
    }
}
