<?php

namespace App\Console\Commands\Wallets;

use App\Enums\Banking\TransactionReason;
use App\Enums\Banking\WalletType;
use App\Models\Users\User;
use App\Models\VirtualBanking\Transaction;
use App\Models\VirtualBanking\Wallet;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RefreshWallet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:special-wallet {--wallet_id=} {--more_than=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

//        $wallet = Wallet::find($this->option('wallet_id'));


        if ($this->option('wallet_id') == 'all') {
            $walletIds = Transaction::query()->distinct('wallet_id');

            if ($this->option('more_than')) {
                $walletIds->where('wallet_id','>', $this->option('more_than'));
            }

            $walletIds = $walletIds->select('wallet_id')->get()->toArray();

        } else {
            $walletIds[0]['wallet_id'] = $this->option('wallet_id');
        }

        $walletIds = array_column($walletIds, 'wallet_id');

        foreach ($walletIds as $walletId) {

            $wallet = Wallet::find($walletId);

            $oldBalance = $wallet->balance;


            if (!$wallet) {
                dd('wallet not valid');
            }

            $wallet->refreshBalance();

            $wallet->is_overview_statistics_consistent = 0;
            $wallet->save();

            dump('done successfully old balance => ' . $oldBalance . ' new => ' . $wallet->balance . 'wallet_id => ' . $wallet->id);
        }

    }
}
