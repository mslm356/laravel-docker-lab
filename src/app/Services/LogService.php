<?php


namespace App\Services;

use Illuminate\Support\Arr;

class LogService
{
    public function log(string $name, object $excObj = null, array $extraData = [])
    {
        try {

            $defaultAuthId = isset($extraData['user_id']) ? $extraData['user_id'] : '';

            $authId = auth()->check() ? auth()->id() : $defaultAuthId;

            $code = rand(100, 1000000) .'-'. $authId;

            $data = [
                'code' => strval($code),
                'error' => '',
            ];

            if ($excObj && $excObj != null) {

                $data['error'] = $excObj->getMessage();
                $data['file'] = $excObj->getfile();
                $data['line'] = $excObj->getline();
            }

            if ($excObj == null && !empty($extraData)) {
                $data['error'] = Arr::first($extraData);
            }

            $data = array_merge($extraData, $data);

            $message = Arr::has($data, 'error') ? $data['error'] : '';

            $log_from = config('app.log_from');

            $message = is_string($message) ? $message : '';

            $title = $log_from .'-'. $code .'-'. $name . ' => ' . $message;

            logger($title, $data);

            return $code;

        } catch (\Exception $e) {
            logger($e->getMessage());
        }
    }
}
