<?php

namespace App\Enums\Banking;

use BenSampo\Enum\Enum;

final class VirtualAccountStatus extends Enum
{
    const Active =   1;
    const Inactive =   2;
}
