<?php

namespace App\Http\Resources\AuthorizedEntities\Listings;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Listings\EditListingResource as CommonEditListingResource;

class EditListingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonEditListingResource($this->resource))->toArray($request),
            []
        );
    }
}
