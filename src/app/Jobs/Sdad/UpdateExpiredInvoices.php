<?php

namespace App\Jobs\Sdad;

use App\Models\EdaatInvoice;
use App\Services\LogService;
use App\Services\Sdad\Enums\InvoiceCodesStatusesEnums;
use App\Support\Zoho\CustomerPayments;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateExpiredInvoices implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $cachedCollectorWallet = null;
    protected $cachedWallets = [];
    protected $zohoCustomerPayments;
    public $logServices;

    public function __construct()
    {
        $this->zohoCustomerPayments = new CustomerPayments();
        $this->logServices = new LogService();
    }

    public function handle()
    {
        try {
            EdaatInvoice::query()
                ->where('status', '!=', InvoiceCodesStatusesEnums::PAID)
                ->lazyById()
                ->each(function ($invoice) {

                    $expire_at = Carbon::parse($invoice->expire_at);

                    if ($expire_at->lessThanOrEqualTo(now())) {
                        $invoice->status = InvoiceCodesStatusesEnums::EXPIRED;
                        $invoice->save();
                    }
                });

            $this->logServices->log('SDAD-UPDATE-EXPIRED-INVOICES-WORK', null, ['done']);

        } catch (\Exception $e) {
            $this->logServices->log('SDAD-UPDATE-EXPIRED-INVOICES', $e);
        }
    }
}
