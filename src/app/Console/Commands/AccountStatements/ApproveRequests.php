<?php

namespace App\Console\Commands\AccountStatements;

use App\Actions\Admin\SubscriptionForm\RequestUpdateStatus;
use App\Enums\SubscriptionForm\RequestStatus;
use App\Models\SubscriptionFormRequest;
use App\Services\SubscriptionForm\ApproveRequest;
use Illuminate\Console\Command;

class ApproveRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approve:account-statements';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Account Statements';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public $approveRequest;

    public function __construct()
    {
        parent::__construct();
        $this->approveRequest = new ApproveRequest();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        SubscriptionFormRequest::whereIn('status', [RequestStatus::PendingReview, RequestStatus::InReview])
            ->lazyById()
            ->each(function ($subscriptionFormRequest) {
                $this->approveRequest->handle($subscriptionFormRequest);
            });

        return true;
    }
}
