<?php

namespace App\Jobs;

use App\Actions\Aml\CheckAmlForUser;
use App\Enums\AML\AmlType;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckUserAmlManually implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $initiator;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 6000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $initiator)
    {
        $this->user = $user;
        $this->initiator = $initiator;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            CheckAmlForUser::run(
                $this->user,
                AmlType::ReAssessment,
                $this->initiator->id
            );
        });
    }
}
