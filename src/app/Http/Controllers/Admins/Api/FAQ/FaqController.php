<?php

namespace App\Http\Controllers\Admins\Api\FAQ;

use App\Traits\SendMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admins\FaqListItemResource;
use App\Models\FAQ\Faq;
use App\Http\Resources\Admins\FaqResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{

    use SendMessage;

    /**
     * Get FAQ list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $questions = Faq::paginate();
        return FaqListItemResource::collection($questions);
    }

    /**
     * create new FAQ
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $weight = DB::table((new Faq)->getTable())
            ->selectRaw('min(weight)-1 as weight')
            ->first();

        $data = $request->all();
        $data['weight'] = $weight->weight ?? 0;

        $question = Faq::create($data);

        return new FaqResource($question);
    }

    /**
     * Get FAQ By Id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Faq $faq)
    {
        return new FaqResource($faq);
    }

    /**
     * update FAQ
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Faq $faq)
    {
        $this->validator($request->all())->validate();

        $faq->question_en = $request->input('question_en');
        $faq->question_ar = $request->input('question_ar');
        $faq->answer_en = $request->input('answer_en');
        $faq->answer_ar = $request->input('answer_ar');
        $faq->save();

        return new FaqResource($faq);
    }

    /**
     * remove FAQ
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();

        return response()->json();
    }

    /**
     * Sort FAQ
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sort(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'questions' => 'required|array',
            'questions.*.id' => 'required|exists:faqs,id',
            'questions.*.weight' => 'required|integer',
        ])->validate();

        return DB::transaction(function () use ($request) {

            $questions = $request->input('questions');
            foreach ($questions as $question) {
                FAQ::where('id', $question['id'])
                    ->update(['weight' => $question['weight']]);
            }

            return response()->json([
                "message" => trans('messages.resource_updated')
            ]);
        });
    }

    /**
     * The type validator instance
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data)
    {
        return Validator::make($data, [
            'question_en' => "required|string|max:100|".textValidation(),
            'question_ar' => "required|string|max:100|".textValidation(),
            'answer_en' => "required|string|max:200|".textValidation(),
            'answer_ar' => "required|string|max:200|".textValidation(),
        ]);
    }
}
