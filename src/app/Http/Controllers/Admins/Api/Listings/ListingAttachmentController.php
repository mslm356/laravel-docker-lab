<?php

namespace App\Http\Controllers\Admins\Api\Listings;

use App\Models\File;
use App\Enums\FileType;
use App\Services\Files\CheckFile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use BenSampo\Enum\Rules\EnumValue;
use App\Http\Controllers\Controller;
use App\Enums\Listings\AttachmentType;

class ListingAttachmentController extends Controller
{

    public $checkFile;

    public function __construct()
    {
        $this->checkFile = new CheckFile();
    }

    /**
     * Store attachments
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Listing $listing)
    {
        $data = $this->validate($request, [
            'attachments.*.file' => ['required',
                'mimetypes:application/pdf,image/png,image/jpeg,image/jpg,image/heic,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet/xlsx,application/vnd.ms-excel/xls,application/vnd.ms-word.document.macroenabled.12/doc,application/vnd.openxmlformats-officedocument.wordprocessingml.document/docx' /*, 'mimes:jpg,jpeg,png,pdf,docx,doc,xls,xlsx'*/, 'max:10240'],
            'attachments.*.type' => ['required', new EnumValue(AttachmentType::class)],
        ]);


        foreach ($data['attachments'] ?? [] as $attachment) {

            $extentions = ['pdf','png','jpeg','jpg','heic','xlsx','xls','doc','docx'];
            $this->checkFile->handle($attachment, $extentions);

            $path = File::store($attachment['file'], 'listing-attachments');

            File::create([
                'id' => (string)Str::uuid(),
                'fileable_id' => $listing->id,
                'fileable_type' => get_class($listing),
                'type' => FileType::ListingAttachment,
                'name' => $attachment['file']->getClientOriginalName(),
                'path' => $path,
                'extra_info' => [
                    'type' => $attachment['type']
                ],
            ]);
        }

        return response()->json([], 201);
    }
}
