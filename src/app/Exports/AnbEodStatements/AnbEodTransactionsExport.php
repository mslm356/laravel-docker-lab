<?php

namespace App\Exports\AnbEodStatements;

use App\Models\Anb\AnbEodTransaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AnbEodTransactionsExport implements FromCollection, WithHeadings, WithMapping
{
    protected $startDate;
    protected $endDate;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return AnbEodTransaction::whereBetween(
            'value_date',
            [
                $this->startDate, $this->endDate
            ]
        )
            ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Customer Reference',
            'Bank Reference',
            'Description',
            'Code',
            'Mark',
            'Amount',
            'Value Date',
        ];
    }

    public function map($transaction): array
    {
        return [
            $transaction->id,
            $transaction->customer_ref,
            $transaction->bank_ref,
            $transaction->description,
            $transaction->code,
            $transaction->mark,
            $transaction->amount,
            $transaction->value_date->toDateString(),
        ];
    }
}
