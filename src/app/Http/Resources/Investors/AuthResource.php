<?php

namespace App\Http\Resources\Investors;

use App\Enums\Banking\VirtualAccountType;
use App\Enums\Banking\WalletType;
use App\Enums\Investors\InvestorLevel;
use App\Enums\Investors\InvestorStatus;
use App\Models\Users\UserCanAccess;
use App\Services\MemberShip\GetLastMemberShip;
use App\Support\Users\UserAccess\ManageUserAccess;
use App\Support\VirtualBanking\Helpers\AnbVA;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{

    public $lastMemberShip;

    public function __construct($resource)
    {
        $this->lastMemberShip = new GetLastMemberShip();
        parent::__construct($resource);
    }


    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $user_can_access = UserCanAccess::where('user_id', $this->id)->first();
        if (!$user_can_access) {
            $this->generateAllModules($this);
            $user_can_access = UserCanAccess::where('user_id', $this->id)->first();
        }

        return array_merge(
            [
                'id' => $this->id,
                'is_vaild' => ($user_can_access->login == 200) ? true : false,
                'is_kyc_completed' => $user_can_access->is_kyc_completed,
                'unsuitability_confirmation' => $user_can_access->unsuitability_confirmation,
                'investment' => ($user_can_access->investment == 200) ? true : false,
                'bank_account' => ($user_can_access->bankAccount == 200) ? true : false,
                'transfer' => ($user_can_access->transfer == 200) ? true : false,
                'kyc_suitability' => $user_can_access->kyc_suitability,
                'first_name' => $this->first_name,
                'is_company' => $this->type == "company" ? true : false,
                'last_name' => $this->last_name,
                'nin' => $this->nationalIdentity ? $this->nationalIdentity->nin : null,
                'virtaul_account' => $this->getVirtualAccount($user_can_access),
                'membership' => $this->getMemberShip($this, $user_can_access),

                'status' => [
                    'value' => $this->investor->status,
                    'description' => InvestorStatus::getDescription($this->investor->status)
                ],
                'review_comment' => $this->investor->reviewer_comment,

                'type' => $this->type,
            ],
            $this->getInfoRelatedToInvestorProfile()
        );
    }

    public function getVirtualAccount($user_can_access)
    {
        $wallet = $this->getWallet(WalletType::Investor);

        if ($wallet === null) {
            return null;
        }

        $anbVirtualAccount = $wallet->virtualAccounts()->where('type', VirtualAccountType::Anb)->first();

        if ($user_can_access && $user_can_access->is_kyc_completed) {
            return [
                'number' => AnbVA::toAccountNumber($anbVirtualAccount->identifier)/*.' '. __('mobile_app/message.your_investment_account_in_anp')*/,
                'iban' => $anbVirtualAccount->identifier,
            ];
        }

        return ['number'=>'****************', 'iban'=>'************************'];
    }

    public function getInfoRelatedToInvestorProfile()
    {
        if (!$this->investor->is_kyc_completed) {
            return [];
        }

        return [
            'level' => [
                'value' => $this->investor->level,
                'description' => InvestorLevel::getDescription($this->investor->level)
            ]
        ];
    }

    public function generateAllModules($user)
    {
        $manageUserAccess = new ManageUserAccess();
        $manageUserAccess->handle($user);

    }

    public function getMemberShip($user, $user_can_access)
    {

        $hasMembershipRequest = ($user_can_access->last_membership_request) && $user->member_ship != json_decode($user_can_access->last_membership_request)->member_ship;

        $membership = [
            'current' => ($user->member_ship != 0)?$user->member_ship:1,
            'new_request' => [
                'id' => $hasMembershipRequest ? json_decode($user_can_access->last_membership_request)->id : null,
                'type' => $hasMembershipRequest ? json_decode($user_can_access->last_membership_request)->member_ship : null,
            ],
            'show_popup' => ($user_can_access->last_membership_request) ? json_decode($user_can_access->last_membership_request)->show_popup : null,
        ];

        return $membership ;

    }

}
