<?php

namespace App\Common\ValidationRules\Listings;

class ListingImageRules
{
    static public function get()
    {
        return [
            'files.*' => ['required', 'image', 'max:2048'],
            'files' =>['nullable', 'array', 'min:1', 'max:5']
        ];
    }
}
