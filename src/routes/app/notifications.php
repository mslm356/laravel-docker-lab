<?php

use \App\Http\Controllers\MobileApp\NotificationsController;

Route::middleware(['auth:api', 'jwt_check:api'])->group(function () {

    Route::get('all-notifications', [NotificationsController::class, 'allNotifications']);
    Route::post('notifications/{id}/update', [NotificationsController::class, 'updateNotification']);
});
