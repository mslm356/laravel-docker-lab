<?php


namespace App\Services\Investors;

use App\Support\QueryScoper\Scopes\Investor\MyInvestments\OrderByDateScope;
use Illuminate\Http\Request;

class DashboardServices
{
    public function getMyUnits(Request $request)
    {
        $listing = $request->user()
            ->investments()
            ->where('payout_done', 0)
            ->wherePivot('invested_shares', '>', 0)
            ->with([
                'latestValuation',
                'payouts.investors' => function ($query) use ($request) {
                    $query->where('users.id', $request->user()->id);
                }
            ])
            ->toScopes(OrderByDateScope::class)
            ->get();

        return $listing;
    }

    public function getShowMyUnits($listing, $user)
    {
        $listingInvestedUnits = $user
            ->investments()
            ->wherePivot('invested_shares', '>', 0)
            ->where('listing_id', $listing->id)
            ->with([
                'latestValuation',
                'payouts.investors' => function ($query) use ($user) {
                    $query->where('users.id', $user->id);
                }
            ])
            ->toScopes(OrderByDateScope::class)
            ->first();

        return $listingInvestedUnits;
    }


}
