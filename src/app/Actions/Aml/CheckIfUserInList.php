<?php

namespace App\Actions\Aml;

use App\Enums\AML\AmlList;
use App\Enums\AML\AmlStatus;
use Illuminate\Support\Facades\Log;
use Lorisleiva\Actions\Concerns\AsAction;

class CheckIfUserInList
{
    use AsAction;

    /**
     * Parse AML list.
     *
     * @param \App\Support\Aml\AmlListInterface $list
     * @param \App\Models\Users\User $user
     * @param \App\Models\AML\AmlHistory $amlHistory
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle($list, $user, $amlHistory)
    {
        $failed = false;
        $exception = null;

        try {
            $suspected = $list->check(
                $user->first_name,
                '',
                '',
                $user->last_name,
                '',
                $user->investor->birth_date,
                $user->investor->nin,
                ''
            );
        } catch (\Throwable $th) {
            $failed = true;
            $exception = $th->getMessage();
            Log::critical('UNABLE_TO_CHECK_USER', [
                'list' => AmlList::getDescription($list->getListIndex()),
                'exception' => $th,
                'user_id' => $user->id
            ]);
        }

        if ($failed) {
            $status = AmlStatus::NotChecked;
            $details = [
                'reason' => __('messages.aml_checking_failed_for_list'),
                'error' => $exception
            ];
        } else {
            $status = (count($suspected) === 0 ? AmlStatus::Passed : AmlStatus::Found);
            $details = ['found' => $suspected];
        }

        return $amlHistory->checks()->create([
            'list' => $list->getListIndex(),
            'status' => $status,
            'details' => $details
        ]);
    }
}
