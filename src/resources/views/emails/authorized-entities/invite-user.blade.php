@component('mail::message')
# {{ __('emails/authorized-entities/invite-user.heading') }}

{{ __('emails/authorized-entities/invite-user.body.greeting', ['name' => $user->first_name]) }}
<br>
{{ __('emails/authorized-entities/invite-user.body.introduction', [
    'authorizedEntityName' => $user->authorizedEntity->name,
    'appName' => config('app.name')
]) }}

@component('mail::button', ['url' => $link])
{{ __('emails/authorized-entities/invite-user.actions.join_now') }}
@endcomponent

{{ __('emails/common.thanks') }}<br>
{{ config('app.name') }}
@endcomponent
