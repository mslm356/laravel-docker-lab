<?php

namespace App\Http\Controllers\Investors\General;

use App\Http\Controllers\Controller;
use App\Models\FAQ\Faq;

class GetFaqs extends Controller
{
    /**
     * Get the content of the required page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        return response()->json([
            'data' => Faq::get()->map(fn ($faq) => [
                'id' => $faq->id,
                'question' => $faq->question,
                'answer' => $faq->answer
            ])
        ]);
    }
}
