<div class="flex p-3 flex-wrap {{ $dark ? 'bg-gray-50' : '' }}">
    <div class="flex-grow order-1 md:flex-grow-0">{{ $field['ar'] }}</div>

    <div class="order-3 w-full text-center md:flex-1 md:order-2 md:w-auto">
        {{ $slot }}
    </div>

    <div class="order-2" dir="ltr">{{ $field['en'] }}</div>
</div>
