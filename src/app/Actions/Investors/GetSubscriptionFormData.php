<?php

namespace App\Actions\Investors;

use App\Models\Users\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\Listings\Listing;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Listings\GetCurrentInvestments;
use App\Models\Users\InvestorSubscriptionToken;
use App\Actions\Investing\CalculateInvestingAmountAndFees;
use App\Enums\Banking\WalletType;
use Alkoumi\LaravelArabicTafqeet\Tafqeet;

class GetSubscriptionFormData
{
    use AsAction;

    /**
     * Undocumented function
     *
     * @param \App\Models\Users\User $user
     * @param \App\Models\Listings\Listing $listing
     * @param \Cknow\Money\Money $amount
     * @param \Illuminate\Support\Collection $currentInvestments
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function handle(InvestorSubscriptionToken $investorSubscriptionToken, User $user, Listing $listing, $amount, $currentInvestments)
    {
        $arabic = new \ArPHP\I18N\Arabic();

        $arabic->setNumberFeminine(1);
        $arabic->setNumberFormat(1);

        $address = $user->nationalIdentity->addresses->last();
        $authoirzedEntity = optional($listing->authoirzedEntity)->name;

        return  [
            'id' => $investorSubscriptionToken->id,
            'date' => now('Asia/Riyadh')->format('Y-m-d'),
            'value_date' => now('Asia/Riyadh')->format('Y-m-d'),
            //'is_new_investor' => $currentInvestments === 0,
            'investor_id' => $user->id,
            'fund_name_ar' => $listing->title_ar,
            'fund_name_en' => $listing->title_en,
            'authoirzedEntity' => $authoirzedEntity,
            'fund_id' => $listing->id,
            'currentInvestmentUnits' => $investorSubscriptionToken->shares,
            'amount' => $amount->formatByDecimal(),
            'amount_fmt' => number_format($amount->formatByDecimal(),2),
            'amount_words' => Tafqeet::inArabic($amount->formatByDecimal())  , //$arabic->int2str(clean_decimal($amount->formatByDecimal())),
            'investor_type' => $user->type == 'company' ? 'company':'individual',
            'investor_name_en' => $user->getAbsherName('ar'),
            'investor_name_ar' => $user->getAbsherName('en'),
            'investor_national_id' => $user->type == 'company' ? $user->c_number : $user->nationalIdentity->nin,
            'nationality' => $user->nationalIdentity->nationality,
            'investor_phone' => $user->phone_number,
            'investor_po_code' => "{$address['post_code']} - {$address['additional_number']}",
            'investor_city' => $address['city'],
            'wallet_id' => $user->getWallet(WalletType::Investor)->uuid
        ];
    }

    public function asController(Request $request, Listing $listing, InvestorSubscriptionToken $investorSubscriptionToken)
    {

        $user = $request->user('sanctum');

        if ($listing->id != $investorSubscriptionToken->listing_id || $user->id != $investorSubscriptionToken->user_id) {
            throw new AuthorizationException;
        }

        return response()->json([
            'data' => GetSubscriptionFormData::run(
                $investorSubscriptionToken,
                $user,
                $listing,
                CalculateInvestingAmountAndFees::run($investorSubscriptionToken->listing, $investorSubscriptionToken->shares)['total'],
                GetCurrentInvestments::run($listing)->where('investor_id', $user->id)
            )
        ]);
    }

    public function authorize(ActionRequest $request)
    {
        $token = $request->route('investor_subscription_token');
        return $request->user()->id === $token->user_id && !$token->expired_at;
    }
}
