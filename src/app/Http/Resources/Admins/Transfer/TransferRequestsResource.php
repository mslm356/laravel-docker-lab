<?php

namespace App\Http\Resources\Admins\Transfer;

use App\Enums\Banking\WalletType;
use App\Enums\Transfer\TransferStatus;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Money\Money;

class TransferRequestsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $wallet = $this->user ? $this->user->getWallet(WalletType::Investor) : null;


        return [

            'id' => $this->id,
            'username' => $this->full_name,
            'user_id' => $this->user_id,
            'balance' => $this->user && $wallet ? ($wallet->balance / 100) : 0.00 ,
            'balance_fmt' => $this->user && $wallet ? number_format(($wallet->balance   / 100), 2) : '0.00',
            'status' => $this->getStatus($this->status),
            'bank' => $this->bank,
            'amount' => \money($this->amount)->formatByDecimal(),
            'nin' => $this->nin,
            'reviewer' => optional($this->reviewer)->full_name,
            'reviewer_comment' => $this->reviewer_comment,
            'created_at' => Carbon::parse($this->created_at)->timezone('Asia/Riyadh')->format('Y-m-d h:i'),
            'updated_at' => Carbon::parse($this->updated_at)->timezone('Asia/Riyadh')->format('Y-m-d h:i'),

        ];
    }


    public function getStatus($value)
    {

        $status = [
            'value' => 1,
            'description' => __('Pending'),
        ];

        if ($value == TransferStatus::Rejected) {
            $status['value'] = $value;
            $status['description'] = __('Rejected');
        } elseif ($value == TransferStatus::InReview) {
            $status['value'] = $value;
            $status['description'] = __('In Review');
        } elseif ($value == TransferStatus::Approved) {
            $status['value'] = $value;
            $status['description'] = __('Approved');
        } elseif ($value == TransferStatus::Refund) {
            $status['value'] = $value;
            $status['description'] = __('Refund');
        } else {
            return $status;
        }

        return $status;
    }
}
