<?php

namespace App\Http\Resources\Admins\Investors\ProfessionalUpgrade;

use App\Http\Resources\Common\Investors\ProfessionalUpgrade\UpgradeRequestResource as CommonUpgradeRequestResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UpgradeRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(
            (new CommonUpgradeRequestResource($this->resource))->toArray($request),
            [
                'user' => [
                    'id' => $this->user_id,
                    'name' => $this->user->full_name,
                ],
                'reviewer' => optional($this->reviewer)->full_name,
                'reviewer_comment' => $this->reviewer_comment,
            ]
        );
    }
}
